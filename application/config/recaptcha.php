<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LehTiEUAAAAALdRe6-n1Q4tpggkR5zza7kVaIxm';
$config['recaptcha_secret_key'] = '6LehTiEUAAAAADaQl035h1Slo_OvujHBibjoNmA9';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'id';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
