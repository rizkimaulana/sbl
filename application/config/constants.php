<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('ADD', 'add');
define('EDIT', 'edit');
define('REMOVE', 'remove');
define('VIEW', 'view');
define('CETAK', 'cetak');

//HALAMAN PESANTREN
define ('DASHBOARD_PESANTREN','DASHBOARD_PESANTREN');
define ('PESANTREN_CLIENT','PESANTREN_CLIENT');
    define ('REG_KYAI','REG_KYAI');
    define ('REG_USTADZ','REG_USTADZ');
    define ('REG_ANGGOTA','REG_ANGGOTA');
    //define ('AKTIVASI_ANGGOTA','AKTIVASI_ANGGOTA');
    define ('LIST_ANGGOTA','LIST_ANGGOTA');
    define ('ANGSURAN_PESANTREN','ANGSURAN_PESANTREN');
    //define ('KONFIRMASI_ANGSURAN_ANGGOTA','KONFIRMASI_ANGSURAN_ANGGOTA');
    //define ('KONFIRMASI_FEE_ANGGOTA','KONFIRMASI_FEE_ANGGOTA');
//BATAS HALAMAN PESANTREN

//HALAMAN AFFILIATE
define ('DASHBOARD_AFFILIATE','DASHBOARD_AFFILIATE');

define ('JAMAAH_REGISTRATION','JAMAAH_REGISTRATION');
        define ('JAMAAH_REGISTRATION_FOR_AFFILIATE','JAMAAH_REGISTRATION_FOR_AFFILIATE');

        define ('REGISTRASI_AFFILIATE_PROMO','REGISTRASI_AFFILIATE_PROMO');
        define ('FAKTUR_REGISTRASI_FOR_AFFILIATE','FAKTUR_REGISTRASI_FOR_AFFILIATE');
        define ('JAMAAH_KUOTA_AFFILIATE','JAMAAH_KUOTA_AFFILIATE');
        define ('JAMAAH_KUOTA_PROMO_AFFILIATE','JAMAAH_KUOTA_PROMO_AFFILIATE');
        
define ('MEMBER_MGM','MEMBER_MGM');
define ('CETAK_BILYETAFFILIATE','CETAK_BILYETAFFILIATE');
define ('REFRENSI_AFFILIATE','REFRENSI_AFFILIATE');

define ('DATA_MANIFEST_AFFILIATE','DATA_MANIFEST_AFFILIATE');
        define ('MANIFEST_AFFILIATE','MANIFEST_AFFILIATE');
	define ('MANIFEST_PERWAKILAN','MANIFEST_PERWAKILAN');
	 define ('INPUT_MANIFEST','INPUT_MANIFEST');

define ('PROFIL','PROFIL');

define ('DATA_JAMAAH_FOR_AFFILIATE','DATA_JAMAAH_FOR_AFFILIATE');
        define ('JAMAAH_NOTAKTIF_FOR_AFFILIATE','JAMAAH_NOTAKTIF_FOR_AFFILIATE');
        define ('JAMAAH_AKTIF_FOR_AFFILIATE','JAMAAH_AKTIF_FOR_AFFILIATE');
        define ('JAMAAH_ALUMNI_FOR_AFFILIATE','JAMAAH_ALUMNI_FOR_AFFILIATE');

        define ('DATA_JAMAAH_NOTAKTIF_FOR_AFFILIATE','DATA_JAMAAH_NOTAKTIF_FOR_AFFILIATE');
        define ('DATA_JAMAAH_AKTIF_FOR_AFFILIATE','DATA_JAMAAH_AKTIF_FOR_AFFILIATE');
        define ('DATA_JAMAAH_ALUMNI_FOR_AFFILIATE','DATA_JAMAAH_ALUMNI_FOR_AFFILIATE');
        define ('BOOKING_SEATS_AFFILIATE','BOOKING_SEATS_AFFILIATE');

define ('FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE','FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE');
define ('FEE_FREE_TUNAI_REGISTRASI_JAMAAH_FOR_AFFILIATE','FEE_FREE_TUNAI_REGISTRASI_JAMAAH_FOR_AFFILIATE');
define ('CLAIM_FEE_INPUT','CLAIM_FEE_INPUT');
define ('VIEW_GAJI_CABANG','VIEW_GAJI_CABANG');

define ('FEE_AFFILIATE','FEE_AFFILIATE');
        define ('FEE_KUOTA_AFFILIATE','FEE_KUOTA_AFFILIATE');
        define ('FEE_POSTING_SPONSOR_AFFILIATE','FEE_POSTING_SPONSOR_AFFILIATE');
        define ('FEE_POSTING_JAMAAH_AFFILIATE','FEE_POSTING_JAMAAH_AFFILIATE');
        
define ('FEE_MGM','FEE_MGM');
        define ('FEE_POSTING_MGM','FEE_POSTING_MGM');
        define ('FEE_SPONSOR_MGM','FEE_SPONSOR_MGM');

define ('OPERASIONAL','OPERASIONAL');
    define ('CLAIM_BIAYA_OPERASIONAL_CABANG','CLAIM_BIAYA_OPERASIONAL_CABANG');
    define ('CLAIM_BIAYA_OPERASIONAL_INVOICE','CLAIM_BIAYA_OPERASIONAL_INVOICE');
    define ('LIST_CLAIM_OPERASIONAL','LIST_CLAIM_OPERASIONAL');
    define ('APPROVAL_CLAIM_BIAYA_OPERASIONAL','APPROVAL_CLAIM_BIAYA_OPERASIONAL');
    define ('CLAIM_OPERASIONAL_MANASIK','CLAIM_OPERASIONAL_MANASIK');
    define ('CLAIM_OPERASIONAL_MANASIK_GM','CLAIM_OPERASIONAL_MANASIK_GM');
    define ('LIST_CLAIM_MANASIK','LIST_CLAIM_MANASIK');
    
// BATAS HALAMAN AFFILLIATE 


define ('DASHBOARD','DASHBOARD');
//PESANTREN
define ('PESANTREN','PESANTREN');
    define ('REGISTRASI_FOUNDER','REGISTRASI_FOUNDER');
    define ('REGISTRASI_KYAI','REGISTRASI_KYAI');
    define ('REGISTRASI_USTADZ','REGISTRASI_USTADZ');
    define ('REGISTRASI_ANGGOTA','REGISTRASI_ANGGOTA');
    define ('AKTIVASI_ANGGOTA','AKTIVASI_ANGGOTA');
    define ('DATA_ANGGOTA','DATA_ANGGOTA');
    define ('ANGSURAN_ANGGOTA','ANGSURAN_ANGGOTA');
    define ('KONFIRMASI_ANGSURAN_ANGGOTA','KONFIRMASI_ANGSURAN_ANGGOTA');
    define ('KONFIRMASI_FEE_ANGGOTA','KONFIRMASI_FEE_ANGGOTA');

//BATAS PESANTREN

define ('REGISTRATION','REGISTRATION');
        define ('AFFILIATE','AFFILIATE');
        // define ('DEPARTURE_GROUP','DEPARTURE_GROUP');
        define ('REGISTRASI_JAMAAH','REGISTRASI_JAMAAH');
        define ('REGISTRASI_JAMAAH_PROMO','REGISTRASI_JAMAAH_PROMO');
        define ('REGISTRASI_JAMAAH_VOUCHER','REGISTRASI_JAMAAH_VOUCHER');
        define ('JAMAAH_REWARD_SAHABAT','JAMAAH_REWARD_SAHABAT');
        define ('JAMAAH_PINDAH_PAKET','JAMAAH_PINDAH_PAKET');
        define ('FAKTUR_REGISTRASI','FAKTUR_REGISTRASI');
        define ('JAMAAH_KUOTA_CABANG','JAMAAH_KUOTA_CABANG');
        define ('JAMAAH_KUOTA_ADMIN','JAMAAH_KUOTA_ADMIN');
        define ('JAMAAH_KUOTA_PROMO_ADMIN','JAMAAH_KUOTA_PROMO_ADMIN');
        define ('BOOKING_SEATS_ADMIN','BOOKING_SEATS_ADMIN');


define ('REWARD_UMROH_MGM','REWARD_UMROH_MGM');

define ('AKTIVASI','AKTIVASI');
       
        define ('AKTIVASI_JAMAAH_KUOTA','AKTIVASI_JAMAAH_KUOTA');
        define ('AKTIVASI_MANUAL','AKTIVASI_MANUAL');
        define ('AKTIVASI_PERWAKILAN','AKTIVASI_PERWAKILAN');
        define ('AKTIVASI_RESCHEDULE','AKTIVASI_RESCHEDULE');
        define ('EDIT_AKTIVASI_BY_INVOICE','EDIT_AKTIVASI_BY_INVOICE');
        define ('EDIT_AKTIVASI_BY_JAMAAH','EDIT_AKTIVASI_BY_JAMAAH');
        define ('AKTIVASI_BOOKING_SEATS','AKTIVASI_BOOKING_SEATS');
        define ('AKTIVASI_PELUNSAN_BOOKING_SEATS','AKTIVASI_PELUNSAN_BOOKING_SEATS');


define ('DATA_JAMAAH','DATA_JAMAAH');
        define ('JAMAAH_NOTAKTIF','JAMAAH_NOTAKTIF');
        define ('JAMAAH_AKTIF','JAMAAH_AKTIF');
        define ('JAMAAH_ALUMNI','JAMAAH_ALUMNI');

        define ('DATA_JAMAAH_NOTAKTIF','DATA_JAMAAH_NOTAKTIF');
        define ('DATA_JAMAAH_AKTIF','DATA_JAMAAH_AKTIF');
        define ('DATA_JAMAAH_ALUMNI','DATA_JAMAAH_ALUMNI');

define ('MONITORING','MONITORING');
        define ('MONITORING_KEBERANGKATAN','MONITORING_KEBERANGKATAN');

define ('PENGAJUAN_TALANGAN_CICILAN','PENGAJUAN_TALANGAN_CICILAN');
        define ('INPUT_CICILAN_TALANGAN','INPUT_CICILAN_TALANGAN');


define ('BILYET','BILYET');
        define ('INFO_BILYET','INFO_BILYET');
        define ('BILYET_REWARD_SAHABAT','BILYET_REWARD_SAHABAT');
        define ('GROUPING_BILYET_REWARD_SAHABAT','GROUPING_BILYET_REWARD_SAHABAT');
        define ('INSERT_REWARD_SAHABAT','INSERT_REWARD_SAHABAT');
        
define ('LOGISTIK','LOGISTIK');
        define ('PENGIRIMAN_BARANG','PENGIRIMAN_BARANG');

define ('MANIFEST','MANIFEST');
        define ('HOTEL','HOTEL');
        define ('ROOM_GROUP','ROOM_GROUP');
        define ('GROUP_KEBERANGKATAN','GROUP_KEBERANGKATAN');
        define ('UPDATE_DATA_MANIFEST','UPDATE_DATA_MANIFEST');
        define ('DATA_MANIFEST','DATA_MANIFEST');
	define ('MANIFEST_CABANG','MANIFEST_CABANG');
        define ('MANIFEST_GM','MANIFEST_GM');

define ('FINANCE','FINANCE');
        define ('KELOLAH_FEE_POSTING','KELOLAH_FEE_POSTING');
        define ('KELOLAH_FEE_POSTING_NEW','KELOLAH_FEE_POSTING_NEW');
        define ('KELOLAH_FEE_POSTING_SPONSOR','KELOLAH_FEE_POSTING_SPONSOR');
        define ('KELOLAH_FEE_KUOTA_SPONSOR','KELOLAH_FEE_KUOTA_SPONSOR');

        define ('REWARD_POSTING_MGM','REWARD_POSTING_MGM');
        define ('FEE','FEE');
        define ('FEE_INPUT','FEE_INPUT');
        define ('GAJI_CABANG','GAJI_CABANG');
        define ('BOOKING_REPORT','BOOKING_REPORT');
        define ('DETAIL_JAMAAH_REPORTING','DETAIL_JAMAAH_REPORTING');
        //define ('OPERASIONAL','OPERASIONAL');
        define ('OPERASIONAL_HANDLING','OPERASIONAL_HANDLING');
        define ('BIAYA_OPERASIONAL_MANASIK','BIAYA_OPERASIONAL_MANASIK');
        define ('OPERASIONAL_MANASIK','OPERASIONAL_MANASIK');

define ('MUTASI','MUTASI');
        define ('MUTASI_NAMA','MUTASI_NAMA');
        define ('MUTASI_PAKET','MUTASI_PAKET');

define ('BIAYA_LAIN_LAIN','BIAYA_LAIN_LAIN');
        define ('BIAYA_AKOMODASI','BIAYA_AKOMODASI');
        define ('BIAYA_MUHRIM','BIAYA_MUHRIM');
        define ('LAP_AKOMODASI','LAP_AKOMODASI');
        define ('LAP_MUHRIM','LAP_MUHRIM');

define ('DATA_PRODUCT','DATA_PRODUCT');
        define ('PRODUCT','PRODUCT');
        define ('PRODUCT_PRICE','PRODUCT_PRICE');
        
define ('LAPORAN','LAPORAN');
        define ('LAPORAN_REKAP_REGISTRASI','LAPORAN_REKAP_REGISTRASI');
        define ('LAPORAN_DETAIL_REGISTRASI','LAPORAN_DETAIL_REGISTRASI');
        
        define ('LAPORAN_PINDAH_PAKET','LAPORAN_PINDAH_PAKET');
        define ('LAPORAN_REG_GM','LAPORAN_REG_GM');
        define ('HISTORY_MUTASI_NAMA','HISTORY_MUTASI_NAMA');
        
        //LAPORAN PESANTREN
        define ('LAP_REGISTRASI','LAP_REGISTRASI');
        define ('LAP_ANGSURAN_ANGGOTA','LAP_ANGSURAN_ANGGOTA');
        define ('LAP_FEE_ANGGOTA','LAP_FEE_ANGGOTA');
        define ('JURNAL_HARIAN','JURNAL_HARIAN');
        //BATAL LAPORAN PESANTREN
        define ('LAP_BAYAR_MANASIK','LAP_BAYAR_MANASIK');
        define ('LAPORAN_BIAYA','LAPORAN_BIAYA');
        
define ('MASTER_DATA','MASTER_DATA');
        define ('BANDARA','BANDARA');
        define ('CLUSTER','CLUSTER');
        define ('FAMILY_RELATION','FAMILY_RELATION');
        define ('MUHRIM','MUHRIM');
        define ('REFERENSI_MUHRIM','REFERENSI_MUHRIM');
        define ('ROOM','ROOM');
        define ('ROOM_TYPE','ROOM_TYPE');
        define ('ROOM_CATEGORY','ROOM_CATEGORY');
        define ('PAKET','PAKET');

define ('SETTINGS','SETTINGS');     
		define ('SCHEDULE','SCHEDULE');
                define ('SETTING_DATA','SETTING_DATA');
   
define ('USER_MANAGEMENT','USER_MANAGEMENT');
        define ('USER','USER');
        define ('JABATAN','JABATAN');
