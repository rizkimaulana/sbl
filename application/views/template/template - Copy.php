<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <script type="text/javascript">var base_url = '<?php echo base_url();?>';</script>
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icon.png">

  <title>Solusi Balad Lumampah</title>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
  
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery.gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/font-awesome-4/css/font-awesome.min.css">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/jquery-ui/jquery-ui.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/chosen/chosen.css">
    <link href="<?php echo base_url();?>assets/css/front/style.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery.nanoscroller/nanoscroller.css" />
  <!--dashboard-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery.codemirror/lib/codemirror.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery.codemirror/theme/ambiance.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery.vectormaps/jquery-jvectormap-1.2.2.css" type="text/css" media="screen"/> 
    <!--dashboard-->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" /> 
  <link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
   <link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url();?>assets/css/back.css" rel="stylesheet"> -->
  <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
     <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
  <link href='<?php echo base_url();?>assets/css/jquery.autocomplete.css' rel='stylesheet' />
    <link href="<?php echo base_url();?>assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     

`  
<!-- 
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/transition.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/alert.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/modal.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/dropdown.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/scrollspy.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/tab.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/popover.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/button.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/collapse.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/carousel.js"></script>
     -->
</head>
<body>

<div id="cl-wrapper">

  <div class="cl-sidebar">
    <div class="cl-toggle"><i class="fa fa-bars"></i></div>
    <div class="cl-navblock">
      <div class="menu-space">
        <div class="content">
          <div class="sidebar-logo">
            <div class="logo">
                <a href="index2.html"></a>
            </div>
          </div>
          <div class="side-user">
            <div class="avatar"><img src="<?php echo base_url();?>assets/images/avatar6.jpg" alt="Avatar" /></div>
         <div class="info">
              <p><?php echo $this->session->userdata('nama');?></b><span><a href="#"></i></a></span></p>
             <!--  <div class="progress progress-user">
                <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                  <span class="sr-only">50% Complete (success)</span>
                </div>
              </div> -->
            </div> 
          </div>
          <ul class="cl-vnavigation">
            <!-- <li  ><a href="<?php echo base_url().'dashboard' ?>"><i class="fa fa-home"></i><span>Dashboard</span></a></li> -->
          <?php if($this->general->privilege_check(DASHBOARD,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='profil') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>dashboard"><i class="fa fa-home"></i> <span>Dashboard </span></a>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check_affiliate(DASHBOARD_AFFILIATE,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='profil') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>dashboard_affiliate/dashboard"><i class="fa fa-home"></i> <span>Dashboard </span></a>
            </li>
        <?php } ?>

<!-- HALAMAN AFFILLIATE -->
        <?php if($this->general->privilege_check_affiliate(JAMAAH_REGISTRATION,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='jamaah_registration') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Jamaah Registration </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check_affiliate(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>registrasi_affiliate">Jamaah Registration</a></li>
                    <?php } ?>

                     <?php if($this->general->privilege_check_affiliate(FAKTUR_REGISTRASI_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>registrasi_affiliate/get_registrasi">Faktur</a></li>
                    <?php } ?>
                   
                </ul>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check_affiliate(PROFIL,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='profil') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>profil"><i class="fa fa-list-alt"></i> <span>Profil </span></a>
            </li>
        <?php } ?>

          <?php if($this->general->privilege_check_affiliate(DATA_JAMAAH_FOR_AFFILIATE,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='data_jamaahaffiliate') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Data Jamaah </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check_affiliate(JAMAAH_NOTAKTIF_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaahaffiliate">Jamaah Belum Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check_affiliate(JAMAAH_AKTIF_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaahaffiliate/jamaah_aktifaffiliate">Jamaah Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check_affiliate(JAMAAH_ALUMNI_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaahaffiliate/jamaah_alumni">Jamaah Alumni</a></li>
                    <?php } ?>
                    
                    <?php if($this->general->privilege_check_affiliate(DATA_JAMAAH_NOTAKTIF_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaahaffiliate/data_jamaahaffiliate/data_jamaahbelumaktif">Data Jamaah Belum Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check_affiliate(DATA_JAMAAH_AKTIF_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaahaffiliate/jamaah_aktifaffiliate/data_jamaahaktif">Data Jamaah Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check_affiliate(DATA_JAMAAH_ALUMNI_FOR_AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaahaffiliate/jamaah_alumni/data_jamaahalumni">Data Jamaah Alumni</a></li>
                    <?php } ?>
                  
                </ul>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check_affiliate(FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='fee_free_hemat_registrasi_affiliate') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>claim/claim_feefreehemat"><i class="fa fa-list-alt"></i> <span>Claim Fee dan Free Paket Hemat </span></a>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check_affiliate(FEE_FREE_TUNAI_REGISTRASI_JAMAAH_FOR_AFFILIATE,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='fee_free_tunai_registrasi_affiliate') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>claim/claim_feefreetunai"><i class="fa fa-list-alt"></i> <span>Claim Fee dan Free Paket TUNAI </span></a>
            </li>
        <?php } ?>

         <?php if($this->general->privilege_check_affiliate(CLAIM_FEE_INPUT,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='clime_fee_input') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>claim/claimfee_input"><i class="fa fa-list-alt"></i> <span>Fee Input Registrasi Jamaah </span></a>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check_affiliate(VIEW_GAJI_CABANG,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='view_gaji_cabang') ? 'active' : '';?> dropdown">
                <a href="<?php echo base_url();?>gaji_cabang"><i class="fa fa-list-alt"></i> <span>Gaji Cabang </span></a>
            </li>
        <?php } ?>

<!-- BATAS HALAMAN AFFILLIATE -->

        <?php if($this->general->privilege_check(REGISTRATION,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='registration') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Registration </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check(AFFILIATE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>affiliate">Affiliate</a></li>
                    <?php } ?>
                    
                   <!--    <?php if($this->general->privilege_check(DEPARTURE_GROUP,'view')){ ?>
                    <li><a href="<?php echo base_url();?>departure_group">Departure Group</a></li>
                    <?php } ?> -->

                    <?php if($this->general->privilege_check(REGISTRASI_JAMAAH,'view')){ ?>
                    <li><a href="<?php echo base_url();?>registrasi_jamaah/index">Registrasi Jamaah</a></li>
                    <?php } ?>

                   <?php if($this->general->privilege_check(REGISTRASI_JAMAAH_REWARD_VOUCHER,'view')){ ?>
                    <li><a href="<?php echo base_url();?>reg_jamaahrewardvoucher/index">Registrasi Jamaah REWARD & VOUCHER</a></li>
                    <?php } ?>

                   <?php if($this->general->privilege_check(FAKTUR_REGISTRASI,'view')){ ?>
                    <li><a href="<?php echo base_url();?>registrasi_jamaah/get_registrasi">Faktur</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(AKTIVASI_MANUAL,'view')){ ?>
                    <li><a href="<?php echo base_url();?>aktivasi_manual">Aktivasi Manual</a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>


        <?php if($this->general->privilege_check(DATA_JAMAAH,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='data_jamaah') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Data Jamaah </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check(JAMAAH_NOTAKTIF,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaah">Jamaah Belum Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(JAMAAH_AKTIF,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaah/jamaah_aktif">Jamaah Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(JAMAAH_ALUMNI,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaah/jamaah_alumni">Jamaah Alumni</a></li>
                    <?php } ?>
                    

                     <?php if($this->general->privilege_check(DATA_JAMAAH_NOTAKTIF,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaah/data_jamaahbelumaktif">Data Jamaah Belum Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(DATA_JAMAAH_AKTIF,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaah/jamaah_aktif/data_jamaahaktif">DataJamaah Aktif</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(DATA_JAMAAH_ALUMNI,'view')){ ?>
                    <li><a href="<?php echo base_url();?>data_jamaah/jamaah_alumni/data_jamaahalumni">Data Jamaah Alumni</a></li>
                    <?php } ?>
                  
                </ul>
            </li>
        <?php } ?>


           <?php if($this->general->privilege_check(FINANCE,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='finance') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Finance </span></a>
                <ul class="sub-menu">

                  

                    <?php if($this->general->privilege_check(FEE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>finance/fee">Fee Affiliate</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(FEE_INPUT,'view')){ ?>
                    <li><a href="<?php echo base_url();?>finance/fee_input">Fee Input Affiliate</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(GAJI_CABANG,'view')){ ?>
                    <li><a href="<?php echo base_url();?>finance/gaji_cabang">Gaji Cabang</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(BOOKING_REPORT,'view')){ ?>
                    <li><a href="<?php echo base_url();?>finance/booking_report">Reporting Keuangan</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(DETAIL_JAMAAH_REPORTING,'view')){ ?>
                    <li><a href="<?php echo base_url();?>finance/booking_report/laporan_detail_jamaah_keungan">Detail Jamaah Reporting</a></li>
                    <?php } ?>
                  
                </ul>
            </li>
        <?php } ?>


         <?php if($this->general->privilege_check(MUTASI,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='mutasi') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Mutasi </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check(MUTASI_NAMA,'view')){ ?>
                    <li><a href="<?php echo base_url();?>mutasi/mutasi_nama">Mutasi Nama</a></li>
                    <?php } ?>

                  <?php if($this->general->privilege_check(MUTASI_PAKET,'view')){ ?>
                    <li><a href="<?php echo base_url();?>mutasi/mutasi_paket">Mutasi PAket</a></li>
                    <?php } ?>

                    
                  
                </ul>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check(MANIFEST,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='manifest') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Manifest </span></a>
                <ul class="sub-menu">

                    <?php if($this->general->privilege_check(HOTEL,'view')){ ?>
                    <li><a href="<?php echo base_url();?>hotel">Hotel</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(ROOM_GROUP,'view')){ ?>
                    <li><a href="<?php echo base_url();?>room_group">Room Group</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(DATA_MANIFEST,'view')){ ?>
                    <li><a href="<?php echo base_url();?>manifest">Data Manifest</a></li>
                    <?php } ?>

                   
                    
                  
                </ul>
            </li>
        <?php } ?>

            <?php if($this->general->privilege_check(DATA_PRODUCT,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='data_product') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Data Product </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check(PRODUCT,'view')){ ?>
                    <li><a href="<?php echo base_url();?>product">Product</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(PRODUCT_PRICE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>product_price">Product Price</a></li>
                    <?php } ?>
               
                </ul>
            </li>
        <?php } ?>

        <?php if($this->general->privilege_check(MASTER_DATA,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='master_data') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Data Master </span></a>
                <ul class="sub-menu">

                     <?php if($this->general->privilege_check(BANDARA,'view')){ ?>
                    <li><a href="<?php echo base_url();?>bandara">Bandara</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(FAMILY_RELATION,'view')){ ?>
                    <li><a href="<?php echo base_url();?>family_relation">Family Relation</a></li>
                    <?php } ?>
                
                    <?php if($this->general->privilege_check(MUHRIM,'view')){ ?>
                    <li><a href="<?php echo base_url();?>muhrim">Indikator Muhrim</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(REFERENSI_MUHRIM,'view')){ ?>
                    <li><a href="<?php echo base_url();?>muhrim/hubungan_muhrim">Referensi Muhrim</a></li>
                    <?php } ?>

                     <?php if($this->general->privilege_check(ROOM,'view')){ ?>
                    <li><a href="<?php echo base_url();?>room">Room </a></li>
                    <?php } ?>

                     <?php if($this->general->privilege_check(ROOM_TYPE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>room/room_type">Room Type</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(ROOM_CATEGORY,'view')){ ?>
                    <li><a href="<?php echo base_url();?>room/room_category">Room Category</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>

            <?php if($this->general->privilege_check(SETTINGS,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='settings') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Settings </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check(SCHEDULE,'view')){ ?>
                    <li><a href="<?php echo base_url();?>schedule">Schedules</a></li>
                    <?php } ?>

                    <?php if($this->general->privilege_check(SETTING_DATA,'view')){ ?>
                    <li><a href="<?php echo base_url();?>setting_data">Setting Data</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
          

              <?php if($this->general->privilege_check(USER_MANAGEMENT,'view')){ ?>
            <li class="<?php echo ($this->session->userdata('menu')=='user_management') ? 'active' : '';?> dropdown">
                <a href="#"><i class="fa fa-smile-o"></i> <span>User Management </span></a>
                <ul class="sub-menu">
                    <?php if($this->general->privilege_check(USER,'view')){ ?>
                    <li><a href="<?php echo base_url();?>user">User</a></li>
                    <?php } ?>
                     <?php if($this->general->privilege_check(JABATAN,'view')){ ?>
                    <li><a href="<?php echo base_url();?>jabatan">Jabatan</a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>

          
           
      
          </ul>
        </div>
      </div>
      <div class="text-right collapse-button" style="padding:7px 9px;">
        <!-- <input type="text" class="form-control search" placeholder="Search..." /> -->
        <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
      </div>
    </div>
  </div>
  <div class="container-fluid" id="pcont">
   <!-- TOP NAVBAR -->
  <div id="head-nav" class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-collapse">
        <ul class="nav navbar-nav navbar-right user-nav">
          <li class="dropdown profile_menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="<?php echo base_url();?>assets/images/avatar6-2.jpg" /><span><?php echo $this->session->userdata('nama');?></span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">My Account</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
              <li class="divider"></li>
              <?php $jabatan_id = $this->session->userdata('jabatan_id');?>
              <?php if ($jabatan_id=='13') { ?>
              <li><a href="<?php echo base_url();?>logout/logout_affiliate">Sign Out</a></li>
              <?php }else{ ?>
              <li><a href="<?php echo base_url();?>logout">Sign Out</a></li>
              <?php }?>
            </ul>
          </li>
        </ul>     
        <ul class="nav navbar-nav not-nav">
          <li class="button dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class=" fa fa-inbox"></i></a>
            <ul class="dropdown-menu messages">
              <li>
                <div class="nano nscroller">
                  <div class="content">
                    <ul>
                      <li>
                        <a href="#">
                          <img src="<?php echo base_url();?>assets/images/avatar2.jpg" alt="avatar" /><span class="date pull-right">13 Sept.</span> <span class="name">Daniel</span> Hey! How are you?
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img src="<?php echo base_url();?>assets/images/avatar_50.jpg" alt="avatar" /><span class="date pull-right">20 Oct.</span><span class="name">Adam</span> Hi! Can you fix my phone?
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img src="<?php echo base_url();?>assets/images/avatar4_50.jpg" alt="avatar" /><span class="date pull-right">2 Nov.</span><span class="name">Michael</span> Regards!
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img src="<?php echo base_url();?>assets/images/avatar3_50.jpg" alt="avatar" /><span class="date pull-right">2 Nov.</span><span class="name">Lucy</span> Hello, my name is Lucy
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <ul class="foot"><li><a href="#">View all messages </a></li></ul>           
              </li>
            </ul>
          </li>
          <li class="button dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span class="bubble">2</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="nano nscroller">
                  <div class="content">
                    <ul>
                      <li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Daniel</b> is now following you <span class="date">2 minutes ago.</span></a></li>
                      <li><a href="#"><i class="fa fa-male success"></i> <b>Michael</b> commented on your link <span class="date">15 minutes ago.</span></a></li>
                      <li><a href="#"><i class="fa fa-bug warning"></i> <b>Mia</b> commented on post <span class="date">30 minutes ago.</span></a></li>
                      <li><a href="#"><i class="fa fa-credit-card danger"></i> <b>Andrew</b> sent you a request <span class="date">1 hour ago.</span></a></li>
                    </ul>
                  </div>
                </div>
                <ul class="foot"><li><a href="#">View all activity </a></li></ul>           
              </li>
            </ul>
          </li>
          <li class="button"><a class="toggle-menu menu-right push-body" href="javascript:;" class="speech-button"><i class="fa fa-comments"></i></a></li>        
        </ul>

      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>
  
    
  <div class="cl-mcont">    

    <div class="cl-mcont">
      <!-- <h3 class="text-center">Content goes here!</h3> -->
          <?php echo $contents; ?> 
    </div>  
  </div>
  
  </div> 
  
</div>
<!-- Right Chat-->
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right side-chat">
  <div class="header">
    <h3>Chat</h3>
  </div>
  <div class="sub-header" href="#">
    <div class="icon"><i class="fa fa-user"></i></div> <p>Online (4)</p>
  </div>
  <div class="content">
    <p class="title">Family</p>
    <ul class="nav nav-pills nav-stacked contacts">
      <li class="online"><a href="#"><i class="fa fa-circle-o"></i> Michael Smith</a></li>
      <li class="online"><a href="#"><i class="fa fa-circle-o"></i> John Doe</a></li>
      <li class="online"><a href="#"><i class="fa fa-circle-o"></i> Richard Avedon</a></li>
      <li class="busy"><a href="#"><i class="fa fa-circle-o"></i> Allen Collins</a></li>
    </ul>
    
    <p class="title">Friends</p>
    <ul class="nav nav-pills nav-stacked contacts">
      <li class="online"><a href="#"><i class="fa fa-circle-o"></i> Jaime Garzon</a></li>
      <li class="outside"><a href="#"><i class="fa fa-circle-o"></i> Dave Grohl</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Victor Jara</a></li>
    </ul>   
    
    <p class="title">Work</p>
    <ul class="nav nav-pills nav-stacked contacts">
      <li><a href="#"><i class="fa fa-circle-o"></i> Ansel Adams</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Gustavo Cerati</a></li>
    </ul>
    
  </div>
</nav>




    <style type="text/css">
    #color-switcher{
      position:fixed;
      background:#272930;
      padding:10px;
      top:50%;
      right:0;
      margin-right:-109px;
    }
    
    #color-switcher .toggle{
      cursor:pointer;
      font-size:15px;
      color: #FFF;
      display:block;
      position:absolute;
      padding:4px 10px;
      background:#272930;
      width:25px;
      height:30px;
      left:-24px;
      top:22px;
    }
    
    #color-switcher p{color: rgba(255, 255, 255, 0.6);font-size:12px;margin-bottom:3px;}
    #color-switcher .palette{padding:1px;}
    #color-switcher .color{width:15px;height:15px;display:inline-block;cursor:pointer;}
    #color-switcher .color.purple{background:#7761A7;}
    #color-switcher .color.green{background:#19B698;}
    #color-switcher .color.red{background:#EA6153;}
    #color-switcher .color.blue{background:#54ADE9;}
    #color-switcher .color.orange{background:#FB7849;}
    #color-switcher .color.prusia{background:#476077;}
    #color-switcher .color.yellow{background:#fec35d;}
    #color-switcher .color.pink{background:#ea6c9c;}
    #color-switcher .color.brown{background:#9d6835;}
    #color-switcher .color.gray{background:#afb5b8;}
 </style>
 <!--  <div id="color-switcher">
    <p>Select Color</p>
    <div class="palette">
      <div class="color purple" data-color="purple"></div>
      <div class="color green" data-color="green"></div>
      <div class="color red" data-color="red"></div>
      <div class="color blue" data-color="blue"></div>
      <div class="color orange" data-color="orange"></div>
    </div>
    <div class="palette">
      <div class="color prusia" data-color="prusia"></div>
      <div class="color yellow" data-color="yellow"></div>
      <div class="color pink" data-color="pink"></div>
      <div class="color brown" data-color="brown"></div>
      <div class="color gray" data-color="gray"></div>
    </div>
    <div class="toggle"><i class="fa fa-angle-left"></i></div>
  </div> -->

  <script type="text/javascript">
    var link = $('link[href="<?php echo base_url();?>assets/css/style.css"]');
    
    if($.cookie("css")) {
      link.attr("href",'css/skin-' + $.cookie("css") + '.css');
    }
    
    $(function(){
      $("#color-switcher .toggle").click(function(){
        var s = $(this).parent();
        if(s.hasClass("open")){
          s.animate({'margin-right':'-109px'},400).toggleClass("open");
        }else{
          s.animate({'margin-right':'0'},400).toggleClass("open");
        }
      });
      
      $("#color-switcher .color").click(function(){
        var color = $(this).data("color");
        $("body").fadeOut(function(){
          //link.attr('href','css/skin-' + color + '.css');
          $.cookie("css", color, {expires: 365, path: '/'});
          window.location.href = "";
          $(this).fadeIn("slow");
        });
      });
    });
  </script>   

 <!--dashboard-->
  </script>   

  <script src="<?php echo base_url();?>assets/js/dataTables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/js/dataTables/dataTables.bootstrap.js"></script>
<!-- 
      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
        -->
  <script src="<?php echo base_url();?>assets/js/jquery.codemirror/lib/codemirror.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.codemirror/mode/xml/xml.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.codemirror/mode/css/css.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.codemirror/mode/htmlmixed/htmlmixed.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.codemirror/addon/edit/matchbrackets.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.vectormaps/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.vectormaps/maps/jquery-jvectormap-world-mill-en.js"></script>

  <script src="<?php echo base_url();?>assets/js/behaviour/dashboard.js"></script>



   <!--dashboard-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!--
<script type="text/javascript" src="<?php echo base_url();?>assets/js/behaviour/voice-commands.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot/jquery.flot.labels.js"></script>-->
</body>
</html>
