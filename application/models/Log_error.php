<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log_error extends CI_Model{
    var $table = 'log_error';
    
    function get_data_error($awal, $akhir){
        $awal = !empty($awal) ? date('Y-m-d', strtotime($awal)) : date('Y-m-d');
        $akhir = !empty($akhir) ? date('Y-m-d', strtotime($akhir)) : date('Y-m-d');
        $list_data = $this->db->get_where($this->table, array("DATE_FORMAT(created, '%Y-%m-%d') >= " => $awal, "DATE_FORMAT(created, '%Y-%m-%d') <= " => $akhir))->result_array();
        return $list_data;
    }
    
    function save_error($log_error){
        return $this->db->insert($this->table, $log_error);
    }
}
?>
