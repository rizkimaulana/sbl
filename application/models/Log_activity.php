<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log_activity extends CI_Model{
    var $table = 'log_aktivitas';
    
    function get_data_activity($awal, $akhir){
        $awal = !empty($awal) ? date('Y-m-d', strtotime($awal)) : date('Y-m-d');
        $akhir = !empty($akhir) ? date('Y-m-d', strtotime($akhir)) : date('Y-m-d');
        $list_data = $this->db->get_where($this->table, array("DATE_FORMAT(created, '%Y-%m-%d') >= " => $awal, "DATE_FORMAT(created, '%Y-%m-%d') <= " => $akhir))->result_array();
        return $list_data;
    }
    
    function get_detail_activity($id){
        $data = $this->db->get_where($this->table, array('id'=>$id))->row_array();
        return $data;
    }
    
    function save_aktivitas($log_aktivitas){
        return $this->db->insert($this->table, $log_aktivitas);
    }
    
}
?>
