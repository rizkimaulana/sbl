<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Affiliate_model extends CI_Model{
    var $table = 'view_affiliate';
    var $column_order = array('
npwp','sponsor','id_user','id_affiliate','id_affiliate_type','wilayah','nama','nama_affiliate','username','password','tempat_lahir','tanggal_lahir','kelamin','ktp','provinsi_id','kabupaten_id','kecamatan_id','alamat','alamat_kantor','alamat_kirim','agama','telp','email','waris','hubwaris','bank','norek','namarek','pic1','tanggal_daftar','status','update_by','create_by','keterangan','jeniskelamin','provinsi','kabupaten','namabank','kecamatan','hubungan','affiliate_type','id_login','jabatan','pajak'); //set column field database for datatable orderable
    var $column_search = array('
npwp','sponsor','id_user','id_affiliate','id_affiliate_type','wilayah','nama','nama_affiliate','username','password','tempat_lahir','tanggal_lahir','kelamin','ktp','provinsi_id','kabupaten_id','kecamatan_id','alamat','alamat_kantor','alamat_kirim','agama','telp','email','waris','hubwaris','bank','norek','namarek','pic1','tanggal_daftar','status','update_by','create_by','keterangan','jeniskelamin','provinsi','kabupaten','namabank','kecamatan','hubungan','affiliate_type','id_login','jabatan','pajak'); //set column field database for datatable searchable 
    var $order = array('id_aff' => 'asc'); // default order 

   
    private $_table="affiliate";
    private $_primary="id_aff";


     public function generate_kode($idx){
        $today=date('md');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save($data){
        
        $get_initial = $this->db->select('affiliate_code')
                                ->where('id_affiliate_type',$data['affiliate_type'])->get('affiliate_type')->row();
        
        $initial = '';
        if($get_initial)
            $initial = $get_initial->affiliate_code;
        
            
        $arr = array(
            // 'id_user' => $data['id_user'],
            'id_affiliate_type' => $data['affiliate_type'],
            'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
            'nama_affiliate' => $data['nama_affiliate'],
            // 'username' => $data['username'],
            'password' => md5($data['password']),
            'telp' => $data['telp'],
            'email' => $data['email'],

            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'hubwaris' => $data['hubwaris'],
            'waris' => $data['waris'],

            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            // 'provinsi_id' => $data['provinsi_id'],

            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],

            'alamat' => $data['alamat'],
            'alamat_kantor' => $data['alamat_kantor'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
             'pic1' => isset($data['pic1']) ? $data['pic1']: '',
            'bank' => $data['select_bank'],
            'status' => $data['select_status'],
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user'),
            'jabatan_id'=>$data['jabatan_id'],
            'create_by'=>$this->session->userdata('id_user'),
            'npwp'=>$data['npwp'],
            'pajak' => '0.06'
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_aff =  $this->db->insert_id(); //get last insert ID
        
        //krena kode_client hrus digenerate, do it
        $this->db->update($this->_table,
                    array('id_user'=> $this->generate_kode($initial.$id_aff)),
                    // array('username'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));

                $this->db->update($this->_table,
                    array('username'=> $this->generate_kode($initial.$id_aff)),
                    // array('username'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));

        //  $arr = array(
        
        //     'id_user' => $id_aff,
        //     'nama' => $data['nama'],
        //     'username' => $data['username'],
        //     'password' => md5($data['password']),
        //     'jabatan_id' => 2,
        //     'status' => $data['select_status'],
        // );       
        
        //  $this->db->insert('user',$arr);
    

       

      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
       
    }

    function cek($id_affiliate){
        $query = $this->db->query("SELECT * FROM affiliate Where id_affiliate ='$id_affiliate' and status='1'");
        return $query;
    }
    

    public function update($data){
        
        $arr = array(
        
            // 'id_affiliate_type' => $data['affiliate_type'],
            // 'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
             'nama_affiliate' => $data['nama_affiliate'],
            // 'username' => $data['username'],
            // 'password' => $data['password'],
            'telp' => $data['telp'],
            'email' => $data['email'],

            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'hubwaris' => $data['hubwaris'],
            'waris' => $data['waris'],
            'kelamin' => $data['select_kelamin'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            // 'provinsi_id' => $data['provinsi_id'],

            // 'kabupaten_id' => $data['kabupaten'],
            // 'kecamatan_id' => $data['kecamatan'],
            // 'provinsi_id' => $data['provinsi'],

            'alamat' => $data['alamat'],
            'alamat_kantor' => $data['alamat_kantor'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
             // 'file_name' => $foto,
            'bank' => $data['select_bank'],
            'status' => $data['select_status'],
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            // 'jabatan_id'=>$this->session->userdata('jabatan_id'),
            'update_by'=>$this->session->userdata('id_user'),
            // 'create_by'=>$this->session->userdata('nama')
            'npwp'=>$data['npwp'],
            'pajak' => '0.06'
        );       
        
        // if($data['password']!='') || ($data['provinsi']!='') || ($data['kecamatan']!='') || ($data['kabupaten']!=''){
         if($data['password']!='') {
          
            $arr['password'] = md5($data['password']);
          
        }
         if($data['provinsi_id']!='' && $data['kecamatan_id']!='' && $data['kabupaten_id']!='') {
          
            // $arr['password'] = ($data['password']);
            $arr['provinsi_id'] = ($data['provinsi_id']);
            $arr['kecamatan_id'] = ($data['kecamatan_id']);
            $arr['kabupaten_id'] = ($data['kabupaten_id']);

        }

       if(isset($data['pic1'])){
            
            $arr['pic1'] = $data['pic1'];

        }
        // return $this->db->update('user',$arr,array('id_user'=>$data['id_user']));
         $this->db->update('affiliate',$arr,array('id_aff'=>$data['id_aff']));


        // $arr = array(
        
        //     'nama' => $data['nama'],
        //     'username' => $data['username'],
        //     // 'password' => sha1($data['password']),
        //     'jabatan_id' => 2,
        //     'status' => $data['select_status'],
        // );       
        
        // if($data['password']!=''){
            
        //     $arr['password'] = md5($data['password']);
        // }
       
        // $this->db->update('user',$arr,array('id'=>$data['id_login']));

         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }


      public function get_detail($id){
         $sql = "SELECT * from view_affiliate
                    WHERE id_aff={$id}
                ";
         // $sql = "SELECT a.*,b.keterangan, h.keterangan as jeniskelamin,c.nama as provinsi, d.nama as kabupaten, i.nama as namabank,e.nama as kecamatan, f.keterangan as hubungan, g.affiliate_type FROM affiliate a 
         //            LEFT JOIN wilayah_provinsi c ON c.provinsi_id = a.provinsi_id
         //            LEFT JOIN wilayah_kabupaten d ON d.kabupaten_id = a.kabupaten_id
         //            LEFT JOIN wilayah_kecamatan e ON e.kecamatan_id = a.kecamatan_id
         //            LEFT JOIN family_relation f ON f.id_relation = a.hubwaris
         //            LEFT JOIN affiliate_type g ON g.id_affiliate_type = a.id_affiliate_type
         //            LEFT JOIN status b ON b.kdstatus = a.status
         //            LEFT JOIN status h ON h.kdstatus = a.kelamin
         //            LEFT JOIN bank i ON i.id = a.bank
         //            WHERE a.id_aff={$id}
         //        ";
        return $this->db->query($sql)->row_array();
    }

    public function getEditaffiliate($id){
        $query = $this->db->query("
                    SELECT a.*,b.keterangan,c.nama as provinsi, d.nama as kabupaten, e.nama as kecamatan, f.keterangan as hubungan, g.affiliate_type FROM affiliate a 
                    LEFT JOIN wilayah_provinsi c ON c.provinsi_id = a.provinsi_id
                    LEFT JOIN wilayah_kabupaten d ON d.kabupaten_id = a.kabupaten_id
                    LEFT JOIN wilayah_kecamatan e ON e.kecamatan_id = a.kecamatan_id
                    LEFT JOIN family_relation f ON f.id_relation = a.hubwaris
                    LEFT JOIN affiliate_type g ON g.id_affiliate_type = a.id_affiliate_type
                    LEFT JOIN status b ON b.kdstatus = a.status
                    WHERE 1=1 and a.id_aff ='$id' LIMIT 1
                    ");
        return $query;
    }
    
    public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT * from data_affiliate
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND id_user LIKE '%{$q}%' 
            		OR nama LIKE '%{$q}%'
                    OR sponsor LIKE '%{$q}%'
                    OR telp LIKE '%{$q}%'
                    OR id_affiliate LIKE '%{$q}%'
            		OR nama LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_aff DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    function get_data_jabatan_id($affiliate_type='') {
      $this->db->where("id_affiliate_type",$affiliate_type);
      return $this->db->get("affiliate_type");
    }

     public function delete($id_aff){
    
        // $this->db->trans_begin(); //transaction initialize
        
        //     $this->db->delete($this->table,array('id_aff'=>$id_aff));
        //     // $this->db->delete('user',array('id_user'=>$id_user));
        
        // if($this->db->trans_status()===false){
            
        //     $this->db->trans_rollback();
        //     return false;    
            
        // }else{
            
        //     $this->db->trans_complete();
        //     return true;
        // }

          $get_img  = $this->db->select('pic1')
                                ->where('id_aff',$id_aff)->get('affiliate')->row_array();
        
        //remove all images
        if($get_img){
            $img = array('pic1');
            foreach($img as $im){
                
                if($get_img[$im])
                    unlink('./assets/images/affiliate/'.$get_img[$im]);
            }
        
        }
        
        return $this->db->delete('affiliate',array('id_aff'=>$id_aff));
  
    }
    
    public function delete_by_id($id_aff)
    {
        $this->db->where('id_aff', $id_aff);
        $this->db->delete($this->_table);
    }
    
      private function _get_datatables_query()
    {
         $status = array('1');

        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function update_pajak($data) {
        $arr = array(
            'pajak' => $data['pajak']
        );
        // if($data['password']!='') || ($data['provinsi']!='') || ($data['kecamatan']!='') || ($data['kabupaten']!=''){
        $this->db->update('affiliate', $arr, array('id_aff' => $data['id_aff']));
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_complete();
            return true;
        }
    }
}
