<div id="page-wrapper">
    <div class="row">
       	<!-- <div class="col-lg-7"> -->
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Edit Affiliate</h3>
                <form action="<?php echo base_url(); ?>affiliate/update_pajak" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <!-- <div class="col-lg-6"> -->
                    <div class="panel panel-default">
                        <div class="panel-body">  
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >ID Affiliate</label>
                                <div class="col-lg-5">
                                    <input name="id_user" value="<?php echo $id_user; ?>" class="form-control" readonly="readonly">
                                </div>
                            </div>
                            <input type="hidden" name="id_aff" value="<?php echo $id_aff; ?>">
                            <input type="hidden" name="id_login" value="<?php echo $id_login; ?>">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Jenis affiliate</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_affiliate_type" disabled>
                                        <?php foreach ($affiliate_type as $sj) {
                                            $selected = ($id_affiliate_type == $sj->id_affiliate_type) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $sj->id_affiliate_type; ?>" <?php echo $selected; ?>><?php echo $sj->affiliate_type; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >ID Affiliate</label>
                                <div class="col-lg-5">
                                    <input name="id_affiliate" class="form-control" value="<?php echo $id_affiliate; ?>" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Nama</label>
                                <div class="col-lg-5">
                                    <input name="nama" class="form-control" value="<?php echo $nama; ?>" readonly="readonly" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Nama Affiliate/(Cabang)</label>
                                <div class="col-lg-5">
                                    <input name="nama_affiliate" value="<?php echo $nama_affiliate; ?>" class="form-control" readonly="readonly" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">No Tlp</label>
                                <div class="col-lg-5">
                                    <input name="telp" class="form-control" value="<?php echo $telp; ?>" readonly="readonly">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">E-Mail</label>
                                <div class="col-lg-5">
                                    <input name="email" class="form-control" value="<?php echo $email; ?>" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">No Identitas</label>
                                <div class="col-lg-5">
                                    <input name="ktp" class="form-control" value="<?php echo $ktp; ?>" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tempat tanggal Lahir</label>
                                <div class="col-lg-5">
                                    <input name="tempat_lahir" class="form-control" value="<?php echo $tempat_lahir; ?>" readonly="readonly"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tanggal Lahir</label>
                                <div class="col-lg-5">
                                    <div class='input-group date' id='datepicker'>
                                        <input type='text' name="tanggal_lahir" id="datepicker" value="<?php echo $tanggal_lahir; ?>" class="form-control" readonly="readonly" >
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Alamat Rumah</label>
                                <div class="col-lg-5">
                                    <textarea  name="alamat" class="form-control" disabled="disabled" ><?php echo $alamat; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Alamat Kantor</label>
                                <div class="col-lg-5">
                                    <textarea  name="alamat_kantor" class="form-control" disabled="disabled" ><?php echo $alamat_kantor; ?></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Pajak (format 0.06 = 6%)</label>
                                <div class="col-lg-5">
                                    <input name="pajak" class="form-control" type="number" step="0.01" value="<?php echo $pajak; ?>"> 
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                        <!-- </div> -->
                    </div>
                    <!-- </div>  -->
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    <a href="<?php echo base_url(); ?>affiliate" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                </form>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
    $(function () {
        $('#datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#provinsi").change(function () {
            var url = "<?php echo site_url('affiliate/add_ajax_kab'); ?>/" + $(this).val();
            $('#kabupaten').load(url);
            return false;
        })

        $("#kabupaten").change(function () {
            var url = "<?php echo site_url('affiliate/add_ajax_kec'); ?>/" + $(this).val();
            $('#kecamatan').load(url);
            return false;
        })

        $("#kecamatan").change(function () {
            var url = "<?php echo site_url('affiliate/add_ajax_des'); ?>/" + $(this).val();
            $('#desa').load(url);
            return false;
        })
    });


</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $(".chosen-select").chosen({width: "95%"});
    });

//  	jQuery(document).ready(function(){
// 	jQuery(".chosen").data("placeholder","Select Frameworks...").chosen();
// });
</script>