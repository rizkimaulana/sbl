
 <div id="page-wrapper">
    
    
       <div class="row">
       	<!-- <div class="col-lg-7"> -->
       		<div class="panel panel-default">
       			<div class="panel-body">
			        <h3 class="page-header">Add Affiliate</h3>
			       
			         <?php if($this->session->flashdata('info')) { ?>
			                            <div class="alert alert-danger">  
			                                    <a class="close" data-dismiss="alert">x</a>  
			                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
			                            </div>
			                        <?php } ?>
       
        <form action="<?php echo base_url();?>affiliate/save" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
             <!-- <div class="col-lg-6"> -->
             <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <b>Data Koordinator</b>
                </div> -->
                <!-- /.panel-heading -->
                <div class="panel-body">  
                <!-- 	 <div class="form-group">
	                    
	                    <div class="col-lg-5">
	                      <input type="text" name="id_user" class="form-control" required>
	                    </div>
	                </div> -->
                		<div class="form-group">
		                    <label class="col-lg-2 control-label">Jenis affiliate</label>
		                    <div class="col-lg-5">
		                      <select required class="form-control"  name="affiliate_type" id="affiliate_type">
		                      	<option value="">- PILIH AFFILIATE -</option>
			                    <?php foreach($affiliate_type as $sj){?>
			                        <option value="<?php echo $sj->id_affiliate_type;?>"><?php echo $sj->affiliate_type;?></option>
			                    <?php } ?>
			                </select>

			                <input type ="hidden" name="jabatan_id" id="jabatan_id" class="form-control" >
		                    </div>
	                  </div>  
	                 
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label" >ID AFFILIATE</label>
	                    <div class="col-lg-5">
	                      <input type ="text" name="id_affiliate" id="id_affiliate" class="form-control" >
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-lg-2 control-label" >Nama</label>
	                    <div class="col-lg-5">
	                      <input name="nama" class="form-control" >
	                    </div>
	                </div>
	                 <div class="form-group">
	                    <label class="col-lg-2 control-label" >Nama Affiliate/(Cabang)</label>
	                    <div class="col-lg-5">
	                      <input name="nama_affiliate" class="form-control" >
	                    </div>
	                </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">No Tlp</label>
	                    <div class="col-lg-5">
	                      <input name="telp" class="form-control" >
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">E-Mail</label>
	                    <div class="col-lg-5">
	                      <input name="email" class="form-control" >
	                    </div>
	                  </div>
	                 <div class="form-group">
	                    <label class="col-lg-2 control-label">No Identitas</label>
	                    <div class="col-lg-5">
	                      <input name="ktp" class="form-control" >
	                    </div>
	                  </div>
	                   <div class="form-group">
	                   
	                   <label class="col-lg-2 control-label">Jenis Kelamin</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_kelamin">
		                	<option value=''>- PILIH JENIS KELAMIN -</option>
		                    <?php foreach($select_kelamin as $sk){?>
		                        <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
		                    <?php } ?>
		                </select>
		               
		            	</div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Hubungan Keluarga</label>
	                     <div class="col-lg-5">
		                      <select class="form-control" name="hubwaris">
		                      	<option>- PILIH HUBUNGAN KELUARGA -</option>
			                    <?php foreach($family_relation as $fr){?>
			                        <option value="<?php echo $fr->id_relation;?>"><?php echo $fr->keterangan;?></option>
			                    <?php } ?>
			                </select>
		                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Ahli waris</label>
	                    <div class="col-lg-5">
	                      <input name="waris" class="form-control" >
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Tempat tanggal Lahir</label>
	                    <div class="col-lg-5">
	                      <input name="tempat_lahir" class="form-control"> 
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">Tanggal Lahir</label>
	                    <div class="col-lg-5">
	                     <div class='input-group date' id='datepicker'>
	                          <input type='text' name="tanggal_lahir" id="datepicker" class="form-control" placeholder="dd/mm/yyyy "/>
	                          <span class="input-group-addon">
	                            <span class="glyphicon glyphicon-calendar"></span>
	                          </span>
                        </div>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Provinsi</label>
	                    <div class="col-lg-5">
	                      
                            <select name="provinsi_id" class="form-control chosen-select" id="provinsi" >
                                <option value=''>- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kabupaten</label>
	                    <div class="col-lg-5">
	                      
                            <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kecamatan</label>
	                    <div class="col-lg-5">
	                      <select name="kecamatan_id" class="form-control" id="kecamatan">
                                <option value=''>Select Kecamatan</option>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat Rumah</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="alamat" ></textarea>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat Kantor</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="alamat_kantor" ></textarea>
	                    </div>
	                  </div>
	                 <!--  <div class="form-group">
	                    <label class="col-lg-2 control-label">UserName</label>
	                    <div class="col-lg-5">
	                      <input name="username" class="form-control"  > 
	                    </div>
	                  </div> -->
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Password</label>
	                    <div class="col-lg-5">
	                      <input name="password" type="password" class="form-control"  > 
	                    </div>
	                  </div>
                 <!-- </div> -->
                 
               <!-- <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Bank</b>
                </div>
                <div class="panel-body">  -->
               		<div class="form-group">
	                    <label class="col-lg-2 control-label">No Rekening</label>
	                    <div class="col-lg-5">
	                      <input name="norek" class="form-control" > 
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Atas Nama Rekening</label>
	                    <div class="col-lg-5">
	                      <input name="namarek" class="form-control"  > 
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Bank</label>
	                    <div class="col-lg-5">
	                       <select class="form-control" name="select_bank">
	                       	<option>- Select Bank -</option>
		                    <?php foreach($select_bank as $sb){?>
		                        <option value="<?php echo $sb->id;?>"><?php echo $sb->nama;?></option>
		                    <?php } ?>
		                </select> 
	                    </div>
	                  </div>
	                      <div class="form-group">
					        <label class="col-lg-2 control-label">FOTO</label>
					        <div class="col-lg-5">
					            <input type="file" name="pic[]"  class="form-control">
					        </div>
					    </div>
					    <div class="form-group">
	                   
	                   <label class="col-lg-2 control-label">Status</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_status">
		                	<option>- PILIH STATUS -</option>
		                    <?php foreach($select_status as $st){?>
		                        <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
		                    <?php } ?>
		                </select>
		            	</div>
	                  </div>
	              </div>
              <!-- </div> -->

            <!-- </div> -->
               </div>
            <!-- </div>  -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>affiliate" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       <!-- </div> -->
    </div>
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
			$(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
		</script>
<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('affiliate/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('affiliate/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('affiliate/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });


</script>
     
 <script type="text/javascript">
 	jQuery(document).ready(function(){
	$(".chosen-select").chosen({width: "95%"}); 
});

//  	jQuery(document).ready(function(){
// 	jQuery(".chosen").data("placeholder","Select Frameworks...").chosen();
// });

  $("#affiliate_type").change(function(){
            var affiliate_type=$("#affiliate_type").val();
            console.log(affiliate_type);
            $.ajax({
                url:"<?php echo site_url('affiliate/get_jabatan_id');?>",
                type:"POST",
                data:"affiliate_type="+affiliate_type,
                cache:false,
                success:function(html){
                    $("#jabatan_id").val(html);
                    
                }
            })
        })
 </script>