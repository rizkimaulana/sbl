<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Affiliate extends CI_Controller{
	var $folder = "affiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('affiliate_model');
		$this->load->model('affiliate_model','r');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/affiliate');
		
	}
	

	private function _affiliate_type(){
		$id_affiliate_type = array('1', '2','3','5');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		return $this->db->get('affiliate_type')->result();
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	
		public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
	               	$row[] ='<td width="20%" >'.$r->id_user.'</strong></td>';
	               	$row[] ='<td width="20%" >'.$r->id_affiliate.' </strong> </td>';
	               	$row[] ='<td width="20%" >'.$r->nama.' </strong> </td>';
	               	$row[] ='<td width="20%" >'.$r->sponsor.' </strong> </td>';
	               	
	              
	               $row[] ='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'affiliate/edit/'.$r->id_aff.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> 

	                <a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'affiliate/detail/'.$r->id_aff.'">
	                            <i class="fa fa-pencil"></i> Detail
	                        </a> 
	                        <a title="Pajak" class="btn btn-sm btn-success" href="' . base_url() . 'affiliate/pajak/' . $r->id_aff . '">
	                            <i class="fa fa-pencil"></i> Pajak
	                        </a>
	                <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_affiliate('."'".$r->id_aff."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';

			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->affiliate_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	               
	               
	                $rows .='<td width="10%">'.$r->id_user.'</td>';
	                $rows .='<td width="20%">'.$r->sponsor.'</td>';
	                $rows .='<td width="20%">'.$r->nama.'</td>';
	                $rows .='<td width="20%">'.$r->id_affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->telp.'</td>';
	                $rows .='<td width="10%">'.$r->email.'</td>';
	                $rows .='<td width="10%">'.$r->provinsi.'</td>';
	                $rows .='<td width="10%">'.$r->keterangan.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'affiliate/edit/'.$r->id_aff.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';

	                $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'affiliate/detail/'.$r->id_aff.'">
	                            <i class="fa fa-pencil"></i> Detail
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_affiliate('."'".$r->id_aff."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href=""'.base_url().'affiliate/ajax_delete/'.$r->id_aff.'"" title="Hapus" ><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	                              
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'affiliate/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	


	
	

	public function add(){
	     
	     if(!$this->general->privilege_check(AFFILIATE,'add'))
		    $this->general->no_access();
		
	    $data = array('affiliate_type'=>$this->_affiliate_type(),
	    			'family_relation'=>$this->_family_relation(),
	    			'provinsi'=>$this->affiliate_model->get_all_provinsi(),
	    			'kabupaten'=>$this->affiliate_model->get_all_kabupaten(),
	    			'select_status'=>$this->_select_status(),
	    			'select_bank'=>$this->_select_bank(),
	    			'select_kelamin'=>$this->_select_kelamin()
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	

	public function detail(){
	 
	    if(!$this->general->privilege_check(AFFILIATE,'view'))
		    $this->general->no_access();

	     $id = $this->uri->segment(3);
	   	 $detail = $this->affiliate_model->get_detail($id);
	    if(!$detail)
	        show_404();
	        
	    $data = array(
	    			 'detail'=>$detail
	    	);
        $this->template->load('template/template', $this->folder.'/detail',($data));
	}

	public function edit(){
	     if(!$this->general->privilege_check(AFFILIATE,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('view_affiliate',array('id_aff'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	    $data = array('affiliate_type'=>$this->_affiliate_type(),
	    			 'family_relation'=>$this->_family_relation(),
	    			 'provinsi'=>$this->affiliate_model->get_all_provinsi(),
	    			 'kabupaten'=>$this->affiliate_model->get_all_kabupaten(),
	    			 'select_status'=>$this->_select_status(),
	    			 'select_kelamin'=>$this->_select_kelamin(),
	    			 'select_bank'=>$this->_select_bank()
	    	);
	  	// 		$this->data['family_relation']=$this->_family_relation();
				// $this->data['select_kelamin']=$this->_select_kelamin();
		  //   	$this->data['provinsi']=$this->affiliate_model->get_all_provinsi();
		  //   	$this->data['kabupaten']=$this->affiliate_model->get_all_kabupaten();
		  //   	$this->data['affiliate_type']=$this->_affiliate_type();
		  //   	$this->data['select_status']=$this->_select_status();
		  //   	$this->data['select_bank']=$this->_select_bank();
        		$this->template->load('template/template', $this->folder.'/edit',array_merge($get,$data));
	}


	public function save(){
	 	$this->form_validation->set_rules('nama','nama','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            $id_affiliate=$this->input->post('id_affiliate'); // mendapatkan input dari kode
            $cek=$this->affiliate_model->cek($id_affiliate); // cek kode di database
            if($cek->num_rows()>0){ // jika kode sudah ada, maka tampilkan pesan
                 $this->session->set_flashdata('info', "ID AFFILIATE sudah digunakan");
               
                 redirect('affiliate/add');
            }else{

		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/affiliate/',
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		   		}
	    	}
	     }
	  //print_r($data);exit;
	    $send = $this->affiliate_model->save($data);
                $this->session->set_flashdata('info', "User berhasil ditambahkan.");
	 	       redirect('affiliate');
		}
	}

	public function update(){
	     

		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/affiliate/',
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }
		    else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		    }
	    }
	     
		  //print_r($data);exit;
		    $send = $this->affiliate_model->update($data);
	                $this->session->set_flashdata('info', "User berhasil diubah.");
		 	       redirect('affiliate');
		
	}
	

public function unlink(){
	
	    $data = $this->input->post(null,true);
	    
	    if(unlink('./assets/images/affiliate/'.$data['img'])){
	        
	        $column = substr($data['img'],0,4); //pic1,pic2 etc...
	        $this->db->update('affiliate',array($column=>''),array('id_aff'=>$data['id_aff']));
	        
	       echo json_encode(array('status'=>true)); 
	    }
	    
	}

	public function delete(){
	    
	     if(!$this->general->privilege_check(AFFILIATE,'remove'))
		    $this->general->no_access();
		    
	     $id  = $this->uri->segment(4);
	     $send = $this->affiliate_model->delete($id);
	     if($send)
	        redirect('affiliate');
	}
	
	public function ajax_delete($id_aff)
	{
		if(!$this->general->privilege_check(AFFILIATE,'remove'))
		    $this->general->no_access();
		$send = $this->affiliate_model->delete($id_aff);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('affiliate');

		// $id = $this->uri->segment(4);
		// $del = $this->affiliate_model->delete($id);
		// echo json_encode(array("status" => TRUE));
		// if($del)
		//     redirect('affiliate');
	}

public function _resize_affiliate($fulpat) {
        $config['source_image'] = './assets/images/affiliate/' . $fulpat;
        $config['new_image'] = './assets/images/affiliate/' . $fulpat;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 900;
        $config['height'] = 600;
        $this->image_lib->initialize($config);

        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }
	
	public function _create_thumb_affiliate($fulpet) {
        $config['source_image'] = './assets/images/affiliate/' . $fulpet;
        $config['new_image'] = './upload/produk/thumb/' . $fulpet;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 200;
        //$config['height'] = 200;
        $this->image_lib->initialize($config);

        if (!$this->image_lib->resize()) {
            echo "tum" . $this->image_lib->display_errors();
        }
    }

    function get_jabatan_id(){
		$affiliate_type = $this->input->post('affiliate_type');
		$affiliate_type = $this->affiliate_model->get_data_jabatan_id($affiliate_type);
		 if($affiliate_type->num_rows()>0){
            $affiliate_type=$affiliate_type->row_array();
            echo $affiliate_type['jabatan_id'];
           
          
        }
        
	}

	public function pajak() {
        if (!$this->general->privilege_check(AFFILIATE, 'edit'))
            $this->general->no_access();

        $id = $this->uri->segment(3);
        $get = $this->db->get_where('view_affiliate', array('id_aff' => $id))->row_array();
        if (!$get)
            show_404();

        $data = array('affiliate_type' => $this->_affiliate_type(),
            'family_relation' => $this->_family_relation(),
            'provinsi' => $this->affiliate_model->get_all_provinsi(),
            'kabupaten' => $this->affiliate_model->get_all_kabupaten(),
            'select_status' => $this->_select_status(),
            'select_kelamin' => $this->_select_kelamin(),
            'select_bank' => $this->_select_bank()
        );
        // 		$this->data['family_relation']=$this->_family_relation();
        // $this->data['select_kelamin']=$this->_select_kelamin();
        //   	$this->data['provinsi']=$this->affiliate_model->get_all_provinsi();
        //   	$this->data['kabupaten']=$this->affiliate_model->get_all_kabupaten();
        //   	$this->data['affiliate_type']=$this->_affiliate_type();
        //   	$this->data['select_status']=$this->_select_status();
        //   	$this->data['select_bank']=$this->_select_bank();

        $this->template->load('template/template', $this->folder . '/pajak', array_merge($get, $data));
    }

    public function update_pajak() {
        $data = $this->input->post(null, true);
        
        $send = $this->affiliate_model->update_pajak($data);
        $this->session->set_flashdata('info', "User berhasil diubah.");
        redirect('affiliate');
    }
}
