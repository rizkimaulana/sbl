<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Claimfeefreehemat_modal extends CI_Model{
   
   
    private $_table="claim_feefree";
    private $_primary="id_claim_feefree";


    
     public function save_feehemat($data,$data2=''){
        

        $arr = array(
            // 'id_user' => $data['id_user'],
            
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 0,
            'status_feefree' => 2,
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_claim_feefree =  $this->db->insert_id(); //get last insert ID


      $sql  = "INSERT INTO historis_fee
               SELECT NULL,id_fee,id_claim_fee,NULL,id_registrasi,id_booking,invoice,id_affiliate,id_schedule,id_product,fee,keterangan,claim_date,create_date,update_date,create_by,update_by,payment_date,status_fee,status_free
               FROM fee
               WHERE id_registrasi IN ($data2)";
      $insert_data = $this->db->query($sql);


     if($insert_data){
        $sql = "UPDATE FEE SET status_fee = '2',claim_date='".date('Y-m-d H:i:s')."',id_claim_fee='".$id_claim_feefree."' WHERE id_registrasi IN ($data2)";
        $update_data = $this->db->query($sql);
         $update_data;
      }
       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

    
       public function save_freehemat($data,$data2=''){
        

        $arr = array(
            // 'id_user' => $data['id_user'],
            
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 0,
            'status_feefree' => 4,
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_claim_feefree =  $this->db->insert_id(); //get last insert ID


      $sql  = "INSERT INTO historis_fee
               SELECT NULL, id_fee,id_claim,id_registrasi,id_booking,invoice,id_affiliate,id_schedule,id_product,fee,keterangan,claim_date,create_date,update_date,create_by,update_by,payment_date,status,status_fee,status_free
               -- SELECT NULL,id_fee,NULL,id_claim_free,id_registrasi,id_booking,invoice,id_affiliate,id_schedule,id_product,fee,keterangan,claim_date,create_date,update_date,create_by,update_by,payment_date,status_fee,status_free
               FROM fee
               WHERE id_registrasi IN ($data2)";
      $insert_data = $this->db->query($sql);


     if($insert_data){
        $sql = "UPDATE FEE SET status_free = '2',claim_date='".date('Y-m-d H:i:s')."',id_claim='".$id_claim_feefree."' WHERE id_registrasi IN ($data2)";
        $update_data = $this->db->query($sql);
         $update_data;
      }
       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }



    public function get_pic($id_affiliate,$id_product=''){
        $id_user=  $this->session->userdata('id_user');
        $sql ="SELECT * from view_detail_feefreehemat WHERE  id_affiliate  = ? and id_product = ? and id_affiliate='$id_user' and status_fee='1'
              ";
        return $this->db->query($sql,array($id_affiliate,$id_product))->result_array();    

    }

      public function get_pic_free($id_affiliate,$id_product=''){
        $id_user=  $this->session->userdata('id_user');
        $sql ="SELECT * from view_detail_feefreehemat WHERE  id_affiliate  = ? and id_product = ? and id_affiliate='$id_user' and status_free='1' limit 20
              ";
        return $this->db->query($sql,array($id_affiliate,$id_product))->result_array();    

    }
   

    public function get_pic_data($id_affiliate,$id_product){
 
         $id_user=  $this->session->userdata('id_user');
         $sql = "SELECT * from view_feefreehemat where id_affiliate = '$id_affiliate' and id_product={$id_product} and id_affiliate='$id_user'";
        return $this->db->query($sql)->row_array();  
    }
    
    public function get_data($offset,$limit,$q=''){
    
      $id_user = $this->session->userdata('id_user');
          $sql = " SELECT * from view_feefreehemat where id_affiliate = '".$id_user."' ";
        
        if($q){
            
            $sql .=" AND id_affiliate LIKE '%{$q}%' 
            		OR nama_affiliate LIKE '%{$q}%'
            		OR product LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_affiliate DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }




   
    
    
}
