<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Claimfeeinput_model extends CI_Model{
   
   
    private $_table="claim_fee_input";
    private $_primary="id_claim_feeinput";


    
     public function save_fee_input($data,$data2=''){
        

        $arr = array(
            // 'id_user' => $data['id_user'],
            
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 0,
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_claim_feeinput =  $this->db->insert_id(); //get last insert ID


      $sql  = "INSERT INTO historis_fee_input
               SELECT NULL,id_claim_feeinput,id_registrasi,id_booking,id_product,invoice,id_schedule,id_affiliate,fee_input,keterangan,claim_date,create_date,update_date,create_by,update_by,payment_date,status 
               FROM fee_input
               WHERE id_registrasi IN ($data2)";
      $insert_data = $this->db->query($sql);


     if($insert_data){
        $sql = "UPDATE FEE_INPUT SET STATUS = '2',claim_date='".date('Y-m-d H:i:s')."',id_claim_feeinput='".$id_claim_feeinput."' WHERE id_registrasi IN ($data2)";
        $update_data = $this->db->query($sql);
         $update_data;
      }
       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

    
  



    public function get_pic($id_affiliate,$id_product=''){
        $id_user=  $this->session->userdata('id_user');
        $sql ="SELECT * from view_detail_fee_input WHERE  id_affiliate  = ? and id_product = ? and id_affiliate='$id_user'
              ";
        return $this->db->query($sql,array($id_affiliate,$id_product))->result_array();    

    }

   

    public function get_pic_data($id_affiliate,$id_product){
 
         $id_user=  $this->session->userdata('id_user');
         $sql = "SELECT * from view_fee_input where id_affiliate = '$id_affiliate' and id_product={$id_product} and id_affiliate='$id_user'";
        return $this->db->query($sql)->row_array();  
    }
    
    public function get_data($offset,$limit,$q=''){
    
      $id_user = $this->session->userdata('id_user');
          $sql = " SELECT * from view_fee_input where id_affiliate = '".$id_user."' ";
        
        if($q){
            
            $sql .=" AND id_affiliate LIKE '%{$q}%' 
            		OR nama_affiliate LIKE '%{$q}%'
            		OR product LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_affiliate DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }



 function searchAdvance($from , $to, $id_product, $id_affiliate){
       
       $form = trim($from);
       $to = trim($to);
       $id_product = trim($id_product);
      $id_affiliate = trim($id_affiliate);
      $condition = "create_date  between '$from' And '$to' And id_product='$id_product' And id_affiliate ='$id_affiliate' ";

       $sql = "SELECT * from view_detail_fee WHERE   $condition ";        
       $query = $this->db->query($sql);
       if(empty($query->result())){
           echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! invalid date option</h2></td></tr>';
           exit;
       }else{ 
       // if(!empty($form) AND empty($to) AND empty($id_product)){
       // $condition = "='$form'";
       // }
       
      
       
       $sno = 1;
       foreach ($query->result() as $row)
       {    
           // $created = strtotime($row->created);
           echo'<tr><td> <input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="'.$row->id_registrasi.'"</td>
           <td>'.$sno.'</td>
                  <td>'.$row->invoice.'</td>
                  <td>'.$row->id_jamaah.'</td>
                  <td>'.$row->nama.'</td>
                  <td>'.$row->product.'</td>
                  <td>'.$row->fee.'</td>
                  <td>'.$row->keterangan.'</td>
                </td></tr>';
           $sno = $sno+1; 
       }
     }
   }


    function searchAdvanceTunai($from , $to, $id_product, $id_affiliate){
       
       $form = trim($from);
       $to = trim($to);
       $id_product = trim($id_product);
      $id_affiliate = trim($id_affiliate);
      $condition = "create_date between '$from' And '$to' And id_product='$id_product' And id_affiliate ='$id_affiliate' ";

       $sql = "SELECT * from view_detail_fee WHERE   $condition ";        
       $query = $this->db->query($sql);
       if(empty($query->result())){
           echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! invalid date option</h2></td></tr>';
           exit;
       }else{ 
       // if(!empty($form) AND empty($to) AND empty($id_product)){
       // $condition = "='$form'";
       // }
       
      
       
       $sno = 1;
       foreach ($query->result() as $row)
       {    
           // $created = strtotime($row->created);
            echo'<tr><td> <input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="<?php '.$row->id_registrasi.' ?>"</td>
           <td>'.$sno.'</td>
                  <td>'.$row->invoice.'</td>
                  <td>'.$row->id_jamaah.'</td>
                  <td>'.$row->nama.'</td>
                  <td>'.$row->product.'</td>
                  <td>'.$row->fee.'</td>
                  <td>'.$row->keterangan.'</td>
                </td></tr>';
           $sno = $sno+1; 
       }
     }
   }


     public function delete($id_product){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_product'=>$id_product));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
   
    
    
}
