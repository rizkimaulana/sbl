<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claim_feefreehemat extends CI_Controller{
	var $folder = "claim";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check_affiliate(FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_free_hemat_registrasi_affiliate');	
		$this->load->model('claimfeefreehemat_modal');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/feefree_hemat');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->claimfeefreehemat_modal->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';                
	                $rows .='<td width="10%">'.$r->id_affiliate.'</td>';
	                $rows .='<td width="15%">'.$r->nama_affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->product.'</td>';
	                $rows .='<td width="9%">'.$r->jumlah_jamaah.'</td>';
	                
	             	$rows .='<td width="9%">'.$r->FEE.'</td>';
	             	$rows .='<td width="9%">'.$r->free_hemat.'</td>';
	             	// $rows .='<td width="9%">'.$r->claim_fee.'</td>';
	             	// $rows .='<td width="9%">'.$r->free_hemat_claim.'</td>';
	             	$rows .='<td width="9%">'.$r->PERIODE.'</td>';
	                $rows .='<td width="30%" align="center">';
	                if ($r->status_fee == 2){
	                	 $rows .='<a title="Menuggu konfirmasi" class="btn btn-sm btn-info" href="#" >
	                            <i class="glyphicon glyphicon-upload" ></i> Menuggu konfirmasi
	                        </a> ';
	                }else if($r->status_fee == 3){
	                	$rows .='<a title="Fee Paket Hemat sudah diclaim" class="btn btn-sm btn-success" href="#" >
	                            <i class="glyphicon glyphicon-ok" ></i> Fee sudah diclaim
	                        </a> ';
	                }else{
	                $rows .='<a title="Clime Fee Paket Hemat" class="btn btn-sm btn-primary" href="'.base_url().'claim/claim_feefreehemat/detail/'.$r->id_affiliate.'/'.$r->id_product.'" >
	                            <i class="fa fa-pencil" ></i> Claim Fee Paket Hemat
	                        </a> ';

	            	}

	            	$rows .='</br>';
	            	$rows .='</br>';
	            	 // if ($r->free_hemat <= 0){

	            	 // }else{ 
	            	 if ($r->status_free == 2){
	                	 $rows .='<a title="Menuggu konfirmasi" class="btn btn-sm btn-info" href="#" >
	                            <i class="glyphicon glyphicon-upload" ></i> Menuggu konfirmasi
	                        </a> ';
	                }else if($r->status_free == 3){
	                	$rows .='<a title="20 Gratis Paket Hemat Disetujui" class="btn btn-sm btn-success" href="#" >
	                            <i class="glyphicon glyphicon-ok" ></i> Free Paket Hemat Disetujui
	                        </a> ';
	                }else{
	                $rows .='<a title="Clime Free Hemat" class="btn btn-sm btn-primary" href="'.base_url().'claim/claim_feefreehemat/detail_free_hemat/'.$r->id_affiliate.'/'.$r->id_product.'" >
	                            <i class="fa fa-pencil" ></i> Claim Free Hemat
	                        </a> ';
	            	}
	            // }
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'claim/claim_feefreehemat/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	


	public function detail(){
	


	    if(!$this->general->privilege_check_affiliate(FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
	    
	    $id_affiliate = $this->uri->segment(4);
	    $id_product = $this->uri->segment(5);
	    $affiliate = $this->claimfeefreehemat_modal->get_pic_data($id_affiliate,$id_product);
	    // $product = $this->fee_model->get_pic($id_product);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$affiliate ){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->claimfeefreehemat_modal->get_pic($id_affiliate,$id_product);
	       
	    }    

	    $data = array(
	    		
	       		 'affiliate'=>$affiliate,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_feehemat',($data));

	}

public function detail_free_hemat(){
	


	    if(!$this->general->privilege_check_affiliate(FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
	    
	    $id_affiliate = $this->uri->segment(4);
	    $id_product = $this->uri->segment(5);
	    $affiliate = $this->claimfeefreehemat_modal->get_pic_data($id_affiliate,$id_product);
	    // $product = $this->fee_model->get_pic($id_product);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$affiliate ){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->claimfeefreehemat_modal->get_pic_free($id_affiliate,$id_product);
	       
	    }    

	    $data = array(
	    		
	       		 'affiliate'=>$affiliate,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_freehemat',($data));

	}




	function save_feehemat(){
		 $data = $this->input->post(null,true);
	    // $data2 = $this->input->post('checkbox');
		 $data2 = $this->input->post('text');
	    $inputan = '';
	    foreach($data2 as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;

	    }
		 // print_r($data);
	    $simpan = $this->claimfeefreehemat_modal->save_feehemat($data,$inputan);
	    if($simpan)
	        redirect('claim/claim_feefreehemat');
	}

function save_freehemat(){
		 $data = $this->input->post(null,true);
	    // $data2 = $this->input->post('checkbox');
		 $data2 = $this->input->post('text');
	    $inputan = '';
	    foreach($data2 as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;

	    }
		 // print_r($data);
	    $simpan = $this->claimfeefreehemat_modal->save_freehemat($data,$inputan);
	    if($simpan)
	        redirect('claim/claim_feefreehemat');
	}
	
}
