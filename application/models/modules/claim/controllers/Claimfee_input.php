<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claimfee_input extends CI_Controller{
	var $folder = "claim";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check_affiliate(CLAIM_FEE_INPUT,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','clime_fee_input');	
		$this->load->model('claimfeeinput_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/claimfee_input');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->claimfeeinput_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';                
	                $rows .='<td width="10%">'.$r->id_affiliate.'</td>';
	                $rows .='<td width="30%">'.$r->nama_affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->product.'</td>';
	                $rows .='<td width="9%">'.$r->jumlah_jamaah.'</td>';
	                $rows .='<td width="9%">'.$r->total_fee_input.'</td>';
	             	
	                $rows .='<td width="30%" align="center">';
	                if ($r->status == 2){
	                	 $rows .='<a title="Menuggu konfirmasi" class="btn btn-sm btn-info" href="#" >
	                            <i class="glyphicon glyphicon-upload" ></i> Menuggu konfirmasi
	                        </a> ';
	                }else if($r->status == 3){
	                	$rows .='<a title="Fee Input Disetujui" class="btn btn-sm btn-success" href="#" >
	                            <i class="glyphicon glyphicon-ok" ></i> Fee Input Disetujui
	                        </a> ';
	                }else{
	                $rows .='<a title="Clime Fee" class="btn btn-sm btn-primary" href="'.base_url().'claim/claimfee_input/detail/'.$r->id_affiliate.'/'.$r->id_product.'" >
	                            <i class="fa fa-pencil" ></i> Clime Fee
	                        </a> ';
	            	}
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'claim/claimfee_input/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	


	public function detail(){
	


	    if(!$this->general->privilege_check_affiliate(CLAIM_FEE_INPUT,'view'))
		    $this->general->no_access();
	    
	    $id_affiliate = $this->uri->segment(4);
	    $id_product = $this->uri->segment(5);
	    $affiliate = $this->claimfeeinput_model->get_pic_data($id_affiliate,$id_product);
	    // $product = $this->fee_model->get_pic($id_product);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$affiliate ){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->claimfeeinput_model->get_pic($id_affiliate,$id_product);
	       
	    }    

	    $data = array(
	    		
	       		 'affiliate'=>$affiliate,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_feeinput',($data));

	}


function tes(){
		 $this->template->load('template/template', $this->folder.'/tes');
	}



	function save_fee_input(){
		 $data = $this->input->post(null,true);
	    // $data2 = $this->input->post('checkbox');
		 $data2 = $this->input->post('text');
	    $inputan = '';
	    foreach($data2 as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;

	    }
		 // print_r($data);
	    $simpan = $this->claimfeeinput_model->save_fee_input($data,$inputan);
	    if($simpan)
	        redirect('claim/claimfee_input');
	}


	
}
