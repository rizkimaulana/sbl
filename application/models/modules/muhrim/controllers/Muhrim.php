<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Muhrim extends CI_Controller{
	var $folder = "muhrim";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(MUHRIM,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('muhrim_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/muhrim');
		
	}
	
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	}
		private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_hubungan_muhrim(){
		
		return $this->db->get('hubungan_muhrim')->result();
	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->muhrim_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="30%">'.$r->jenis_kelamin.'</td>';
	                $rows .='<td width="8%">'.$r->statuskawin.'</td>';
	                $rows .='<td width="8%">'.$r->rentang_umur.'</td>';
	                $rows .='<td width="10%">'.$r->hubungan.'</td>';
	                $rows .='<td width="10%">'.$r->biaya_muhrim.'</td>';
	                $rows .='<td width="9%">'.$r->create_by.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'muhrim/edit/'.$r->id_muhrim.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_muhrim('."'".$r->id_muhrim."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'product_price/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->muhrim_model->save($data);
	     if($send)
	        redirect('muhrim');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(MUHRIM,'add'))
		    $this->general->no_access();

	    
	    $data = array(
	    	
	    	'select_hubungan_muhrim'=>$this->_select_hubungan_muhrim(),
			'select_rentangumur'=>$this->_select_rentangumur(),
			'select_kelamin'=>$this->_select_kelamin(),
			'select_statuskawin'=>$this->_select_statuskawin()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(MUHRIM,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('muhrim',array('id_muhrim'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   $data = array(
	   		'select_hubungan_muhrim'=>$this->_select_hubungan_muhrim(),
			'select_rentangumur'=>$this->_select_rentangumur(),
			'select_kelamin'=>$this->_select_kelamin(),
			'select_statuskawin'=>$this->_select_statuskawin()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->muhrim_model->update($data);
	     if($send)
	        redirect('muhrim');
		
	}
	
	public function delete(){
	    
	     if(!$this->general->privilege_check(MUHRIM,'remove'))
		    $this->general->no_access();
		    
	     $id  = $this->uri->segment(4);
	     $send = $this->muhrim_model->delete($id);
	     if($send)
	        redirect('muhrim');
	}
	
	public function ajax_delete($id_muhrim)
	{
		if(!$this->general->privilege_check(MUHRIM,'remove'))
		    $this->general->no_access();
		$send = $this->muhrim_model->delete_by_id($id_muhrim);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('muhrim');
	}


}
