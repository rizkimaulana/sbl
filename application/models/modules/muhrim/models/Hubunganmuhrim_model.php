<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hubunganmuhrim_model extends CI_Model{
   
   
    private $_table="hubungan_muhrim";
    private $_primary="id_hub_muhrim";

    public function save($data){
    
        // $cek = $this->db->select('ket_hubungan')->where('ket_hubungan',$data['ket_hubungan'])->get('hubungan_muhrim')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'ket_hubungan' => $data['ket_hubungan'],
        );       
        
         return $this->db->insert('hubungan_muhrim',$arr);
    }


    public function update($data){
        
        $arr = array(
        
           'ket_hubungan' =>$data['ket_hubungan'],
            // 'nama' =>$data['nama'],
            // 'status' =>$data['status'],
        );       
              
        return $this->db->update($this->_table,$arr,array('id_hub_muhrim'=>$data['id_hub_muhrim']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT * FROM hubungan_muhrim  
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND ket_hubungan LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_hub_muhrim DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_hub_muhrim)
    {
        $this->db->where('id_hub_muhrim', $id_hub_muhrim);
        $this->db->delete($this->_table);
    }
    
    
}
