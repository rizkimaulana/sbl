 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Referensi Muhrim</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>muhrim/hubungan_muhrim/save">
            
            <div class="form-group">
                <label>Nama Hubungan Keluarga </label>
                <input class="form-control" name="ket_hubungan" required>
            </div>
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>muhrim/hubungan_muhrim" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
