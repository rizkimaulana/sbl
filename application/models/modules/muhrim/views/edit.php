 <div id="page-wrapper">
    
    
       <div class="row">
       	<!-- <div class="col-lg-7"> -->
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Edit Affiliate</h3>
       
        <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>muhrim/update">
             <!-- <div class="col-lg-6"> -->
             <div class="panel panel-default">
               
                <div class="panel-body">  
             
                	<input type="hidden" name="id_muhrim" value="<?php echo $id_muhrim;?>">
                	
                	    <div class="form-group">
			             <label class=" col-lg-2 control-label" >Jenis Kelamin :</label>
			             <div class="col-lg-5">
			            <select class="form-control" name="select_kelamin">
		                <?php foreach($select_kelamin as $sp){ 
                        	$selected = ($id_jenis_kelamin == $sp->kdstatus)  ? 'selected' :'';
                    	?>
                        	<option value="<?php echo $sp->kdstatus;?>" <?php echo $selected;?>><?php echo $sp->keterangan;?> </option>
                    	<?php } ?>
				            </select>
				          </div>
			          </div>

	                   <div class="form-group">
			                 <label class="col-lg-2 control-label" >Status :</label>
			                 <div class="col-lg-5">
			                <select class="form-control" name="select_statuskawin">
			                 
			                     <?php foreach($select_statuskawin as $sp){ 
		                        	$selected = ($id_status == $sp->kdstatus)  ? 'selected' :'';
		                    	?>
		                        	<option value="<?php echo $sp->kdstatus;?>" <?php echo $selected;?>><?php echo $sp->keterangan;?> </option>
		                    	<?php } ?>
			                </select>
			              </div>
			            </div>

			            <div class="form-group">
			                 <label class="col-lg-2 control-label" >Rentang Umur :</label>
			                 <div class="col-lg-5">
			                <select class=" form-control" name="rentang_umur">
			                 

			                      <?php foreach($select_rentangumur as $sp){ 
		                        	$selected = ($id_rentang_umur == $sp->id_rentang_umur)  ? 'selected' :'';
		                    	?>
		                        	<option value="<?php echo $sp->id_rentang_umur;?>" <?php echo $selected;?>><?php echo $sp->keterangan;?> </option>
		                    	<?php } ?>
			                </select>
			              </div>
					      </div>

				            <div class="form-group">
				              <label class="col-lg-2 control-label" >Hubungan Keluarga</label>
				               <div class="col-lg-5">
				                <select class="form-control" name="select_hubungan_muhrim">
				                
				                   <?php foreach($select_hubungan_muhrim as $sp){ 
		                        	$selected = ($id_hubungan == $sp->id_hub_muhrim)  ? 'selected' :'';
		                    		?>
		                        	<option value="<?php echo $sp->id_hub_muhrim;?>" <?php echo $selected;?>><?php echo $sp->ket_hubungan;?> </option>
		                    	<?php } ?>
				              </select>
				              </div>
				              </div>


	                <div class="form-group">
	                    <label class="col-lg-2 control-label" >Biaya Muhrim</label>
	                    <div class="col-lg-5">
	                      <input name="biaya_muhrim" class="form-control" value="<?php echo $biaya_muhrim;?>" required>
	                    </div>
	                </div>

	               
	                 
	                
	             
                 </div>
              
  

            <!-- </div> -->
               </div>
            <!-- </div>  -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>muhrim" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       <!-- </div> -->
    </div>
</div>
<!-- /#page-wrapper -->

