<div class="page-head">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">DATA CLUSTER</li>
    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>CLUSTER</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <span class="input-group-btn">
                        <a href="<?php echo base_url();?>cluster/add_cluster" class="btn btn-success">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </span>    
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover " >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User ID</th>
                                    <th>Nama</th>
                                    <th>Provinsi</th>
                                    <th>Kabupaten</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script>
    var table;
    $(document).ready(function () {
        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('cluster/cluster/ajax_list') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });
    function delete_cluster(id_cluster)
    {
        if(confirm('Are you sure delete this data '))
        {
            // ajax delete data to database
            alert(id_cluster);
            $.ajax({
                url : "<?php echo site_url('cluster/ajax_delete')?>/"+id_cluster,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    // alert('Error deleting data');
                    window.location.href="<?php echo site_url('cluster')?>";
                }
            });

    }
}
</script>







