		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 7pt; text-align: center;}
		</style>';
		<table border='2' >
			<tr>
				<td colspan="2" class="h_kanan"><strong>PT SOLUSI BALAD LUMAMPAH</strong></td>
			</tr>
			<tr>
				<td width="20%"><strong>STRUK</strong>
					<hr width="500%">
				</td>
				<td class="h_kanan" width="80%">WISMA BUMI PUTRA- 5&7th Floor Jl. Asia Afrika No 141-149 Bandung</td>
			</tr>

		</table>
		<table border='2'width="100%">
		 <?php 
              $biaya_setting_ = $this->kuotacabang_model->get_key_val();
                foreach ($biaya_setting_ as $key => $value){
                  $out[$key] = $value;
                }
          ?>
			<tr>
				<td width="25%"> No INVOICE </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri"><strong>#<?php echo $report_kuota['invoice']?></strong></td>

				<td colspan="5" class="h_kiri">BANK Transfer A/N SBL</td>
			</tr>
		
			<tr>
				<td width="25%"> Tanggal Transaksi </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri"><strong><?php echo $report_kuota['tgl_pembelian']?></strong></td>

				<td> BANK MANDIRI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['MANDIRI'];?></strong></td>
			</tr>
		
			<tr>
				<td> ID Affiliate </td>
				<td>:</td>
				<td><strong><?php echo $report_kuota['id_affiliate']?></strong></td>
				
				
				
				<td> BANK BNI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['BNI'];?></strong></td>
			</tr>
			<tr>
				<td> Nama </td>
				<td>:</td>
				<td><strong><?php echo $report_kuota['affiliate']?></strong></td>

				<td> BANK BCA </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['BCA'];?></strong></td>
			</tr>
			<tr>
				<td> Jumlah Kuota </td>
				<td>:</td>
				<td><strong><?php echo $report_kuota['jml_kuota']?></strong></td>

				
				<td> BANK MANDIRI Syariah </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['MANDIRI_Syariah'];?></strong></td>
			</tr>
			<tr>
				<td> Total Harga </td>
				<td>:</td>
				<td><strong>Rp. <?php echo number_format ($report_kuota['total_harga']);?></strong></td>


				<td> BANK BRI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['BRI'];?></strong></td>
			</tr>
			
			<tr>
				<td colspan="3" text-align:center;> Ketentuan dan Syarat</td> 
				
				<td> BANK MUAMALAT </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['MUAMALAT'];?></strong></td>
			</tr>
				<tr>
				
				
				
				<td colspan="6"><strong> Untuk Waktu Pembayaran Dibatasi 3hari, Apabila Pembayaran lebih dari 3 hari Kerja data yang anda Registrasikan akan Terhapus Otomatis dan pembayaran lebih 3 hari harus Registrasi ulang.</strong></td>
				
				
			</tr>
	</table> 

  <span class="input-group-btn">
  	 <?php
      echo '
      
      <a href="'.site_url('kuota_admin/cetak_jamaahkuota').'/cetak/' . $report_kuota['id_booking_kuota'] . '"  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank"> <i class="glyphicon glyphicon-print"></i> CETAK KE PDF</a>';
    ?>
    
</span>