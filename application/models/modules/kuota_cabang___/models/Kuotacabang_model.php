<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kuotacabang_model extends CI_Model{
   
   
    private $_table="kuota_booking";
    private $_primary="id_booking_kuota";

     private $_jamaah_kuota="jamaah_kuota";
    private $_id_kuota="id_kuota";

    private $_booking="booking";
    private $_id_booking="id_booking";

 private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi_jamaah="id_registrasi";

    public function generate_kode($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('md');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }

        public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save_get_kuota($data){
          $schedule = $this->input->post('radio');
         $sql = "SELECT *
         from view_schedule where id_schedule ='$schedule'";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
      }



        $initial = "KC";
        $arr = array(
                                          
            'id_affiliate' => $data['id_affiliate'],
            'id_affiliate_type' => $data['affiliate_type'],
            'status'=>0,
            'status_fee'=>0,
            'id_schedule'=>$id_schedule,
            'schedule'=>$date_schedule,
            'harga' => $harga,
            'kode_unik' => $data['unique_digit'],
            'tgl_pembelian' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status_kuota'=>1,
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_table,$arr);
        $id_booking_kuota =  $this->db->insert_id(); 
        $this->db->update($this->_table,
                    array('invoice'=> $this->generate_kode($initial.$id_booking_kuota)),
                    array('id_booking_kuota'=>$id_booking_kuota));


        $jml_kuota = $this->input->post('jml_kuota');
        for($i=0; $i < $jml_kuota; $i++) {
            $arr = array(
                'id_booking_kuota' => $id_booking_kuota,  
                'kd_kuota' => $this->input->post('id_affiliate').'-'.($i + 1), 
                'nama' => $this->input->post('id_affiliate').'-'.($i + 1),
                'id_affiliate' => $this->input->post('id_affiliate'),
                'id_affiliate_type' => $data['affiliate_type'],
               
                'harga' => $harga,
                'id_schedule'=>$id_schedule,
                'schedule'=>$date_schedule,
                'fee'=>0,
                'fee_refrensi' => 0,
                'status'=>0,
                 'status_fee'=>0,
                 'status_fee_refrensi'=>0,
                 'tgl_pembelian' => date('Y-m-d H:i:s'),
                 'create_date' => date('Y-m-d H:i:s'),
                 'create_by'=>$this->session->userdata('id_user'),
                 'status_kuota'=>1,
               );
           
        
        $this->db->insert($this->_jamaah_kuota,$arr);
        $id_kuota =  $this->db->insert_id(); 

        // $this->db->update($this->_jamaah_kuota,
        //             array('id_jamaah'=> $this->generate_kode($initial.$id_kuota)),
        //             array('id_kuota'=>$id_kuota));
        // }



         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('kuota_cabang/report_kuota/'.$id_booking_kuota.'');
            return true;
        }
    }
}
    public function save($data){

      $invoice = $this->input->post('invoice');
      $id_schedule = $this->input->post('id_schedule');
      $schedule = $this->input->post('schedule');
      $id_kuota = $this->input->post('id_kuota');
     // $sql = "SELECT * from booking where 
     //        invoice = '".$invoice."' 
     //      ";
     //    $query = $this->db->query($sql)->result_array();


     //  foreach($query as $key=>$value){
     //     $booking = $value['id_booking'];
     //  }
         $sql = "SELECT *
         from view_schedule_promo where id_schedule = $id_schedule";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
      }
      $initial = "KC";
        $arr = array(
                                          
            'id_user_affiliate' => $data['id_affiliate'],
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'schedule' => $data['schedule'],
            'harga' => $data['harga'],
            'create_date' => date('Y-m-d H:i:s'),
            // 'status_claim_fee'=>$data['select_status_fee'],
            'create_by'=>$this->session->userdata('id_user'),
            'tempjmljamaah'=>1,
            'status_fee'=>0,
            'tipe_jamaah'=>1,
            'status'=>1
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        
        
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));

          $arr = array(
                'id_booking'=> $id_booking,
                'invoice'=>$this->generate_kode($initial.$id_booking),
                'id_schedule'=> $data['id_schedule'],
                'id_affiliate'=> $data['id_affiliate'],
                'id_product'=> $id_product,

                'embarkasi'=> $embarkasi,
                 'tgl_daftar'=>date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
                'ket_keberangkatan'=>$data['select_status_hubungan'],
                // 'hubkeluarga'=> $data['hubungan'],
                // 'keluarga'=> $data['select_keluarga'],

                 'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                
                'no_pasport'=> $data['no_pasport'],
                'room_type'=> 1,
                'category'=> $room_category,
                'issue_office'=> $data['issue_office'],
                'isui_date'=> $data['isue_date'],

                'merchandise'=> $data['select_merchandise'],
                // 'muhrim'=> $data['muhrim'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> $data['refund'],
                'handling'=> 0,
                'akomodasi'=> $data['akomodasi'],


                'fee'=> 0,
                'fee_input'=> 0,


                'harga_paket'=> $data['harga'],
                'id_status_jamaah'=> 1,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status_identitas'=> $data['status_identitas'],
                'status_kk'=> $data['status_kk'],
                'status_photo'=> $data['status_photo'],
                'status_pasport'=> $data['status_pasport'],
                'status_vaksin'=> $data['status_vaksin'],
                // 'status_claim_fee'=>$data['status_claim_fee'],
                'schedule' => $data['schedule'],
                'status_buku_nikah'=>$data['status_buku_nikah'],
                'pic1' => isset($data['pic1']) ? $data['pic1']: '',
                'pic2' => isset($data['pic2']) ? $data['pic2']: '',
                'pic3' => isset($data['pic3']) ? $data['pic3']: '',
                'pic4' => isset($data['pic4']) ? $data['pic4']: '',
                'pic5' => isset($data['pic5']) ? $data['pic5']: '',
                'pic6' => isset($data['pic6']) ? $data['pic6']: '',
                'status'=> 1,
                'tipe_jamaah'=>1,
                // 'kode_unik'=>$data['unique_digit'],
                // 'cashback'=>$data['cashback'],
                'status_fee'=>0,

        );       
          $this->db->trans_begin(); 
        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
       // $id_jamaah =  $this->db->insert_id();
        // $invoice =  $this->db->insert_id(); 
        // $initial ="SBL";
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($id_registrasi)),
                    // array('harga_paket'=> $this->generate_angka_unik($harga)),
                    array('id_registrasi'=>$id_registrasi));

        
         $arr = array(
            'status'=>2,
                'id_jamaah' => $this->generate_id_jamaah($id_registrasi),   
                
         );
            $this->db->update('jamaah_kuota',$arr,array('id_kuota'=>$id_kuota)); 
       
        
        $arr = array(    
            
            'id_registrasi' => $id_registrasi,
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_affiliate' => $data['id_affiliate'],
            
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 0

        );

        $this->db->insert('list_gaji',$arr);

   

         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $data['id_booking'],
            
            'status'=>0
        );       
        $this->db->insert('pengiriman',$arr);

         $arr = array(
          'id_registrasi'  =>$id_registrasi,
          'id_booking'=> $data['id_booking'],
          'prov_domisili' =>$data['provinsi_domisili'],
          'kab_domisili' =>$data['kabupaten_domisili'],
          'kec_domisili' =>$data['kecamatan_domisili'],
          'alamat_domisili' =>$data['alamat_domisili'],
          'status' => 0
          );
        $this->db->insert('domisili_jamaah',$arr);

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $data['id_booking'],
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>0
          );
        $this->db->insert('visa',$arr);

  $sql = "SELECT * from hitung_muhrim where id_registrasi ='$id_registrasi'
           ";
          $query = $this->db->query($sql)->result_array();
          foreach($query as $key=>$value){
            $id_registrasi = $value['id_registrasi'];
            $biaya_muhrim = $value['muhrim'];
          }

        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }
    
    
      function cek($no_identitas){
        $invoice = $this->input->post('invoice');
        $query = $this->db->query("SELECT * FROM data_jamaah_aktif Where no_identitas ='$no_identitas' and invoice='$invoice'");
        return $query;
    }

      function cek_id_jamaah($id_jamaah){
        $id_jamaah = $this->input->post('id_jamaah');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where id_jamaah ='$id_jamaah'");
        return $query;
    }
    
    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = " SELECT * FROM grouping_jamaah_kuota_cabang_admin  
                    WHERE  status_kuota ='1'
                    ";
        
        if($q){
         

            $sql .=" AND affiliate LIKE '%{$q}%'
                    OR id_affiliate LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY tgl_pembelian DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    

    
    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

    function get_fee_jamaah_kuota($affiliate_type='') {
      $this->db->where("id_affiliate_type",$affiliate_type);
      return $this->db->get("affiliate_type");
    }


     public function get_booking_kuota($id_booking_kuota){
 

         $sql = "SELECT * from faktur_jamaah_kuota where id_booking_kuota = {$id_booking_kuota} and status_kuota='1'";
  
        return $this->db->query($sql)->row_array();  
    }

    function lap_data_jamaah($id_booking_kuota) {
        $this->db->select('*');
        $this->db->from('faktur_jamaah_kuota');
        $this->db->where('id_booking_kuota',$id_booking_kuota);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }



     public function get_jamaah_kuota_aktif($id_booking_kuota){
    
        $sql ="SELECT * from detail_jamaah_kuota where  id_booking_kuota  = ?
              ";
        return $this->db->query($sql,array($id_booking_kuota))->result_array();    

    }


      public function get_jamaah_kuota_mutasi_nama($id_jamaah){
    
        $sql ="SELECT * from detail_jamaah_kuota_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

    }

    function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
      
          $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.'</strong></td>
                   <td><strong>'.$row->keterangan.'</strong></td>
                 <td><strong>'.$row->harga.'</strong></td>
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }


     public function get_booking_kuota_aktif($id_booking_kuota){
 

         $sql = "SELECT * from grouping_jamaah_kuota_admin where id_booking_kuota = '$id_booking_kuota'";
  
        return $this->db->query($sql)->row_array();  
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

    public function update_cetak($data){
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            
            'keterangan' => $data['keterangan'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user'),
        );       
        
     
         $this->db->update('jamaah_kuota',$arr,array('kd_kuota'=>$data['kd_kuota']));

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();

            return true;

        }
    }



    public function cetak_bilyet($id_kuota){
 

         $sql = "SELECT * from detail_jamaah_kuota where id_kuota = '$id_kuota'";
  
        return $this->db->query($sql)->row_array();  
    }
}
