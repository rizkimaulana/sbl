<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kuota_cabang extends CI_Controller{ 
	var $folder = "kuota_cabang";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(JAMAAH_KUOTA_CABANG,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		 $this->load->library('terbilang');
		$this->load->model('kuotacabang_model');
		$this->load->library('ci_qr_code');
        $this->config->load('qr_code');
	}
	
	public function index(){
		 
	    $this->template->load('template/template', $this->folder.'/kuota_cabang');
		
	}
	
	public function cetak_data_kuota(){
	    $id_booking_kuota = $this->uri->segment(3);

	    $this->template->load('template/template', $this->folder.'/kuota_cabang/report_kuota/$id_booking_kuota ');
		
	}

	private function _select_embarkasi(){
	 // 	 $embarkasi = array('1','3');
		// $this->db->where_in('id_embarkasi', $embarkasi);
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_affiliate_type(){
		$id_affiliate_type = array('1','5');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
	    return $this->db->get('affiliate_type')->result();
	}


	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->order_by("nama", "asc")->get_where(
		    	'affiliate',array(
		    		'id_affiliate_type'=>$id_affiliate_type,
		    		'status'=>1
		    		)

		    	);
		    $this->db->order_by("nama", "asc");
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama." ".$value->id_user."</option>";
		    }
		    echo $data;
	}
	
public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->kuotacabang_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="20%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->id_affiliate.'</td>';
	             	 $rows .='<td width="20%">'.$r->tgl_pembelian.'</td>';
	                $rows .='<td width="9%">'.$r->jml_kuota.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Data Kuota" class="btn btn-sm btn-primary" href="'.base_url().'kuota_cabang/detail/'.$r->id_booking_kuota.'">
	                            <i class="fa fa-pencil"></i> Data Kuota
	                        </a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room('."'".$r->id_room."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'room/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	public function generate_data_kuota(){
		// $data = $this->input->post(null,true);
		// $send = $this->kuotacabang_model->save_get_kuota($data);
	  
	//   	$this->form_validation->set_rules('radio','radio','required|trim');
	//    if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
	// 	           $data = $this->input->post(null,true);
	// 			    $schedule = $this->input->post('radio');
	// 			    $inputan = '';
	// 			    foreach($schedule as $value){
	// 			    	$inputan .= ($inputan!=='') ? ',' : '';
	// 			    	$inputan .= $value;
	// 			    }
				   
	// 			  $send = $this->kuotacabang_model->save_get_kuota($data,$inputan);
 //            }else{
			 
	// 	$this->session->set_flashdata('info', "ERROR");
	// }

		  	$this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
		           $data = $this->input->post(null,true);
				    $schedule = $this->input->post('radio');

				  $send = $this->kuotacabang_model->save_get_kuota($data);
            }

	}
	
	public function save(){    
	

	   $data = $this->input->post(null,true);
	  $send = $this->kuotacabang_model->save($data);
	  if($send)
	  	$this->session->set_flashdata('info', "Successfull");
			//$id_booking = $this->uri->segment(4);
			$id_booking = $this->input->post('id_booking');
	        redirect('kuota_cabang/detail/'.$id_booking.'');

					// echo'<div class="alert alert-dismissable alert-success"><h4>Transaction Successfull</h4></div>';
		}

	

	public function add(){
	     
	     if(!$this->general->privilege_check(JAMAAH_KUOTA_CABANG,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_affiliate_type'=>$this->_select_affiliate_type(),
	    				'select_roomcategory'=>$this->_select_roomcategory(),
	    				'select_embarkasi'=>$this->_select_embarkasi(),
	    	
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);		
	}
	
	
	 function get_fee_kuota(){
		$affiliate_type = $this->input->post('affiliate_type');
		$affiliate_type = $this->kuotacabang_model->get_fee_jamaah_kuota($affiliate_type);
		 if($affiliate_type->num_rows()>0){
            $affiliate_type=$affiliate_type->row_array();
            echo $affiliate_type['fee_jamaah_kuota'];
           
          
        }
        
	}


	public function report_kuota(){  
	    $id_booking_kuota = $this->uri->segment(3);
	    $report_kuota = $this->kuotacabang_model->get_booking_kuota($id_booking_kuota);
	    // $pic    = array();
	    // $pic_booking= array();
	    // if(!$report_kuota){
	    //     show_404();
	    // }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'report_kuota'=>$report_kuota,
	       		 // 'pic'=>$pic
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_kuota',($data));
	    // $this->load->view('bilyet_barcode',($data));
	}


	public function detail(){
	    if(!$this->general->privilege_check(JAMAAH_KUOTA_CABANG,'view'))
		    $this->general->no_access();
	    
	    $id_booking_kuota = $this->uri->segment(3);
	    $booking = $this->kuotacabang_model->get_jamaah_kuota_aktif($id_booking_kuota);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    // else{

	    //     $pic = $this->kuotacabang_model->get_jamaah_kuota_aktif($id_booking);
	    // }    

	    $data = array(

	       		 'booking'=>$booking,
	       		 // 'pic'=>$pic
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/mutasi_data',($data));

	}


	public function mutasi_data_jamaah($id_booking_kuota=''){
	    if(!$this->general->privilege_check(JAMAAH_KUOTA_CABANG,'view'))
		    $this->general->no_access();

		$id_booking= $this->uri->segment(4);

	    $id_kuota = $this->uri->segment(3);
	    $get = $this->db->get_where('jamaah_kuota',array('id_kuota'=>$id_kuota))->row_array();
	   if(!$get)
	        show_404();
	        $grouping_kuota = $this->kuotacabang_model->get_booking_kuota_aktif($id_booking);
	    $data = array(
	    		 'select_product'=>$this->_select_product(),
				'select_embarkasi'=>$this->_select_embarkasi(),
				'select_roomcategory'=>$this->_select_roomcategory(),
	    		'grouping_kuota'=>$grouping_kuota,
	    		'provinsi'=>$this->kuotacabang_model->get_all_provinsi(),
		    	'kabupaten'=>$this->kuotacabang_model->get_all_kabupaten(),		
		    	'select_kelamin'=>$this->_select_kelamin(),	
		    	'select_rentangumur'=>$this->_select_rentangumur(),
		    	'select_statuskawin'=>$this->_select_statuskawin(),
		    	'select_status_hubungan'=>$this->_select_status_hubungan(),
		    	'family_relation'=>$this->_family_relation(),
		    	'select_keluarga'=>$this->_select_keluarga(),
		    	'select_pemberangkatan'=>$this->_select_pemberangkatan(),
		    	'select_type'=>$this->_select_type(),
		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'select_merchandise'=>$this->_select_merchandise(),
		    	'select_status_visa'=>$this->_select_status_visa(),
	    	);
		$this->template->load('template/template', $this->folder.'/mutasi_data_jamaah',array_merge($get,$data));
	   



	}

	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->kuotacabang_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

 //     private function _select_embarkasi(){
	
	//     return $this->db->get('embarkasi')->result();
	// }
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_roomcategory(){
		//  $id_room_category = array('1');
		// $this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

	function add_ajax_kab($id_prov){
	    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
	    $data = "<option value=''>- Select Kabupaten -</option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_keluarga($id_booking=''){
	// $id_booking = $this->uri->segment(4);
	// $this->db->where_in('id_booking', $id_booking);
 //    return $this->db->get('jamaah_kuota')->result();
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}


	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}

	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->kuotacabang_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->kuotacabang_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->kuotacabang_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}

	function searchItem_paket_promo(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($datepicker_tahun_keberangkatan)){
                 $this->kuotacabang_model->searchItem_paket_promo($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

   private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}


	public function bilyet(){
	


	    if(!$this->general->privilege_check(JAMAAH_KUOTA_CABANG,'cetak'))
		    $this->general->no_access();
	    
	    $id_kuota = $this->uri->segment(4);
	    $get = $this->db->get_where('detail_jamaah_kuota',array('id_kuota'=>$id_kuota))->row_array();
	    if(!$get)
	        show_404();
	        
	   			 $data = array('
	   			 	select_user'=>$this->_select_user(),	
	    	);
        $this->template->load('template/template', $this->folder.'/cetak_bilyet',array_merge($get,$data));

	}
	private function _select_user(){
		
		return $this->db->get('user')->result();
	}
	
	function print_qr($id_kuota)
    {
        $qr_code_config = array();
        $qr_code_config['cacheable'] = $this->config->item('cacheable');
        $qr_code_config['cachedir'] = $this->config->item('cachedir');
        $qr_code_config['imagedir'] = $this->config->item('imagedir');
        $qr_code_config['errorlog'] = $this->config->item('errorlog');
        $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
        $qr_code_config['quality'] = $this->config->item('quality');
        $qr_code_config['size'] = $this->config->item('size');
        $qr_code_config['black'] = $this->config->item('black');
        $qr_code_config['white'] = $this->config->item('white');
        $this->ci_qr_code->initialize($qr_code_config);

        // get full name and user details
        $id_bilyet = $this->uri->segment(3);
	    $bilyet = $this->kuotacabang_model->cetak_bilyet($id_kuota);
        $image_name = $id_bilyet . ".png";

        // create user content
        // $codeContents = "user_name:";
        // $codeContents .= "$user_details->user_name";
        // $codeContents .= " user_address:";
        // $codeContents .= "$user_details->user_address";
        // $codeContents .= "\n";
        // $codeContents .= "user_email :";
        $codeContents .= $bilyet['id_bilyet'];

        $params['data'] = $codeContents;
        $params['level'] = 'H';
        $params['size'] = 2;

        $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
        $this->ci_qr_code->generate($params);

        $this->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

        // save image path in tree table
        // $this->user->change_userqr($user_id, $image_name);
        // then redirect to see image link
        $file = $params['savename'];
        if(file_exists($file)){
            header('Content-Description: File Transfer');
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            unlink($file); // deletes the temporary file

            exit;
        }
    }


	public function cetak_bilyet(){

	   
	    $kd_kuota = $this->uri->segment(3);
	    $id_kuota = $this->uri->segment(4);
	    $id_booking_kuota = $this->uri->segment(5);
	    $bilyet = $this->kuotacabang_model->cetak_bilyet($id_kuota);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        // show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 // 'pic'=>$pic
	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/bilyet_barcode',($data));
	    $this->load->view('cetak',($data));
	}

	public function save_cetak_bilyet(){    
     $kd_kuota = $this->input->post('kd_kuota');
     $id_booking_kuota = $this->input->post('id_booking_kuota');
      $id_kuota = $this->input->post('id_kuota');
     // $id_sahabat = $this->input->post('id_sahabat'); 

     // $userid = $this->input->post('id_sahabat');
     // $kelas = $this->input->post('kelas'); 
     // $cek_cetak_bilyet=$this->bilyetsahabat_model->cek_cetak_bilyet($id_bilyet,$id_sahabat);
     
     //   $this->form_validation->set_rules('keterangan','keterangan','required|trim');
     
     //   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
          
     //         // cek kode di database
     //       if ($cek_cetak_bilyet->num_rows()>0) {
     //            $data = $this->input->post(null,true);
     //            $send = $this->bilyetsahabat_model->update_cetak($data);
     //            if($send)
     //            redirect('bilyet_sahabat/cetak_bilyet/'.$id_bilyet.'');
            
     //        }else{
     //            $data = $this->input->post(null,true);
     //            $send = $this->bilyetsahabat_model->save_cetak($data);
     //            if($send)
     //            redirect('bilyet_sahabat/cetak_bilyet/'.$id_bilyet.'');
     //        }
         $data = $this->input->post(null,true);
                $send = $this->kuotacabang_model->update_cetak($data);
                if($send)
                redirect('kuota_cabang/cetak_bilyet/'.$kd_kuota.'/'.$id_kuota.'/'.$id_booking_kuota.'');
    }

}
