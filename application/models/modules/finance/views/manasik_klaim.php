<div id="page-wrapper">
    <form   class="form-horizontal" role="form" action='<?= base_url(); ?>finance/manasik/save_manasik_klaim' method='POST'>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Klaim Manasik Fee</b>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"for="invoice" >Invoice</label>
                            <div class="col-sm-3">
                                <input type="hidden" class="form-control" name="id_booking" id="id_booking" value="<?php echo $detail['id_booking']; ?>" readonly=""/>
                                <input type="text" class="form-control" name="invoice" id="invoice" value="<?php echo $detail['invoice']; ?>" readonly=""/>
                            </div>
                            <label class="col-sm-2 control-label"for="tgl_daftar" >Tgl.Daftar</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="tgl_daftar" id="tgl_daftar" value="<?php echo $detail['tgl_daftar']; ?>" readonly=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"for="nama_affiliate" >Nama Affiliate</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="nama_affiliate" id="nama_affiliate" value="<?php echo $detail['nama_affiliate']; ?>" readonly=""/>
                            </div>
                            <label class="col-sm-2 control-label"for="id_affiliate" >ID Affiliate</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="id_affiliate" id="id_affiliate" value="<?php echo $detail['id_affiliate']; ?>" readonly=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"for="id_affiliate" >Detail</label>
                            <div class="col-sm-10">
                                <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <tr>
                                        <th>No</th>
                                        <th>ID Jamaah</th>
                                        <th>Nama Jamaah</th>
                                        <th>Keterangan</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th>Manifest</th>
                                    </tr>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $total = 0;
                                        ?>
                                        <?php foreach ($list_detail as $item) : ?>
                                            <?php
                                            if (($item['status'] === '1') && ($item['status_manifest'] === '1')) {
                                                $kelas = '';
                                                $total += $item['jumlah'];
                                            } else {
                                                $kelas = 'class="danger"';
                                            }
                                            ?>
                                            <tr <?php echo $kelas; ?> >
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $item['id_jamaah']; ?></td>
                                                <td><?php echo $item['nama']; ?></td>
                                                <td><?php echo $item['keterangan']; ?></td>
                                                <td class="text-right"><?php echo number_format($item['jumlah'], 0, ',', '.'); ?></td>
                                                <td class="text-center"><?php echo $item['status']; ?></td>
                                                <td class="text-center"><?php echo $item['status_manifest']; ?></td>
                                            </tr>
                                            <?php $no++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tr>
                                        <th colspan="4" class="text-right">TOTAL YANG BISA DIKLAIM</th>
                                        <th class="text-right"><?php echo number_format($total, 0, ',', '.'); ?></th>
                                        <th colspan="2"></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"for="id_user" >Penerima</label>
                            <div class="col-sm-4">
                                <input type="search" name="autocomplete" id="autocomplete1" class="form-control autocomplete userid" placeholder="INPUT ID CABANG PENERIMA KLAIM" tabindex="1" required>
                            </div>
                            <label class="col-sm-2 control-label"for="nama_user" >ID Penerima</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="id_user" id="id_user" value="<?php $detail['invoice']; ?>" readonly="" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"for="transfer_date" >Transfer Date</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="transfer_date" id="datepicker2"   placeholder="YYYY-MM-DD" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Dari Dana Bank </label>
                            <div class="col-lg-3">
                                <select required class="form-control" name="dana_bank" id="dana_bank">
                                    <option value="">- Select Bank -</option>
                                    <?php foreach ($dana_bank as $bank) : ?>
                                        <option value="<?php echo $bank['id']; ?>"><?php echo $bank['nama']; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Bank Transfer </label>
                            <div class="col-lg-3">
                                <select required class="form-control" name="bank_transfer" id="bank_transfer">
                                    <option value="">- Select Bank -</option>
                                    <?php foreach ($bank_transfer as $item_bank) : ?>
                                        <option value="<?php echo $item_bank['id']; ?>"><?php echo $item_bank['nama']; ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">No Rekening</label>
                            <div class="col-lg-3">
                                <input name="no_rek" id="no_rek" class="form-control" required=""> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Atas Nama Rekening</label>
                            <div class="col-lg-4">
                                <input name="nama_rek" id="nama_rek" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Nominal</label>
                            <div class="col-lg-3">
                                <input name="nominal" id="nominal" class="form-control" value="<?php echo number_format($total, 0, ',', ','); ?>" required=""> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Keterangan</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" name="keterangan" required=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label"></label>
                            <div class="col-lg-10">
                                <button type="submit" class="btn btn-primary" onclick="return confirm('Data sudah dipastikan benar?')"><i class="fa fa-save"></i> Klaim Manasik Fee</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/jquery.autocomplete.js'></script>
<link href='<?php echo base_url(); ?>assets/jquery.autocomplete.css' rel='stylesheet' />
<script type='text/javascript'>
    var site = "<?php echo site_url(); ?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + 'finance/manasik/autocomplete_affiliate',
            onSelect: function (suggestion) {
                $('#id_user').val('' + suggestion.id_user);
                $('#no_rek').val(suggestion.no_rek);
                $('#nama_rek').val(suggestion.nama_rek);
                $('#bank_transfer').val(suggestion.bank_transfer);
            }
        });
    });
    $('#datepicker2').datetimepicker({
        format: 'YYYY-MM-DD',
    });
</script>