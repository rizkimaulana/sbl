
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        
        <!-- yg disini bg -->
                        
<form   class="form-inline" role="form" method='POST'  action='<?= base_url();?>finance/booking_report/export_to_excell_'>                 
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Repoting Keuangan</b>
                </div>
           	<div class="form-group">
					    
					    
					</div>
              		<div class="form-group">
					    <label>From:</label>
					    <input type="text" class="form-control" id='from' placeholder="Start date" name="from">
					</div>
					  <div class="form-group">
					    <label>To:</label>
					     <input type="text" class="form-control" id='to' placeholder="End date" name="to">
					     
					  </div>
					 
            <div class="form-group">
					   <label>Cari Periode:</label>
					      <button class="form-control" type="submit" id='date-search'><i class="glyphicon glyphicon-search"></i></button>
                
					  </div>

        
                 
                 <div class="panel-body">  

                     </div>
                          <div class="table-responsive">
                              <table  class="table table-bordered" id="dataTables-example">
                               <thead>
                                <tr>
                                	
                                    <th>No</th>
                                    <th>Invoice</th>
                                    <th>Afiliate</th>
                                    <!-- <th>Paket </th>
                                    <th>Keberangkatan </th>
                                   <th>Departure </th> -->
                                   <th>Jml Jamaah </th>
                                  <!--  <th>Quard </th>
                                   <th>Double </th>
                                   <th>Triple </th>
                                   <th>Harga Quard </th>
                                   <th>Harga Double </th>
                                   <th>Harga Triple </th> -->
                                   <th>Room Price</th>
                                    <th>CashBack</th>
                                    <th>Akomodasi</th>
                                    <th>Handling</th>
                                    <th>muhrim</th>
                                   <th>Visa</th>
                                  <th>Total Bayar </th>

                                  <th>Action </th>
                                </tr>
                            </thead>
                             <tbody id='response-table'>
                    <tr><td colspan="13"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>
                        
                            </tbody>
                     </table>
                   </div>
                  <!-- <button type='submit' class="btn btn-primary"><i class="fa fa-save"></i> Clime Fee</button> -->
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->

              <div class="form-group">
                <button type='submit' class="btn btn-primary" target="_blank"><i class="fa fa-save"></i> Export Excell</button>
             
          </div> 
           

          </div>
      </div>

    </div>

 </form> 
<script type="text/javascript">
 $('#selecctall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
</script>

              
  <script>
 // $(document).ready(function () {
 //    $('#dataTables-example').dataTable({
 //      "pagingType": "full_numbers",
 //      "iDisplayLength": 45});

 //  });
</script>



<script type="text/javascript">
	$( "#from" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		dateFormat: "yy-mm-dd",
		onClose: function( selectedDate ) {
		$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
		});
		$( "#to" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		dateFormat: "yy-mm-dd",
		onClose: function( selectedDate ) {
		$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
});




$("#date-search").on('click', function (e){
    e.preventDefault();
    var from = $("#from").val();
    var to = $("#to").val();
 
      console.log(from);
    console.log(to);
   
        $.ajax({
            type:'POST',
            url:'<?php echo base_url() ?>finance/booking_report/searchAdvance',

            data:'from='+from+'&to='+to
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});
</script>