
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        <form   class="form-horizontal" role="form" action='<?= base_url();?>finance/fee_input/save_fee_input' method='POST'>
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah Fee </b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_claim_feeinput" value="<?php echo $affiliate['id_claim_feeinput']?>">
                    <!-- <input type="hidden" name="id_schedule" value="<?php echo $affiliate['id_schedule']?>"> -->
                    <!-- <input type="hidden" name="id_affiliate" value="<?php echo $pic['id_affiliate']?>"> -->
                   
                      
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Fee Affiliate</b>
                </div>

                 <div class="panel-body">
                    
                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                	<th><input id="selecctall" type="checkbox">&nbsp;Check All</th>
                                    <!-- <th>No</th> -->
                                    <th>Invoice</th>
                                    <th>ID Jamaah</th>
                                    <th>Nama </th>
                                    <th>Product </th>
                                   <th>Fee Input </th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                  	<td> <input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="<?php echo $pi['id_registrasi'] ?>"><?php echo $no; ?></td>
                                      



                                       <td><?php echo $pi['invoice']?>  </td> 
                                      <td><?php echo $pi['id_jamaah']?>  </td>
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['product']?>  </td>
                                      <td><?php echo $pi['fee_input']?>  </td>
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                
                  
                  
              </div>
          </div>
      </div>
    </div>
     
       <div class="row">
         <div class="col-lg-10">
         
          <div class="panel-body">
            <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Transfer Date</label>
                    <div class="col-sm-10">
                    
                         <input type="text" class="form-control" name="transfer_date" id="datepicker2"   placeholder="YYYY-MM-DD" />
                
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Dari Dana Bank </label>
                      <div class="col-lg-10">
                         <select required class="form-control" name="dana_bank">
                          <option value="">- Select Bank -</option>
                            <?php foreach($dana_bank as $sb){?>
                                <option value="<?php echo $sb->id;?>"><?php echo $sb->nama;?></option>
                            <?php } ?>
                    </select> 
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer </label>
                      <div class="col-lg-10">
                         <select required class="form-control" name="bank_transfer">
                          <option value="">- Select Bank -</option>
                            <?php foreach($bank_transfer as $sb){?>
                                <option value="<?php echo $sb->id;?>"><?php echo $sb->nama;?></option>
                            <?php } ?>
                    </select> 
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">No Rekening</label>
                      <div class="col-lg-10">
                        <input name="no_rek" class="form-control" > 
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Atas Nama Rekening</label>
                      <div class="col-lg-10">
                        <input name="nama_rek" class="form-control"  > 
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-lg-2 control-label">Nominal</label>
                      <div class="col-lg-10">
                        <input name="nominal" class="form-control"  > 
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Keterangan</label>
                      <div class="col-lg-10">
                        <textarea class="form-control" name="keterangan" ></textarea>
                      </div>
                    </div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Approved</button>
        <a href="<?php echo base_url();?>finance/fee_input"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
          </div>


        
     </div>
   </div>
  </form>       
                
</div>
<script type="text/javascript">
 $('#selecctall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
</script>

              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 45});

  });
</script>

<script type="text/javascript">
	$("#clime").on('click', function (e){
    e.preventDefault();
var checkValues = $('.checkbox1:checked').map(function()
    {
        return $(this).val();
    }).get();
//console.log(checkValues); return  false;
$.ajax({
        url: '<?php echo base_url() ?>finance/fee_input/save',
        type: 'post',
        data: 'id_affiliate='+checkValues
        }).done(function(data){
            window.location.reload();
//            $("#respose-text").html(data);
        });
    });
 $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
   
</script>