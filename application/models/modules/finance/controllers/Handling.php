<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Handling extends CI_Controller {

    var $folder = "finance";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if(!$this->general->privilege_check(BIAYA_HANDLING,'view'))
            $this->general->no_access();
        
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('handling_model');
    }

    public function index() {
        $this->template->load('template/template', $this->folder . '/handling');
    }

    function json_biaya_handling(){
        $hasil = $this->handling_model->get_where_handling();
        $data = array();
        $no = 1;
        foreach ($hasil as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['invoice'];
            $row[] = $item['id_user'];
            $row[] = $item['nama_affiliate'];
            $row[] = date('d M Y', strtotime($item['tgl_daftar']));
            $row[] = $item['jml_jamaah'];
            $row[] = number_format($item['subtotal'],0,',','.');
            $row[] = '<a title="Klaim Handling Fee" class="btn btn-sm btn-warning" href="'.base_url().'finance/handling/handling_klaim/'.$item['id_booking'].'"><i class="fa fa-pencil"></i>KLAIM</a>';
            $data[] = $row;
            $no++;
        }
        
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function handling_klaim($id_booking){
        if(!$this->general->privilege_check(BIAYA_HANDLING,'edit'))
            $this->general->no_access();
        
        $detail = $this->handling_model->get_handling($id_booking);
        $list_detail = $this->handling_model->get_item_handling($id_booking);
        $opsi_bank = $this->db->get('bank')->result_array();
        $data = array(
            'dana_bank' => $opsi_bank,
            'bank_transfer' => $opsi_bank,
            'detail' => $detail,
            'list_detail' => $list_detail,
        );
        $this->template->load('template/template', $this->folder . '/handling_klaim', $data);
    }
    
    function save_handling_klaim(){
        if(!$this->general->privilege_check(BIAYA_HANDLING,'edit'))
            $this->general->no_access();
        
        $post_data = $this->input->post(NULL, FALSE);
        try {
            $hasil = $this->handling_model->simpan_klaim_handling($post_data);
            $this->simpan_aktivitas('Simpan Klaim Handling Fee', $post_data);
        } catch (Exception $exc) {
            $this->simpan_error('Simpan Klaim Handling Fee', $post_data, $exc);
        }
        redirect('finance/handling');
    }
    
    public function autocomplete_affiliate() {
        $keyword = $this->uri->segment(4);
        $data = $this->handling_model->search_affiliate($keyword);
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row['nama'].' - '.$row['id_user'],
                'nama' => $row['nama'],
                'id_user' => $row['id_user'],
                'no_rek' => $row['norek'],
                'nama_rek' => $row['namarek'],
                'bank_transfer' => $row['bank']
            );
        }
        echo json_encode($arr);
    }
}
