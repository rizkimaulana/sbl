<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_report extends CI_Controller{
	var $folder = "finance";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(BOOKING_REPORT,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','finance');	
		$this->load->model('bookingreport_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/booking_report');
		
	}

  public function laporan_detail_jamaah_keungan(){
      
      $this->template->load('template/template', $this->folder.'/report_detail_jamaah');
    
  }
	
	public function laporan(){
	    
	    // $this->template->load('template/template', $this->folder.'/lap_keuangan_excell');
	     $this->load->view('finance/lap_keuangan_excell');
		
	}
	
	 public function reporting_payment(){
      
      // $this->template->load('template/template', $this->folder.'/lap_keuangan_excell');
       $this->load->view('finance/payment_reporting');
    
  }

	 function searchAdvance(){
	 		
             $from = $this->input->post('from');
             $to = $this->input->post('to');
             if(empty($from) && empty($to)){
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
                 exit;
             }
             else{
                 $this->bookingreport_model->searchAdvance($from , $to);
             }
   }

     function searchAdvance_detail_jamaah(){
      
             $from = $this->input->post('from');
             $to = $this->input->post('to');
             if(empty($from) && empty($to)){
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
                 exit;
             }
             else{
                 $this->bookingreport_model->searchAdvance_detail_jamaah($from , $to);
             }
   }
   


	public function export_to_excell_(){
  


      if(!$this->general->privilege_check(BOOKING_REPORT,'view'))
        $this->general->no_access();
   		 $from = $this->input->post('from');
    	$to = $this->input->post('to');
    if(empty($from) && empty($to)){
         echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
         exit;
     }
     else{
    
      $id_booking = $this->uri->segment(3);
      $booking = $this->bookingreport_model->get_pic($from,$to);
      $pic    = array();
      // $pic_booking= array();
      if(!$booking){
          show_404();
      }
      else{
          
          // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
          $pic = $this->bookingreport_model->get_pic($from,$to);
      }    

        
      $data = array(

       
             'booking'=>$booking,'pic'=>$pic

             );

   		 $this->load->view('finance/lap_keuangan_excell',($data));
      // $this->template->load('template/template', $this->folder.'/lap_keuangan_excell',($data));

  		}
	}


  public function export_to_excell_detail_jamaah(){
  


      if(!$this->general->privilege_check(BOOKING_REPORT,'view'))
        $this->general->no_access();
       $from = $this->input->post('from');
      $to = $this->input->post('to');
    if(empty($from) && empty($to)){
         echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
         exit;
     }
     else{
    
      $id_booking = $this->uri->segment(3);
      $booking = $this->bookingreport_model->get_pic_detail_jamaah($from,$to);
      $pic    = array();
      // $pic_booking= array();
      if(!$booking){
          show_404();
      }
      else{
          
          // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
          $pic = $this->bookingreport_model->get_pic_detail_jamaah($from,$to);
      }    

        
      $data = array(

       
             'booking'=>$booking,'pic'=>$pic

             );

       $this->load->view('finance/lap_keuangan_detail_jamaah',($data));
      // $this->template->load('template/template', $this->folder.'/lap_keuangan_excell',($data));

      }
  }


   public function export_reporting_payment(){
  


      if(!$this->general->privilege_check(BOOKING_REPORT,'view'))
        $this->general->no_access();
      
      $id_booking = $this->uri->segment(4);
      $booking = $this->bookingreport_model->get_pic_payment($id_booking);
      $pic    = array();
      // $pic_booking= array();
      if(!$booking){
          show_404();
      }
      else{
          
          // $pic_booking = $this->bookingreport_model->get_pic_payment_($id_booking);
          $pic = $this->bookingreport_model->get_pic_detail_payment($id_booking);
      }    

        
      $data = array(

             // 'pic_booking'=>$pic_booking,
             'booking'=>$booking,'pic'=>$pic

             );
  
      // $this->template->load('template/template', $this->folder.'/detail_booking_report',($data));
      $this->load->view('finance/payment_reporting',($data));
     

  }


   public function detail(){
  


      if(!$this->general->privilege_check(BOOKING_REPORT,'view'))
        $this->general->no_access();
      
      $id_booking = $this->uri->segment(4);
      $booking = $this->bookingreport_model->get_pic_payment($id_booking);
      $pic    = array();
      // $pic_booking= array();
      if(!$booking){
          show_404();
      }
      else{
          
          // $pic_booking = $this->bookingreport_model->get_pic_payment_($id_booking);
          $pic = $this->bookingreport_model->get_pic_detail_payment($id_booking);
      }    

        
      $data = array(

             // 'pic_booking'=>$pic_booking,
             'booking'=>$booking,'pic'=>$pic

             );
  
      $this->template->load('template/template', $this->folder.'/detail_booking_report',($data));

     

  }
}