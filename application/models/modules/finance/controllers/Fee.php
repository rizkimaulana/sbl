<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fee extends CI_Controller{
	var $folder = "finance";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(FEE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','finance');	
		$this->load->model('fee_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/fee');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	
	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->fee_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';                
	                $rows .='<td width="10%">'.$r->id_affiliate.'</td>';
	                $rows .='<td width="30%">'.$r->nama_affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->product.'</td>';
	                $rows .='<td width="9%">'.$r->jumlah_jamaah.'</td>';
	                $rows .='<td width="9%">'.$r->total_fee.'</td>';
	                 $rows .='<td width="9%">'.$r->free_hemat.'</td>';
	                  $rows .='<td width="9%">'.$r->free_tunai.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Clime Fee" class="btn btn-sm btn-primary" href="'.base_url().'finance/fee/detail/'.$r->id_affiliate.'/'.$r->id_product.'">
	                            <i class="fa fa-pencil"></i> Clime Fee
	                        </a> ';
	               $rows .='<br></br>';

	                 $rows .='<a title="Clime Free Hemat" class="btn btn-sm btn-success" href="'.base_url().'finance/fee/detail_freeHemat/'.$r->id_affiliate.'/5">
	                            <i class="glyphicon glyphicon-ok-sign"></i> Clime Free Hemat
	                        </a> ';

	                 $rows .='<br></br>';

	                 $rows .='<a title="Clime Free Tunai" class="btn btn-sm btn-info" href="'.base_url().'finance/fee/detail_freeTunai/'.$r->id_affiliate.'/4">
	                            <i class="glyphicon glyphicon-ok-circle"></i> Clime Free Tunai
	                        </a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_product('."'".$r->id_product."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'finance/fee/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	

	public function detail_freeHemat(){
	

	  		
        $this->template->load('template/template', $this->folder.'/detail_freehemat');

	}

		public function detail_freeTunai(){
	

	  		
        $this->template->load('template/template', $this->folder.'/detail_freetunai');

	}

	public function detail(){
	


	    if(!$this->general->privilege_check(FEE,'view'))
		    $this->general->no_access();
	    
	    $id_affiliate = $this->uri->segment(4);
	    $id_product = $this->uri->segment(5);
	    $affiliate = $this->fee_model->get_pic($id_affiliate,$id_product);
	    // $product = $this->fee_model->get_pic($id_product);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$affiliate ){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->fee_model->get_pic($id_affiliate,$id_product);
	       
	    }    

	    $data = array(
	    		
	       		 'affiliate'=>$affiliate,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_fee',($data));

	}

	 function searchAdvance(){
	 		  $id_affiliate = $this->input->post('id_affiliate');
       		$id_product = $this->input->post('id_product');
             $from = $this->input->post('from');
             $to = $this->input->post('to');
             if(empty($from) && empty($to)){
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
                 exit;
             }
             else{
                 $this->fee_model->searchAdvance($from , $to, $id_product, $id_affiliate );
             }
   }

    function searchAdvanceTunai(){
	 		  $id_affiliate = $this->input->post('id_affiliate');
       		$id_product = $this->input->post('id_product');
             $from = $this->input->post('from');
             $to = $this->input->post('to');
             if(empty($from) && empty($to)){
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
                 exit;
             }
             else{
                 $this->fee_model->searchAdvance($from , $to, $id_product, $id_affiliate );
             }
   }



	function save_fee(){
	    $data = $this->input->post('checkbox');
	    $inputan = '';
	    foreach($data as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;
	    }
	    $simpan = $this->fee_model->save_freehemat($inputan);
	    if($simpan)
	        redirect('finance/fee');
	}


	function save_freehemat(){
	    $data = $this->input->post('checkbox');
	    $inputan = '';
	    foreach($data as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;
	    }
	    $simpan = $this->fee_model->save_freehemat($inputan);
	    if($simpan)
	        redirect('finance/fee');
	}

	function save_freetunai(){
	    $data = $this->input->post('checkbox');
	    $inputan = '';
	    foreach($data as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;
	    }
	    $simpan = $this->fee_model->save_freehemat($inputan);
	    if($simpan)
	        redirect('finance/fee');
	}
	
}
