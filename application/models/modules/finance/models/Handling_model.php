<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Handling_model extends CI_Model {

    private $_table = "claim_additional_cost";

    function get_where_handling(){
        $this->db->select('a.id_booking, SUM(a.jumlah) as subtotal, COUNT(*) as jml_jamaah, a.keterangan, a.id_user, b.nama as nama_affiliate, c.invoice, c.id_user_affiliate, c.tgl_daftar, c.id_schedule, c.id_product, c.category, c.id_productprice, c.embarkasi');
        $this->db->from('additional_cost AS a');
        $this->db->join('affiliate AS b', 'b.id_user = a.id_user', 'left');
        $this->db->join('booking AS c', 'c.id_booking = a.id_booking', 'left');
        $this->db->where('a.status', '1');
        $this->db->where('a.status_manifest', '1');
        $this->db->where('a.jns_trans', '144');
        $this->db->group_by('a.id_booking');
        $this->db->order_by('a.id_booking asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_item_handling($id_booking){
        $this->db->select('a.id, a.id_jamaah, a.jumlah, a.keterangan, a.status, a.status_manifest, b.invoice, b.nama, b.alamat, b.tanggal_lahir');
        $this->db->from('additional_cost AS a');
        $this->db->join('registrasi_jamaah AS b', 'b.id_jamaah = a.id_jamaah', 'left');
        $this->db->where('a.id_booking', $id_booking);
        $this->db->where('a.jns_trans', '144');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_handling($id_booking){
        $this->db->select('a.id_booking, b.nama as nama_affiliate, a.invoice, a.id_user_affiliate as id_affiliate, a.tgl_daftar, a.id_schedule, a.id_product, a.category, a.id_productprice, a.embarkasi');
        $this->db->from('booking AS a');
        $this->db->join('affiliate AS b', 'b.id_user = a.id_user_affiliate', 'left');
        $this->db->where('a.id_booking', $id_booking);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function search_affiliate($keyword){
        $this->db->select('id_user, nama, norek, namarek, bank');
        $this->db->from('affiliate');
        $this->db->where('nama LIKE ', '%'.$keyword.'%');
        $this->db->or_where('id_user LIKE '. '\'%'.$keyword.'%\'');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function simpan_klaim_handling($arr_data){
        // 1. Simpan ke tabel claim_additional_cost
        $data_claim = array(
            'kd_trans' => '',
            'jns_trans' => '144',
            'id_booking' => $arr_data['id_booking'],
            'id_user' => $arr_data['id_user'],
            'invoice' => $arr_data['invoice'],
            'dana_bank' => $arr_data['dana_bank'],
            'transfer_date' => $arr_data['transfer_date'],
            'bank_transfer' => $arr_data['bank_transfer'],
            'nama_rek' => $arr_data['nama_rek'],
            'no_rek' => $arr_data['no_rek'],
            'nominal' => str_replace(',', '', $arr_data['nominal']),
            'keterangan' => $arr_data['keterangan'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'update_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'update_by' => $this->session->userdata('id_user'),
            'status' => 1,
        );
        $this->db->insert($this->_table, $data_claim);
        $id_claim_additional_cost = $this->db->insert_id();
        // 2. Update status di table additional cost
        $data_cost = array(
            'status' => '4',
            'id_claim_additional_cost' => $id_claim_additional_cost,
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user'),
        );
        $this->db->update('additional_cost', $data_cost, 
            array('id_booking'=>$arr_data['id_booking'], 'jns_trans'=>'144', 'status'=>'1', 'status_manifest'=>'1'));
    }
}
