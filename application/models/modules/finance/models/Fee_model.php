<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fee_model extends CI_Model{
   
   
    private $_table="fee";
    private $_primary="id_fee";


    
     public function save_fee($data){
        

      $sql  = "INSERT INTO historis_fee
               SELECT NULL, id_fee, id_registrasi, id_booking, invoice, id_affiliate, id_schedule, id_product, fee, create_date, update_date, create_by, update_by,`status` 
               FROM fee 
               WHERE id_registrasi IN ($data)";
      $insert_data = $this->db->query($sql);
      if($insert_data){
        $sql = "UPDATE FEE SET STATUS = '2' WHERE id_registrasi IN ($data)";
        $update_data = $this->db->query($sql);
        return $update_data;
      }
      return false;
    }

    public function save_freehemat($data){
        

      $sql  = "INSERT INTO historis_fee
               SELECT NULL, id_fee, id_registrasi, id_booking, invoice, id_affiliate, id_schedule, id_product, fee, create_date, update_date, create_by, update_by,`status` 
               FROM fee 
               WHERE id_registrasi IN ($data)";
      $insert_data = $this->db->query($sql);
      if($insert_data){
        $sql = "UPDATE FEE SET STATUS = '2' WHERE id_registrasi IN ($data)";
        $update_data = $this->db->query($sql);
        return $update_data;
      }
      return false;
    }

     public function save_freetunai($data){
        

      $sql  = "INSERT INTO historis_fee
               SELECT NULL, id_fee, id_registrasi, id_booking, invoice, id_affiliate, id_schedule, id_product, fee, create_date, update_date, create_by, update_by,`status` 
               FROM fee 
               WHERE id_registrasi IN ($data)";
      $insert_data = $this->db->query($sql);
      if($insert_data){
        $sql = "UPDATE FEE SET STATUS = '2' WHERE id_registrasi IN ($data)";
        $update_data = $this->db->query($sql);
        return $update_data;
      }
      return false;
    }
   

    public function get_pic($id_affiliate,$id_product=''){
      
        $sql ="SELECT * from view_detail_fee WHERE  id_affiliate  = ? and id_product = ? 
              ";
        return $this->db->query($sql,array($id_affiliate,$id_product))->result_array();    

    }

    // public function get_pic_20_free1($id_affiliate,$id_product=''){
    
    //     $sql ="SELECT * from view_detail_fee WHERE  id_affiliate  = ? and id_product = 5
    //           ";
    //     return $this->db->query($sql,array($id_affiliate,$id_product))->result_array();    

    // }

    public function get_pic_affiliate($id_affiliate){
 

         $sql = "SELECT * from view_fee where id_affiliate = {$id_affiliate}";
        return $this->db->query($sql)->row_array();  
    }
    
    public function get_data($offset,$limit,$q=''){
    

          $sql = " SELECT * from view_fee";
        
        if($q){
            
            $sql .=" AND id_affiliate LIKE '%{$q}%' 
            		OR nama_affiliate LIKE '%{$q}%'
            		OR product LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_affiliate DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }



 function searchAdvance($from , $to, $id_product, $id_affiliate){
       
       $form = trim($from);
       $to = trim($to);
       $id_product = trim($id_product);
      $id_affiliate = trim($id_affiliate);
      $condition = "create_date  between '$from' And '$to' And id_product='$id_product' And id_affiliate ='$id_affiliate' ";

       $sql = "SELECT * from view_detail_fee WHERE   $condition limit 20";        
       $query = $this->db->query($sql);
       if(empty($query->result())){
           echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! invalid date option</h2></td></tr>';
           exit;
       }else{ 
       // if(!empty($form) AND empty($to) AND empty($id_product)){
       // $condition = "='$form'";
       // }
       
      
       
       $sno = 1;
       foreach ($query->result() as $row)
       {    
           // $created = strtotime($row->created);
           echo'<tr><td> <input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="'.$row->id_registrasi.'"</td>
           <td>'.$sno.'</td>
                  <td>'.$row->invoice.'</td>
                  <td>'.$row->id_jamaah.'</td>
                  <td>'.$row->nama.'</td>
                  <td>'.$row->product.'</td>
                  <td>'.$row->fee.'</td>
                  <td>'.$row->keterangan.'</td>
                </td></tr>';
           $sno = $sno+1; 
       }
     }
   }


    function searchAdvanceTunai($from , $to, $id_product, $id_affiliate){
       
       $form = trim($from);
       $to = trim($to);
       $id_product = trim($id_product);
      $id_affiliate = trim($id_affiliate);
      $condition = "create_date between '$from' And '$to' And id_product='$id_product' And id_affiliate ='$id_affiliate' ";

       $sql = "SELECT * from view_detail_fee WHERE   $condition limit 15 ";        
       $query = $this->db->query($sql);
       if(empty($query->result())){
           echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! invalid date option</h2></td></tr>';
           exit;
       }else{ 
       // if(!empty($form) AND empty($to) AND empty($id_product)){
       // $condition = "='$form'";
       // }
       
      
       
       $sno = 1;
       foreach ($query->result() as $row)
       {    
           // $created = strtotime($row->created);
            echo'<tr><td> <input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="<?php '.$row->id_registrasi.' ?>"</td>
           <td>'.$sno.'</td>
                  <td>'.$row->invoice.'</td>
                  <td>'.$row->id_jamaah.'</td>
                  <td>'.$row->nama.'</td>
                  <td>'.$row->product.'</td>
                  <td>'.$row->fee.'</td>
                  <td>'.$row->keterangan.'</td>
                </td></tr>';
           $sno = $sno+1; 
       }
     }
   }


     public function delete($id_product){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_product'=>$id_product));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
   
    
    
}
