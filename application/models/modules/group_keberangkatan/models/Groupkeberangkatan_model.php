<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Groupkeberangkatan_model extends CI_Model {

    private $_table = "group_manifest";
    private $_primary = "id_grp_keberangkatan";

    /**
     * Get Data Group Keberangkatan/Manifest
     * @param type $offset
     * @param type $limit
     * @param type $q
     * @return type
     */
    public function get_data($offset, $limit, $q = '') {
        $sql = " SELECT a.* "
            . "FROM group_manifest as a";
        if ($q) {
            $sql .=" AND a.kd_grp_keberangkatan LIKE '%{$q}%'
                    OR a.nama LIKE '%{$q}%'
                    OR a.keterangan LIKE '%{$q}%'
                    OR b.date_schedule LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY date_schedule DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        $sql .=" LIMIT {$offset},{$limit} ";
        $ret['data'] = $this->db->query($sql)->result();
        return $ret;
    }
    
    /**
     * Save Group Keberangkatan/Manifest
     * @param type $arr
     * @return boolean
     */
    public function save($arr) {
        $this->db->insert('group_manifest', $arr);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_complete();
            return true;
        }
    }
   
    /**
     * Update Group Keberangkatan/Manifest
     * @param type $data
     * @return type
     */
    public function update($data) {
        $embarkasi = $this->db->get_where('embarkasi', array('id_embarkasi'=>$data['embarkasi']))->row_array();
        $arr = array(
            'id_embarkasi'=>$data['embarkasi'],
            'embarkasi' => $embarkasi['departure'],
            'date_schedule' => date('Y-m-d', strtotime($data['tanggal'])),
            'nama' => $data['nama'],
            'keterangan' => $data['keterangan'],
            'jumlah' => $data['jumlah'],
        );
        return $this->db->update($this->_table, $arr, array('id_grp_keberangkatan' => $data['id_grp_keberangkatan']));
    }

    public function get_data_manifest_by_tanggal_embarkasi($tanggal, $id_embarkasi, $id_group_keberangkatan){
        $sql = "SELECT rj.id_registrasi, rj.id_jamaah, rj.nama, rj.kelamin, p.nama as nama_product, "
            . "k.nama as nama_kabupaten, ru.keterangan, gm.nama as nama_group, m.id_grp_keberangkatan  "
            . "FROM registrasi_jamaah AS rj "
            . "LEFT JOIN schedule AS s ON (rj.id_schedule = s.id_schedule) "
            . "LEFT JOIN product AS p On (p.id_product = rj.id_product) "
            . "LEFT JOIN manifest AS m On (m.id_registrasi = rj.id_registrasi) "
            . "LEFT JOIN group_manifest AS gm ON (gm.id_grp_keberangkatan = m.id_grp_keberangkatan) "
            . "LEFT JOIN wilayah_kabupaten AS k ON (k.kabupaten_id = rj.kabupaten_id) "
            . "LEFT JOIN rentang_umur AS ru On (ru.id_rentang_umur = rj.rentang_umur) "
            . "WHERE s.embarkasi  = {$id_embarkasi} AND s.date_schedule = '{$tanggal}' "
            . "AND (m.id_grp_keberangkatan = {$id_group_keberangkatan} OR m.id_grp_keberangkatan IS NULL ) "
            . "ORDER BY rj.nama";
        return $this->db->query($sql)->result_array();
    }
    
    public function update_manifest_group($id_grp_keberangkatan, $id_registrasi) {
        $arr = array(
            'id_grp_keberangkatan' => $id_grp_keberangkatan
        );
        return $this->db->update('manifest', $arr, array('id_registrasi' => $id_registrasi));
    }
    
    
    
    
    
    
    
    
    public function get_pic_group_keberangkatan($id_schedule) {
        $sql = "SELECT * from refrensi_group_keberangkatan WHERE  id_schedule  = {$id_schedule} ";
        return $this->db->query($sql)->row_array();
    }

    

    function get_data_schedule($schedule = '') {
        $this->db->where("schedule", $schedule);
        return $this->db->get("refrensi_group_keberangkatan");
    }

    public function delete_by_id($id_grp_keberangkatan) {
        $this->db->where('id_grp_keberangkatan', $id_grp_keberangkatan);
        $this->db->delete($this->_table);
    }
    
    
    
    public function reset_manifest_by_group_keberangkatan($id_grp_keberangkatan){
         if($this->db->simple_query("UPDATE manifest SET id_grp_keberangkatan = NULL "
                . " WHERE id_grp_keberangkatan = {$id_grp_keberangkatan} ")){
             return TRUE;
         }else{
             return FALSE;
         }
    }
    
    public function get_count_group($id_grp_keberangkatan){
        $hasil = $this->db->query("SELECT COUNT(*) as jumlah FROM manifest WHERE id_grp_keberangkatan = {$id_grp_keberangkatan} ")->row_array();
        return $hasil;
    }
    

}
