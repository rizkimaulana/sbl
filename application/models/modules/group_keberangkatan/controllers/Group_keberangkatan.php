<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Group_keberangkatan extends CI_Controller {
    var $folder = "group_keberangkatan";
    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(GROUP_KEBERANGKATAN, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'manifest');
        $this->load->model('groupkeberangkatan_model');
    }

    /**
     * Re-direct page
     */
    public function index() {
        $this->template->load('template/template', $this->folder . '/group_keberangkatan');
    }
    
    /**
     * Get Data Group Keberangkatan/Manifest
     */
    public function get_data() {
        $limit = $this->config->item('limit');
        $offset = $this->uri->segment(3, 0);
        $q = isset($_POST['q']) ? $_POST['q'] : '';
        $data = $this->groupkeberangkatan_model->get_data($offset, $limit, $q);
        $rows = $paging = '';
        $total = $data['total'];
        if ($data['data']) {
            $i = $offset + 1;
            $j = 1;
            foreach ($data['data'] as $r) {
                $hasil = $this->groupkeberangkatan_model->get_count_group($r->id_grp_keberangkatan);
                $rows .='<tr>';
                $rows .='<td width="4%" class="text-right">' . $i . '</td>';
                $rows .='<td width="10%" class="text-center">' . $r->kd_grp_keberangkatan . '</td>';
                $rows .='<td width="10%">' . $r->nama . '</td>';
                $rows .='<td width="10%">' . $r->embarkasi . '</td>';
                $rows .='<td width="10%" class="text-center">' . date('d M Y', strtotime($r->date_schedule)). '</td>';
                $rows .='<td width="10%" class="text-right">' . $r->jumlah . '</td>';
                $rows .='<td width="10%" class="text-center"> <a title="Set Jamaah" class="btn btn-sm btn-success" href="' . base_url() . 'group_keberangkatan/set_jamaah/'. $r->id_grp_keberangkatan .'">
                        <i class="fa fa-pencil"></i> ' . $hasil['jumlah'] . ' </a></td>';
                $rows .='<td width="20%" align="center">';
                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="' . base_url() . 'group_keberangkatan/edit/' . $r->id_grp_keberangkatan. '">
                        <i class="fa fa-pencil"></i> Edit </a> ';
                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_group_keberangkatan(' . "'" . $r->id_grp_keberangkatan . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
                $rows .=' ';
                $rows .='</td>';
                $rows .='</tr>';
                ++$i;
                ++$j;
            }

            $paging .= '<li><span class="page-info">Displaying ' . ($j - 1) . ' Of ' . $total . ' items</span></i></li>';
            $paging .= $this->_paging($total, $limit);
        } else {
            $rows .='<tr>';
            $rows .='<td colspan="6">No Data</td>';
            $rows .='</tr>';
        }

        echo json_encode(array('rows' => $rows, 'total' => $total, 'paging' => $paging));
    }

    private function _paging($total, $limit) {
        $config = array(
            'base_url' => base_url() . 'group_keberangkatan/get_data/',
            'total_rows' => $total,
            'per_page' => $limit,
            'uri_segment' => 3
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
    
    
    /**
     * View : Form Add Group Keberangkatan
     */
    public function add() {
        if (!$this->general->privilege_check(GROUP_KEBERANGKATAN, 'add'))
            $this->general->no_access();
        $list_embarkasi = $this->db->get('embarkasi')->result_array();
        $data = array('list_embarkasi' => $list_embarkasi);
        $this->template->load('template/template', $this->folder . '/add', $data);
    }
    
    /**
     * Call Function : Generate Tanggal Departure based on id embarkasi
     * @param type $id_embarkasi
     */
    function add_ajax_tanggal($id_embarkasi) {
        $query = $this->db->group_by('date_schedule')->order_by('date_schedule','ASC')->get_where('schedule', array('embarkasi' => $id_embarkasi, 'date_schedule > '=>date('Y-m-d')));
        $data = "";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->date_schedule."'>".date('d M Y', strtotime($value->date_schedule))."</option>";
        }
        echo $data;
    }

    /**
     * Function : Save Group Keberangkatan
     * @throws Exception
     */
    public function save() {
        $data = $this->input->post(null, true);
        try {
            $embarkasi = $this->db->get_where('embarkasi', array('id_embarkasi'=>$data['embarkasi']))->row_array();
            // NEXT : should be validate on view
            if(empty($embarkasi)){
                throw new Exception('Data Embarkasi Tidak Boleh Kosong');
            }
            if(empty($data['tanggal'])){
                throw new Exception('Data tanggal Tidak Boleh Kosong');
            }
            $arr = array(
                'kd_grp_keberangkatan' => $data['embarkasi'],    // NEXT : should be generate kode here
                'id_embarkasi' => $data['embarkasi'],
                'embarkasi' => $embarkasi['departure'],
                'date_schedule' => date('Y-m-d', strtotime($data['tanggal'])),
                'nama' => $data['nama'],
                'keterangan' => $data['keterangan'],
                'jumlah' => $data['jumlah'],
            );
            if(!$this->groupkeberangkatan_model->save($arr)){
                throw new Exception('Ada kesalahan ketika simpan data');
            }
            $this->session->set_flashdata('info', "Data Berhasil Disimpan!");
            redirect('group_keberangkatan');
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->session->set_flashdata('info', $msg);
            redirect('group_keberangkatan/add');
        }
    }
    
    /**
     * View : Form Edit Group Keberangkatan
     */
    public function edit() {
        if (!$this->general->privilege_check(GROUP_KEBERANGKATAN, 'edit'))
            $this->general->no_access();
        $id_grp_keberangkatan = $this->uri->segment(3);
        $list_embarkasi = $this->db->get('embarkasi')->result_array();
        $group_manifest = $this->db->get_where('group_manifest', array('id_grp_keberangkatan'=>$id_grp_keberangkatan))->row_array();
        if (!$group_manifest){
            show_404();
        }
        $data = array(
            'list_embarkasi' => $list_embarkasi,
            'group_manifest'=>$group_manifest
        );
        $this->template->load('template/template', $this->folder . '/edit',$data);
    }
    
    /**
     * Function : Update Group Keberangkatan
     * @throws Exception
     */
    public function update() {
        $data = $this->input->post(null, true);
        try {
            if(!$this->groupkeberangkatan_model->update($data)){
                throw new Exception('Ada kesalahan ketika simpan data');
            }
            redirect('group_keberangkatan');
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->session->set_flashdata('info', $msg);
            redirect('group_keberangkatan/edit/'.$data);
        }
    }
    
    /**
     * View : Form Set Jamaah
     */
    public function set_jamaah(){
        if (!$this->general->privilege_check(GROUP_KEBERANGKATAN, 'edit'))
            $this->general->no_access();
        $id_group_keberangkatan = $this->uri->segment(3);
        $group_manifest = $this->db->get_where('group_manifest', array('id_grp_keberangkatan' => $id_group_keberangkatan))->row_array();
        if (!$group_manifest) show_404();
        $list_manifest = $this->groupkeberangkatan_model->get_data_manifest_by_tanggal_embarkasi($group_manifest['date_schedule'], $group_manifest['id_embarkasi'], $id_group_keberangkatan);
        $data = array(
            'group_manifest' => $group_manifest,
            'list_manifest' => $list_manifest,
        );
        $this->template->load('template/template', $this->folder . '/set_jamaah', $data);
    }
    
    
    
    
    /**
     * Function : Simpan Set Jamaah
     * @throws Exception
     */
    function save_set_jamaah(){
        $data = $this->input->post(null, true);
        try {
            if(empty($data['is_check'])){
                throw new Exception('Data tidak ada!');
            }
            // 1. Reset tabel manifest
            $hasil = $this->groupkeberangkatan_model->reset_manifest_by_group_keberangkatan($data['id_grp_keberangkatan']);
            // 2. Update tabel manifest
            for ($index = 0; $index < count($data['is_check']); $index++) {
                if(!empty($data['is_check'][$index])){
                    if(!$this->groupkeberangkatan_model->update_manifest_group($data['id_grp_keberangkatan'], $data['id_registrasi'][$index])){
                        throw new Exception('Ada kesalahan ketika update manifest');
                    }
                }
            }
            $this->session->set_flashdata('info', "Data Berhasil Disimpan!");
            redirect('group_keberangkatan');
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->session->set_flashdata('info', $msg);
        }
    }
    

    
    

    

    public function ajax_delete($id_grp_keberangkatan) {
        if (!$this->general->privilege_check(GROUP_KEBERANGKATAN, 'remove'))
            $this->general->no_access();
        $send = $this->groupkeberangkatan_model->delete_by_id($id_grp_keberangkatan);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('group_keberangkatan');
    }

    function get_data_schedule_() {
        $schedule = $this->input->post('schedule');
        $select_schedule = $this->groupkeberangkatan_model->get_data_schedule($schedule);
        if ($select_schedule->num_rows() > 0) {
            $select_schedule = $select_schedule->row_array();
            echo $select_schedule['seats'];
        }
    }
    
    
    

}
