<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feekuotaadmin_model extends CI_Model{
    
    private $_table="claim_feekuota";
    private $_primary="id_claim_feekuota";

    private $_kuota_booking="kuota_booking";
    private $_id_booking="id_booking";

        public function save($data){
        

        $arr = array(
          'id_booking' => $data['id_booking'],
           'invoice' => $data['invoice'],
           'id_sponsor' => $data['sponsor'],
           
            'transfer_date' => $data['transfer_date'],
            'dana_bank' => $data['dana_bank'],
            'bank_transfer' => $data['bank_transfer'],
            'nama_rek' => $data['nama_rek'],
            'no_rek' => $data['no_rek'],
            'nominal' => $data['fee'],
            'keterangan' => $data['keterangan'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
        );       
        
        $this->db->trans_begin(); //transaction initialize
        $this->db->insert($this->_table,$arr);


      $arr = array(
           
            'status_fee'=>3,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );     

          $this->db->update('kuota_booking',$arr,array('id_booking_kuota'=>$data['id_booking'])); 

       $arr = array(
        
            'status_fee'=>3,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );     

          $this->db->update('jamaah_kuota',$arr,array('id_booking'=>$data['id_booking'])); 
       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }
    public function get_data($offset,$limit,$q=''){
    

          $sql = " SELECT * FROM grouping_jamaah_kuota_admin where 1=1
          ";
        
        if($q){
            
            $sql .=" AND invoice LIKE '%{$q}%' 
            	    	OR sponsor LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR tgl_pembelian LIKE '%{$q}%'
                    ";
        }
        // $sql .=" ORDER BY update_date DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   
     public function get_pic($id_booking,$sponsor=''){
      
        $sql ="SELECT * from jamaah_kuota WHERE  id_booking  = ? and sponsor = ? and status_fee='2'
              ";
        return $this->db->query($sql,array($id_booking,$sponsor))->result_array();    

    }

   

    public function get_pic_kouta($id_booking,$sponsor=''){
 

         $sql = "SELECT * from grouping_jamaah_kuota_admin where id_booking_kuota = '$id_booking' and sponsor = '$sponsor'";
        return $this->db->query($sql)->row_array();  
    }
    
}
