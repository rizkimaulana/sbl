<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aktivasimanual_model extends CI_Model{
   
    var $table = 'aktivasi_jamaah_manual';
    var $column_order = array(null, 'id_booking','id_affiliate','invoice','jumlah_jamaah','pre_double','pre_triple','id_schedule','id_product','create_date','category_id','tgl_daftar','paket','tgl_keberangkatan','departure','bulan_menunggu','schedule','bulan_keberangkatan','user_create','create_by','update_date'); //set column field database for datatable orderable
    var $column_search = array('id_booking','id_affiliate','invoice','jumlah_jamaah','pre_double','pre_triple','id_schedule','id_product','create_date','category_id','tgl_daftar','paket','tgl_keberangkatan','departure','bulan_menunggu','schedule','bulan_keberangkatan','user_create','create_by','update_date'); //set column field database for datatable searchable 
    var $order = array('update_date' => 'asc'); 

   private $db2;
    private $_table="aktivasi";
    private $_primary="id_aktivasi";

    private $_claim_feefree="claim_feefree";
    private $_id_claim_feefree="id_claim_feefree";
   
    private $_table_aktivasi_jamaah="aktivasi_jamaah";
    private $id_aktivasi_jamaah="id_aktivasi_jamaah";
   public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }

public function save($data){  

        $p_id_booking = $this->input->post('id_booking');
        $p_id_product = $this->input->post('id_product');
        $p_id_schedule = $this->input->post('id_schedule');
        $p_id_affiliate = $this->input->post('id_user_affiliate');
        $p_pay_method = $this->input->post('select_pay_method');
        $p_keterangan = mysqli_real_escape_string($this->input->post('ket_pembayaran'));
        $p_pic1 = isset($data['pic1']) ? $data['pic1']: '';
        $p_pic2 = isset($data['pic2']) ? $data['pic2']: '';
        $p_pic3 = isset($data['pic3']) ? $data['pic3']: '';
        $p_nominal_pembayara = $this->input->post('nominal_pembayaran');
        $p_bank = $this->input->post('select_bank');
        $p_update_by = $this->session->userdata('id_user');
        $p_create_by = $this->session->userdata('id_user');
        $p_payment_date = $this->input->post('payment_date');
        $p_status_akomodasi = $this->input->post('status_akomodasi');
        $p_status_muhrim = $this->input->post('status_muhrim');
        $p_status_handling = $this->input->post('status_handling');
        $p_status_fee = $this->input->post('status_fee_jamaah');
        $p_invoice = $this->input->post('invoice');

        $sql_query=$this->db->query("call aktivasi_jamaah('".$p_id_booking."','".$p_id_product ."','".$p_id_schedule ."','".$p_id_affiliate."','".$p_pay_method ."','".$p_keterangan ."','".$p_pic1."','".$p_pic2."','".$p_pic3."','".$p_nominal_pembayara."','".$p_bank."','".$p_update_by."','".$p_create_by."','".$p_payment_date."','".$p_status_akomodasi."','".$p_status_muhrim."','".$p_status_handling."','".$p_status_fee."','".$p_invoice."')");



//            $arr = array(
//             'id_booking'=> $data['id_booking'],
//             'id_schedule'=> $data['id_schedule'],
//             'id_product'=> $data['id_product'],
//             'id_user_affiliate'=> $data['id_user_affiliate'],
//             'pay_method'=> $data['select_pay_method'],
//             'keterangan' => $data['ket_pembayaran'],
//             'bank' => $data['select_bank'],
//             'pic1' => isset($data['pic1']) ? $data['pic1']: '',
//             'pic2' => isset($data['pic2']) ? $data['pic2']: '',
//             'pic3' => isset($data['pic2']) ? $data['pic3']: '',
//             'payment_date' => $data['payment_date'],
//             'create_date' => date('Y-m-d H:i:s'),
//             'status_muhrim'=> $data['status_muhrim'],
//             'status_akomodasi'=> $data['status_akomodasi'],
//             'status_handling'=> $data['status_handling'],
//             'create_by'=>$this->session->userdata('id_user')
//         );       
// 		    $this->db->trans_begin(); //transaction initialize
//         $this->db->insert($this->_table,$arr);


        
//         $arr = array(

//             'status'=>1,
//             'update_by'=>$this->session->userdata('id_user'),
//             'update_date' => date('Y-m-d H:i:s'),

//         );       
//         $this->db->update('manifest',$arr,array('id_booking'=>$data['id_booking'])); 

//          $arr = array(

//             'status'=>1,
//             'payment_date' => $data['payment_date'],
//             'update_by'=>$this->session->userdata('id_user'),
//             'update_date' => date('Y-m-d H:i:s'),

//         );       
//         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 

//         $arr = array(


//             'status'=>1,
//             'payment_date' => $data['payment_date'],
//             'update_by'=>$this->session->userdata('id_user'),
//             // 'cashback'=> $data['cashback'],
//             // 'status_fee'=> $data['status_fee'],
//             // 'status_akomodasi'=> $data['status_akomodasi'],
//             // 'status_muhrim'=> $data['status_muhrim'],
//             // 'status_handling'=> $data['status_handling'],
//             'update_date' => date('Y-m-d H:i:s'),

//         );  
//         $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));  
    
//         $arr = array(
//             'payment_date' => $data['payment_date'],
//             'status' => 1,
//             'status_fee'=> $data['status_fee'],
//             'status_free'=> 3,
//             'status_fee_sponsor'=>2,
//             'update_by'=>$this->session->userdata('id_user'),
//             'update_date' => date('Y-m-d H:i:s'),
            
//         );      
//        $this->db->update('fee',$arr,array('id_booking'=>$data['id_booking'])); 



//          $arr = array(
//              'payment_date' => $data['payment_date'],
//              'status'=>1,
//              'update_by'=>$this->session->userdata('id_user'),
//             'update_date' => date('Y-m-d H:i:s'),
//         );  
             
//           $this->db->update('list_gaji',$arr,array('id_booking'=>$data['id_booking']));  
    
        
//          $this->db->delete('transaksi',array('id_booking'=>$id_booking));
      
//         $arr = array(
                              
            
//             'update_by'=>$this->session->userdata('id_user'),
//             'update_date' => date('Y-m-d H:i:s'),
//             'status'=>1
//         );       
//          $this->db->update('pengiriman',$arr,array('id_booking'=>$data['id_booking']));  


//        $arr = array(
         
//           'status' =>1
//           );
     
//         $this->db->update('visa',$arr,array('id_booking'=>$data['id_booking']));

//       $arr = array(
         
//           'status_pelunasan' =>1
//           );
     
//         $this->db->update('member_mgm',$arr,array('id_mgm'=>$data['id_mgm']));

      

//        $status_room= $this->input->post('status_room');


//       if ($status_room == 3){ 
         
//           $arr = array(

//             'status' => 1,
//             'status_room_order' => $data['status_room'],
//             'update_date' => date('Y-m-d H:i:s'),
//             'update_by'=>$this->session->userdata('id_user'),
//         );       
              
//         $this->db->update('room_order',$arr,array('id_booking'=>$data['id_booking']));

//         $arr = array(

//             'status' => 1,
//             'status_room_order' => $data['status_room'],
//             'update_date' => date('Y-m-d H:i:s'),
//             'update_by'=>$this->session->userdata('id_user'),
//         );       
              
//         $this->db->update('list_order_room',$arr,array('id_booking'=>$data['id_booking']));
          
//          $arr = array(
         
//           'status_pelunasan' =>1
//           );
     
//         $this->db->update('member_mgm',$arr,array('id_mgm'=>$data['id_mgm']));


//         $sql = "SELECT COUNT(*) as jml_double  From list_order_room 
//           WHERE  id_booking = '".$id_booking."'  and room_type='2'";
//         $query = $this->db->query($sql)->result_array();

//         // if(count($query) > 0){
//           foreach($query as $key=>$value){
//             $jml_double = $value['jml_double'];
//             if($jml_double > 0){
//               $query = $this->db->query("SELECT GROUP_CONCAT(id_jamaah) as id_jamaah_double From list_order_room 
//               WHERE  id_booking = '".$id_booking."' and room_type='2'");
//               foreach ($query->result_array() as $row)
//               {
//                 $id_jamaah_double = $row['id_jamaah_double'];
                  
//               }

//                $sql = "UPDATE registrasi_jamaah SET room_type = '2'  WHERE id_jamaah  IN($id_jamaah_double)";
//               $update_data = $this->db->query($sql);
//               $update_data;

//                // print_r($id_jamaah_double);
//           }
//         }
//       // }


//          $sql = "SELECT COUNT(*) as jml_triple  From list_order_room 
//           WHERE  id_booking = '".$id_booking."'  and room_type='3'";
//         $query = $this->db->query($sql)->result_array();

//         // if(count($query) > 0){
//           foreach($query as $key=>$value){
//             $jml_triple = $value['jml_triple'];
//             if( $jml_triple > 0){
//              $query2 = $this->db->query("SELECT GROUP_CONCAT(id_jamaah) as id_jamaah_triple From list_order_room 
//             WHERE  id_booking = '".$id_booking."' and room_type='3'");
//             foreach ($query2->result_array() as $row)
//             {
//               $id_jamaah_triple = $row['id_jamaah_triple'];
                
//             }
//              $sql = "UPDATE registrasi_jamaah SET room_type = '3'  WHERE id_jamaah  IN($id_jamaah_triple)";
//             $update_data = $this->db->query($sql);
//             $update_data;
//           }
//         }
//        // }
//       }else{

//          $arr = array(

//             'status' => 1,
//             'status_room_order' => 1,
//             'update_date' => date('Y-m-d H:i:s'),
//             'update_by'=>$this->session->userdata('id_user'),
//         );       
              
//         $this->db->update('room_order',$arr,array('id_booking'=>$data['id_booking']));

//         $arr = array(

//             'status' => 1,
//             'status_room_order' => 1,
//             'update_date' => date('Y-m-d H:i:s'),
//             'update_by'=>$this->session->userdata('id_user'),
//         );       
              
//         $this->db->update('list_order_room',$arr,array('id_booking'=>$data['id_booking']));
//       }

//       $tipe_jamaah= $this->input->post('tipe_jamaah');
// if ($tipe_jamaah!='9' && $tipe_jamaah!='10'){ 
//       $id_schedule= $this->input->post('id_schedule');
//         $sql = "SELECT seats  from schedule
//           WHERE  id_schedule = '".$id_schedule."'";
//         $query = $this->db->query($sql)->result_array();

//          foreach($query as $key=>$value){
//             $seats = $value['seats'];
           
//           }


//         $sql = "SELECT COUNT(*) as jumlah_jamaah from registrasi_jamaah
//           WHERE  id_booking = '".$id_booking."'";
//         $query = $this->db->query($sql)->result_array();


//           foreach($query as $key=>$value){
//             $jumlah_jamaah = $value['jumlah_jamaah'];
//             $total_jamaah =$seats - $jumlah_jamaah;
//             if($jumlah_jamaah > 0){
//               // print_r($total_jamaah);
//             $arr = array(
                
                         
//                     'seats' => ($seats - $jumlah_jamaah),  
//                 );       
//                  $this->db->update('schedule',$arr,array('id_schedule'=>$data['id_schedule']));
       
//             }
//           }
// }
 $sql = "SELECT * from affiliate
          WHERE  id_user = '".$data['id_user_affiliate']."'";
        $query = $this->db->query($sql)->result_array();
 foreach($query as $key=>$value){
            $notlp = $value['telp'];
            $nama =$value['nama'];
 }

  $return = '0';
      $api_element = '/api/web/send/';

      $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=".$notlp."&message=".$this->input->post('id_user_affiliate')."+".str_replace(' ', '+', $nama)."+Terimakasih+anda+telah+melakukan+aktifasi+Jamaah+Pembayaran+senilai+".$this->input->post('Total_Bayar')."+inv:+".$this->input->post('invoice')."+bulan+Keberangkatan+".str_replace(' ', '+', $this->input->post('bulan_berangkat'))."+melalaui(aktivasi+manual)+(office.sbl.co.id)";
      $smsgatewaydata = $api_params;

      $url = $smsgatewaydata;

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, false);

      curl_setopt($ch, CURLOPT_URL, $url);

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $output = curl_exec($ch);

      curl_close($ch);

      if(!$output){ $output = file_get_contents($smsgatewaydata); }

      if($return == '1'){
       return $output; 
       }
       // else{ echo "Sent"; }


        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
	
    

    }



    public function save_perjamaah($data){
       
      
        $id_registrasi = $this->input->post('id_registrasi');
        $id_jamaah = $this->input->post('id_jamaah');
        $id_booking= $this->input->post('id_booking');
        $id_schedule= $this->input->post('id_schedule');
        
        $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_user_affiliate'=> $data['id_affiliate'],
            'id_product'=> $data['id_product'],
            'id_registrasi'=> $data['id_registrasi'],
            'id_jamaah'=> $data['id_jamaah'],
            // 'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            'pic1' => isset($data['pic1']) ? $data['pic1']: '',
            'pic2' => isset($data['pic2']) ? $data['pic2']: '',
            'pic3' => isset($data['pic2']) ? $data['pic3']: '',
            // 'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'pay_method'=> $data['select_pay_method'],
            'status_muhrim'=> $data['status_muhrim'],
            'status_akomodasi'=> $data['status_akomodasi'],
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table_aktivasi_jamaah,$arr);
        // $id_aktivasi_jamaah =  $this->db->insert_id(); //get last insert ID
        
        // $this->db->insert('aktivasi_jamaah',$arr);


        
         $arr = array(
          'status'=>1,
          // 'id_sahabat'=> $data['id_sahabat'],
         
          'dp_angsuran' => $data['dp_angsuran'],
          'angsuran' => $data['angsuran'],
          'jml_angsuran' => $data['angsuran_bulanan'],
          'payment_date' => $data['payment_date'],
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  
    

                  
        $arr = array(

            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),

        );       
        $this->db->update('manifest',$arr,array('id_jamaah'=>$data['id_jamaah'])); 

        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            // 'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            // 'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }



        $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> $data['status_fee'],
             // 'status_fee'=> 2,
            'status_free'=> 3,
			      'status_fee_sponsor'=>2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  

          $this->db->update('fee',$arr,array('id_registrasi'=>$data['id_registrasi']));  


        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));
        
   
        $arr = array(

            
            'status'=>1,
             'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi'])); 
       
         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      


       $arr = array(
         
          'status' =>1
          );
     
        $this->db->update('visa',$arr,array('id_registrasi'=>$data['id_registrasi']));

        
        $sql = "SELECT seats from schedule
          WHERE  id_schedule = '".$id_schedule."'";
        $query = $this->db->query($sql)->result_array();

         foreach($query as $key=>$value){
            $seats = $value['seats'];
           
          }


       $arr = array(
                      
          'seats' => ($seats - 1),  
      );       
       $this->db->update('schedule',$arr,array('id_schedule'=>$data['id_schedule']));

       $tipe_jamaah = $this->input->post('tipe_jamaah');
      $MyCheckBox = $this->input->post('MyCheckBox');
      $status_fee = $this->input->post('status_fee');

       if((int)$MyCheckBox == 1 ){
        // print_r("expression");

         $id_affiliate_type = $this->input->post('id_affiliate_type');
        $sql = "SELECT fee_pindah_paket from affiliate_type where id_affiliate_type ='".$id_affiliate_type."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $fee_pindah_paket = $value['fee_pindah_paket'];

       }

       if ($status_fee == 4){

         $arr = array(
          'id_sahabat' => $data['id_sahabat'],
          'dp_angsuran' => $data['dp_angsuran'],
          'angsuran' => $data['angsuran'],
          'jml_angsuran' => $data['angsuran_bulanan'],
          'tipe_jamaah' => 6,
          'fee'=>$fee_pindah_paket,
           );  
        $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  

       

       }else{
            $arr = array(
          'id_sahabat' => $data['id_sahabat'],
          'dp_angsuran' => $data['dp_angsuran'],
          'angsuran' => $data['angsuran'],
          'jml_angsuran' => $data['angsuran_bulanan'],
          'tipe_jamaah' => 6,
           );  
        $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah'])); 


       }


            $arr = array(
                    'id_sahabat' =>$data['id_sahabat'],
                    'tgl_pindahpaket'=> date('Y-m-d H:i:s'),
                    'status' =>1
                    );
            $this->db->insert('pindah_paket',$arr); 

        $arr = array(
          'id_sahabat' => $data['id_sahabat'],
          'dp_angsuran' => $data['dp_angsuran'],
          'angsuran' => $data['angsuran'],
          'jml_angsuran' => $data['angsuran_bulanan'],
          'tipe_jamaah' => 6,
          
        );  
        $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  
        
         $arr = array(
            
            'fee'=>$fee_pindah_paket,
            'fee_sponsor'=>0,
            'sponsor'=>0,  
            'status_fee_sponsor'=>2,
        );  
        $this->db->update('fee',$arr,array('id_registrasi'=>$data['id_registrasi']));

         $arr = array(
          'status'=>1,
         
        );    
          $this->db2->update('t_cicilan',$arr,array('userid'=>$data['id_sahabat'])); 

         $arr = array(
          'status_cicilan'=>1,
         
        );    
          $this->db2->update('t_member',$arr,array('userid'=>$data['id_sahabat']));  
       
     
       } 


        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
       

    }

 public function get_pic($id_booking){
    
        // $sql ="SELECT * from data_jamaah_belum_aktif WHERE  id_booking  = ?
        //       ";
       $sql ="SELECT * from registrasi_jamaah WHERE  id_booking  = ? and status in('0','6')
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

 public function get_pic_jamaah($id_jamaah){
    
       
        //  $sql = "SELECT * from aktivasi_perjamaah where id_jamaah = '$id_jamaah'
        //         ";
        // return $this->db->query($sql)->row_array();   
  $stored_procedure = "call aktivasi_by_jamaah(?)";
         return $this->db->query($stored_procedure,array('id_jamaah'=>$id_jamaah))->row_array(); 

}

 public function get_pic_booking($id_booking){
 

        $stored_procedure = "call aktivasi_manual(?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking))->row_array();   
    }

 public function get_pic_harga($id_booking){


     $sql = "SELECT * from harga_perinvoice where id_booking = '$id_booking'
            ";
    return $this->db->query($sql)->row_array();  
}

 public function get_pic_harga_kamar($id_booking){


     $sql = "SELECT * from harga_kamar where id_booking = '$id_booking'
            ";
    return $this->db->query($sql)->row_array();  
}

 public function get_pic_booking_jamaah($id_booking){
   // $id_booking= $this->input->post('id_booking');

         $stored_procedure = "call aktivasi_manual(?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking))->row_array();  
    }

      



    // public function get_data($offset,$limit,$q=''){
    
    //     $id=  $this->session->userdata('id_user');
    //       $sql = " SELECT * from data_laporan_jamah_belumaktif  where 1=1 
    //                   ";
        
    //     if($q){
            
    //         $sql .=" AND affiliate LIKE '%{$q}%'
    //                 OR invoice LIKE '%{$q}%'
    //         ";
    //     }
    //     $sql .=" ORDER BY id_booking DESC ";
    //     $ret['total'] = $this->db->query($sql)->num_rows();
        
    //         $sql .=" LIMIT {$offset},{$limit} ";
        
    //     $ret['data']  = $this->db->query($sql)->result();
       
    //     return $ret;
        
    // }

    
   



    
    function get_data_jamaahnoaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }
    

     private function _get_datatables_query()
    {
        //  $status = array('1','2');
        // $id_user=  $this->session->userdata('id_user');
        // $this->db->where_in('id_affiliate', $id_user);
        //  $this->db->where_in('status', $status);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
