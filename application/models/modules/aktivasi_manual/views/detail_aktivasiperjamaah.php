 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});


      
</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_manual/save_jamaah" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah Belum Aktif</b>
                </div>
                  <div class="panel-body">                       

                  
                    <input type="hidden" name="id_schedule" value="<?php echo $jamaah['id_schedule']?>">
                   <input type="hidden" name="id_affiliate" value="<?php echo $jamaah['id_affiliate']?>">
                    <input type="hidden" name="id_affiliate_type" value="<?php echo $jamaah['id_affiliate_type']?>">
                     <input type="hidden" name="tipe_jamaah" value="<?php echo $jamaah['tipe_jamaah']?>">
                   <input type="hidden" name="id_product" value="<?php echo $jamaah['id_product']?>">
                   <input type="hidden" name="id_registrasi" value="<?php echo $jamaah['id_registrasi']?>">
                   <input type="hidden" name="id_booking" value="<?php echo $jamaah['id_booking']?>">
                   <input type="hidden" name="jumlah_jamaah" value="<?php echo $booking['jumlah_jamaah']?>">
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="#<?php echo $jamaah['invoice']?>" readonly="readonly">
  
                      </div>

                       <div class="col-sm-4">
                        <label>Paket</label> 
                        <input type="text" name="paket" class="form-control" value="<?php echo $jamaah['paket']?> dengan Waktu tunggu <?php echo $jamaah['bulan_menunggu']?> Bulan " readonly="readonly" >
                            
                      </div>
                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_affiliate" class="form-control" value="<?php echo $jamaah['id_affiliate']?>" readonly="readonly" >
                           
                      </div>
                      <div class="col-sm-4">
                        <label>Nama Affiliate</label> 
                        <input type="text" name="nama_affiliate" class="form-control" value="<?php echo $jamaah['nama_affiliate']?>" readonly="readonly" >
                            
                      </div>

                       <div class="col-sm-4">
                        <label>Nama Jamaah</label> 
                        <input type="text" name="nama" class="form-control" value="<?php echo $jamaah['nama']?>" readonly="readonly" >
                            
                      </div>
                         <div class="col-sm-4">
                        <label>ID Jamaah</label> 
                        <input type="text" name="id_jamaah" class="form-control" value="<?php echo $jamaah['id_jamaah']?>" readonly="readonly" >
                            
                      </div>
                   
                    
                      <div class="col-sm-4">
                        <label>Muhrim</label> 
                        
                        <input name="muhrim" class="form-control" value="<?php echo number_format($jamaah['muhrim'])?>" readonly="readonly">
                      </div>

                     <div class="col-sm-4">
                        <label>Akomodasi</label> 
                        
                        <input name="akomodasi" class="form-control" value="<?php echo number_format($jamaah['akomodasi'])?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                        <label>Cashback</label> 
                        
                        <input name="refund" class="form-control" value="<?php echo number_format($jamaah['refund'])?>" readonly="readonly">
                      </div>

                        <div class="col-sm-4">
                        <label>Handling</label> 
                        
                        <input name="handling" class="form-control" value="<?php echo number_format($jamaah['handling'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Fee</label> 
                        
                        <input name="fee" class="form-control" value="<?php echo number_format($jamaah['fee'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Jml Yg Telah DI cicil</label> 
                        
                        <input name="angsuran" class="form-control" value="<?php echo number_format($jamaah['angsuran'])?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                        <label>Angsuran Perbulan</label> 
                        
                        <input name="jml_angsuran" class="form-control" value="<?php echo number_format($jamaah['jml_angsuran'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Harga Paket</label> 
                        
                        <input name="harga_paket" class="form-control" value="<?php echo number_format($jamaah['harga_paket'])?>" readonly="readonly">
                      </div>
                     
                      <div class="col-sm-4">
                        <label>dp angsuran</label> 
                        
                        <input name="dp_angsuran" class="form-control" value="<?php echo number_format($jamaah['dp_angsuran'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Total Bayar</label> 
                        
                        <input name="Total_Bayar" class="form-control" value="<?php echo number_format($jamaah['Total_Bayar'])?>" readonly="readonly">
                      </div>
                  <div class="col-sm-4">
                        <label>status fee</label> 
                        
                       
                        <?php if($booking['status_fee']==4){?>
                          <input type='hidden' name="status_fee" class="form-control"  value="<?php echo $jamaah['status_fee']?>" readonly="readonly">

                           <input type='text' name="ket_fee" class="form-control"  value="SUDAH POTONG FEE" readonly="readonly">
                       <?php }else{  ?>
                         <input type='hidden' name="status_fee" class="form-control"  value="<?php echo $jamaah['status_fee'] + 1?>" readonly="readonly">
                         <input type='text' name="ket_fee" class="form-control"  value="BELUM POTONG FEE" readonly="readonly">
                      <?php } ?>
                      </div>
                  
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
                	     <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                     <div class="form-group">
	                    <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="ket_pembayaran" required></textarea>
	                    </div>
	                  </div>

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Metode Pembayaran</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_pay_method" id='select_pay_method'>
                      <option ="">- PILIH Metode Pembayaran -</option>
                    <?php foreach($select_pay_method as $sk){?>
                            <option value="<?php echo $sk->pay_method;?>"><?php echo $sk->pay_method;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                 </div>
	                  
	                  

              <?php if ($jamaah['akomodasi'] > 0) { ?>      
                    <div class="form-group">
                      <label class="col-lg-2 control-label">STATUS AKOMODASI</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="status_akomodasi" id="status_akomodasi">
                      <option ="">- PILIH STATUS PEMBAYARAN AKOMODASI -</option>
                    
                            <option value="1">TIDAK LANGSUNG BAYAR</option>
                            <option value="3">LANGSUNG BAYAR</option>

                    </select>
                  </div>
                  </div>
                  <?php }else{ ?>
                  
                  <?php } ?> 

                   <?php if ($jamaah['muhrim'] > 0) { ?>      
                    <div class="form-group">
                      <label class="col-lg-2 control-label">STATUS MUHRIM</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="status_muhrim" id="status_muhrim">
                      <option ="">- PILIH STATUS PEMBAYARAN MUHRIM -</option>
                    
                            <option value="1">TIDAK LANGSUNG BAYAR</option>
                            <option value="3">LANGSUNG BAYAR</option>

                    </select>
                  </div>
                  </div>
                  <?php }else{ ?>
                  
                  <?php } ?> 


            <div class="form-group">
                <label class="col-lg-2 control-label">Check Apabila Jamaah Pindah Paket </label>
                <div class="col-lg-5">

                        <div class="checkbox">
                        <label><input type="checkbox" name="MyCheckBox" id="MyCheckBox" value="1">Check</label>
                      </div>
                </div>
            </div>

<div id="ShowMeDIV" style="display: none;">
    
  


            <div class="form-group">
                <label class="col-lg-2 control-label">ID Sahabat </label>
                <div class="col-lg-5">
                    <input type="text" name="id_sahabat"  id="id_sahabat" value="<?php echo $jamaah['id_sahabat']?>" class="form-control"   >
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">DP Angsuran </label>
                <div class="col-lg-5">
                    <input type="text" name="dp_angsuran"  class="form-control" value="<?php echo $jamaah['dp_angsuran']?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Angsuran Ke</label>
                <div class="col-lg-5">
                    <input type="text" name="angsuran"  class="form-control" value="<?php echo $jamaah['angsuran']?>" required>
                </div>
            </div>
            

            <div class="form-group">
                <label class="col-lg-2 control-label">Angsuran Perbulan</label>
                <div class="col-lg-5">
                    <input type="text" name="angsuran_bulanan"  class="form-control" value="<?php echo $jamaah['jml_angsuran']?>" required>
                </div>
            </div>
</div>
				 <!--    <div class="form-group">
				        <label class="col-lg-2 control-label">Nominal Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="text" name="nominal_pembayaran"  class="form-control" required>
				        </div>
				    </div> -->
 <div id="hidden_div" style="display: none;">
                <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pembayaran</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                </div>
            </div>
             <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Mutasi Rekening</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pindah Paket</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                </div>
            </div>

             <div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                      <div class="col-lg-5">
                    <select  class="form-control" name="select_bank">
                      <option value = "">- PILIH BANK TRANSFER -</option>
                    <?php foreach($select_bank as $sk){?>
                            <option value="<?php echo $sk->nama;?>"><?php echo $sk->nama;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                  </div>
</div>
            <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div>

	                 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aktivasi</button>
           			 <a href="<?php echo base_url();?>aktivasi_manual" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   </div>
      
  </form>       
                
</div>
   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>
<script type="text/javascript">
      document.getElementById('select_pay_method').addEventListener('change', function () {
    var style = this.value == 'TRANSFER' ? 'block' : 'none';
    document.getElementById('hidden_div').style.display = style;
    // document.getElementById('hidden_div2').style.display = style;
    // $("#datepicker").prop('required',true);
     select_pay_method = $(this).val();

      console.log(select_pay_method);

      if(select_pay_method=='TRANSFER' ){
         $("#select_bank").prop('required',true);
        $("#pic1").prop('required',true);
       
      }else{
        $("#select_bank").prop('required',false);
         $("#pic1").prop('required',false);
         
      }
    });


  </script>`

  <script type="text/javascript">
    $('#MyCheckBox').click(function() {
    if ($(this).is(':checked')) {
        $("#ShowMeDIV").show();
         $("#id_sahabat").prop('required',true);
    } else {
        $("#ShowMeDIV").hide();
         $("#id_sahabat").prop('required',false);
    }
});
  </script>