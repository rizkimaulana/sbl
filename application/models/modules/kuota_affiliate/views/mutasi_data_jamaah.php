<div id="page-wrapper">
  <div class="row">
   <div class="col-lg-12">
        
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Mustasi Data Jamaah</b>
        </div>
        <div class="panel-body">
        <form  role="form" method="POST"  class="form-horizontal" action="<?php echo base_url();?>kuota_affiliate/save">
        <!-- <form  role="form" method="POST"  class="form-horizontal" > -->
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Registration Mutasi Jamaah Kuota</b>
                </div>
                 <?php if($this->session->flashdata('Warning')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Warning! </strong><?php echo $this->session->flashdata('Warning'); ?>  
                                  </div>
                              <?php } ?>
                <!-- /.panel-heading -->
                  <div class="panel-body">
                  <div class="form-group">
                    <label class="col-lg-2 control-label">Category</label>
                    <div class="col-lg-5">
                      
                     <select required class="form-control" name="paket" id="paket">
                        <option value="">Pilih Category</option>
                       <!--  <?php foreach($select_product as $sp){?>
                            <option required value="<?php echo $sp->id_product;?>"><?php echo $sp->nama;?> </option>
                        <?php } ?> -->
                      <?php foreach($select_roomcategory as $se){?>
                        <option value="<?php echo $se->id_room_category;?>"><?php echo $se->category;?></option>
                    <?php } ?>
                    </select>
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Embarkasi</label>
                      <div class="col-lg-5">
                      <select required class="form-control" name="departure" id="departure" >
                          <option value="">Pilih Embarkasi</option>
                          <?php foreach($select_embarkasi as $se){?>
                              <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                 <!--   <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div> -->
                <div class="form-group">
                          <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' id='datepicker_tahun_keberangkatan1'> -->
                                <input type='text' name="datepicker_tahun_keberangkatan" id="datepicker_tahun_keberangkatan1" class="form-control" placeholder="YYYY/2016" required/>
                             <!--    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                             
                          </div>
                    </div>

                     <div class="form-group">
                          <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                <input type='text' name="datepicker_keberangkatan" id="datepicker_keberangkatan1" class="form-control" placeholder="MM/January" required/>
                               <!--  <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                          </div>

                    </div>
                    
             
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-10">
                        <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check Waktu Keberangkatan</button>
                         
                    </div>
                  </div>
                
                  <div class="table-responsive">
                   <table id="data-table" style="width:100%" align="center" class="table table-striped table-bordered table-hover" >
                    <caption>Pilih Jadwal</caption>
                    <thead>
                        <tr>
                            <!-- <th width="2%">Check </th> -->
                            <th width="8%"><strong>Paket</strong></th>
                            <th width="10%"><strong>Embarkasi</strong></th>
                            <th width="10%"><strong>Tgl/Bln </strong></th>
                            <th width="8%"><strong>Waktu </strong></th>
                            <th width="5%"><strong>Jml Kursi</strong></th>
                            <th width="5%"><strong>Tipe Kamar</strong></th>
                            <th width="5%"><strong>Category</strong></th>
                            <th width="5%"><strong>Keterangan</strong></th>
                            
                            <!-- <th width="5%"><strong>Status Jadwal</strong></th> -->
                            <th width="5%"><strong>PILIH</strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <tr><td colspan="11"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>

                    </tbody>
                </table>
                </div>
                   
                <div class="form-group">
                    <label  class="col-sm-2 control-label" >ID AFFILIATE</label>
                    <div class="col-sm-10">
                        <input name="id_affiliate" type="text" class="form-control" id="id_affiliate" value="<?php echo $id_affiliate;?>" required readonly="treu"/>
                    </div>
                  </div>

                    <div class="form-group">
                    <label  class="col-sm-2 control-label" >ID Jamaah</label>
                    <div class="col-sm-10">
                      <input name="invoice" type="hidden" class="form-control" id="invoice" value="<?php echo $grouping_kuota['invoice']?> "readonly="treu"/>
                        <input name="id_kuota" type="hidden" class="form-control" id="id_kuota" value="<?php echo $id_kuota;?>" readonly="treu"/>
                        <input name="id_booking" type="hidden" class="form-control" id="id_booking" value="<?php echo $id_booking;?>" readonly="treu"/>
                        <input type="hidden" class="form-control" id="tgl_pembelian" name="tgl_pembelian" value="<?php echo $tgl_pembelian;?>" readonly="treu" />
                        <input type="hidden" class="form-control" id="kode_unik" name="kode_unik" value="<?php echo $grouping_kuota['kode_unik']?>" readonly="treu" />
                        <input type="hidden" class="form-control" id="payment_date" name="payment_date" value="<?php echo $grouping_kuota['payment_date']?>" readonly="treu" />
                        <input name="id_jamaah" type="text" class="form-control" id="id_jamaah" value="<?php echo $id_jamaah;?>" readonly="treu"/>
                       
                      
                    </div>
                  </div>



                <div class="form-group">
                    <label  class="col-sm-2 control-label" >Nama</label>
                    <div class="col-sm-10">
                        <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $nama;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3" >Tempat Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir"  required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Tanggal Lahir</label>
                    <div class="col-sm-10">
                     
                         <input type="text" data-mask="date" class="form-control" name="tanggal_lahir" id="datepicker"   required/>
                
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                            id="no_identitas" name="no_identitas"  required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Provinsi</label>
                    <div class="col-sm-10">
                          <select name="provinsi_id" class="form-control " id="provinsi" >
                                <option value="">- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                         
                            <div class="col-sm-5">
                             
                            </div>
                        
                    </div>

                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kabupaten</label>
                    <div class="col-sm-10">
                       <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
                       
                            <div class="col-sm-5">
                            
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kecamatan</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_id" class="form-control" id="kecamatan">
                                <option value=''>Select Kecamatan</option>
                            </select>
                            <div class="col-sm-5">
                           
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Alamat</label>
                    <div class="col-sm-10">
                        <textarea  name="alamat" class="form-control"  ></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Telp</label>
                    <div class="col-sm-10">
                        <input id="telp" name="telp" type="type" class="input_telp form-control"  required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Email</label>
                    <div class="col-sm-10">
                        <input  name="email" type="text" class="form-control"  >
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select required class="form-control" id="jenis_kelamin" name="select_kelamin">
                        <option value="">- PILIH JENIS KELAMIN -</option>
               
                         <?php foreach($select_kelamin as $sj){?>
                              <option value="<?php echo $sj->kdstatus;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                     <label class=" col-sm-2 control-label" >Rentang Umur</label>
                     <div class="col-sm-10">
                    <select required class="form-control" name="select_rentangumur">
                      <option value="">- PILIH RENTANG UMUR -</option>
                

                       <?php foreach($select_rentangumur as $sj){?>
                              <option value="<?php echo $sj->id_rentang_umur;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-2 control-label" for="status">Status :</label>
                   <div class="col-sm-10">
                  <select required class="select_status form-control" name="select_statuskawin">
                    <option value="">- PILIH STATUS -</option>
                  

                       <?php foreach($select_statuskawin as $sj){?>
                              <option value="<?php echo $sj->kdstatus;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                    <label class="col-lg-2 control-label">Berangkat dengan : </label>
                    <div class="col-lg-10">
                     <select required class="form-control" name="select_status_hubungan" id="select_status_hubungan" >
                      <option value="">- PILIH BERANGKAT DENGAN -</option>
                   
                        <?php foreach($select_status_hubungan as $sj){?>
                              <option value="<?php echo $sj->kdstatus;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>

            <div id="hidden_div" style="display: none;">
              <div class="form-group">
                  <label class=" col-sm-2 control-label" >Hubungan Keluarga</label>
                   <div class="col-sm-10">
                    <select  class="form-control" name="hubungan" id="hubungan">
                        <option value="">- PILIH HUBUNGAN KELUARGA -</option>
                      
                        <?php foreach($family_relation as $sj){?>
                              <option value="<?php echo $sj->id_relation;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                  </select>
                  </div>
                  </div>

                  

                 <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >Nama Keluarga</label>
                        <div class="col-sm-10">
                         <select  class="form-control" name="select_keluarga" id="select_keluarga">
                           <option value="">- PILIH KELUARGA -</option>
                           <?php foreach($select_keluarga as $sj){?>
                              <option value="<?php echo $sj->id_jamaah;?>"><?php echo $sj->nama;?></option>
                          <?php } ?>
                        </select>
                        </div>
                    </div>
                  
                 </div>
 
                   <div class="form-group">
                      <label class="col-sm-2 control-label">Berangkat Dari</label>
                        <div class="col-sm-10">
                          <select required name="pemberangkatan" class="form-control chosen-select" id="pemberangkatan">
                            <option value="">PILIH BERANGKAT DARI BANDARA</option>
                            <?php foreach($select_pemberangkatan as $select_pemberangkatan):?>
                            <option value="<?php echo $select_pemberangkatan->pemberangkatan;?> "><?php echo $select_pemberangkatan->pemberangkatan;?></option>
                            <?php endforeach;?>
                          </select>
                          <input  type="hidden" class="form-control" id="id_bandara" name="id_bandara"  readonly="true" />
                          <input type="hidden" class="form-control" id="refund" name="refund" readonly="true"   />
                          <input type="hidden" class="form-control" id="akomodasi" name="akomodasi"  readonly="true"  />
                          <input type="hidden" class="form-control" id="handling" name="handling"  readonly="true" />
                        </div> 
                      </div>


<!--                     <div class="form-group">
                        <label class="col-sm-2 control-label">Cast Back / Refund</label>
                        <div class="col-sm-10">
                          <input  type="text" class="form-control" id="id_bandara" name="id_bandara"  readonly="true" />
                          <input type="text" class="form-control" id="refund" name="refund"   />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akomodasi</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="akomodasi" name="akomodasi"   />
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Handling</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="handling" name="handling"  />
                        </div>
                    </div>
                     -->

                    <div class="form-group">
                        <label  class="col-lg-4 control-label">Dikenakan Biaya MUHRIM</label>
                        <div >

                            <?php 
                          $biaya_setting_ = $this->kuotaaffiliate_model->get_key_val();
                            foreach ($biaya_setting_ as $key => $value){
                              $out[$key] = $value;
                            }
                        ?>

                            <input type='radio' name='XIAlmnus' value='Yes' id="XIyes" required/>Yes
                            <input type='radio' name='XIAlmnus' value='No' id="XIno" required />No
                            
                        </div>
                    </div>
                  <div class="form-group">

                    <div class="col-sm-10">
                        <input id="muhrim" name="muhrim" type="hidden" class="input_telp form-control" placeholder="Muhrim" readonly="true" required>
                    </div>
                  </div>
                   <!--  <div class="form-group">
                        <label class="col-sm-2 control-label">Harga Paket</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="harga" name="harga" value="<?php echo $harga;?>" readonly="treu" />
                        </div>
                    </div> -->
                     
                          
                        
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Room Type</label>
                        <div class="col-sm-10">
                       <select required class="form-control" name="select_type">
                         <option  value="">- PILIH ROOM TYPE -</option>

                           <?php foreach($select_type as $sj){?>
                              <option value="<?php echo $sj->id_room_type;?>"><?php echo $sj->type;?></option>
                          <?php } ?>
                      </select>
                      </div>
                      </div>
                   
                  <div class="form-group">
                      <label class="col-sm-2 control-label"
                            for="inputPassword3" >Ahli Waris</label>
                      <div class="col-sm-10">
                          <input id="waris" name="waris" type="text" class="input_telp form-control"   required>
                      </div>
                  </div>

                  <div class="form-group">
                         <label class="col-sm-2 control-label" >Hubungan Ahli waris </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_hub_ahliwaris">
                          <option  value="">- PILIH Hubungan Ahli Waris -</option>

                          <?php foreach($select_hub_ahliwaris as $sj){?>
                              <option value="<?php echo $sj->id_hubwaris;?>"><?php echo $sj->hubungan;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                       <div class="form-group">
                         <label class="col-sm-2 control-label" for ="select_merchandise">Merchandise </label>
                         <div class="col-sm-10">
                        <select required class="select_merchandise form-control" name="select_merchandise">
                          <option  value="">- PILIH MERCHANDISE -</option>
      
                            <?php foreach($select_merchandise as $sj){?>
                              <option value="<?php echo $sj->id_merchandise;?>"><?php echo $sj->merchandise;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >No Pasport</label>
                        <div class="col-sm-10">
                          <input name="no_pasport" type="text" id="no_pasport"class="input_keluarga form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >issue office</label>
                        <div class="col-sm-10">
                          <input name="issue_office" type="text" id="issue_office"class="input_keluarga form-control" >
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Isue Date</label>
                    <div class="col-sm-10">
                    
                         <input type="text" class="form-control" name="isue_date" id="datepicker2"    />
                
                    </div>
                  </div>
                  <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Kartu Identitas</label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_identitas" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_identitas" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Kartu Keluarga </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_kk" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_kk" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                     <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Pas Photo </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_photo" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_photo" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Pasport </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_pasport" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_pasport" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Vaksin </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_vaksin" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_vaksin" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Buku Nikah </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_buku_nikah" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_buku_nikah" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    
                  </div>
 
                           
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            <a href="<?php echo base_url();?>kuota_affiliate"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>

                   </div>
                </div>

              </div>
            </div><!--end col-->




        </form>
           
           </div>

     </div>
     </div>
     </div>
  </div><!--end row-->
</div>
<script type="text/javascript">
  $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('kuota_affiliate/get_biaya_refund_');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#refund").val(html);
                    
                }
            })
        })


            $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('kuota_affiliate/get_biaya_refund_2');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#id_bandara").val(html);
                    
                }
            })
        })


        $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('kuota_affiliate/get_biaya_refund_3');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#akomodasi").val(html);
                    
                }
            })
        })
</script>

<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker_keberangkatan1').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
    </script>

    

<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('kuota_affiliate/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('kuota_affiliate/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('kuota_affiliate/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });

    $("#check").on('click', function (e){
    e.preventDefault();
    var paket = $("#paket").val();
    var departure = $("#departure").val();
    var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
    var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
    console.log(paket);
    console.log(departure);
    console.log(datepicker_tahun_keberangkatan);
    console.log(datepicker_keberangkatan);
    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>kuota_affiliate/searchItem',
             // data:'from='+from+'&to='+to
            data:'q='+paket+'&l='+departure+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});


   document.getElementById('select_status_hubungan').addEventListener('change', function () {
    var style = this.value == 8 ? 'block' : 'none';
    document.getElementById('hidden_div').style.display = style;
    });



            

</script>
<script type="text/javascript">


</script>
 <script type="text/javascript">
  jQuery(document).ready(function(){
  $(".chosen-select").chosen({width: "95%"}); 
});


document.getElementById('XIyes').onchange = displayTextBox;
document.getElementById('XIno').onchange = displayTextBox;

var textBox = document.getElementById('muhrim');

function displayTextBox(evt){
    console.log(evt.target.value)
    if(evt.target.value=="Yes"){
        textBox.value = '<?php echo $out['MUHRIM'];?>';
    }else{
        textBox.value = 0;
    }
}
 </script>