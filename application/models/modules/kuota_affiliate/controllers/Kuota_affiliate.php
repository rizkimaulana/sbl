<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kuota_affiliate extends CI_Controller{ 
	var $folder = "kuota_affiliate";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(JAMAAH_KUOTA_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','kuota_affiliate');	
		$this->load->model('kuotaaffiliate_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/kuota_affiliate');
		
	}
	
	public function cetak_data_kuota(){
	    $id_booking_kuota = $this->uri->segment(3);

	    $this->template->load('template/template', $this->folder.'/kuota_affiliate/report_kuota/$id_booking_kuota ');
		
	}


	private function _select_affiliate_type(){
		$id_affiliate_type = array('1','5');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
	    return $this->db->get('affiliate_type')->result();
	}


	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->order_by("nama", "asc")->get_where(
		    	'affiliate',array(
		    		'id_affiliate_type'=>$id_affiliate_type,
		    		'status'=>1
		    		)

		    	);
		    $this->db->order_by("nama", "asc");
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama." ".$value->id_user."</option>";
		    }
		    echo $data;
	}
	
public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->kuotaaffiliate_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="20%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->id_affiliate.'</td>';
	             	 $rows .='<td width="20%">'.$r->tgl_pembelian.'</td>';
	                $rows .='<td width="9%">'.$r->jml_kuota.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Data Kuota" class="btn btn-sm btn-primary" href="'.base_url().'kuota_affiliate/detail/'.$r->id_booking_kuota.'">
	                            <i class="fa fa-pencil"></i> Data Kuota
	                        </a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room('."'".$r->id_room."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'kuota_affiliate/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function generate_data_kuota(){
		$data = $this->input->post(null,true);
		$send = $this->kuotaaffiliate_model->save_get_kuota($data);
	  // if($send)
			// // $id_booking = $this->uri->segment(4);
			// $id_booking = $this->input->post('id_booking');
	  //       redirect('kuota_admin/report_kuota/'.$id_booking.'');
	}
	public function save(){    
	 // $data = $this->input->post(null,true);
	 //    $schedule = $this->input->post('radio');
	 //    $inputan = '';
	 //    foreach($schedule as $value){
	 //    	$inputan .= ($inputan!=='') ? ',' : '';
	 //    	$inputan .= $value;
	 //    }
	   
	 //  $send = $this->kuotaaffiliate_model->save($data,$inputan);
	 //  if($send)
		// 	// $id_booking = $this->uri->segment(4);
		// 	$id_booking = $this->input->post('id_booking');
	 //        redirect('kuota_affiliate/detail/'.$id_booking.'');


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            $no_identitas=$this->input->post('no_identitas'); // mendapatkan input dari kode
            $cek=$this->kuotaaffiliate_model->cek($no_identitas);

            $id_jamaah=$this->input->post('id_jamaah'); // mendapatkan input dari kode
            $cek_id_jamaah=$this->kuotaaffiliate_model->cek_id_jamaah($id_jamaah);
             // cek kode di database
            if($cek->num_rows()>0){ // jika kode sudah ada, maka tampilkan pesan
                 
               // echo'<div class="alert alert-dismissable alert-danger"><small>No Identitas sudah digunakan</small></div>';
            	$this->session->set_flashdata('Warning', "No Identitas sudah digunakan");
            	 $id_booking = $this->input->post('id_booking');
            	 $id_jamaah = $this->input->post('id_jamaah');
            	redirect('kuota_affiliate/mutasi_data_jamaah/'.$id_jamaah.'/'.$id_booking.'');
                  
            }elseif ($cek_id_jamaah->num_rows()>0) {
            	$this->session->set_flashdata('Warning', "ID Jamaah Sudah terdaftar di jamaah Aktif");
            	 $id_booking = $this->input->post('id_booking');
            	 $id_jamaah = $this->input->post('id_jamaah');
            	redirect('kuota_affiliate/mutasi_data_jamaah/'.$id_jamaah.'/'.$id_booking.'');
            }else{
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    $inputan = '';
	    foreach($schedule as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;
	    }
	   
	  $send = $this->kuotaaffiliate_model->save($data,$inputan);
	  if($send)
	  	$this->session->set_flashdata('info', "Successfull");
			//$id_booking = $this->uri->segment(4);
			$id_booking = $this->input->post('id_booking');
	        redirect('kuota_affiliate/detail/'.$id_booking.'');

					// echo'<div class="alert alert-dismissable alert-success"><h4>Transaction Successfull</h4></div>';
		}

	}else{
		
		$this->session->set_flashdata('Warning', "Pilih Jadwal Keberangkatan");
		$id_booking = $this->input->post('id_booking');
		$id_jamaah = $this->input->post('id_jamaah');
	    redirect('kuota_affiliate/mutasi_data_jamaah/'.$id_jamaah.'/'.$id_booking.'');
	}
	}

	public function add(){
	     
	     if(!$this->general->privilege_check_affiliate(JAMAAH_KUOTA_AFFILIATE,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_affiliate_type'=>$this->_select_affiliate_type(),
	    				// 'select_affiliate'=>$this->_select_affiliate(),
	    	
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);		
	}
	
	
	 function get_fee_kuota(){
		$affiliate_type = $this->input->post('affiliate_type');
		$affiliate_type = $this->kuotaaffiliate_model->get_fee_jamaah_kuota($affiliate_type);
		 if($affiliate_type->num_rows()>0){
            $affiliate_type=$affiliate_type->row_array();
            echo $affiliate_type['fee_jamaah_kuota'];
           
          
        }
        
	}


	public function report_kuota(){  
	    $id_booking_kuota = $this->uri->segment(3);
	    $report_kuota = $this->kuotaaffiliate_model->get_booking_kuota($id_booking_kuota);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$report_kuota){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'report_kuota'=>$report_kuota,
	       		 // 'pic'=>$pic
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_kuota',($data));
	    // $this->load->view('bilyet_barcode',($data));
	}


	public function detail(){
	    if(!$this->general->privilege_check_affiliate(JAMAAH_KUOTA_AFFILIATE,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->kuotaaffiliate_model->get_jamaah_kuota_aktif($id_booking);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    // else{

	    //     $pic = $this->kuotaaffiliate_model->get_jamaah_kuota_aktif($id_booking);
	    // }    

	    $data = array(

	       		 'booking'=>$booking,
	       		 // 'pic'=>$pic
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/mutasi_data',($data));

	}


	public function mutasi_data_jamaah($id_booking=''){
	    if(!$this->general->privilege_check_affiliate(JAMAAH_KUOTA_AFFILIATE,'edit'))
		    $this->general->no_access();

		$id_booking = $this->uri->segment(4);

	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('jamaah_kuota',array('id_jamaah'=>$id))->row_array();
	   if(!$get)
	        show_404();
	        $grouping_kuota = $this->kuotaaffiliate_model->get_booking_kuota_aktif($id_booking);
	    $data = array(
	    		 'select_product'=>$this->_select_product(),
				'select_embarkasi'=>$this->_select_embarkasi(),
				'select_roomcategory'=>$this->_select_roomcategory(),
	    		'grouping_kuota'=>$grouping_kuota,
	    		'provinsi'=>$this->kuotaaffiliate_model->get_all_provinsi(),
		    	'kabupaten'=>$this->kuotaaffiliate_model->get_all_kabupaten(),		
		    	'select_kelamin'=>$this->_select_kelamin(),	
		    	'select_rentangumur'=>$this->_select_rentangumur(),
		    	'select_statuskawin'=>$this->_select_statuskawin(),
		    	'select_status_hubungan'=>$this->_select_status_hubungan(),
		    	'family_relation'=>$this->_family_relation(),
		    	'select_keluarga'=>$this->_select_keluarga(),
		    	'select_pemberangkatan'=>$this->_select_pemberangkatan(),
		    	'select_type'=>$this->_select_type(),
		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'select_merchandise'=>$this->_select_merchandise(),
	    	);
		$this->template->load('template/template', $this->folder.'/mutasi_data_jamaah',array_merge($get,$data));
	   



	}

		function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->kuotaaffiliate_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

     private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_roomcategory(){
		 $id_room_category = array('1');
		$this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

	function add_ajax_kab($id_prov){
	    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
	    $data = "<option value=''>- Select Kabupaten -</option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_keluarga($id_booking=''){
	$id_booking = $this->uri->segment(4);
	$this->db->where_in('id_booking', $id_booking);
    return $this->db->get('jamaah_kuota')->result();
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}


	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}

	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->kuotaaffiliate_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->kuotaaffiliate_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->kuotaaffiliate_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}


}
