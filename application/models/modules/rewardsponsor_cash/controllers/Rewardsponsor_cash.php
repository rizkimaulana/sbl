<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rewardsponsor_cash extends CI_Controller{
	var $folder = "rewardsponsor_cash";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REWARD_POSTING_MGM,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_affiliate');	
		$this->load->model('rewardsponsorcash_model');
		
	}
	
	public function index(){
	    	      $data = array(
			'pic' => $this->rewardsponsorcash_model->get_data_fee('order by datewin ASC')->result_array(),
		);
	    $this->template->load('template/template', $this->folder.'/rewardsponsor_cash',$data);
		// $this->load->helper('url');
		// $this->load->view('rewardmgm_umroh');
	}
	

	public function ajax_list()
	{
		$list = $this->fee->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fee) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $fee->invoice;
			$row[] ='<td width="20%" >'.$fee->id_affiliate.' - '.$fee->affiliate.'</td>';
			// $row[] = $fee->affiliate;
			$row[] = $fee->create_date;
			$row[] = $fee->jml_jamaah;
			$row[] = $fee->total_fee;
			$row[] = $fee->Potongan_pajak;
			$row[] = $fee->setelah_potong_pajak;
				 if ($fee->status_fee == 2){ 
	                $row[] ='<a title="TRANSFERRED" class="btn btn-sm btn-primary" href="'.base_url().'rewardmgm_umroh/detail/'.$fee->id_booking.'/'.$fee->id_affiliate.'">
	                            <i class="fa fa-pencil"></i> TRANSFERRED
	                        </a>  ';
	              }
  $row[] ='<a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan('."'".$fee->id_booking."'".')"> '.$fee->proses.'</a> ';
  			// if ($fee->proses == null){ 
	    //             $row[] ='<a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan('."'".$fee->id_booking."'".')"> '.$fee->proses.'</a> ';
	                
	    //             }else{
	    //             	$row[] ='<a title="ON PROSES" class="btn btn-sm btn-success" href="#">
	    //                         </i> ON PROSES
	    //                     </a> ';
	    //             }
	              

	            
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->fee->count_all(),
						"recordsFiltered" => $this->fee->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	

	public function detail(){

    if(!$this->general->privilege_check(REWARD_POSTING_MGM,'edit'))
	    $this->general->no_access();
    
    $id_reward = $this->uri->segment(3);
    $fee_posting = $this->rewardsponsorcash_model->get_pic_posting($id_reward);
    // $pic    = array();
    $pic_booking= array();
    if(!$fee_posting ){
        show_404();
    }
     

    $data = array(
    		'dana_bank'=>$this->_select_bank(),
    		'bank_transfer'=>$this->_select_bank(),
       		 'fee_posting'=>$fee_posting

       		 );

    $this->template->load('template/template', $this->folder.'/detailfeeposting_admin',($data));

	}
	
	function save(){


	    $data = $this->input->post(null,true);
	
	    
	  // print_r($data);
	    if($this->rewardsponsorcash_model->save($data)){
	        
	        // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
	      	redirect('rewardsponsor_cash');  
	    }
}

	public function update_pengajuan($id)
	{
		// if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
		//     $this->general->no_access();
		$send = $this->rewardsponsorcash_model->update_pengajuan($id);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('rewardsponsor_cash');

	}


}
