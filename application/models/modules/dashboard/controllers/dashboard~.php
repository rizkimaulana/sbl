<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function cek_aktif() {
			if ($this->session->userdata('admin_valid') == false && $this->session->userdata('admin_id') == "") {
				redirect('login/masuk');
			} 
	}
	
	public function index()
	{
		$this->cek_aktif();
		$this->load->view('template/header');
		$this->load->view('template/menu');
		$this->load->view('template/nav');
		$this->load->view('dashboard');
		$this->load->view('template/footer');
		
	}
	public function logout() {
		$data = array(
                    'admin_id' 		=> "",
                    'admin_user' 	=> "",
                    'admin_level' 	=> "",
                    
                    'admin_nama' 	=> "",
					'admin_valid' 	=> false
                    );
        $this->session->set_userdata($data);
		redirect('home');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */