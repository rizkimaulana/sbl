<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	var $folder =   "dashboard";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DASHBOARD,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','dashboard');	
		$this->load->model('dashboard_model');

	}
	
	public function index(){
	    


	    // $this->data['jamaah_belum_aktif'] = $this->dashboard_model->get_jamaah_belum_aktif();
	    // $this->data['jamaah_aktif'] = $this->dashboard_model->get_jamaah_aktif();
	    // $this->data['jamaah_alumni'] = $this->dashboard_model->get_jamaah_alumni();
	    // $this->data['total_jamaah'] = $this->dashboard_model->get_total_jamaah();

	 //    $data=array();
		// foreach($this->dashboard_model->get()->result_array() as $row)
		// 	$data[] = (int) $row['total_jamaah'];
		// $this->load->view('welcome_message',array('data'=>$data));

	    // $this->template->load('template/template', $this->folder.'/dashboard',$this->data);


		$data=array();
		foreach($this->dashboard_model->get_grafik_jamah_all()->result_array() as $row)
			$data[] = (int) $row['total_jamaah'];

		
		$get_totalmanifest = $this->dashboard_model->get_total_manifest()->result();
        foreach ($get_totalmanifest as $r)
            $totalmanifest = $r->total_manifest;
        $datamanifest['totalmanifest'] = $totalmanifest;
        
        // total manifest periode > current periode
        $get_total_jamaah = $this->dashboard_model->get_jumlah_jamaah()->result();
        foreach ($get_total_jamaah as $r){
            $datamanifest['jumlah_jamaah'] = $r->jumlah_jamaah;
            $datamanifest['bulan'] = $r->bulan;
            $datamanifest['bulan_tahun'] = $r->bulan_tahun;
        }

        $get_manifest_all = $this->dashboard_model->get_total_manifest_all()->result();
        foreach ($get_manifest_all as $r){
            $datamanifest['total_manifest'] = $r->jumlah_jamaah; 
            $total_manifest = $r->jumlah_jamaah; 
        }
        
        $get_manifest_appr = $this->dashboard_model->get_total_manifest_approved()->result();
        foreach ($get_manifest_appr as $r){
            $datamanifest['total_manifest_appr'] = $r->jumlah_jamaah; 
        }
        
        $datamanifest['total_manifest_not_appr'] = $datamanifest['total_manifest'] - $datamanifest['total_manifest_appr'];
        

        // total manifest approved
        $get_totalmanifest_appr = $this->dashboard_model->get_total_manifest_appr()->result();
        foreach ($get_totalmanifest_appr as $r)
            $totalmanifest_appr = $r->total_manifest;
        $datamanifest['totalmanifest_appr'] = $totalmanifest_appr;
        
        // total manifest approved per period
        $get_totalmanifest_appr_period = $this->dashboard_model->grafik_manifest_appr_period()->result();
        foreach ($get_totalmanifest_appr_period as $r)
            $totalmanifest_appr_period = $r->jumlah;

        $datamanifest['totalmanifest_appr_period'] = $totalmanifest_appr_period;

        // total manifest not approved
        $totalmanifest_not_appr = $total_manifest - $totalmanifest_appr;
        $datamanifest['totalmanifest_not_appr'] = $totalmanifest_not_appr;
        
        // total manifest not approved period
        //$totalmanifest_not_appr_period = $totalmanifest_period - $totalmanifest_appr_period;
        //$datamanifest['totalmanifest_not_appr_period'] = $totalmanifest_not_appr_period;

        // persen manifest
        $datamanifest['persen_appr'] = round(($totalmanifest_appr / $totalmanifest) * 100);
        $datamanifest['persen_not_appr'] = round(($totalmanifest_not_appr / $totalmanifest) * 100);

        // get data grafik progress manifest 
        $getdata = $this->dashboard_model->get_manifest_bydate()->result();
        foreach ($getdata as $row) {
            $datamanifest['jumlah'] = $row->jumlah;
            $datamanifest['tgl'] = $row->tgl;
        }

	    $this->template->load('template/template', $this->folder.'/dashboard',array('data'=>$data,'manifest'=>$datamanifest));
	 //   $this->load->view('template/header');
		// $this->load->view('template/menu');
		// $this->load->view('template/nav');
		// $this->load->view('dashboard/dashboard');
		// $this->load->view('template/footer');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */