<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bandara_model extends CI_Model{
   
   
    private $_table="bandara";
    private $_primary="id_bandara";

    public function save($data,$data2){
  
            
        $arr = array(
        
           'lokasi' =>$data['lokasi'],
            'provinsi' =>$data['provinsi'],
            'kode' => $data['kode'],
            'bandara' => $data['bandara'],
            'refund' =>$data['refund'],
           'handling' =>$data['handling'],
        );       
        
         return $this->db->insert('bandara',$arr);

    }


    public function update($data){
        
        $arr = array(
        
           'lokasi' =>$data['lokasi'],
            'provinsi' =>$data['provinsi'],
            'kode' => $data['kode'],
            'bandara' => $data['bandara'],
            'refund' =>$data['refund'],
            'handling' =>$data['handling'],
        );       
              
        return $this->db->update($this->_table,$arr,array('id_bandara'=>$data['id_bandara']));
    }

  
 
    
    public function get_data($offset,$limit,$q=''){
    
  
          $sql = " SELECT * FROM bandara where  1=1 and status='1'
                    ";
        
        if($q){
            
            $sql .=" AND lokasi LIKE '%{$q}%' 
            		OR provinsi LIKE '%{$q}%'
                    OR bandara LIKE '%{$q}%'
            		OR kode LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_bandara DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     public function delete($id_bandara){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_bandara'=>$id_bandara));
            // $this->db->delete('user',array('id_user'=>$id_user));
       
  
    }
    
    public function delete_by_id($id_bandara)
    {
        $this->db->where('id_bandara', $id_bandara);
        $this->db->delete($this->_table);
    }
    
    
}
