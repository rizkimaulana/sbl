 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit User</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>bandara/update">
             <input type="hidden" name="id_bandara" value="<?php echo $id_bandara;?>">
            <div class="form-group">
                <label>Lokasi</label>
                <input class="form-control" name="lokasi" value="<?php echo $lokasi;?>" required autofocus>
            </div>
            <div class="form-group">
                <label>Provinsi</label>
                <input class="form-control" name="provinsi" value="<?php echo $provinsi;?>" required>
            </div>
              <div class="form-group">
                <label>Kode</label>
                <input class="form-control" name="kode" value="<?php echo $kode;?>" required>
            </div>

               <div class="form-group">
                <label>Bandara</label>
                <input class="form-control" name="bandara" value="<?php echo $bandara;?>" required>
            </div>
            
             <div class="form-group">
                <label>Refund</label>
                <input class="form-control" name="refund" value="<?php echo $refund;?>" required>
            </div>
            <div class="form-group">
                <label>Handling</label>
                <input class="form-control" name="handling" value="<?php echo $handling;?>" required>
            </div>

       <!--      <div class="form-group">
                <label>Status</label>
                 <select class="form-control" name="status">
                     <?php foreach($select_status as $st){ 
                    
                        $selected = ($status == $st->kdstatus)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                    <?php } ?>
                </select>
            </div> -->
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>bandara" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->
