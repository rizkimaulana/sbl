 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add PRODUCT</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>bandara/save">
            
            <div class="form-group">
                <label>lokasi</label>
                <input class="form-control" name="lokasi" required>
            </div>
            <div class="form-group">
                <label>Provinsi</label>
                <input class="form-control" name="provinsi" required>
            </div>
            <div class="form-group">
                <label>Kode</label>
                <input class="form-control" name="kode" required>
            </div>
            <div class="form-group">
                <label>Bandara</label>
                <input class="form-control" name="bandara" required>
            </div>
            <div class="form-group">
                <label>Refund</label>
                <input class="form-control" name="refund" required>
            </div>
              <div class="form-group">
                <label>Handling</label>
                <input class="form-control" name="handling"  required>
            </div>
       
          <!--   <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="select_status">
                    <?php foreach($select_status as $st){?>
                        <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                    <?php } ?>
                </select>
            </div> -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>bandara" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
