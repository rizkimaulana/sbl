<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jamaahalumni_model extends CI_Model{
   
   
    private $_table="booking";
    private $_primary="id_booking";


public function get_pic($id_booking){
    
       $sql ="SELECT * from detail_jamaahalumni WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

 public function get_pic_booking($id_booking){
 

         $sql = "SELECT * from view_booking_alumni where id_booking = {$id_booking}";
 	
        return $this->db->query($sql)->row_array();  
    }


      


    function get_biaya_refund() {
        $this->db->select('*');
        $this->db->from('view_refund');
        // $this->db->where('tampil', 'Y');
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    
    public function get_data($offset,$limit,$q=''){
    
        $id=  $this->session->userdata('id_user');
          $sql = " SELECT * from view_booking_alumni 
                    ";
        
        if($q){
            
            $sql .=" AND affiliate LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_booking DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     

    function get_nama_jamaah() {
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status', '0');
        $this->db->order_by('id_registrasi', 'ASC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

        function get_data_jamaah($id) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }
    
    
    
}
