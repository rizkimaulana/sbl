<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datajamaah_model extends CI_Model{
   
   
    private $_table="booking";
    private $_primary="id_booking";

     private $_registrasi_jamaah="registrasi_jamaah";
     private $_id_registrasi="id_registrasi";




 public function get_pic($id_booking){
    
        $sql ="SELECT * from detail_jamaahnoaktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

 public function get_pic_booking($id_booking){
 

         $sql = "SELECT * from view_lap_pembayaran where id_booking = {$id_booking}
                ";
        return $this->db->query($sql)->row_array();  
    }


      



    public function get_data($offset,$limit,$q=''){
    
        $id=  $this->session->userdata('id_user');
          $sql = " SELECT * from view_lap_pembayaran 
                    ";
        
        if($q){
            
            $sql .=" AND affiliate LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_booking DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    
    function lap_data_jamaah($id_booking) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }



    function get_data_booking($id_booking) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_jamaahnoaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        } else {
            return FALSE;
        }
    }
    
}
