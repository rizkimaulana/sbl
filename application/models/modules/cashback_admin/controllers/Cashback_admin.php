<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cashback_admin extends CI_Controller{
	var $folder = "cashback_admin";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(KELOLAH_FEE_POSTING_NEW,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_affiliate');	
		$this->load->model('cashbackadmin_model');
		$this->load->model('cashbackadmin_model','r');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/cashback_admin');
		// $this->load->helper('url');
		// $this->load->view('feepostingnew_admin');
	}
	

	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $r->invoice;
			$row[] ='<td width="20%" >'.$r->id_affiliate.' - '.$r->affiliate.'</td>';
			// $row[] = $r->affiliate;
			$row[] = $r->create_date;
			$row[] = $r->jml_jamaah;
			$row[] = $r->total_fee;
			$row[] = $r->Potongan_pajak;
			$row[] = $r->setelah_potong_pajak;
				 if ($r->status_fee == 2){ 
	                $row[] ='<a title="TRANSFERRED" class="btn btn-sm btn-primary" href="'.base_url().'feepostingnew_admin/detail/'.$r->id_booking.'/'.$r->id_affiliate.'">
	                            <i class="fa fa-pencil"></i> TRANSFERRED
	                        </a>  ';
	              }

  			if ($r->proses == null){ 
	                $row[] ='<a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan('."'".$r->id_booking."'".')"> PROSES</a> ';
	                
	                }else{
	                	$row[] ='<a title="ON PROSES" class="btn btn-sm btn-success" href="#">
	                            </i> ON PROSES
	                        </a> ';
	                }
	              

	            
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->fee->count_all(),
						"recordsFiltered" => $this->fee->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	

	public function detail(){

    if(!$this->general->privilege_check(KELOLAH_FEE_POSTING_NEW,'edit'))
	    $this->general->no_access();
    
    $id_booking = $this->uri->segment(3);
    $id_affiliate = $this->uri->segment(4);
    $fee_posting = $this->cashbackadmin_model->get_pic_posting($id_booking,$id_affiliate);
    $pic    = array();
    // $pic_booking= array();
    if(!$fee_posting ){
        show_404();
    }
    else{
        
        $pic = $this->cashbackadmin_model->get_pic($id_booking,$id_affiliate);
       
    }    

    $data = array(
    		'dana_bank'=>$this->_select_bank(),
    		'bank_transfer'=>$this->_select_bank(),
       		 'fee_posting'=>$fee_posting,'pic'=>$pic

       		 );

    $this->template->load('template/template', $this->folder.'/detailfeeposting_admin',($data));

	}
	
	function save(){
	// 	$this->form_validation->set_rules('checkbox','checkbox','required|trim');

	// 	if($this->form_validation->run()==true){

	//     $inputan = '';
	//     foreach($data2 as $value){
	//     	$inputan .= ($inputan!=='') ? ',' : '';
	//     	$inputan .= $value;
	//     }
	//     $data = $this->input->post(null,true);
	//     $data2 = $this->input->post('checkbox');
	//     $simpan = $this->cashbackadmin_model->save($data,$inputan);
	//     if($simpan)
	//         redirect('feepostingnew_admin');
	// }else{
	// print_r("error Anda Belum Memilih Jamaah");
	// }



		// $data = $this->input->post(null,true);
	 //    $data2 = $this->input->post('checkbox');
	 //    $inputan = '';
	 //    foreach($data2 as $value){
	 //    	$inputan .= ($inputan!=='') ? ',' : '';
	 //    	$inputan .= $value;
	 //    }
	 //    $simpan = $this->cashbackadmin_model->save($data,$inputan);
	 //    if($simpan)
	 //        redirect('feepostingnew_admin');

		$id_booking = $this->input->post('id_booking');
	    $data = $this->input->post(null,true);
	    $data2 = $this->input->post('id_registrasi');
	    
	  // print_r($data);
	    if($this->cashbackadmin_model->save($data)){
	        
	        // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
	      	redirect('feepostingnew_admin');  
	    }
}

	public function update_pengajuan($id_booking)
	{
		// if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
		//     $this->general->no_access();
		$send = $this->cashbackadmin_model->update_pengajuan($id_booking);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('feepostingnew_admin');

	}


}
