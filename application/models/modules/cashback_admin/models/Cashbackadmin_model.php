<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cashbackadmin_model extends CI_Model{

    
    var $table = 'fee_posting_jamaah_admin_new';
    var $column_order = array(null, 'invoice','id_affiliate','affiliate','create_date'); //set column field database for datatable orderable
    var $column_search = array('invoice','id_affiliate','affiliate','create_date'); //set column field database for datatable searchable 
    var $order = array('invoice' => 'asc'); // default order 

    private $_table="fee";
    private $_primary="id_booking";

    private $_kuota_booking="kuota_booking";
    private $_id_booking="id_booking";

     private $_claim_feefree="claim_feefree";
    private $_id_claim_feefree="id_claim_feefree";

    
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save($data,$data2=''){
        

        $arr = array(
           'id_booking' => $data['id_booking'],
           'id_affiliate' => $data['id_affiliate'],
           'id_schedule' => $data['id_schedule'],
           'id_product' => $data['id_product'],
            'transfer_date' => $data['transfer_date'],
            'dana_bank' => $data['dana_bank'],
            'bank_transfer' => $data['bank_transfer'],
            'nama_rek' => $data['nama_rek'],
            'no_rek' => $data['no_rek'],
            'nominal' => $data['fee'],
            'keterangan' => $data['keterangan'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            // 'status_feefree'=>2,
            
        );       
        
        $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();

        //  $sql = "UPDATE fee SET status_fee = '3',proses='TRANSFERRED',id_claim='".$id_claim_feefree."',update_date='".date('Y-m-d H:i:s')."',update_by='".$this->session->userdata('id_user')."' WHERE id_registrasi IN ($data2)";
        // $update_data = $this->db->query($sql);
        // $update_data;

          $id_registrasi = $this->input->post('id_registrasi');
    if(isset($data['data2'])) {
        
            $pic = array();
            foreach($data['data2'] as $pi){
                $id_registrasi = $pi['id_registrasi'];
                $tmp = array(
                
                    'id_registrasi' => $pi['id_registrasi'],
                    'status_fee' => 3,
                    'proses' => 'TRANSFERRED',
                    'id_claim' => $id_claim_feefree,
                    'update_date' => date('Y-m-d H:i:s'),
                    'update_by' => $this->session->userdata('id_user'),
                );
               
                $pic[] = $tmp;

            }

            $this->db->update_batch('fee',$pic,'id_registrasi'); 

        }

    $id_registrasi = $this->input->post('id_registrasi');
    $total_fee = $this->input->post('total_fee');
    if(isset($data['data2'])) {
        
            $pic = array();
            foreach($data['data2'] as $pi){
                $id_registrasi = $pi['id_registrasi'];
                $total_fee = $pi['total_fee'];
                $tmp = array(
                
                    'id_registrasi' => $pi['id_registrasi'],
                    'status_fee' => 3,
                    'fee' => $pi['total_fee'],
                  
                );
               
                $pic[] = $tmp;

            }

            $this->db->update_batch('registrasi_jamaah',$pic,'id_registrasi'); 

        }


       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }



   
    
      public function get_pic($id_booking,$id_affiliate=''){
      
        $sql ="SELECT * from detail_fee_posting_jamaah_admin WHERE  id_booking  = ? and id_affiliate = ?
              ";
        return $this->db->query($sql,array($id_booking,$id_affiliate))->result_array();    

    }

   

    public function get_pic_posting($id_booking,$id_affiliate=''){
 

         $sql = "SELECT * from fee_posting_jamaah_admin_new where id_booking = '$id_booking' and id_affiliate = '$id_affiliate'";
        return $this->db->query($sql)->row_array();  
    }


    public function update_pengajuan($id_booking){

        $arr = array(
        
           'proses' => 'ON PROSES',
 
        );       
         $this->db->update($this->_table,$arr,array('id_booking'=>$id_booking));

  
    }


   private function _get_datatables_query()
    {
        
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
