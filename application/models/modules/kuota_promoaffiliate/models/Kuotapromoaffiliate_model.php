<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kuotapromoaffiliate_model extends CI_Model{
   
   
    private $_table="kuota_booking";
    private $_primary="id_booking_kuota";

     private $_jamaah_kuota="jamaah_kuota";
    private $_id_kuota="id_kuota";

    private $_booking="booking";
    private $_id_booking="id_booking";

 private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi_jamaah="id_registrasi";

    public function generate_kode($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('md');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }

        public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save_get_kuota($data){
      $schedule = $this->input->post('radio');
      $jml_kuota = $this->input->post('jml_kuota');
     $stored_procedure = "CALL view_schedule_promo(?)";
             $query =  $this->db->query($stored_procedure,array('id_schedule'=>$schedule))->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
      }



        $initial = "KP1";
        $arr = array(
                                          
            'id_affiliate' => $this->session->userdata('id_user'),
            'id_affiliate_type' => $this->session->userdata('id_affiliate_type'),
            'status'=>0,
            'status_fee'=>3,
            'id_schedule'=>$id_schedule,
            'schedule'=>$date_schedule,
            'harga' => $harga,
            'kode_unik' => $data['unique_digit'],
            'tgl_pembelian' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status_kuota'=>3,
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_table,$arr);
        $id_booking_kuota =  $this->db->insert_id(); 
        $this->db->update($this->_table,
                    array('invoice'=> $this->generate_kode($initial.$id_booking_kuota)),
                    array('id_booking_kuota'=>$id_booking_kuota));

        $stored_procedure = "CALL all_refrensi_fee(?)";
             $query =  $this->db->query($stored_procedure,array('id_user'=>$this->session->userdata('id_user')))->result_array();


              foreach($query as $key=>$value){
                $sponsor = $value['id_refrensi'];
                $fee_sponsor_paketpromo  = $value['fee_sponsor_paketpromo'];
                $fee_posting_paketpromo = $value['fee_posting_paketpromo'];

           $biaya_mutasi_ = $this->get_key_val();
          foreach ($biaya_mutasi_ as $key => $value){
            $out[$key] = $value;
          }

        $jml_kuota = $this->input->post('jml_kuota');
        for($i=0; $i < $jml_kuota; $i++) {
            $arr = array(
                'id_booking_kuota' => $id_booking_kuota,   
                'kd_kuota' => $this->session->userdata('id_user').'-'.($i + 1),
                'nama' => $this->session->userdata('id_user').'-'.$id_booking_kuota.'-'.($i + 1),
                'id_affiliate' => $this->session->userdata('id_user'),
                'id_affiliate_type' => $this->session->userdata('id_affiliate_type'),
                'sponsor' => $sponsor,
                'biaya_mutasi'=>$out['BIAYA_MUTASI_NAMA'],
                'harga' => $harga,
                'id_schedule'=>$id_schedule,
                'schedule'=>$date_schedule,
                'fee'=>$fee_posting_paketpromo,
                'fee_refrensi' => $fee_sponsor_paketpromo,
                'status'=>0,
                 'status_fee'=>3,
                 'status_fee_refrensi'=>0,
                 'tgl_pembelian' => date('Y-m-d H:i:s'),
                 'create_date' => date('Y-m-d H:i:s'),
                 'create_by'=>$this->session->userdata('id_user'),
                 'status_kuota'=>3,
               );

        
        $this->db->insert($this->_jamaah_kuota,$arr);
         
     
        }

    }


         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('kuota_promoaffiliate/report_kuota/'.$id_booking_kuota.'');
            return true;
        }
    }

    public function save($data){

      $invoice = $this->input->post('invoice');
      $id_schedule = $this->input->post('id_schedule');
      $schedule = $this->input->post('schedule');
      $id_kuota = $this->input->post('id_kuota');
     
         $stored_procedure = "CALL view_schedule_promo_booking_seats(?)";
             $query =  $this->db->query($stored_procedure,array('id_schedule'=>$id_schedule))->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
      }
      $initial = "PK1";
        $arr = array(
                                          
            'id_user_affiliate' => $data['id_affiliate'],
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'schedule' => $data['schedule'],
            'harga' => $data['harga'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'tempjmljamaah'=>1,
            'status_fee'=>4,
            'tipe_jamaah'=>1,
            'status'=>1
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        
        
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));

          $arr = array(
                'id_booking'=> $id_booking,
                'invoice'=>$this->generate_kode($initial.$id_booking),
                'id_schedule'=> $data['id_schedule'],
                'id_affiliate'=> $data['id_affiliate'],
                'id_product'=> $id_product,
                'embarkasi'=> $embarkasi,
                'tgl_daftar'=>date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'telp_2'=> $data['telp_2'],
                'email'=> $data['email'],
                'ket_keberangkatan'=>$data['select_status_hubungan'],
                 'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'room_type'=> 1,
                'category'=> $room_category,
                'merchandise'=> $data['select_merchandise'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> $data['refund'],
                'handling'=> 0,
                'akomodasi'=> $data['akomodasi'],
                'fee'=> $data['fee_posting_paketpromo'],
                'fee_input'=> 0,
                'harga_paket'=> $data['harga'],
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status'=> 1,
                'tipe_jamaah'=>1,
                'status_fee'=>4,

        );       

        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

    

         $arr = array(
            'status'=>2,
            'id_jamaah' => $this->generate_id_jamaah($id_registrasi),   
                
         );
            $this->db->update('jamaah_kuota',$arr,array('id_kuota'=>$id_kuota)); 
       

         $arr = array(
            'id_booking'=> $id_booking,
            'id_registrasi'=> $id_registrasi,
            'id_jamaah'=> $this->generate_id_jamaah($id_registrasi),
            'id_affiliate'=> $data['id_affiliate'],
            'no_pasport'=> $data['no_pasport'],
            'issue_office'=> $data['issue_office'],
            'isui_date'=> $data['isue_date'],
            'status_identitas'=> $data['status_identitas'],
            'status_kk'=> $data['status_kk'],
            'status_photo'=> $data['status_photo'],
            'status_pasport'=> $data['status_pasport'],
            'status_vaksin'=> $data['status_vaksin'],
            'hubkeluarga'=> 9,
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 1,
        );       
        $this->db->insert('manifest',$arr);
        
         $stored_procedure = "CALL all_refrensi_fee(?)";
             $query =  $this->db->query($stored_procedure,array('id_user'=>$this->session->userdata('id_user')))->result_array();


              foreach($query as $key=>$value){
                $sponsor = $value['id_refrensi'];
                $fee_posting_paketpromo = $value['fee_posting_paketpromo'];
                $fee_sponsor_paketpromo  = $value['fee_sponsor_paketpromo'];
                

          
          }

       $arr = array(    

                'invoice' => $this->generate_kode($initial.$id_booking),
                'id_registrasi' => $id_registrasi,
                'id_booking'=> $id_booking,
                'id_schedule'=> $data['id_schedule'],
                'id_product'=> $id_product,
                'id_affiliate' => $data['id_affiliate'],
                'sponsor' => $sponsor,
                'fee' => $fee_posting_paketpromo,
                'fee_sponsor' => $fee_sponsor_paketpromo,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status' => 1,
                 'status_fee'=> 4,
                'status_free'=> 4,
                'status_fee_sponsor'=> 1
              );

              $this->db->insert('fee',$arr);

        $arr = array(    
            
            'id_registrasi' => $id_registrasi,
            'id_booking'=> $id_booking,
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $id_product,
            'id_affiliate' => $data['id_affiliate'],
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 1

        );

        $this->db->insert('list_gaji',$arr);

   

         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $id_booking,
            
            'status'=>1
        );       
        $this->db->insert('pengiriman',$arr);

       

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $data['id_booking'],
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>1
          );
        $this->db->insert('visa',$arr);

        $sql = "call hitung_muhrim(?) ";
           $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
          foreach($query as $key=>$value){
            // $id_registrasi = $value['id_registrasi'];
            $biaya_muhrim = $value['muhrim'];
          }


        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 





       $sql = "call v_visa(?) ";
       $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
          foreach($query as $key=>$value){
            // $id_registrasi = $value['id_registrasi'];
            $biaya_visa = $value['Total_VISA'];
          }

        $arr = array(
          'visa'=>$biaya_visa,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 


        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }
    
    
      function cek($no_identitas){
        $invoice = $this->input->post('invoice');
        $query = $this->db->query("SELECT * FROM data_jamaah_aktif Where no_identitas ='$no_identitas' and invoice='$invoice'");
        return $query;
    }

      function cek_id_jamaah($id_jamaah){
        $id_jamaah = $this->input->post('id_jamaah');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where id_jamaah ='$id_jamaah'");
        return $query;
    }
    
    
    public function get_data($offset,$limit,$q=''){
    
      $user_id=$this->session->userdata('id_user');
         $sql = " SELECT * FROM grouping_jamaah_kuota_admin  
                    WHERE  status_kuota ='3'and id_affiliate='".$user_id."'
                    ";
        
        if($q){
         

            $sql .=" AND affiliate LIKE '%{$q}%'
                    OR id_affiliate LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY tgl_pembelian DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    

    
    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

    function get_fee_jamaah_kuota($affiliate_type='') {
      $this->db->where("id_affiliate_type",$affiliate_type);
      return $this->db->get("affiliate_type");
    }


     public function get_booking_kuota($id_booking,$id){
 
        // $user_id=$this->session->userdata('id_user');
        //  $sql = "SELECT * from faktur_jamaah_kuota where id_booking_kuota = '$id_booking_kuota'and id_affiliate='$user_id'";
  
        // return $this->db->query($sql)->row_array();  

      $stored_procedure = "call faktur_jamaah_kuota(?,?)";
         return $this->db->query($stored_procedure,array('id_booking_kuota'=>$id_booking,
            'id_affiliate'=>$id
            ))->row_array();
    }

    function lap_data_jamaah($id_booking_kuota) {
        $this->db->select('*');
        $this->db->from('faktur_jamaah_kuota');
        $this->db->where('id_booking_kuota',$id_booking_kuota);
        $this->db->where('id_affiliate',$this->session->userdata('id_user'));
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }



     public function get_jamaah_kuota_aktif($id_booking_kuota){
        $user_id=$this->session->userdata('id_user');
        $sql ="SELECT a.*,b.nama as affiliate,c.invoice FROM jamaah_kuota as a, affiliate as b, kuota_booking as c
                where a.id_affiliate = b.id_user and a.id_booking_kuota = c.id_booking_kuota and a.`status`='1' and  a.status_kuota='3' and  a.id_booking_kuota  = ?
                and a.id_affiliate='".$user_id."'
              ";
        return $this->db->query($sql,array($id_booking_kuota))->result_array();    

    }


      public function get_jamaah_kuota_mutasi_nama($id_jamaah){
    
        $sql ="SELECT * from detail_jamaah_kuota_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

    }

   
     public function get_booking_kuota_aktif($id_booking_kuota){
 

         $sql = "SELECT * from grouping_jamaah_kuota_admin where id_booking_kuota = '$id_booking_kuota'";
  
        return $this->db->query($sql)->row_array();  
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

     function searchItem_paket_promo($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure){
     
        
        $jumlah_jamaah = $this->input->post('jumlah_jamaah');
          $sql = "SELECT * from view_schedule_promo where  BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.' - '.$row->hari.' hari</strong></td>
                   
                   <td><strong>'.number_format($row->harga).'</strong></td>
                 
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }
}
