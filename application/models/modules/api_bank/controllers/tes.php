<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_bank extends CI_Controller {
   private $_table="aktivasi";
    private $_primary="id_aktivasi";

    private $_claim_feefree="claim_feefree";
    private $_id_claim_feefree="id_claim_feefree";

    private $_table_aktivasi_jamaah="aktivasi_jamaah";
    private $id_aktivasi_jamaah="id_aktivasi_jamaah";

	public function index(){
		 $this->load->view('test');
		 ini_set('display_error', 0);
		 error_reporting(0);
	}

	function getbank(){		
		$nominal = $_GET['jumlah'];
		$bank = $_GET['bank'];
		$no_rek = $_GET['no_rek'];
		$tgl_bank = $_GET['tgl_bank'];
		try{
			if(!isset($nominal)) throw new Exception("Error Processing Request", 1);
			if(!isset($bank)) throw new Exception("Error Processing Request", 1);
			if(!isset($no_rek)) throw new Exception("Error Processing Request", 1);
			if(!isset($tgl_bank)) throw new Exception("Error Processing Request", 1);
		}catch(Exception $e){
			die('Isian Data Tidak Lengkap');
		}

		$var = $tgl_bank;
		$date = str_replace('/', '-', $var);
		// $keter = $bank."/".$no_rek."/".$nominal."/".$keterangan;

		$sql = "SELECT *
				FROM view_transaksi 
				WHERE nominal = '".$nominal."' 
					AND status_aktif = '0'";
        $query = $this->db->query($sql)->result_array();

		if(count($query) > 0){
			foreach($query as $key=>$value){
				$id_booking = $value['id_booking'];
				$nominal = $value['nominal'];
				$jumlah_transaksi = count($query);
					   
				$id_schedule= $value['id_schedule'];
				$id_product = $value['id_product'];
				$id_affiliate = $value['id_affiliate'];
				$status_claim_fee = $value['status_claim_fee'];
				$autosistem ='autosistem';

				if($status_claim_fee==1){
					  $arr = array(           
			              'no_rek' => $no_rek,
			              'bank' => $bank,
			              'nominal' => $nominal,
			              'tgl_bank'=> $date,
			              // 'keterangan' => $keterangan,
			          );       
			         $this->db->insert('historis_transaksi',$arr);

			          $arr = array(
			             'id_booking'=> $id_booking,
			            'id_schedule'=> $id_schedule,
			            'id_product'=> $id_product,
			            'id_user_affiliate'=> $id_affiliate,
			            'nominal_pembayaran' => $nominal,
			            'payment_date' => $date,
			            'create_date' => date('Y-m-d H:i:s'),
			            'create_by'=>$autosistem 
			        );       
			   		$this->db->trans_begin(); //transaction initialize
			        $this->db->insert($this->_table,$arr);
			        $id_aktivasi =  $this->db->insert_id(); //get last insert ID
	        
			         $arr = array(
			           'status_claim_fee'=>$status_claim_fee ,
			            'status'=>1,
			            'payment_date' => $date,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			        );       
			        $this->db->update('booking',$arr,array('id_booking'=>$id_booking)); 

			        $arr = array(
			            'status_claim_fee'=>$status_claim_fee ,
			            'status'=>1,
			            'payment_date' => $date,
			            'cashback' => 2,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			        );  
			        $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$id_booking));  
	    
			        $arr = array(
			            'payment_date' => $date,
			            'status' => 1,
			            'status_fee'=> 1,
			            'status_free'=> 1,
			            'status_fee_sponsor'=>1,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			            
			        );      
			       $this->db->update('fee',$arr,array('id_booking'=>$id_booking)); 

			        $arr = array(
			            'payment_date' => $date,
			             'status'=>1,
			             'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			           
			        );  
		          $this->db->update('fee_input',$arr,array('id_booking'=>$id_booking));  

			         $arr = array(
			             'payment_date' => $date,
			             'status'=>1,
			             'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			        );  
	         		 $this->db->update('list_gaji',$arr,array('id_booking'=>$id_booking));  
			       
			        $arr = array(
			                             
			            'id_booking' => $id_booking,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			            'status'=>1
			        );       
			         $this->db->update('pengiriman',$arr,array('id_booking'=>$id_booking));  
			         $this->db->delete('transaksi',array('id_booking'=>$id_booking));
			        if($this->db->trans_status()===false){
			            
			            $this->db->trans_rollback();
			           
			            return false;    
			            
			        }else{
			            
			            $this->db->trans_complete();
			            
			            return true;
			        }
				}elseif($status_claim_fee==2){
					 $arr = array(           
				              'no_rek' => $no_rek,
				              'bank' => $bank,
				              'nominal' => $nominal,
				              'tgl_bank'=> $date,
				          );       
				      $this->db->insert('historis_transaksi',$arr);
			          $arr = array(
			            'id_booking'=> $id_booking,
			            'id_schedule'=> $id_schedule,
			            'id_product'=> $id_product,
			            'id_user_affiliate'=> $id_affiliate,
			            'nominal_pembayaran' => $nominal,
			            'payment_date' => $date,
			            'create_date' => date('Y-m-d H:i:s'),
			            'create_by'=>$autosistem 
			        );   
			         $this->db->trans_begin(); //transaction initialize
			        $this->db->insert($this->_table,$arr);
			        $id_aktivasi =  $this->db->insert_id(); //get last insert ID
		   			
		   			$arr = array(
			            'id_booking'=> $id_booking,
			            'id_schedule'=> $id_schedule,
			            'id_affiliate'=> $id_affiliate,
			            'id_product'=> $id_product,
			            'claim_date' => $date,
			            'create_date' => date('Y-m-d H:i:s'),
			            'create_by'=>$autosistem ,
			            'status' => 1,
			            'status_feefree' => 2,
			        );       
		           $this->db->trans_begin(); //transaction initialize
		        
			       $this->db->insert($this->_claim_feefree,$arr);
			       $id_claim_feefree =  $this->db->insert_id();
		     if ($id_product==4){
			        $arr = array(
			             'payment_date' => $date,
			             'status'=>1,
			             'status_free'=>3,
			             'status_fee' => 3,
			             'status_fee_sponsor'=>1,
			             'id_claim'=>$id_claim_feefree,
			             'update_by'=>$date,
			             'update_date' => date('Y-m-d H:i:s'),
			        );  
          			$this->db->update('fee',$arr,array('id_booking'=>$id_booking)); 
	        }else{ 
			         $arr = array(
				             'payment_date' => $date,
				             'status'=>1,
				             'status_free'=>1,
				             'status_fee' => 3,
				             'status_fee_sponsor'=>1,
				             'id_claim'=>$id_claim_feefree,
				             'update_by'=>$date,
				             'update_date' => date('Y-m-d H:i:s'),
				        );  
	          		 $this->db->update('fee',$arr,array('id_booking'=>$id_booking)); 
    		}
			    	$arr = array(
			            'status_claim_fee'=>$status_claim_fee,
			            'status'=>1,
			            'payment_date' => $date,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			        );       
			        $this->db->update('booking',$arr,array('id_booking'=>$id_booking)); 
			         $arr = array(
			            'status_claim_fee'=>$status_claim_fee,
			            'status'=>1,
			            'payment_date' =>$date,
			            'cashback' =>2,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			        );  
			         $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$id_booking));  

			         $arr = array(
			            'payment_date'=>$date,
			             'status'=>1,
			            'update_by'=>$autosistem,
			            'update_date' => date('Y-m-d H:i:s'),
			        );  
			        $this->db->update('fee_input',$arr,array('id_booking'=>$id_booking)); 
			        $arr = array(
			            'payment_date' =>$date,
			            'status'=>1,
			            'update_by'=>$autosistem,
			            'update_date' =>date('Y-m-d H:i:s'),
			        );    
			        $this->db->update('list_gaji',$arr,array('id_booking'=>$id_booking));
			        $arr = array(        
		            'id_booking' => $id_booking,
		            'update_by'=>$autosistem,
		            'update_date' =>date('Y-m-d H:i:s'),
		            'status'=>1
	      		 	 );       
	        		$this->db->update('pengiriman',$arr,array('id_booking'=>$id_booking));

				}elseif($status_claim_fee==5){
					 
				}else{ 
						
				}

					
			}
		}
	
	}



}