<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class List_jamaah extends CI_Controller {
	var $folder = "registrasi_affiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','jamaah_registration');	
		$this->load->model('registrasiaffiliate_model');
		
	}


	public function index() {
		

		$this->template->load('template/template', $this->folder.'/list_jamaah');
		
	}


	function ajax_list() {
		/*Default request pager params dari jeasyUI*/
		$offset = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$limit  = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort  = isset($_POST['sort']) ? $_POST['sort'] : 'create_date';
		$order  = isset($_POST['order']) ? $_POST['order'] : 'desc';
		$id_booking = isset($_POST['id_booking']) ? $_POST['id_booking'] : '';
		$tgl_dari = isset($_POST['tgl_dari']) ? $_POST['tgl_dari'] : '';
		$tgl_sampai = isset($_POST['tgl_sampai']) ? $_POST['tgl_sampai'] : '';
		$search = array('id_booking' => $id_booking, 'tgl_dari' => $tgl_dari, 'tgl_sampai' => $tgl_sampai);
		$offset = ($offset-1)*$limit;
		$data   = $this->registrasiaffiliate_model->get_data_jamaah_ajax($offset,$limit,$search,$sort,$order);
		$i	= 0;
		$rows   = array(); 

		foreach ($data['data'] as $r) {
			// $tgl_bayar = explode(' ', $r->tgl_pinjam);
			// $txt_tanggal = jin_date_ina($tgl_bayar[0],'p');		

			//array keys ini = attribute 'field' di view nya
			// $anggota = $this->general_m->get_data_anggota($r->anggota_id);   

			$rows[$i]['id_booking'] = $r->id_booking;
			$rows[$i]['invoice'] ='#'.$r->invoice;
			$rows[$i]['paket_txt'] = $r->paket;
			$rows[$i]['departure'] = $r->departure;
			$rows[$i]['create_date'] = $r->create_date;
			$rows[$i]['tgl_keberangkatan'] = $r->tgl_keberangkatan;
			$rows[$i]['jumlah_jamaah'] = $r->jumlah_jamaah.' Orang';
			// $rows[$i]['bayar'] = '<p></p><p>
			// <a href="'.site_url('angsuran').'/index/' . $r->id . '" title="Bayar Angsuran"> <i class="fa fa-money"></i> Bayar </a></p>';
			$i++;
		}
		//keys total & rows wajib bagi jEasyUI
		$result = array('total'=>$data['count'],'rows'=>$rows);
		echo json_encode($result); //return nya json
	}

	

}