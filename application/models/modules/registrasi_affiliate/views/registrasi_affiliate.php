<div id="page-wrapper">
  <div class="row">
   <div class="col-lg-12">
        
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Registration</b>
        </div>
        <div class="panel-body">
        <form  role="form" method="post"  class="form-horizontal" action="<?php echo base_url();?>registrasi_affiliate/update">
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Registration Data</b>
                </div>
                  <?php if($this->session->flashdata('Warning')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Warning! </strong><strong><?php echo $this->session->flashdata('Warning'); ?>  </strong>
                                  </div>
                              <?php } ?>
                <!-- /.panel-heading -->
                  <div class="panel-body">
                    <div class="form-group">
                    <label class="col-lg-2 control-label">Category</label>
                    <div class="col-lg-5">
                      
                     <select class="form-control" name="paket" id="paket">
                        <option value="">Pilih Category</option>
                       <!--  <?php foreach($select_product as $sp){?>
                            <option required value="<?php echo $sp->id_product;?>"><?php echo $sp->nama;?> </option>
                        <?php } ?> -->
                      <?php foreach($select_roomcategory as $se){?>
                        <option value="<?php echo $se->id_room_category;?>"><?php echo $se->category;?></option>
                    <?php } ?>
                    </select>
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Embarkasi</label>
                      <div class="col-lg-5">
                      <select class="form-control" name="departure" id="departure" >
                          <option value="">Pilih Embarkasi</option>
                          <?php foreach($select_embarkasi as $se){?>
                              <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>

               <div class="form-group">
                          <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' id='datepicker_tahun_keberangkatan1'> -->
                                <input type='text' name="datepicker_tahun_keberangkatan" id="datepicker_tahun_keberangkatan1" class="form-control" placeholder="YYYY/2016" required/>
                             <!--    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                             
                          </div>
                    </div>

                     <div class="form-group">
                          <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                <input type='text' name="datepicker_keberangkatan" id="datepicker_keberangkatan1" class="form-control" placeholder="MM/January" required/>
                               <!--  <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                          </div>

                    </div>
                     <div class="form-group">
                          <label class="col-lg-2 control-label">Jumlah Jamaah : </label>
                          <div class="col-lg-5">
                                <input  name="jumlah_jamaah"  class="form-control" placeholder="Jumlah Jamaah" required/>
                          </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-10">
                        <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check Waktu Keberangkatan</button>
                         
                    </div>
                  </div>
                
                  <div class="table-responsive">
                   <table id="data-table" style="width:100%" align="center" class="table table-striped table-bordered table-hover" >
                    <caption>Pilih Jadwal</caption>
                    <thead>
                        <tr>
                            <!-- <th width="2%">Check </th> -->
                           <th width="8%"><strong>Paket</strong></th>
                            <th width="10%"><strong>Embarkasi</strong></th>
                            <th width="10%"><strong>Tgl/Bln </strong></th>
                            <th width="8%"><strong>Waktu </strong></th>
                            <th width="5%"><strong>Jml Kursi</strong></th>
                            <th width="5%"><strong>Tipe Kamar</strong></th>
                            <th width="5%"><strong>Category</strong></th>
                            <th width="5%"><strong>Keterangan</strong></th>
                            <th width="5%"><strong>Harga</strong></th>
                            <th width="5%"><strong>maskapai</strong></th>
                            <th width="5%"><strong>PILIH</strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <tr><td colspan="10"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>

                    </tbody>
                </table>
                </div>
                   

                   </div>
                </div>
              </div>
            </div><!--end col-->

        
           
           </div>
       </form>
     </div>
     </div>
     </div>
  </div><!--end row-->
</div>

<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
      
       $(function () {
                $('#datepicker_keberangkatan1').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
    </script>

    

<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('registrasi_affiliate/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('registrasi_affiliate/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('registrasi_affiliate/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });

    $("#check").on('click', function (e){
    e.preventDefault();
    var paket = $("#paket").val();
    var departure = $("#departure").val();
    var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
    var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
    console.log(paket);
    console.log(departure);
    console.log(datepicker_tahun_keberangkatan);
    console.log(datepicker_keberangkatan);
    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>registrasi_affiliate/searchItem',
             // data:'from='+from+'&to='+to
            data:'q='+paket+'&l='+departure+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});


     var site = "<?php echo base_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                serviceUrl: site+'registrasi_affiliate/search',
                onSelect: function (suggestion) {
                    $('#refund').val(''+suggestion.refund);

                }
            });
        });


</script>
<script type="text/javascript">


</script>
 <script type="text/javascript">
  jQuery(document).ready(function(){
  $(".chosen-select").chosen({width: "95%"}); 
});


 </script>