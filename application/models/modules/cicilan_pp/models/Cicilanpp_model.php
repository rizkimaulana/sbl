<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cicilanpp_model extends CI_Model{
   
   
   

    private $db2;
    private $_table="pindah_paket_konven";
    private $_primary="id_groupbilyet";
    private $_booking="booking";
    private $_id_booking="id_booking";
    private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi_jamaah="id_registrasi";
    public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }

    public function generate_kode($idx){
        $today=date('ymd');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }


   
    public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save_pp($data){
      $harga_kuota = $this->input->post('harga');
       $schedule = $this->input->post('radio');
      $initial = "PP";
             
       $sql = "call schedule_reguler(?) ";
        $query = $this->db->query($sql,array('id_schedule'=>$schedule))->result_array();
       
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
      }

      



       $arr = array(
                                   
            'id_user_affiliate' => 'SBL01',
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'harga' => $harga,
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'kode_unik'=> $data['unique_digit'],
            'tipe_jamaah'=>6,
            'status'=>0
        );       
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));


     
         $arr = array(
                'id_booking'=> $id_booking,
                'invoice'=> $this->generate_kode($initial.$id_booking),
                'id_schedule'=> $id_schedule,
                'id_affiliate'=> $data['sponsor'],
                'id_sahabat'=> $data['idsahabat'],
                'id_product'=> $id_product,
                'embarkasi'=> $embarkasi,
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'telp_2'=> $data['telp_2'],
                'email'=> $data['email'],
                'ket_keberangkatan'=>9,
                'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'room_type'=> 1,
                'category'=> $room_category,
                'merchandise'=> $data['select_merchandise'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> 0,
                'akomodasi'=> $data['akomodasi'],
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> $harga,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status'=> 0,
                'tipe_jamaah'=>6,
                'dp_angsuran'=> $data['dp'],
                'angsuran'=> $data['jml_cicilan'],
                'jml_angsuran'=> $data['cicilan_perbulan'],

        );     
 
        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
       
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

$arr = array(
            'id_booking'=> $id_booking,
            'id_registrasi' => $id_registrasi,
            'id_jamaah'=> $this->generate_id_jamaah($id_registrasi),
            'id_affiliate'=> $data['sponsor'],
            'no_pasport'=> $data['no_pasport'],
            'issue_office'=> $data['issue_office'],
            'isui_date'=> $data['isue_date'],
            'status_identitas'=> $data['status_identitas'],
            'status_kk'=> $data['status_kk'],
            'status_photo'=> $data['status_photo'],
            'status_pasport'=> $data['status_pasport'],
            'status_vaksin'=> $data['status_vaksin'],
            'hubkeluarga'=> $data['hubungan'],
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 0,
        );       
        $this->db->insert('manifest',$arr);


        // $arr = array(    

        //     'id_registrasi' => $id_registrasi,
        //     'id_booking'=> $id_booking,
        //     'id_affiliate' => $data['userid'],
        //     'sponsor' => 0,
        //     'fee' => $data['fee_pindah_paket'],
        //     'fee_sponsor' => 0,
        //     'create_by'=>$this->session->userdata('id_user'),
        //     'create_date'=>date('Y-m-d H:i:s'),
        //     'status' => 0,
        //      'status_fee'=> 0,
            
        // );

        // $this->db->insert('fee',$arr);

         // $arr = array(    

         //          'invoice' => $this->generate_kode($initial.$id_booking),
         //          'id_registrasi' => $id_registrasi,
         //          'id_booking'=> $id_booking,
         //          'id_schedule'=> $id_schedule,
         //          'id_product'=> $id_product,
         //          'id_affiliate' => $data['id_affiliate'],
         //          'sponsor' => $data['sponsor'],
         //          'fee' => $fee_pindah_paket,
         //          'fee_sponsor' => 0,
         //          'create_by'=>$this->session->userdata('id_user'),
         //          'create_date'=>date('Y-m-d H:i:s'),
         //          'status' => 0,
         //           'status_fee'=> $data['status_fee'],
         //          'status_free'=> 0
         //      );

         //      $this->db->insert('fee',$arr);



      $sponsor = $this->input->post('sponsor');
        $sql = "SELECT COUNT(*) as jml,id_user from affiliate where id_affiliate ='$sponsor'";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_user = $value['id_user'];
        $jml = $value['jml'];
      }
      if ($jml == 1){
          $arr = array(      
            'id_affiliate' => $id_user,  
        );       
        $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));

         $arr = array(      
            'id_affiliate' => $id_user,  
        );       
        $this->db->update('fee',$arr,array('id_registrasi'=>$id_registrasi));
      }


      
         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $data['id_booking'],
            
            'status'=>0
        );       
        $this->db->insert('pengiriman',$arr);

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $id_booking,
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>0
          );
        $this->db->insert('visa',$arr);


        $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_jamaah'=> $this->generate_id_jamaah($initial.$id_registrasi),
         
          );
        $this->db->insert('tgl_persyaratan',$arr);

       
       $sql = "call hitung_muhrim(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
          foreach($query as $key=>$value){
            $id_registrasi = $value['id_registrasi'];
            $biaya_muhrim = $value['muhrim'];
          }

        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  



         $sql = "call v_visa(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
          foreach($query as $key=>$value){
            $id_registrasi = $value['id_registrasi'];
            $biaya_visa = $value['Total_VISA'];
          }

        $arr = array(
          'visa'=>$biaya_visa,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  

        $arr = array(
          'status'=>1,
         
        );    
          $this->db2->update('t_cicilan',$arr,array('userid'=>$data['userid']));  

        $arr = array(
                'id_sahabat' =>$data['idsahabat'],
                'tgl_pindahpaket'=> date('Y-m-d H:i:s'),
                'status' =>0
                );
              $this->db->insert('pindah_paket',$arr);

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
             redirect('cicilan_pp/report_registrasi/'.$id_booking.'');
            return true;
        }

       

        
    }


    function cek($idsahabat){
        // $userid = $this->input->post('userid');
        $query = $this->db->query("SELECT * FROM pindah_paket Where id_sahabat ='$idsahabat' and status in('0','1') ");
        return $query;
    }

    
    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }



function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
      
          $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="11"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.' - '.$row->hari.' hari</strong></td>
                   <td><strong>'.$row->keterangan.'</strong></td>
                   <td><strong>'.number_format($row->harga).'</strong></td>
                   <td><strong>'.$row->status_jadwal.'</td>
                   <td><strong>'.$row->maskapai.'</strong></td>
                 
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }

 function search_pp($search){
     
     $stored_procedure = "call pindah_paket(?) ";
            $query = $this->db->query($stored_procedure,array('userid'=>$search,
              ));
    
            if (empty($query->result())){
                
               echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Data Yang Anda Cari Kosong </h2></td></tr>';
                   exit;
               
               }else{

                
                foreach ($query->result() as $row){    


                    echo'<tr>
                           
                           <td><strong>'.$row->userid.'</strong></td>
                           <td><strong>'.$row->jml_cicilan.'</strong></td>
                        <td><div class="btn-group"><a href="'.base_url().'cicilan_pp/pindah_paket/'.$row->userid.'"  class="btn btn-info btn-sm" ><i class="glyphicon glyphicon-pencil"></i>MUTASI</a>
                        </div>
                        </td>
                        </tr>';

               }

               }


   }
    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

     function get_all_provinsi_manasik() {
        $this->db->distinct();
        $this->db->select('provinsi_id,provinsi');
        $this->db->from('view_manasik');
        $query = $this->db->get();
        
        return $query->result();
    }

     public function get_report_registrasi($id_booking,$id){
 
       $stored_procedure = "call data_laporan_jamah_belumaktif_all(?,?)";
         return $this->db->query($stored_procedure,
            array('id_booking'=>$id_booking,
                    'create_by'=>$id
            ))->row_array();   
}
         public function get_cicilan($id){
 
        
         $sql = "SELECT * from pindah_paket_konven where userid = '$id' ";
  
        return $this->db2->query($sql)->row_array();  
    }
}
