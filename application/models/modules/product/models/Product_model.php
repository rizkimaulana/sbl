<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model{
   
   
    private $_table="product";
    private $_primary="id_product";

    public function save($data,$data2){
    //     $this->load->database();
    // $this->load->model('my_model');

    // $data_user = array(
    //   'customerName' => $this->input->post('customerName'),
    //   'phone'        => $this->input->post('phone')
    // );

    // $data_address = array(
    //   'address'    => $this->input->post('address'),
    //   'city'       => $this->input->post('city'),
    //   'zipcode'    => $this->input->post('zipcode')
    // );    

    // $this->my_model->insert_entry($data_user, $data_address);

    //     $this->db->insert('customers', $data_user);

    // $data_address['customerID'] = $this->db->insert_id();

    // $this->db->insert('addresses', $data_address);
        // $cek = $this->db->select('kode')->where('nama',$data['nama'])->get('product')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'kode' => $data['kode'],
            'nama' => $data['nama'],
            'harga' => $data['harga'],
            // 'hari' => $data['select_hari'],
            'status' => $data['select_status'],
           
        );       
        
         return $this->db->insert('product',$arr);

        //  $arr2 = array(
        
        //     'kode' => $data2['kode'],
        //     'nama' => $data2['nama'],
        //     'status' => $data2['select_status'],
           
        // );       
        
        // return $this->db->insert('product_copy',$arr2);
    }


    public function update($data){
        
        $arr = array(
        
           'kode' =>$data['kode'],
            'nama' =>$data['nama'],
            'harga' => $data['harga'],
            // 'hari' => $data['select_hari'],
            'status' =>$data['status'],
        );       
              
        return $this->db->update($this->_table,$arr,array('id_product'=>$data['id_product']));
    }

    // public function UpdateProduct($kode, $nama, $status){
    //     $query = $this->db->query("UPDATE product set kode='$kode',
    //                                 nama ='$nama',
    //                                 status = '$kdstatus'
    //                                 where id_product = '$id_product'
    //                                 ");
    // }

    // public function update3()
    // {
    //     $this->db->update($this->_table, $data, $where);
    //     return $this->db->affected_rows();
    // }
    
    public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT a.*,b.keterangan FROM product a 
                    LEFT JOIN status b ON b.kdstatus = a.status
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND a.kode LIKE '%{$q}%' 
            		OR a.nama LIKE '%{$q}%'
            		OR b.keterangan LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_product DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     public function delete($id_product){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_product'=>$id_product));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
    
    public function delete_by_id($id_product)
    {
        $this->db->where('id_product', $id_product);
        $this->db->delete($this->_table);
    }
    
    
}
