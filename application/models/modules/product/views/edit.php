 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit User</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>product/update">
             
            <div class="form-group">
                <label>Kode</label>
                <input type="hidden" name="id_product" value="<?php echo $id_product;?>">
                <input class="form-control" name="kode" value="<?php echo $kode;?>" required autofocus>
            </div>
            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" name="nama" value="<?php echo $nama;?>" required>
            </div>
     <!--        <div class="form-group">
                <label>Harga</label>
                <input class="form-control" name="harga" value="<?php echo $harga;?>" required>
            </div> -->
      <!--       <div class="form-group">
                <label>Jumlah Hari</label>
                <select class="form-control" name="select_hari">
                    <?php foreach($select_hari as $sj){ 
                                     $selected = ($hari == $sj->hari)  ? 'selected' :'';?>
                                    <option value="<?php echo $sj->hari;?>" <?php echo $selected;?>><?php echo $sj->hari;?> </option>
                                <?php } ?>
                </select>
            </div> -->

            <div class="form-group">
                <label>Status</label>
                 <select class="form-control" name="status">
                     <?php foreach($select_status as $st){ 
                    
                        $selected = ($status == $st->kdstatus)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                    <?php } ?>
                </select>
            </div>
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>product" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->
