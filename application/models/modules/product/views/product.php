
<div class="page-head">
            <h2>Product </h2>
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Master</a></li>
              <li class="active">Product</li>
            </ol>
        </div>      
<div id="page-wrapper">
   <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Product</b>
                </div>
                <!-- /.panel-heading -->
                 <div class="panel-body">
                    
                      <form role="form">
                           
                             <div class="form-group input-group col-lg-4">
                                 <span class="input-group-btn">
                                     <a href="<?php echo base_url();?>product/add" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Add
                                       </a>
                                </span>
                                
                                <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div>
                        </form>           
                          
                          <div class="table-responsive">
                              <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <!-- <th>Harga</th> -->
                                    <!-- <th>Jumlah Hari</th> -->
                                    <th>Status</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <!--Appended by Ajax-->
                            </tbody>
                     </table>
                   </div>
                   <!-- /.table-responsive -->
                     <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>                     
                       
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script>
    
    function get_data(url,q){
        
        if(!url)
            url = base_url+'product/get_data';
        
        $.ajax({
            
            url:url,type:'post',dataType:'json',
            data:{q:q},
            success:function(result){
                
                $("#data-table tbody").html(result.rows);
                $("ul.pagination").html(result.paging);
                $(".page-info").html(result.page_info);
            }
        
        });
    } 
    function do_search(){
    
                
        get_data('',$("#search").val());
      
    }
    $(function(){
    
        get_data();//initialize
        
        $(document).on('click',"ul.pagination>li>a",function(){
        
            var href = $(this).attr('href');
            get_data(href);
            
            return false;
        });
        
        $("#search").keypress(function(e){
            
            var key= e.keyCode ? e.keyCode : e.which ;
            if(key==13){ //enter
                
                do_search();
            }
            
        });
        
        $("#btn-search").click(function(){
            
            do_search();
            
            return false;
        });
        
    });


function delete_product(id)
    {
        if(confirm('Are you sure delete this data '))
        {
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('product/ajax_delete')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    get_data();
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                   window.location.href="<?php echo site_url('no_access')?>";
                }
            });

    }
}
</script>



 
 


