<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller{
	var $folder = "product";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(PRODUCT,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('product_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/product');
		
	}
	
	private function _select_hari(){
	    
	    return $this->db->get('hari')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	private function _status(){
	    
	    $status = array('1'=>'Active','0'=>'Non Active');
	    
	    return $status;
	}

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->product_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->kode.'</td>';
	                $rows .='<td width="40%">'.$r->nama.'</td>';
	                // $rows .='<td width="10%">'.$r->harga.'</td>';
	                // $rows .='<td width="10%">'.$r->hari.'</td>';
	                $rows .='<td width="9%">'.$r->keterangan.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'product/edit/'.$r->id_product.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_product('."'".$r->id_product."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'product/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->product_model->save($data);
	     if($send)
	        redirect('product');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(PRODUCT,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_status'=>$this->_select_status(),
	    	'select_hari'=>$this->_select_hari()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(PRODUCT,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('product',array('id_product'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	    $data = array('select_status'=>$this->_select_status(),
	    	'select_hari'=>$this->_select_hari()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->product_model->update($data);
	     if($send)
	        redirect('product');
		
	}
	
	public function delete(){
	    
	     if(!$this->general->privilege_check(PRODUCT,'remove'))
		    $this->general->no_access();
		    
	     $id  = $this->uri->segment(4);
	     $send = $this->product_model->delete($id);
	     if($send)
	        redirect('product');
	}
	
	public function ajax_delete($id_product)
	{
		if(!$this->general->privilege_check(PRODUCT,'remove'))
		    $this->general->no_access();
		$send = $this->product_model->delete_by_id($id_product);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('product');
	}


}
