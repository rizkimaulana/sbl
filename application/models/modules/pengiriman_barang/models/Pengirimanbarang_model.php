<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengirimanbarang_model extends CI_Model{
         var $table = 'view_pengiriman_barang';
    var $column_order = array(null, 'id_registrasi','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','alamat','provinsi_id','kabupaten_id','kecamatan_id','tgl_daftar','paket','date_schedule','bulan_menunggu','bulan_keberangkatan','tipe_jamaah','pengiriman','update_date'); //set column field database for datatable orderable
    var $column_search = array('id_registrasi','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','alamat','provinsi_id','kabupaten_id','kecamatan_id','tgl_daftar','paket','date_schedule','bulan_menunggu','bulan_keberangkatan','tipe_jamaah','pengiriman','update_date'); //set column field database for datatable searchable 
    var $order = array('update_date' => 'asc'); // default order 

   
   
    private $_table="pengiriman";
     private $_registrasi_jamaah="registrasi_jamaah";
    private $_primary="id_pengiriman";

    


    public function update($data){
        
        $arr = array(
        
           'jasa_pengiriman' =>$data['select_pengiriman'],
            'no_resi' =>$data['no_resi'],
            'tgl_pengiriman' => $data['tgl_pengiriman'],
            'create_by' => $this->session->userdata('id_user'),
            'create_date' =>date('Y-m-d H:i:s'),
            'status' =>2,
        );       
              
         $this->db->update($this->_table,$arr,array('id_registrasi'=>$data['id_registrasi']));


         $arr = array(
            'update_date' =>date('Y-m-d H:i:s'),
            'pengiriman' =>2,
        );       
              
         $this->db->update($this->_registrasi_jamaah,$arr,array('id_registrasi'=>$data['id_registrasi']));

          if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

  
 
    

    private function _get_datatables_query()
    {
        
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
