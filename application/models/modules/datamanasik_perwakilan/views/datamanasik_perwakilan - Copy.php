

<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<div id="page-wrapper">
     <div class="row">
       <div class="col-lg-12">
           <div class="content">
           <div class="form-group">
                  <label class="col-sm-2 control-label">Bulan Pemberangkatan : </label>
                  <div class="col-sm-10">
                        <input type='text' name="datepicker_keberangkatan" id="filter" class="form-control" placeholder="MM/January" required/>
                  </div>
              </div>
               <div class="form-group">
              <label class="col-sm-2 control-label">ID Jamaah : </label>
                   <div class="col-sm-10">             
                      <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                  </div>
                </div>

              <div class="form-group">
             <label class="col-sm-5 control-label">Search : </label>
                   <div class="col-sm-10">             
                    
                        <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                    
                  </div>
                </div>
        </div>
       </div>
      </div>

    <form   class="form-horizontal" role="form" action='<?= base_url();?>registrasi_affiliate/update_keluarga' method='POST'> 
      <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                 
                    <b>LENGKAPI DATA MANIFEST</b>
                </div>
                <div class="panel-body">

                <div style="overflow-x:auto;">

                          <div class="table-responsive">
                               <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                  <th>No</th>
                                 <th>DataJamaah</th>
                                  <!-- <th>NAMA</th> -->
                                  <!-- <th>BlnBrangkat</th> -->
                                 
                                  <th>TTG</th>
                                  <!-- <th>TglLahir</th> -->
                                   <th>Data Passport</th>
                                  <!-- <th>NoPassport</th> -->
                                  <!-- <th>IssueOffice</th> -->
                                  <!-- <th>IssuiDate</th> -->
                                  <th>HubKeluarga</th>
                                  <th>PhotoCopy</th>
                                  <th>TGL_TerimaPhotoCopy</th>
                                  <!-- <th>Keluarga</th> -->
                                 <!--  <th>Ktp</th>
                                  <th>Kk</th>
                                  <th>Photo</th>
                                  <th>Pasport</th>
                                  <th>Vaksin</th>
                                  <th>BNikah</th> -->
                                  <!-- <th>TglKtp</th>
                                  <th>TglKk</th>
                                  <th>TglPhoto</th>
                                  <th>TglPasport</th>
                                  <th>TglVaksin</th>
                                  <th>TglBNikah</th> -->
                                  <th>KetManifest</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                          
                            <tbody>
                      <!-- 
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                    <td><input type="hidden" name="data2[<?php echo $no;?>][id_jamaah]" class="form-control" value="<?php echo $pi['id_jamaah']?>" id="id_jamaah" readonly="true"><?php echo $no; ?></td>
                                      
                                    <td><?php echo $pi['id_registrasi']?> </td>
                                    <td><?php echo $pi['id_affiliate']?> </td>
                                    <td><?php echo $pi['id_jamaah']?> </td>
                                    <td><?php echo $pi['nama']?> </td>
                                    <td><?php echo $pi['nama_passport']?> </td>
                                    <td><?php echo $pi['tempat_lahir']?> </td>
                                    <td><?php echo $pi['tanggal_lahir']?> </td>
                                    <td><?php echo $pi['no_pasport']?> </td>
                                    <td><?php echo $pi['issue_office']?> </td>
                                    <td><?php echo $pi['isui_date']?> </td>
                                    <td><?php echo $pi['hubwaris']?> </td>
                                    <td><?php echo $pi['hubkeluarga']?> </td>
                                    <td><?php echo $pi['keluarga']?> </td>
                                    <td><?php echo $pi['status_identitas']?> </td>
                                    <td><?php echo $pi['status_kk']?> </td>
                                    <td><?php echo $pi['status_photo']?> </td>
                                    <td><?php echo $pi['status_pasport']?> </td>
                                    <td><?php echo $pi['status_vaksin']?> </td>
                                    <td><?php echo $pi['status_buku_nikah']?> </td>
                                    <td><?php echo $pi['pic1']?> </td>
                                    <td><?php echo $pi['pic2']?> </td>
                                    <td><?php echo $pi['pic3']?> </td>
                                    <td><?php echo $pi['pic4']?> </td>
                                    <td><?php echo $pi['pic5']?> </td>
                                    <td><?php echo $pi['pic6']?> </td>
                                    <td><?php echo $pi['tgl_status_identitas']?> </td>
                                    <td><?php echo $pi['tgl_status_kk']?> </td>
                                    <td><?php echo $pi['tgl_status_photo']?> </td>
                                    <td><?php echo $pi['tgl_status_pasport']?> </td>
                                    <td><?php echo $pi['tgl_status_vaksin']?> </td>
                                    <td><?php echo $pi['tgl_status_buku_nikah']?> </td>
                                    <td><?php echo $pi['ket_manifest']?> </td>
                                      
                                  </tr>

                                  <?php } ?>  -->
                            </tbody>
                     </table>
                   </div>
                  
                   <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
                    
              </div>
              </div>
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> LANJUTKAN</button>
          </div>
      </div>
    </div>
    
  </form>       
                
</div>


 <script>

function get_data(url,q,filter){
        
        if(!url)
            url = base_url+'datamanasik_perwakilan/get_data';
        
        $.ajax({
            
            url:url,type:'post',dataType:'json',
            data:{q:q,filter:filter},
            success:function(result){
                
                $("#data-table tbody").html(result.rows);
                $("ul.pagination").html(result.paging);
                $(".page-info").html(result.page_info);
            }
        
        });
    } 
    function do_search(){
    
         var flter = $("#filter").val();         
        get_data('',$("#search").val(),flter);
        // alert ($(this).val());
        // $("#search").val('');

    }
    $(function(){
    
        get_data();//initialize
        
        $(document).on('click',"ul.pagination>li>a",function(){
        
            var href = $(this).attr('href');
            get_data(href);
            
            return false;
        });
        
        $("#search").keypress(function(e){
            
            var key= e.keyCode ? e.keyCode : e.which ;
            if(key==13){ //enter
                
                do_search();
                // $(this).val('');

            }
            // if(do_search());{ 
            // alert ($(this).val());
            //  }



        });
        
        $("#btn-search").click(function(){
            
            do_search();
             
            return false;

        });
        
    });

</script>
<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
</script>
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
      
       $(function () {
                $('#filter').datetimepicker({
          format: 'MMMM YYYY',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
    </script>