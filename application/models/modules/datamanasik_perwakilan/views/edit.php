                <div class="md-modal md-effect-1" id="nft-default">
                    <div class="md-content">
                      <div class="modal-header">
                        <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="text-center">
                          <div class="i-circle primary"><i class="fa fa-check"></i></div>
                          <h4>Awesome!</h4>
                          <p>Can you say supercalifragilisticoexpialidoso?</p>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary btn-flat md-close" data-dismiss="modal">Proceed</button>
                      </div>
                    </div>
                </div>