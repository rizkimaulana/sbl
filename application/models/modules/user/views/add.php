 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add User</h3>
        <?php echo $message;?>
         <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-danger">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
        <form role="form" method="post" action="<?php echo base_url();?>user/save">
            <div class="form-group">
                <label>NIK</label>
                <input type ="text" name="id_user"  id="id_user" class="form-control"  autofocus required>
                <br>
            </div>
            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" name="nama" required>
            </div>
            <div class="form-group">
                <label>Username</label>
                <input class="form-control" name="username" required>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="password" name="password" required>
            </div>
            <div class="form-group">
                <label>Jabatan</label>
                <select class="form-control" name="jabatan_id">
                    <?php foreach($select_jabatan as $jb){?>
                        <option value="<?php echo $jb->id;?>"><?php echo $jb->jabatan;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                
                 <select name="status" class="form-control">
                   <?php foreach($select_status as $st){?>
                        <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                    <?php } ?>
                  </select>
                
              </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>user" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
