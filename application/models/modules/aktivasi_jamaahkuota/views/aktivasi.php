 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});


</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_jamaahkuota/save" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        


      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
                	     <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                 	<input type="hidden" name="id_booking_kuota" value="<?php echo $aktivasi['id_booking_kuota']?>"  class="form-control" readonly="true">
                  <input type="hidden" name="id_schedule" value="<?php echo $aktivasi['id_schedule']?>"  class="form-control" readonly="true">
                 	<div class="form-group">
	                    <label class="col-lg-2 control-label">ID Affiliate</label>
	                    <div class="col-lg-5">
	                       <input type="text" name="id_affiliate" value="<?php echo $aktivasi['id_affiliate']?>"  class="form-control" readonly="true">
	                    </div>
	                  </div>

	                  	<div class="form-group">
	                    <label class="col-lg-2 control-label">NAMA Affiliate</label>
	                    <div class="col-lg-5">
	                       <input type="text" name="affiliate" value="<?php echo $aktivasi['affiliate']?>"  class="form-control" readonly="true">
	                    </div>
	                  </div>
                    <?php if ($aktivasi['status_kuota']=='1') { ?>
                      <input type="hidden" name="status_fee_refrensi" value="0"  class="form-control" readonly="true">

                    <?php }else{ ?>
                      <?php if  ($aktivasi['sponsor']== '0' or $aktivasi['sponsor']== 'null') {?>
                       <input type="hidden" name="status_fee_refrensi" value="<?php echo $aktivasi['status_fee_refrensi'] ?>"  class="form-control" readonly="true">
                      <?php }else{  ?>
                      <input type="hidden" name="status_fee_refrensi" value="<?php echo $aktivasi['status_fee_refrensi'] + 1 ?>"  class="form-control" readonly="true">
                      <?php } ?>
                    <?php } ?>
                    <input type="hidden" name="status_fee" value="<?php echo $aktivasi['status_fee']?>"  class="form-control" readonly="true">
                     <div class="form-group">
	                    <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="keterangan" required></textarea>
	                    </div>
	                  </div>

             <div class="form-group">
                      <label class="col-lg-2 control-label">Metode Pembayaran</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_pay_method" id='select_pay_method'>
                      <option required value="">- PILIH Metode Pembayaran -</option>
                    <?php foreach($select_pay_method as $sk){?>
                            <option value="<?php echo $sk->pay_method;?>"><?php echo $sk->pay_method;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>

              </div>

              <div id="hidden_div" style="display: none;">
		            <div class="form-group">
				        <label class="col-lg-2 control-label">Bukti Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="file" name="pic[]"  class="form-control" >
				        </div>
				    </div>
         

     

           		<div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                  <div class="col-lg-5">
                    <select  class="form-control" name="select_bank">
                      <option value="">- PILIH BANK TRANSFER -</option>
                      <?php foreach($select_bank as $sk){?>
                              <option value="<?php echo $sk->id;?>"><?php echo $sk->nama;?></option>
                          <?php } ?> 
                    </select>
                  </div>
                  </div>
              </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Jumlah KUOTA</label>
                <div class="col-lg-5">
                    <input type="text" name="jml_kuota" value="<?php echo $aktivasi['jml_kuota']?>"  class="form-control" readonly="true">
                </div>
            </div>
				    <div class="form-group">
				        <label class="col-lg-2 control-label">Nominal Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="text" name="nominal_pembayaran" value="<?php echo $aktivasi['total_harga']?>"  class="form-control" readonly="true">
				        </div>
				    </div>

           			 <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div>

	                 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aktivasi</button>
           			 <a href="<?php echo base_url();?>aktivasi_jamaahkuotapromo" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   
      
  </form>       
                
</div>
   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>
<script type="text/javascript">
      document.getElementById('select_pay_method').addEventListener('change', function () {
    var style = this.value == 'TRANSFER' ? 'block' : 'none';
    document.getElementById('hidden_div').style.display = style;
    // document.getElementById('hidden_div2').style.display = style;
    // $("#datepicker").prop('required',true);
     select_pay_method = $(this).val();

      console.log(select_pay_method);

      if(select_pay_method=='TRANSFER' ){
         $("#select_bank").prop('required',true);
        $("#pic1").prop('required',true);
       
      }else{
        $("#select_bank").prop('required',false);
         $("#pic1").prop('required',false);
         
      }
    });


  </script>