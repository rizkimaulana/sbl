<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aktivasijamaahkuota_model extends CI_Model{
   
   
    private $_table="aktivasi_jamaah_kuota";
   
    private $_primary="id_booking_kuota";



public function save($data){
        $id_booking_kuota= $this->input->post('id_booking_kuota');
        $id_schedule= $this->input->post('id_schedule');
        $arr = array(

            'id_booking_kuota'=> $data['id_booking_kuota'],
            'id_user_affiliate'=> $data['id_affiliate'],
            'keterangan' => $data['keterangan'],
            'pay_method'=> $data['select_pay_method'],
            'bank' => $data['select_bank'],
            'pic1' => isset($data['pic1']) ? $data['pic1']: '',
            // 'pic2' => isset($data['pic2']) ? $data['pic2']: '',
            // 'pic3' => isset($data['pic2']) ? $data['pic3']: '',
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user')
        );       

      $this->db->insert($this->_table,$arr);

       $arr = array(
          
            'status'=>1,
            'status_fee'=>$data['status_fee'],
            'payment_date' => $data['payment_date'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'tgl_aktivasi' => date('Y-m-d H:i:s'),
        );       
        
          $this->db->update('kuota_booking',$arr,array('id_booking_kuota'=>$data['id_booking_kuota'])); 

         $arr = array(
            'status'=>1,
            'status_fee'=>$data['status_fee'],
            'status_fee_refrensi'=>$data['status_fee_refrensi'],
            'payment_date' => $data['payment_date'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'tgl_aktivasi' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('jamaah_kuota',$arr,array('id_booking_kuota'=>$data['id_booking_kuota']));  

                  $sql = "SELECT * from schedule
          WHERE  id_schedule = '".$id_schedule."'";
        $query = $this->db->query($sql)->result_array();

         foreach($query as $key=>$value){
            $seats = $value['seats'];
           
          }


        $sql = "SELECT COUNT(*) as jumlah_jamaah from jamaah_kuota
          WHERE  id_booking_kuota = '".$id_booking_kuota."'";
        $query = $this->db->query($sql)->result_array();


          foreach($query as $key=>$value){
            $jumlah_jamaah = $value['jumlah_jamaah'];
            if($jumlah_jamaah > 0){
            $arr = array(
                
                         
                    'seats' => ($seats - $jumlah_jamaah),  
                );       
                 $this->db->update('schedule',$arr,array('id_schedule'=>$data['id_schedule']));
            }
          }

    
      if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;

        }
}


 public function get_pic_booking($id_booking){
 

         $sql = "SELECT * from faktur_jamaah_kuota  where id_booking_kuota = {$id_booking_kuota}";
 	
        return $this->db->query($sql)->row_array();  
    }


public function get_jamaah_kuota($where= "") {
	// $data = $this->db->query('select * from detail_jamaahnoaktif where id_affiliate="'.$id_user.'" '.$where);
        $data = $this->db->query('select * from faktur_jamaah_kuota '.$where);
        return $data;
}

public function get_pic_aktivasi($id_booking_kuota=''){

         $sql = "SELECT * from faktur_jamaah_kuota where id_booking_kuota = {$id_booking_kuota}
                ";
        return $this->db->query($sql)->row_array();  
    }
  
}
