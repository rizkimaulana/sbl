<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bilyet_model extends CI_Model{
    var $table = 'view_bilyet';
    var $column_order = array(null, 'id_cetak_bilyet','id_jamaah','keterangan','id_user','create_date','update_date','create_by','update_by','user'); //set column field database for datatable orderable
    var $column_search = array('id_cetak_bilyet','id_jamaah','keterangan','id_user','create_date','update_date','create_by','update_by','user'); //set column field database for datatable searchable 
    var $order = array('create_by' => 'asc'); // default order 

    private $_primary="id_cetak_bilyet";
    private $_table="bilyet";

  public function update($data){
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            'id_jamaah' => $data['id_jamaah'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['select_user'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
        
              $this->db->update($this->_table,$arr,array('id_jamaah'=>$data['id_jamaah']));
        // $id_jamaah =  $this->db->insert_id(); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();

            return true;

        }
    }


   

    public function get_pic_bilyet($id_jamaah){
    
       $sql ="SELECT * from data_jamaah_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

    }

   
       public function get_data($offset,$limit,$q=''){
    
        $id=  $this->session->userdata('id_user');
          $sql = "SELECT * FROM bilyet
                   where (1=1) ";
        
        if($q){
            
            $sql .=" AND id_jamaah LIKE '%{$q}%'
                   
                     OR id_user LIKE '%{$q}%'
                     OR create_by LIKE '%{$q}%'
                     OR create_date LIKE '%{$q}%'   
                     
                ";
        }
        $sql .=" ORDER BY create_date DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }


 public function get_pic_detail_bilyet($id_jamaah){
 

         $sql = "SELECT * from data_jamaah_aktif where id_jamaah = '$id_jamaah'";
  
        return $this->db->query($sql)->row_array();  
    }
    
    public function get_pic_log_bilyet($id_jamaah){
    
       $sql ="SELECT * from view_log_bilyet WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

}


 private function _get_datatables_query()
    {
      
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
   
}
