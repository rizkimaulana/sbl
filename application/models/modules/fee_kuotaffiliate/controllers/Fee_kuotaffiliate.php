<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fee_kuotaffiliate extends CI_Controller{
	var $folder = "fee_kuotaffiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(FEE_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_affiliate');	
		$this->load->model('feekuotaffiliate_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/fee_kuota_affiliate');
		
	}
	

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->feekuotaffiliate_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	               
	               
	                $rows .='<td width="10%"><strong>'.$r->invoice.'</strong></td>';
	                $rows .='<td width="10%">Rp '.number_format($r->fee_refrensi).'</td>';
	             	 $rows .='<td width="20%">Rp '.number_format($r->pajak_fee).'</td>';
	                $rows .='<td width="20%">Rp '.number_format($r->fee_dipotong_pajak).'</td>';
	                $rows .='<td width="20%" align="center">';
	                if ($r->status_fee == 1){ 
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Claim Fee" onclick="update_status_fee_kuota_affiliate('."'".$r->id_booking_kuota."'".')"> Claim Fee</a> ';
	                }elseif($r->status_fee == 2){ 
	                	$rows .='<a title="Menunggu Konfirmasi" class="btn btn-sm btn-primary" href="#">
	                            </i> Menunggu Konfirmasi
	                        </a> ';
	                }else{
	                	$rows .='<a title="OK" class="btn btn-sm btn-success" href="#">
	                            </i> OK
	                        </a> ';
	                }
	               //  $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'profil/detail/'.$r->id_user.'">
	               //              <i class="fa fa-pencil"></i> Detail
	               //          </a> ';
	                          
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'fee_postingaffiliate/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	

	public function update_status_fee($id_booking)
	{
		// if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
		//     $this->general->no_access();
		$send = $this->feekuotaffiliate_model->update_status_fee($id_booking);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('fee_kuotaffiliate');

	}

	

	
	


}
