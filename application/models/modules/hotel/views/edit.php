 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Edit Hotel</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>hotel/update">
        	<input type="hidden" name="id_hotel" value="<?php echo $id_hotel;?>">



         <div class="form-group">
                <label>Room Type</label>
                <select class="form-control" name="select_bintang">
                     <?php foreach($select_bintang as $sp){ 
                    
                        $selected = ($id_bintang == $sp->id_bintang)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $sp->id_bintang;?>" <?php echo $selected;?>><?php echo $sp->bintang;?> </option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="form-group">
                <label>Nama Hotel </label>
                <input class="form-control" name="nama_hotel" value="<?php echo $nama_hotel;?>" required>
            </div>
              <div class="form-group">
                <label>Keterangan</label>
                    <textarea  name="keterangan" class="form-control"  ><?php echo $keterangan;?></textarea>
                 
                </div>
          
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>hotel" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
