
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah ALUMNI</b>
                </div>

                 <div class="panel-body">
                     <form role="form">
                           
                             <div class="form-group input-group col-lg-4">
                               
                                <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div>
                        </form>  
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <!-- <th>BARCODE</th> -->
                                    <th>NAMA</th>
                                    <th>USERID </th>
                                    <th>REWARD </th>
                                    <th>DATEWIN </th>
                                    <th>KET </th>
                                    <th>ADMIN </th>
                                   <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                     </table>
                   </div>
                  
                      <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>

function get_data(url,q){
        
        if(!url)
            url = base_url+'reward_sahabat/get_data';
        
        $.ajax({
            
            url:url,type:'post',dataType:'json',
            data:{q:q},
            success:function(result){
                
                $("#data-table tbody").html(result.rows);
                $("ul.pagination").html(result.paging);
                $(".page-info").html(result.page_info);
            }
        
        });
    } 
    function do_search(){
    
                
        get_data('',$("#search").val());
        // alert ($(this).val());
        // $("#search").val('');

    }
    $(function(){
    
        get_data();//initialize
        
        $(document).on('click',"ul.pagination>li>a",function(){
        
            var href = $(this).attr('href');
            get_data(href);
            
            return false;
        });
        
        $("#search").keypress(function(e){
            
            var key= e.keyCode ? e.keyCode : e.which ;
            if(key==13){ //enter
                
                do_search();
                // $(this).val('');

            }
            // if(do_search());{ 
            // alert ($(this).val());
            //  }



        });
        
        $("#btn-search").click(function(){
            
            do_search();
             
            return false;

        });
        
    });

</script>
