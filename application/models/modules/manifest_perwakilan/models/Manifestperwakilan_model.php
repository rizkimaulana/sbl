<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manifestperwakilan_model extends CI_Model{
    var $table = 'manifest_sebaran_detail';
    var $column_order = array(null, 'id','id_registrasi','invoice','id_jamaah','no_pasport','id_affiliate','nama','id_booking','nama_passport','issue_office','tanggal_lahir','tempat_lahir','kelamin','isui_date','telp','telp_2','status_identitas','status_kk','status_photo','status_pasport','status_vaksin','status_buku_nikah','status_akte','pic1','pic2','pic3','pic4','pic5','pic6','bulan_keberangkatan','tahun_keberangkatan','bulantahun_keberangkatan','schedule','create_date','ket_keberangkatan','hubkeluarga','keluarga','ket_keluarga'); //set column field database for datatable orderable
    var $column_search = array('id','id_registrasi','invoice','id_jamaah','no_pasport','id_affiliate','nama','id_booking','nama_passport','issue_office','tanggal_lahir','tempat_lahir','kelamin','isui_date','telp','telp_2','status_identitas','status_kk','status_photo','status_pasport','status_vaksin','status_buku_nikah','status_akte','pic1','pic2','pic3','pic4','pic5','pic6','bulan_keberangkatan','tahun_keberangkatan','bulantahun_keberangkatan','schedule','create_date','ket_keberangkatan','hubkeluarga','keluarga','ket_keluarga'); //set column field database for datatable searchable 
    var $order = array('id_registrasi' => 'asc'); // default order 
   

   
    public function __construct()
    {
          parent::__construct();
          $this->load->database();
         
    }

    private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');

         if($this->input->post('bulantahun_keberangkatan'))
        {
            $this->db->where('bulantahun_keberangkatan', $this->input->post('bulantahun_keberangkatan'));
        }
        // if($this->input->post('schedule'))
        // {
        //     $this->db->like('schedule', $this->input->post('schedule'));
        // }
         if($this->input->post('cluster'))
        {
            $this->db->like('cluster', $this->input->post('cluster'));
        }
      
        $this->db->where_in('id_affiliate', $id_user);
        $this->db->order_by("id_affiliate", "asc");
        $this->db->from('manifest_affiliate');
  
       

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // -----------------------------------------------------

    
      public function get_list_bulantahun_keberangkatan()
    {
        $id_user=  $this->session->userdata('id_user');
       $koordinator= $this->uri->segment(3.0);
        $this->db->select('bulantahun_keberangkatan');
        $this->db->from($this->table);
        $this->db->where('cluster', $koordinator);
        $this->db->where('id_affiliate', $id_user);
        // $this->db->order_by('bulantahun_keberangkatan','desc');
        $query = $this->db->get();
        $result = $query->result();

        $bulantahun_keberangkatans = array();
        foreach ($result as $row) 
        {
            $bulantahun_keberangkatans[] = $row->bulantahun_keberangkatan;
        }
        return $bulantahun_keberangkatans;
    }

      public function get_list_date_schedule()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->select('schedule');
        
        $this->db->from($this->table);
        $this->db->where('id_affiliate', $id_user);
        // $this->db->order_by('bulantahun_keberangkatan','desc');
        $query = $this->db->get();
        $result = $query->result();

        $schedules = array();
        foreach ($result as $row) 
        {
            $schedules[] = $row->schedule;
        }
        return $schedules;
    }


      function get_all_schedule() {
        

     $query = $this->db->query("SELECT DISTINCT(schedule) from  manifest_affiliate ORDER BY  schedule DESC ");

        return $query->result();
    }

        public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();

        return $query->row();
    }


   

     public function get_detail_manifest($id){

          $stored_procedure = "call detail_manifest_affiliate(?)";
         return $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array();

    }

    public function get_detail_hubkeluarga($id){

          $stored_procedure = "call refrensi_hubungankeluarga(?)";
         return $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array();

    }

    function get_keluarga() {
        $id_user =  $this->session->userdata('id_user');
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status','1');
        $this->db->where('id_affiliate', $id_user);
        $query = $this->db->get();
        
        return $query->result();
    }

     public function get_data_koordinator($id_user){
    
           $sql = "SELECT * from affiliate where id_user = '$id_user'
                ";
        return $this->db->query($sql)->row_array();   

        
    }

     public function get_data_pic($id){
    
           $sql = "SELECT * from manifest where id_registrasi = '$id'
                ";
        return $this->db->query($sql)->row_array();   

        
    }
}
