 <div id="page-wrapper">
    
    
       <div class="row">
       	<!-- <div class="col-lg-7"> -->
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Edit Affiliate</h3>
       
        <!-- <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>affiliate/update"> -->
        	<form action="<?php echo base_url();?>profil/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
             <!-- <div class="col-lg-6"> -->
             <div class="panel panel-default">
               
	
                <div class="panel-body">  
                	 <div class="form-group">
	                    <label class="col-lg-2 control-label" >ID Affiliate</label>
	                    <div class="col-lg-5">
	                      <input name="id_user" value="<?php echo $id_user;?>" class="form-control" readonly="readonly">
	                    </div>
	                </div>
                	<input type="hidden" name="id_aff" value="<?php echo $id_aff;?>">
                	<input type="hidden" name="id_login" value="<?php echo $id_login;?>">
                		
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label" >ID Join</label>
	                    <div class="col-lg-5">
	                      <input name="id_affiliate" class="form-control" value="<?php echo $id_affiliate;?>" readonly="readonly">
	                    </div>
	                </div>
		                 <div class="form-group">
		                    <label class="col-lg-2 control-label" >Nama</label>
		                    <div class="col-lg-5">
		                      <input name="nama" class="form-control" value="<?php echo $nama;?>"  >
		                    </div>
		                </div>
		              <div class="form-group">
	                    <label class="col-lg-2 control-label" >Nama Affiliate/(Cabang)</label>
	                    <div class="col-lg-5">
	                      <input name="nama_affiliate" value="<?php echo $nama_affiliate;?>" class="form-control" >
	                    </div>
	                </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">No Tlp</label>
	                    <div class="col-lg-5">
	                      <input name="telp" class="form-control" value="<?php echo $telp;?>">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">E-Mail</label>
	                    <div class="col-lg-5">
	                      <input name="email" class="form-control" value="<?php echo $email;?>" >
	                    </div>
	                  </div>

	                 <div class="form-group">
	                    <label class="col-lg-2 control-label">No Identitas</label>
	                    <div class="col-lg-5">
	                      <input name="ktp" class="form-control" value="<?php echo $ktp;?>" >
	                    </div>
	                  </div>
	                  <div class="form-group">
	                   
	                   <label class="col-lg-2 control-label">Jenis Kelamin</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_kelamin">
		             

		                    <?php foreach($select_kelamin as $st){ 
		                        $selected = ($kelamin == $st->kdstatus)  ? 'selected' :'';?>
		                        <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
		                    <?php } ?>
		                </select>
		            	</div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Hubungan Keluarga</label>
	                     <div class="col-lg-5">
		                      <select class="form-control" name="hubwaris">
		                      	<!-- <option>- PILIH HUBUNGAN KELUARGA -</option> -->
			                    <?php foreach($family_relation as $fr){?>
			                        <option value="<?php echo $fr->id_relation;?>"<?php echo $selected;?>><?php echo $fr->keterangan;?></option>
			                    <?php } ?> 
			                  <!--     <?php foreach($hubwaris as $sp){ 
		                        	$selected = ($hubwaris == $sp->id_relation)  ? 'selected' :'';
		                    		?>
		                        	<option value="<?php echo $sp->id_relation;?>" <?php echo $selected;?>><?php echo $sp->keterangan;?> </option>
		                    	<?php } ?> -->
			                </select>
		                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Ahli waris</label>
	                    <div class="col-lg-5">
	                      <input name="waris" class="form-control" value="<?php echo $waris;?>">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Tempat tanggal Lahir</label>
	                    <div class="col-lg-5">
	                      <input name="tempat_lahir" class="form-control" value="<?php echo $tempat_lahir;?>"> 
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">Tanggal Lahir</label>
	                    <div class="col-lg-5">
	                     <div class='input-group date' id='datepicker'>
	                          <input type='text' name="tanggal_lahir" id="datepicker" value="<?php echo $tanggal_lahir;?>"class="form-control" />
	                          <span class="input-group-addon">
	                            <span class="glyphicon glyphicon-calendar"></span>
	                          </span>
                        </div>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Provinsi</label>
	                    <div class="col-lg-5">
	                      
                            <select name="provinsi" class="form-control" id="provinsi">
                            	 
                                <!-- <option>Select Provinsi</option> -->
                                <option></option>
                                <?php foreach($provinsi as $Country){

                                     echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                    
                                } ?> 
								<!-- EDIT
                                <?php foreach($provinsi as $st){ 
		                        $selected = ($provinsi == $st->provinsi_id)  ? 'selected' :'';?>
		                        <option value="<?php echo $st->provinsi_id;?>" <?php echo $selected;?>><?php echo $st->nama;?></option>
		                    <?php } ?> -->
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kabupaten</label>
	                    <div class="col-lg-5">
	                      
                            <select name="kabupaten" class="form-control" id="kabupaten">
                                <!-- <option value=''>Select Kabupaten</option> -->
                            	<option></option>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kecamatan</label>
	                    <div class="col-lg-5">
	                      <select name="kecamatan" class="form-control" id="kecamatan">
                                <!-- <option value=''>Select Kecamatan</option> -->
                                <option></option>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat Rumah</label>
	                    <div class="col-lg-5">
	                      <textarea  name="alamat" class="form-control"  ><?php echo $alamat;?></textarea>
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat Kantor</label>
	                    <div class="col-lg-5">
	                      <textarea  name="alamat_kantor" class="form-control" ><?php echo $alamat_kantor;?></textarea>
	                    </div>
	                </div>
	                <!--   <div class="form-group">
	                    <label class="col-lg-2 control-label">UserName</label>
	                    <div class="col-lg-5">
	                      <input name="username" class="form-control" value="<?php echo $username;?>"  > 
	                    </div>
	                  </div> -->
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Password</label>
	                    <div class="col-lg-5">
	                      <input name="password" class="form-control"  > 
	                    </div>
	                  </div>
                 <!-- </div>
                
               <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Bank</b>
                </div>
                <div class="panel-body">  -->
               		<div class="form-group">
	                    <label class="col-lg-2 control-label">No Rekening</label>
	                    <div class="col-lg-5">
	                      <input name="norek" class="form-control"  value="<?php echo $norek;?>" > 
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Atas Nama Rekening</label>
	                    <div class="col-lg-5">
	                       <input name="namarek" class="form-control" value="<?php echo $namarek;?>" > 
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Bank</label>
	                    <div class="col-lg-5">
	                       <select class="form-control" name="select_bank">
	                      
		                     <?php foreach($select_bank as $sb){ 
		                    
		                        $selected = ($bank == $sb->id)  ? 'selected' :'';
		                    ?>
		                        
		                        <option value="<?php echo $sb->id;?>" <?php echo $selected;?>><?php echo $sb->nama;?></option>
		                    <?php } ?>
		                </select> 
	                    </div>
	                  </div>
	                   <div class="form-group">
					        <label class="col-lg-2 control-label">FOTO</label>
					        <div class="col-lg-5">
					            <input type="file" name="pic[]"  class="form-control">
					        </div>
					    </div>
	                  <div class="form-group">
		                <label class="col-lg-2 control-label">Status</label>
		                <div class="col-lg-5">
		                 <select class="form-control" name="select_status">


		                 	

		                     <?php foreach($select_status as $st){ 
		                        $selected = ($status == $st->kdstatus)  ? 'selected' :'';?>
		                        <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
		                    <?php } ?>
		                </select>
            		</div>
	                  </div>
	              <!-- </div> -->
              </div>

            <!-- </div> -->
               </div>
            <!-- </div>  -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>profil" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       <!-- </div> -->
    </div>
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
			$(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
		</script>
<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('profil/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('profil/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('profil/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });
</script>