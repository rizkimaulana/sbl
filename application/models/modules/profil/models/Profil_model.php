<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profil_model extends CI_Model{
   
   
    private $_table="affiliate";
    private $_primary="id_aff";

    public function update($data){
        
        $arr = array(
        
            // 'id_affiliate_type' => $data['affiliate_type'],
            'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
             'nama_affiliate' => $data['nama_affiliate'],
            // 'username' => $data['username'],
            // 'password' => $data['password'],
            'telp' => $data['telp'],
            'email' => $data['email'],

            'ktp' => $data['ktp'],
            // 'kelamin' => $data['select_kelamin'],
            // 'hubwaris' => $data['hubwaris'],
            'waris' => $data['waris'],
            // 'kelamin' => $data['select_kelamin'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'alamat' => $data['alamat'],
            'alamat_kantor' => $data['alamat_kantor'],
            'alamat_kirim' => $data['alamat_kirim'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
             // 'file_name' => $foto,
            // 'bank' => $data['select_bank'],
            'npwp' => $data['npwp'],
            // 'tanggal_daftar' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user'),
            // 'create_by'=>$this->session->userdata('nama')
        );       
        
         if($data['password']!='') {
          
            $arr['password'] = md5($data['password']);
          
        }
         if($data['provinsi']!='' && $data['kecamatan']!='' && $data['kabupaten']!='') {
            $arr['provinsi_id'] = ($data['provinsi']);
            $arr['kecamatan_id'] = ($data['kecamatan']);
            $arr['kabupaten_id'] = ($data['kabupaten']);
        }

        if($data['family_relation']!='') {
            $arr['hubwaris'] = ($data['family_relation']);
           
        }
        if($data['select_kelamin']!='') {
           
            $arr['kelamin'] = ($data['select_kelamin']);
         
        }
        if($data['select_bank']!='') {
          
            $arr['bank'] = ($data['select_bank']);
        }
       if(isset($data['pic1'])){
            
            $arr['pic1'] = $data['pic1'];

        }
         $this->db->update('affiliate',$arr,array('id_user'=>$this->session->userdata('id_user')));


    //      $id_user=  $this->session->userdata('id_user');
    //       $sql = "SELECT id_affiliate from affiliate where id_user='$id_user'
    //        ";
    //       $query = $this->db->query($sql)->result_array();
    //       foreach($query as $key=>$value){
    //         $id_affiliate = $value['id_affiliate'];
    //       }

    //     if ($id_affiliate == 'null'){ 
    //     $arr = array(
    //       'id_affiliate'=>$data['id_affiliate'],
         
    //     );    
    //     $this->db->update('affiliate',$arr,array('id_user'=>$this->session->userdata('id_user')));
    // }
         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");

        return $query->result();
    }


      public function get_detail($id_user){

         $id_user=  $this->session->userdata('id_user');
         $sql = "SELECT * from view_affiliate
                    WHERE id_user='$id_user'
                ";
        
        return $this->db->query($sql)->row_array();
    }

   
    public function get_profil($id_user){

         $id_user=  $this->session->userdata('id_user');
         $sql = "SELECT * from view_login_affiliate
                    WHERE id_user='$id_user'
                ";
        
        return $this->db->query($sql)->row_array();
    }
    public function get_data($offset,$limit,$q=''){
    
     $id_user=  $this->session->userdata('id_user');
        $id=  $this->session->userdata('id');
          $sql = " SELECT * FROM view_affiliate where id_user='".$id_user." ' 
          ";
        
        if($q){
            
            $sql .=" AND id_user LIKE '%{$q}%' 
            		OR nama LIKE '%{$q}%'
                    OR telp LIKE '%{$q}%'
            		OR nama LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_affiliate DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   
    
    
}
