<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil extends CI_Controller{
	var $folder = "profil";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(PROFIL,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','profil');	
		$this->load->model('profil_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/profil');
		
	}
	
	public function profile(){

		 // if(!$this->general->privilege_check_affiliate(PROFIL,'view'))
		 //    $this->general->no_access();

	    $id_user=  $this->session->userdata('id_user');
	   	$detail = $this->profil_model->get_profil($id_user);
	    // if(!$detail)
	    //     show_404();
	  	  $data = array('affiliate_type'=>$this->_affiliate_type(),
			 'family_relation'=>$this->_family_relation(),
			 'provinsi'=>$this->profil_model->get_all_provinsi(),
			 'kabupaten'=>$this->profil_model->get_all_kabupaten(),
			 'select_status'=>$this->_select_status(),
			 'select_kelamin'=>$this->_select_kelamin(),
			 'select_bank'=>$this->_select_bank(),
			 'detail'=>$detail,
	);
	     $this->template->load('template/template', $this->folder.'/update',($data));
	    // $this->template->load('template/template', $this->folder.'/update',$data);	
	}

	private function _affiliate_type(){
		
		return $this->db->get('affiliate_type')->result();
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->profil_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	               
	               
	                $rows .='<td width="10%">'.$r->id_user.'</td>';
	                $rows .='<td width="20%">'.$r->nama.'</td>';
	                $rows .='<td width="10%">'.$r->telp.'</td>';
	                $rows .='<td width="10%">'.$r->email.'</td>';
	                $rows .='<td width="10%">'.$r->provinsi.'</td>';
	                $rows .='<td width="10%">'.$r->keterangan.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'profil/edit/'.$r->id_user.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';

	                $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'profil/detail/'.$r->id_user.'">
	                            <i class="fa fa-pencil"></i> Detail
	                        </a> ';
	                          
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'profil/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	



	public function detail(){
	 
	    if(!$this->general->privilege_check_affiliate(PROFIL,'view'))
		    $this->general->no_access();

	     $id = $this->uri->segment(3);
	   	 $detail = $this->profil_model->get_detail($id);
	    if(!$detail)
	        show_404();
	        
	    $data = array('affiliate_type'=>$this->_affiliate_type(),
	    			 'family_relation'=>$this->_family_relation(),
	    			 'provinsi'=>$this->profil_model->get_all_provinsi(),
	    			 'kabupaten'=>$this->profil_model->get_all_kabupaten(),
	    			 'select_status'=>$this->_select_status(),
	    			 'select_kelamin'=>$this->_select_kelamin(),
	    			 'select_bank'=>$this->_select_bank(),
	    			 'detail'=>$detail
	    	);
        $this->template->load('template/template', $this->folder.'/detail',($data));
	}

		public function edit($id){
		     if(!$this->general->privilege_check_affiliate(PROFIL,'edit'))
			    $this->general->no_access();
		     $id_user=  $this->session->userdata('id_user');
		    $id = $this->uri->segment(3,0);
		    $get = $this->db->get_where('view_affiliate',array('id_user'=>$id_user))->row_array();
		    if(!$get)
		        show_404();
		        
		    $data = array('affiliate_type'=>$this->_affiliate_type(),
		    			 'family_relation'=>$this->_family_relation(),
		    			 'provinsi'=>$this->profil_model->get_all_provinsi(),
		    			 'kabupaten'=>$this->profil_model->get_all_kabupaten(),
		    			 'select_status'=>$this->_select_status(),
		    			 'select_kelamin'=>$this->_select_kelamin(),
		    			 'select_bank'=>$this->_select_bank()
		    	);
	        $this->template->load('template/template', $this->folder.'/edit',array_merge($get,$data));
		}


	

	public function update(){
	     

		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/affiliate/',
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }
		    else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		    }
	    }
	     
	  //print_r($data);exit;
	   			 $send = $this->profil_model->update($data);
                $this->session->set_flashdata('info', "User berhasil diubah.");
	 	       redirect('profil/profile');
		
	}
	

public function unlink(){
	
	    $data = $this->input->post(null,true);
	    
	    if(unlink('./assets/images/affiliate/'.$data['img'])){
	        
	        $column = substr($data['img'],0,4); //pic1,pic2 etc...
	        $this->db->update('affiliate',array($column=>''),array('id_aff'=>$data['id_aff']));
	        
	       echo json_encode(array('status'=>true)); 
	    }
	    
	}

	
	


}
