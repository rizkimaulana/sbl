<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jabatan_model extends CI_Model{

    
    public function save($data){
        
        $arr = array(
        
            'jabatan' => $data['jabatan'],
            'keterangan' => $data['keterangan']
        );       
        
        return $this->db->insert('privilege',$arr);
    }
    public function update($data){
        
        $arr = array(
        
            'jabatan' => $data['jabatan'],
            'keterangan' => $data['keterangan']
        );       
        
        return $this->db->update('privilege',$arr,array('id'=>$data['id']));
    }
    public function get_data($offset,$limit,$q=''){
    
        $sql = "SELECT * FROM privilege WHERE 1=1 ";
        
        if($q){
            
            $sql .=" AND jabatan LIKE '%{$q}%' ";
        }
        
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
        
        return $ret;
        
    }
    
    
}
