<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Input_manifest extends CI_Controller{ 
  var $folder = "manifest_affiliate";
  public function __construct(){
    
  parent::__construct();
    if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
    if(!$this->general->privilege_check_affiliate(INPUT_MANIFEST,'view'))
        $this->general->no_access();
    $this->session->set_userdata('menu','manifest_affiliate');  
    $this->load->model('input_manifestmodel');
    $this->load->model('input_manifestmodel','r');


  }



  function add_ajax_schedule($bulantahun_keberangkatan){
        $id_user=  $this->session->userdata('id_user'); 
        $koordinator= $this->uri->segment(4.0);

     $query = $this->db->query("SELECT DISTINCT(schedule) from  manifest_affiliate where 
      bulantahun_keberangkatan ='".$bulantahun_keberangkatan."' and cluster ='".$id_user."' and id_affiliate ='".$koordinator."' ORDER BY  schedule");
        $data = "<option value=''>- Pilih tanggal Keberangkatan -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->schedule."'>".$value->schedule."</option>";
        }
        echo $data;
  }
  
  public function data_manifest(){

    
    $bulantahun_keberangkatans = $this->input_manifestmodel->get_list_bulantahun_keberangkatan();

    $opt = array('' => 'SEMUA PENCARIAN');
    foreach ($bulantahun_keberangkatans as $bulantahun_keberangkatan) {
      $opt[$bulantahun_keberangkatan] = $bulantahun_keberangkatan;
    }

    $get_bulantahun_keberangkatan['form_bulantahun_keberangkatan'] = form_dropdown('',$opt,'','id="bulantahun_keberangkatan" class="form-control"');

    // ------------------------------------------------------------------------------------------

    // $schedules = $this->input_manifestmodel->get_list_date_schedule();
    // $opt = array('' => 'SEMUA PENCARIAN');
    // foreach ($schedules as $schedule) {
    //  $opt[$schedule] = $schedule;
    // }
    // $get_list_date_schedule['form_get_list_date_schedule'] = form_dropdown('',$opt,'','id="schedule" class="form-control"');
     $id_user = $this->session->userdata('id_user');
     $get_koordinator = $this->input_manifestmodel->get_data_koordinator($id_user);
     $data = array(
            
            // 'schedule'=>$this->input_manifestmodel->get_all_schedule(),
            'family_relation'=>$this->_family_relation(),
            'get_keluarga'=>$this->input_manifestmodel->get_keluarga(),
            'get_koordinator'=>$get_koordinator
        );

    $this->template->load('template/template', $this->folder.'/input_manifest',array_merge($get_bulantahun_keberangkatan,$data)); 
  }


    private function _family_relation(){
    
    return $this->db->get('family_relation')->result();
  }
private function _status($take){
      $x='0';
     $status = array('0'=>'X','1'=>'Y');
      if($take)
          return $status[$take];
        // return $status;
 
  }
    public function ajax_list()
  {
    $affiliate= $this->uri->segment(3.0);
    $list = $this->r->get_datatables();
    $data = array();
    $no = $_POST['start'];
    
    foreach ($list as $r) {
      $no++;
      $row[] ='<tr>'; 
      $row = array();
            
      $row[] = $no;

        $sql = "SELECT * from  wilayah_provinsi where provinsi_id ='".$r->provinsi_id."'
                     ";
                    $query = $this->db->query($sql)->result_array();
                    foreach($query as $key=>$value){
                      $namaprovinsi = $value['nama'];
                      
                    }
            $sql = "SELECT * from  wilayah_kabupaten where kabupaten_id ='".$r->kabupaten_id."'
                     ";
                    $query = $this->db->query($sql)->result_array();
                    foreach($query as $key=>$value){
                      $namakab = $value['nama'];
                      
                    }

                   // $row[] ='<td width="10%" >  <strong>'.$r->id_affiliate.'<strong>
                   //     </td>';
              
                  $row[] ='<td width="10%" > DATA JAMAAH DARI ID : <strong>'.$r->id_affiliate.'<strong><br>
                     <strong>('.$r->invoice.')</strong><br>
                     <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
                     PROVINSI :  '.$namaprovinsi.'<br>
                     Kota/kab :  '.$namakab.'<br>
                     TGL KEBERANGKATAN :  '.$r->schedule.'<br>
                    
                       </td>';

                  $row[] = '<td width="10%">NAMA PASSPORT :'.$r->nama_passport.'<br>
                       NO PASSPORT : <strong>'.$r->no_pasport.'</strong><br>
                     ISSUE PASSPORT : <strong>'.$r->issue_office.'</strong><br>
                     ISSUE DATE : <strong>'.$r->isui_date.' </strong>
                       </td>';
                  if ($r->status_manifest ==0){ 
                 $row[] = '<td width="10%">KRT IDENTITAS/KTP : <strong>'.$this->_status($r->status_identitas).'</strong><br>
                     PASSPORT : <strong>'.$this->_status($r->status_pasport).'</strong><br>
                     PHOTO : <strong>'.$this->_status($r->status_photo).' </strong><br>
                     KARTU KELUARGA : <strong>'.$this->_status($r->status_kk).'</strong><br>
                     BUKU NIKAH : <strong>'.$this->_status($r->status_buku_nikah).' </strong><br>
                     VAKSIN : <strong>'.$this->_status($r->status_vaksin).' </strong><br>
                     AKTE : <strong>'.$this->_status($r->status_akte).' </strong><br>

                       </td>';    
                     }else{
                        $row[] = '<td width="10%">KRT IDENTITAS/KTP : <strong>'.$this->_status($r->status_identitas).'</strong><br>
                     PASSPORT : <strong>'.$this->_status($r->status_pasport).'</strong><br>
                     PHOTO : <strong>'.$this->_status($r->status_photo).' </strong><br>
                     KARTU KELUARGA : <strong>'.$this->_status($r->status_kk).'</strong><br>
                     BUKU NIKAH : <strong>'.$this->_status($r->status_buku_nikah).' </strong><br>
                     VAKSIN : <strong>'.$this->_status($r->status_vaksin).' </strong><br>
                     AKTE : <strong>'.$this->_status($r->status_akte).' </strong><br>
                     <h5> <span class="label label-info">APPROVED</span></h5>
                       </td>'; 
                     }
  if ($r->status_manifest ==0){ 
                         $row[] = '<td>
                  
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="UPDATE" onclick="edit_manifest('."'".$r->id."'".')"><i class="glyphicon glyphicon-time"></i> UPDATE</a><br><br>

                  <a class="btn btn-sm btn-info" href="'.base_url().'manifest_affiliate/input_manifest/detail/'.$r->id_registrasi.'" title="DETAIL"><i class="glyphicon glyphicon-pencil"></i> DETAIL</a><br><br>

                  <a class="btn btn-sm btn-warning" href="'.base_url().'manifest_affiliate/input_manifest/get_data_pic/'.$r->id_registrasi.'/'.$r->id_affiliate.'" title="UPDATE PICTURE"> </i> UPDATE PICTURE</a>
                  
                  </td>';
                     }else{

                       $row[] = '<td>
                  
                  <a class="btn btn-sm btn-danger" href="#" title="APPROVED"><i class="glyphicon glyphicon-time"></i> APPROVED</a><br><br>

                  <a class="btn btn-sm btn-info" href="'.base_url().'manifest_affiliate/input_manifest/detail/'.$r->id_registrasi.'" title="DETAIL"><i class="glyphicon glyphicon-pencil"></i> DETAIL</a><br><br>

                  <a class="btn btn-sm btn-warning" href="'.base_url().'manifest_affiliate/input_manifest/get_data_pic/'.$r->id_registrasi.'/'.$r->id_affiliate.'" title="UPDATE PICTURE"> </i> UPDATE PICTURE</a>
                  
                  </td>';
                     }
              $row[] ='</tr>'; 
      $data[] = $row;
     
    }

    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->r->count_all(),
            "recordsFiltered" => $this->r->count_filtered(),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }

    public function edit ($id)
  {
    $data = $this->input_manifestmodel->get_by_id($id);
    $data->create_date = ($data->create_date == '0000-00-00') ? '' : $data->create_date; // if 0000-00-00 set tu empty for datepicker compatibility
    echo json_encode($data);
  }

    public function save(){
    // $this->form_validation->set_rules('nama','nama','required|trim');
     //jika validasi dijalankan dan benar
          
    $data = $this->input->post(null,true);
      
      

       $this->_validate();
      $id_registrasi = $this->input->post('id_registrasi');
               $date_now = date('Y-m-d');
    $expire_date=date_create($this->input->post('isui_date'));
    date_add($expire_date,date_interval_create_from_date_string("50 month"));
      if ($date_now > date_format($expire_date,"Y-m-d") ){
          $this->session->set_flashdata('info', "ISSUE DATE PASSPORT ANDA SUDAH EXPIRE / MELEBIHI 5 TAHUN.");
            // redirect('manifest_affiliate');
      }else{ 
        //print_r('expression');
      $send = $this->input_manifestmodel->update_data($data);
                // $this->session->set_flashdata('info', "SUKSES UPDATED DATA MANIFES.");
      //        // redirect('manifest_affiliate');
                echo json_encode(array("status" => TRUE));
      }

    }

    private function _status2($take){
      $x='0';
     $status = array('0'=>'X','1'=>'Y');
      if($take)
          return $status[$take];
        // return $status;
 
  }

  public function detail(){
  


      if(!$this->general->privilege_check_affiliate(INPUT_MANIFEST,'view'))
        $this->general->no_access();
      $id = $this->uri->segment(4);
      $detail = $this->input_manifestmodel->get_detail_manifest($id);
      if(!$detail){
          show_404();
      }
     
      $detail_keluarga = $this->input_manifestmodel->get_detail_hubkeluarga($id);
       
        $detail['status_pasport'] = $this->_status2($detail['status_pasport']);
        $detail['status_identitas'] = $this->_status2($detail['status_identitas']);
        $detail['status_kk'] = $this->_status2($detail['status_kk']);
        $detail['status_photo'] = $this->_status2($detail['status_photo']);
        $detail['status_vaksin'] = $this->_status2($detail['status_vaksin']);
        $detail['status_buku_nikah'] = $this->_status2($detail['status_buku_nikah']);
        $detail['status_akte'] = $this->_status2($detail['status_akte']);
      $data = array(

         
             'detail'=>$detail,
             'detail_keluarga' => $detail_keluarga
             );

      $this->template->load('template/template', $this->folder.'/detail_manifestaffiliate',($data));

  }

   public function get_data_pic(){
  


      // if(!$this->general->privilege_check_affiliate(INPUT_MANIFEST,'view'))
      //   $this->general->no_access();
      $id = $this->uri->segment(4);
      $detail = $this->input_manifestmodel->get_data_pic($id);
      if(!$detail){
          show_404();
      }
     
     
      $data = array(
         
             'detail'=>$detail,
             );

      $this->template->load('template/template', $this->folder.'/input_manifestpic',($data));

  }




    private function _validate()
  {
    $data = array();
    $data['error_string'] = array();
    $data['inputerror'] = array();
    $data['status'] = TRUE;

    if($this->input->post('nama_passport') == '')
    {
      $data['inputerror'][] = 'nama_passport';
      $data['error_string'][] = 'Nama Passport  Harus di isi';
      $data['status'] = FALSE;
    }

    if($this->input->post('no_pasport') == '')
    {
      $data['inputerror'][] = 'no_pasport';
      $data['error_string'][] = 'No Passport Harus di isi';
      $data['status'] = FALSE;
    }

    if($this->input->post('issue_office') == '')
    {
      $data['inputerror'][] = 'issue_office';
      $data['error_string'][] = 'issue_officeis Harus di isi';
      $data['status'] = FALSE;
    }

    if($this->input->post('isui_date') == '')
    {
      $data['inputerror'][] = 'isui_date';
      $data['error_string'][] = 'isui_date Harus di isi';
      $data['status'] = FALSE;
    }

    if($this->input->post('telp') == '')
    {
      $data['inputerror'][] = 'telp';
      $data['error_string'][] = 'No Telp Harus di isi';
      $data['status'] = FALSE;
    }
     if($this->input->post('telp_2') == '')
    {
      $data['inputerror'][] = 'telp_2';
      $data['error_string'][] = 'No Telp Orang tidak serumah Harus di isi';
      $data['status'] = FALSE;
    }
     if($this->input->post('tanggal_lahir') == '')
    {
      $data['inputerror'][] = 'tanggal_lahir';
      $data['error_string'][] = 'Tanggal Lahir Harus di isi';
      $data['status'] = FALSE;
    }
    if($this->input->post('tempat_lahir') == '')
    {
      $data['inputerror'][] = 'tempat_lahir';
      $data['error_string'][] = 'Tempat Lahir Harus di isi';
      $data['status'] = FALSE;
    }

     if($this->input->post('kelamin') == '')
    {
      $data['inputerror'][] = 'kelamin';
      $data['error_string'][] = 'Jenis Kelamin Harus di isi';
      $data['status'] = FALSE;
    }

     if($this->input->post('ket_keberangkatan') == '')
    {
      $data['inputerror'][] = 'ket_keberangkatan';
      $data['error_string'][] = 'Status Pergi dengan Harus di isi';
      $data['status'] = FALSE;
    }
    if($data['status'] === FALSE)
    {
      echo json_encode($data);
      exit();
    }
  }




public function update_picture(){
    // $this->form_validation->set_rules('nama','nama','required|trim');
     //jika validasi dijalankan dan benar
          
    $data = $this->input->post(null, true);
        try {
            $flag = 0;
            $rename_file = array();
            for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                if ($_FILES['pic']['name'][$i]) {
                    $rename_file[$i] = 'pic' . ($i + 1) . '_' . $_FILES['pic']['name'][$i];
                    $flag++;
                } else {
                    $rename_file[$i] = '';
                }
            }
            if ($flag = 0) {
                throw new Exception('Tidak ada file yang diupload!');
            }
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name" => $rename_file,
                'upload_path' => './assets/images/foto_copy/',
                'allowed_types' => 'gif|jpg|png|jpeg|pdf',
                'max_size' => '2000' //Max 2MB
            ));
            if(!$this->upload->do_multi_upload("pic")){
                throw new Exception('Ukuran file melebihi 2MB, silahkan Ulangi Lagi!');
                // $error = array('error' => $this->upload->display_errors());
                // echo "Errors Occured : "; //sini aja lah
                // print_r($error);
            }
            $info = $this->upload->get_multi_upload_data();
            foreach ($info as $in) {
                $picx = substr($in['file_name'], 0, 4);
                $data[$picx] = $in['file_name'];
            }
            if (!$this->input_manifestmodel->update_data_pic($data)){
                throw new Exception('GAGAL UPDATED PICTURE DATA MANIFES!');
            }
            $this->session->set_flashdata('info', "SUKSES UPDATED PICTURE DATA MANIFES.");
            redirect('manifest_affiliate/input_manifest/data_manifest/');
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->session->set_flashdata('info', $msg);
            redirect('manifest_affiliate/input_manifest/data_manifest/');
        }
         
              

    }
    public function cobadate(){
       $date_now = date('Y-m-d');
      // $isui_date = $this->input->post('isui_date');
      $orderdate = explode("-",'2013-02-04');
    $month = $orderdate[1]-'2';
    $day   = $orderdate[2];
    $year  = $orderdate[0]+ '5';
    // $expire_date = $year ."-".$month."-".$day;
    $expire_date=date_create($year ."-".$month."-".$day);
    echo date_format($expire_date,"Y-m-d");
    }
}
