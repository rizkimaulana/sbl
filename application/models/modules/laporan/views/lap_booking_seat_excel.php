<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=laporan_booking_seat".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>TGL PEMBELIAN</th>
                    <th>INVOICE</th>
                    <th>ID AFFILIATE</th>
                    <th>AFFILIATE</th>
                    <th>JML JAMAAH</th>
                    <th>BOOKING SEAT</th>
                    <th>SISA PEMBAYARAN</th>
                    <th>TOTAL</th>
                    <th>END DATE</th>                    
                    <th>STATUS</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['tgl_pembelian'] ?>  </td>
                        <td><?php echo $pi['invoice'] ?>  </td>
                        <td><?php echo $pi['id_affiliate'] ?>  </td>
                        <td><?php echo $pi['nama_affiliate'] ?>  </td>
                        <td><?php echo $pi['jml_booking_seats'] ?>  </td>
                        <td><?php echo $pi['booking_seats'] ?>  </td>
                        <td><?php echo $pi['sisa_pembayaran'] ?>  </td>
                        <td><?php echo $pi['total_harga'] ?>  </td>
                        <td><?php echo $pi['end_date'] ?>  </td>
                        <td><?php echo $pi['status'] ?>  </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url();?>assets/css/opensans.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/raleway.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
