<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=laporan_reg_gm_mgm_".date('d-M-Y').".xls");
                    ?> 
                    <th>No</th>
                    <th>Nama Jamaah</th>
                    <th>Id User</th>
                    <th>Sponsor</th>
                    <th>Id Sahabat</th>
                    <th>Harga Aktivasi</th>
                    <th>Keterangan</th>
                    <th>Create Date</th>
                    <th>Payment Date</th>
                    <!--<th>TGL AKTIVASI</th>
                    <th>ID AFFILIATE</th>
                    <th>NAMA JAMAAH</th>
                    <th>JML JAMAAH</th>
                    <th>TIPE JAMAAH</th>
                    <th>PAKET</th>
                    <th>TGL BERANGKAT</th>
                    <th>QUARD</th>
                    <th>TRIPLE</th>
                    <th>DOUBLE</th>
                    <th>REFUND</th>
                    <th>FEE</th>
                    <th>FEE INPUT</th>
                    <th>MUHRIM</th>
                    <th>AKOMODASI</th>
                    <th>HANDLING</th>
                    <th>VISA</th>
                    <th>HARGA</th>
                    <th>ROOM PRICE</th>
                    <th>DP BOOKING SEAT</th>
                    <th>JML ANGSURAN</th>
                    <th>ANGSURAN</th>
                    <th>DP ANGSURAN</th>
                    <th>TOTAL</th>
                    <th>KODE UNIK</th>
                    <th>CARA BAYAR</th>
                    <th>JML BAYAR</th>
                    <th>KETERANGAN</th>
                    <th>USER</th>-->
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['nama'] ?>  </td>
                        <td><?php echo $pi['id_user'] ?>  </td>
                        <td><?php echo $pi['sponsor'] ?>  </td>
                        <td><?php echo $pi['id_sahabat'] ?>  </td>
                        <td><?php echo $pi['harga_aktivasi'] ?>  </td>
                        <td><?php echo $pi['keterangan'] ?>  </td>
                        <td><?php echo $pi['create_date'] ?>  </td>
                        <td><?php echo $pi['payment_date'] ?>  </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url();?>assets/css/opensans.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/raleway.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
