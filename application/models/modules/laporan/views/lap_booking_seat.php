<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Laporan Booking Seat</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog full-width">
                            <div class="modal-content">
                                <div class="modal-body" id="antrianUnit" >
                                    <div class="block block-color success">
                                        <div class="header">							
                                            <h3>Data Affiliate</h3>
                                        </div>
                                        <div class="content">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-1 control-label">Nama</label>
                                                    <div class="col-sm-2">
                                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email" readonly="readonly">
                                                    </div>
                                                    <label for="inputEmail3" class="col-sm-1 control-label">ID</label>
                                                    <div class="col-sm-2">
                                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email" readonly="readonly">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="block block-color info">
                                        <div class="header">							
                                            <h3>Data Jamaah</h3>
                                        </div>
                                        <div class="content">
                                            <table class="no-border red">
                                                <thead class="no-border">
                                                    <tr>
                                                        <th style="width:50%;">Task</th>
                                                        <th>Date</th>
                                                        <th class="text-right">Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="no-border-x">
                                                    <tr>
                                                        <td style="width:30%;">Filet Mignon</td>
                                                        <td>05/14/2013</td>
                                                        <td class="text-right">$5,230.000</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%;">Blue beer</td>
                                                        <td>16/08/2013</td>
                                                        <td class="text-right">$5,230.000</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:30%;">T-shirts</td>
                                                        <td>22/12/2013</td>
                                                        <td class="text-right">$5,230.000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="block block-color warning">
                                        <div class="header">							
                                            <h3>Data Aktivasi</h3>
                                        </div>
                                        <div class="content">
                                            <table class="no-border red">
                                                <thead class="no-border">
                                                    <tr>
                                                        <th>Jamaah</th>
                                                        <th>Cara Pembayaran</th>
                                                        <th>Keterangan</th>
                                                        <th>Info</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="no-border-x">
                                                    <tr>
                                                        <td>Filet Mignon</td>
                                                        <td>05/14/2013</td>
                                                        <td>$5,230.000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->


                    <form action="<?php echo base_url(); ?>laporan/export_booking_seat" role="form" method="POST"  class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tgl Pembelian</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tgl.Pembelian</th>
                                    <th>Invoice</th>
                                    <th>Affiliate</th>
                                    <th>Jml Jamaah</th>
                                    <th>Booking Seats</th>
                                    <th>Sisa Pembayaran</th>
                                    <th>Total</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('laporan/json_booking_seat') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                }
            },
        });
        $('#btn-filter').click(function () { //button filter event click
            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            table.ajax.url("<?php echo site_url('laporan/json_booking_seat'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table
        });

    });
    function detail(id_booking_seat) {
        
        var link2 = '<?php echo site_url('laporan/get_detail_booking'); ?>';
        link2 = link2 + "/" + id_booking_seat;
        $.get(link2, function (data) {
            $('#antrianUnit').empty();
            $('#antrianUnit').append(data);
        });

        $('#myModal').modal('show');
    }
    ;
</script>