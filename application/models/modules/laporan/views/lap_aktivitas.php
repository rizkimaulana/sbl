<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Log Aktivitas</b>
                </div>
                <div class="panel-body">
                    <div class="modal fade" id="md-default" tabindex="-1" role="dialog">
                        <div class="modal-dialog full-width">
                            <div class="modal-content">
                                <div class="modal-body" id="antrianUnit" >
                                    <div class="block block-color success">
                                        <div class="header">							
                                            <h3>Detail Aktivitas</h3>
                                        </div>
                                        <div class="content">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="tanggal" name="tanggal" readonly="readonly">
                                                    </div>
                                                    <label for="ip_address" class="col-sm-2 control-label">IP Address</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="ip_address" name="ip_address" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="username" class="col-sm-2 control-label">Username</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="username" name="username" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="aktivitas" class="col-sm-2 control-label">Aktivitas</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="aktivitas" name="aktivitas" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="konten" class="col-sm-2 control-label">Konten</label>
                                                    <div class="col-sm-8">
                                                        <textarea id="konten" name="konten" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form action="<?php echo base_url(); ?>laporan/export_log_aktivitas" role="form" method="POST"  class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tanggal</th>
                                    <th>Username</th>
                                    <th>Aktivitas</th>
                                    <th>Konten</th>
                                    <th>IP Address</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('laporan/json_log_aktivitas') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                }
            },
        });
        $('#btn-filter').click(function () { //button filter event click
            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            table.ajax.url("<?php echo site_url('laporan/json_log_aktivitas'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table
        });

    });
    function detail(id_aktivitas){
        var link_rm = "<?php echo site_url('laporan/get_log_aktivitas') ?>";
                link_rm = link_rm + "/" + id_aktivitas;
        $.get(link_rm, function(data){
            $('#tanggal').val(data.created);
            $('#ip_address').val(data.ip_address);
            $('#username').val(data.username);
            $('#aktivitas').val(data.aktivitas);
            $('#konten').val(data.konten);
        },"json");
        $('#md-default').modal('show');
    };
</script>