
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>

    <div class="col-sm-12 col-md-12">
          <div class="block-flat">
               <div class="header">              
                    <h4>KETERANGAN<h4>
                </div>
               <div class="content">
                   Table dibawah ini digunakan untuk setting keluarga, masukan data keluarga sesuai keluarga masing2 
                     
            </div>
         </div>
      </div>
<div id="page-wrapper">
     
    <form   class="form-horizontal" role="form" action='<?= base_url();?>registrasi_jamaah/update_keluarga' method='POST'> 
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>

                 <div class="panel-body">
                    
                         

                        <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                        <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                       <input type="hidden" name="id_user_affiliate" value="<?php echo $booking['id_user_affiliate']?>">
                       <input type="hidden" name="id_product" value="<?php echo $booking['id_product']?>">
                       <input type="hidden" name="category" value="<?php echo $booking['category']?>">
                  <?php 
                          $biaya_setting_ = $this->registrasijamaah_model->get_key_val();
                            foreach ($biaya_setting_ as $key => $value){
                              $out[$key] = $value;
                            }
                        ?>
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                   
                                    <th>Nama </th>
                                    <!-- <th>No Identitas </th> -->
                                   <th>Telp </th>
                                   <th>Umur </th>
                                   <th>Jns Kelamin </th>
                                   <th>Biaya Muhrim </th>
                                    <th>Hubungan Keluarga </th>
                                   <th>Masukan Nama Keluarga </th>
                                </tr>
                            </thead>
                          
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                    <td><input type="hidden" name="data2[<?php echo $no;?>][id_jamaah]" class="form-control" value="<?php echo $pi['id_jamaah']?>" id="id_jamaah" readonly="true"><?php echo $no; ?></td>
                                      
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['telp']?>  </td>
                                      <td><?php echo $pi['umur']?>  </td>
                                      <td><?php echo $pi['kelamin']?>  </td>
                                      <td>
                                       
                                         <select  required class="form-control" name="data2[<?php echo $no;?>][muhrim]" id="hubungan">
                                                <option value="">- PILIH BIAYA MUHRIM-</option>
                                                <option value="<?php echo $out['MUHRIM'];?>">DIKENAKAN BIAYA MUHRIM</option>
                                                 <option value="0">TIDAK DIKENAKAN BIAYA MUHRIM</option>
                                         </select>
                                      </td>
                                      <td>
                                          <select  required class="form-control" name="data2[<?php echo $no;?>][hubungan]" id="hubungan">
                                                <option value=''>- PILIH HUBUNGAN KELUARGA -</option>
                                              <?php foreach($family_relation as $fr){?>
                                                  <option value='<?php echo $fr->id_relation;?>'><?php echo $fr->keterangan;?></option>
                                              <?php } ?>
                                          </select>
                                      </td>
                                       <td width="25%">
                                     
                                          <select required class="form-control selectOnlyOnce_jamaah" name="data2[<?php echo $no;?>][select_keluarga]" id="select_keluarga" >
                                            <option value=''>- PILIH KELUARGA-</option>
                                              <?php foreach($select_keluarga as $sk){?>
                                                  <option value='<?php echo $sk->id_jamaah;?>'><?php echo $sk->nama;?></option>
                                              <?php } ?>
                                          </select>
                                      </td>
                                  </tr>

                                  <?php } ?> 


                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
                    
              </div>
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> LANJUTKAN</button>
          </div>
      </div>
    </div>
     <?php 
      $id_group_concat = $this->registrasijamaah_model->get_id_group_concat_keluarga();
        foreach ($id_group_concat as $key => $value){
          $id_concat_keluarga[$key] = $value;
        }
    ?>

    <?php 
      $ket_group_concat = $this->registrasijamaah_model->get_ket_group_concat_keluarga();
        foreach ($ket_group_concat as $key => $value){
          $ket_concat_keluarga[$key] = $value;
        }
    ?>

    <?php 
      $id_group_id_jamaah = $this->registrasijamaah_model->get_group_concat_id_jamaah();
        foreach ($id_group_id_jamaah as $key => $value){
          $id_jamaah_group_concat[$key] = $value;
        }
    ?>

    <?php 
      $group_jamaah = $this->registrasijamaah_model->get_group_concat_jamaah();
        foreach ($group_jamaah as $key => $value){
          $jamaah_group_concat[$key] = $value;
        }
    ?>

      <!-- <p><?php echo $id_jamaah_group_concat[$booking['id_booking']];?></p> -->
      <!-- <input type="text" name="www" value="<?php echo $id_concat_keluarga['id_con'];?>"  class="form-control" readonly="true"> --> 
  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>

<script type="text/javascript">
  var prefArr_hubungan = [<?php echo  $id_concat_keluarga['id_con'];?>];
  prefText_hubungan =  ["<option value=''>- PILIH HUBUNGAN KELUARGA -</option>",
    <?php echo  $ket_concat_keluarga['ket'];?>]
// var prefText_hubungan =  ["<option value=''>- PILIH HUBUNGAN KELUARGA -</option>",
//                   "<option value='1'>Suami Dari</option>",
//                   "<option value='3'>Istri dari</option>",
//                   "<option value='4'>Anak dari</option>",
//                   "<option value='5'>Cucu dari</option>",
//                   "<option value='6'>IBU dari</option>"];


jQuery('body').on("change", ".select_keluarga", function () {
    var selectedPrefs = [];
    //What items are selected
    jQuery(".select_keluarga").each(function () {
        selectedPrefs.push(jQuery(this).val());
    });
    //make sure to add selection back to other selects
    jQuery(".select_keluarga").each(function (i, select) {
        jQuery(select).empty();
        jQuery(select).append(prefText_hubungan);
        var elZero = selectedPrefs[i];
        jQuery(select).val(elZero);
    });
    //remove already selected options
    jQuery(".select_keluarga").each(function (i, select) {
        jQuery(".select_keluarga option").each(function (ii, option) {
            if (jQuery(option).val() != "" && selectedPrefs[i] == jQuery(option).val() && selectedPrefs[i] != jQuery(option).parent().val()) {
                jQuery(option).remove();
            }
        });
    });
});

$(".chzn-select").css('width', '300px').chosen({
    display_selected_options: false,
    no_results_text: 'not found',
  }).change(function(){
        var me = $(this);
        $(".search-choice").each(function(){
            var text = $('span',this).text();

            $("option").each(function(){
                if ($(this).text() == text) { 
                    $(this).prop('disabled',true);
                                            }
                //else $(this).prop('disabled',false);
            });
        });
        $(".chzn-select").trigger("chosen:updated");
});
</script>



<script type="text/javascript">
  var prefArr = [<?php echo  $id_jamaah_group_concat[$booking['id_booking']];?>];
var prefText =  ["<option value=''>- PILIH KELUARGA-</option>",
                <?php echo $jamaah_group_concat[$booking['id_booking']];?>];


jQuery('body').on("change", ".selectOnlyOnce_jamaah", function () {
    var selectedPrefs = [];
    //What items are selected
    jQuery(".selectOnlyOnce_jamaah").each(function () {
        selectedPrefs.push(jQuery(this).val());
    });
    //make sure to add selection back to other selects
    jQuery(".selectOnlyOnce_jamaah").each(function (i, select) {
        jQuery(select).empty();
        jQuery(select).append(prefText);
        var elZero = selectedPrefs[i];
        jQuery(select).val(elZero);
    });
    //remove already selected options
    jQuery(".selectOnlyOnce_jamaah").each(function (i, select) {
        jQuery(".selectOnlyOnce_jamaah option").each(function (ii, option) {
            if (jQuery(option).val() != "" && selectedPrefs[i] == jQuery(option).val() && selectedPrefs[i] != jQuery(option).parent().val()) {
                jQuery(option).remove();
            }
        });
    });
});

$(".chzn-select").css('width', '300px').chosen({
    display_selected_options: false,
    no_results_text: 'not found',
  }).change(function(){
        var me = $(this);
        $(".search-choice").each(function(){
            var text = $('span',this).text();

            $("option").each(function(){
                if ($(this).text() == text) { 
                    $(this).prop('disabled',true);
                                            }
                //else $(this).prop('disabled',false);
            });
        });
        $(".chzn-select").trigger("chosen:updated");
});
</script>

<script type="text/javascript">
    (function (global) { 

    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };          
    }

})(window);
</script>