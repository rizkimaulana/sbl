<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak_registrasi extends CI_Controller {
	var $folder = "registrasi_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('registrasijamaah_model');
		$this->load->helper('fungsi');
		// $this->load->model('registrasijamaah_model');
		// $this->load->model('settingdata_model');
		//$this->load->model('setting_m');
		$this->load->library('terbilang');
	}


	

	function cetak($id_booking) {
		
		$registrasi = $this->registrasijamaah_model->lap_data_jamaah($id_booking);

		$opsi_val_arr = $this->registrasijamaah_model->get_key_val();
		foreach ($opsi_val_arr as $key => $value){
			$out[$key] = $value;
		}

		$this->load->library('Struk');
		$pdf = new Struk('P', 'mm', 'A5', true, 'UTF-8', false);
		$pdf->set_nsi_header(false);
		// $resolution = array(280, 110);
		$pdf->AddPage('P');
		$html = '
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 7pt; text-align: center;}
		</style>';
		$html .= ''.$pdf->nsi_box($text =' <table width="100%">
			<tr>
				<td colspan="2" class="h_kanan"><strong>PT SOLUSI BALAD LUMAMPAH</strong></td>
			</tr>
			<tr>
				<td width="20%"><strong>STRUK</strong>
					<hr width="500%">
				</td>
				<td class="h_kanan" width="80%">WISMA BUMI PUTRA- 5&7th Floor Jl. Asia Afrika No 141-149 Bandung</td>
			</tr>

		</table>', $width = '100%', $spacing = '0', $padding = '1', $border = '0', $align = 'left').'';
		$no =1;
		foreach ($registrasi as $row) {
			// $registrasi= $this->registrasijamaah_model->lap_data_jamaah($row->id_booking);
			// $jns_simpan= $this->cetak_simpanan_m->get_jenis_simpan($row->jenis_id);

			// $tgl_bayar = explode(' ', $row->tgl_transaksi);
			// $txt_tanggal = jin_date_ina($tgl_bayar[0]);
			// $txt_tanggal .= ' / ' . substr($tgl_bayar[1], 0, 5);

			// if($row->nama_penyetor ==''){
			// 	$penyetor = '-';
			// }else{
			// 	$penyetor = $row->nama_penyetor;
			// }

			// if($row->alamat ==''){
			// 	$alamat = '-';
			// } else {
			// 	$alamat = $row->alamat;
			// }

        //'.'AG'.sprintf('%04d', $row->anggota_id).'
			$html .='<table width="100%">
			<tr>
				<td width="25%"> No INVOICE </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri">#'.$row->invoice.'</td>

				
			</tr>
		
			<tr>
				<td width="25%"> Tanggal Transaksi </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri">'.$row->create_date.'</td>

				<td width="30%" class="h_kiri">BANK Transfer A/N SBL</td>
			</tr>
		
			<tr>
				<td> ID Affiliate </td>
				<td>:</td>
				<td>'.$row->id_user_affiliate.'</td>
				
				
				
				<td> BANK MANDIRI </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['MANDIRI'].'</td>
			</tr>
			<tr>
				<td> Nama </td>
				<td>:</td>
				<td>'.$row->affiliate.'</td>

				<td> BANK BNI </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['BNI'].'</td>
			</tr>
			<tr>
				<td> Paket </td>
				<td>:</td>
				<td>'.$row->paket.'</td>

				<td> BANK BCA </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['BCA'].'</td>

			</tr>
			<tr>
				<td> TGL Keberangkatan </td>
				<td>:</td>
				<td>'.$row->tgl_keberangkatan.'</td>


				<td> BANK MANDIRI Syariah </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['MANDIRI_Syariah'].'</td>
			</tr>
			<tr>
				<td> Jama Keberangkatan </td>
				<td>:</td>
				<td>'.$row->jam_keberangkatan.'</td>

				<td> BANK BRI </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['BRI'].'</td>
			</tr>
			<tr>
				<td> Lama Menunggu </td>
				<td>:</td>
				<td>'.$row->bulan_menunggu.' .Bulan</td>
				
				<td> BANK MUAMALAT </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['MUAMALAT'].'</td>
			</tr>
		
			<tr>
				<td> Jumlah Jamaah </td>
				<td>:</td>
				<td>'.$row->jumlah_jamaah.' .Orang</td>
				
				
			</tr>
			<tr>
				<td> Quard </td>
				<td>:</td>
				<td>'.$row->quard_2.' .Kamar</td>
				

			</tr>
			<tr>
				<td> Double </td>
				<td>:</td>
				<td>'.$row->double_2.' .Kamar</td>
				
			</tr>
			<tr>
				<td> Tripel </td>
				<td>:</td>
				<td>'.$row->triple_2.' .Kamar</td>
				
			</tr>
			<tr>
				<td> Harga Ganti Kamar </td>
				<td>:</td>
				<td>Rp. '.number_format($row->Room_Price_2).'</td>

					

			</tr>
			<tr>
				<td> Biaya Visa </td>
				<td>:</td>
				<td>Rp. '.number_format($row->visa).'</td>

			</tr>
			<tr>
				<td> Biaya Muhrim </td>
				<td>:</td>
				<td>Rp. '.number_format($row->muhrim).'</td>

			</tr>
			<tr>
				<td> Refund </td>
				<td>:</td>
				<td>Rp. '.number_format($row->refund).'</td>

			</tr>
			<tr>
				<td> Akomodasi </td>
				<td>:</td>
				<td>Rp. '.number_format($row->akomodasi).'</td>

			</tr>
			<tr>
				<td> Harga Paket </td>
				<td>:</td>
				<td>Rp. '.number_format($row->harga).'</td>

			</tr>
			<tr>
				<td> Total Harga Wajib dibayar </td>
				<td>:</td>
				<td>Rp. '.number_format($row->Total_Bayar).'</td>

			</tr>
			<tr>
				<td> Terbilang </td> 
				<td>:</td>
				<td colspan="3"><strong>'.$this->terbilang->eja($row->Total_Bayar).' RUPIAH </strong></td>


				
			</tr>
			<tr>
				
				<td> Ketentuan dan Syarat< </td> 
				<td>:</td>
				<td colspan="5"> Untuk Waktu Pembayaran Dibatasi 4jam, Apabila Pembayaran lebih dari 4jam data yang anda Registrasikan akan Terhapus Otomatis dan pembayaran lebih 4jam harus Registrasi ulang.</td>
				
				
			</tr>';
		}
		$html .= '</table> 
		';
		$pdf->nsi_html($html);
		$pdf->Output(date('Ymd_His') . '.pdf', 'I');
	} 

}

// $html .= '</table> 
// 		<p class="txt_content"></p>

// 		<p class="txt_content">Ref. '.date('Ymd').'<br> 
// 			Informasi Hubungi Call Center : 23423423
// 			<br>
// 			atau dapat diakses melalui : web
// 		</p>';
// 		$pdf->nsi_html($html);
// 		$pdf->Output(date('Ymd_His') . '.pdf', 'I');