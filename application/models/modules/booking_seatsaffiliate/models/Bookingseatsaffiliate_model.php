<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bookingseatsaffiliate_model extends CI_Model{
    var $table = 'view_booking_seats';
    var $column_order = array(null, 'id_booking_seats','id_schedule','invoice','id_affiliate','sponsor','tgl_pembelian','jml_booking_seats','schedule','booking_seats','sisa_pembayaran','total_harga','end_date','status'); //set column field database for datatable orderable
    var $column_search = array('id_booking_seats','id_schedule','invoice','id_affiliate','sponsor','tgl_pembelian','jml_booking_seats','schedule','booking_seats','sisa_pembayaran','total_harga','end_date','status'); //set column field database for datatable searchable 
    var $order = array('tgl_pembelian' => 'asc'); 
    public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }
    private $db2;
    private $_table="booking_seats";
    private $_primary="id_booking_seats";

     private $_booking_seats_list="booking_seats_list";
    private $_id_booking_seats="id_booking_seats";

    private $_booking="booking";
    private $_id_booking="id_booking";

 private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi_jamaah="id_registrasi";

    public function generate_kode($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('md');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }

        public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save_get_bookingseats($data){
      $schedule = $this->input->post('radio');
      
            $stored_procedure = "CALL view_schedule_promo_booking_seats(?)";
             $query =  $this->db->query($stored_procedure,array('id_schedule'=>$schedule))->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
        $end_date = $value['end_date'];
      }



        $initial = "BS";
        $arr = array(
                                          
            'id_affiliate' => $this->session->userdata('id_user'),
            'id_affiliate_type' => $this->session->userdata('id_affiliate_type'),
            'status'=>0,
            'status_fee'=>4,
            'id_schedule'=>$id_schedule,
            'schedule'=>$date_schedule,
            'harga'=>$harga,
            'booking_seats' => $data['booking_seats'],
            'kode_unik' => $data['unique_digit'],
            'tgl_pembelian' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'end_date' => $end_date,
            'kode_unik' => random_3_booking_seats()
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_table,$arr);
        $id_booking_seats =  $this->db->insert_id(); 
        $this->db->update($this->_table,
                    array('invoice'=> $this->generate_kode($initial.$id_booking_seats)),
                    array('id_booking_seats'=>$id_booking_seats));

        
            $id_affiliate = $this->session->userdata('id_user');
           
   $stored_procedure = "CALL all_refrensi_fee(?)";
             $query =  $this->db->query($stored_procedure,array('id_user'=>$id_affiliate))->result_array();
              
              // $sql = "SELECT *
              //   FROM affiliate_type 
              //   WHERE id_affiliate_type = '".$this->session->userdata('id_affiliate_type')."' 
              //    ";
              //   $query4 = $this->db->query($sql)->result_array();
            if(count($query) > 0){
              foreach($query as $key=>$value){
                $sponsor = $value['id_refrensi'];
                $fee_sponsor_paketpromo  = $value['fee_sponsor_paketpromo'];
                $fee_posting_paketpromo = $value['fee_posting_paketpromo'];
              }
           $biaya_mutasi_ = $this->bookingseatsaffiliate_model->get_key_val();
          foreach ($biaya_mutasi_ as $key => $value){
            $out[$key] = $value;
          }
// print_r($fee_posting_paketpromo);
        $jml_seats = $this->input->post('jml_seats');
        for($i=0; $i < $jml_seats; $i++) {
            $arr = array(
                'id_booking_seats' => $id_booking_seats,   
                'kd_booking' => $this->session->userdata('id_user').'-'.$id_booking_seats.'-'.($i + 1),
                'nama' => $this->session->userdata('id_user').'-'.$id_booking_seats.'-'.($i + 1),
                'id_affiliate' => $this->session->userdata('id_user'),
                'id_affiliate_type' => $this->session->userdata('id_affiliate_type'),
                'sponsor' => $this->session->userdata('sponsor'),
                // 'biaya_mutasi'=>$out['BIAYA_MUTASI_NAMA'],
                'harga' => $harga,
                'booking_seats' => $data['booking_seats'],
                'sisa_pembayaran' => ($harga - $fee_posting_paketpromo)-$data['booking_seats'],
                'id_schedule'=>$id_schedule,
                'schedule'=>$date_schedule,
                'fee'=>$fee_posting_paketpromo,
                'fee_refrensi' => $fee_sponsor_paketpromo,
                'status'=>0,
                 'status_fee'=>4,
                 'status_fee_refrensi'=>0,
                 'tgl_pembelian' => date('Y-m-d H:i:s'),
                 'create_date' => date('Y-m-d H:i:s'),
                 'create_by'=>$this->session->userdata('id_user'),
                 'status_booking'=>0,
                 'status_pelunasan'=>0,
                  'end_date' => $end_date,
                   'status_mutasi' => 0,
               );
        
        $this->db->insert($this->_booking_seats_list,$arr);
  
     
        }

    
  $sql = "SELECT * from schedule
          WHERE  id_schedule = '".$id_schedule."'";
        $query = $this->db->query($sql)->result_array();

         foreach($query as $key=>$value){
            $seats = $value['seats'];
           
          }


        $sql = "SELECT COUNT(*) as jumlah_jamaah from booking_seats_list
          WHERE  id_booking_seats = '".$id_booking_seats."'";
        $query = $this->db->query($sql)->result_array();


          foreach($query as $key=>$value){
            $jumlah_jamaah = $value['jumlah_jamaah'];
            if($jumlah_jamaah > 0){
            $arr = array(
                
                         
                    'seats' => ($seats - $jumlah_jamaah),  
                );       
                 $this->db->update('schedule',$arr,array('id_schedule'=>$id_schedule));
            }
          }

         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('booking_seatsaffiliate/report_booking_seats/'.$id_booking_seats.'');
            return true;
          }
        }
    }


public function save_booking_mutasi($data){
       $initial = "P1";
      
       $id_schedule = $this->input->post('id_schedule');

            $stored_procedure = "CALL view_schedule_promo_booking_seats(?) ";
      $query = $this->db->query($stored_procedure,array('id_schedule'=>$id_schedule))->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
        $end_date = $value['end_date'];
      }
// print_r($id_schedule);
      $arr = array(
                                          
            'id_user_affiliate' => $this->session->userdata('id_user'),
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'schedule' => $date_schedule,
            'harga' => $harga,
            'create_date' => date('Y-m-d H:i:s'),
            'kode_unik'=>$data['unique_digit'],
            'create_by'=>$this->session->userdata('id_user'),
            'status_fee'=>4,
            'tipe_jamaah'=>9,
            'status'=>0,
            'id_booking_seats'=>$data['id_booking_seats'],
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        
        $invoice =  $this->db->insert_id(); 
        
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));


        $id_booking_seats = $this->input->post('id_booking_seats');

         $sql = "SELECT * from booking_seats where id_booking_seats ='$id_booking_seats'
           ";
          $query = $this->db->query($sql)->result_array();
          foreach($query as $key=>$value){
            $status_bookingan = $value['status'];
          }

          if ($status_bookingan ==2){
           $arr = array(
              
                'status'=>1,

            );       
            
              $this->db->update('booking',$arr,array('id_booking'=>$id_booking));
          }else{
            $arr = array(
              
                'status'=>0,

            );       
            
              $this->db->update('booking',$arr,array('id_booking'=>$id_booking));
          }


          
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('booking_seatsaffiliate/registrasi/'.$id_booking.'/'.$id_booking_seats);
            return true;

        }


       
    }

  
    
    
      function cek($no_identitas){
        $invoice = $this->input->post('invoice');
        $query = $this->db->query("SELECT * FROM data_jamaah_aktif Where no_identitas ='$no_identitas' and invoice='$invoice'");
        return $query;
    }

    function cek_pindah_paket($id_sahabat){
        $id_sahabat = $this->input->post('id_sahabat');
        $query = $this->db->query("SELECT * FROM pindah_paket Where id_sahabat ='$id_sahabat' and status in('0','1','2','6')");
        return $query;
    }
      function cek_id_jamaah($id_jamaah){
        $id_jamaah = $this->input->post('id_jamaah');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where id_jamaah ='$id_jamaah'");
        return $query;
    }
    
          function cek_room($id_booking){
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM room_order Where id_booking='$id_booking'");
        return $query;
    }
    
    public function get_data($offset,$limit,$q=''){
      $id_user = $this->session->userdata('id_user');  
 
         $sql = " SELECT * FROM faktur_booking_seats  
                    WHERE  status in('1','2') and id_affiliate='".$id_user."'
                    ";
        
        if($q){
         

            $sql .=" AND affiliate LIKE '%{$q}%'
                    OR id_affiliate LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY tgl_pembelian DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    

    
    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

    function get_fee_jamaah_kuota($affiliate_type='') {
      $this->db->where("id_affiliate_type",$affiliate_type);
      return $this->db->get("affiliate_type");
    }


     public function faktur_booking_seats($id_booking_seats,$id_user){
         $stored_procedure = "call faktur_booking_seats(?,?)";
         return $this->db->query($stored_procedure,
          array('id_booking_seats'=>$id_booking_seats,
                'id_affiliate'=>$id_user
            ))->row_array(); 
    }

    function lap_data_jamaah($id_booking_kuota) {
        $this->db->select('*');
        $this->db->from('faktur_jamaah_kuota');
        $this->db->where('id_booking_kuota',$id_booking_kuota);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }




      public function get_jamaah_kuota_mutasi_nama($id_jamaah){
    
        $sql ="SELECT * from detail_jamaah_kuota_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

    }




     public function get_booking_seats($id_booking_seats,$id_user){
 

    $stored_procedure = "call faktur_booking_seats(?,?)";
         return $this->db->query($stored_procedure,array('id_booking_seats'=>$id_booking_seats,
          'id_affiliate'=>$id_user
            ))->row_array();
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

     function searchItem_paket_promo($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure,$paket=''){
      $paket = $this->input->post('paket');
     // if ($paket=='4') {
               $jumlah_jamaah = $this->input->post('jumlah_jamaah');
          $sql = "SELECT * from view_schedule_promo where  BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.' - '.$row->hari.' hari</strong></td>
                   
                   <td><strong>'.number_format($row->harga).'</strong></td>
                 
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   
 }


 function searchItem_paket_reguller($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure,$paket=''){
      // $paket = $this->input->post('paket');
   
                $jumlah_jamaah = $this->input->post('jumlah_jamaah');
          $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.' - '.$row->hari.' hari</strong></td>
                   
                   <td><strong>'.number_format($row->harga).'</strong></td>
                 
                   <td> <input name="radio[]" class="radio1" type="radio" id="radio1[]" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   
 }




    public function save_registrasi($data){
   
 if ($this->input->post('MyCheckBox') =='1'){
       $id_affiliate_type = $this->session->userdata('id_affiliate_type');
                      $sql = "SELECT fee_pindah_paket from affiliate_type where id_affiliate_type ='$id_affiliate_type'";
                      $query = $this->db->query($sql)->result_array();
                    foreach($query as $key=>$value){
                       $fee_pindah_paket = $value['fee_pindah_paket'];

                     }
        $arr = array(
                          'id_booking'=> $data['id_booking'],
                          'invoice'=> $data['invoice'],
                          'id_schedule'=> $data['id_schedule'],
                          'id_affiliate'=> $data['id_affiliate'],
                          'id_product'=> $data['id_product'],
                          'embarkasi'=> $data['embarkasi'],
                          'tgl_daftar'=>date('Y-m-d H:i:s'),
                          'nama'=> $data['nama'],
                          'tempat_lahir'=> $data['tempat_lahir'],
                          'tanggal_lahir'=> $data['tanggal_lahir'],
                          'status_diri'=> $data['select_statuskawin'],
                          'kelamin'=> $data['select_kelamin'],
                          'rentang_umur'=> $data['rentang_umur'],
                          'no_identitas'=> $data['no_identitas'],
                          'provinsi_id'=> $data['provinsi'],
                          'kabupaten_id'=> $data['kabupaten'],
                          'kecamatan_id'=> $data['kecamatan'],
                          'alamat'=> $data['alamat'],
                          'telp'=> $data['telp'],
                          'telp_2'=> $data['telp_2'],
                          'email'=> $data['email'],
                          'ket_keberangkatan'=>$data['select_status_hubungan'],
                           'ahli_waris'=> $data['waris'],
                          'hub_waris'=> $data['select_hub_ahliwaris'],
                          'room_type'=> 1,
                          'category'=> $data['category'],
                          'merchandise'=> $data['select_merchandise'],
                          'id_bandara'=> $data['id_bandara'],
                          'refund'=> $data['refund'],
                          'handling'=> $data['handling'],
                          'akomodasi'=> $data['akomodasi'],
                          'fee'=> $fee_pindah_paket,
                          'fee_input'=> 0,
                          'harga_paket'=> $data['harga'],
                          'create_by'=>$this->session->userdata('id_user'),
                          'create_date'=>date('Y-m-d H:i:s'),
                          'status'=> 0,
                          'tipe_jamaah'=>10,
                          'kode_unik'=>$data['unique_digit'],
                          'status_fee'=>$data['status_fee'],
                          'dp_angsuran'=> $data['dp'],
                          'angsuran'=> $data['jml_cicilan'],
                          'jml_angsuran'=> $data['cicilan_perbulan'],
                  );       
                    $this->db->trans_begin(); 
                    $this->db->insert($this->_registrasi_jamaah,$arr);
                    $id_registrasi =  $this->db->insert_id(); 
                    $initalpp='PP';
                    $this->db->update($this->_registrasi_jamaah,
                                array('id_jamaah'=> $this->generate_id_jamaah($initalpp.$id_registrasi)),
                                array('id_registrasi'=>$id_registrasi));
      
         $select_kode_booking = $this->input->post('select_kode_booking');
        $sql = "call cek_booking_seats_list(?) ";
        $query = $this->db->query($sql,array('id_list_seats'=>$select_kode_booking))->result_array();
          foreach($query as $key=>$value){
            $booking_seats = $value['booking_seats'];
            $status_pelunasan = $value['status_pelunasan'];
             $harga_paket_bs = $value['harga'];
          }
          
            $arr = array(
                  'id_booking'=> $data['id_booking'],
                  'id_registrasi'=> $id_registrasi,
                  'id_jamaah'=> $this->generate_id_jamaah($initalpp.$id_registrasi),
                  'id_affiliate'=> $data['id_affiliate'],
                  'no_pasport'=> $data['no_pasport'],
                  'issue_office'=> $data['issue_office'],
                  'isui_date'=> $data['isue_date'],
                  'status_identitas'=> $data['status_identitas'],
                  'status_kk'=> $data['status_kk'],
                  'status_photo'=> $data['status_photo'],
                  'status_pasport'=> $data['status_pasport'],
                  'status_vaksin'=> $data['status_vaksin'],
                  'hubkeluarga'=> $data['hubungan'],
                  'create_by'=>$this->session->userdata('id_user'),
                  'create_date'=>date('Y-m-d H:i:s'),
                  'status'=> $status_pelunasan,
              );       
              $this->db->insert('manifest',$arr);

               $arr = array(    

                  'invoice' => $data['invoice'],
                  'id_registrasi' => $id_registrasi,
                  'id_booking'=> $data['id_booking'],
                  'id_schedule'=> $data['id_schedule'],
                  'id_product'=> $data['id_product'],
                  'id_affiliate' => $data['id_affiliate'],
                  'sponsor' => $data['id_refrensi'],
                  'fee' => $fee_pindah_paket,
                  'fee_sponsor' => 0,
                  'create_by'=>$this->session->userdata('id_user'),
                  'create_date'=>date('Y-m-d H:i:s'),
                  'status' => $status_pelunasan,
                 'status_fee'=> 4,
                'status_free'=> 1,
                'status_fee_sponsor'=> 1
              );

              $this->db->insert('fee',$arr);

                    $arr = array(
                    'id_sahabat' =>$data['id_sahabat'],
                    'tgl_pindahpaket'=> date('Y-m-d H:i:s'),
                    'status' =>0
                    );
                  $this->db->insert('pindah_paket',$arr);

                   $arr = array(
                      'id_registrasi' =>$id_registrasi,                   
                      'id_booking' => $data['id_booking'],
                      
                      'status'=>$status_pelunasan
                  );       
                  $this->db->insert('pengiriman',$arr);

                 

                 $arr = array(
                    'id_registrasi' =>$id_registrasi,
                    'id_booking'=> $data['id_booking'],
                    'status_visa' =>$data['select_status_visa'],
                    'tgl_visa' =>$data['tanggal_visa'],
                    'tgl_daftar' =>date('Y-m-d H:i:s'),
                    'status' =>$status_pelunasan
                    );
                  $this->db->insert('visa',$arr);


                     $sql = "call hitung_muhrim(?) ";
                     $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
                    foreach($query as $key=>$value){
                      // $id_registrasi = $value['id_registrasi'];
                      $biaya_muhrim = $value['muhrim'];
                    }


                  $arr = array(
                    'muhrim'=>$biaya_muhrim,
                   
                  );    
                    $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  


                 $sql = "call v_visa(?) ";
                 $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
                    foreach($query as $key=>$value){
                      // $id_registrasi = $value['id_registrasi'];
                      $biaya_visa = $value['Total_VISA'];
                    }

                  $arr = array(
                    'visa'=>$biaya_visa,
                   
                  );    
                    $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 

                  $arr = array(
                    'status_cicilan'=>1,
                   
                  );    
                    $this->db2->update('t_member',$arr,array('userid'=>$data['id_sahabat']));  


                   $arr = array(
                    'status'=>1,
                   
                  );    
                    $this->db2->update('t_cicilan',$arr,array('userid'=>$data['id_sahabat']));  

         $arr = array(
        'id_jamaah'=> $this->generate_id_jamaah($initalpp.$id_registrasi),
        'status_mutasi' =>1,
       
        );
      $this->db->update('booking_seats_list',$arr,array('id_list_seats'=>$select_kode_booking)); 

        $arr = array(
          'dpbooking_seats'=>$booking_seats,
          'status'=>$status_pelunasan,
          'harga_paket'=> $harga_paket_bs,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 

          $arr = array(
            'create_date'=>date('Y-m-d H:i:s'),
            'tgl_daftar'=>date('Y-m-d H:i:s'),
          );    
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));

         $arr = array(
                    'create_date'=>date('Y-m-d H:i:s'),
                    'tgl_daftar'=>date('Y-m-d H:i:s'),
                  );    
                    $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));
                    
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
 }else{
         $arr = array(
                'id_booking'=> $data['id_booking'],
                'invoice'=> $data['invoice'],
                'id_schedule'=> $data['id_schedule'],
                'id_affiliate'=> $data['id_affiliate'],
                'id_product'=> $data['id_product'],
                'embarkasi'=> $data['embarkasi'],
                'tgl_daftar'=>date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['rentang_umur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi'],
                'kabupaten_id'=> $data['kabupaten'],
                'kecamatan_id'=> $data['kecamatan'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'telp_2'=> $data['telp_2'],
                'email'=> $data['email'],
                'ket_keberangkatan'=>$data['select_status_hubungan'],
                'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'room_type'=> 1,
                'category'=> $data['category'],
                'merchandise'=> $data['select_merchandise'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> $data['refund'],
                'handling'=> $data['handling'],
                'akomodasi'=> $data['akomodasi'],
                'fee'=> $data['fee_posting_jamaahpaketpromo'],
                'fee_input'=> 0,
                'harga_paket'=> $data['harga'],
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status'=> 0,
                'tipe_jamaah'=>9,
                'status_fee'=>4,

        );       
          $this->db->trans_begin(); 
        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

       $select_kode_booking = $this->input->post('select_kode_booking');
        $sql = "call cek_booking_seats_list(?) ";
        $query = $this->db->query($sql,array('id_list_seats'=>$select_kode_booking))->result_array();
          foreach($query as $key=>$value){
            $booking_seats = $value['booking_seats'];
            $status_pelunasan = $value['status_pelunasan'];
            $harga_paket_bs = $value['harga'];
          }

        $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_registrasi'=> $id_registrasi,
            'id_jamaah'=>  $this->generate_id_jamaah($id_registrasi),
            'id_affiliate'=> $data['id_affiliate'],
            'no_pasport'=> $data['no_pasport'],
            'issue_office'=> $data['issue_office'],
            'isui_date'=> $data['isue_date'],
            'status_identitas'=> $data['status_identitas'],
            'status_kk'=> $data['status_kk'],
            'status_photo'=> $data['status_photo'],
            'status_pasport'=> $data['status_pasport'],
            'status_vaksin'=> $data['status_vaksin'],
            'hubkeluarga'=> $data['hubungan'],
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 0,
        );       
        $this->db->insert('manifest',$arr);
         $arr = array(    

          'invoice' => $data['invoice'],
          'id_registrasi' => $id_registrasi,
          'id_booking'=> $data['id_booking'],
          'id_schedule'=> $data['id_schedule'],
          'id_product'=> $data['id_product'],
          'id_affiliate' => $data['id_affiliate'],
          'sponsor' => $data['id_refrensi'],
          'fee' => $data['fee_posting_jamaahpaketpromo'],
          'fee_sponsor' => $data['fee_sponsor_paketpromo'],
          'create_by'=>$this->session->userdata('id_user'),
          'create_date'=>date('Y-m-d H:i:s'),
          'status' => $status_pelunasan,
          'status_fee'=> 4,
          'status_free'=> 1,
          'status_fee_sponsor'=> 1
      );

      $this->db->insert('fee',$arr);
        
        $arr = array(    
            
            'id_registrasi' => $id_registrasi,
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_affiliate' => $data['id_affiliate'],
            
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> $status_pelunasan

        );

        $this->db->insert('list_gaji',$arr);

   

         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $data['id_booking'],
            
            'status'=>$status_pelunasan
        );       
        $this->db->insert('pengiriman',$arr);


       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $data['id_booking'],
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>$status_pelunasan
          );
        $this->db->insert('visa',$arr);
      
      $sql = "call v_visa(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();

          foreach($query as $key=>$value){

            $biaya_visa = $value['Total_VISA'];
          }

        $arr = array(
          'visa'=>$biaya_visa,
         
        );    

          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  
      $sql = "call hitung_muhrim(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
          foreach($query as $key=>$value){
            // $id_registrasi = $value['id_registrasi'];
            $biaya_muhrim = $value['muhrim'];
          }

        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 


        
        $arr = array(
        'id_jamaah'=> $this->generate_id_jamaah($id_registrasi),
        'status_mutasi' =>1,
       
        );
      $this->db->update('booking_seats_list',$arr,array('id_list_seats'=>$select_kode_booking)); 




        $arr = array(
          'dpbooking_seats'=>$booking_seats,
          'status'=>$status_pelunasan,
          'harga_paket'=> $harga_paket_bs,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 


          // if ($status_pelunasan == 1){
          //         $arr = array(
          //           'status'=>1,
                   
          //         );    
          //       $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  
          // }
          $arr = array(
                    'create_date'=>date('Y-m-d H:i:s'),
                    'tgl_daftar'=>date('Y-m-d H:i:s'),
                );    
            $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));
      }

       $arr = array(
                    'create_date'=>date('Y-m-d H:i:s'),
                    'tgl_daftar'=>date('Y-m-d H:i:s'),
                  );    
                    $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }


    public function get_pic_keluarga($id_booking,$id){
    
   $stored_procedure = "call setting_keluarga_booking_seats(?,?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking, 'create_by '=>$id
            ))->result_array();       


}

function get_id_group_concat_keluarga() {

        $out = array();
        $this->db->select('id_con,id_relation');
        $this->db->from('group_concat_keluarga');
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->id_con] = $value->id_relation;
                }
                return $out;
        } else {
            return array();
        }
    }

    function get_ket_group_concat_keluarga() {

        $out = array();
        $this->db->select('ket, keterangan');
        $this->db->from('group_concat_keluarga');
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->ket] = $value->keterangan;
                }
                return $out;
        } else {
            return array();
        }
    }

        function get_group_concat_id_jamaah() {

        $out = array();
        $this->db->select('id_booking, id_jamaah');
        $this->db->from('group_concat_jamaah');
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->id_booking] = $value->id_jamaah;
                }
                return $out;
        } else {
            return array();
        }
    }

      function get_group_concat_jamaah() {

        $out = array();
        $this->db->select('id_booking, jamaah');
        $this->db->from('group_concat_jamaah');
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->id_booking] = $value->jamaah;
                }
                return $out;
        } else {
            return array();
        }
    }

     public function update_room_order($data){
        $id_booking = $this->input->post('id_booking');
     $category = $this->input->post('category');
     $id_product = $this->input->post('id_product');
      $room_type = $this->input->post('room_type');
      $double = $this->input->post('double');
      $triple = $this->input->post('triple');
      $pre_double = $this->input->post('pre_double');
      $pre_triple = $this->input->post('pre_triple');
      $jumlah_jamaah = $this->input->post('jumlah_jamaah');

        $sql = "SELECT * from room where category = '$category' and room_type ='2'";
        $query = $this->db->query($sql)->result_array();
        foreach($query as $key=>$value){
            $harga_double = $value['harga_idr'];
        }

        $sql2 = "SELECT * from room where category = '$category' and room_type ='3'";
        $query = $this->db->query($sql2)->result_array();
        foreach($query as $key=>$value){
            $harga_triple = $value['harga_idr'];
        }

    $pilihan_double = $double * 2;
    $pilihan_triple = $triple * 3;


    $total_pilihan  = $pilihan_double + $pilihan_triple;
    if ($total_pilihan > $jumlah_jamaah){
         $this->session->set_flashdata('info', "Jamaah yang anda input = ".$jumlah_jamaah." Orang,  
          <br>Jumlah Kamar yang and Input tidak Sesuai dengan jumlah jamaah yang anda input, isi jumlah kamar sesuai jumlah jamaah yang anda input!!");
       redirect('registrasi_jamaahpromo/room_question/'.$id_booking.'/'.$category.'');    
    }else{



    $total_harga_double = $harga_double * $double;
    $total_harga_triple = $harga_triple * $triple;
               $arr = array(
        
            'id_booking' => $data['id_booking'],
             'id_product' => $data['id_product'],
            'category' => $data['category'],
            'room_type' => 2,
            'jumlah' => $data['double'],
            'harga' => $total_harga_double,
            'status' => 0,
            'status_room_order' => 0,
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
              
        $this->db->insert('room_order',$arr);

        $arr = array(
        
            'id_booking' => $data['id_booking'],
            'id_product' => $data['id_product'],
            'category' => $data['category'],
            'room_type' => 3,
            'jumlah' => $data['triple'],
            'harga' => $total_harga_triple,
            'status' => 0,
            'status_room_order' => 0,
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       

        $this->db->insert('room_order',$arr);

        for($i=0; $i < $pilihan_double; $i++) {
            $arr = array(
                'id_booking' => $data['id_booking'],   
                'category' => $data['category'],
                'room_type' => 2,
                'harga' => $harga_double,
                'status'=>0,
                'create_date' => date('Y-m-d H:i:s'),
                'create_by'=>$this->session->userdata('id_user'),
               );
        $this->db->insert('list_order_room',$arr);
    }
        for($i=0; $i < $pilihan_triple; $i++) {
            $arr = array(
                'id_booking' => $data['id_booking'],   
                'category' => $data['category'],
                'room_type' => 3,
                'harga' => $harga_triple,
                'status'=>0,
                'create_date' => date('Y-m-d H:i:s'),
                'create_by'=>$this->session->userdata('id_user'),
               );
        $this->db->insert('list_order_room',$arr);
}
         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
    }
}

public function update_room_setting($data,$data2='',$data3=''){
     $id_jamaah = $this->input->post('select_jamaah');
    $id_booking = $this->input->post('id_booking');
   
        if(isset($data['data2'])) {
        
            $pic = array();
            foreach($data['data2'] as $pi){
                $id_jamaah = $pi['select_jamaah'];
                $room_order = $pi['room_order'];
                $room_type = $pi['room_type'];
                $tmp = array(
                    'id_jamaah' => $id_jamaah,
                    'room_order' => $room_order,
                    // 'room_type' => $room_type,
                );  
               
                $pic[] = $tmp;

            }

            $this->db->update_batch('registrasi_jamaah',$pic,'id_jamaah'); 
        }

        if(isset($data['data2'])) {
        
            $pic = array();
            foreach($data['data2'] as $pi){
                $id_listorderroom = $pi['id_listorderroom'];
                $id_jamaah = $pi['select_jamaah'];
                
                $tmp = array(
                    'id_listorderroom' => $id_listorderroom,
                    'id_jamaah' => $id_jamaah,
                    
                );  
               
                $pic[] = $tmp;

            }

            $this->db->update_batch('list_order_room',$pic,'id_listorderroom'); 
        }
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
    
}

public function update_setting_keluarga($data,$data2=''){
 $id_jamaah = $this->input->post('id_jamaah');
if(isset($data['data2'])) {
        
            $pic = array();
            foreach($data['data2'] as $pi){
                $id_jamaah = $pi['id_jamaah'];
                $tmp = array(
                
                    'id_jamaah' => $pi['id_jamaah'],
                    'muhrim' => $pi['muhrim'],
                  
                );
               
                $pic[] = $tmp;

            }
          
            $this->db->update_batch('registrasi_jamaah',$pic,'id_jamaah'); 
               // print_r($pic);
              $pic = array();
            foreach($data['data2'] as $pi){
                $id_jamaah = $pi['id_jamaah'];
                $tmp = array(
                
                   'id_jamaah' => $pi['id_jamaah'],
                    'hubkeluarga' => $pi['hubungan'],
                    'keluarga' => $pi['select_keluarga'],
                    
                );
               
                $pic[] = $tmp;

            }
          
            $this->db->update_batch('manifest',$pic,'id_jamaah'); 
         
        }

   // if(isset($data['data2'])) {
   //          $pic = array();
   //          foreach($data['data2'] as $pi){
   //              $id_jamaah = $pi['id_jamaah'];
   //              $tmp = array(
                
   //                 'id_jamaah' => $pi['id_jamaah'],
   //                  'hubkeluarga' => $pi['hubungan'],
   //                  'keluarga' => $pi['select_keluarga'],
                    
   //              );
               
   //              $pic[] = $tmp;

   //          }
          
   //          $this->db->update_batch('manifest',$pic,'id_jamaah'); 

   //        }
   
   
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
}

public function get_pic($id_booking){
    
         $sql ="SELECT * from registrasi_jamaah WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();     


}

  public function get_pic_booking($id_booking,$id){
         
          $stored_procedure = "call view_for_booking_seats(?,?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking,
            'create_by'=>$id
            ))->row_array(); 
    }
public function get_pic_room_type($id_booking){
     

     $stored_procedure = "call list_order_room_view(?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking))->result_array(); 
}

 public function get_pic_booking_room($id_booking,$id){
  
     $stored_procedure = "call room_order_view(?,?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking,
            'create_by'=>$id
            ))->row_array();  
    }

 public function get_pic_order_view($id_booking,$id){

             $stored_procedure = "call room_order_view(?,?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking,
            'create_by'=>$id
            ))->row_array();    
    }


 public function get_report_registrasi($id_booking,$id){
 
      
            $stored_procedure = "call data_laporan_jamah_belumaktif_all(?,?)";
         return $this->db->query($stored_procedure,
            array('id_booking'=>$id_booking,
                    'create_by'=>$id
            ))->row_array();   
    }


 public function get_pic_room_price($category_id){
        $category = $this->input->post('category');
         $sql = "SELECT * from room_price where category_id = ?
         ";
        return $this->db->query($sql,array($category_id))->result_array();  
    }

    private function _get_datatables_query()
    {
         $status = array('1','2');
        $id_user=  $this->session->userdata('id_user');
        $this->db->where_in('id_affiliate', $id_user);
         $this->db->where_in('status', $status);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


   function searchItem_pindahpaket($id_sahabat){
     

        $id_sponsor=  $this->session->userdata('id_affiliate');
        // $jumlah_jamaah = $this->input->post('jumlah_jamaah');
              $stored_procedure = "call pindah_paket(?) ";
            $query = $this->db->query($stored_procedure,array('userid'=>$id_sahabat,
              ));
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="4"><h2 style="color: #9F6000;">Maaf ! ID SAHABAT TIDAK ADA</h2></td></tr>
                <tr>
                   
                   <td><input name="nama_dp" type="text"  id="nama_dp" maxlength="0" class="form-control" required/></td>
                   <td><input name="dp" type="text"  id="dp" class="form-control" maxlength="0" required/></td>
                   <td><input name="jml_cicilan" type="text"  class="form-control" maxlength="0"  id="jml_cicilan"   required/></td>
                   <td><input name="cicilan_perbulan" type="text" class="form-control"  maxlength="0" id="cicilan_perbulan"   required /></td>
                    
                </tr>
       ';

       }else{

        
        foreach ($query->result() as $row){    
       

              echo'<tr>
                   
                   <td>'.$row->nama.'</strong></td>
                   <td width="15%"><input name="dp" type="text"  id="dp" class="form-control"  value='.$row->dp.'  readonly=true/></strong></td>
                   <td><input name="jml_cicilan" type="text"  class="form-control"  id="jml_cicilan"  value='.$row->jml_cicilan.' readonly=true/></strong></td>
                   <td><input name="cicilan_perbulan" type="text" class="form-control" id="cicilan_perbulan"  value='.$row->cicilan_perbulan.'  readonly=true/></strong></td>
                    
                </tr>';

       }

       }
       
       
   }
}
