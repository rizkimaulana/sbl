 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Room Category</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>room/save">
           <div class="form-group">
                <label>Room Type</label>
                <select class="form-control" name="select_type">
                    <?php foreach($select_type as $st){?>
                        <option value="<?php echo $st->id_room_type;?>"><?php echo $st->type;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Room Type</label>
                <select class="form-control" name="select_category">
                    <?php foreach($select_category as $st){?>
                        <option value="<?php echo $st->id_room_category;?>"><?php echo $st->category;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Harga $ </label>
                <input class="form-control" name="harga_us" required>
            </div>
             <div class="form-group">
                <label>Harga Rp</label>
                <input class="form-control" name="harga_idr" required>
            </div>
             <div class="form-group">
                <label>Kapasitas </label>
                <input class="form-control" name="kapasitas" required>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>room" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
