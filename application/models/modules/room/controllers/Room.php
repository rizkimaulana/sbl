<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room extends CI_Controller{
	var $folder = "room";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(ROOM,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('room_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/room');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_category(){
	    
	    return $this->db->get('room_category')->result();
	}

	
public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->room_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->room_type.'</td>';
	                $rows .='<td width="40%">'.$r->room_category.'</td>';
	               
	                $rows .='<td width="9%">'.$r->harga_us.'</td>';
	                $rows .='<td width="9%">'.number_format ($r->harga_idr).'</td>';
	                $rows .='<td width="9%">'.$r->kapasitas.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'room/edit/'.$r->id_room.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room('."'".$r->id_room."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'room/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->room_model->save($data);
	     if($send)
	        redirect('room');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(ROOM,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_type'=>$this->_select_type(),
	    	'select_category'=>$this->_select_category()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);		
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(ROOM,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('room',array('id_room'=>$id))->row_array();
	   if(!$get)
	        show_404();
	        
	    $data = array('select_type'=>$this->_select_type(),
	    	'select_category'=>$this->_select_category()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->room_model->update($data);
	     if($send)
	        redirect('room');
		
	}
	
	
	public function ajax_delete($id_room)
	{
		if(!$this->general->privilege_check(ROOM,'remove'))
		    $this->general->no_access();
		$send = $this->room_model->delete_by_id($id_room);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('room');
	}


}
