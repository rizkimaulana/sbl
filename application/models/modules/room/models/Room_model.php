<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Room_model extends CI_Model{
   
   
    private $_table="room";
    private $_primary="id_room";

    public function save($data){
    
        // $cek = $this->db->select('ket_hubungan')->where('ket_hubungan',$data['ket_hubungan'])->get('hubungan_muhrim')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
            'room_type' => $data['select_type'],
            'category' => $data['select_category'],
            'harga_us' => $data['harga_us'],
            'harga_idr' => $data['harga_idr'],
            'kapasitas' => $data['kapasitas'],
        );       
        
         return $this->db->insert('room',$arr);
    }


    public function update($data){
        
        $arr = array(
        
           'room_type' => $data['select_type'],
            'category' => $data['select_category'],
            'harga_us' => $data['harga_us'],
            'harga_idr' => $data['harga_idr'],
            'kapasitas' => $data['kapasitas'],
            
           
        );       
              
        return $this->db->update($this->_table,$arr,array('id_room'=>$data['id_room']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = " SELECT * FROM view_room  
                    WHERE 1=1
                    ";
        
        if($q){
         

            $sql .=" AND room_type LIKE '%{$q}%'
                    OR room_category LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_room DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_room)
    {
        $this->db->where('id_room', $id_room);
        $this->db->delete($this->_table);
    }
    
    
}
