<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_mgm extends CI_Controller{ 
	var $folder = "member_mgm";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(MEMBER_MGM,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','member_mgm');	
		$this->load->model('membermgm_model');
		$this->load->helper('fungsi');
		$this->load->library('terbilang');
		$this->load->library('ci_qr_code');
        $this->config->load('qr_code');
	}
	
	public function index(){
		$id_user = $this->session->userdata('id_user');
	    $get_status_gm = $this->membermgm_model->get_status_gm($id_user);

		 if ($get_status_gm['mgm'] =='1'){

	    $this->template->load('template/template', $this->folder.'/member_mgm');
		}else{
		$this->template->load('template/template', $this->folder.'/syaratketentuan');	
		}
	}

	public function upgrade_perwakilanlama(){
		 
	    $this->template->load('template/template', $this->folder.'/perwakilanupgrade_gm');
		$id_user = $this->session->userdata('id_user');
	    $get_status_gm = $this->membermgm_model->get_status_gm($id_user);

		 if ($get_status_gm['mgm'] =='1'){

	    $this->template->load('template/template', $this->folder.'/perwakilanupgrade_gm');
		}else{
		$this->template->load('template/template', $this->folder.'/syaratketentuan');	
		}
		// $bug ="MENU INI BELUM AKTIF,KARENA MASIH TERDAPAT BEBERAPA PERMASALAHAN";
		// print_r($bug);
	}
	public function bilyet_mgm(){
		 
	    $this->template->load('template/template', $this->folder.'/bilyet_mgm');
		
	}

	public function syaratdanketentuan(){
		 
	    $this->template->load('template/template', $this->folder.'/syaratketentuan');
		
	}
	
	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->membermgm_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }	

    function searchperwakilan_lama(){
            
			 
             $search = $this->input->post('s');

             if(!empty($search)){
                 $this->membermgm_model->searchperwakilan_lama($search);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

    function search_bilyet(){
            
			 
             $search = $this->input->post('s');

             if(!empty($search)){
                 $this->membermgm_model->search_bilyet($search);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }
	
public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->membermgm_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                // $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->id_user.'</td>';
	                $rows .='<td width="10%">'.$r->nama.'</td>';
	                $rows .='<td width="10%">'.$r->sponsor.'</td>';
	             	 $rows .='<td width="15%">'.$r->telp.'</td>';
	                $rows .='<td width="20%">'.$r->alamat.'</td>';
	                $rows .='<td width="30%" align="center">';
	                if ($r->id_affiliate_type == 6){
	                		$rows .='<a title="UPGRADE GM" class="btn btn-sm btn-primary" href="'.base_url().'member_mgm/upgrade_gm/'.$r->id_mgm.'">
	                             UPGRADE GM
	                        </a> ';
	                }
	                else{ 
	                // $rows .='<a title="Mutasi" class="btn btn-sm btn-primary" href="'.base_url().'booking_seats/detail/'.$r->id_booking_seats.'">
	                //             <i class="fa fa-pencil"></i> Mutasi
	                //         </a> ';
	                }
	                if ($r->status_pelunasan == 0){
	                		$rows .='<a title="Pelunasan" class="btn btn-sm btn-primary" href="'.base_url().'member_mgm/mutasi_jamaah/'.$r->id_mgm.'">
	                             Pelunasan
	                        </a> ';
	                }
	                else{ 
	                $rows .='<a title="Mutasi" class="btn btn-sm btn-success" href="#">
	                            </i> Sudah Mutasi
	                        </a> ';
	                }
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'member_mgm/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3,
			 $config['use_page_numbers']  = TRUE,
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	

	public function report_member(){  
	    $id_mgm = $this->uri->segment(3);
	    $report_member = $this->membermgm_model->report_member($id_mgm);
	    if(!$report_member){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 'report_member'=>$report_member,

	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/report_dp',($data));
	    $this->load->view('member_mgm/report_dp',$data);
	}

	public function report_upgradeperwakilan(){  
	    $id_mgm = $this->uri->segment(3);
	    $report_member = $this->membermgm_model->report_member($id_mgm);
	    if(!$report_member){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 'report_member'=>$report_member,

	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/report_dp',($data));
	    $this->load->view('member_mgm/report_upgrade_perwakilanlama',$data);
	}


		public function report_member_split(){  
	    $id_mgm = $this->uri->segment(3);
	    $id_mgm2 = $this->uri->segment(4);
	    $report_member = $this->membermgm_model->report_member_split($id_mgm,$id_mgm2);
	    // if(!$report_member){
	    //     show_404();
	    // }
	   

	      
	    $data = array(

	       		 'report_member'=>$report_member,

	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/report_dp',($data));
	    $this->load->view('member_mgm/faktur_upgrade_perwakilan',$data);
	}

	public function report_member_oldupgrade(){  
	    $id_user = $this->uri->segment(3);
	    $report_member = $this->membermgm_model->report_member_upgradeoldperwakilan($id_user);
	    if(!$report_member){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 'report_member'=>$report_member,

	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/report_dp',($data));
	    $this->load->view('member_mgm/faktur_upgrade_perwakilan',$data);
	}
	// public function detail(){
	//     if(!$this->general->privilege_check_affiliate(MEMBER_MGM,'view'))
	// 	    $this->general->no_access();
	    
	//     $id_booking_seats = $this->uri->segment(3);
	//     $booking = $this->membermgm_model->get_booking_seats($id_booking_seats);

	//     // if(!$booking){
	//     //     show_404();
	//     // }

	//     $data = array(

	//        		 'booking'=>$booking,
	//        		 );
	
	//     $this->template->load('template/template', $this->folder.'/booking',($data));

	// }


	public function add()
	{
		 $data = array(
	    			
	    			'select_kelamin'=>$this->_select_kelamin(),
	    			'select_bank'=>$this->_select_bank(),
	    			'select_hub_ahliwaris'=>$this->_select_hub_ahliwaris(),
	    			'provinsi'=>$this->membermgm_model->get_all_provinsi(),
	    			'kabupaten'=>$this->membermgm_model->get_all_kabupaten(),
	    			
	    			// 'select_bank'=>$this->_select_bank(),
	    			// 'select_kelamin'=>$this->_select_kelamin()
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);
		
		
	}

	
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}

	private function _select_roomcategory(){
		 $id_room_category = array('2');
		$this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('category')->result();
	}

	function add_ajax_kab($id_prov){
	    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
	    $data = "<option value=''>- Select Kabupaten -</option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_keluarga($id_booking=''){
	$this->db->where_in('id_booking', $id_booking);
	    return $this->db->get('registrasi_jamaah')->result();
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}


	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}

	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	public function save_jamaahdp(){
	 

	    $data = $this->input->post(null,true);
	    $send = $this->membermgm_model->save($data);
	     if($send)
                $this->session->set_flashdata('info', "Registrasi Sukses.");
	 	       redirect('member_mgm');
		}
	
	public function upgrade_gm()
	{
		$id_mgm = $this->uri->segment(3);
	    $upgrade_gm = $this->membermgm_model->upgrade_gm($id_mgm);
	    if(!$upgrade_gm){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 'upgrade_gm'=>$upgrade_gm,

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/upgrade_gm',($data));
	   
		
	}

		public function save_upgrade_gm(){
			 $id_affiliate=$this->input->post('id_sahabat', true);
            $pin=$this->input->post('pin', true);

		$this->form_validation->set_rules('id_sahabat','id_sahabat','required|trim');
	   if($this->form_validation->run()==true){
	   	   
	   	    $id_sahabat=$this->input->post('id_sahabat');
	   		$cek=$this->membermgm_model->cek_id_sahabat($id_sahabat);

	   		$id_sahabat=$this->input->post('id_sahabat');
	   		$cek_sb_terdaftar=$this->membermgm_model->cek_id_sahabat_affiliate($id_sahabat);

	   		$ktp=$this->input->post('ktp');
	   		$cek_ktp_sahabat=$this->membermgm_model->cek_ktp_sahabat($id_sahabat);
	   		
	   		$cek_id_sahabat_perwakilan=$this->membermgm_model->cek_id_sahabat_perwakilan($id_sahabat);
	   		if($cek->num_rows()==0){
	   			$this->session->set_flashdata('info', "ID SAHABAT Tidak DI Temukan");
            	 $id_mgm = $this->input->post('id_mgm');
            	redirect('member_mgm/upgrade_gm/'.$id_mgm.'');

	   		}elseif ($cek_sb_terdaftar->num_rows()>0) {
	   			$this->session->set_flashdata('info', "ID SAHABAT Yang Anda Input Sudah TERDAFTAR");
            	 $id_mgm = $this->input->post('id_mgm');
            	redirect('member_mgm/upgrade_gm/'.$id_mgm.'');
	   		}

		   	elseif ($cek_ktp_sahabat->num_rows()==0) {
		   			$this->session->set_flashdata('info', "KTP Tidak Sama dengan data di SBL SAHABAT");
	            	 $id_mgm = $this->input->post('id_mgm');
	            	redirect('member_mgm/upgrade_gm/'.$id_mgm.'');
		   		}
	   		elseif ($cek_id_sahabat_perwakilan->num_rows()>0) {
	   			$this->session->set_flashdata('info', "ID SAHABAT ANDA SUDAH PERWAKILAN");
            	 $id_mgm = $this->input->post('id_mgm');
            	redirect('member_mgm/upgrade_gm/'.$id_mgm.'');
	   		}

	   		else{

	   	// 		if ($cek->num_rows()>0 && $cek_ktp_sahabat->num_rows()>0){ 
	   	// 		$data = $this->input->post(null,true);
			  //   $send = $this->membermgm_model->save_upgrade_gm($data);
			  //    if($send)
		   //              $this->session->set_flashdata('info', "UPGRADE GM SUKSES.");
			 	//        redirect('member_mgm');
			 	// }      
			 	// $add_perwakilan = $this->membermgm_model->add_perwakilan($id_affiliate, $pin*100000);
     //            if($add_perwakilan == '0')
     //            {
     //                $ket_add = 'Error ..!! <br> Gagal add perwakilan di sistem sahabat, cek userid yang anda inputkan';
                    
     //            } else
     //            {
     //                $arr_response = array();
     //                $arr_response = explode('|', $add_perwakilan->hasil_add);
     //                if($arr_response[0] == '00')
     //                {
     //                   $ket_add = "Sukses ..!! <br> ".$arr_response[1]."silahkan transfer ke no rek SBL untuk mendapatkan kartu Pasif Anda"; 
		   //              if ($cek->num_rows()>0 && $cek_ktp_sahabat->num_rows()>0){ 
			  //  			$data = $this->input->post(null,true);
					//     $send = $this->membermgm_model->save_upgrade_gm($data);
					//      if($send)
				 //                $this->session->set_flashdata('info', "UPGRADE GM SUKSES.");
					//  	       redirect('member_mgm');
					//  	}      
     //                } else
     //                {
     //                    $ket_add = "Error ..!! <br> ".$arr_response[1];
     //                }
	   			 if ($cek->num_rows()>0 && $cek_ktp_sahabat->num_rows()>0){ 
			   			$data = $this->input->post(null,true);
					    $send = $this->membermgm_model->save_upgrade_gm($data);
					     if($send)
				                $this->session->set_flashdata('info', "UPGRADE GM SUKSES.");
					 	       redirect('member_mgm');
					 	}      
                    
                }
                
                $this->session->set_flashdata('info', $ket_add."-");
	 	redirect('member_mgm'); 
	   			
		}
	   }
	  

	private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}

	private function _select_embarkasi(){
		// $id_embarkasi = array('1');
		// $this->db->where_in('id_embarkasi', $id_embarkasi);
	    return $this->db->get('embarkasi')->result();
	}
	
	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$category = $this->input->post('category');
		$refund = $this->input->post('refund');
		
		$select_pemberangkatan = $this->membermgm_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            

            echo $select_pemberangkatan['refund'];
           
    
        }
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->membermgm_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->membermgm_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}
	public function mutasi_jamaah(){
	    if(!$this->general->privilege_check_affiliate(MEMBER_MGM,'view'))
		    $this->general->no_access();

		

	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('member_mgm',array('id_mgm'=>$id))->row_array();
	   // if(!$get)
	   //      show_404();
	        // $grouping_kuota = $this->rewardsahabat_model->get_booking_kuota_aktif($id_booking);
	    $data = array(
	    		'select_roomcategory'=>$this->_select_roomcategory(),
				'select_embarkasi'=>$this->_select_embarkasi(),
	    		'provinsi'=>$this->membermgm_model->get_all_provinsi(),
		    	'kabupaten'=>$this->membermgm_model->get_all_kabupaten(),		
		    	'select_kelamin'=>$this->_select_kelamin(),	
		    	'select_rentangumur'=>$this->_select_rentangumur(),
		    	'select_statuskawin'=>$this->_select_statuskawin(),
		    	'select_status_hubungan'=>$this->_select_status_hubungan(),
		    	'family_relation'=>$this->_family_relation(),
		    	'select_pemberangkatan'=>$this->_select_pemberangkatan(),

		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'select_merchandise'=>$this->_select_merchandise(),
	       		 'select_status_visa'=>$this->_select_status_visa(),
	       		
	    	);
		$this->template->load('template/template', $this->folder.'/mutasi_data',array_merge($get,$data));

	}

	public function save_mutasi(){

	  	$this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
		           $data = $this->input->post(null,true);
				    $schedule = $this->input->post('radio');

				  $send = $this->membermgm_model->save_mutasi($data);
				 // if($send)
		   //              $this->session->set_flashdata('info', "MUTASI Sukses.");
			 	//        redirect('member_mgm');

            }
            else{
			// print_r('JADWAL BELUM DI PILIH');
            $id_mgm=$this->input->post('id_mgm');
			 $this->session->set_flashdata('Warning', "JADWAL BELUM DIPILIH.");
			redirect('member_mgm/mutasi_jamaah/'.$id_mgm.'');
			} 
	}

	public function report_registrasi(){  
	    $id_booking = $this->uri->segment(3);
	    $report_registrasi = $this->membermgm_model	->get_report_registrasi($id_booking);
	    if(!$report_registrasi){
	        show_404();
	    }
	   
      
	    $data = array(

	       		 'report_registrasi'=>$report_registrasi,
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_mutasi',($data));
	    // $this->load->view('bilyet_barcode',($data));
	}


	public function upgrade_perwakilan_lama()
	{
		$id_aff = $this->uri->segment(3);
	    $upgrade_gm = $this->membermgm_model->upgrade_perwakilan_lama_p($id_aff);
	    // if(!$upgrade_gm){
	    //     show_404();
	    // }
	   

	      
	    $data = array(

	       		 'upgrade_gm'=>$upgrade_gm,
	       		 	'select_kelamin'=>$this->_select_kelamin(),
	    			'select_bank'=>$this->_select_bank(),
	    			'select_hub_ahliwaris'=>$this->_select_hub_ahliwaris(),
	    			'provinsi'=>$this->membermgm_model->get_all_provinsi(),
	    			'kabupaten'=>$this->membermgm_model->get_all_kabupaten(),
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/upgrade_perwakilanlama',($data));
	   
		
	}


	public function save_upgrade_perwakilan_lama(){
	 
			   	$data = $this->input->post(null,true);
			    $send = $this->membermgm_model->save_upgrade_perwakilan($data);
			   //   if($send)
		             
			 	 // redirect('member_mgm/report_upgradeperwakilan/'.$id_mgm.'');
// 		$MyCheckBox = $this->input->post('MyCheckBox');
// 		$this->form_validation->set_rules('id_sahabat','id_sahabat','required|trim');
// 	   if($this->form_validation->run()==true){
	   	   
// 	   	    $id_sahabat=$this->input->post('id_sahabat');
// 	   		$cek=$this->membermgm_model->cek_id_sahabat($id_sahabat);

// 	   		$id_sahabat=$this->input->post('id_sahabat');
// 	   		$cek_sb_terdaftar=$this->membermgm_model->cek_id_sahabat_affiliate($id_sahabat);

// 	   		$ktp=$this->input->post('ktp');
// 	   		$cek_ktp_sahabat=$this->membermgm_model->cek_ktp_sahabat($id_sahabat);


// 	   		$cek_id_sahabat_perwakilan=$this->membermgm_model->cek_id_sahabat_perwakilan($id_sahabat);
	   		
// 	   		if((int)$MyCheckBox == 1 &&  $cek->num_rows()==0){
// 	   			$this->session->set_flashdata('info', "ID SAHABAT Tidak DI Temukan");
//             	redirect('member_mgm/upgrade_perwakilanlama');

// 	   		}elseif ((int)$MyCheckBox == 1 && $cek_sb_terdaftar->num_rows()>0) {
// 	   			$this->session->set_flashdata('info', "ID SAHABAT Yang Anda Input Sudah TERDAFTAR Konven");
//             	redirect('member_mgm/upgrade_perwakilanlama');
// 	   		}

// 		   	elseif ((int)$MyCheckBox == 1 && $cek_ktp_sahabat->num_rows()==0) {
// 		   			$this->session->set_flashdata('info', "KTP Tidak Sama dengan data di SBL SAHABAT");
// 	            	redirect('member_mgm/upgrade_perwakilanlama');
// 		   		}
// 		   	elseif ((int)$MyCheckBox == 1 && $cek_id_sahabat_perwakilan->num_rows()>0) {
// 		   			$this->session->set_flashdata('info', "ID SAHABAT YANG ANDA INPUT SUDAH TERDAFTAR SEBAGAI PERWAKILAN SAHABAT, DATA JAMAAH HARUS YANG BELUM TERDAFTAR SEBAGAI PERWAKILAN");
// 	            	redirect('member_mgm/upgrade_perwakilanlama');
// 		   		}

// 	   		else{

// 	   	// 		if ($cek->num_rows()>0 && $cek_ktp_sahabat->num_rows()>0){ 
// 	   	// 		$data = $this->input->post(null,true);
// 			  //   $send = $this->membermgm_model->save_upgrade_gm($data);
// 			  //    if($send)
// 		   //              $this->session->set_flashdata('info', "UPGRADE GM SUKSES.");
// 			 	//        redirect('member_mgm');
// 			 	// }      
// 			 	$data = $this->input->post(null,true);
// 			    $send = $this->membermgm_model->save_upgrade_perwakilan($data);

// 			    if($send)
// 			    	if((int)$MyCheckBox == 1 ){
//                 redirect('member_mgm/report_member_split/'.$id_mgm.'/'.$id_mgm2.'/');
//             }else{
//              redirect('member_mgm/report_member/'.$id_mgm.'/');
//             }

// 	 }
// }
}

	public function bilyet(){
	


	    if(!$this->general->privilege_check_affiliate(MEMBER_MGM,'cetak'))
		    $this->general->no_access();
	    $id_user = $this->uri->segment(3);
	    $id = $this->uri->segment(4);
	    $bilyet = $this->membermgm_model->get_pic_detail_bilyet($id_user);
	    // if(!$bilyet){ 
	    //     show_404();
	       
	    //    }
	   			 $data = array(
	   			 	'bilyet'=>$bilyet,
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit_bilyet',$data);

	}
	public function cetak_bilyet(){

	   if(!$this->general->privilege_check_affiliate(MEMBER_MGM,'cetak'))
		    $this->general->no_access();
	    
	    $id_user = $this->uri->segment(3);
	    $bilyet = $this->membermgm_model->get_pic_detail_bilyet($id_user);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 // 'pic'=>$pic
	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/bilyet_barcode',($data));
	    $this->load->view('cetak_bilyet',($data));
	}

	public function save_cetak_bilyet(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->membermgm_model->save_cetak_bilyet($data);
	     if($send)
	         // $id_user = $this->input->post('id_user');
          //   redirect('data_jamaah/bilyet/cetak/'.$id_user.'');
	     	
	     	$id_user = $this->input->post('id_user');
            redirect('member_mgm/cetak_bilyet/'.$id_user.'');
	}

function print_qr($id_user)
    {
        $qr_code_config = array();
        $qr_code_config['cacheable'] = $this->config->item('cacheable');
        $qr_code_config['cachedir'] = $this->config->item('cachedir');
        $qr_code_config['imagedir'] = $this->config->item('imagedir');
        $qr_code_config['errorlog'] = $this->config->item('errorlog');
        $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
        $qr_code_config['quality'] = $this->config->item('quality');
        $qr_code_config['size'] = $this->config->item('size');
        $qr_code_config['black'] = $this->config->item('black');
        $qr_code_config['white'] = $this->config->item('white');
        $this->ci_qr_code->initialize($qr_code_config);

        // get full name and user details
         $id_user = $this->uri->segment(3);
        // $id_user = $this->input->post('id_user');
	    $bilyet = $this->membermgm_model->get_pic_detail_bilyet($id_user);
        $image_name = $id_user . ".png";

        // create user content
        // $codeContents = "user_name:";
        // $codeContents .= "$user_details->user_name";
        // $codeContents .= " user_address:";
        // $codeContents .= "$user_details->user_address";
        // $codeContents .= "\n";
        // $codeContents .= "user_email :";
        $codeContents .= $bilyet['id_user'];

        $params['data'] = $codeContents;
        $params['level'] = 'H';
        $params['size'] = 3;

        $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
        $this->ci_qr_code->generate($params);

        $this->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

        // save image path in tree table
        // $this->user->change_userqr($user_id, $image_name);
        // then redirect to see image link
        $file = $params['savename'];
        if(file_exists($file)){
            header('Content-Description: File Transfer');
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            unlink($file); // deletes the temporary file

            exit;
        }
    }

    public function upgrade_program_mgm(){
	 

	    // $data = $this->input->post(null,true);
	    // $send = $this->membermgm_model->upgrade_program_mgm($data);
	    //  if($send)
	 	  //      redirect('member_mgm');

    	 $checkbox=$this->input->post('checkbox', true);

		$this->form_validation->set_rules('checkbox','checkbox','required|trim');
	   if($this->form_validation->run()==true){
	   	   
	   	    $id_user=$this->session->userdata('id_user');
	   		$cek=$this->membermgm_model->cek_id_sahabat_profil($id_user);

	   		  $sql = "SELECT COUNT(id_affiliate) as idsahabat FROM affiliate Where id_user ='$id_user'";
		 	$query = $this->db->query($sql)->result_array();
	     	 foreach($query as $key=>$value){
	         $idsahabat = $value['idsahabat'];
	        
	   }
	   		
	   		if($idsahabat==0){
	   			$this->session->set_flashdata('info', "ID SAHABAT Tidak DI Temukan / MASUKAN ID SAHABAT ATAU LENGKAPI MENU PROFIL ANDA");
            	redirect('member_mgm');


		   	}else{
		$data = $this->input->post(null,true);
	    $send = $this->membermgm_model->upgrade_program_mgm($data);
	     if($send)
	 	       redirect('member_mgm');
	   			
		}
	   }
	}
	
}
