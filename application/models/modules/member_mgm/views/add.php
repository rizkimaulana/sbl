<style type="text/css">
  
/* Credit to bootsnipp.com for the css for the color graph */
.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}
</style>


  
<div class="container">
<div class="panel panel-default">
            <div class="panel-body">
              <h3 class="page-header">Add MGM</h3>
<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <form action="<?php echo base_url(); ?>member_mgm/save_jamaahdp" role="form" method="post" >
      <h2>Please Sign Up
       <!-- <small>to become a General Marketing  </small>  -->
      </h2>
      <hr class="colorgraph">

      <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
             <?php $tiga_digit = random_3_digit_member_mgm(); ?>
                        <input type='hidden' name='unique_digit' value='<?= $tiga_digit; ?>'>
                            <?php
                                $biaya_setting_ = $this->membermgm_model->get_key_val();
                                  foreach ($biaya_setting_ as $key => $value){
                                    $out[$key] = $value;
                                  }
                              ?>

                         <input type="hidden" name="biaya_registrasi" class="form-control input-sm" id="biaya_registrasi" value="<?php echo $out['DEPOSIT_GM_1'];?>" />

                           <input type="hidden" name="harga_product" class="form-control input-sm" id="harga_product" value="<?php echo $out['HARGA_PRODUCT_DP5JT'];?>" />
        
       <div class="form-group">
            <input type="text" name="nama" id="nama" class="form-control input-lg" placeholder="Nama" tabindex="1" required>
          </div>

       <div class="form-group">
        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="EMAIL" tabindex="2" required>
      </div>

      <div class="form-group">
        <input type="text" name="alamat" id="alamat" class="form-control input-lg" placeholder="Alamat Rumah" tabindex="3" required>
      </div>
      

          <div class="form-group">
            <input type="text" name="telp" id="telp" class="form-control input-lg" onKeyPress="return numbersonly(this, event)" placeholder="Input No. Telp (harus karakter angka)" tabindex="4">
          </div>
        
         
          <div class="form-group">
            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control input-lg" placeholder="Tempat Lahir" tabindex="5">
          </div>
     
          
          <div class="form-group">
            <input type="text" name="tanggal_lahir" id="datepicker" class="form-control input-lg" placeholder="Tanggal Bulan Tahun Kelahiran" tabindex="6">
          </div>
        
         <div class="form-group">
            <input type="text" name="ktp" id="ktp" class="form-control input-lg" placeholder="No Identitas / KTP" tabindex="7">
          </div>

         <div class="form-group">

         <select class="form-control input-lg"  name="select_kelamin" tabindex="8" >
             <option value=''>- PILIH JENIS KELAMIN -</option>
                <?php foreach($select_kelamin as $sk){?>
                    <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
                <?php } ?>
          </select>
      </div>

      <div class="form-group">
         <select class="form-control input-lg"  name="provinsi_id" id="provinsi" tabindex="9" >
            <option value=''>- PILIH Provinsi -</option>
                                  <?php foreach($provinsi as $Country){
                                      echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                  } ?>
          </select>
      </div>
      <div class="form-group">
        <select required name="kabupaten_id" class="form-control input-lg" id="kabupaten" tabindex="10">
            <option value=''>- PILIH Kabupaten -</option>
             </select>
      </div>
      <div class="form-group">
         <select class="form-control input-lg"  name="kecamatan_id" id="kecamatan" tabindex="11">
            <option value=''>- PILIH Kabupaten -</option>
            
          </select>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <input type="text" class="form-control input-lg" name="waris" placeholder="Input Ahli waris"  tabindex="12"/>
           
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
             <select required class="form-control input-lg" name="select_hub_ahliwaris" tabindex="13">
                <option  value="">Hubungan Ahli Waris</option>
                  <?php foreach($select_hub_ahliwaris as $sk){?>
                      <option value="<?php echo $sk->id_hubwaris;?>"><?php echo $sk->hubungan;?></option>
                  <?php } ?>
              </select>
          </div>
        </div>
      </div>
      
     
  
       
      <hr class="colorgraph">
      <div class="row">
        <div class="col-xs-12 col-md-12"><input type="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="14"></div>
        <!-- <div class="col-xs-12 col-md-6"><a href="#" class="btn btn-success btn-block btn-lg">Sign In</a></div> -->
        <!-- <button type="submit" class="col-xs-12 col-md-12"><i class="fa fa-save"></i> Aktivasi</button> -->
      </div>
    </form>
  </div>
</div>


</div>
</div>
</div>


    <script type="text/javascript">

$(function(){
    
    $('#thedate').datepicker({
        dateFormat: 'YYYY-MM-DD',
        altField: '#thealtdate',
        altFormat: 'YYYY-MM-DD'
    });
    
});
$("#datepicker").datepicker({
  dateFormat: 'yy-mm-dd',

    changeMonth: true,
    changeYear: true,
    
});
    </script>

    <script type="text/javascript">
       $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('member_mgm/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('member_mgm/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('member_mgm/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })

             jQuery(document).ready(function(){
              $(".chosen-select").chosen({width: "95%"}); 
            });
        });

    </script>
   

   <script type="text/javascript">
function numbersonly(myfield, e, dec) { var key; var keychar; if (window.event) key = window.event.keyCode; else if (e) key = e.which; else return true; keychar = String.fromCharCode(key); // control keys 
if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) return true; // numbers 
else if ((("0123456789").indexOf(keychar) > -1)) return true; // decimal point jump 
else if (dec && (keychar == ".")) { myfield.form.elements[dec].focus(); return false; } else return false; }

</script>
  