<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktivasi_reschedule extends CI_Controller{
	var $folder = "aktivasi_reschedule";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(AKTIVASI_RESCHEDULE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','aktivasi');	
		$this->load->model('aktivasireschedule_model');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	     $data = array(
			'pic' => $this->aktivasireschedule_model->get_reschedule('order by create_date desc')->result_array(),
		);
	   $this->template->load('template/template', $this->folder.'/aktivasi_reschedule',($data));

	
	
		
	}

	
	public function aktivasi($id_booking){
	


	    if(!$this->general->privilege_check(AKTIVASI_RESCHEDULE,'view'))
		    $this->general->no_access();
	    
	    $id_jamaah = $this->uri->segment(3);
	    // $id_booking = $this->uri->segment(4);
	    // $id_booking= $this->input->post('id_booking');
	    $aktivasi = $this->aktivasireschedule_model->get_pic_aktivasi($id_jamaah);
	    if(!$aktivasi){
	        show_404();
	    }
	   
	    $data = array(
	    		
	    		'select_bank'=>$this->_select_bank(),
	       		 'aktivasi'=>$aktivasi

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/aktivasi',($data));

	}



	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	
	 

	public function update(){
	     

		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/bukti_pembayaran_reschedule/',
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }
		    else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		    }
	    }
	     
		  //print_r($data);exit;
		    $send = $this->aktivasireschedule_model->update($data);
		    if($send)
	                $this->session->set_flashdata('info', "Aktivasi Sukses.");
		 	       redirect('aktivasi_reschedule');
		
	}





}
