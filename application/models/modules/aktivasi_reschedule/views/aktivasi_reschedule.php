
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>AKTIVASI RESCHEDULE</b>
                </div>
                <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID JAMAAH</th>
                                    <th>NAMA </th>
                                    <th>TIPE JAMAAH </th>
                                    <th>TGL LAMA </th>
                                    <th>TGL BARU </th>
                                    <th>BIAYA</th>
                                    <th>STATUS PERUBAHAAN</th>
                                    <th>CREATE BY </th>
                                   <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $pi['id_jamaah']?> </td>
                                      <td><?php echo $pi['nama']?>  </td>
                                      
                                      <td><?php echo $pi['type_jamaah']?>  </td>
                                      <td><?php echo $pi['waktu_sebelum']?>  </td>
                                      <td><?php echo $pi['waktu_ubah']?>  </td>
                                      <td><?php echo $pi['biaya']?>  </td>
                                      <td><?php echo $pi['ket_reschedule']?>  </td>
                                      <td><?php echo $pi['user']?>  </td>
                                       <td>
                                        <div class="btn-group">
                                            <a class="btn-sm btn-primary" title='Edit' href="<?php echo base_url();?>aktivasi_reschedule/aktivasi/<?php echo $pi['id_jamaah']; ?>">Aktivasi</a>
                                   
                                         </div> 
                                                                                         
                                  </td>
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
