 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});


</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_reschedule/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        


      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
                	     <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                 	 <input type="hidden" name="id_booking" value="<?php echo $aktivasi['booking']?>"  class="form-control" readonly="true"> 
                   <input type="hidden" name="id_registrasi" value="<?php echo $aktivasi['id_registrasi']?>"  class="form-control" readonly="true"> 
                    <input type="hidden" name="id_reschedule" value="<?php echo $aktivasi['reschedule']?>"  class="form-control" readonly="true"> 
                     <input type="hidden" name="id_product" value="<?php echo $aktivasi['product']?>"  class="form-control" readonly="true"> 
                    <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama  </label>
                               <div class="col-sm-5">
                                    <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $aktivasi['nama']?>"  readonly='true' required/>
                                </div>
                    </div>
                 	<div class="form-group">
	                    <label class="col-lg-2 control-label">ID JAMAAH</label>
	                    <div class="col-lg-5">
	                       <input type="text" name="id_jamaah" value="<?php echo $aktivasi['id_jamaah']?>"  class="form-control" readonly="true">
	                    </div>
	                  </div>

                     <div class="form-group">
	                    <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="ket_pembayaran" required></textarea>
	                    </div>
	                  </div>

		            <div class="form-group">
				        <label class="col-lg-2 control-label">Bukti Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="file" name="pic[]"  class="form-control" required>
				        </div>
				    </div>
         
           		<div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_bank">
                      <option ="">- PILIH BANK TRANSFER -</option>
                    <?php foreach($select_bank as $sk){?>
                            <option value="<?php echo $sk->id;?>"><?php echo $sk->nama;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                  </div>

				    <div class="form-group">
				        <label class="col-lg-2 control-label">Nominal Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="text" name="biaya" value="<?php echo $aktivasi['biaya']?>"  class="form-control" readonly="true">
				        </div>
				    </div>

           			 <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div>

	                 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aktivasi</button>
           			 <a href="<?php echo base_url();?>aktivasi_reschedule" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   
      
  </form>       
                
</div>
   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>
