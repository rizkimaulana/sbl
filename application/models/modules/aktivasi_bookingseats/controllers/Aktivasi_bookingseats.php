<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktivasi_bookingseats extends CI_Controller{
	var $folder = "aktivasi_bookingseats";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(AKTIVASI_JAMAAH_KUOTA,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('aktivasibookingseats_model');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	     $data = array(
			'pic' => $this->aktivasibookingseats_model->get_booking_seats('order by tgl_pembelian desc')->result_array(),
		);
	   $this->template->load('template/template', $this->folder.'/aktivasi_bookingseats',($data));
		
	}

	private function _select_pay_method(){
		
		return $this->db->get('pay_method')->result();
	}
	
	public function aktivasi($id_booking_seats){
	


	    if(!$this->general->privilege_check(AKTIVASI_JAMAAH_KUOTA,'edit'))
		    $this->general->no_access();
	    
	    $id_booking_seats = $this->uri->segment(3);
	    $aktivasi = $this->aktivasibookingseats_model->get_pic_aktivasi($id_booking_seats);
	    if(!$aktivasi){
	        show_404();
	    }
	   
	    $data = array(
	    		
	    		'select_bank'=>$this->_select_bank(),
	    		'select_pay_method'=>$this->_select_pay_method(),
	       		 'aktivasi'=>$aktivasi

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/aktivasi',($data));

	}



	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	
	 

	public function save(){
	     

		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/bukti_pembayaran_jamaahkuota/',
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size'      => '5000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }
		    else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		    }
	    }
	     
		  //print_r($data);exit;
		    $send = $this->aktivasibookingseats_model->save($data);
	                $this->session->set_flashdata('info', "Aktivasi Sukses.");
		 	       redirect('aktivasi_bookingseats');
		
	}





}
