<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jamaah_aktif extends CI_Controller{
	var $folder = "data_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','jamaah_aktif');	
		$this->load->model('jamaahaktif_model');
		$this->load->model('jamaahaktif_model','r');
		//$this->load->helper('fungsi');
		$this->load->library('terbilang');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/jamaah_aktif');
		
	}

  public function data_jamaahaktif(){
	
		
	   $this->template->load('template/template', $this->folder.'/data_jamaah_aktif');
		
	}

public function faktur(){
	
		
	   $this->template->load('template/template', $this->folder.'/cetak_reschedulereguler');
		
	}




	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	                $row[] ='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 ID SAHABAT :  '.$r->id_sahabat.'<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $row[] ='<td width="18%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $row[] ='<td width="18%" >Tgl Daftar : '.$r->tgl_daftar.'<br>
	             			 Tgl Aktivasi : '.$r->update_date.'<br>
	             			
	               			 </td>';
	                			
	                // $row[] ='<td width="18%">Admin SBL :'.$r->tgl_aktivasi.' <br>
	                	
	             			

	                // 		</td>';
	              
	                
	                $row[] ='<td width="18%"><a title="Edit" class=" btn-sm btn-primary" href="'.base_url().'data_jamaah/jamaah_aktif/edit_jamaah_aktif/'.$r->id_jamaah.'">
	                            Edit
	                        </a> <br><br>
	                <a title="Detail" class=" btn-sm btn-success" href="'.base_url().'data_jamaah/jamaah_aktif/detail_data_jamaah_aktif/'.$r->id_booking.'/'.$r->id_jamaah.'">
	                            Detail
	                        </a> <br><br>';

	              if ($r->tc_bilyet >'0'){
	               $row[] = '<a class=" btn-sm btn-success"  title="cetak" target="_blank" href="'.base_url().'data_jamaah/jamaah_aktif/bilyet/'.$r->id_jamaah.'">
	                		 Bilyet</a>
	                		 </td>';
	                	}else{
	                $row[] ='<a class=" btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'data_jamaah/jamaah_aktif/bilyet/'.$r->id_jamaah.'">
	                		 Bilyet</a>
	                		 </td>';
	                	}

	                if ($r->id_product != '6' && $r->kd_tipe == '1'){

	               $row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="'.base_url().'data_jamaah/jamaah_aktif/reschedule/'.$r->id_booking.'/'.$r->id_jamaah.'/'.$r->id_affiliate.'">
	                		 Reschedule</a>';
	                }elseif($r->id_product != '6' && $r->kd_tipe == '2' ){
	                	$row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="'.base_url().'data_jamaah/jamaah_aktif/reschedule_reward_sahabat/'.$r->id_booking.'/'.$r->id_jamaah.'/'.$r->id_affiliate.'">
	                		 Reschedule</a>';
	                } elseif($r->id_product != '6' && $r->kd_tipe == '3' ){
	                	$row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="'.base_url().'data_jamaah/jamaah_aktif/reschedule_jamaah_voucher/'.$r->id_booking.'/'.$r->id_jamaah.'/'.$r->id_affiliate.'">
	                		 Reschedule</a>';
	                }
	                elseif($r->id_product != '6' && $r->kd_tipe == '5'){
	                	$row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="'.base_url().'data_jamaah/jamaah_aktif/reschedule_reward_prestasi/'.$r->id_booking.'/'.$r->id_jamaah.'/'.$r->id_affiliate.'">
	                		 Reschedule</a>';

	               	}elseif($r->id_product == '6'){
	                	$row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="'.base_url().'data_jamaah/jamaah_aktif/reschedule_jamaah_paketpromo/'.$r->id_booking.'/'.$r->id_jamaah.'/'.$r->id_affiliate.'">
	                		 Reschedule P Promo</a>';
	                }elseif($r->id_product == '6'&& $r->kd_tipe == '6'){
	                	$row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="'.base_url().'data_jamaah/jamaah_aktif/reschedule_jamaah_paketpromo/'.$r->id_booking.'/'.$r->id_jamaah.'/'.$r->id_affiliate.'">
	                		 Reschedule P Promo</a>';
	                
	                }else{
	                	$row[] ='<a class=" btn-sm btn-info"  title="Reschedule" href="#">
	                		 Reschedule</a>';
	                }

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	  



	 public function detail(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	    $booking = $this->jamaahaktif_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{

	        $pic = $this->jamaahaktif_model->get_pic($id_booking);
	    }    

	      
	    $data = array(

	       		 'booking'=>$booking,'pic'=>$pic
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahaktif',($data));

	}
	

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	private function _select_status_fee(){
	    
	    return $this->db->get('status_claim_fee')->result();
	}
	private function _select_affiliate_type(){
	
	    return $this->db->get('affiliate_type')->result();
	}

    private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}
	private function _select_keluarga($date_schedule=''){

		$this->db->where_in('date_schedule', $date_schedule);
	    return $this->db->get('detail_jamaahaktif')->result();
	}
	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}
	
	private function _select_roomcategory_reward_sahabat(){
		$id_room_category = array('1');
		$this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

		private function _select_roomcategory_reward_prestasi(){
		$id_room_category = array('1','2');
		$this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		$this->db->order_by("nama", "asc");
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	private function _select_tipe_jamaah(){
		
		return $this->db->get('tipe_jamaah')->result();
	}
	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_ket_fee(){
		// $kd = array('5','2','1','0');
		// $this->db->where_in('id_aktivasi_manual', $kd);
		return $this->db->get('status_aktivasi_manual')->result();
	}

	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->order_by("nama", "asc")->get_where('affiliate',array('id_affiliate_type'=>$id_affiliate_type));
		    $this->db->order_by("nama", "asc");
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama."</option>";
		    }
		    echo $data;
	}

	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
	

	private function _select_status_pic(){
		return $this->db->get('status_pic')->result();
	    
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}

	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->jamaahaktif_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->jamaahaktif_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->jamaahaktif_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }


	}

	function edit_jamaah_aktif(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'edit'))
		    $this->general->no_access();
		$id = $this->uri->segment(4);
		// $get = $this->db->get_where('detail_jamaahaktif',array('id_jamaah'=>$id))->row_array();
	  $stored_procedure = "call detail_jamaah_aktif(?)";
          $get = $this->db->query($stored_procedure,array('id_registrasi'=>$id))->row_array(); 
	     
	    
	     if(!$get)
	        show_404();
	  
	    $data = array(
	    		'select_tipe_jamaah'=>$this->_select_tipe_jamaah(),
	        	// 'family_relation'=>$this->_family_relation(),
				'select_product'=>$this->_select_product(),
				'select_merchandise'=>$this->_select_merchandise(),
				'select_rentangumur'=>$this->_select_rentangumur(),
				// 'select_status_hubungan'=>$this->_select_status_hubungan(),
				'select_kelamin'=>$this->_select_kelamin(),
				'select_statuskawin'=>$this->_select_statuskawin(),
		    	'provinsi'=>$this->jamaahaktif_model->get_all_provinsi(),
		    	'kabupaten'=>$this->jamaahaktif_model->get_all_kabupaten(),
		    	'select_embarkasi'=>$this->_select_embarkasi(),
		    	'select_roomcategory'=>$this->_select_roomcategory(),
		    	'id_bandara' => $this->jamaahaktif_model->get_data_bandara(),
		    	// 'select_keluarga'=>$this->_select_keluarga($date_schedule),
		    	// 'select_affiliate'=>$this->_select_affiliate(),
	  			 // 'select_pemberangkatan'=>$this->_select_pemberangkatan(),
	  			 'select_type'=>$this->_select_type(),
	  			 'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
	       		'select_status_identitas'=>$this->_select_status_pic(),
		    	'select_status_kk'=>$this->_select_status_pic(),
		    	'select_photo'=>$this->_select_status_pic(),
		    	'select_pasport'=>$this->_select_status_pic(),
		    	'select_vaksin'=>$this->_select_status_pic(),
		    	'select_ket_fee'=>$this->_select_ket_fee(),
		    	'select_product'=>$this->_select_product()

	       		 );
	  $this->template->load('template/template', $this->folder.'/edit_jamaah_aktif',array_merge($get,$data));
	   

	}

	public function update(){
	    
	    $data = $this->input->post(null,true);
	    				
		

	  
	   $send = $this->jamaahaktif_model->update($data);
	   if($send)
	     // $id_booking = $this->input->post('id_booking');
            	redirect('data_jamaah/jamaah_aktif/data_jamaahaktif');

	// print_r($_POST);

	}

	

	 public function detail_data_jamaah_aktif(){
	


	    // if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		   //  $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	     $id_jamaah = $this->uri->segment(5);
	     $id_affiliate = $this->uri->segment(6);
	   $pic = $this->jamaahaktif_model->get_pic_aktivasi_invoice($id_booking);
	   $pic_by_jamaah = $this->jamaahaktif_model->get_pic_aktivasi_by_jamaah($id_jamaah);
	    // $pic    = array();
	    // $pic_booking= array();
	    // if(!$pic){
	    //     show_404();
	    // }
	    // else{
	        
	    //     // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	    //     // $pic = $this->jamaahaktif_model->get_pic_aktivasi($id_booking);
	    // }    
	     $detail = $this->jamaahaktif_model->get_pic_detail_jamaah($id_jamaah);
	     $get_pic_jamaah = $this->jamaahaktif_model->get_pic_jamaah($id_jamaah);
	    $data = array(

	       		 // 'pic_booking'=>$pic_booking,
	       		 // 'booking'=>$booking,
	       		 'pic'=>$pic,
	       		 'detail'=>$detail,
	       		 'get_pic_jamaah'=>$get_pic_jamaah,
	       		 'pic_by_jamaah'=>$pic_by_jamaah
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/viewdetail_jamaahaktif',($data));

	}



	private function _select_user(){
		
		return $this->db->get('user')->result();
	}

	 public function bilyet(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'add'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(4);
	    $get = $this->db->get_where('data_jamaah_aktif',array('id_jamaah'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   			 $data = array('select_user'=>$this->_select_user(),
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/cetak_bilyet',array_merge($get,$data));

	}


	public function save_cetak_bilyet(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->jamaahaktif_model->save_cetak_bilyet($data);
	     if($send)
	         // $id_jamaah = $this->input->post('id_jamaah');
          //   redirect('data_jamaah/bilyet/cetak/'.$id_jamaah.'');
	     	
	     	$id_jamaah = $this->input->post('id_jamaah');
            redirect('bilyet/bilyet_barcode/detail_bilyet_barcode/'.$id_jamaah.'');
	}

	public function ca_barcode($primary_key , $row)
	{
	    return site_url('barcode/get_barcode').'/'.$row->id_jamaah;
	}

	public function get_barcode($code)
    {
        $this->set_barcode($code);
    }

    private function set_barcode($code)
    {
        $this->load->library('Zend');
        $this->zend->load('Zend/Barcode');
        //generate barcode
        Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
    }

    	 function gambar($kode)
	{
				$height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '50';	
				$width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst
				$this->load->library('zend');
		        $this->zend->load('Zend/Barcode');
		 		$barcodeOPT = array(
				    'text' => $kode, 
				    'barHeight'=> $height, 
				    'factor'=>$width,
				);
						
					$renderOPT = array();
			$render = Zend_Barcode::factory(
		'code128', 'image', $barcodeOPT, $renderOPT
		)->render();
	}

	

function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->jamaahaktif_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="12"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

  
	

	public function reschedule(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	     $id_jamaah = $this->uri->segment(5);
	    $booking = $this->jamaahaktif_model->get_pic_booking_schedule($id_booking);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }

	     $detail = $this->jamaahaktif_model->get_pic_detail_jamaah($id_jamaah);
	      
	    $data = array(

	       		 'select_product'=>$this->_select_product(),
				 'select_roomcategory'=>$this->_select_roomcategory(),
				 'select_embarkasi'=>$this->_select_embarkasi(),
	       		 'booking'=>$booking,
	       		 'detail'=>$detail

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/reschedule',($data));

	}

	public function update_schedule(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    // $inputan = '';
	    // foreach($schedule as $value){
	    // 	$inputan .= ($inputan!=='') ? ',' : '';
	    // 	$inputan .= $value;
	    // }
	   
	  $send = $this->jamaahaktif_model->update_schedule($data);
	  // if($send)
	  	
			// $this->session->set_flashdata('success', "Successfull");
			//  $id_booking = $this->input->post('id_booking');
			//  $id_jamaah = $this->input->post('id_jamaah');
	  //       redirect('data_jamaah/jamaah_aktif/report_reschedule/'.$id_jamaah.'');
	         
		

	}else{
		
	
	    redirect('data_jamaah/jamaah_aktif/data_jamaahaktif');
	}
	}

	public function report_reschedule(){  
	    $id_reschedule = $this->uri->segment(4);
	    $reschedule_reguler = $this->jamaahaktif_model->get_report_reschedule_reguler($id_reschedule);
	    if(!$reschedule_reguler){
	        show_404();
	    }
	   
      
	    $data = array(

	       		 'reschedule_reguler'=>$reschedule_reguler,
	       		 );
	
	    //$this->template->load('template/template', $this->folder.'/cetak_reschedulereguler',($data));
	     $this->load->view('data_jamaah/cetak_reschedulereguler',($data));
	}


		public function reschedule_reward_sahabat(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	     $id_jamaah = $this->uri->segment(5);
	    $booking = $this->jamaahaktif_model->get_pic_booking_schedule($id_booking);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }

	     $detail = $this->jamaahaktif_model->get_pic_detail_jamaah($id_jamaah);
	      
	    $data = array(

	       		 'select_product'=>$this->_select_product(),
				 'select_roomcategory'=>$this->_select_roomcategory_reward_sahabat(),
				 'select_embarkasi'=>$this->_select_embarkasi(),
	       		 'booking'=>$booking,
	       		 'detail'=>$detail

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/reschedule_reward_sahabat',($data));

	}

	public function update_schedule_reward_sahabat(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    // $inputan = '';
	    // foreach($schedule as $value){
	    // 	$inputan .= ($inputan!=='') ? ',' : '';
	    // 	$inputan .= $value;
	    // }
	   
	  $send = $this->jamaahaktif_model->update_schedule_reward_sahabat($data);
	  // if($send)
	  	
			// $this->session->set_flashdata('success', "Successfull");
			//  $id_booking = $this->input->post('id_booking');
			//  $id_jamaah = $this->input->post('id_jamaah');
	  //       redirect('data_jamaah/jamaah_aktif/report_reschedule/'.$id_jamaah.'');
	         
		

	}else{
		
	
	    redirect('data_jamaah/jamaah_aktif/data_jamaahaktif');
	}
	}


	public function update_schedule_paketpromo(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    
	   
	  $send = $this->jamaahaktif_model->update_schedule_paketpromo($data);
	  

	}else{
		
	
	    redirect('data_jamaah/jamaah_aktif/data_jamaahaktif');
	}
	}

	public function reschedule_reward_prestasi(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	     $id_jamaah = $this->uri->segment(5);
	    $booking = $this->jamaahaktif_model->get_pic_booking_schedule($id_booking);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }

	     $detail = $this->jamaahaktif_model->get_pic_detail_jamaah($id_jamaah);
	      
	    $data = array(

	       		 'select_product'=>$this->_select_product(),
				 'select_roomcategory'=>$this->_select_roomcategory_reward_prestasi(),
				 'select_embarkasi'=>$this->_select_embarkasi(),
	       		 'booking'=>$booking,
	       		 'detail'=>$detail

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/reschedule_reward_prestasi',($data));

	}
	public function update_schedule_reward_prestasi(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    // $inputan = '';
	    // foreach($schedule as $value){
	    // 	$inputan .= ($inputan!=='') ? ',' : '';
	    // 	$inputan .= $value;
	    // }
	   
	  $send = $this->jamaahaktif_model->update_schedule_reward_prestasi($data);
	  // if($send)
	  	
			// $this->session->set_flashdata('success', "Successfull");
			//  $id_booking = $this->input->post('id_booking');
			//  $id_jamaah = $this->input->post('id_jamaah');
	  //       redirect('data_jamaah/jamaah_aktif/report_reschedule/'.$id_jamaah.'');
	         
		

	}else{
		
	
	    redirect('data_jamaah/jamaah_aktif/data_jamaahaktif');
	}
	}



	public function reschedule_jamaah_voucher(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	     $id_jamaah = $this->uri->segment(5);
	    $booking = $this->jamaahaktif_model->get_pic_booking_schedule($id_booking);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }

	     $detail = $this->jamaahaktif_model->get_pic_detail_jamaah($id_jamaah);
	      
	    $data = array(

	       		 'select_product'=>$this->_select_product(),
				 'select_roomcategory'=>$this->_select_roomcategory(),
				 'select_embarkasi'=>$this->_select_embarkasi(),
	       		 'booking'=>$booking,
	       		 'detail'=>$detail

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/reschedule_jamaah_voucher',($data));

	}

	public function update_schedule_jamaah_voucher(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    // $inputan = '';
	    // foreach($schedule as $value){
	    // 	$inputan .= ($inputan!=='') ? ',' : '';
	    // 	$inputan .= $value;
	    // }
	   
	  $send = $this->jamaahaktif_model->update_schedule_voucher($data);
	  

	}else{
		
	
	    redirect('data_jamaah/jamaah_aktif/data_jamaahaktif');
	}
	}

public function reschedule_jamaah_paketpromo(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	     $id_jamaah = $this->uri->segment(5);
	    $booking = $this->jamaahaktif_model->get_pic_booking_schedule($id_booking);
	    // $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }

	     $detail = $this->jamaahaktif_model->get_pic_detail_jamaah($id_jamaah);
	      
	    $data = array(

	       		 'select_product'=>$this->_select_product(),
				 'select_roomcategory'=>$this->_select_roomcategory(),
				 'select_embarkasi'=>$this->_select_embarkasi(),
	       		 'booking'=>$booking,
	       		 'detail'=>$detail

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/reschedule_paketpromo',($data));

	}

	function searchItem_paket_promo(){
            
			 
             
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($departure)){
                 $this->jamaahaktif_model->searchItempaketpromo($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }
}
