<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_jamaah extends CI_Controller{
	var $folder = "data_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','data_jamaah');	
		$this->load->model('datajamaah_model');
				$this->load->model('datajamaah_model','r');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/data_jamaah');
		
	}

	public function data_jamaahbelumaktif(){
	
		
	   $this->template->load('template/template', $this->folder.'/data_jamaah_belumaktif');
		
	}



	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	               	$row[] ='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $row[] ='<td width="20%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $row[] ='<td width="20%" >Tgl Daftar : '.$r->tgl_daftar.'
	             			
	               			 </td>';
	                			
	                $row[] ='<td width="20%">'.$r->create_by.'<br>
	             			

	                		</td>';
	                // $row[] ='<td width="20%">'.$r->status_jamaah.'</td>';
	              
	              

			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	 public function get_jamaah_belum_aktif(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->datajamaah_model->get_jamaah_belum_aktif($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="3%" >'.$i.'</td>';
	                $rows .='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $rows .='<td width="20%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $rows .='<td width="20%" >Tgl Daftar : '.$r->tgl_daftar.'
	             			
	               			 </td>';
	                			
	                $rows .='<td width="20%">'.$r->create_by.'<br>
	             			 User ID : '.$r->id_user.'<br>

	                		</td>';
	                $rows .='<td width="20%">'.$r->status_jamaah.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                // $rows .='<a title="Edit" class=" btn-sm btn-primary" href="'.base_url().'data_jamaah/jamaah_aktif/edit_jamaah_aktif/'.$r->id_jamaah.'">
	                //             <i class="fa fa-pencil"></i> Edit
	                //         </a> <br><br>';
	                // $rows .='<a title="Detail" class=" btn-sm btn-success" href="'.base_url().'data_jamaah/jamaah_aktif/detail_data_jamaah_aktif/'.$r->id_booking.'">
	                //             <i class="fa fa-pencil"></i> Detail
	                //         </a> <br><br>';
	                // $rows .='<a class=" btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'data_jamaah/bilyet/cetak/'.$r->id_jamaah.'">
	                // 		<i class="glyphicon glyphicon-print"></i> Bilyet</a>';

	            
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging_jamaah_belum_aktif($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="10">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging_jamaah_belum_aktif($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'data_jamaah/get_jamaah_belum_aktif/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	
	 public function detail(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->datajamaah_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	        $pic = $this->datajamaah_model->get_pic($id_booking);
	        // $row_datajamaah = $this->datajamaah_model->get_pic_booking ($id_booking);
		
	    }    

	    $data = array(
	    		
	        	// 'row_datajamaah' => $this->datajamaah_model->get_pic_booking ($id_booking),
	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaah',($data));

	}
	






	



}
