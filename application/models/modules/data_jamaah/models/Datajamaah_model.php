<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datajamaah_model extends CI_Model{
   
      var $table = 'jamaah_belum_aktif';
    var $column_order = array(null, 'id_registrasi','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','telp_2','no_identitas','tempat_lahir','umur','tanggal_lahir','email','id_kelamin','id_rentang_umur','id_status_diri','id_ket_keberangkatan','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','create_by','tipe_jamaah','alamat','kelamin','status_jamaah','status','harga_paket','muhrim','refund','handling'); //set column field database for datatable orderable
    var $column_search = array('id_registrasi','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','telp_2','no_identitas','tempat_lahir','umur','tanggal_lahir','email','id_kelamin','id_rentang_umur','id_status_diri','id_ket_keberangkatan','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','create_by','tipe_jamaah','alamat','kelamin','status_jamaah','status','harga_paket','muhrim','refund','handling'); //set column field database for datatable searchable 
    var $order = array('id_registrasi' => 'asc'); // default order 

   
   
    private $_table="booking";
    private $_primary="id_booking";

     private $_registrasi_jamaah="registrasi_jamaah";
     private $_id_registrasi="id_registrasi";




 public function get_pic($id_booking){
    
        $sql ="SELECT * from detail_jamaahnoaktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

public function get_pic_belumaktif($where= "") {
        $data = $this->db->query('select * from detail_jamaahnoaktif '.$where);
        return $data;
    }





    public function get_jamaah_belum_aktif($offset,$limit,$q=''){
    
        $id=  $this->session->userdata('id_user');
          $sql = "SELECT * FROM data_jamaah_belum_aktif
                   where (1=1) ";
        
        if($q){
            
            $sql .=" AND invoice LIKE '%{$q}%'
                    OR id_jamaah LIKE '%{$q}%'
                     OR nama LIKE '%{$q}%'
                     OR id_affiliate LIKE '%{$q}%'
                     OR telp LIKE '%{$q}%'   
                     OR tgl_daftar LIKE '%{$q}%' 
                     OR paket LIKE '%{$q}%'
                     OR bulan_menunggu LIKE '%{$q}%'   
                     OR create_by LIKE '%{$q}%' 
                ";
        }
        $sql .=" ORDER BY tgl_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }




   

    function get_data_jamaahnoaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        } else {
            return FALSE;
        }
    }
    
private function _get_datatables_query()
    {
        
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
