
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Aktif</b>
                </div>

                 <div class="panel-body">
                     <form role="form">
                           
                            <!--  <div class="form-group input-group col-lg-4">
                               
                                <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div> -->
                        </form>  
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <!-- <th>BARCODE</th> -->
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TANGGAL DFTR/AKT </th>
                                    <!-- <th>USER </th> -->
                                   <th>Action </th>
                                    <th>Bilyet </th>
                                   <th>Rechedule </th>
                                </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                     </table>
                   </div>
                  
                     <!--    <div class="pull-right">
                          <ul class="pagination"></ul>    
                       </div>   -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables
    table = $('#data-table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_jamaah/jamaah_aktif/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

});

</script>
