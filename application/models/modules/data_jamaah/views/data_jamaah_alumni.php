
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Alumni</b>
                </div>

                 <div class="panel-body">
                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Invoice</th>
                                    <th>ID Jamaah </th>
                                    <th>Nama </th>
                                    <th>No Identitas </th>
                                   <th>Telp </th>
                                   <th>Paket </th>
                                   <th>Status </th>
                                   <th>user </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $pi['invoice']?>  </td>
                                      <td><?php echo $pi['id_jamaah']?>  </td>
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['no_identitas']?>  </td>
                                      <td><?php echo $pi['telp']?>  </td>
                                      <td><?php echo $pi['paket']?>  </td>
                                      <td><?php echo $pi['status']?>  </td>
                                       <td><?php echo $pi['create_by']?>  </td>
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
