
 <div id="page-wrapper">
    
    
       <div class="row">
        <!-- <div class="col-lg-7"> -->
          <div class="panel panel-default">
            <div class="panel-body">
        <h3 class="page-header">Periksa Data Jamaah</h3>

       
        <form action="<?php echo base_url();?>data_jamaah/jamaah_aktif/update" class="form-horizontal" role="form" method="post">
                
                <div class="form-group">
                    <label  class="col-sm-2 control-label" >ID Jamaah</label>
                    <div class="col-sm-10">
                       <input name="id_registrasi" type="hidden" value="<?php echo $id_registrasi;?>">
                       <input name="id_booking" id="id_booking" type="hidden" value="<?php echo $id_booking;?>">
                       <input name="status" id="status" type="hidden" value="<?php echo $status;?>">
                        <input name="id_jamaah" type="text" class="form-control" id="nama" value="<?php echo $id_jamaah;?>" readonly="treu"/>
                      <input name="date_schedule" id="status" type="hidden" value="<?php echo $date_schedule;?>">
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Tipe Jamaah</label>
                    <div class="col-sm-10">
                        <select required class="form-control"  name="select_tipe_jamaah">
                        <!-- <option value="">- Pilih Tipe Jamaah -</option> -->
                       <?php foreach($select_tipe_jamaah as $st){ 
                            $selected = ($tipe_jamaah == $st->id_tipe_jamaah)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_tipe_jamaah;?>" <?php echo $selected;?>><?php echo $st->nama;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                        <label class="col-sm-2 control-label">Room Category</label>
                        <div class="col-sm-10">
                       <select required class="form-control" name="category">
                         <option  value="">- PILIH Category -</option>  
                            <?php foreach($select_roomcategory as $st){ 
                              $selected = ($category == $st->id_room_category)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_room_category;?>" <?php echo $selected;?>><?php echo $st->category;?></option>
                          <?php } ?>
                      </select>
                      </div>
                      </div>

                <div class="form-group">
                    <label  class="col-sm-2 control-label" >Nama</label>
                    <div class="col-sm-10">
                        <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $nama;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3" >Tempat Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $tempat_lahir;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Tanggal Lahir</label>
                    <div class="col-sm-10">
                     
                         <input type="text" data-mask="date" class="form-control" name="tanggal_lahir" id="datepicker"  value="<?php echo $tanggal_lahir;?>" required/>
                
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                            id="no_identitas" name="no_identitas" value="<?php echo $no_identitas;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Provinsi</label>
                    <div class="col-sm-10">
                          <select name="provinsi_id" class="form-control " id="provinsi" >
                                <option value="">- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                         
                            <div class="col-sm-5">
                              <input  type="text"  name="namaprovinsi" class="form-control" value="<?php echo $namaprovinsi;?>" readonly="true" />
                            </div>
                        
                    </div>

                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kabupaten</label>
                    <div class="col-sm-10">
                       <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
                       
                            <div class="col-sm-5">
                              <input  type="text"  name="namakabupaten" class="form-control" value="<?php echo $namakabupaten;?>" readonly="true" />
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kecamatan</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_id" class="form-control" id="kecamatan">
                                <option value=''>Select Kecamatan</option>
                            </select>
                            <div class="col-sm-5">
                              <input  type="text"  name="namakecamatan" class="form-control" value="<?php echo $namakecamatan;?>" readonly="true" />
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Alamat</label>
                    <div class="col-sm-10">
                        <textarea  name="alamat" class="form-control"  ><?php echo $alamat;?></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Telp</label>
                    <div class="col-sm-10">
                        <input id="telp" name="telp" type="type" class="input_telp form-control" value="<?php echo $telp;?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Email</label>
                    <div class="col-sm-10">
                        <input  name="email" type="text" class="form-control" value="<?php echo $email;?>" >
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select required class="form-control" id="jenis_kelamin" name="select_kelamin">
                        <option value="">- PILIH JENIS KELAMIN -</option>
                       <?php foreach($select_kelamin as $st){ 
                            $selected = ($kelamin == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                     <label class=" col-sm-2 control-label" >Rentang Umur</label>
                     <div class="col-sm-10">
                    <select required class="form-control" name="select_rentangumur">
                      <option value="">- PILIH RENTANG UMUR -</option>
                        <?php foreach($select_rentangumur as $st){ 
                            $selected = ($rentang_umur == $st->id_rentang_umur)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_rentang_umur;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-2 control-label" for="status">Status :</label>
                   <div class="col-sm-10">
                  <select required class="select_status form-control" name="select_statuskawin">
                    <option value="">- PILIH STATUS -</option>
                      <?php foreach($select_statuskawin as $st){ 
                            $selected = ($status_diri == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <!-- <div class="form-group">
                    <label class="col-lg-2 control-label">Berangkat dengan : </label>
                    <div class="col-lg-10">
                     <select required class="form-control" name="select_status_hubungan" id="select_status_hubungan" >
                      <option value="">- PILIH BERANGKAT DENGAN -</option>
                      
                       <?php foreach($select_status_hubungan as $st){ 
                            $selected = ($id_ket_keberangkatan == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div> -->


             <!--  <div class="form-group">
                  <label class=" col-sm-2 control-label" >Hubungan Keluarga</label>
                   <div class="col-sm-10">
                    <select  class="form-control" name="hubungan" id="hubungan">
                        <option value="">- PILIH HUBUNGAN KELUARGA -</option>
                       <?php foreach($family_relation as $st){ 
                            $selected = ($id_hubkeluarga == $st->id_relation)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_relation;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                  </select>
                  </div>
                  </div> -->

                  
<!-- 
                 <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >Nama Keluarga</label>
                        <div class="col-sm-10">
                         <select  class="form-control" name="select_keluarga" id="select_keluarga">
                           <option value="">- PILIH KELUARGA -</option>
                           <?php foreach($select_keluarga as $st){ 
                              $selected = ($id_jamaah == $st->id_jamaah)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_jamaah;?>" <?php echo $selected;?>><?php echo $st->nama;?></option>
                          <?php } ?>
                        </select>
                        </div>
                    </div> -->
                  
                 
 
                <!--    <div class="form-group">
                        <label class="col-sm-2 control-label">Berangkat Dari</label>
                        <div class="col-sm-10">        
                            <select required name="pemberangkatan" class="form-control chosen-select" id="pemberangkatan">
                          
                            <option value=""></option>

                               <?php foreach($select_pemberangkatan as $st){ 
                              $selected = ($bandara == $st->id_bandara)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_bandara;?>" <?php echo $selected;?>><?php echo $st->pemberangkatan;?></option>
                          <?php } ?>
                        </select>
                        </div> 
                      </div> -->


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cast Back / Refund</label>
                        <div class="col-sm-10">
                          <!-- <input  type="hidden" class="form-control" id="id_bandara" name="id_bandara"  readonly="true" /> -->
                          <input type="text" class="form-control" id="refund" name="refund"  value="<?php echo $refund;?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akomodasi</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="akomodasi" name="akomodasi" value="<?php echo $akomodasi;?>"  />
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Handling</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="handling" name="handling" value="<?php echo $handling;?>"  />
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Dp Angsuran</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="dp_angsuran" name="dp_angsuran" value="<?php echo $dp_angsuran;?>"  />
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Angsuran Ke</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="angsuran" name="angsuran" value="<?php echo $angsuran;?>"  />
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Angsuran</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="jml_angsuran" name="jml_angsuran" value="<?php echo $jml_angsuran;?>"  />
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-2 control-label">Muhrim</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="muhrim" name="muhrim" value="<?php echo $muhrim;?>"  />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Harga Paket</label>
                        <div class="col-sm-10">
                          
                          <input type="text" class="form-control" id="harga_paket" name="harga_paket" value="<?php echo $harga_paket;?>"  />
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Room Type</label>
                        <div class="col-sm-10">
                       <select required class="form-control" name="select_type">
                         <option  value="">- PILIH ROOM TYPE -</option>

                            <?php foreach($select_type as $st){ 
                              $selected = ($room_type == $st->id_room_type)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_room_type;?>" <?php echo $selected;?>><?php echo $st->type;?></option>
                          <?php } ?>
                      </select>
                      </div>
                      </div>
                   
                  <div class="form-group">
                      <label class="col-sm-2 control-label"
                            for="inputPassword3" >Ahli Waris</label>
                      <div class="col-sm-10">
                          <input id="waris" name="waris" type="text" class="input_telp form-control" value="<?php echo $ahli_waris;?>"  required>
                      </div>
                  </div>

                  <div class="form-group">
                         <label class="col-sm-2 control-label" >Hubungan Ahli waris </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_hub_ahliwaris">
                          <option  value="">- PILIH Hubungan Ahli Waris -</option>
                            

                            <?php foreach($select_hub_ahliwaris as $st){ 
                              $selected = ($hub_waris == $st->id_hubwaris)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_hubwaris;?>" <?php echo $selected;?>><?php echo $st->hubungan;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                       <div class="form-group">
                         <label class="col-sm-2 control-label" for ="select_merchandise">Merchandise </label>
                         <div class="col-sm-10">
                        <select required class="select_merchandise form-control" name="select_merchandise">
                          <option  value="">- PILIH MERCHANDISE -</option>
                            

                             <?php foreach($select_merchandise as $st){ 
                              $selected = ($merchandise == $st->id_merchandise)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_merchandise;?>" <?php echo $selected;?>><?php echo $st->merchandise;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
        <!--             <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >No Pasport</label>
                        <div class="col-sm-10">
                          <input name="no_pasport" type="text" id="no_pasport"class="input_keluarga form-control" value="<?php echo $no_pasport;?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >issue office</label>
                        <div class="col-sm-10">
                          <input name="issue_office" type="text" id="issue_office"class="input_keluarga form-control" value="<?php echo $issue_office;?>">
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Isue Date</label>
                    <div class="col-sm-10">
                    
                         <input type="text" class="form-control" name="isui_date" id="datepicker2"   value="<?php echo $isui_date;?>" />
                
                    </div>
                  </div> -->
                 <!--  <div class="form-group">
                         <label class="col-sm-2 control-label" >Kartu Identitas </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_status_identitas">                      
                          <?php foreach($select_status_identitas as $st){ 
                              $selected = ($status_identitas == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                         <label class="col-sm-2 control-label" >Kartu Keluarga </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_status_kk">                      
                          <?php foreach($select_status_kk as $st){ 
                              $selected = ($status_kk == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                         <label class="col-sm-2 control-label" >Pas Photo </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_photo">                      
                          <?php foreach($select_photo as $st){ 
                              $selected = ($status_photo == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                         <label class="col-sm-2 control-label" >No Pasport </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_pasport">                      
                          <?php foreach($select_pasport as $st){ 
                              $selected = ($status_pasport == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                     <div class="form-group">
                         <label class="col-sm-2 control-label" >Vaksin </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_vaksin">                      
                          <?php foreach($select_vaksin as $st){ 
                              $selected = ($status_vaksin == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> -->

  <!--              <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Tanggal Pendaftaran</label>
                    <div class="col-sm-10">
                         <input type="text" data-mask="date" class="form-control" name="tgl_daftar" id="datepicker2"  value="<?php echo $tgl_daftar;?>" required/>
                        
                    </div>
                  </div> -->

<!--                     <div class="form-group">
                         <label class="col-sm-2 control-label" >Product </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="product">    
                         <option  value="">- PILIH PRODUCT -</option>                  
                           <?php foreach($select_product as $sp){ 
                    
                              $selected = ($id_product == $sp->id_product)  ? 'selected' :'';
                          ?>
                              
                              <option value="<?php echo $sp->id_product;?>" <?php echo $selected;?>><?php echo $sp->nama;?> </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Room Category</label>
                        <div class="col-sm-10">
                       <select required class="form-control" name="category">
                         <option  value="">- PILIH Category -</option>  
                            <?php foreach($select_roomcategory as $st){ 
                              $selected = ($category == $st->id_room_category)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_room_category;?>" <?php echo $selected;?>><?php echo $st->category;?></option>
                          <?php } ?>
                      </select>
                      </div>
                      </div> -->
                      <!-- <div class="form-group">
                         <label class="col-sm-2 control-label" >Embarkasi </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="embarkasi">  
                                            
                           <?php foreach($select_embarkasi as $se){ 
                    
                              $selected = ($embarkasi == $se->id_embarkasi)  ? 'selected' :'';
                          ?>
                              
                              <option value="<?php echo $se->id_embarkasi;?>" <?php echo $selected;?>><?php echo $se->departure;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> -->

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Keterangan Edit</label>
                    <div class="col-sm-10">
                        <textarea  name="ket_edit" class="form-control" placeholder="ISI KETERANGAN KENAPA ID JAMAAH INI HARUS DI EDIT" required><?php echo $ket_edit;?></textarea>
                    </div>
                  </div>

               <!--   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Status Fee, Pembayaran Dll</label>
                    <div class="col-sm-10">
                        <select required class="form-control"  name="status_claim_fee">
                 
                       <?php foreach($select_ket_fee as $st){ 
                            $selected = ($status_claim_fee == $st->id_aktivasi_manual)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_aktivasi_manual;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div> -->
           
                           
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            <a href="<?php echo base_url();?>data_jamaah/jamaah_aktif/data_jamaahaktif"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    
    </div>
       </div>
    </div>
</div>
<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('data_jamaah/jamaah_aktif/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('data_jamaah/jamaah_aktif/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('data_jamaah/jamaah_aktif/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });


</script>
<script type="text/javascript">
      $(function () {
        $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
         $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
</script>

 <script type="text/javascript">

            $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('registrasi_jamaah/get_biaya_refund_');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#refund").val(html);
                    
                }
            })
        })


            $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('registrasi_jamaah/get_biaya_refund_2');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#id_bandara").val(html);
                    
                }
            })
        })


        $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('registrasi_jamaah/get_biaya_refund_3');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#akomodasi").val(html);
                    
                }
            })
        })

    </script>

</script> <script src="<?php echo base_url();?>assets/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    $("[data-mask='date']").mask("9999-99-99");
    $("[data-mask='phone']").mask("(999) 999-9999");
    $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
    $("[data-mask='phone-int']").mask("+33 999 999 999");
    $("[data-mask='taxid']").mask("99-9999999");
    $("[data-mask='ssn']").mask("999-99-9999");
    $("[data-mask='product-key']").mask("a*-999-a999");
    $("[data-mask='percent']").mask("99%");
    $("[data-mask='currency']").mask("$999,999,999.99");
  });
</script>