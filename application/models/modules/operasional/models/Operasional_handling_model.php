<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_handling_model extends CI_Model {

    private $db2;
    private $_table_affiliate = "affiliate";
    private $_primary_id_mgm = "id_mgm";
    private $_table = "member_mgm";
    private $_primary = "id_mgm";
    private $_table_backup = "member_mgm_backup";
    private $_primary_mgm = "id_mgm";
    private $_booking = "booking";
    private $_id_booking = "id_booking";
    private $_registrasi_jamaah = "registrasi_jamaah";
    private $_id_registrasi_jamaah = "id_registrasi";

    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2', TRUE);
    }

    public function generate_kode($idx) {
        $today = date('ym');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $idx . $today;
    }

    public function generate_kode_jamaah($idx) {
        $today = date('md');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $today . $idx;
    }

    public function generate_id_jamaah($idx) {
        $today = date('ym');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $idx . $today;
    }

    public function save($data) {

        $initial = "DP1";
        $arr = array(
            'id_user' => $this->generate_kode($initial . $id_aff),
            'id_affiliate_type' => 6,
            'id_sahabat' => $data['id_affiliate'],
            'sponsor' => $this->session->userdata('id_user'),
            'nama' => $data['nama'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['select_hub_ahliwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'id_product' => 4,
            'category' => 2,
            'wkt_tunggu' => 1,
            'dp' => $data['biaya_registrasi'],
            'deposit' => $data['biaya_registrasi'],
            'deposit_sahabat' => $data['biaya_registrasi'],
            'pembayaran_dp' => $data['biaya_registrasi'],
            'pin' => '50',
            'harga_satuan' => $data['harga_product'],
            'biaya_pelunasan' => ($data['harga_product'] - $data['biaya_registrasi']),
            'status' => 0,
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'kode_unik' => $data['unique_digit'],
        );
        $this->db->trans_begin(); //transaction initialize

        $this->db->insert($this->_table, $arr);
        $id_mgm = $this->db->insert_id(); //get last insert ID

        $this->db->update($this->_table, array('id_user' => $this->generate_kode($initial . $id_mgm)), array('id_mgm' => $id_mgm));

        //   $sponsor= $this->session->userdata('id_user');
        //  $sql = "SELECT a.*,COUNT(b.id_user) as jml_downline,CONCAT('GR',CEILING(COUNT(b.id_user)/5)) as grouping
        //     from affiliate as a
        //     LEFT JOIN member_mgm b ON b.sponsor = a.id_user
        //     where a.id_affiliate_type in('1','5') and a.id_user = '".$sponsor."'
        //     GROUP BY a.id_user";
        // $query = $this->db->query($sql)->result_array();
        //   foreach($query as $key=>$value){
        //     $grouping = $value['grouping'];
        // }
        // $arr = array(
        //   'group_id' => $grouping,
        // );    
        // $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 
        // if ($grouping == GR1){
        //     $arr = array(
        //       'id_reward' => 1,
        //     );    
        //     $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 
        // }else{
        //      $arr = array(
        //       'id_reward' => 2,
        //     );    
        //     $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 
        // }

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            return false;
        } else {

            $this->db->trans_complete();
            redirect('member_mgm/report_member/' . $id_mgm . '');
            return true;
        }
    }

    public function save_upgrade_gm($data) {
        $initial = "GM";


        $arr = array(
            'sponsor' => $data['sponsor'],
            'id_affiliate_type' => 5,
            'id_affiliate' => $data['id_sahabat'],
            'nama' => $data['nama'],
            'password' => md5($data['password']),
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['hubwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'status' => 0,
            'mgm' => 1,
            'ket_upgrade' => 'UPGRADE JAMAAH DP KE GM',
            'status' => 1,
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'jabatan_id' => 26,
            'create_by' => $this->session->userdata('id_user'),
            'pajak' => '0.06'
        );

        $this->db->trans_begin(); //transaction initialize

        $this->db->insert($this->_table_affiliate, $arr);
        $id_aff = $this->db->insert_id(); //get last insert ID


        $this->db->update($this->_table_affiliate, array('id_user' => $this->generate_kode($initial . $id_aff)), array('id_aff' => $id_aff));

        $this->db->update($this->_table_affiliate, array('username' => $this->generate_kode($initial . $id_aff)), array('id_aff' => $id_aff));
        $id_user = $this->db->insert_id();


        $arr = array(
            'id_user' => $data['id_user'],
            'id_perwakilan' => $this->generate_kode($initial . $id_aff),
            'id_affiliate_type' => 6,
            'id_sahabat' => $data['id_sahabat'],
            'sponsor' => $data['sponsor'],
            'nama' => $data['nama'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['hubwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'id_product' => 4,
            'category' => 2,
            'wkt_tunggu' => 1,
            'dp' => $data['dp'],
            'biaya_pelunasan' => $data['biaya_pelunasan'],
            'status' => 1,
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
        );

        $this->db->insert($this->_table_backup, $arr);


        $arr = array(
            'id_user' => $this->generate_kode($initial . $id_aff),
            'id_affiliate_type' => 5,
            'st_upgrade' => 1,
            'id_sahabat' => $data['id_sahabat'],
            'mgm' => 1,
            'st_upgrade' => 3,
            'upgrade' => '2',
            'ket_upgrade' => 'UPGRADE JAMAAH DP KE GM',
        );
        $this->db->update('member_mgm', $arr, array('id_mgm' => $data['id_mgm']));


        // $arr = array(
        //     'userid' => $data['id_sahabat'], 
        //     'tgl_daftar' => date('Y-m-d H:i:s'),
        //     'tgl_post' => date('Y-m-d H:i:s'),
        //     'admin'=> 'KONVEN', 
        //     'st'=> 0, 
        //     'nominal' => 5000000,
        //     'sisa' => 0,
        // );    
        // $this->db2->insert('t_perwakilan',$arr);

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            return false;
        } else {

            $this->db->trans_complete();

            return true;
        }
    }

    public function save_upgrade_perwakilan($data) {
        $initial = "GM";
        $id_aff_before = $this->input->post('id_aff');

        $arr = array(
            'sponsor' => $this->session->userdata('id_user'),
            'id_affiliate_type' => 5,
            'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
            'password' => md5($data['password']),
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['hubwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            // 'namarek' => $data['namarek'],
            // 'norek' => $data['norek'],
            'status' => 0,
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'jabatan_id' => 26,
            'create_by' => $this->session->userdata('id_user'),
            'pajak' => '0.08'
        );

        $this->db->trans_begin(); //transaction initialize

        $this->db->insert($this->_table_affiliate, $arr);
        $id_aff = $this->db->insert_id(); //get last insert ID


        $this->db->update($this->_table_affiliate, array('id_user' => $this->generate_kode($initial . $id_aff)), array('id_aff' => $id_aff));

        $this->db->update($this->_table_affiliate, array('username' => $this->generate_kode($initial . $id_aff)), array('id_aff' => $id_aff));
        $id_user = $this->db->insert_id();


        $arr = array(
            'id_user' => $this->generate_kode($initial . $id_aff),
            'id_affiliate_type' => 5,
            'id_sahabat' => $data['id_affiliate'],
            'sponsor' => $this->session->userdata('id_user'),
            'nama' => $data['nama'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['hubwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'id_product' => 4,
            'category' => 2,
            'wkt_tunggu' => 1,
            'dp' => 5000000,
            'pembayaran_dp' => 10000000 - $data['deposit'],
            'pin' => 100,
            'deposit' => 10000000,
            'deposit_sahabat' => $data['deposit'],
            'biaya_pelunasan' => 28000000 - 5000000,
            'harga_satuan' => $data['harga_product'],
            'status' => 0,
            'mgm' => 1,
            'st_upgrade' => 1,
            'upgrade' => '3',
            'ket_upgrade' => 'UPGRADE PERWAKILAN LAMA KE GM',
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'kode_unik' => 0,
        );

        $this->db->insert($this->_table, $arr);
        $id_mgm = $this->db->insert_id();

        $arr = array(
            'status' => 0,
            'pajak' => '0.08'
        );
        $this->db->update('affiliate', $arr, array('id_aff' => $id_aff_before));

        $arr = array(
            'nominal' => $data['deposit'],
            'st' => 0,
            'sisa' => $data['deposit'] - 5000000,
        );
        $this->db2->update('t_perwakilan', $arr, array('userid' => $data['id_affiliate']));



        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            return false;
        } else {

            $this->db->trans_complete();
            redirect('member_mgm/report_upgradeperwakilan/' . $id_mgm . '/');

            return true;
        }
    }

    public function save_mutasi($data) {
        $schedule = $this->input->post('radio');
        $initial = "MG";
        $sql = "SELECT *
         from view_schedule where id_schedule ='$schedule'";
        $query = $this->db->query($sql)->result_array();
        foreach ($query as $key => $value) {
            $id_schedule = $value['id_schedule'];
            $id_product = $value['id_product'];
            $id_room_type = $value['id_room_type'];
            $room_category = $value['room_category'];
            $embarkasi = $value['embarkasi'];
            $harga = $value['harga'];
            $id_productprice = $value['id_productprice'];
            $seats = $value['seats'];
            $date_schedule = $value['date_schedule'];
        }
        $arr = array(
            'id_user_affiliate' => $this->session->userdata('id_user'),
            'id_product' => $id_product,
            'id_schedule' => $schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'harga' => $data['harga_satuan'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'kode_unik' => random_3_digit(),
            'tipe_jamaah' => 8,
            'status' => 0
        );
        $this->db->trans_begin();

        $this->db->insert($this->_booking, $arr);
        $id_booking = $this->db->insert_id();
        $this->db->update($this->_booking, array('invoice' => $this->generate_kode($initial . $id_booking)), array('id_booking' => $id_booking));

        $arr = array(
            'id_booking' => $id_booking,
            'invoice' => $this->generate_kode($initial . $id_booking),
            'id_schedule' => $id_schedule,
            'id_affiliate' => $this->session->userdata('id_user'),
            'id_product' => $id_product,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'nama' => $data['nama'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'status_diri' => $data['select_statuskawin'],
            'kelamin' => $data['select_kelamin'],
            'rentang_umur' => $data['select_rentangumur'],
            'no_identitas' => $data['no_identitas'],
            'provinsi_id' => $data['provinsi_id'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'alamat' => $data['alamat'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ket_keberangkatan' => 9,
            'ahli_waris' => $data['waris'],
            'hub_waris' => $data['select_hub_ahliwaris'],
            'room_type' => 1,
            'category' => $room_category,
            'merchandise' => $data['select_merchandise'],
            'id_bandara' => $data['id_bandara'],
            'refund' => 0,
            'handling' => $data['handling'],
            'akomodasi' => $data['akomodasi'],
            'fee_input' => 0,
            'fee' => 0,
            'dp_angsuran' => $data['dp'],
            'harga_paket' => $data['harga_satuan'],
            'create_by' => $this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 0,
            'tipe_jamaah' => 8,
            'id_mgm' => $data['id_mgm'],
        );


        $this->db->insert($this->_registrasi_jamaah, $arr);
        $id_registrasi = $this->db->insert_id();

        $this->db->update($this->_registrasi_jamaah, array('id_jamaah' => $this->generate_id_jamaah($initial . $id_registrasi)), array('id_registrasi' => $id_registrasi));



        $arr = array(
            'id_booking' => $id_booking,
            'id_registrasi' => $id_registrasi,
            'id_jamaah' => $this->generate_id_jamaah($initial . $id_registrasi),
            'id_affiliate' => $data['id_affiliate'],
            'no_pasport' => $data['no_pasport'],
            'issue_office' => $data['issue_office'],
            'isui_date' => $data['isue_date'],
            'status_identitas' => $data['status_identitas'],
            'status_kk' => $data['status_kk'],
            'status_photo' => $data['status_photo'],
            'status_pasport' => $data['status_pasport'],
            'status_vaksin' => $data['status_vaksin'],
            'hubkeluarga' => 9,
            'create_by' => $this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 0,
        );
        $this->db->insert('manifest', $arr);

        $arr = array(
            'id_affiliate' => $this->session->userdata('id_user'),
            'id_booking' => $id_booking,
            'id_product' => $id_product,
            'id_schedule' => $schedule,
            'id_registrasi' => $id_registrasi,
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'status' => 0
        );
        $this->db->insert('transaksi_jamaah', $arr);

        $arr = array(
            'id_registrasi' => $id_registrasi,
            'id_booking' => $id_booking,
            'status' => 0
        );
        $this->db->insert('pengiriman', $arr);



        $arr = array(
            'id_registrasi' => $id_registrasi,
            'id_booking' => $id_booking,
            'status_visa' => $data['select_status_visa'],
            'tgl_visa' => $data['tanggal_visa'],
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'status' => 0
        );
        $this->db->insert('visa', $arr);


        $sql = "call hitung_muhrim(?) ";
        $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
        foreach ($query as $key => $value) {
            // $id_registrasi = $value['id_registrasi'];
            $biaya_muhrim = $value['muhrim'];
        }

        $arr = array(
            'muhrim' => $biaya_muhrim,
        );
        $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));



        $sql = "call v_visa(?) ";
        $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
        foreach ($query as $key => $value) {
            // $id_registrasi = $value['id_registrasi'];
            $biaya_visa = $value['Total_VISA'];
        }

        $arr = array(
            'visa' => $biaya_visa,
        );
        $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));




        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;
        } else {

            $this->db->trans_complete();
            redirect('member_mgm/report_registrasi/' . $id_booking . '');
            return true;
        }
    }

    function cek_ktp_sahabat($ktp) {
        $ktp = $this->input->post('ktp');
        $id_sahabat = $this->input->post('id_sahabat');
        $query = $this->db2->query("SELECT * FROM t_member Where ktp ='$ktp'");
        return $query;
    }

    function cek_id_sahabat($id_sahabat) {
        $id_sahabat = $this->input->post('id_sahabat');
        $query = $this->db2->query("SELECT * FROM t_member Where userid ='$id_sahabat' ");
        return $query;
    }

    function cek_id_sahabat_affiliate($id_sahabat) {
        $id_sahabat = $this->input->post('id_sahabat');
        $query = $this->db->query("SELECT * FROM affiliate Where id_affiliate ='$id_sahabat'");
        return $query;
    }

    function cek_id_sahabat_perwakilan($id_sahabat) {
        $id_sahabat = $this->input->post('id_sahabat');
        $query = $this->db2->query("SELECT * FROM t_perwakilan Where userid ='$id_sahabat'");
        return $query;
    }

    function cek_id_sahabat_profil($id_user) {
        $id_user = $this->session->userdata('id_user');
        $query = $this->db->query("SELECT COUNT(id_affiliate) FROM affiliate Where id_user ='$id_user'");
        return $query;
    }

    public function get_data($offset, $limit, $q = '') {
        $id_user = $this->session->userdata('id_user');

        $sql = " 
                SELECT a.*,b.nama as provinsi,c.nama as kabupaten,d.nama as kecamatan,e.keterangan as jns_kelamin
                from member_mgm as a
                LEFT JOIN  wilayah_provinsi  b on b.provinsi_id = a.provinsi_id
                LEFT JOIN  wilayah_kabupaten  c on c.kabupaten_id = a.kabupaten_id
                LEFT JOIN  wilayah_kecamatan  d on d.kecamatan_id = a.kecamatan_id
                LEFT JOIN  status  e on e.kdstatus = a.kelamin
                where a.status ='1' and a.sponsor ='" . $id_user . "'
                    ";

        if ($q) {


            $sql .= " AND a.id_user LIKE '%{$q}%'
                    OR a.id_sahabat LIKE '%{$q}%'
                    OR a.nama LIKE '%{$q}%'
                    
                    ";
        }
        $sql .= " ORDER BY a.tanggal_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .= " LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->opsi_setting] = $value->key_setting;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_all_kabupaten() {
        $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }

    public function report_member($id_mgm) {
        $sql = "SELECT * from transaksi_mgm where id_mgm = '$id_mgm'";

        return $this->db->query($sql)->row_array();
    }

    public function report_member_split($id_mgm, $id_mgm2) {

        $id_mgm = $this->uri->segment(3);
        $id_mgm2 = $this->uri->segment(4);
        $sql = "SELECT * from transaksi_mgm where  id_mgm in('" . $id_mgm . "','" . $id_mgm2 . "')

              ";
        return $this->db->query($sql, array($id_mgm, $id_mgm2))->result_array();
    }

    public function get_status_gm($id_user) {
        $id_user = $this->session->userdata('id_user');
        $sql = "SELECT * from affiliate where id_user = '$id_user'";

        return $this->db->query($sql)->row_array();
    }

    public function report_member_upgradeoldperwakilan($id_user) {
        $sql = "SELECT a.*,b.nama as provinsi,c.nama as kabupaten,d.nama as kecamatan,e.keterangan as jns_kelamin,
                (a.dp + a.kode_unik) as nominal_dp
                from member_mgm as a
                LEFT JOIN  wilayah_provinsi  b on b.provinsi_id = a.provinsi_id
                LEFT JOIN  wilayah_kabupaten  c on c.kabupaten_id = a.kabupaten_id
                LEFT JOIN  wilayah_kecamatan  d on d.kecamatan_id = a.kecamatan_id
                LEFT JOIN  status  e on e.kdstatus = a.kelamin 
                where a.`status`='1' ";

        return $this->db->query($sql)->row_array();
    }

    public function upgrade_gm($id_mgm) {
        $id_user = $this->session->userdata('id_user');

        $sql = " 
                SELECT a.*,b.nama as provinsi,c.nama as kabupaten,d.nama as kecamatan,e.keterangan as jns_kelamin
                from member_mgm as a
                LEFT JOIN  wilayah_provinsi  b on b.provinsi_id = a.provinsi_id
                LEFT JOIN  wilayah_kabupaten  c on c.kabupaten_id = a.kabupaten_id
                LEFT JOIN  wilayah_kecamatan  d on d.kecamatan_id = a.kecamatan_id
                LEFT JOIN  status  e on e.kdstatus = a.kelamin
                where status ='1' and a.sponsor ='" . $id_user . "' and id_mgm = '$id_mgm'
                    ";
        return $this->db->query($sql)->row_array();
    }

    public function upgrade_perwakilan_lama_p($id_aff) {
        $id_user = $this->session->userdata('id_user');

        $sql = "SELECT a.*,b.st,b.nominal
                from affiliate as a,sahabatsbl_live.t_perwakilan as b where a.id_affiliate=b.userid and a.id_affiliate_type='2'  and id_aff = '$id_aff'
                    ";
        return $this->db->query($sql)->row_array();
    }

    function searchItem($paket, $departure, $datepicker_tahun_keberangkatan, $datepicker_keberangkatan) {



        $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'";
        $query = $this->db->query($sql);

        if (empty($query->result())) {

            echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
            exit;
        } else {


            foreach ($query->result() as $row) {


                echo'<tr>
                   
                   <td><strong>' . $row->paket . '</strong></td>
                   <td><strong>' . $row->departure . '</strong></td>
                   <td><strong>' . $row->date_schedule . '</strong></td>
                   <td><strong>' . $row->time_schedule . '</strong></td>
                   <td><strong>' . $row->seats . '</strong></td>
                   <td><strong>' . $row->type . '</strong></td>
                   <td><strong>' . $row->category . '</strong></td>
                   <td><strong>' . $row->keterangan . '</strong></td>
                 
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="' . $row->id_schedule . ' "</td>
                
                </a></div>
                </td>
                </tr>';
            }
        }
    }

    function get_data_bandara($pemberangkatan = '') {
        $this->db->where("pemberangkatan", $pemberangkatan);
        return $this->db->get("view_refund");
    }

    public function get_report_registrasi($id_booking) {

        $id_user = $this->session->userdata('id_user');
        $sql = "SELECT * from data_laporan_jamah_belumaktif_all where id_booking = {$id_booking} and id_user_affiliate ='" . $id_user . "'";

        return $this->db->query($sql)->row_array();
    }

    function searchperwakilan_lama($id_sahabat) {



        $sql = "SELECT a.*,b.st,b.nominal
        from affiliate as a,sahabatsbl_live.t_perwakilan as b where a.id_affiliate=b.userid and a.id_affiliate_type='2' and a.status='1' and id_affiliate='$id_sahabat'";
        $query = $this->db->query($sql);

        if (empty($query->result())) {

            echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! DATA Yang Anda Cari Kosong </h2></td></tr>';
            exit;
        } else {


            foreach ($query->result() as $row) {


                echo'<tr>
                   
                   <td><strong>' . $row->nama . '</strong></td>
                   <td><strong>' . $row->id_user . '</strong></td>
                   <td><strong>' . $row->id_affiliate . '</strong></td>

                 
                    <td><div class="btn-group"><a href="' . base_url() . 'member_mgm/upgrade_perwakilan_lama/' . $row->id_aff . '"  class="btn btn-info btn-sm" ><i class="glyphicon glyphicon-pencil"></i>UPGRADE</a>
                        </div>
                </td>
                </tr>';
            }
        }
    }

    function add_perwakilan($id, $nominal) {
        $query = $this->db->query("select f_addperwakilan('$id','$nominal') as hasil_add");
        if ($query->num_rows === 1) {
            return $query->row();
        } else {
            return '0';
        }
    }

    public function get_pic_detail_bilyet($id_user) {

        $sponsor = $this->session->userdata('id_user');
        $sql = " SELECT a.*,b.nama as provinsi,c.nama as kabupaten,d.nama as kecamatan,e.keterangan as jns_kelamin
                from member_mgm as a
                LEFT JOIN  wilayah_provinsi  b on b.provinsi_id = a.provinsi_id
                LEFT JOIN  wilayah_kabupaten  c on c.kabupaten_id = a.kabupaten_id
                LEFT JOIN  wilayah_kecamatan  d on d.kecamatan_id = a.kecamatan_id
                LEFT JOIN  status  e on e.kdstatus = a.kelamin
                where status ='1' and a.sponsor ='" . $sponsor . "' and id_user = '$id_user'
                    ";
        return $this->db->query($sql)->row_array();
    }

    public function save_cetak_bilyet($data) {
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            'id_mgm' => $data['id_mgm'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['id_user'],
            'sponsor' => $data['sponsor'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
        );

        $this->db->insert('bilyet_mgm', $arr);

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();

            return true;
        }
    }

    function search_invoice($invoice) {

        $cabang = $this->session->userdata('id_user');

        $sql = "call List_handling(?)";
        $query = $this->db->query($sql, array('invoice' => $invoice));

        if (empty($query->result())) {

            echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! DATA Yang Anda Cari Kosong </h2></td></tr>';
            exit;
        } else {

            foreach ($query->result() as $row) {
                
                $btnProcess = '';
                if ($row->status == '1') {
                    $btnProcess = '<div class="btn-group">
                        <button type="button" onclick ="claim_biaya_submit(\'' . $row->invoice . '\')"  class="btn btn-warning" ><i class="glyphicon glyphicon-pencil"></i>Claim</a>
                        </div>';
                    //$btnProcess = '<button onclick="claim_biaya(' . $item['invoice'] . ')" type="button" class="btn btn btn-warning" >Claim</button>';
                    //$btnProcess = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
                } elseif ($row->status == '1A') {
                    $btnProcess = '<button type="button" class="btn btn btn-danger" >Menunggu Approval GM</button>';
                } elseif ($row->status == '2') {
                    $btnProcess = '<button type="button" class="btn btn btn-primary" >Menunggu Konfirmasi</button>';
                } elseif ($row->status == '3') {
                    $btnProcess = '<button type="button" class="btn btn btn-info" >On Process</button>';
                } else {
                    $btnProcess = '<button type="button" class="btn btn btn-success" >Sudah di uangkan</button>';
                }

                echo'<tr>
                   
                   <td><strong>' . $row->invoice . '</strong></td>
                   <td>' . $row->id_user . '</td>    
                   <td>' . $row->affiliate . '</td>
                   <td>' . $row->jml_jamaah . '</td>
                   <td>' . number_format($row->biaya_operasional) . '</td>
                   <td>' . $row->keterangan . '</td>
                   
                    <td>' . $btnProcess . '</td>
                </tr>';
                //<a href="' . base_url() . 'operasional/claim_biaya/' . $row->invoice . '"  class="btn btn-warning" ><i class="glyphicon glyphicon-pencil"></i>Claim</a>
            }
        }
    }

    public function claim_update($invoice) {

        $arr = array(
            'status' => '1A',
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user'),
            'id_claim' => $this->session->userdata('id_user')
        );
        $param = array(
            'invoice' => $invoice,
            'jns_trans' => '144',
            'status_manifest' => '1'
        );
        return $this->db->update('additional_cost', $arr, $param);
    }

    public function approve($invoice) {

        $arr = array(
            'status' => '2',
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user')
            //'id_claim' => $this->session->userdata('id_user')
        );
        $param = array(
            'invoice' => $invoice,
            'jns_trans' => '144',
            'status_manifest' => '1'
        );
        return $this->db->update('additional_cost', $arr, $param);
    }

    public function not_approve($invoice) {

        $arr = array(
            'status' => '1',
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user'),
            'id_claim' => ''
        );
        $param = array(
            'invoice' => $invoice,
            'jns_trans' => '144',
            'status_manifest' => '1'
        );
        return $this->db->update('additional_cost', $arr, $param);
    }

}
