<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional extends CI_Controller {

    var $folder = "operasional";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend_affiliate/login_affiliate');
        if (!$this->general->privilege_check_affiliate(OPERASIONAL, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'operasional');
        $this->load->model('Operasional_handling_model');

    }

    public function index() {
        $id_user = $this->session->userdata('id_user');
        $this->template->load('template/template', $this->folder . '/claim_biaya_operasional');
        
    }

    public function operasional_handling() {
        $this->template->load('template/template', $this->folder . '/operasional_handling');
    }

    public function ajax_list() {
        $id_user = $this->session->userdata('id_user');
        $sql = "call List_handling_cabang(?)";
        $hasil = $this->db->query($sql,array('id_user' => $id_user))->result_array();

        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;

//$tgl = empty($item['tgl_pindahpaket'])? "" : date('d-m-Y', strtotime($item['tgl_pindahpaket']));
            //$transfered = '';
            $row[] = '<td width="20%" >' . $item['invoice'] . '<td>';
            $row[] = '<td width="20%" >' . $item['id_user'] . '<br>' . $item['affiliate'] . '<td>';
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . $item['keterangan'] . '<td>';
            $row[] = '<td width="20%" >' . number_format($item['biaya_operasional']) . '<td>';
//$row[] = '<td width="20%" >' . $transfered . '<td>';
           
            $btnProcess = '';
            if ($item['status'] == '1') {
//$btnProcess = '<button onclick="claim_biaya(' . $item['invoice'] . ')" type="button" class="btn btn btn-warning" >Claim</button>';
                $btnProcess = '<a class="btn btn-sm btn-warning" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
            }elseif($item['status'] == '1A'){
                $btnProcess = '<button type="button" class="btn btn btn-danger" >Menunggu Approval GM</button>';
            }elseif ($item['status'] == '2') {
                $btnProcess = '<button type="button" class="btn btn btn-primary" >Menunggu Konfirmasi</button>';
            } elseif ($item['status'] == '3') {
                $btnProcess = '<button type="button" class="btn btn btn-info" >On Process</button>';
            } else{
                $btnProcess = '<button type="button" class="btn btn btn-success" >Sudah di uangkan</button>';
            }
//            $btnEdit = '<button onclick="edit_data(\''.$item['id'].'\')" type="button" class="btn btn-primary" ><i class="fa fa-edit"></i></button>';
//            //$btnDelete = '<button type="button" id="btn-Delete" class="btn btn-danger" onclick=':.delete_paket(.$item['id_sahabat'].).'"><i class="glyphicon glyphicon-trash"></i></button>';
//            $btnDelete = '<a class="btn btn-danger" href="javascript:void()" title="Hapus" onclick="delete_paket(' . "'" . $item['id'] . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a> ';
            $row[] = '<td width="20%" >' . $btnProcess . '</td>';

//$row[] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'].'<br>'.$aktivasi['update_by'];
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function claim_biaya_submit() {
        $invoice = $this->input->post('invoice');
        
        $send = $this->Operasional_handling_model->claim_update($invoice);
        if ($send){
            //redirect('operasional');
            $data['invoice'] = $invoice;
            $this->template->load('template/template', $this->folder . '/operasional_handling', $data);
        }
    }

    public function claim_biaya() {
        $invoice = $this->uri->segment(3);
        $send = $this->Operasional_handling_model->claim_update($invoice);

//echo json_encode(array("status" => TRUE));
//if ($send)
//    redirect('operasional');
    }
    
    public function approve() {
        $invoice = $this->uri->segment(3);
        $send = $this->Operasional_handling_model->approve($invoice);
    }
    
     public function not_approve() {
        $invoice = $this->uri->segment(3);
        $send = $this->Operasional_handling_model->not_approve($invoice);
    }

//    public function ajax_list_invoice($invoice) {
//        $invoice = $invoice;
//        $id_user = $this->session->userdata('id_user');
//        $data = array();
//        
//        $id_user = $this->session->userdata('id_user');
//        $sql = "call List_handling_claim(?)";
//        $hasil = $this->db->query($sql,array('id_user' => $id_user))->result_array();
//        
//        $no = 0;
//        foreach ($hasil as $item) {
//            $no++;
//            $row = array();
//            //$row[] = $no;
//            
//            $btnProcess = '';
//            if ($item['status'] == '1') {
////$btnProcess = '<button onclick="claim_biaya(' . $item['invoice'] . ')" type="button" class="btn btn btn-warning" >Claim</button>';
//                $btnProcess = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
//            }elseif($item['status'] == '1A'){
//                $btnProcess = '<button type="button" class="btn btn btn-danger" >Menunggu Approval GM</button>';
//            } elseif ($item['status'] == '2') {
//                $btnProcess = '<button type="button" class="btn btn btn-primary" >Menunggu Konfirmasi</button>';
//            } elseif ($item['status'] == '3') {
//                $btnProcess = '<button type="button" class="btn btn btn-info" >On Process</button>';
//            } else{
//                $btnProcess = '<button type="button" class="btn btn btn-success" >Sudah di Transfer ke </button>';
//            }
//
//            $row[] = '<td>' . $item['invoice'] . '<td>';
//            $row[] = '<td>' . $item['id_user'] . '<td>';
//            $row[] = '<td>' . $item['affiliate'] . '<td>';
//            $row[] = '<td>' . $item['jml_jamaah'] . '<td>';
//            $row[] = '<td>' . number_format($item['biaya_operasional']) . '<td>';
//            $row[] = '<td>' . $item['keterangan'] . '<td>';
//            $row[] = '<td>'.$btnProcess.'<td>';
//
//            $data[] = $row;
//        }
//        $output = array(
//            'data' => $data
//        );
//        //output
//        //$this->myDebug($data);
//        echo json_encode($output);
//    }
    
    function search_invoice() {
        $search = $this->input->post('s');

        if (!empty($search)) {
            $this->Operasional_handling_model->search_invoice($search);
        } else {
            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
        }
    }
    
    function approval_claim_operasional()
    {
        $this->template->load('template/template', $this->folder . '/appr_claim_biaya_operasional');
    }
    
    public function ajax_list_appr() { 
        $id_user = $this->session->userdata('id_user');
        $sql = "call List_handling_cabang(?)";
        $hasil = $this->db->query($sql,array('id_user' => $id_user))->result_array();

        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $cabang = $this->get_data_cabang($item['id_claim']);
           
            $transfered = '';
            $row[] = '<td width="20%" >' . $item['invoice'] . '<td>';
            $row[] = '<td width="20%" >' .  $item['id_user'].'<br>'.$item['affiliate'] . '<td>';
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . $item['keterangan'] . '<td>';
            $row[] = '<td width="20%" >' . number_format($item['biaya_operasional']) . '<td>';

            $btnAppr = '';
            $btnNotAppr = '';
            $btnStatus = '';
            if($item['status']=='1'){
                $btnStatus = '<a class="btn btn-sm btn-warning" > Belum di claim </a>';
                $btnAppr = '<a disabled class="btn btn-sm btn-success" href="javascript:void()" title="APPROVE" onclick="approve(' . "'" . $item['invoice'] . "'" . ')"> Approve</a>';
                $btnNotAppr = '<a disabled class="btn btn-sm btn-danger" href="javascript:void()" title="NOT APPROVE" onclick="not_approve(' . "'" . $item['invoice'] . "'" . ')"> Not Approve</a>';
            }elseif($item['status']=='1A'){
                $btnStatus = '<a class="btn btn-sm btn-primary" > Di claim oleh : '.$item['id_claim'].'<br>'.$cabang['nama_cabang'].' </a>';
                $btnAppr = '<a class="btn btn-sm btn-success" href="javascript:void()" title="APPROVE" onclick="approve(' . "'" . $item['invoice'] . "'" . ')"> Approve</a>';
                $btnNotAppr = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="NOT APPROVE" onclick="not_approve(' . "'" . $item['invoice'] . "'" . ')"> Not Approve</a>';
            }elseif($item['status']=='2'){
                $btnStatus = '<a class="btn btn-sm btn-primary" > Menunggu konfirmasi </a>';
                $btnAppr = '<a disabled class="btn btn-sm btn-success" href="javascript:void()" title="APPROVE" onclick="approve(' . "'" . $item['invoice'] . "'" . ')"> Approve</a>';
                $btnNotAppr = '<a disabled class="btn btn-sm btn-danger" href="javascript:void()" title="NOT APPROVE" onclick="not_approve(' . "'" . $item['invoice'] . "'" . ')"> Not Approve</a>';
            }
            $row[] = '<td width="20%" >' . $btnStatus . '<td>';
            
            $row[] = '<td width="20%" >' . $btnAppr .' '.$btnNotAppr. '</td>';

//$row[] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'].'<br>'.$aktivasi['update_by'];
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        
        //output to json format
        echo json_encode($output);
    }
    
    function get_data_cabang($id_claim) {
        $this->db->select('nama as nama_cabang');
        $this->db->where('id_user', $id_claim);
        $this->db->order_by('id_user', 'DESC');
        $hasil = $this->db->get('affiliate')->row_array();
        $result = array(
            'nama_cabang' => !empty($hasil['nama_cabang']) ? $hasil['nama_cabang'] : ''
        );
        //$this->myDebug($result);
        return $result;
    }
}

