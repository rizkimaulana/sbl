<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Approval Claim Biaya Operasional Oleh Cabang</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form name="form" role="form" action="<?php echo base_url(); ?>paket/export_pindah_paket" method="POST"  class="form-horizontal">
                        <div class="form-group">
                            <!--<div class="col-sm-2">
                                
                                <select name="filter" id="filter" class="form-control">
                                    <option value="0" selected="selected">Select Filter</option>
                                    <option value="1">Create Date</option>
                                </select>
                            </div>-->
                            <!--<div class="col-sm-2">

                                <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" name="btn" value="filter" id="btn-filter" class="btn btn-primary">Filter</button>
                            <!--<button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>-->
                            <!--</div>-->
                        </div>
                        <!--<div class="form-group">
                            <div class="col-sm-6">
                                <button data-toggle="modal" data-target="#md-custom" type="button" class="btn btn-primary"> <i class="fa fa-plus"></i>Add</button>
                                <button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>
                                <!--<button data-toggle="modal" data-target="#md-custom2" type="button" class="btn btn-warning"> <i class="fa fa-upload"></i>Import</button>-->
                                <!--<button type="submit" id="btnInput" class="btn btn-success"><i class="fa fa-plus"></i>Add</button>-->
                        <!--</div>
                    </div>-->
                    </form>
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Invoice</th>
                                    <th>Data Affiliate</th>
                                    <th>JML</th>
                                    <th>Keterangan</th>
                                    <th>Biaya</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->

<script>
    function approve(id)
    {
        if (confirm('Apakah Anda Akan Approve Data Ini ('+id+') '))

        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('operasional/approve') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    //alert('success');
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                    table.ajax.reload();

                    //  alert('Error deleting data');
                    // window.location.href="<?php //echo site_url('no_access') ?>";
                }
            });

        }
    }
    
    function not_approve(id)
    {
        if (confirm('Apakah Anda Akan Menolak Claim Ini ('+id+') '))

        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('operasional/not_approve') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    //alert('success');
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                    table.ajax.reload();

                    //  alert('Error deleting data');
                    // window.location.href="<?php //echo site_url('no_access') ?>";
                }
            });

        }
    }


    
</script>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker3').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker4').datetimepicker({
            format: 'YYYY-MM-DD',
        });

        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('operasional/ajax_list_appr') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                    //data.filter = $('#filter').val();
                }
            },
        });

        $('#btn-filter').click(function () { //button filter event click
            //var tipe_tanggal = document.getElementById("tipe_tanggal").value;

            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            //var filter = document.getElementById("filter").value;
            table.ajax.url("<?php echo site_url('operasional_cost/json_pindah_paket'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table

        });

        
    });

   
</script>