<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Claim Biaya Operasional By Invoice</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <!--<form name="form" role="form" action="<?php echo base_url(); ?>paket/export_pindah_paket" method="POST"  class="form-horizontal">
                        <div class="form-group">
                            <!--<div class="col-sm-2">
                                
                                <select name="filter" id="filter" class="form-control">
                                    <option value="0" selected="selected">Select Filter</option>
                                    <option value="1">Create Date</option>
                                </select>
                            </div>-->
                            <!--<div class="col-sm-2">

                                <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" name="btn" value="filter" id="btn-filter" class="btn btn-primary">Filter</button>
                            <!--<button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>-->
                            <!--</div>-->
                        <!--</div>
                        <!--<div class="form-group">
                            <div class="col-sm-6">
                                <button data-toggle="modal" data-target="#md-custom" type="button" class="btn btn-primary"> <i class="fa fa-plus"></i>Add</button>
                                <button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>
                                <!--<button data-toggle="modal" data-target="#md-custom2" type="button" class="btn btn-warning"> <i class="fa fa-upload"></i>Import</button>-->
                                <!--<button type="submit" id="btnInput" class="btn btn-success"><i class="fa fa-plus"></i>Add</button>-->
                        <!--</div>
                    </div>-->
                    <!--</form>
                    <form role="form">
                        <div class="form-group input-group col-lg-4">


                            <input type="text" class="form-control" id="search" placeholder="MASUKAN NO INVOICE" x-webkit-speech>
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="check"><i class="fa fa-search"></i></button>
                            </span>

                        </div>
                    </form>-->
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Invoice</th>
                                    <th>Data Affiliate</th>
                                    <th>JML</th>
                                    <th>Keterangan</th>
                                    <th>Biaya</th>
                                    <!--<th>Status</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->


<form action="<?php echo base_url(); ?>operasional/claim_biaya" method="post" id="form_detail" >
    <input type="hidden" name="id_booking" id="id_booking">
    <input type="hidden" name="invoice" id="invoice">

</form>

<script>
    function claim_biaya(id)
    {
        if (confirm('Apakah Anda Akan Claim Data Ini ('+id+') '))

        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('operasional/claim_biaya') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    //alert('success');
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                    table.ajax.reload();

                    //  alert('Error deleting data');
                    // window.location.href="<?php //echo site_url('no_access') ?>";
                }
            });

        }
    }



    function delete_paket(id)
    {
        if (confirm('Are you sure delete this data '))
        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('operasional/claim_biaya') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {   //alert('success');
                    //if success reload ajax table
                    //get_data();
                    //reload_table();
                    //window.location.reload(true);
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    //alert('Error deleting data');
                    //window.location.href = "<?php echo site_url('no_access') ?>";
                    table.ajax.reload();
                }
            });

        }
    }
</script>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        

        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('operasional/ajax_list_invoice') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                    //data.filter = $('#filter').val();
                }
            },
        });

        $('#btn-filter').click(function () { //button filter event click
            //var tipe_tanggal = document.getElementById("tipe_tanggal").value;

            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            //var filter = document.getElementById("filter").value;
            table.ajax.url("<?php echo site_url('operasional_cost/json_pindah_paket'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table

        });

        $("#btn-Save").click(function (e) {
            e.preventDefault();
            //alert('test');
            var id = $("#id_sahabat").val();
            var tgl = $("#datepicker3").val();
            $.ajax({
                type: 'POST',
                data: {id_sahabat: id, tgl_pindah: tgl},
                url: "<?php echo site_url('operasional_cost/save'); ?>",
                success: function (data) {
                    //alert(data);
                    $('#md-custom').modal('hide');
                    table.ajax.reload();
                    //window.location.reload(true);
                }
            });
        });

        $("#btn-Update").click(function (e) {
            e.preventDefault();
            //alert('test');
            var id_sahabat = $("#id").val();
            var id = $("#id2").val();
            var tgl = $("#datepicker4").val();
            $.ajax({
                type: 'POST',
                data: {id: id, id_sahabat: id_sahabat, tgl_pindah: tgl},
                url: "<?php echo site_url('operasional_cost/update'); ?>",
                success: function (data) {
                    //alert(data);
                    //
                    $('#md-custom3').modal('hide');
                    table.ajax.reload();
                    //window.location.reload(true);
                }
            });
        });


        /*$("#btn-Import").click(function (e) {
         e.preventDefault();
         var file = $("#txt-file").val();
         //alert(file);
         //var tgl = $("#datepicker3").val();
         $.ajax({
         type: 'POST',
         data: {txt_file : file },
         url: "<?phcp echo site_url('paket/upload_data'); ?>",
         success: function (data) {
         //alert(data);
         //window.location.reload(true);
         }
         });
         });*/
    });

    function edit_data(id) {
        var link_rm = "<?php echo site_url('operasional_cost/get_data') ?>";
        link_rm = link_rm + "/" + id;
        $.get(link_rm, function (data) {
            $('#id').val(data.id_sahabat);
            $('#id2').val(data.id);
            $('#nama').val(data.nama);
            $('#datepicker4').val(data.tgl_pindahpaket);
        }, "json");
        $('#md-custom3').modal('show');
    }
    ;



    function update_data(id, jns_trans) {
        if (confirm('Apakah Anda Akan Proses Biaya Ini '))

        {
            //alert(id+' '+jns_trans);
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('operasional_cost/update_pengajuan') ?>/" + id + "/" + jns_trans,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table

                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                    table.ajax.reload();


                }
            });

        }

    }

    function claim_biaya1(id) {
        //alert(id+' '+jns_trans);
        //$('#id_booking').val(id);
        $('#invoice').val(id);
        $('#form_detail').submit();

        // ajax delete data to database
//            $.ajax({
//                url: "<?php //echo site_url('operasional_cost/detail')  ?>/" + id+"/"+ jns_trans,
//                type: "POST",
//                dataType: "JSON",
//                success: function (data)
//                {
//                    //if success reload ajax table
//
//                    table.ajax.reload();
//                },
//                error: function (jqXHR, textStatus, errorThrown)
//                {
//
//                    table.ajax.reload();
//
//                    
//                }
//            });

    }
</script>