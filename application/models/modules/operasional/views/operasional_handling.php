
<div class="page-head">
    <h2>OPERATIONAL HANDLING</h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">OPERATIONAL</a></li>
        <li class="active">OPERATIONAL HANDLING</li>
    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>DATA OPERATIONAL HANDLING</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form role="form">
                        <div class="form-group input-group col-lg-4">


                            <input type="text" class="form-control" id="search" placeholder="MASUKAN NO INVOICE" x-webkit-speech>
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="check"><i class="fa fa-search"></i></button>
                            </span>

                        </div>
                    </form>           

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <!--<th>NO</ath>-->
                                    <th>INVOICE</ath>
                                    <th>ID USER</ath>
                                    <th>AFFILIATE</ath>
                                    <th>JML</th>
                                    <th>BIAYA</th>
                                    <th>KETERANGAN</th>
                                    <!--<th>TELP</th>
                                    <th>ALAMAT</th>
                                    <th>JML CETAK</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id='response-table'>
                                <tr><td colspan="8"><h2 style="color: #f5b149">Cari Invoice</h2></td></tr>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>      -->                

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<form action="<?php echo base_url(); ?>operasional/claim_biaya_submit" method="post" id="form_detail" >
    <input type="hidden" name="invoice" id="invoice" value="<?php echo isset($invoice) ? $invoice : ''; ?>">

</form>

<script>
    function test(id)
    {
        //alert('test'+id);
    }
    function claim_biaya_submit(id) {
        //alert(id+' '+jns_trans);
        //$('#id_booking').val(id);
        $('#invoice').val(id);
        $('#form_detail').submit();
    }

    function claim_biaya(id)
    {
        if (confirm('Apakah Anda Akan Claim Data Ini (' + id + ') '))

        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('operasional/claim_biaya') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    //alert('success');
                    //table.ajax.reload();
                    //window.location.href="<?phdp echo site_url('operasional/ajax_list_invoice/' + id) ?>";
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                    table.ajax.reload();

                    //  alert('Error deleting data');
                    // window.location.href="<?php //echo site_url('no_access')  ?>";
                }
            });

        }
    }
</script>

<script type="text/javascript">
//    var table;
//    $(document).ready(function () {
//        var invoice = $('#invoice').val();
//        //(invoice);
//        //datatables
//        table = $('#data-table').DataTable({
//            "processing": true, //Feature control the processing indicator.
//             "serverSide": true,
//            "ajax": {
//                "url": "<?phsp echo site_url('operasional/ajax_list_invoice') ?>/" + invoice,
//                "type": "GET",
//                error: function (jqXHR, textStatus, errorThrown){
//                    //table.ajax.reload();
//                    //alert('data is empty');
//                    "processing" : false
//                }
//                //"data": {invoice : invoice }
//            }
//        });
//
//    });
   
</script>

<script>

    $("#check").on('click', function (e) {
        e.preventDefault();
        var search = $("#search").val();
         var departure = $("#departure").val();
        // var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
        // var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
        console.log(search);
        // console.log(departure);
        // console.log(datepicker_tahun_keberangkatan);
        // console.log(datepicker_keberangkatan);

        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>operasional/search_invoice',
            // data:'from='+from+'&to='+to
            data: 's=' + search
                    // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data) {
            $("#response-table").html(data);
        });

    });
</script>









