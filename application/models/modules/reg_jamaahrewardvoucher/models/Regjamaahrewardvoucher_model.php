<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Regjamaahrewardvoucher_model extends CI_Model{
   
   
    private $_table="booking";
    private $_primary="id_booking";

     private $_registrasi_jamaah="registrasi_jamaah";
     private $_id_registrasi="id_registrasi";

     private $_schedule="schedule";
     private $_id_schedule="id_schedule";

  

    private $db2;
    private $refrensi_reward_sahabat="refrensi_reward_sahabat";
  public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }
    function noinvoice(){
     
        $out = array();
        $this->db->select('id_booking,invoice');
        $this->db->from('booking');
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->id_booking] = $value->invoice;
                }
                return $out;
        } else {
            return array();
        }
    }

     public function generate_kode($idx){
        $today=date('ymh');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

   

    public function save($data){
       $initial = "VO";
           
        $arr = array(
                                          
            'id_user_affiliate' => SBL01,
            'id_product' => $data['id_product'],
            'id_schedule' => $data['id_schedule'],
            'id_productprice' => 0,
            'category' => $data['category'],
            'embarkasi' => $data['embarkasi'],
            'schedule' => $data['schedule'].' '.$data['time_schedule'],
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'harga' => 0,
            'create_date' => date('Y-m-d H:i:s'),
            'status_claim_fee'=>0,
            'create_by'=>$this->session->userdata('id_user'),
            'kode_unik'=> $data['unique_digit'],
            'tempjmljamaah'=>$data['jumlah_jamaah'],
            'tipe_jamaah'=>3,
            'status'=>0
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_table,$arr);
        $id_booking =  $this->db->insert_id(); 
        
        $invoice =  $this->db->insert_id(); 
        
        $this->db->update($this->_table,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));


        $arr = array(
            'id_affiliate' => SBL01,                            
            'id_booking' => $id_booking,
            'id_product' => $data['id_product'],
            'id_schedule' => $data['id_schedule'],
            
          
            'create_date' => date('Y-m-d H:i:s'),
            
            'create_by'=>$this->session->userdata('id_user'),
            
            'status'=>0
        );       
        $this->db->insert('transaksi',$arr);
        


        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('reg_jamaahrewardvoucher/detail/'.$id_booking.'');
            return true;

        }
        
        
       
    }

     function cek($no_identitas){
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where no_identitas ='$no_identitas' and id_booking='$id_booking'");
        return $query;
    }
    function cek_idsahabat($id_sahabat){
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where id_sahabat ='$id_sahabat'");
        return $query;
    }
    public function generate_kodejamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function generate_angka_unik($angkax){
            $rand="0123456789";
            
            $ret = '';
            
            $limit = 3;
                    
            for($x=0;$x<($limit- strlen($idx));$x++){
            
                $ret .='0';
            }
            
            return $angkax.$rand; 
        }

     

public function save_registrasi($data){
  $initial = "VO";
 $arr = array(
                'id_booking'=> $data['id_booking'],
                'invoice'=> $data['invoice'],
                'id_schedule'=> $data['id_schedule'],
                'id_affiliate'=> 'SBL01',
                'id_product'=> $data['id_product'],
                'embarkasi'=> $data['embarkasi'],
                'tgl_daftar'=>date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'id_sahabat'=> $data['id_sahabat'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['rentang_umur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi'],
                'kabupaten_id'=> $data['kabupaten'],
                'kecamatan_id'=> $data['kecamatan'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'telp'=> $data['telp_2'],
                'email'=> $data['email'],
                'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'room_type'=> 1,
                'category'=> 1,
                'ket_keberangkatan'=>$data['select_status_hubungan'],
                'merchandise'=> $data['select_merchandise'],
                'muhrim'=> $data['muhrim'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> $data['handling'],
                'akomodasi'=> $data['akomodasi'],
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> 0,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status'=> 0,
                'tipe_jamaah'=>3,
                'kode_unik'=>$data['unique_digit']

        );       
          $this->db->trans_begin(); 
        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_kodejamaah($initial.$id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

       

          $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_jamaah'=> $this->generate_kodejamaah($id_registrasi),
            'no_pasport'=> $data['no_pasport'],
            'issue_office'=> $data['issue_office'],
            'isui_date'=> $data['isue_date'],
            'status_identitas'=> $data['status_identitas'],
            'status_kk'=> $data['status_kk'],
            'status_photo'=> $data['status_photo'],
            'status_pasport'=> $data['status_pasport'],
            'status_vaksin'=> $data['status_vaksin'],
            'hubkeluarga'=> $data['hubungan'],
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 0,
        );       
        $this->db->insert('manifest',$arr);

         $arr = array(
            'id_affiliate' => 'SBL01',                            
            'id_booking' => $data['id_booking'],
            'id_product' => $data['id_product'],
            'id_schedule' => $data['id_schedule'],
            'id_registrasi' =>$id_registrasi,
            'create_date' => date('Y-m-d H:i:s'),
            
            'create_by'=>$this->session->userdata('id_user'),
            
            'status'=>0
        );       
        $this->db->insert('transaksi_jamaah',$arr);

        $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $data['id_booking'],
            
            'status'=>0
        );       
        $this->db->insert('pengiriman',$arr);

     

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $data['id_booking'],
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>0
          );
        $this->db->insert('visa',$arr);

          $sql = "call hitung_muhrim(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
          foreach($query as $key=>$value){
            $biaya_muhrim = $value['muhrim'];
          }


        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  



         $sql = "call v_visa(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();

          foreach($query as $key=>$value){
            $biaya_visa = $value['Total_VISA'];
          }

        $arr = array(
          'visa'=>$biaya_visa,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
}


 public function get_pic($id_booking){
    
          $sql ="SELECT * from registrasi_jamaah WHERE  id_booking  = ? 
              ";
        return $this->db->query($sql,array($id_booking))->result_array();  

}

 public function get_pic_booking($id_booking,$id){
         $stored_procedure = "call view_booking(?,?)";
         return $this->db->query($stored_procedure,array('id_booking'=>$id_booking,
            'create_by'=>$id
            ))->row_array();
    }


      function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }


    function get_biaya_refund() {
        $this->db->select('*');
        $this->db->from('view_refund');
        // $this->db->where('tampil', 'Y');
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function get_allrefund() {
        $this->db->from('view_refund');
        // $this->db->from($this->tabel);
        $query = $this->db->get();
 
        //cek apakah ada data
        if ($query->num_rows() > 0) { //jika ada maka jalankan
            return $query->result();
        }
    }

    public function get_booking($id_booking){
    
        $sql ="SELECT a.*,b.nama as koordinator, c.nama as paket FROM booking a
              LEFT JOIN affiliate b ON b.id_user = a.id_user_affiliate
              LEFT JOIN product c ON c.id_product = a.id_product
              WHERE a.id_booking = ?
              ";
              
        return $this->db->query($sql,array($id_booking))->row_array();    
    }

     public function searchrefrensi(){
  
        // $segment = intval($this->uri->segment(4));
          $sql = "SELECT * from view_schedule "; 
           $query = $this->db->query($sql);
            return $query;

          //   $sql = 'SELECT * from view_schedule  where id_product = '.$paket.' and BulanKeberangkatan='.$datepicker_keberangkatan.' 
          // and TahunKeberangkatan='.$datepicker_tahun_keberangkatan.' and embarkasi='.$departure.'';
          // $query = $this->db->query($sql);
          // return $query;
         
   }

   

    function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
        $jumlah_jamaah = $this->input->post('jumlah_jamaah');
          $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row)
       {    
        if ($row->status_keberangkatan == 6){ 
           echo'<tr>
                   
                   <td>'.$row->paket.'</td>
                   <td>'.$row->departure.'</td>
                   <td>'.$row->date_schedule.'</td>
                   <td>'.$row->time_schedule.'</td>
                   <td>'.$row->seats.'</td>
                   <td>'.$row->type.'</td>
                   <td>'.$row->category.'</td>
                   <td>'.$row->keterangan.'</td>
                   
                   <td><div class="btn-group"><button type="submit" formaction="'.base_url().'reg_jamaahrewardvoucher/schedule/'.$row->id_schedule.'"  class="btn btn-info btn-sm" title="Pilih Waktu Keberangkatan"><i class="glyphicon glyphicon-pencil"></i></a>
                </a></div>
                </td>
                </tr>';
            }else{
                  $jumlah_jamaah = $this->input->post('jumlah_jamaah');
                echo'<tr>
                       
                        <td>'.$row->paket.'</td>
                        <td>'.$row->departure.'</td>
                        <td>'.$row->BulanKeberangkatan.'</td>
                        <td>'.$row->TahunKeberangkatan.'</td>
                        <td>'.$row->seats.'</td>
                        <td>'.$row->type.'</td>
                        <td>'.$row->category.'</td>
                        <td>'.$row->keterangan.'</td>
                        
                        <td><div class="btn-group"><button type="submit" formaction="'.base_url().'reg_jamaahrewardvoucher/schedule/'.$row->id_schedule.'"  class="btn btn-info btn-sm" title="Pilih Waktu Keberangkatan"><i class="glyphicon glyphicon-pencil"></i></a>
                </a></div>
                </td>
                </tr>';
            }

       }

       }
   }

   function get_keluarga()  {
        
        $query = $this->db->get('registrasi_jamaah');
        return $query->result();
        
        }
   function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    
    function get_kabupaten() {
    
    $query = $this->db->get('wilayah_kabupaten');
    return $query->result();
    
    }
    
    function get_kecamatan()    {
    
    $query = $this->db->get('wilayah_kecamatan');
    return $query->result();
    
    }


     function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }


    function get_nama_jamaah() {
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status', '0');
        $this->db->order_by('id_registrasi', 'ASC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function get_data_jamaah($id) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }
     
    


     function lap_data_perjamaah($id_jamaah) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_jamaah',$id_jamaah);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }
    
    function get_all_provinsi_manasik() {
        $this->db->distinct();
        $this->db->select('provinsi_id,provinsi');
        $this->db->from('view_manasik');
        $query = $this->db->get();
        
        return $query->result();
    }

    function refrensi_reward_sahabat() {
        $this->db2->from($this->refrensi_reward_sahabat);
        $query = $this->db2->get();
 
        //cek apakah ada data
        if ($query->num_rows() > 0) { //jika ada maka jalankan
            return $query->result();
        }
    }

   public function get_report_registrasi($id_booking,$id){
 
        // $id_user=  $this->session->userdata('id_user');
        //  $sql = "SELECT * from data_laporan_jamah_belumaktif_all where id_booking = {$id_booking} ";
  
        // return $this->db->query($sql)->row_array();  

      $stored_procedure = "call data_laporan_jamah_belumaktif_all(?,?)";
         return $this->db->query($stored_procedure,
            array('id_booking'=>$id_booking,
                    'create_by'=>$id
            ))->row_array(); 
    }
}
