<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Generatedata_model extends CI_Model{
   
   
   


    private $_booking="booking";
    private $_id_booking="id_booking";
    private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi_jamaah="id_registrasi";
    public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }

    public function generate_kode($idx){
        $today=date('ymd');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }


   
    public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save_mutasi($data){
      $harga_kuota = $this->input->post('harga');
      $schedule = $this->input->post('radio');
      $initial = "P1";
         $stored_procedure = "CALL schedul_jamahpromo(?)";
             $query =  $this->db->query($stored_procedure,array('id_schedule'=>$schedule))->result_array();
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
      }



      $stored_procedure = "CALL all_refrensi_fee(?)";
             $query =  $this->db->query($stored_procedure,array('id_user'=>$data['id_user']))->result_array();

            
              foreach($query as $key=>$value){
                $sponsor = $value['id_refrensi'];
                $fee_sponsor_paketpromo  = $value['fee_sponsor_paketpromo'];
                $fee_posting_paketpromo = $value['fee_posting_paketpromo'];
            }
  $arr = array(
                                   
            'id_user_affiliate' => $data['id_user'],
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'harga' => 0,
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'kode_unik'=> 0,
           
            'tipe_jamaah'=>6,
            'status'=>0
        );       
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));


        $jml_seats = $this->input->post('jml_seats');
        for($i=0; $i < $jml_seats; $i++) { 

     
          $arr = array(
                'id_booking'=> $id_booking,
                'invoice'=> $this->generate_kode($initial.$id_booking),
                'id_schedule'=> $id_schedule,
                'id_affiliate'=> $data['id_user'],
                'id_product'=> $id_product,

                'embarkasi'=> $embarkasi,
                 'tgl_daftar'=>date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
                'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'no_pasport'=> $data['no_pasport'],
                'room_type'=> 1,
                'category'=> $room_category,
                'issue_office'=> $data['issue_office'],
                'isui_date'=> $data['isue_date'],
                'merchandise'=> $data['select_merchandise'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> 0,
                'akomodasi'=> $data['akomodasi'],
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> $harga,
                'id_status_jamaah'=> 1,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status_identitas'=> $data['status_identitas'],
                'status_kk'=> $data['status_kk'],
                'status_photo'=> $data['status_photo'],
                'status_pasport'=> $data['status_pasport'],
                'status_vaksin'=> $data['status_vaksin'],
                'status_buku_nikah'=>$data['status_buku_nikah'],
                'pic1' => isset($data['pic1']) ? $data['pic1']: '',
                'pic2' => isset($data['pic2']) ? $data['pic2']: '',
                'pic3' => isset($data['pic3']) ? $data['pic3']: '',
                'pic4' => isset($data['pic4']) ? $data['pic4']: '',
                'pic5' => isset($data['pic5']) ? $data['pic5']: '',
                'pic6' => isset($data['pic6']) ? $data['pic6']: '',
                'status'=> 1,
                'tipe_jamaah'=>6,
                'kode_unik'=>0,
                'status_fee'=>0,

        );       
          $this->db->trans_begin(); 
        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

      
      
         $arr = array(    

                  'invoice' => $this->generate_kode($initial.$id_booking),
                  'id_registrasi' => $id_registrasi,
                  'id_booking'=> $id_booking,
                  'id_schedule'=> $id_schedule,
                  'id_product'=> $id_product,
                  'id_affiliate' => $data['id_user'],
                  'sponsor' => $sponsor,
                  'fee' => $fee_posting_paketpromo,
                  'fee_sponsor' => $fee_sponsor_paketpromo,
                  'create_by'=>$this->session->userdata('id_user'),
                  'create_date'=>date('Y-m-d H:i:s'),
                  'status' => 1,
                   'status_fee'=> 1,
                 'status_fee_sponsor'=> 1,
              );

              $this->db->insert('fee',$arr);
        
        $arr = array(    
            
            'id_registrasi' => $id_registrasi,
            'id_booking'=> $id_booking,
            'id_schedule'=> $id_schedule,
            'id_product'=> $id_product,
            'id_affiliate' => $data['id_user'],
            'create_by'=>$this->session->userdata('id_user'),
            'create_date'=>date('Y-m-d H:i:s'),
            'status'=> 1

        );

        $this->db->insert('list_gaji',$arr);

   

         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $id_booking,
            
            'status'=>1
        );       
        $this->db->insert('pengiriman',$arr);

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $id_booking,
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>1
          );
        $this->db->insert('visa',$arr);

    $sql = "call hitung_muhrim(?) ";
                     $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
                    foreach($query as $key=>$value){
                      $biaya_muhrim = $value['muhrim'];
                    }


        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 


           $sql = "call v_visa(?) ";
                 $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();
                    foreach($query as $key=>$value){
                      $biaya_visa = $value['Total_VISA'];
                    }

          $arr = array(
            'visa'=>$biaya_visa,
           
          );    
            $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 
            




       

       }

         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
             print_r('sukses'.$this->generate_kode($initial.$id_booking).'');
            return true;
        }
    }


    function cek($userid){
        // $userid = $this->input->post('userid');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where id_sahabat ='$userid' and tipe_jamaah ='2' ");
        return $query;
    }

    
    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

 

    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = "SELECT * from refrensi_reward_sahabat where 1=1
                    ";
        
        if($q){
         

            $sql .=" AND userid LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR datewin LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY datewin DESC ";
        $ret['total'] = $this->db2->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db2->query($sql)->result();
       
        return $ret;
        
    }


function searchItem($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure,$paket=''){
     
        
       $paket = $this->input->post('paket');
     // if ($paket=='4') {

          $sql = "SELECT * from view_schedule_promo where  BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>

                
                 
                   
                  <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }


    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

     function get_all_provinsi_manasik() {
        $this->db->distinct();
        $this->db->select('provinsi_id,provinsi');
        $this->db->from('view_manasik');
        $query = $this->db->get();
        
        return $query->result();
    }

     public function get_report_registrasi($id_booking){
 
        
         $sql = "SELECT * from data_laporan_jamah_belumaktif_all where id_booking = '$id_booking' ";
  
        return $this->db->query($sql)->row_array();  
    }

        function getaffiliatetype(){
        $this->db->select('id_affiliate_type,affiliate_type');
        $this->db->from('affiliate_type');
        $this->db->order_by('id_affiliate_type', 'asc'); 
        $query=$this->db->get();
        return $query; 
    }
}
