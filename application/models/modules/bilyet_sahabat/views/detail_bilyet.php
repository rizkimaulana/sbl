
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form" method="POST" action="<?php echo base_url();?>bilyet_sahabat/add_bilyet" >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List BILYET REWARD</b>
                </div>

                 <div class="panel-body">
                        <?php if($this->session->flashdata('success')) { ?>
                                  <div class="alert alert-success">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('success'); ?>  
                                  </div>
                  <?php } ?>
                 <span>
                     <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Add Bilyet</button>
                 </span>
                   <!-- <input type="text" class="form-control" id="id_bilyet" name="id_bilyet" value="<?php echo $detail['id_bilyet'];?>" readonly="treu" /> -->
                   <input type="hidden" class="form-control" id="userid" name="userid" value="<?php echo $detail['userid'];?>" readonly="treu" />

                   <input type="hidden" class="form-control" id="peringkat" name="peringkat" value="<?php echo $detail['peringkat'];?>" readonly="treu" />

                    <input type="hidden" class="form-control" id="kelas" name="kelas" value="<?php echo $detail['kelas'];?>" readonly="treu" />
                     <input type="hidden" class="form-control" id="harga" name="harga" value="<?php echo $detail['harga'];?>" readonly="treu" />
                      <input type="hidden" class="form-control" id="id_groupbilyet" name="id_groupbilyet" value="<?php echo $detail['id_groupbilyet'];?>" readonly="treu" />
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>USER ID`</th>
                                    <th>ID BILYET </th>
                                    <th>PERINGKAT </th>
                                    
                                    <th>TGL KLAIM</th>
                                    <th>TOTAL CETAK</th>
                                    <th>TGL MUTASI</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($get_bilyet as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $pi['userid']?>  </td>
                                      <td><?php echo $pi['id_bilyet']?> </td>
                                      <td><?php echo $pi['peringkat']?>  </td> 
                                      <td><?php echo $pi['tgl_klaim']?>  </td>
                                      <td><?php echo $pi['total_cetak']?>  </td>
                                     
                                      <td><?php echo $pi['tgl_mutasi']?>  </td>
                                    <td>
                                        <div class="btn-group">
                                            <!-- <a class="btn-sm btn-primary" title='Edit' target="_blank" href="<?php echo base_url();?>bilyet_sahabat/cetak_bilyet/<?php echo $pi['id_bilyet'];?>">CETAK</a> -->
                                      <?php  if ($pi['total_cetak'] > 0) {?>
                                            <a class="btn-sm btn-success" title='Edit' target="_blank" href="<?php echo base_url();?>bilyet_sahabat/bilyet/<?php echo $pi['id_bilyet'];?>">CETAK</a>
                                      <?php } else { ?>
                                            <a class="btn-sm btn-danger" title='Edit' target="_blank" href="<?php echo base_url();?>bilyet_sahabat/bilyet/<?php echo $pi['id_bilyet'];?>">CETAK</a>
                                    <?php } ?>
                                    <?php  if ($pi['tgl_skrng'] >= $pi['tgl_mutasi']) {?>
                                    <a class="btn-sm btn-primary" title='MUTASI'  href="<?php echo base_url();?>bilyet_sahabat/mutasi_data_jamaah/<?php echo $pi['id_bilyet'];?>">MUTASI</a> 
                                     <?php } else { ?>
                                      <a class="btn-sm btn-danger" title='MUTASI'  href="#">MUTASI</a> 
                                      <?php } ?>
                                        </div> 
                                                                                         
                                  </td>  

                                   <!-- <td>
                                        <div class="btn-group">
                                          <a class="btn-sm btn-primary" title='MUTASI' target="_blank" href="<?php echo base_url();?>bilyet_sahabat/mutasi_data_jamaah/<?php echo $pi['id_bilyet'];?>">MUTASI</a> 
                                     
                                         </div> 
                                                                                         
                                  </td>   -->
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
