<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bilyetsahabat_model extends CI_Model{
   
   
   

    private $db2;
    private $_table="t_group_bilyetprestasi";
    private $_primary="id_groupbilyet";

    private $_t_bilyet_reward="t_bilyet_reward";
    private $_booking="booking";
    private $_id_booking="id_booking";

    private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi_jamaah="id_registrasi";
    public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }

    public function generate_kode($idx){
        $today=date('ymd');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }
public function save_generate_old($data){
    $userid = $this->input->post('userid');
    $peringkat = $this->input->post('kelas');
    $sql = "SELECT * from t_bilyet_ref where peringkat = '".$peringkat."' 
         ";
    $query = $this->db2->query($sql)->result_array();

    if(count($query) > 0){
      foreach($query as $key=>$value){
         $kelas = $value['kelas'];
        $sisa = $value['sisa'];
        $jumlah = $value['jumlah'];
        $arr = array(
                                          
            'userid' => $this->input->post('userid'),
            'harga' => $this->input->post('harga'),
            'peringkat' => $this->input->post('kelas'),
            'kelas'=>$kelas,
            'sisa'=>$sisa,
            'status'=>0,
            'create_by'=>$this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s'),
            
        );       
        $this->db2->insert('t_group_bilyetprestasi', $arr);
        $id_groupbilyet =  $this->db2->insert_id(); 

               for($i=0; $i < $jumlah; $i++) {
                    $arr = array(
                        
                        'id_bilyet' => $this->input->post('userid').''.($peringkat).'-'.($i + 1),
                        'id_groupbilyet'=>$id_groupbilyet,
                        'userid' => $this->input->post('userid'),
                        'peringkat' => $this->input->post('kelas'),
                        'kelas'=>$kelas,
                        'harga' => $this->input->post('harga'),
                         'tgl_klaim' => date('Y-m-d H:i:s'),
                         // 'tgl_wd' => date('Y-m-d H:i:s'),
                         'status'=>0,
                       );
                     $this->db2->insert('t_bilyet_prestasi', $arr);

               }

    }
}

         if($this->db2->trans_status()===false){
            
            $this->db2->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db2->trans_complete();
            
            return true;
        }
    }

public function save($data){

        $userid = $this->input->post('userid');
        $peringkat_reward = $this->input->post('peringkat_reward');
        // $jml_bilyet = $this->input->post('jml_bilyet');
        $jumlah = $this->input->post('jml_bilyet');

        $arr = array(
                                          
            'userid' => $this->input->post('userid'),
            'harga' => $this->input->post('harga'),
            'peringkat' => $this->input->post('peringkat_reward'),
            // 'kelas'=>$kelas,
            // 'sisa'=>$sisa,
            'status'=>0,
            'create_by'=>$this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s'),
            
        );       
        $this->db2->insert('t_group_bilyetprestasi', $arr);
        $id_groupbilyet =  $this->db2->insert_id(); 

               for($i=0; $i < $jumlah; $i++) {
                    $arr = array(
                        
                        'id_bilyet' => $this->input->post('userid').''.($peringkat_reward).'-'.($i + 1),
                        'id_groupbilyet'=>$id_groupbilyet,
                        'userid' => $this->input->post('userid'),
                        'peringkat' => $this->input->post('peringkat_reward'),
                        // 'kelas'=>$kelas,
                        'harga' => $this->input->post('harga'),
                         'tgl_klaim' => date('Y-m-d H:i:s'),
                         // 'tgl_wd' => date('Y-m-d H:i:s'),
                         'status'=>0,
                       );
                     $this->db2->insert('t_bilyet_prestasi', $arr);

               }

    
    $arr = array(
                                          
            'status' => 1,
            'updated_date' => date('Y-m-d H:i:s'),
            'updated_by'=>$this->session->userdata('id_user'),
            
        );       
        $this->db->update('t_req_bilyet',$arr,array('userid'=>$data['userid']));



         if($this->db2->trans_status()===false){
            
            $this->db2->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db2->trans_complete();
            
            return true;
        }
    }

    public function save_cetak($data){
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            'id_bilyet' => $data['id_bilyet'],
            'id_sahabat' => $data['id_sahabat'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['select_user'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
        
     
         $this->db2->trans_begin(); 
        
        $this->db2->insert($this->_t_bilyet_reward,$arr);
        // $id_jamaah =  $this->db->insert_id(); 

           if($this->db2->trans_status()===false){
            
            $this->db2->trans_rollback();
            return false;    
            
        }else{
            
            $this->db2->trans_complete();


            return true;

        }
    }
   public function update_cetak($data){
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            'id_bilyet' => $data['id_bilyet'],
            'id_sahabat' => $data['id_sahabat'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['select_user'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
        
     
         $this->db2->update('t_bilyet_reward',$arr,array('id_bilyet'=>$data['id_bilyet']));

        if($this->db2->trans_status()===false){
            
            $this->db2->trans_rollback();
            return false;    
            
        }else{
            
            $this->db2->trans_complete();

            return true;

        }
    }
    
    public function add_bilyet($data){

    $userid = $this->input->post('userid');
    $peringkat = $this->input->post('kelas');
    $harga = $this->input->post('harga');
    $id_groupbilyet = $this->input->post('id_groupbilyet');

    $sql = " SELECT COUNT(*) as jmlbilyet from t_bilyet_prestasi where userid='".$userid."'  
         ";
    $query = $this->db2->query($sql)->result_array();

    if(count($query) > 0){
      foreach($query as $key=>$value){
        $jmlbilyet = $value['jmlbilyet'];
        $kelas = $value['kelas'];
        $arr = array(
                                          
            'id_bilyet' => $this->input->post('userid').''.($peringkat).'-'.($jmlbilyet + 1),
            'id_groupbilyet'=> $this->input->post('id_groupbilyet'),
            'userid' => $this->input->post('userid'),
            'peringkat' => $this->input->post('peringkat'),
            'kelas' => $this->input->post('kelas'),
            'harga' => $this->input->post('harga'),
             'tgl_klaim' => date('Y-m-d H:i:s'),
             // 'tgl_wd' => date('Y-m-d H:i:s'),
             'status'=>0,
            
        );       
        $this->db2->insert('t_bilyet_prestasi', $arr);
        // $id_groupbilyet =  $this->db2->insert_id(); 

        //        for($i=0; $i < $jumlah; $i++) {
        //             $arr = array(
                        
        //                 'id_bilyet' => $this->input->post('userid').''.($kelas).'-'.($i + 1),
        //                 'id_groupbilyet'=>$id_groupbilyet,
        //                 'userid' => $this->input->post('userid'),
        //                 'kelas' => $this->input->post('kelas'),
        //                 'harga' => $this->input->post('harga'),
        //                  'tgl_klaim' => date('Y-m-d H:i:s'),
        //                  // 'tgl_wd' => date('Y-m-d H:i:s'),
        //                  'status'=>0,
        //                );
        //              $this->db2->insert('t_bilyet_prestasi', $arr);

               }

    }
}
    public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save_mutasi($data){
     $schedule = $this->input->post('radio');
      $harga_kuota = $this->input->post('harga');
      $initial = "R";
        $sql = "call schedule_reguler(?) ";
        $query = $this->db->query($sql,array('id_schedule'=>$schedule))->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $date_schedule = $value['date_schedule'];
      }
       $arr = array(
                                   
            'id_user_affiliate' => $data['userid'],
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'harga' => $harga_kuota,
            'create_date' => date('Y-m-d H:i:s'),
             // 'payment_date'=>$data['payment_date'],
            'create_by'=>$this->session->userdata('id_user'),
            // 'kode_unik'=> $data['kode_unik'],
           
            'tipe_jamaah'=>5,
            'status'=>1
        );       
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));

         $arr = array(
                'id_booking'=> $id_booking,
                'id_schedule'=> $id_schedule,
                'id_affiliate'=> $data['userid'],
                'invoice'=> $this->generate_kode($initial.$id_booking),
                'id_product'=> $id_product,
                'embarkasi'=> $embarkasi,
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'id_sahabat' => $data['idsahabat'],
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'telp_2'=> $data['telp_2'],
                'email'=> $data['email'],
                'ket_keberangkatan'=>9,
                'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'room_type'=> 1,
                'category'=> $room_category,
                'merchandise'=> $data['select_merchandise'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> 0,
                'akomodasi'=> $data['akomodasi'],
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> $harga_kuota,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status'=> 1,
                'tipe_jamaah'=>5,
                 'update_date' => date('Y-m-d H:i:s'),
        );     

        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 
       
        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

    $arr = array(
          'id_booking'=> $id_booking,
          'id_registrasi'=> $id_registrasi,
          'id_jamaah'=> $this->generate_id_jamaah($id_registrasi),
          'id_affiliate'=> $data['userid'],
          'no_pasport'=> $data['no_pasport'],
          'issue_office'=> $data['issue_office'],
          'isui_date'=> $data['isue_date'],
          'status_identitas'=> $data['status_identitas'],
          'status_kk'=> $data['status_kk'],
          'status_photo'=> $data['status_photo'],
          'status_pasport'=> $data['status_pasport'],
          'status_vaksin'=> $data['status_vaksin'],
          'hubkeluarga'=> $data['hubungan'],
          'create_by'=>$this->session->userdata('id_user'),
          'create_date'=>date('Y-m-d H:i:s'),
          'status'=> 1,
      );       
      $this->db->insert('manifest',$arr);

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>1
          );
        $this->db->insert('visa',$arr);

       

        $arr = array(
            'id_bilyet' => $data['id_bilyet'],
            'id_groupbilyet'=>$data['id_groupbilyet'],
            'id_jamaah'=> $this->generate_id_jamaah($id_registrasi),
            'userid' => $data['userid'],
            'peringkat' => $data['peringkat'],
            'kelas' => $data['kelas'],
            'harga' => $data['harga'],
             'tgl_klaim' => $data['tgl_klaim'],
             'tgl_wd' => date('Y-m-d H:i:s'),
             'status'=>1,
        ); 

         $this->db->insert('claim_bilyet_prestasi',$arr);

          $arr = array(

            'status' => 1,
            'tgl_wd' => date('Y-m-d H:i:s'),
            
        );       
         $this->db2->update('t_bilyet_prestasi',$arr,array('id_bilyet'=>$data['id_bilyet']));

        $arr = array(

            'status' => 0,
            'update_date' => date('Y-m-d H:i:s'),
            
        );       
         $this->db2->update('t_group_bilyetprestasi',$arr,array('userid'=>$data['userid']));

           $sql = "call hitung_muhrim(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();

          foreach($query as $key=>$value){
            $biaya_muhrim = $value['muhrim'];
          }

        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));  


        $sql = "call v_visa(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();

          foreach($query as $key=>$value){
            $biaya_visa = $value['Total_VISA'];
          }

        $arr = array(
          'visa'=>$biaya_visa,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));   

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();

            return true;
        }

       

        
    }

    function cek_data_reward($userid,$kelas){
        $userid = $this->input->post('userid');
        $kelas = $this->input->post('peringkat_reward');
        $query = $this->db2->query("SELECT * FROM t_reward_member Where userid ='$userid' and peringkat='$kelas'");
        return $query;
    }

    function cek_bilyet($userid,$kelas){
        $userid = $this->input->post('userid');
        $kelas = $this->input->post('peringkat_reward');
        $query = $this->db2->query("SELECT * FROM t_bilyet_prestasi Where userid ='$userid' and peringkat='$kelas'");
        return $query;
    }


    function cek_data_reward_old($userid,$kelas){
        $userid = $this->input->post('userid');
        $kelas = $this->input->post('kelas');
        $query = $this->db2->query("SELECT * FROM t_reward_member Where userid ='$userid' and peringkat='$kelas'");
        return $query;
    }

    function cek_bilyet_old($userid,$kelas){
        $userid = $this->input->post('userid');
        $kelas = $this->input->post('kelas');
        $query = $this->db2->query("SELECT * FROM t_bilyet_prestasi Where userid ='$userid' and peringkat='$kelas'");
        return $query;
    }
    
    function cek_cetak_bilyet($userid,$kelas){
        $id_bilyet = $this->input->post('id_bilyet');
        $id_sahabat = $this->input->post('id_sahabat'); 
        $query = $this->db2->query("SELECT * FROM t_bilyet_reward Where id_bilyet ='$id_bilyet' and id_sahabat='$id_sahabat'");
        return $query;
    }
  
    function cek($no_identitas){
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where no_identitas ='$no_identitas' and id_booking='$id_booking'");
        return $query;
    }

    
    function get_key_val() {

        $out = array();
        $this->db2->select('id,nama,harga');
        $this->db2->from('t_bilyet_prestasi_harga');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db2->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->nama] = $value->harga;
                }
                return $out;
        } else {
            return array();
        }
    }

  function get_key_val_konven() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

     public function get_bilyet($id_groupbilyet){
    
        $sql ="SELECT * from detail_bilyet_reward WHERE  id_groupbilyet  = ? order by id
              ";
        return $this->db2->query($sql,array($id_groupbilyet))->result_array();    

    }

 public function get_detail_bilyet($id_groupbilyet){
    
       $sql = "SELECT * from detail_bilyet_reward WHERE  id_groupbilyet = '$id_groupbilyet' GROUP BY userid
                ";
       
        return $this->db2->query($sql)->row_array();


    }
    public function cetak_bilyet($id_bilyet){
 

         $sql = "SELECT * from detail_bilyet_reward where id_bilyet = '$id_bilyet'";
  
        return $this->db2->query($sql)->row_array();  
    }

    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = " SELECT * FROM cetak_bilyet_reward  
                    WHERE 1=1
                    ";
        
        if($q){
         

            $sql .=" AND userid LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR kelas LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY create_date DESC ";
        $ret['total'] = $this->db2->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db2->query($sql)->result();
       
        return $ret;
        
    }

    public function get_data_generate_bilyet_prestasi($offset,$limit,$q=''){
    
 
         $sql = " SELECT * FROM generate_bilyet_prestasi  
                    WHERE 1=1
                    ";
        if($q){
         

            $sql .=" AND userid LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR ket LIKE '%{$q}%'
                    OR created_date LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY created_date DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
      
          $sql = "SELECT * from view_schedule_admin where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.'</strong></td>
                   <td><strong>'.$row->keterangan.'</strong></td>
                 
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }


    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

     function get_all_provinsi_manasik() {
        $this->db->distinct();
        $this->db->select('provinsi_id,provinsi');
        $this->db->from('view_manasik');
        $query = $this->db->get();
        
        return $query->result();
    }



     public function get_pic_generate_bilyet($userid){

         $sql = "SELECT * from generate_bilyet_prestasi where userid = '$userid'";
        return $this->db->query($sql)->row_array();  

    }

    public function get_bilyet_generate_bilyet($userid){
    
        $sql ="SELECT * from generate_bilyet_prestasi WHERE  userid  = ?
              ";
        return $this->db->query($sql,array($userid))->result_array();    

    }


        public function get_data_refrensi_reward($offset,$limit,$q=''){
    
 
         $sql = "SELECT * from refrensi_reward_dua where 1=1
                    ";
        
        if($q){
         

            $sql .=" AND userid LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR reward LIKE '%{$q}%'
                    OR datewin LIKE '%{$q}%'
                    OR admin LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY datewin DESC ";
        $ret['total'] = $this->db2->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db2->query($sql)->result();
       
        return $ret;
        
    }

    public function insert_reward($userid){
        $peringkat = $this->uri->segment(5);

         $sql = "SELECT * from refrensi_reward_dua where userid = '$userid' and peringkat='".$peringkat."'";
        return $this->db2->query($sql)->row_array();  

    }

    public function peringkat($peringkat){
        $peringkat = $this->uri->segment(5);

         $sql = "SELECT * from t_bilyet_ref where peringkat = '$peringkat' ";
        return $this->db2->query($sql)->row_array();  

    }

    public function save_insert_reward($data){
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            'userid'=>$data['userid'],
            'peringkat_reward'=>$data['peringkat_reward'],
            'jml_bilyet'=>$data['jml_bilyet'],
            'created_date'=>$data['datewin'],
            'created_by'=>$this->session->userdata('username'),
            'id_transaksi'=>0,
            'status'=>0,
        );       
         $this->db->insert('t_req_bilyet', $arr);

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

    function cek_reward($userid,$peringkat_reward){
        $userid = $this->input->post('userid');
        $kelas = $this->input->post('peringkat_reward');
        $query = $this->db->query("SELECT * FROM t_req_bilyet Where userid ='$userid' and peringkat_reward='$kelas'");
        return $query;
    }
    
}
