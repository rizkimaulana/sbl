 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add PRODUCT PRICE</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>product_price/save">
            
            <div class="form-group">
                <label>Pilih Product</label>
                <select class="form-control" name="kode_product">
                    <option></option>
                    <?php foreach($select_product as $sp){?>
                        <option required value="<?php echo $sp->kode;?>"><?php echo $sp->nama;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Hotel</label>

                <select class="form-control" name="room_category">
                    <option></option>
                    <?php foreach($select_roomcategory as $se){?>
                        <option value="<?php echo $se->id_room_category;?>"><?php echo $se->category;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Embarkasi</label>
                <select class="form-control" name="embarkasi">
                    <option></option>
                    <?php foreach($select_embarkasi as $se){?>
                        <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Bulan Menunggu</label>
                <!-- <input class="form-control" name="bulan" required> -->
                <select class="form-control" name="bulan">
                     <option></option>
                   <?php foreach($bulan as $sp){?>
                        <option required value="<?php echo $sp->bulan;?>"><?php echo $sp->bulan;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Lama Hari</label>
                <!-- <input class="form-control" name="hari" required> -->
                <select class="form-control" name="select_hari">
                     <option></option>
                     <?php foreach($select_hari as $sp){?>
                        <option required value="<?php echo $sp->hari;?>"><?php echo $sp->hari;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Destination</label>
                <!-- <input class="form-control" name="hari" required> -->
                <select class="form-control" name="select_detination">
                     <option></option>
                     <?php foreach($select_detination as $sp){?>
                        <option required value="<?php echo $sp->destination_id;?>"><?php echo $sp->destination;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input class="form-control" name="harga" required>
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" name="keterangan" >
            </div>
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>product_price" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
