<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_price extends CI_Controller{
	var $folder = "product_price";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DATA_PRODUCT,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','data_product');	
		$this->load->model('productprice_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/product_price');
		
	}

	private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}
	private function _select_product(){
		$status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}
	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	private function _bulan(){
	    
	    return $this->db->get('bulan')->result();
	}

	private function _select_hari(){
	    
	    return $this->db->get('hari')->result();
	}

	private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}


	private function _select_detination(){
	
	    return $this->db->get('destinations')->result();
	}

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->productprice_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="20%">'.$r->nama.'</td>';
	                $rows .='<td width="8%">'.$r->hotel.'</td>';
	                $rows .='<td width="8%">'.$r->departure.'</td>';
	                $rows .='<td width="8%">'.$r->bulan.'</td>';
	                $rows .='<td width="4%">'.$r->hari.'</td>';
	                $rows .='<td width="10%">'.number_format($r->harga).'</td>';
	                $rows .='<td width="10%">'.$r->keterangan.'</td>';
	                $rows .='<td width="9%">'.$r->username.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'product_price/edit/'.$r->id_productprice.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_productprice('."'".$r->id_productprice."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'product_price/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->productprice_model->save($data);
	     if($send)
	        redirect('product_price');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(PRODUCT_PRICE,'add'))
		    $this->general->no_access();

	    
	    $data = array(
	    	
	    	'select_product'=>$this->_select_product(),
	    	'select_embarkasi'=>$this->_select_embarkasi(),
	    	'select_roomcategory'=>$this->_select_roomcategory(),
	    	'bulan'=>$this->_bulan(),
	    	'select_hari'=>$this->_select_hari(),
	    	'select_detination'=>$this->_select_detination()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(PRODUCT_PRICE,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('product_price',array('id_productprice'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   $data = array(
	   		'select_bulan'=>$this->_bulan(),
	    	'select_hari'=>$this->_select_hari(),
	    	'select_embarkasi'=>$this->_select_embarkasi(),
	    	'select_roomcategory'=>$this->_select_roomcategory(),
	    	'select_product'=>$this->_select_product(),
	    	'select_detination'=>$this->_select_detination()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->productprice_model->update($data);
	     if($send)
	        redirect('product_price');
		
	}
	
	public function delete(){
	    
	     if(!$this->general->privilege_check(PRODUCT,'remove'))
		    $this->general->no_access();
		    
	     $id  = $this->uri->segment(4);
	     $send = $this->product_model->delete($id);
	     if($send)
	        redirect('product');
	}
	
	public function ajax_delete($id_productprice)
	{
		if(!$this->general->privilege_check(PRODUCT_PRICE,'remove'))
		    $this->general->no_access();
		$send = $this->productprice_model->delete_by_id($id_productprice);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('product_price');
	}


}
