<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Pindah Paket</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form name="form" role="form" action="<?php echo base_url(); ?>paket/export_pindah_paket" method="POST"  class="form-horizontal">
                        <div class="form-group">
                            <!--<div class="col-sm-2">
                                
                                <select name="filter" id="filter" class="form-control">
                                    <option value="0" selected="selected">Select Filter</option>
                                    <option value="1">Create Date</option>
                                </select>
                            </div>-->
                            <div class="col-sm-2">

                                <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" name="btn" value="filter" id="btn-filter" class="btn btn-primary">Filter</button>
                                <!--<button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <button data-toggle="modal" data-target="#md-custom" type="button" class="btn btn-primary"> <i class="fa fa-plus"></i>Add</button>
                                <button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>
                                <!--<button data-toggle="modal" data-target="#md-custom2" type="button" class="btn btn-warning"> <i class="fa fa-upload"></i>Import</button>-->
                                <!--<button type="submit" id="btnInput" class="btn btn-success"><i class="fa fa-plus"></i>Add</button>-->
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Id Sahabat</th>
                                    <!--<th>Nama Jamaah</th>-->
                                    <th>Tgl Pindah</th>
                                    <th>Status</th>
                                    <th>Pilih</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->

<!-- Modal -->
<div class="modal fade" id="md-custom" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form name="form1" class="form-horizontal" method="POST">

                    <div class="block-flat">
                        <div class="header">              
                            <h3>Pindah Paket</h3>
                        </div>
                        <div class="content">

                            <div class="form-group">
                                <label for="id_sahabat" class="col-sm-3 control-label">ID Sahabat</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="id_sahabat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="datepicker3" class="col-sm-3 control-label">Tgl. Pindah</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control " name="tgl_pindah" id="datepicker3" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-primary" id="btn-Save">Save</button>
                                    <button class="btn btn-default" id="btn-Cancel">Cancel</button>
                                </div>
                            </div>

                        </div>
                    </div>        
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--<form name="1" action="<?phdp echo base_url();?>paket/edit" method="POST">
    <input type="hidden" id ="edit" name="edit" value="-1">
</form>

<script>
    
    function formSubmit(id)
    {
      document.forms[1].id.value = id;
      document.forms[1].submit();
    }
</script>-->

<!-- Modal -->
<div class="modal fade" id="md-custom2" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form name="form2" action="<?php echo base_url();?>paket/upload_data" class="form-horizontal" method="POST" enctype="multipart/form-data">

                    <div class="block-flat">
                        <div class="header">              
                            <h3>Import Data</h3>
                        </div>
                        <div class="content">

                            <div class="form-group">
                                <label for="id_sahabat" class="col-sm-3 control-label">File Name</label>
                                <div class="col-sm-6">
                                    <input type="file" class="form-control" id="txt-file" name="txt_file">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="datepicker3" class="col-sm-3 control-label">Tgl. Pindah</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control " name="tgl_pindah" id="datepicker3" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="">Upload</button>
                                    <button data-dismiss="modal" type="button" class="btn btn-default" id="btn-Cancel2">Cancel</button>
                                </div>
                            </div>

                        </div>
                    </div>        



                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="md-custom3" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form name="form3" class="form-horizontal" method="POST">

                    <div class="block-flat">
                        <div class="header">              
                            <h3>Edit Pindah Paket</h3>
                        </div>
                        <div class="content">

                            <div class="form-group">
                                <label for="id_sahabat" class="col-sm-3 control-label">ID Sahabat</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="id">
                                    <input type="hidden" class="form-control" id="id2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="id_sahabat" class="col-sm-3 control-label">Nama Jamaah</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama" readonly="readonly" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="datepicker3" class="col-sm-3 control-label">Tgl. Pindah</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control " name="tgl_pindah" id="datepicker4" placeholder="yyyy-mm-dd " required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-primary" id="btn-Update">Save</button>
                                    <button data-dismiss="modal" class="btn btn-default" id="btn-Cancel">Cancel</button>
                                </div>
                            </div>

                        </div>
                    </div>        
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
/*
    function get_data(url, q) {

        if (!url)
            url = base_url + 'paket/get_data';

        $.ajax({

            url: url, type: 'post', dataType: 'json',
            data: {q: q},
            success: function (result) {

                $("#data-table tbody").html(result.rows);
                $("ul.pagination").html(result.paging);
                $(".page-info").html(result.page_info);
            }

        });
    }
    function do_search() {


        get_data('', $("#search").val());

    }
    $(function () {

        get_data();//initialize

        $(document).on('click', "ul.pagination>li>a", function () {

            var href = $(this).attr('href');
            get_data(href);

            return false;
        });

        $("#search").keypress(function (e) {

            var key = e.keyCode ? e.keyCode : e.which;
            if (key == 13) { //enter

                do_search();
            }

        });

        $("#btn-search").click(function () {

            do_search();

            return false;
        });

    });*/

    
    
    function delete_paket(id)
    {
        if (confirm('Are you sure delete this data '))
        {
            // ajax delete data to database
            $.ajax({
               url: "<?php echo site_url('paket/ajax_delete') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {   //alert('success');
                    //if success reload ajax table
                    //get_data();
                    //reload_table();
                    //window.location.reload(true);
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    //alert('Error deleting data');
                    //window.location.href = "<?php echo site_url('no_access') ?>";
                    table.ajax.reload();
                }
            });
            
        }
    }
</script>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker3').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker4').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        
        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('paket/json_pindah_paket') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                    //data.filter = $('#filter').val();
                }
            },
        });
    
        $('#btn-filter').click(function () { //button filter event click
            //var tipe_tanggal = document.getElementById("tipe_tanggal").value;
            
            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            //var filter = document.getElementById("filter").value;
            table.ajax.url("<?php echo site_url('paket/json_pindah_paket'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table
            
        });

        $("#btn-Save").click(function (e) {
            e.preventDefault();
            //alert('test');
            var id = $("#id_sahabat").val();
            var tgl = $("#datepicker3").val();
            $.ajax({
                type: 'POST',
                data: {id_sahabat : id, tgl_pindah: tgl },
                url: "<?php echo site_url('paket/save'); ?>",
                success: function (data) {
                    //alert(data);
                    $('#md-custom').modal('hide');
                    table.ajax.reload();
                    //window.location.reload(true);
                }
            });
        });
        
        $("#btn-Update").click(function (e) {
            e.preventDefault();
            //alert('test');
            var id_sahabat = $("#id").val();
            var id = $("#id2").val();
            var tgl = $("#datepicker4").val();
            $.ajax({
                type: 'POST',
                data: {id: id,id_sahabat : id_sahabat, tgl_pindah: tgl },
                url: "<?php echo site_url('paket/update'); ?>",
                success: function (data) {
                    //alert(data);
                    //
                    $('#md-custom3').modal('hide');
                    table.ajax.reload();
                    //window.location.reload(true);
                }
            });
        });
        
       
        /*$("#btn-Import").click(function (e) {
            e.preventDefault();
            var file = $("#txt-file").val();
            //alert(file);
            //var tgl = $("#datepicker3").val();
            $.ajax({
                type: 'POST',
                data: {txt_file : file },
                url: "<?phcp echo site_url('paket/upload_data'); ?>",
                success: function (data) {
                    //alert(data);
                    //window.location.reload(true);
                }
            });
        });*/
    });
    
    function edit_data(id){
        var link_rm = "<?php echo site_url('paket/get_data') ?>";
            link_rm = link_rm + "/"+id;
        $.get(link_rm, function(data){
            $('#id').val(data.id_sahabat);
            $('#id2').val(data.id);
            $('#nama').val(data.nama);
            $('#datepicker4').val(data.tgl_pindahpaket);
        },"json");
        $('#md-custom3').modal('show');
    };
</script>