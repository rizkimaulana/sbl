 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});





        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('data_jamaahaffiliate/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('data_jamaahaffiliate/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('data_jamaahaffiliate/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });
</script>
 <div id="page-wrapper">
    
    
    
       <div class="row">
        <!-- <div class="col-lg-7"> -->
          <div class="panel panel-default">
            <div class="panel-body">
        <h3 class="page-header">Periksa Data Jamaah</h3>

       
        <form action="<?php echo base_url();?>data_jamaahaffiliate/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                
                <div class="form-group">
                    <label  class="col-sm-2 control-label" >ID Jamaah</label>
                    <div class="col-sm-10">
                       <!-- <input name="id_registrasi" type="hidden" value="<?php echo $id_registrasi;?>"> -->
                       <input name="id_booking" id="id_booking" type="hidden" value="<?php echo $id_booking;?>">
                       <input name="status" id="status" type="hidden" value="<?php echo $status;?>">
                        <input name="id_jamaah" type="text" class="form-control" id="nama" value="<?php echo $id_jamaah;?>" readonly="treu"/>
                      <input name="date_schedule" id="status" type="hidden" value="<?php echo $date_schedule;?>">
                    </div>
                  </div>

                  

                <div class="form-group">
                    <label  class="col-sm-2 control-label" >Nama</label>
                    <div class="col-sm-10">
                        <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $nama;?>" required/>
                    </div>
                  </div>
                   <div class="form-group">
                    <label  class="col-sm-2 control-label" >Nama PASSPORT</label>
                    <div class="col-sm-10">
                        <input name="nama_passport" type="text" class="form-control" id="nama_passport" value="<?php echo $nama_passport;?>" required/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3" >Tempat Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $tempat_lahir;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Tanggal Lahir</label>
                    <div class="col-sm-10">
                     
                         <input type="text" data-mask="date" class="form-control" name="tanggal_lahir" id="datepicker"  value="<?php echo $tanggal_lahir;?>" required/>
                
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                            id="no_identitas" name="no_identitas" value="<?php echo $no_identitas;?>" required/>
                    </div>
                  </div>

             <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Provinsi</label>
                    <div class="col-sm-10">
                          <select name="provinsi_id" class="form-control " id="provinsi" >
                                <option value="">- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                         
                            <div class="col-sm-5">
                              <input  type="text"  name="namaprovinsi" class="form-control" value="<?php echo $namaprovinsi;?>" readonly="true" />
                            </div>
                        
                    </div>

                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kabupaten</label>
                    <div class="col-sm-10">
                       <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
                       
                            <div class="col-sm-5">
                              <input  type="text"  name="namakabupaten" class="form-control" value="<?php echo $namakabupaten;?>" readonly="true" />
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kecamatan</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_id" class="form-control" id="kecamatan">
                                <option value=''>Select Kecamatan</option>
                            </select>
                            <div class="col-sm-5">
                              <input  type="text"  name="namakecamatan" class="form-control" value="<?php echo $namakecamatan;?>" readonly="true" />
                            </div>
                        
                    </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Alamat</label>
                    <div class="col-sm-10">
                        <textarea  name="alamat" class="form-control"  ><?php echo $alamat;?></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Telp</label>
                    <div class="col-sm-10">
                        <input id="telp" name="telp" type="type" class="input_telp form-control" value="<?php echo $telp;?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Email</label>
                    <div class="col-sm-10">
                        <input  name="email" type="text" class="form-control" value="<?php echo $email;?>" >
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select required class="form-control" id="jenis_kelamin" name="select_kelamin">
                        <option value="">- PILIH JENIS KELAMIN -</option>
                       <?php foreach($select_kelamin as $st){ 
                            $selected = ($id_kelamin == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                     <label class=" col-sm-2 control-label" >Rentang Umur</label>
                     <div class="col-sm-10">
                    <select required class="form-control" name="select_rentangumur">
                      <option value="">- PILIH RENTANG UMUR -</option>
                        <?php foreach($select_rentangumur as $st){ 
                            $selected = ($id_rentang_umur == $st->id_rentang_umur)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_rentang_umur;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-2 control-label" for="status">Status :</label>
                   <div class="col-sm-10">
                  <select required class="select_status form-control" name="select_statuskawin">
                    <option value="">- PILIH STATUS -</option>
                      <?php foreach($select_statuskawin as $st){ 
                            $selected = ($id_status_diri == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                    <label class="col-lg-2 control-label">Berangkat dengan : </label>
                    <div class="col-lg-10">
                     <select required class="form-control" name="select_status_hubungan" id="select_status_hubungan" >
                      <option value="">- PILIH BERANGKAT DENGAN -</option>
                      <!-- <option></option> -->
                       <?php foreach($select_status_hubungan as $st){ 
                            $selected = ($id_ket_keberangkatan == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>


              <div class="form-group">
                  <label class=" col-sm-2 control-label" >Hubungan Keluarga</label>
                   <div class="col-sm-10">
                    <select  class="form-control" name="hubungan" id="hubungan">
                        <option value="">- PILIH HUBUNGAN KELUARGA -</option>
                       <?php foreach($family_relation as $st){ 
                            $selected = ($id_hubkeluarga == $st->id_relation)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_relation;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                      <?php } ?>
                  </select>
                  </div>
                  </div>

                  

                 <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >Nama Keluarga</label>
                        <div class="col-sm-10">
                         <select  class="form-control chosen-select" name="select_keluarga" id="select_keluarga">
                           <option value="">- PILIH KELUARGA -</option>
                           <?php foreach($select_keluarga as $st){ 
                              $selected = ($id_keluarga == $st->id_jamaah)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_jamaah;?>" <?php echo $selected;?>><?php echo $st->nama;?> - <?php echo $st->id_jamaah;?></option>
                          <?php } ?>
                        </select>
                        </div>
                    </div>
                  
                 
 
                  <!-- 
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Room Type</label>
                        <div class="col-sm-10">
                       <select required class="form-control" name="select_type">
                         <option  value="">- PILIH ROOM TYPE -</option>
                            <?php foreach($select_type as $st){ 
                              $selected = ($id_room_type == $st->id_room_type)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_room_type;?>" <?php echo $selected;?>><?php echo $st->type;?></option>
                          <?php } ?>
                      </select>
                      </div>
                      </div> -->
                   
                  <div class="form-group">
                      <label class="col-sm-2 control-label"
                            for="inputPassword3" >Ahli Waris</label>
                      <div class="col-sm-10">
                          <input id="waris" name="waris" type="text" class="input_telp form-control" value="<?php echo $ahli_waris;?>"  required>
                      </div>
                  </div>

                  <div class="form-group">
                         <label class="col-sm-2 control-label" >Hubungan Ahli waris </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_hub_ahliwaris">
                          <option  value="">- PILIH Hubungan Ahli Waris -</option>
                            

                            <?php foreach($select_hub_ahliwaris as $st){ 
                              $selected = ($hub_waris == $st->id_hubwaris)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_hubwaris;?>" <?php echo $selected;?>><?php echo $st->hubungan;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                       <div class="form-group">
                         <label class="col-sm-2 control-label" for ="select_merchandise">Merchandise </label>
                         <div class="col-sm-10">
                        <select required class="select_merchandise form-control" name="select_merchandise">
                          <option  value="">- PILIH MERCHANDISE -</option>
                            

                             <?php foreach($select_merchandise as $st){ 
                              $selected = ($id_merchandise == $st->id_merchandise)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_merchandise;?>" <?php echo $selected;?>><?php echo $st->merchandise;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >No Pasport</label>
                        <div class="col-sm-10">
                          <input name="no_pasport" type="text" id="no_pasport"class="input_keluarga form-control" value="<?php echo $no_pasport;?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >issue office</label>
                        <div class="col-sm-10">
                          <input name="issue_office" type="text" id="issue_office"class="input_keluarga form-control" value="<?php echo $issue_office;?>">
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Isue Date</label>
                    <div class="col-sm-10">
                    
                         <input type="text" class="form-control" name="isui_date" id="datepicker2"   value="<?php echo $isui_date;?>" />
                
                    </div>
                  </div>
                  <div class="form-group">
                         <label class="col-sm-2 control-label" >Kartu Identitas </label>
                         <div class="col-sm-3">
                        <select required class="form-control" name="select_status_identitas">                      
                          <?php foreach($select_status_identitas as $st){ 
                              $selected = ($status_identitas == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option> 
                          <?php } ?>
                        </select>
                         <!-- <input type="text" data-mask="date" class="form-control" name="tgl_status_identitas" id="datepicker3"  value="<?php echo $tgl_status_identitas;?>" placeholder="dd/mm/yyyy "  /> -->
                      </div>
                    </div>

                    <div class="form-group">
                         <label class="col-sm-2 control-label" >Kartu Keluarga </label>
                         <div class="col-sm-3">
                        <select required class="form-control" name="select_status_kk">                      
                          <?php foreach($select_status_kk as $st){ 
                              $selected = ($status_kk == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                          <!-- <input type="text" data-mask="date" class="form-control" name="tgl_status_kk" id="datepicker4"  value="<?php echo $tgl_status_kk;?>" placeholder="dd/mm/yyyy "  /> -->
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                         <label class="col-sm-2 control-label" >Pas Photo </label>
                         <div class="col-sm-3">
                        <select required class="form-control" name="select_photo">                      
                          <?php foreach($select_photo as $st){ 
                              $selected = ($status_photo == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                          <!-- <input type="text" data-mask="date" class="form-control" name="tgl_status_photo" id="datepicker5"  value="<?php echo $tgl_status_photo;?>" placeholder="dd/mm/yyyy " /> -->
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                         <label class="col-sm-2 control-label" >No Pasport </label>
                         <div class="col-sm-3">
                        <select required class="form-control" name="select_pasport">                      
                          <?php foreach($select_pasport as $st){ 
                              $selected = ($status_pasport == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                          <!-- <input type="text" data-mask="date" class="form-control" name="tgl_status_pasport" id="datepicker6"  value="<?php echo $tgl_status_pasport;?>"placeholder="dd/mm/yyyy "  /> -->
                        </select>
                      </div>
                    </div>

                     <div class="form-group">
                         <label class="col-sm-2 control-label" >Vaksin </label>
                         <div class="col-sm-3">
                        <select required class="form-control" name="select_vaksin">                      
                          <?php foreach($select_vaksin as $st){ 
                              $selected = ($status_vaksin == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                          <!-- <input type="text" data-mask="date" class="form-control" name="tgl_status_vaksin" id="datepicker7"  value="<?php echo $tgl_status_vaksin;?>" placeholder="dd/mm/yyyy " /> -->
                        </select>
                      </div>
                    </div>

                   <div class="form-group">
                         <label class="col-sm-2 control-label" >Buku Nikah </label>
                         <div class="col-sm-3">
                        <select required class="form-control" name="select_buku_nikah">                      
                          <?php foreach($select_vaksin as $st){ 
                              $selected = ($status_buku_nikah == $st->id_status_pic)  ? 'selected' :'';?>
                              <option value="<?php echo $st->id_status_pic;?>" <?php echo $selected;?>><?php echo $st->status_pic;?></option>
                          <?php } ?>
                          <!-- <input type="text" data-mask="date" class="form-control" name="tgl_status_buku_nikah" id="datepicker8"  value="<?php echo $tgl_status_buku_nikah;?>" /> -->
                        </select>
                      </div>
                    </div>

                 



                     <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td width="40"><b>Foto Copy Data Persayratan</b></td>
                                <td width="40">
                                    1. Kartu Identitas <input name="pic[]" type="file">
                                    <?php if($detail['pic1']){?>
                                    <a href="<?php echo base_url().'assets/images/foto_copy/'.$detail['pic1']?>" target="_blank">
                                        <i class="fa fa-lightbulb-o"></i> View
                                    </a>
                                    <a  href="javascript:;" 
                                        img="<?php echo $detail['pic1'];?>"
                                        id-registrasi="<?php echo $detail['id_registrasi'];?>"
                                        title="Delete" class="a-danger delete-gambar">
                                      <i class="fa fa-times"></i> Delete
                                  </a>
                                  <?php } ?>
                                 </td>
                                <td>
                                    2. Kartu Keluarga <input name="pic[]" type="file">
                                    <?php if($detail['pic2']){?>
                                    <a href="<?php echo base_url().'assets/images/foto_copy/'.$detail['pic2']?>" target="_blank">
                                        <i class="fa fa-lightbulb-o"></i> View
                                    </a>
                                    <a  href="javascript:;" 
                                        img="<?php echo $detail['pic2'];?>"
                                        id-registrasi="<?php echo $detail['id_registrasi'];?>"
                                        title="Delete" class="a-danger delete-gambar">
                                      <i class="fa fa-times"></i> Delete
                                  </a>
                                  <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    3. Pas Photo <input name="pic[]" type="file">
                                    <?php if($detail['pic3']){?>
                                    <a href="<?php echo base_url().'assets/images/foto_copy/'.$detail['pic3']?>" target="_blank">
                                        <i class="fa fa-lightbulb-o"></i> View
                                    </a>
                                    <a  href="javascript:;" 
                                        img="<?php echo $detail['pic3'];?>"
                                        id-registrasi="<?php echo $detail['id_registrasi'];?>"
                                        title="Delete" class="a-danger delete-gambar">
                                      <i class="fa fa-times"></i> Delete
                                  </a>
                                  <?php } ?>
                                </td>
                                <td>
                                    4. Pasport <input name="pic[]" type="file">
                                    <?php if($detail['pic4']){?>
                                    <a href="<?php echo base_url().'assets/images/foto_copy/'.$detail['pic4']?>" target="_blank">
                                        <i class="fa fa-lightbulb-o"></i> View
                                    </a>
                                    <a  href="javascript:;" 
                                        img="<?php echo $detail['pic4'];?>"
                                        id-registrasi="<?php echo $detail['id_registrasi'];?>"
                                        title="Delete" class="a-danger delete-gambar">
                                      <i class="fa fa-times"></i> Delete
                                  </a>
                                  <?php } ?>
                                </td>
                               
                            </tr>

                             <tr>
                                <td></td>
                                <td>
                                    5. Vaksin <input name="pic[]" type="file">
                                    <?php if($detail['pic5']){?>
                                    <a href="<?php echo base_url().'assets/images/foto_copy/'.$detail['pic5']?>" target="_blank">
                                        <i class="fa fa-lightbulb-o"></i> View
                                    </a>
                                    <a  href="javascript:;" 
                                        img="<?php echo $detail['pic5'];?>"
                                        id-registrasi="<?php echo $detail['id_registrasi'];?>"
                                        title="Delete" class="a-danger delete-gambar">
                                      <i class="fa fa-times"></i> Delete
                                  </a>
                                  <?php } ?>
                                </td>
                                 <td>
                                    6. Buku Nikah <input name="pic[]" type="file">
                                    <?php if($detail['pic6']){?>
                                    <a href="<?php echo base_url().'assets/images/foto_copy/'.$detail['pic6']?>" target="_blank">
                                        <i class="fa fa-lightbulb-o"></i> View
                                    </a>
                                    <a  href="javascript:;" 
                                        img="<?php echo $detail['pic6'];?>"
                                        id-registrasi="<?php echo $detail['id_registrasi'];?>"
                                        title="Delete" class="a-danger delete-gambar">
                                      <i class="fa fa-times"></i> Delete
                                  </a>
                                  <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                     <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            <a href="<?php echo base_url();?>manifest/detail"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
           
    </div>
    </div>
</div>



<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });

       // $(function () {
       //          $('#datepicker2').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
       // $(function () {
       //          $('#datepicker3').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
       // $(function () {
       //          $('#datepicker4').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
       // $(function () {
       //          $('#datepicker5').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
       // $(function () {
       //          $('#datepicker6').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
       // $(function () {
       //          $('#datepicker7').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
       // $(function () {
       //          $('#datepicker8').datetimepicker({
       //    format: 'YYYY-MM-DD',
       //  });
       //      });
    

    jQuery(document).ready(function(){
  $(".chosen-select").chosen({width: "95%"}); 
});


    </script>

</script> <script src="<?php echo base_url();?>assets/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    $("[data-mask='date']").mask("9999-99-99");
    $("[data-mask='phone']").mask("(999) 999-9999");
    $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
    $("[data-mask='phone-int']").mask("+33 999 999 999");
    $("[data-mask='taxid']").mask("99-9999999");
    $("[data-mask='ssn']").mask("999-99-9999");
    $("[data-mask='product-key']").mask("a*-999-a999");
    $("[data-mask='percent']").mask("99%");
    $("[data-mask='currency']").mask("$999,999,999.99");
  });
</script>