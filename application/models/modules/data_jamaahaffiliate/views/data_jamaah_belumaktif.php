
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Belum Aktif</b>
                </div>

                 <div class="panel-body">
                     <form role="form">
                          <!--  
                             <div class="form-group input-group col-lg-4">
                               
                                <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div> -->
                        </form>  
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TANGGAL DAFTAR</th>
                                    <!-- <th>USER </th> -->
                                   <!-- <th>STATUS JAMAAH </th> -->
                                   <!-- <th>ACTION </th> -->
                                </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                     </table>
                   </div>
                  
                      <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>

// function get_data(url,q){
        
//         if(!url)
//             url = base_url+'data_jamaahaffiliate/get_jamaah_belum_aktif';
        
//         $.ajax({
            
//             url:url,type:'post',dataType:'json',
//             data:{q:q},
//             success:function(result){
                
//                 $("#data-table tbody").html(result.rows);
//                 $("ul.pagination").html(result.paging);
//                 $(".page-info").html(result.page_info);
//             }
        
//         });
//     } 
//     function do_search(){
    
                
//         get_data('',$("#search").val());
      
//     }
//     $(function(){
    
//         get_data();//initialize
        
//         $(document).on('click',"ul.pagination>li>a",function(){
        
//             var href = $(this).attr('href');
//             get_data(href);
            
//             return false;
//         });
        
//         $("#search").keypress(function(e){
            
//             var key= e.keyCode ? e.keyCode : e.which ;
//             if(key==13){ //enter
                
//                 do_search();
//             }
            
//         });
        
//         $("#btn-search").click(function(){
            
//             do_search();
            
//             return false;
//         });
        
//     });

</script>
<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables
    table = $('#data-table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_jamaahaffiliate/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

});

</script>