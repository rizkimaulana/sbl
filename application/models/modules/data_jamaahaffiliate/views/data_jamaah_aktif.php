
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Aktif</b>
                </div>

                 <div class="panel-body">
                     <form role="form">
                           <!-- 
                             <div class="form-group input-group col-lg-4">
                               
                                <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div> -->
                        </form>  
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <!-- <th>BARCODE</th> -->
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TANGGAL DFTR/AKT </th>
                                    <!-- <th>USER </th> -->
                                   <!--  <?php if( $this->session->userdata('id_affiliate_type')==1){ ?>
                                   <th>Action </th>
                                   <?php } ?> -->
                                </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                     </table>
                   </div>
                  
                      <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Welcome To Office.sbl.co.id...</h4>
      </div>
      <div class="modal-body">
      PEMBERITAHUAN MEMO MANIFEST PUSAT TERBARU...Sesuai Memo SBL Nomor 4670/SBL/Manifest/VI/2017 perihal "Program Kerja Musim Umrah 2018" </br>
         1. memasuki 4 bulan menuju keberangkatan dimohon mulai melakukan input data passport pada back office sistem masing-masing cabang. Reminder akan kami lakukan pada jarak waktu 3 bulan menuju keberangkatan di MENU (<a href="http://office.sbl.co.id/manifest_affiliate/input_manifest/data_manifest"> INPUT DATA MANIFEST</a>).
 </br>
 </br>
        2. memasuki 3 bulan menuju keberangkatan mohon lengkapi data passport jamaah yang belum diinput pada system. Apabila melewati 3 bulan ini data belum juga masuk, kami tidak dapat menjamin akan satu jadwal keberangkatan dengan kelompoknya. Reminder akan kami lakukan pada jarak waktu 2 bulan menuju keberangkatan. 
  </br>
 </br>
        3. memasuki 2 bulan menuju keberangkatan mohon lengkapi data passport jamaah yang belum diinput pada system. Sesuai peringatan sebelumnya, jamaah yang baru diinput saat ini akan terpisah jadwal keberangkatan dengan kelompoknya. Mohon dicatat bahwa ini adalah peringatan terakhir. Jamaah yang belum juga melengkapi datanya hingga bulan ini berakhir akan dipindah ke tanggal Waiting list.
</br>
 </br>
       4. memasuki 1 bulan menuju keberangkatan jamaah yang belum lengkap datanya telah dipindah ke tanggal Waiting list. 
       <br>
SEKIAN TERIMAKASIH.
      </div>
      <div class="modal-footer">
     <!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div> 
  </div>
</div>
              
  <script>
  $(function() {
    $("#myModal").modal();
});
// function get_data(url,q){
        
//         if(!url)
//             url = base_url+'data_jamaahaffiliate/jamaah_aktifaffiliate/get_jamaah_aktif';
        
//         $.ajax({
            
//             url:url,type:'post',dataType:'json',
//             data:{q:q},
//             success:function(result){
                
//                 $("#data-table tbody").html(result.rows);
//                 $("ul.pagination").html(result.paging);
//                 $(".page-info").html(result.page_info);
//             }
        
//         });
//     } 
//     function do_search(){
    
                
//         get_data('',$("#search").val());
//         // alert ($(this).val());
//         // $("#search").val('');

//     }
//     $(function(){
    
//         get_data();//initialize
        
//         $(document).on('click',"ul.pagination>li>a",function(){
        
//             var href = $(this).attr('href');
//             get_data(href);
            
//             return false;
//         });
        
//         $("#search").keypress(function(e){
            
//             var key= e.keyCode ? e.keyCode : e.which ;
//             if(key==13){ //enter
                
//                 do_search();
//                 // $(this).val('');

//             }
//             // if(do_search());{ 
//             // alert ($(this).val());
//             //  }



//         });
        
//         $("#btn-search").click(function(){
            
//             do_search();
             
//             return false;

//         });
        
//     });

</script>
<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables
    table = $('#data-table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('data_jamaahaffiliate/jamaah_aktifaffiliate/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

});

</script>