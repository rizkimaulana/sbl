<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_jamaahaffiliate extends CI_Controller{
	var $folder = "data_jamaahaffiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(DATA_JAMAAH_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','data_jamaahaffiliate');	
		$this->load->model('datajamaahaffiliate_model');
		$this->load->model('datajamaahaffiliate_model','r');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/data_jamaahaffiliate');
		
	}

	public function data_jamaahbelumaktif(){
	
	 //   $data = array(
		// 	// 'pic' => $this->datajamaahaffiliate_model->get_pic_belumaktif('order by id_jamaah desc')->result_array(),
	 //   		'pic' => $this->datajamaahaffiliate_model->get_pic_belumaktif('order by id_jamaah desc')->result_array(),
		// );
	   $this->template->load('template/template', $this->folder.'/data_jamaah_belumaktif');

	
		
	}
	
	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('view_refund')->like('pemberangkatan',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->pemberangkatan,
				 'refund'	=>$row->refund
				

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}

	function searchItem(){
            

             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->registrasijamaah_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

    	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	               	$row[] ='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 Telp 2: '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $row[] ='<td width="20%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $row[] ='<td width="20%" >Tgl Daftar : '.$r->tgl_daftar.'
	             			
	               			 </td>';
	                			
	                // $row[] ='<td width="20%">'.$r->create_by.'<br>
	             			//  User ID : '.$r->id_user.'<br>

	                		// </td>';
	                // $row[] ='<td width="20%">'.$r->status_jamaah.'</td>';
	              
	              

			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
    public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->datajamaahaffiliate_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="10%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->paket.'</td>';
	                $rows .='<td width="10%">'.$r->departure.'</td>';
	                $rows .='<td width="20%">'.$r->tgl_daftar.'</td>';
	                $rows .='<td width="10%">'.$r->tgl_keberangkatan.'</td>';
	                $rows .='<td width="10%">'.$r->jumlah_jamaah.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'data_jamaahaffiliate/detail/'.$r->id_booking.'">
	                            <i class="fa fa-pencil"></i> Detail
	                        </a> ';
	                  $rows .='<a class="btn btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'data_jamaahaffiliate/cetak_datajamaah/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'data_jamaahaffiliate/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}


   
	
	 public function get_jamaah_belum_aktif(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->datajamaahaffiliate_model->get_jamaah_belum_aktif($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="3%" >'.$i.'</td>';
	                $rows .='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $rows .='<td width="20%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $rows .='<td width="20%" >Tgl Daftar : '.$r->tgl_daftar.'
	             			
	               			 </td>';
	                			
	                $rows .='<td width="20%">'.$r->create_by.'<br>
	             			 User ID : '.$r->id_user.'<br>

	                		</td>';
	                $rows .='<td width="20%">'.$r->status_jamaah.'</td>';
	                $rows .='<td width="20%" align="center">';
	               
	                // $rows .='<a title="Edit" class=" btn-sm btn-success" href="'.base_url().'data_jamaahaffiliate/edit/'.$r->id_jamaah.'">
	                //            Edit
	                //         </a> <br><br>';
	               

	            
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging_jamaah_belum_aktif($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="10">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging_jamaah_belum_aktif($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'data_jamaahaffiliate/get_jamaah_belum_aktif/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	 public function detail(){
	


	    if(!$this->general->privilege_check_affiliate(DATA_JAMAAH_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->datajamaahaffiliate_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	        $pic = $this->datajamaahaffiliate_model->get_pic($id_booking);
	        // $row_datajamaah = $this->datajamaah_model->get_pic_booking ($id_booking);
		
	    }    

	    $data = array(
	    		
	        	// 'row_datajamaah' => $this->datajamaah_model->get_pic_booking ($id_booking),
	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahaffiliate',($data));

	}
	

private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_keluarga(){
		 $id_user=  $this->session->userdata('id_user');
		$sql ="SELECT a.id_jamaah,a.nama,b.keterangan AS status 
				FROM registrasi_jamaah AS a, `status` AS b
				WHERE a.status = b.kdstatus and a.status in('0','1') and a.id_affiliate='".$id_user."'";
		// $sql .= ($id_booking !== '') ? " and id_booking ='$id_booking'" : "";
		return $this->db->query($sql)->result();
		

		// $id_booking = $this->input->post('id_booking');
		// $status = array('0', '1');
		// $this->db->where_in('status', $status);
	 //    return $this->db->get('registrasi_jamaah')->result();
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_group_keberangkatan(){
	
		return $this->db->get('pilih_group_pemberangkatan')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	
	private function _select_status_pic(){
		return $this->db->get('status_pic')->result();
	    
	}

	private function _select_shedule($tanggal=''){
		// if($tanggal ==''){
		// 	return $this->db->get('view_concet_schedule')->result();
		// }
		// else{
		// 	$sql ="select * from view_concet_schedule  date_schedule "
		// }
		
		$sql ="select * 
					from view_concet_schedule 
					where date_schedule is not null";
		$sql .= ($tanggal !== '') ? " and date_schedule ='$tanggal'" : "";
		return $this->db->query($sql)->result();
		
	    
	}

	private function _select_room_group(){
		return $this->db->get('view_concat_room_group')->result();
	    
	}
	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}

public function edit($id){
	    $get = $this->db->get_where('data_jamaah_belum_aktif',array('id_jamaah'=>$id))->row_array();
	    $date_schedule = $get['date_schedule'];
	    $id_booking = $get['id_booking'];
	    $detail = $this->db->get_where('data_jamaah_belum_aktif',array('id_jamaah'=>$id))->row_array();
	    if(!$get)
	        show_404();
	         $data = array(
	         	'family_relation'=>$this->_family_relation(),

				'select_merchandise'=>$this->_select_merchandise(),
				'select_rentangumur'=>$this->_select_rentangumur(),
				'select_status_hubungan'=>$this->_select_status_hubungan(),
				'select_kelamin'=>$this->_select_kelamin(),
				'select_statuskawin'=>$this->_select_statuskawin(),
		    	'provinsi'=>$this->datajamaahaffiliate_model->get_all_provinsi(),
		    	'kabupaten'=>$this->datajamaahaffiliate_model->get_all_kabupaten(),
		    	
		    	'select_status_identitas'=>$this->_select_status_pic(),
		    	'select_status_kk'=>$this->_select_status_pic(),
		    	'select_photo'=>$this->_select_status_pic(),
		    	'select_pasport'=>$this->_select_status_pic(),
		    	'select_vaksin'=>$this->_select_status_pic(),
		    	'select_buku_nikah'=>$this->_select_status_pic(),
		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	// 'id_bandara' => $this->manifest_model->get_data_bandara(),
		    	// 'select_keluarga'=>$this->_select_keluarga($id_booking),
		    	'select_keluarga'=>$this->_select_keluarga(),
		    	// 'select_affiliate'=>$this->_select_affiliate(),
	  			 // 'select_pemberangkatan'=>$this->_select_pemberangkatan(),
	  			 // 'select_shedule'=>$this->_select_shedule($date_schedule),
	  			 // 'select_group_keberangkatan'=>$this->_select_group_keberangkatan(),
	  		//	 'select_room_group'=>$this->_select_room_group(),
	  			  // 'get_room'=>$this->manifest_model->getroom_group()->result(),
	  			 // 'get_data_keluarga'=>$this->manifest_model->get_data_keluarga(),
	  			 // 'anggota'=>$this->manifest_model->getAnggota()->result(),
	         	'detail'=>$detail

	  		);
	  		
        $this->template->load('template/template', $this->folder.'/edit_jamaah_belum_aktif',array_merge($get,$data));
	}


	public function update(){
	    
	   		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/foto_copy/',
                'allowed_types' => 'gif|jpg|png|jpeg|pdf',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }
		    else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		    }
	    }

	  
	   $send = $this->datajamaahaffiliate_model->update($data);
	   if($send)
	    
            	redirect('data_jamaahaffiliate/data_jamaahbelumaktif');

	// print_r($_POST);

	}



}
