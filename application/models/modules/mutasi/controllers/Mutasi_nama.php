<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi_nama extends CI_Controller{
	var $folder = "mutasi";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(MUTASI_NAMA,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','mutasi');	
		$this->load->model('mutasinama_model');
		$this->load->model('mutasinama_model','r');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/mutasi_nama');
		
	}

	private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_keluarga($id_booking=''){
		//$id_booking = $this->input->post('id_booking');

		$this->db->where_in('id_booking', $id_booking);
	    return $this->db->get('registrasi_jamaah')->result();
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	
	private function _select_status_pic(){
		return $this->db->get('status_pic')->result();
	    
	}

	private function _select_shedule(){
		return $this->db->get('view_concet_schedule')->result();
	    
	}

	private function _select_room_group(){
		return $this->db->get('view_concat_room_group')->result();
	    
	}
	
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	                $row[] ='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 ID SAHABAT :  '.$r->id_sahabat.'<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $row[] ='<td width="18%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $row[] ='<td width="18%" >Tgl Daftar : '.$r->tgl_daftar.'<br>
	             			 Tgl Aktivasi : '.$r->update_date.'<br>
	             			
	               			 </td>';
	                			


	               $row[] ='<a title="Mutasi" class="btn btn-sm btn-primary" href="'.base_url().'mutasi/mutasi_nama/edit/'.$r->id_jamaah.'">
	                            <i class="fa fa-pencil"></i> Mutasi
	                        </a> ';

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
   

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'mutasi/mutasi_nama/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}


   public function update(){
	    
		$data = $this->input->post(null,true);
	   
	  
	   try {
            $flag = 0;
            $rename_file = array();
            for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                if ($_FILES['pic']['name'][$i]) {
                    $rename_file[$i] = 'pic' . ($i + 1) . '_mutasi_nama_' . $_FILES['pic']['name'][$i];
                    $flag++;
                } else {
                    $rename_file[$i] = '';
                }
            }
            if ($flag > 0) {
                $this->load->library('upload');
                $this->upload->initialize(array(
                    "file_name" => $rename_file,
                    'upload_path' => './assets/images/bukti_pembayaran/',
                    'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                    'max_size' => '2000' //Max 2MB
                ));

                if ($this->upload->do_multi_upload("pic")) {
                    $info = $this->upload->get_multi_upload_data();
                    foreach ($info as $in) {
                        $picx = substr($in['file_name'], 0, 4);
                        $data[$picx] = $in['file_name'];
                    }
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    echo "Errors Occured : "; //sini aja lah
                    print_r($error);
                }
            }
            if(!$this->mutasinama_model->update($data)){
                throw new Exception('Ada kesalahan ketika simpan data mutasi nama!');
            }
            $id_booking = $this->input->post('id_booking');
            $msg = 'Data Mutasi Nama '.$data['nama'].' berhasil disimpan!';
            $data_jamaah_lama = $this->db->get_where('registrasi_jamaah', array('id_registrasi'=>$data['id_registrasi']))->row_array();
            $konten['post_data'] = $data;
            $konten['data_jamaah_lama'] = $data_jamaah_lama;
            $this->simpan_aktivitas('Mutasi Nama', $konten);
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->simpan_error('Mutasi Nama', $data, $exc);
        }
        $this->session->set_flashdata('info', $msg);
    	//redirect('mutasi/mutasi_nama/');
    	redirect('mutasi/mutasi_nama/cetak_invoice/'.$data['id_registrasi']);
	}
	
	function cetak_invoice($id_registrasi)
    {
        $getdata = $this->mutasinama_model->get_history_nama($id_registrasi)->result();
        $data = array();
        foreach($getdata as $row){
            $data = array(
                        'id_historis_mutasi_nama' => $row->id_historis_mutasi_nama,
                        'id_registrasi' => $row->id_registrasi,
                        'id_jamaah' => $row->id_jamaah,
                        'nama' => $row->nama,
                        'alamat' => $row->alamat,
                        'telp' => $row->telp,
                        'biaya_mutasi_nama' => $row->biaya_mutasi_nama,
                        'keterangan_pembayaran' => $row->keterangan_pembayaran,
                        'pay_method' => $row->pay_method,
                        'payment_date' => $row->payment_date,
                        'bank_transfer' => $row->bank_transfer,
                        'no_transaksi' => $row->no_transaksi
                    );
           
        }
        
        $this->load->view('cetak_invoice', $data);
        
        //$this->template->load('template/template', $this->folder . '/mutasi_nama/edit_mutasi_nama');
        //$data = '18491701';
        //$this->template->load('template/template', $this->folder . '/edit_mutasi_nama',$data);
    }
	
	
	function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }

     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        return $query->result();
    }



public function edit(){
	   if(!$this->general->privilege_check(MUTASI_NAMA,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(4);
	    // $get = $this->db->get_where('view_mutasi',array('id_registrasi'=>$id))->row_array();
	      $stored_procedure = "call detail_jamaah_aktif(?)";
          $get = $this->db->query($stored_procedure,array('id_registrasi'=>$id))->row_array(); 
	    if(!$get)
	        show_404();

	    $cek_double = $this->r->cek_is_double($id);
	        
	    $data = array('family_relation'=>$this->_family_relation(),
				'select_product'=>$this->_select_product(),
				'select_merchandise'=>$this->_select_merchandise(),
				'select_rentangumur'=>$this->_select_rentangumur(),
				'select_status_hubungan'=>$this->_select_status_hubungan(),
				'select_kelamin'=>$this->_select_kelamin(),
				'select_statuskawin'=>$this->_select_statuskawin(),
				'provinsi'=>$this->get_all_provinsi(),
	    		'kabupaten'=>$this->get_all_kabupaten(),
		    	'select_embarkasi'=>$this->_select_embarkasi(),
		    	// 'select_status_identitas'=>$this->_select_status_pic(),
		    	// 'select_status_kk'=>$this->_select_status_pic(),
		    	// 'select_photo'=>$this->_select_status_pic(),
		    	// 'select_pasport'=>$this->_select_status_pic(),
		    	// 'select_vaksin'=>$this->_select_status_pic(),
		    	// 'id_bandara' => $this->mutasinama_model->get_data_bandara(),
		    	// 'select_keluarga'=>$this->_select_keluarga(),
		    	// 'select_affiliate'=>$this->_select_affiliate(),
	  			 // 'select_pemberangkatan'=>$this->_select_pemberangkatan(),
	  			 // 'select_shedule'=>$this->_select_shedule(),
	  			'select_room_group'=>$this->_select_room_group(),
	  			'select_bank'=>$this->_select_bank(),
            	'select_pay_method'=>$this->_select_pay_method(),
            	'cek_double' => $cek_double
	    	);


	  
        $this->template->load('template/template', $this->folder.'/edit_mutasi_nama',array_merge($get,$data));
	}

	private function _select_pay_method(){
        return $this->db->get('pay_method')->result();
    }
    
    private function _select_bank(){
        return $this->db->get('bank')->result();
    }

	



}
