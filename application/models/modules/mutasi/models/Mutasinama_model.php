<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mutasinama_model extends CI_Model{
      var $table = 'jamaah_aktif';
    var $column_order = array(null, 'id_registrasi','id_sahabat','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','no_identitas','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','id_user','tipe_jamaah','status','kd_tipe','tgl_aktivasi','update_date'); //set column field database for datatable orderable
    var $column_search = array('id_registrasi','id_sahabat','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','no_identitas','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','id_user','tipe_jamaah','status','kd_tipe','tgl_aktivasi','update_date'); //set column field database for datatable searchable 
    var $order = array('update_date' => 'asc'); // default order 

   
     private $_mutasi_nama="historis_mutasi_nama";
     private $_id_registrasi="id_registrasi";


      
    public function update($data){
        // $harga_kuota = $this->input->post('harga');
        $id_registrasi = $this->input->post('id_registrasi');
        $sql ="Select count(*) as jumlah_row from historis_mutasi_nama  
                where id_registrasi ='$id_registrasi' 
                group by id_registrasi";
        $dataa= $this->db->query($sql)->result_array();
        $jumlah_row = 0;
         foreach ($dataa as  $value) {
            $jumlah_row =$value ['jumlah_row'];
            // echo $jumlah_row;
             # code...
         }
         $anggota = $this->db->get_where('registrasi_jamaah', array('id_registrasi'=>$data['id_registrasi']))->row_array();
    if ($jumlah_row >= 2) die('jumlah mutasi tidak boleh lebih dari 2');
        $arr = array(
                
                'id_jamaah'=> $data['id_jamaah'],
                'id_registrasi'=> $data['id_registrasi'],
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
               
                'hubkeluarga'=> $data['select_status_hubungan'],
                // 'keluarga'=> $data['select_keluarga'],
                'no_pasport'=> $data['no_pasport'],
                
                'merchandise'=> $data['select_merchandise'],
                'create_date'=>date('Y-m-d H:i:s'),
                'create_by'=>$this->session->userdata('id_user'),
               
                'status'=> $data['status'],
                'biaya_mutasi_nama'=> $data['biaya_mutasi'],
                'keterangan_pembayaran' => $data['ket_pembayaran'],
                'pay_method' => $data['select_pay_method'],
                'payment_date' => $data['payment_date'],
                'bank_transfer' => $data['select_bank'],
                'data_lama' => serialize($anggota)
        );
        if(isset($data['pic1'])){
            $arr['pic1'] = $data['pic1'];
        }
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_mutasi_nama,$arr);
        $id_historis_mutasi_nama =  $this->db->insert_id(); 
        
        // Generate No Kuitansi Mutasi Nama
        $initial_kode = 'KMN';
        $item_update = array(
            'no_transaksi' => (sprintf($initial_kode . '%07d', $id_historis_mutasi_nama)),
        );
        $this->db->update($this->_mutasi_nama, $item_update, array('id_historis_mutasi_nama' => $id_historis_mutasi_nama));


         
         $arr = array(    
                
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
               
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'telp_2'=> $data['telp_2'],
                'email'=> $data['email'],
                // 'no_pasport'=> $data['no_pasport'],
                'merchandise'=> $data['select_merchandise'],
                'update_by'=>$this->session->userdata('id_user'),
                'update_date'=>date('Y-m-d H:i:s'),
                'provinsi_id' =>$data['provinsi_id'],
                'kecamatan_id' =>$data['kecamatan_id'],
                'kabupaten_id' =>$data['kabupaten_id'],

               
        );

        $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$data['id_registrasi']));

 if($data['provinsi_id']!='' && $data['kecamatan_id']!='' && $data['kabupaten_id']!='') {
          
            // $arr['password'] = ($data['password']);
            $arr['provinsi_id'] = ($data['provinsi_id']);
            $arr['kecamatan_id'] = ($data['kecamatan_id']);
            $arr['kabupaten_id'] = ($data['kabupaten_id']);

        }
             $arr = array(
                  
                  'no_pasport'=> $data['no_pasport'],
                  
              );       
    
        $this->db->update('manifest',$arr,array('id_registrasi'=>$data['id_registrasi']));

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            // echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            // $id_booking = $this->input->post('id_booking');
            //  redirect('registrasi_jamaah/detail/'.$id_booking.'');
            return true;
        }

    }
         
    

function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

    public function get_data($offset,$limit,$q=''){
    
       $sql = " SELECT * from jamaah_aktif where 1=1
                    ";
        
        if($q){
            
            $sql .=" AND id_jamaah LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR nama_affiliate LIKE '%{$q}%'

            ";
        }
        $sql .=" ORDER BY id_jamaah DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    

    public function get_detail($id){
    
         $sql = "SELECT * from view_mutasi
                WHERE id_registrasi = {$id}
                ";
        return $this->db->query($sql)->row_array();
    }  
    


   function get_keluarga()  {
    
    $query = $this->db->get('registrasi_jamaah');
    return $query->result();
    
    }
   function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }


     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        return $query->result();
    }
    
    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    
    function get_kabupaten() {
    
    $query = $this->db->get('wilayah_kabupaten');
    return $query->result();
    
    }
    
    function get_kecamatan()    {
    
    $query = $this->db->get('wilayah_kecamatan');
    return $query->result();
    
    }

    
    private function _get_datatables_query()
    {
        
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function cek_is_double($id_registrasi){
        $this->db->where(array('id_registrasi' => $id_registrasi));
        $this->db->get_where($this->_mutasi_nama);
        return $this->db->count_all();
    }

    function get_history_nama($id_registrasi){
            $this->db->where('id_registrasi',$id_registrasi);
            return $this->db->get($this->_mutasi_nama);
        }
    }
