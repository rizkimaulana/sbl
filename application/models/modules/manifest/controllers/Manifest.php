<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manifest extends CI_Controller{
	var $folder = "manifest";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DATA_MANIFEST,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','manifest');	
		$this->load->model('manifest_model');
		$this->load->model('manifest_model','r');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/manifest');
		
	}


    function export_to_excell() {
        if (!$this->general->privilege_check(DATA_MANIFEST, 'view'))
            $this->general->no_access();

        $tahun_keberangkatan = $this->uri->segment(3);
        $bulan_keberangkatan = $this->uri->segment(4);
        $data_manifest = $this->manifest_model->get_jamaah_manifest($tahun_keberangkatan, $bulan_keberangkatan);
        $pic = array();

        $list_data = array();
        foreach ($data_manifest as $item) {
            $affiliate = $this->db->get_where('affiliate', array('id_user'=>$item['id_affiliate']))->row_array();
            $product = $this->db->get_where('product', array('id_product'=>$item['id_product']))->row_array();
            $room_type = $this->db->get_where('room_type', array('id_room_type' => $item['room_type']))->row_array();
            $room_category = $this->db->get_where('room_category', array('id_room_category' => $item['room_category']))->row_array();
            $embarkasi = $this->db->get_where('embarkasi', array('id_embarkasi'=>$item['embarkasi']))->row_array();
            $keberangkatan = $this->db->get_where('family_relation', array('id_relation'=>$item['ket_keberangkatan']))->row_array();
            $manifest = $this->db->get_where('manifest', array('id_registrasi'=>$item['id_registrasi']))->row_array();
            $keluarga = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $manifest['keluarga']))->row_array();
            $hub_keluarga = $this->db->get_where('family_relation', array('id_relation'=>$manifest['hubkeluarga']))->row_array();
            $item['nama_embarkasi'] = !empty($embarkasi['departure']) ? $embarkasi['departure'] : '';
            $item['nama_produk'] = !empty($product['nama']) ? $product['nama'] : '';
            $item['nama_room_category'] = !empty($room_category['category']) ? $room_category['category'] : '';
            $item['jenis_kelamin'] = ($item['kelamin'] === '2') ? 'Pria' : 'Perempuan';
            // $item['hub_relasi'] = !empty($keberangkatan['keterangan']) ? $keberangkatan['keterangan'] : '';
            $item['hub_relasi'] = !empty($hub_keluarga['keterangan']) ? $hub_keluarga['keterangan'] : '';
            $item['nama_passport'] = !empty($manifest['nama_passport']) ? $manifest['nama_passport'] : '';
            $item['no_passport'] = !empty($manifest['no_pasport']) ? $manifest['no_pasport'] : '';
            $item['issue_date'] = !empty($manifest['isui_date']) ? $manifest['isui_date'] : '';
            $item['issue_office'] = !empty($manifest['issue_office']) ? $manifest['issue_office'] : '';
            $item['status_identitas'] = !empty($manifest['status_identitas']) ? 'Ya' : 'Tidak';
            $item['status_kk'] = !empty($manifest['status_kk']) ? 'Ya' : 'Tidak';
            $item['status_foto'] = !empty($manifest['status_photo']) ? 'Ya' : 'Tidak';
            $item['status_passport'] = !empty($manifest['status_pasport']) ? 'Ya' : 'Tidak';
            $item['status_vaksin'] = !empty($manifest['status_vaksi']) ? 'Ya' : 'Tidak';
            $item['status_buku_nikah'] = !empty($manifest['status_buku_nikah']) ? 'Ya' : 'Tidak';
            $item['status_akte'] = !empty($manifest['status_akte']) ? 'Ya' : 'Tidak';
            $item['nama_affiliate'] = !empty($affiliate['nama']) ? $affiliate['nama'] : '';
            $item['tipe_kamar'] = !empty($room_type['type']) ? $room_type['type'] : '';
            $item['tgl_berangkat'] = !empty($item['date_schedule']) ? $item['date_schedule'] : '';
            $item['keluarga'] = !empty($keluarga['nama']) ? $keluarga['nama'] : '';
            $list_data[] = $item;
        }
        
        $data = array(
            'data_manifest' => $list_data,
                // 'pic'=>$pic
        );
        $this->load->view('manifest/view_laporan', ($data));
    }


	function get_jumlah_room(){
        $room_group=$this->input->post('room_group');
        $get_room=$this->manifest_model->carikapasitas($room_group);
        if($get_room->num_rows()>0){
            $get_room=$get_room->row_array();
            echo $get_room['id_room_group'];
           
           

        }
    }


	private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_keluarga($id_booking=''){
		// $sql ="select * 
		// 			from registrasi_jamaah 
		// 			where id_booking is not null";
		// $sql .= ($id_booking !== '') ? " and id_booking ='$id_booking'" : "";
		// return $this->db->query($sql)->result();

	    return $this->db->get('registrasi_jamaah')->result();
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_group_keberangkatan(){
	
		return $this->db->get('pilih_group_pemberangkatan')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	
	private function _select_status_pic(){
		return $this->db->get('status_pic')->result();
	    
	}

	private function _select_shedule($tanggal=''){
		// if($tanggal ==''){
		// 	return $this->db->get('view_concet_schedule')->result();
		// }
		// else{
		// 	$sql ="select * from view_concet_schedule  date_schedule "
		// }
		
		$sql ="select * 
					from view_concet_schedule 
					where date_schedule is not null";
		$sql .= ($tanggal !== '') ? " and date_schedule ='$tanggal'" : "";
		return $this->db->query($sql)->result();
		
	    
	}

	private function _select_room_group(){
		return $this->db->get('view_concat_room_group')->result();
	    
	}
	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}



    public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->manifest_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="20%"><strong>'.$r->tahun_keberangkatan.'</strong></td>';
	                $rows .='<td width="20%"><strong>'.$r->bulan_keberangkatan.'</strong></td>';
	                $rows .='<td width="20%"><strong>'.$r->total_jamaah.'</strong></td>';
	          		 $rows .='<td width="20%" align="center">';
	                 

	                $rows .='<a title="Export Excell" class="btn btn-sm btn-primary" href="' . base_url() . 'manifest/export_to_excell/' . $r->tahun_keberangkatan . '/' . $r->bulan_keberangkatan . '">
                            <i class="fa fa-pencil"></i> Export Excell </a>
                        <a title="Export Excell" target="_blank" class="btn btn-sm btn-success" href="' . base_url() . 'manifest/data_manifest/' . $r->tahun_keberangkatan . '/' . $r->bulan_keberangkatan . '">
                            <i class="fa fa-pencil"></i> Detail </a>';
	                
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

		private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'manifest_model/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}


private function _status($take){
      $x='0';
     $status = array('0'=>'X','1'=>'Y');
      if($take)
          return $status[$take];
        // return $status;
 
  }
	
		public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	                $row[] ='<td width="10%" > '.$r->invoice.'<br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 TGL KEBERANGKATAN :  '.$r->schedule.'<br>
	             			 EMBARKASI:  '.$r->embarkasi.'<br>
	             			
	               			 </td>';
	               	$row[] = '<td width="10%">NAMA PASSPORT :'.$r->nama_passport.'<br>
	               	 		 NO PASSPORT : <strong>'.$r->no_pasport.'</strong><br>
	             			 ISSUE PASSPORT : <strong>'.$r->issue_office.'</strong><br>
	             			 ISSUE DATE : <strong>'.$r->isui_date.' </strong>
	               			 </td>';

	                $row[] = '<td width="10%">KRT IDENTITAS/KTP : <strong>'.$this->_status($r->status_identitas).'</strong><br>
		                     PASSPORT : <strong>'.$this->_status($r->status_pasport).'</strong><br>
		                     PHOTO : <strong>'.$this->_status($r->status_photo).' </strong><br>
		                     KARTU KELUARGA : <strong>'.$this->_status($r->status_kk).'</strong><br>
		                     BUKU NIKAH : <strong>'.$this->_status($r->status_buku_nikah).' </strong><br>
		                     VAKSIN : <strong>'.$this->_status($r->status_vaksin).' </strong><br>
		                     AKTE : <strong>'.$this->_status($r->status_akte).' </strong><br>
		                       </td>';    			
	               
	            //    	$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="UPDATE" onclick="edit_manifest('."'".$r->id."'".')"><i class="glyphicon glyphicon-pencil"></i> UPDATE</a>
             //      <a class="btn btn-sm btn-danger" target="_blank" href="'.base_url().'manifest/detail_data_manifest/'.$r->id_registrasi.'" title="DETAIL"><i class="glyphicon glyphicon-pencil"></i> DETAIL</a>
				         // ';
		                       $row[] = '<a class="btn btn-sm btn-primary"  href="'.base_url().'manifest/edit_manifest/'.$r->id_registrasi.'"title="UPDATE" <i class="glyphicon glyphicon-pencil"></i> UPDATE</a>
                  <a class="btn btn-sm btn-danger" target="_blank" href="'.base_url().'manifest/detail_data_manifest/'.$r->id_registrasi.'" title="DETAIL"><i class="glyphicon glyphicon-pencil"></i> DETAIL</a>
				         ';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

    public function detail_data_manifest(){
  


      if(!$this->general->privilege_check(DATA_MANIFEST,'view'))
        $this->general->no_access();
      $id = $this->uri->segment(3);
      $detail = $this->manifest_model->get_detail_manifest($id);
      if(!$detail){
          show_404();
      }
     
      $detail_keluarga = $this->manifest_model->get_detail_hubkeluarga($id);
        
      $data = array(

         
             'detail'=>$detail,
             'detail_keluarga' => $detail_keluarga
             );

      $this->template->load('template/template', $this->folder.'/detail_manifestaffiliate',($data));

  }
		public function edit ($id)
	{
		$data = $this->manifest_model->get_by_id($id);
		$data->create_date = ($data->create_date == '0000-00-00') ? '' : $data->create_date; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function detail(){
	
		$embarkasis = $this->manifest_model->get_list_embarkasih();

		$opt = array('' => 'All Emabarkasi');
		foreach ($embarkasis as $embarkasi) {
			$opt[$embarkasi] = $embarkasi;
		}

		$get['form_embarkasi'] = form_dropdown('',$opt,'','id="embarkasi" class="form-control"');
	   $data = array(
			'family_relation'=>$this->_family_relation(),
			'select_embarkasi'=>$this->_select_embarkasi(),
			// 'get_keluarga'=>$this->manifest_model->get_keluarga(),
	    	);


	     $this->template->load('template/template', $this->folder.'/detail_manifest',array_merge($get,$data));
	}
	

	public function detail_manifest(){
	    
	    $id = $this->uri->segment(3);
	    $seg5 = $this->uri->segment(4,0); //pass from katalog
	    
	    $detail_manifest = $this->manifest_model->get_detail($id);
	  
	    $data = array(
	               
	                'detail_manifest'=>$detail_manifest
	            );
	       $this->template->load('template/template', $this->folder.'/edit_manifest',($data));
	}

	 private function _status2($take){
      $x='0';
     $status = array('0'=>'X','1'=>'Y');
      if($take)
          return $status[$take];
        // return $status;
 
  }
	public function edit_manifest(){


		  if(!$this->general->privilege_check(DATA_MANIFEST,'view'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
		$stored_procedure = "call detail_manifest(?)";
        $get = $this->db->query($stored_procedure,array('id_registrasi'=>$id))->row_array(); 

        // $stored_procedure = "call detail_manifest(?)";
        // $get_tgl = $this->db->get_where('tgl_persyaratan',array('id_registrasi'=>$id))->row_array();
	    // $get_manifest = $this->manifest_model->get_manifest($id);
	    $detail    = array();
	    $tgl_terima= array();
	    // if(!$get){
	    //     show_404();
	    // }else{
	   
	        

	      $detail = $this->manifest_model->get_detail_manifest($id);
	      $tgl_terima = $this->manifest_model->get_detail_tglpersyaratan($id);
	  // }
	    $data = array(
	    		'tgl_terima'=>$tgl_terima,
				'detail'=>$detail,
	        	'family_relation'=>$this->_family_relation(),
				'select_status_hubungan'=>$this->_select_status_hubungan(),
				'select_kelamin'=>$this->_select_kelamin(),
				
	         	
				// 'select_statuskawin'=>$this->_select_statuskawin(),
		  //   	'select_status_identitas'=>$this->_select_status_pic(),
		  //   	'select_status_kk'=>$this->_select_status_pic(),
		  //   	'select_photo'=>$this->_select_status_pic(),
		  //   	'select_pasport'=>$this->_select_status_pic(),
		  //   	'select_vaksin'=>$this->_select_status_pic(),
		  //   	'select_buku_nikah'=>$this->_select_status_pic(),
		  //   	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
	       		 );
	// printf($tgl_terima('tgl_status_pasport'));
	    // printf($tgl_terima['tgl_status_pasport']);
	    $this->template->load('template/template', $this->folder.'/edit_manifest',array_merge($get,$data));
	}
	

	
public function update(){
	    $id_registrasi = $this->input->post('id_registrasi');
	    $date_now = date('Y-m-d');
    $expire_date=date_create($this->input->post('isui_date'));
    date_add($expire_date,date_interval_create_from_date_string("50 month"));
      if ($date_now > date_format($expire_date,"Y-m-d") ){
          $this->session->set_flashdata('info', "ISSUE DATE PASSPORT ANDA SUDAH EXPIRE / MELEBIHI 5 TAHUN.");
      }else{ 
        //print_r('expression');
	    $send = $this->manifest_model->update($data);
                echo json_encode(array("status" => TRUE));
	    }


	}
	
	public function update_manifest(){
	   //  $id_registrasi = $this->input->post('id_registrasi');
	   //  $date_now = date('Y-m-d');
    // $expire_date=date_create($this->input->post('isui_date'));
    // date_add($expire_date,date_interval_create_from_date_string("50 month"));
    //   if ($date_now > date_format($expire_date,"Y-m-d") ){
    //       $this->session->set_flashdata('info', "ISSUE DATE PASSPORT ANDA SUDAH EXPIRE / MELEBIHI 5 TAHUN.");
    //   }else{ 
    //     //print_r('expression');
	   //  $send = $this->manifest_model->update($data);
    //             redirect('manifest/detail');
	   //  }

			$data = $this->input->post(null,true);
      
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/foto_copy/',
                'allowed_types' => 'gif|jpg|png|jpeg|pdf',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
        if ($this->upload->do_multi_upload("pic")){
          
          $info = $this->upload->get_multi_upload_data();
          
          foreach($info as $in){     
             
             $picx = substr($in['file_name'],0,4);
                 $data[$picx] = $in['file_name'];
                 
              }
        }
        else{
    
          
          $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
      
        }
      }
        $id_registrasi = $this->input->post('id_registrasi');
	    $date_now = date('Y-m-d');
    $expire_date=date_create($this->input->post('isui_date'));
    date_add($expire_date,date_interval_create_from_date_string("50 month"));
      if ($date_now > date_format($expire_date,"Y-m-d") ){
          $this->session->set_flashdata('info', "ISSUE DATE PASSPORT ANDA SUDAH EXPIRE / MELEBIHI 5 TAHUN.");
      }else{ 
        //print_r('expression');
	    $send = $this->manifest_model->update($data);
                redirect('manifest/detail');
	    }

	}

	public function unlink(){
	
	    $data = $this->input->post(null,true);
	    
	    if(unlink('./assets/images/foto_copy/'.$data['img'])){
	        
	        $column = substr($data['img'],0,4); //pic1,pic2 etc...
	        $this->db->update('registrasi_jamaah',array($column=>''),array('id_registrasi'=>$data['id_registrasi']));
	        
	       echo json_encode(array('status'=>true)); 
	    }
	    
	}


function gambar($kode)
	{
				$height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '25';	
				$width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst
				$this->load->library('zend');
		        $this->zend->load('Zend/Barcode');
		 		$barcodeOPT = array(
				    'text' => $kode, 
				    'barHeight'=> $height, 
				    'factor'=>$width,
				);
						
					$renderOPT = array();
			$render = Zend_Barcode::factory(
		'code128', 'image', $barcodeOPT, $renderOPT
		)->render();
	}

		public function search_autocomplete()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('autocomplete_hubkeluarga')
					->like('autocomplete',$keyword)->get();	

		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->autocomplete,
				'nama'	=>$row->nama,
				'id_jamaah'	=>$row->id_jamaah,
				

			);
		}

		echo json_encode($arr);
	}

	function data_manifest(){
        if (!$this->general->privilege_check(DATA_MANIFEST, 'view'))
            $this->general->no_access();
        $tahun_keberangkatan = $this->uri->segment(3);
        $bulan_keberangkatan = $this->uri->segment(4);
        $data = array(
            'tahun' => $tahun_keberangkatan,
            'bulan' => $bulan_keberangkatan,
        );
        $this->template->load('template/template', $this->folder . '/data_manifest', $data);
    }

    public function json_data_manifest() {
        $tahun_keberangkatan = $this->uri->segment(3);
        $bulan_keberangkatan = $this->uri->segment(4);
        $data_manifest = $this->manifest_model->get_jamaah_manifest($tahun_keberangkatan, $bulan_keberangkatan);

        $list_data = array();
        $data = array();
        $no = 1;
        
        foreach ($data_manifest as $item) {
            $affiliate = $this->db->get_where('affiliate', array('id_user'=>$item['id_affiliate']))->row_array();
            $product = $this->db->get_where('product', array('id_product'=>$item['id_product']))->row_array();
            $room_category = $this->db->get_where('room_category', array('id_room_category' => $item['room_category']))->row_array();
            $room_type = $this->db->get_where('room_type', array('id_room_type' => $item['room_type']))->row_array();
            $embarkasi = $this->db->get_where('embarkasi', array('id_embarkasi'=>$item['embarkasi']))->row_array();            
            $keberangkatan = $this->db->get_where('family_relation', array('id_relation'=>$item['ket_keberangkatan']))->row_array();
            $manifest = $this->db->get_where('manifest', array('id_registrasi'=>$item['id_registrasi']))->row_array();
            $item['nama_produk'] = !empty($product['nama']) ? $product['nama'] : '';
            $item['nama_room_category'] = !empty($room_category['category']) ? $room_category['category'] : '';
            $item['hub_relasi'] = !empty($keberangkatan['keterangan']) ? $keberangkatan['keterangan'] : '';
            $item['nama_affiliate'] = !empty($affiliate['nama']) ? $affiliate['nama'] : '';
            $item['nama_passport'] = !empty($manifest['nama_passport']) ? $manifest['nama_passport'] : '';
            $item['no_passport'] = !empty($manifest['no_passport']) ? $manifest['no_passport'] : '';
            $item['issue_date'] = !empty($manifest['isui_date']) ? $manifest['isui_date'] : '';
            $item['issue_office'] = !empty($manifest['issue_office']) ? $manifest['issue_office'] : '';
            
            $row = array();
            $row[] = $no;
            $row[] = $item['id_affiliate'].'<br>'.$item['nama_affiliate'];
            $row[] = $item['id_jamaah'].'<br>'.$item['nama'];
            $row[] = $item['nama_produk'].' '.$item['nama_room_category'];
            $row[] = !empty($room_type['type']) ? $room_type['type'] : '';
            $row[] = !empty($embarkasi['departure']) ? $embarkasi['departure'] : '';
            $row[] = $item['telp'].'<br>'.$item['telp_2'];
            $row[] = ($item['kelamin'] === '2') ? 'Pria' : 'Perempuan';
            $row[] = $item['tempat_lahir'].'<br>'.$item['tanggal_lahir'];
            $row[] = $item['no_passport'].'<br>'.$item['nama_passport'];
            $row[] = $item['issue_date'].'<br>'.$item['issue_office'];
            
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
}
