<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kuotaadmin_model extends CI_Model{
   
   
    private $_table="kuota_booking";
    private $_primary="id_booking_kuota";

     private $_jamaah_kuota="jamaah_kuota";
    private $_id_kuota="id_kuota";

    private $_booking="booking";
    private $_id_booking="id_booking";

    public function generate_kode($idx){
        $today=date('ymd');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }

    public function save_get_kuota($data){
        $initial = "K";
        $arr = array(
                                          
            'id_affiliate' => $data['id_affiliate'],
            'id_affiliate_type' => $data['affiliate_type'],
            'status'=>0,
            'status_fee'=>0,
            'harga' => $data['harga_jamaah_kuota'],
            'kode_unik' => $data['unique_digit'],
            'tgl_pembelian' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_table,$arr);
        $id_booking_kuota =  $this->db->insert_id(); 
        $this->db->update($this->_table,
                    array('invoice'=> $this->generate_kode($initial.$id_booking_kuota)),
                    array('id_booking_kuota'=>$id_booking_kuota));

        
            $id_affiliate = $this->input->post('id_affiliate');
            $sql = "SELECT *
                FROM view_all_refrensi_fee 
                WHERE id_user = '".$id_affiliate."' 
                 ";
                $query = $this->db->query($sql)->result_array();

            if(count($query) > 0){
              foreach($query as $key=>$value){
                $sponsor = $value['id_refrensi'];
                $fee_sponsor_jamaah_kuota  = $value['fee_sponsor_jamaah_kuota'];

        $jml_kuota = $this->input->post('jml_kuota');
        for($i=0; $i < $jml_kuota; $i++) {
            $arr = array(
                'id_booking' => $id_booking_kuota,   
                'nama' => $this->input->post('id_affiliate').' - '.($i + 1),
                'id_affiliate' => $this->input->post('id_affiliate'),
                'id_affiliate_type' => $data['affiliate_type'],
                'sponsor' => $sponsor,
                'harga' => $data['harga_jamaah_kuota'],
                'fee_refrensi' => $fee_sponsor_jamaah_kuota,
                'status'=>0,
                 'tgl_pembelian' => date('Y-m-d H:i:s'),
                 'create_date' => date('Y-m-d H:i:s'),
                 'create_by'=>$this->session->userdata('id_user'),
               );
            // $this->db->insert('jamaah_kuota', $arr);
             $this->db->trans_begin(); 
        
        $this->db->insert($this->_jamaah_kuota,$arr);
        $id_kuota =  $this->db->insert_id(); 
        $this->db->update($this->_jamaah_kuota,
                    array('id_jamaah'=> $this->generate_kode_jamaah($id_kuota)),
                    array('id_kuota'=>$id_kuota));
        }

    }
}

         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('kuota_admin/report_kuota/'.$id_booking_kuota.'');
            return true;
        }
    }

    public function save($data,$schedule){

      $invoice = $this->input->post('invoice');

     $sql = "SELECT * from booking where 
            invoice = '".$invoice."' 
          ";
        $query = $this->db->query($sql)->result_array();

    if(count($query) > 0){
      foreach($query as $key=>$value){
         $booking = $value['id_booking'];
      }
         $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
      }

       $arr = array(
                'id_booking'=> $booking,
                'id_schedule'=> $id_schedule,
                'id_affiliate'=> $data['id_affiliate'],
                'id_jamaah'=> $data['id_jamaah'],
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
                // 'ket_keberangkatan'=>$data['select_status_hubungan'],
                'hubkeluarga'=> $data['hubungan'],
                'keluarga'=> $data['select_keluarga'],
                'payment_date'=>$data['payment_date'],
                 'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],

                'no_pasport'=> $data['no_pasport'],
                'room_type'=> $data['select_type'],
                'category'=> $room_category,
                'issue_office'=> $data['issue_office'],
                'isui_date'=> $data['isue_date'],

                'merchandise'=> $data['select_merchandise'],
                'muhrim'=> $data['muhrim'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> 0,
                'akomodasi'=> 0,
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> $harga,
                // 'id_status_jamaah'=> 1,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status_identitas'=> $data['status_identitas'],
                'status_kk'=> $data['status_kk'],
                'status_photo'=> $data['status_photo'],
                'status_pasport'=> $data['status_pasport'],
                'status_vaksin'=> $data['status_vaksin'],
                'status_buku_nikah'=> $data['status_buku_nikah'],
                // 'status_claim_fee'=>$data['status_claim_fee'],
                'pic1' => isset($data['pic1']) ? $data['pic1']: '',
                'pic2' => isset($data['pic2']) ? $data['pic2']: '',
                'pic3' => isset($data['pic3']) ? $data['pic3']: '',
                'pic4' => isset($data['pic4']) ? $data['pic4']: '',
                'pic5' => isset($data['pic5']) ? $data['pic5']: '',
                'pic6' => isset($data['pic6']) ? $data['pic6']: '',
                'status'=> 1,
                'tipe_jamaah'=>4,
                // 'kode_unik'=>$data['unique_digit']
                // 'alumni'=> 0

        );     
      $this->db->insert('registrasi_jamaah',$arr);

        $arr = array(
           'nama' => $data['nama'],
            'status' => 2,
           'update_by'=>$this->session->userdata('id_user'),
           'update_date'=>date('Y-m-d H:i:s'),
        );       
              
         $this->db->update($this->_jamaah_kuota,$arr,array('id_kuota'=>$data['id_kuota']));


        $hitung_seat = $seats;
         if ($hitung_seat == 0){
            $hitung_seat == 0;
            
        }else{
            $hitung_seat =($hitung_seat - 1);
        }

      $arr = array( 
            'seats' => $hitung_seat,  
        );       
     $this->db->update('schedule',$arr,array('id_schedule'=>$id_schedule));


         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }

      }else{


      
  
        $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
      }

      // print_r($id_schedule);
      
        $arr = array(
            'invoice' => $data['invoice'],                         
            'id_user_affiliate' => $data['id_affiliate'],
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => $data['tgl_pembelian'],
            'harga' => $harga,
            'create_date' => date('Y-m-d H:i:s'),
             'payment_date'=>$data['payment_date'],
            'create_by'=>$this->session->userdata('id_user'),
            // 'kode_unik'=> $data['kode_unik'],
           
            'tipe_jamaah'=>4,
            'status'=>1
        );       
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        
        $invoice =  $this->db->insert_id(); 

            $arr = array(
                'id_booking'=> $id_booking,
                'id_schedule'=> $id_schedule,
                'id_affiliate'=> $data['id_affiliate'],
                'id_jamaah'=> $data['id_jamaah'],
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
                // 'ket_keberangkatan'=>$data['select_status_hubungan'],
                'hubkeluarga'=> $data['hubungan'],
                'keluarga'=> $data['select_keluarga'],
                'payment_date'=>$data['payment_date'],
                 'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],

                'no_pasport'=> $data['no_pasport'],
                'room_type'=> $data['select_type'],
                'category'=> $room_category,
                'issue_office'=> $data['issue_office'],
                'isui_date'=> $data['isue_date'],

                'merchandise'=> $data['select_merchandise'],
                'muhrim'=> $data['muhrim'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> 0,
                'akomodasi'=> 0,
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> $harga,
                // 'id_status_jamaah'=> 1,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'status_identitas'=> $data['status_identitas'],
                'status_kk'=> $data['status_kk'],
                'status_photo'=> $data['status_photo'],
                'status_pasport'=> $data['status_pasport'],
                'status_vaksin'=> $data['status_vaksin'],
                'status_buku_nikah'=> $data['status_buku_nikah'],
                // 'status_claim_fee'=>$data['status_claim_fee'],
                'pic1' => isset($data['pic1']) ? $data['pic1']: '',
                'pic2' => isset($data['pic2']) ? $data['pic2']: '',
                'pic3' => isset($data['pic3']) ? $data['pic3']: '',
                'pic4' => isset($data['pic4']) ? $data['pic4']: '',
                'pic5' => isset($data['pic5']) ? $data['pic5']: '',
                'pic6' => isset($data['pic6']) ? $data['pic6']: '',
                'status'=> 1,
                'tipe_jamaah'=>4,
                // 'kode_unik'=>$data['unique_digit']
                // 'alumni'=> 0

        );     
      $this->db->insert('registrasi_jamaah',$arr);

        $arr = array(
           'nama' => $data['nama'],
            'status' => 2,
           'update_by'=>$this->session->userdata('id_user'),
           'update_date'=>date('Y-m-d H:i:s'),
        );       
              
         $this->db->update($this->_jamaah_kuota,$arr,array('id_kuota'=>$data['id_kuota']));


        $hitung_seat = $seats;
         if ($hitung_seat == 0){
            $hitung_seat == 0;
            
        }else{
            $hitung_seat =($hitung_seat - 1);
        }

      $arr = array( 
            'seats' => $hitung_seat,  
        );       
     $this->db->update('schedule',$arr,array('id_schedule'=>$id_schedule));

         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }

       } 
    
}
    
    
      function cek($no_identitas){
        $invoice = $this->input->post('invoice');
        $query = $this->db->query("SELECT * FROM data_jamaah_aktif Where no_identitas ='$no_identitas' and invoice='$invoice'");
        return $query;
    }

      function cek_id_jamaah($id_jamaah){
        $id_jamaah = $this->input->post('id_jamaah');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where id_jamaah ='$id_jamaah'");
        return $query;
    }
    
    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = " SELECT * FROM grouping_jamaah_kuota_aktif  
                    WHERE 1=1
                    ";
        
        if($q){
         

            $sql .=" AND affiliate LIKE '%{$q}%'
                    OR id_affiliate LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY tgl_pembelian DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    

    
    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

    function get_fee_jamaah_kuota($affiliate_type='') {
      $this->db->where("id_affiliate_type",$affiliate_type);
      return $this->db->get("affiliate_type");
    }


     public function get_booking_kuota($id_booking_kuota){
 

         $sql = "SELECT * from faktur_jamaah_kuota where id_booking_kuota = {$id_booking_kuota}";
  
        return $this->db->query($sql)->row_array();  
    }

    function lap_data_jamaah($id_booking_kuota) {
        $this->db->select('*');
        $this->db->from('faktur_jamaah_kuota');
        $this->db->where('id_booking_kuota',$id_booking_kuota);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }



     public function get_jamaah_kuota_aktif($id_booking){
    
        $sql ="SELECT * from detail_jamaah_kuota_aktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

    }


      public function get_jamaah_kuota_mutasi_nama($id_jamaah){
    
        $sql ="SELECT * from detail_jamaah_kuota_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

    }

    function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
      
          $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.'</strong></td>
                   <td><strong>'.$row->keterangan.'</strong></td>
                 
                   <td> <input name="radio[]" class="radio1" type="radio" id="radio1[]" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }


     public function get_booking_kuota_aktif($id_booking_kuota){
 

         $sql = "SELECT * from grouping_jamaah_kuota_aktif where id_booking_kuota = {$id_booking_kuota}";
  
        return $this->db->query($sql)->row_array();  
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }
}
