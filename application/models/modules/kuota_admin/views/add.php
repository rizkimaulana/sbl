 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">TAMBAH JAMAAH KUOTA</h3>
       <form action="<?php echo base_url();?>kuota_admin/generate_data_kuota"  role="form" method="post">
      <!--   <form role="form" method="post" action="<?php echo base_url();?>kuota_admin/save"> -->
       <?php $tiga_digit = random_3_digit_kuota(); ?>
                        <input type='hidden' name='unique_digit' value='<?= $tiga_digit; ?>'>
           <div class="form-group">
                <label>Affiliate Type :</label>
                <select  required name="affiliate_type" id="affiliate_type" class="form-control ">
                      <option value="">- PILIH AFFILIATE TYPE -</option>
                     <!--  <option></option> -->
                        <?php foreach($select_affiliate_type as $st){?>
                            <option  value="<?php echo $st->id_affiliate_type;?>"required><?php echo $st->affiliate_type;?> </option>
                        <?php } ?>
                      </select>
                      <input type ="hidden" name="fee_jamaah_kuota" id="fee_jamaah_kuota" class="form-control" >
            </div>
            <div class="form-group">
                <label>Affiliate : </label>
                <select required name="id_affiliate" id="id_affiliate" class="form-control ">
                      <option value="">- PILIH Affiliate yg Beli Kuota -</option>
                </select>
            </div>
             <div class="form-group">
                <label>Jumlah Kuota </label>
                <select required name="jml_kuota"  class="form-control ">
               <?php 
                  for($i=22; $i<=100; $i++){
               ?>
             <option value="<?php echo $i;?>"><?php echo $i;?></option>
              <?php
               }
              ?>
             </select> 
            </div> 
            <?php 
              $biaya_setting_ = $this->kuotaadmin_model->get_key_val();
                foreach ($biaya_setting_ as $key => $value){
                  $out[$key] = $value;
                }
            ?>
             <div class="form-group">
                <input type='hidden'class="form-control" name="harga_jamaah_kuota" value="<?php echo $out['HARGA_JAMAAH_KUOTA'];?>" required>
            </div>

            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lanjut</button>
            <a href="<?php echo base_url();?>kuota_admin" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
<script>
        $(document).ready(function(){
           
             $("#affiliate_type").change(function (){
                var url = "<?php echo site_url('kuota_admin/add_ajax_affiliate_type');?>/"+$(this).val();
                $('#id_affiliate').load(url);
                return false;
            })


            $("#affiliate_type").change(function(){
            var affiliate_type=$("#affiliate_type").val();
            console.log(affiliate_type);
            $.ajax({
                url:"<?php echo site_url('kuota_admin/get_fee_kuota');?>",
                type:"POST",
                data:"affiliate_type="+affiliate_type,
                cache:false,
                success:function(html){
                    $("#fee_jamaah_kuota").val(html);
                    
                }
            })
        })
        });
</script>
