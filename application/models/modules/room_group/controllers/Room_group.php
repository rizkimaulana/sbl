<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_group extends CI_Controller{
	var $folder = "room_group";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(ROOM_GROUP,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','manifest');	
		$this->load->model('roomgroup_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/room_group');
		
	}
	

	

	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_category(){
	    
	    return $this->db->get('room_category')->result();
	}

	private function _select_hotel(){
	    
	    return $this->db->get('hotel')->result();
	}

	
public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->roomgroup_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->room_group_name.'</td>';
	                $rows .='<td width="10%">'.$r->nama_hotel.'</td>';
	                $rows .='<td width="10%">'.$r->room_type.'</td>';
	                $rows .='<td width="10%">'.$r->room_category.'</td>';
	                $rows .='<td width="9%">'.$r->kapasitas.'</td>';
	                $rows .='<td width="40%">'.$r->keterangan.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'room_group/edit/'.$r->id_room_group.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room_group('."'".$r->id_room_group."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'room_group/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->roomgroup_model->save($data);
	     if($send)
	        redirect('room_group');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(ROOM_GROUP,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_type'=>$this->_select_type(),
	    	'select_category'=>$this->_select_category(),
	    	'select_hotel'=>$this->_select_hotel()			
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);		
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(ROOM_GROUP,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('room_group',array('id_room_group'=>$id))->row_array();
	   if(!$get)
	        show_404();
	        
	    $data = array('select_type'=>$this->_select_type(),
	    	'select_category'=>$this->_select_category(),
	    	'select_hotel'=>$this->_select_hotel()		
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->roomgroup_model->update($data);
	     if($send)
	        redirect('room_group');
		
	}
	
	
	public function ajax_delete($id_room)
	{
		if(!$this->general->privilege_check(ROOM_GROUP,'remove'))
		    $this->general->no_access();
		$send = $this->roomgroup_model->delete_by_id($id_room_group);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('room');
	}


}
