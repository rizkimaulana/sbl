 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});


</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_reschedule/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        


      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
                	     <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                 	 
                 

                    <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama  </label>
                               <div class="col-sm-5">
                                    <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $aktivasi['nama']?>"  readonly='true' required/>
                                </div>
                    </div>
                 	<div class="form-group">
	                    <label class="col-lg-2 control-label">ID JAMAAH</label>
	                    <div class="col-lg-5">
	                       <input type="text" name="id_pengajuan" value="<?php echo $aktivasi['id_pengajuan']?>"  class="form-control" readonly="true">
	                    </div>
	                  </div>


                     <div class="form-group">
                      <label class="col-lg-2 control-label">Metode Pembayaran</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_pay_method" id='select_pay_method'>
                      <option ="">- PILIH Metode Pembayaran -</option>
                    <?php foreach($select_pay_method as $sk){?>
                            <option value="<?php echo $sk->pay_method;?>"><?php echo $sk->pay_method;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                  </div>

                     <div class="form-group">
	                    <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="ket_pembayaran" required></textarea>
	                    </div>
	                  </div>
<div id="hidden_div" style="display: none;">
		            <div class="form-group">
				        <label class="col-lg-2 control-label">Bukti Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="file" name="pic[]"  id="pic" class="form-control" required>
				        </div>
				    </div>
         

           		<div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_bank_transfer" id="select_bank_transfer">
                      <option ="">- PILIH BANK TRANSFER -</option>
                    <?php foreach($select_bank_transfer as $sk){?>
                            <option value="<?php echo $sk->id;?>"><?php echo $sk->nama;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                  </div>
</div>
				    <div class="form-group">
				        <label class="col-lg-2 control-label">Pembayaran DP</label>
				        <div class="col-lg-5">
				            <input type="text" name="dp" value="<?php echo $aktivasi['dp']?>"  class="form-control" readonly="true">
				        </div>
				    </div>

           			 <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div>

	                 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aktivasi</button>
           			 <a href="<?php echo base_url();?>pengajuan_cicilantalangan" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   
      
  </form>       
                
</div>
   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>
<script type="text/javascript">
      document.getElementById('select_pay_method').addEventListener('change', function () {
    var style = this.value == 'TRANSFER' ? 'block' : 'none';
    document.getElementById('hidden_div').style.display = style;
    // document.getElementById('hidden_div2').style.display = style;
    // $("#datepicker").prop('required',true);
     select_pay_method = $(this).val();

      console.log(select_pay_method);

      if(select_pay_method=='TRANSFER' ){
        $("#pic").prop('required',true);
        $("#select_bank_transfer").prop('required',true);
      }else{
         $("#pic").prop('required',false);
         $("#select_bank_transfer").prop('required',false);
      }
    });


  </script>