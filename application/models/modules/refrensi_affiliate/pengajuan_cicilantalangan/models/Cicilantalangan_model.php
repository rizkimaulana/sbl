<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cicilantalangan_model extends CI_Model{
   
   
    private $_table="pengajuan_cicilan";
    private $_primary="id_pengajuan";


     public function generate_kode($idx){
        $today=date('ymd');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save($data,$schedule){

         $biaya_setting_ = $this->get_key_val();
            foreach ($biaya_setting_ as $key => $value){
            $out[$key] = $value;
        }

        $initial = "CT";

      $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $pindah_bulan_menunggu = $value['waktutunggu'];
        $id_tipe_jamaah = $value['id_tipe_jamaah'];
       
      }
        
            
        $arr = array(
            'nama' => $data['nama'],
            'id_schedule' => $id_schedule,
            'identitas' => $data['identitas'],
            'sex' => $data['select_kelamin'],
            'tgl_lahir' => $data['tgl_lahir'],
            'tempat_lahir' => $data['tempat_lahir'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'id_product' => $id_product,
            'category' => $room_category,
            'room_type' => $id_room_type,
            'provinsi_id' => $data['provinsi_id'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'alamat' => $data['alamat'],
            'alamat_domisili' => $data['alamat_domisili'],
            'pendidikan_terakhir' => $data['select_pendidikan'],
            'status_nikah' => $data['select_kawin'],
            'ibu_kandung' => $data['ibu_kandung'],
            'nama_perusahaan' => $data['nama_perusahaan'],
            'jenis_pekerjaan' => $data['select_jenis_pekerjaan'],
            'status_karyawan' => $data['select_status_karyawan'],
            'jabatan' => $data['jabatan'],
            'no_pegawai' => $data['no_pegawai'],
            'gaji' => $data['gaji'],
            'via_bank' => $data['select_bank'],
            'no_sk' => $data['no_sk'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'dp'=> $out['DP_CICILAN_PEMBAYRAN_TALANGAN'],
            'status'=>0,
            'harga_paket'=>$harga,
        );       
        
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_table,$arr);
        $id=  $this->db->insert_id(); 
        $this->db->update($this->_table,
                    array('id_pengajuan'=> $this->generate_kode($initial.$id)),
                    array('id'=>$id));
    
        
      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
       
    }

    function cek($id_affiliate){
        $query = $this->db->query("SELECT * FROM affiliate Where id_affiliate ='$id_affiliate'");
        return $query;
    }
    

    public function update($data){
        
        $arr = array(
        
            'nama' => $data['nama'],
            'identitas' => $data['identitas'],
            'sex' => $data['select_kelamin'],
            'tgl_lahir' => $data['tgl_lahir'],
            'tempat_lahir' => $data['tempat_lahir'],
            'telp' => $data['telp'],
            'email' => $data['email'],

            'alamat' => $data['alamat'],
            'alamat_domisili' => $data['alamat_domisili'],
            'pendidikan_terakhir' => $data['select_pendidikan'],
            'status_nikah' => $data['select_kawin'],
            'ibu_kandung' => $data['ibu_kandung'],
            'nama_perusahaan' => $data['nama_perusahaan'],
            'jenis_pekerjaan' => $data['select_jenis_pekerjaan'],
            'status_karyawan' => $data['select_status_karyawan'],
            'jabatan' => $data['jabatan'],
            'no_pegawai' => $data['no_pegawai'],
            'gaji' => $data['gaji'],
            'via_bank' => $data['select_bank'],
            'no_sk' => $data['no_sk'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        // if($data['password']!='') || ($data['provinsi']!='') || ($data['kecamatan']!='') || ($data['kabupaten']!=''){
         if($data['password']!='') {
          
            $arr['password'] = md5($data['password']);
          
        }
         if($data['provinsi_id']!='' && $data['kecamatan_id']!='' && $data['kabupaten_id']!='') {
          
            // $arr['password'] = ($data['password']);
            $arr['provinsi_id'] = ($data['provinsi_id']);
            $arr['kecamatan_id'] = ($data['kecamatan_id']);
            $arr['kabupaten_id'] = ($data['kabupaten_id']);

        }

       if(isset($data['pic1'])){
            
            $arr['pic1'] = $data['pic1'];

        }

         $this->db->update('pengajuan_cicilan',$arr,array('id_pengajuan'=>$data['id_pengajuan']));


         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }


      public function get_detail($id){
         $sql = "SELECT * from view_affiliate
                    WHERE id_aff={$id}
                ";

        return $this->db->query($sql)->row_array();
    }

    public function getEditaffiliate($id){
        $query = $this->db->query("
                    SELECT a.*,b.keterangan,c.nama as provinsi, d.nama as kabupaten, e.nama as kecamatan, f.keterangan as hubungan, g.affiliate_type FROM affiliate a 
                    LEFT JOIN wilayah_provinsi c ON c.provinsi_id = a.provinsi_id
                    LEFT JOIN wilayah_kabupaten d ON d.kabupaten_id = a.kabupaten_id
                    LEFT JOIN wilayah_kecamatan e ON e.kecamatan_id = a.kecamatan_id
                    LEFT JOIN family_relation f ON f.id_relation = a.hubwaris
                    LEFT JOIN affiliate_type g ON g.id_affiliate_type = a.id_affiliate_type
                    LEFT JOIN status b ON b.kdstatus = a.status
                    WHERE 1=1 and a.id_aff ='$id' LIMIT 1
                    ");
        return $query;
    }
    
    public function get_data($offset,$limit,$q=''){

          $sql = " SELECT * from pengajuan_talangancicilan
                    WHERE 1=1 and status ='0'
                    ";
        
        if($q){
            
            $sql .=" AND id_pengajuan LIKE '%{$q}%' 
            		or identitas LIKE '%{$q}%'
                    or sex LIKE '%{$q}%'
                    or tgl_lahir LIKE '%{$q}%'
                    or tempat_lahir LIKE '%{$q}%'
                    or telp LIKE '%{$q}%'
                    or email LIKE '%{$q}%'
                    or provinsi_id LIKE '%{$q}%'
                    or kabupaten_id LIKE '%{$q}%'
                    or kecamatan_id LIKE '%{$q}%'
                    or alamat LIKE '%{$q}%'
                    or alamat_domisili LIKE '%{$q}%'
                    or pendidikan_terakhir LIKE '%{$q}%'
                    or status_nikah LIKE '%{$q}%'
                    or ibu_kandung LIKE '%{$q}%'
                    or nama_perusahaan LIKE '%{$q}%'
                    or jenis_pekerjaan LIKE '%{$q}%'
                    or status_karyawan LIKE '%{$q}%'
                    or jabatan LIKE '%{$q}%'
                    or no_pegawai LIKE '%{$q}%'
                    or gaji LIKE '%{$q}%'
                    or via_bank LIKE '%{$q}%'
                    or no_sk LIKE '%{$q}%'
                    or dp LIKE '%{$q}%'
                    or harga_paket LIKE '%{$q}%'
                    or status LIKE '%{$q}%'
                    or update_by LIKE '%{$q}%'
                    or create_by LIKE '%{$q}%'
                    or update_date LIKE '%{$q}%'
                    or create_date LIKE '%{$q}%'
                    or provinsi LIKE '%{$q}%'
                    or kabupaten LIKE '%{$q}%'
                    or kecamatan LIKE '%{$q}%'
                    or product LIKE '%{$q}%'
                    or room_category LIKE '%{$q}%'
                    or type LIKE '%{$q}%'
                    or bln_berangkat LIKE '%{$q}%'
                    or bulan_menunggu  LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY id_pengajuan DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    function get_data_jabatan_id($affiliate_type='') {
      $this->db->where("id_affiliate_type",$affiliate_type);
      return $this->db->get("affiliate_type");
    }

     public function delete($id_aff){


          $get_img  = $this->db->select('pic1')
                                ->where('id_aff',$id_aff)->get('affiliate')->row_array();
        
        //remove all images
        if($get_img){
            $img = array('pic1');
            foreach($img as $im){
                
                if($get_img[$im])
                    unlink('./assets/images/affiliate/'.$get_img[$im]);
            }
        
        }
        
        return $this->db->delete('affiliate',array('id_aff'=>$id_aff));
  
    }
    
    public function delete_by_id($id_aff)
    {
        $this->db->where('id_aff', $id_aff);
        $this->db->delete($this->_table);
    }
    
    function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
      
             $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="12"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.', wkt tunggu '.$row->waktutunggu.' Bulan</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.' - '.$row->hari.' hari</strong></td>
                   <td><strong>'.$row->keterangan.'</strong></td>
                   <td><strong>'.number_format($row->harga).'</strong></td>
                   <td>'.$row->status_jadwal.', <strong> JAMAAH ('.$row->tipe_jamaah.')</strong></td>
                   <td><strong>'.$row->maskapai.'</strong></td>
                 
                   <td> <input name="radio[]" class="radio1" type="radio" id="radio1[]" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }

       function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }


     public function update_status_tolak($id_pengajuan){
    
      
        $arr = array(
        
           'status' => 2,
            
           
        );       
              
         $this->db->update($this->_table,$arr,array('id_pengajuan'=>$id_pengajuan));

  
    }

    public function get_pic_aktivasi($id_pengajuan){

       // $sql ="SELECT * from pengajuan_talangancicilan WHERE  id_pengajuan  = ?
       //        ";
       //  return $this->db->query($sql,array($id_pengajuan))->result_array();

                $sql = "SELECT * from pengajuan_talangancicilan where id_pengajuan = '$id_pengajuan'
                ";
        return $this->db->query($sql)->row_array();  
    }
}
