<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Refrensi_affiliate extends CI_Controller{
	var $folder = "refrensi_affiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(REFRENSI_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','refrensi_affiliate');	
		$this->load->model('refrensiaffiliate_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/refrensi_affiliate');
		
	}
	
	public function ajax_list() {

        $hasil = $this->refrensiaffiliate_model->get_ajax_list()->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;

            $row = array();
            $row[] = $no;

            $row[] = $item['Upline'];
            $row[] = $item['IDUpline'];
            

            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
	

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->refrensiaffiliate_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="10%">'.$i.'</td>';
	               
	               
	                $rows .='<td width="40%">'.$r->Upline.'</td>';
	                $rows .='<td width="40%">'.$r->IDUpline.'</td>';
	             
	               //  $rows .='<td width="20%" align="center">';
	                
	               //  $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'profil/edit/'.$r->id_user.'">
	               //              <i class="fa fa-pencil"></i> Edit
	               //          </a> ';

	               //  $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'profil/detail/'.$r->id_user.'">
	               //              <i class="fa fa-pencil"></i> Detail
	               //          </a> ';
	                          
	               // $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'refrensi_affiliate/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	



	

	
	


}
