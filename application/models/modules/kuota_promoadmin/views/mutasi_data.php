
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List ORDER JAMAAH KUOTA</b>
                </div>

                 <div class="panel-body">
                        <?php if($this->session->flashdata('info')) { ?>
                                  <div class="alert alert-success">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                                  </div>
                              <?php } ?>
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>INVOICE`</th>
                                    <th>AFFILIATE </th>
                                    <th>ID AFFIIATE </th>
                                    
                                  
                                    <th>NAMA</th>
                                   
                                   
                                    <th>HARGA</th>
                                   <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($booking as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $pi['invoice']?>  </td>
                                      <td><?php echo $pi['affiliate']?> </td>
                                      <td><?php echo $pi['id_affiliate']?>  </td> 
                                      
                                    
                                      <td><?php echo $pi['nama']?>  </td>
                                      <td><?php echo number_format($pi['harga'])?>  </td>
 
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn-sm btn-primary" title='Edit' href="<?php echo base_url();?>kuota_promoadmin/mutasi_data_jamaah/<?php echo $pi['id_kuota']; ?>/<?php echo $pi['id_booking_kuota']; ?>">Mutasi Data</a>
                                   
                                         </div> 
                                                                                         
                                  </td>  
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
