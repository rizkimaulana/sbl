<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hotel_model extends CI_Model{
   
   
    private $_table="hotel";
    private $_primary="id_hotel";

    public function save($data){
    
      
        $arr = array(
            'id_bintang' => $data['select_bintang'],
            'nama_hotel' => $data['nama_hotel'],
            'keterangan' => $data['keterangan'],
           
        );       
        
         return $this->db->insert('hotel',$arr);
    }


    public function update($data){
        
    $arr = array(
            'id_bintang' => $data['select_bintang'],
            'nama_hotel' => $data['nama_hotel'],
            'keterangan' => $data['keterangan'],
           
        );        
              
        return $this->db->update($this->_table,$arr,array('id_hotel'=>$data['id_hotel']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = " SELECT a.*, b.bintang FROM hotel a
                        LEFT JOIN bintang b ON b.id_bintang = a.id_bintang
                    WHERE 1=1
                    ";
        
        if($q){
         

            $sql .=" AND nama_hotel LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY id_hotel DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_hotel)
    {
        $this->db->where('id_hotel', $id_hotel);
        $this->db->delete($this->_table);
    }
    
    
}
