<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel extends CI_Controller{
	var $folder = "hotel";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(HOTEL,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','manifest');	
		$this->load->model('hotel_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/hotel');
		
	}
	

	

	private function _select_bintang(){
	    
	    return $this->db->get('bintang')->result();
	}


	
	

	
public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->hotel_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->bintang.'</td>';
	                $rows .='<td width="20%">'.$r->nama_hotel.'</td>';
	               
	                $rows .='<td width="40%">'.$r->keterangan.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'hotel/edit/'.$r->id_hotel.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_hotel('."'".$r->id_hotel."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'hotel/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->hotel_model->save($data);
	     if($send)
	        redirect('hotel');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(HOTEL,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_bintang'=>$this->_select_bintang()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);		
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(HOTEL,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('hotel',array('id_hotel'=>$id))->row_array();
	   if(!$get)
	        show_404();
	        
	    $data = array('select_bintang'=>$this->_select_bintang()
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->hotel_model->update($data);
	     if($send)
	        redirect('hotel');
		
	}
	
	
	public function ajax_delete($id_hotel)
	{
		if(!$this->general->privilege_check(HOTEL,'remove'))
		    $this->general->no_access();
		$send = $this->hotel_model->delete_by_id($id_hotel);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('hotel');
	}


}
