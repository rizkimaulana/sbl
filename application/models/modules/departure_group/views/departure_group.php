 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Departure group</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>departure_group/save">
            
            <div class="form-group">
            	<label>Departure date </label>
                <div class='input-group date' id='datetimepicker'>
	                  <input type='text' name="date_schedule" id="date_schedule" class="form-control" placeholder="yyyy/mm/dd "/>
	                  <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                  </span>
                </div>
                 <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check available tours</button>
            </div>
            
			
	          <div class="form-group">
                <label>Total Seats</label>
                <input class="form-control" name="seats" required>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>departure_group" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
			$(function () {
				$('#datetimepicker').datetimepicker({
					format: 'YYYY-MM-DD',
                });
				
				$('#datepicker').datetimepicker({
					format: 'DD MMMM YYYY',
				});
				
				$('#bulan').datetimepicker({
					format: 'MMMM',
				});
				
				$('#timepicker').datetimepicker({
					format: 'HH:mm'
				});
			});
		</script>

<script type="text/javascript">
 
  var base_url = "<?php echo base_url(); ?>";
  // var currency = "EUR"; //$this->booking->show_symbol($company_info->company_currency
  // var destination = "Destination";
  // var time = "Departure time";
  // var price = "Price";
  // var currently = "Currently there are no tours available";
  // var from = "from";
  var date_schedule = "date_schedule";
</script>

<script type="text/javascript" src="  /ajax.js"></script>
