﻿
    <!-- LOGO HEADER END-->
   <script src="https://www.google.com/recaptcha/api.js?render=onload&hl=id" async defer></script>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Silahkan Login Untuk Melanjutkan </h4>

                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                   
                    
        
                        <div class="panel panel-default top150">
                            <div class="panel-heading"><h4 style="margin: 5px"><i class="glyphicon glyphicon-user"></i> Login Back Office</h4></div>
                            
                            <div class="panel-body">
                                <form role="form" method="post" action="<?php echo base_url();?>frontend/login/do_login">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Username" name="username" type="username" autofocus>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                        </div>
                                     <!--   <div class="g-recaptcha" data-sitekey="6LcsvR8UAAAAALBMEaDsrZWSxtEvCuhDUfuvr-in"></div> -->
                                     <div class="form-group">
                <?php echo $captcha ?>
            </div>
           <div class="g-recaptcha" data-sitekey="6LehTiEUAAAAALdRe6-n1Q4tpggkR5zza7kVaIxm"></div>
                                        <span style="color:#d32132"><?php echo isset($message) ? $message:'';?></span>
                                        <div class="pull-right" style="padding-top:30px"> <button type="submit" class="btn btn-info">&nbsp;&nbsp;Login&nbsp;&nbsp;</button>
                                    </fieldset>
                                </form>
                            </div>
                            <!-- <div class="panel-body">
                                <div id="konfirmasi"></div>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="text" id="username" name="username" autofocus value="" placeholder="Username" class="form-control" />
                                </div> 
                                
                                <div class="input-group top15">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" id="password" name="password" value="" placeholder="Password" class="form-control"/>
                                </div> 
                                <div class="login-actions">
                                    <button class="button btn btn-dafault btn-large col-lg-12 top15">Login</button>
                                </div> 
                            </div> -->
                        </div> 
        
        
                   
                </div>
                <div class="col-md-6">
                    <div class="alert alert-info">
                        <p><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo base_url()?>/assets/images/ied-mubarok.jpg" alt="Menyambut Idul Adha" width="345" height="345" /></p>
<p style="text-align: center;">&ldquo;<em>Orang-orang yang mengerjakan ibadah haji dan umrah adalah tamu-tamu Allah. Allah memberi kepada mereka apa yang mereka minta dan cita-citakan, dan Dia mengabulkan semua doa mereka itu yang bersungguh-sungguh berdoa kepadanya, kemudian Dia akan mengganti semua harta yang mereka belanjakan untuk-Nya, satu dirham menjadi sejuta dirham yang akan dia terima</em>&rdquo;.</p>
<p style="text-align: center;">(HR Baihaqi)</p>
<!--<p style="text-align: center;">&nbsp;Kurnia Ramadhan kembali tiba</p>
<p style="text-align: center;">Selamat menyambut bulan suci Ramadhan 2017 (1438 H)</p>-->
<p>&nbsp;</p>
                                             
                                              
                    </div>
                   
                </div>

            </div>
        </div>
    </div>
</div>
</div>
    <!-- CONTENT-WRAPPER SECTION END-->
   
<!-- jQuery -->
<!-- 
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script> 
   
    <script type="text/javascript">
    $("#login").submit(function(event) {
        event.preventDefault();
        var data    = $('#login').serialize();
        $("#konfirmasi").html("<div class='alert alert-info'><i class='icon icon-spinner icon-spin'></i> Checking...</div>")
        $.ajax({
            type: "POST",
            data: data,
            url: "<?php echo base_URL(); ?>login/act_login",
            success: function(r) {
                if (r.log.status == 0) {
                    $("#konfirmasi").html("<div class='alert alert-danger'>"+r.log.keterangan+"</div>");
                } else {
                    $("#konfirmasi").html("<div class='alert alert-success'>"+r.log.keterangan+"</div>");
                    window.location.assign("<?php echo base_url(); ?>dashboard"); 
                }
            }
        });
    });
</script> -->