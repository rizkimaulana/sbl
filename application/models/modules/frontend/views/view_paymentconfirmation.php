﻿
		
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Konfirmsi Pembayaran</h4>

                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        Silahkan Melakukan Konfirmasi Pembayaran
                    </div>
                </div>

            </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           INPUT DATA PEMBAYARAN
                            <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                        </div>
                        <div class="panel-body">
                       <form action="<?php echo base_url(); ?>payment_confirmation/addPayment_confirmation" method="POST">
                             <div class="form-group">
                                <label for="exampleInputEmail1">Nama Rekening Pengirim</label>
                                <input type="text" name="nama" class="form-control input-sm" id="nama"  placeholder="Nama Rekening Pengirim" />
                                <span class="help-inline"><?php echo form_error('nama'); ?></span>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Alamat E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Masukan Alamat E-mail" />
                                <span class="help-inline"><?php echo form_error('email'); ?></span>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">No Handphone</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Masukan No Handphone" />
                                <span class="help-inline"><?php echo form_error('phone'); ?></span>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Jumlah Pembayaran (Rp.)</label>
                                <input type="text" class="form-control" id="total_payment" name="total_payment" placeholder="Masukan Jumlah Pembayaran " />
                                <span class="help-inline"><?php echo form_error('phone'); ?></span>
                              </div>
                              <div class="form-group">
                                            <label>Bank Tujuan</label>
                                            <select class="form-control" name="destination_bank" id="destination_bank">
                                                <option>MANDIRI : 1312333322 atas nama Roy</option>
                                                <option>BNI : 1312333322 atas nama Roy</option>
                                                <option>BCA : 1312333322 atas nama Roy</option>
                                                
                                            </select>
                                        </div>
                              
                              <div class="form-group">
                                <label for="exampleInputEmail1">Invoice</label>
                                <input type="text" class="form-control" id="invoice" name="invoice" placeholder="Masukan #Invoice " />
                                <span class="help-inline"><?php echo form_error('invoice'); ?></span>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Pembayaran</label>
                                <div class='input-group date' id='datepicker'>
                                  <input type='text' name="date" id="datepicker" class="form-control" placeholder="dd/mm/yyyy "/>
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Keterangan Pembayaran</label>
                                <textarea class="form-control" rows="3" name="information"  placeholder="Jika anda Menggunakan sms Banking silahkan copy text disini  "></textarea>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputFile">File Bukti Transfer</label>
                                <input type="file" id="file_url" name="file_url" />
                                <p class="help-block">File Bukti Transfer PDF,JPG,Image dll.</p>
                              </div>
                          <button type="submit" class="btn btn-default">Submit</button>
                        </form>
                            </div>
                            </div>
                    </div>
             </div>
        </div>
    <!-- CONTENT-WRAPPER SECTION END-->
   
	
  
		
		<script type="text/javascript">
			$(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
		</script>