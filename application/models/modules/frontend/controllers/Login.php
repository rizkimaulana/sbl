<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        
        parent::__construct();
        if($this->session->userdata('is_login'))redirect('dashboard');
        $this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation', 'Recaptcha'));
    }
	public function index($param='')
	{
		           $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);
		if($param == 'error')
			$param = 'Incorrect username or password';
			
		$data = array('title'=>'Login',
			'message'=>$param,
			'base_url'=>base_url(),
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
			);
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/view_login', $data);
		$this->load->view('template_frontend/footer');
		 

	}
	
	
	public function do_login(){


		$data = $this->input->post(null,true);
			$password=$data['password'];
		$password2	= md5($password);
				 $stored_procedure = "call view_user(?,?)";
          $is_login = $this->db->query($stored_procedure,array(
          	'username'=>$data['username'],
          	'password'=>md5(trim($data['password'])),
          ))->row(); 
		// $is_login = $this->db->get_where('view_user',array(
		// 								'username'=>$data['username'],
		// 								'password'=>md5(trim($data['password'])),
		// 								'status'=>1,
		// 								))->row();
		  $this->form_validation->set_rules('username', ' ', 'trim|required');
        $this->form_validation->set_rules('password', ' ', 'trim|required');
        
		 // $stored_procedure = "call view_user(?,?)";
   //        $is_login = $this->db->query($stored_procedure,array(
   //        	'username'=>$data['username'],
   //        	'password'=>md5(trim($data['password'])),
          	
   //        	))->row(); 
 $recaptcha = $this->input->post('g-recaptcha-response');
        $response = array( 'success' => true ); //$this->recaptcha->verifyResponse($recaptcha);
 if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true) {
 	redirect('frontend/login/index/error');
 }else{
 	if($is_login ){

			$session_set = array(
				
				'is_login'	=> true,
				'nama'		=> $is_login->nama,
				'id_user'		=> $is_login->id_user,
				'id_ff'		=> $is_login->id_user,
				'id_user_affiliate'		=> $is_login->id_user_affiliate,
				'id'	=> $is_login->id,
				'username'	=> $is_login->username,
				'jabatan_id'=> $is_login->jabatan_id,
				'photo'      => $is_login->photo,
				'status'  => $is_login->status,
			);
			$this->db->update('user',array('last_login'=>date('Y-m-d H:i:s')),array('id_user'=>$is_login->id_user));
			$this->session->set_userdata($session_set);
			redirect('dashboard');
 }
		// if($is_login ){

		// 	$session_set = array(
				
		// 		'is_login'	=> true,
		// 		'nama'		=> $is_login->nama,
		// 		'id_user'		=> $is_login->id_user,
		// 		'id_ff'		=> $is_login->id_user,
		// 		'id_user_affiliate'		=> $is_login->id_user_affiliate,
		// 		'id'	=> $is_login->id,
		// 		'username'	=> $is_login->username,
		// 		'jabatan_id'=> $is_login->jabatan_id,
		// 		'photo'      => $is_login->photo,
		// 		'status'  => $is_login->status,
		// 		// 'recaptcha' => $this->input->post('g-recaptcha-response'),
  //   //     'response' => $this->recaptcha->verifyResponse($recaptcha)
		// 	);
		// 	$this->db->update('user',array('last_login'=>date('Y-m-d H:i:s')),array('id_user'=>$is_login->id_user));
		// 	$this->session->set_userdata($session_set);
		// 	redirect('dashboard');
		// }else{
		// 				 // $recaptcha = $this->input->post('g-recaptcha-response');
  //      //  $response = $this->recaptcha->verifyResponse($recaptcha);
		// 	redirect('frontend/login/index/error');
		// }

	}
}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
