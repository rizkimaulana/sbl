<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_registration extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('db2', TRUE);
		$this->load->model('memberregistration_module');
	}
	
	public function index()
	{
		 $data = array(
	    			// 'select_affiliate'=>$this->_select_affiliate(),
	    			'select_kelamin'=>$this->_select_kelamin(),
	    			'select_bank'=>$this->_select_bank(),
	    			'select_hub_ahliwaris'=>$this->_select_hub_ahliwaris(),
	    			'provinsi'=>$this->memberregistration_module->get_all_provinsi(),
	    			'kabupaten'=>$this->memberregistration_module->get_all_kabupaten(),
	    			
	    			// 'select_bank'=>$this->_select_bank(),
	    			// 'select_kelamin'=>$this->_select_kelamin()
	    	);
        // $this->template->load('template/template', $this->folder.'/add',$data);
		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/member_registration',$data);
		$this->load->view('template_frontend/footer');

		// print_r('KAMI LAGI PERBAIKAN TUNGGU 2jam kedepan unt layanan ini');
	}

	public function registrasi_complete()
	{
		$id_aff = $this->uri->segment(4);
	    $affiliate_noaktif = $this->memberregistration_module->get_pic_viewa_affiliate_noaktif($id_aff);
	    // $pic    = array();
	    if(!$affiliate_noaktif){
	        show_404();
	    }

	    $data = array(
	        
	       		 'affiliate_noaktif'=>$affiliate_noaktif

	       		 );
	 
	    // $this->template->load('template/template', $this->folder.'/data_jamaah',($data));
		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/registrasi_complete',$data);
		$this->load->view('template_frontend/footer');
	}

	public function search() {
        // tangkap variabel keyword dari URL
        $keyword = $this->uri->segment(4);
        // cari di database
        $data = $this->db->from('view_refrensi_id')->like('autocomplete', $keyword)->get();
        foreach ($data->result() as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row->autocomplete,
                'nama' => $row->nama,
                'id_user' => $row->id_user,
            );
        }
        echo json_encode($arr);
    }

	public function registrasi_complete2()
	{
		$id_aff = $this->uri->segment(4);
	    $affiliate_noaktif = $this->memberregistration_module->get_pic_viewa_affiliate_noaktif($id_aff);
	    // $pic    = array();
	    if(!$affiliate_noaktif){
	        show_404();
	    }

	    $data = array(
	        
	       		 'affiliate_noaktif'=>$affiliate_noaktif

	       		 );
	 
	    // $this->template->load('template/template', $this->folder.'/data_jamaah',($data));
		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/registrasi_complete2',$data);
		$this->load->view('template_frontend/footer');
	}

	private function _select_affiliate(){
		// $status = array('1');
		// $this->db->where_in('status', $status);
		return $this->db->get('view_refrensi_id')->result();
	}
	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}

	public function save(){

	 	$this->form_validation->set_rules('nama','nama','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
	   		
	   	 	$id_affiliate=$this->input->post('id_affiliate', true);
            $pin=$this->input->post('pin', true); // mendapatkan input dari kode
            $ktp=$this->input->post('ktp'); // mendapatkan input dari kode
            $id_affiliate = $this->input->post('id_affiliate');
            $cek=$this->memberregistration_module->cek($ktp); // cek kode di database
            $cek_affiliate=$this->memberregistration_module->cek_perwakilan($id_affiliate); // cek kode di database

          $nama=$this->input->post('nama');
            $member = $this->input->post('member');
             $cek_perwakilan_sahabat=$this->memberregistration_module->cek_id_sahabat($id_affiliate); 
              $cek_perwakilan_sahabat_2=$this->memberregistration_module->cek_perwakilan_sahabat_2($id_affiliate);

             if ($cek_perwakilan_sahabat->num_rows()==0 && $member == 1) {
            	 $this->session->set_flashdata('info', "ID BELUM TERDAFTAR DI sahabat");
               
                 redirect('frontend/member_registration');
               }
            elseif ($cek_perwakilan_sahabat->num_rows()==0 && $member == 2) {
        	 $this->session->set_flashdata('info', "ID BELUM TERDAFTAR DI sahabat");
           
             redirect('frontend/member_registration'); 
    	
            }elseif($cek->num_rows()==0 ){ //  CEK KTP DI NEW MEMBER
                 $this->session->set_flashdata('info', "KTP tidak sama dengan Ktp Di data Sahabat SBL 1");
               
                 redirect('frontend/member_registration');

             // }elseif($cek->num_rows()==0 && $member == 2){ // CEK KTP DI  PERWAKILAN
             //     $this->session->set_flashdata('info', "KTP tidak sama dengan Ktp Di data Sahabat SBL");
               
           
            }elseif ($cek_affiliate->num_rows()>0 && $member == 1) { // CEK ID DI KONVEN  NEW MEMBER
            	 $this->session->set_flashdata('info', "ID SAHABAT YANG ANDA INPUT SUDAH TERDAFTAR DI KONVENSIONAL");
               
                 redirect('frontend/member_registration');
             }elseif ($cek_affiliate->num_rows()>0 && $member == 2) { // CEK ID DI KONVEN  PERWAKILAN
            	 $this->session->set_flashdata('info', "ID SAHABAT YANG ANDA INPUT SUDAH TERDAFTAR DI KONVENSIONAL");
               
                 redirect('frontend/member_registration');
 
             }elseif ($cek_perwakilan_sahabat_2->num_rows()>0 && $member == 1) {
            	 $this->session->set_flashdata('info', "ID SAHABAT ANDA SUDAH TERDAFTAR SEBAGIA PERWAKILAN SAHABAT, PILIH OPSI DI FORM REGISTRASI DENGAN BENAR");
                 redirect('frontend/member_registration');

             }elseif ($cek_perwakilan_sahabat_2->num_rows()==0 && $member == 2) {
            	 $this->session->set_flashdata('info', "ID SAHABAT ANDA BELUM TERDAFTAR SEBAGIA PERWAKILAN SAHABAT, PILIH OPSI DI FORM REGISTRASI DENGAN BENAR");
                 redirect('frontend/member_registration');

              }elseif ($nama == "") {
              	 $this->session->set_flashdata('info', "Klik CARI USERID SAHABAT, KRNA KOLOM NAMA KOSONG");
                 redirect('frontend/member_registration');
              // }
              
            
            
    
            }else{

		$data = $this->input->post(null,true);
	    $this->memberregistration_module->save($data);
		}

	
 }
		    
}

	function get_id(){
		$affiliate=$this->input->post('id_affiliate');
		$data['get_data']=$this->memberregistration_module->get_id($affiliate);
		
		// $this->load->view('template_frontend/header');
		// $this->load->view('template_frontend/menu');
		$this->load->view('frontend/v_id_sahabat',$data);
		// $this->load->view('template_frontend/footer');

	}

   function get_nama(){
    $id_affiliate = $this->input->post('id_affiliate');
   
      $sql = "SELECT * from t_member where userid ='$id_affiliate'";
        $query = $this->db2->query($sql)->result_array();
      foreach($query as $key=>$value){
        $nama= $value['nama'];
      }
        if(count($query) > 0){
          
            

            echo $nama;
           
    
        }else{
        	echo '';

        }
  }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */