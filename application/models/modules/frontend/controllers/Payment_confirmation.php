<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_confirmation extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Payment_confirmation_model');
	}
	
	public function index()
	{
		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/view_paymentconfirmation');
		$this->load->view('template_frontend/footer');
	}

	public function addPayment_confirmation(){
		
            $this->form_validation->set_rules('invoice','invoice', 'required|xss_clean|max_length[10]|trim|strip_tags');
            $this->form_validation->set_rules('nama','Nama', 'required|xss_clean|max_length[6]|trim|strip_tags');
            $this->form_validation->set_rules('email','Email', 'required|min_length[3]');
            $this->form_validation->set_rules('phone','Phone','required|min_length[11]|max_length[15]|numeric');
            $this->form_validation->set_rules('total_payment','Jumlah Pembayaran', 'required|min_length[2]');
            $this->form_validation->set_rules('destination_bank','Bank Tujuan', 'required|min_length[11]');
            // $this->form_validation->set_rules('payment_date','Tanggal Kirim', 'required');
			//$this->form_validation->set_rules('text','Keterangan Pembayaran', 'required|xss_clean|max_length[1000]');
            // $this->form_validation->set_rules('file','Tanggal Kirim', 'required');

            if($this->form_validation->run()==TRUE){
            	  $config['upload_path'] = 'upload/payment_confirmation/';
		          $config['allowed_types'] = 'gif|jpeg|png|jpg|pdf|';
		          $config['max_size'] = 2000;
		          $config['max_height'] = 2000;
		          $config['max_width'] = 2000;
				  $config['encrypt_name'] = TRUE;
		  		$this->upload->initialize($config);
		  		if ($this->upload->do_upload('file_url')) {
				$dok = $this->upload->data();
			
                $invoice = $this->input->post('invoice');
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                 $total_payment = $this->input->post('total_payment');
                $destination_bank = $this->input->post('destination_bank');              
                $date = $this->input->post('date');
                //$date = date('Y-m-d', strtotime($this->input->post('date', TRUE)));
                //$date = DateTime::createFromFormat("Y-m-d", $this->input->post('date'));
                $information = $this->input->post('information');
                  $file_url = $dok('file_url');
                $this->Payment_confirmation_model->insertPayment($invoice, $name, $email, $phone, $total_payment, $destination_bank, $date);
                
                $this->session->set_flashdata('info', "konfirmasi pembayaran sukses, terimakasih.");
				redirect('payment_confirmation');
            }else {
                 $this->session->set_flashdata('info', "Input Data error");
            }
        }
		    	$this->load->view('template_frontend/header');
				$this->load->view('template_frontend/menu');
				$this->load->view('view_paymentconfirmation');
				$this->load->view('template_frontend/footer');
		   } 
		    
	}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */