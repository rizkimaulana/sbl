<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_affiliate extends CI_Controller {

    public function __construct(){
        
        parent::__construct();
        if($this->session->userdata('is_login'))redirect('dashboard_affiliate/dashboard');
    }
	public function index($param='')
	{
		if($param == 'error')
			$param = 'Incorrect username or password';
			
		$data = array('title'=>'login','message'=>$param,
			'base_url'=>base_url(),
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
			);
		// $this->load->view('view_loginaffiliate',$data);
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend_affiliate/view_loginaffiliate',$data);
		$this->load->view('template_frontend/footer');
	}
	
	
	public function do_login(){


		$data = $this->input->post(null,true);

		// $is_login = $this->db->get_where('view_login_affiliate',array(
		// 								'username'=>$data['username'],
		// 								'password'=>md5(trim($data['password'])),
		// 								))->row();
		$this->form_validation->set_rules('username', ' ', 'trim|required');
        $this->form_validation->set_rules('password', ' ', 'trim|required');
		$stored_procedure = "call view_login_affiliate(?,?)";
          $is_login = $this->db->query($stored_procedure,array(
          	'username'=>$data['username'],
          	'password'=>md5(trim($data['password']))
          	))->row(); 
		
 $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);
 if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true) {
 	redirect('frontend_affiliate/login_affiliate/index/error');
	redirect('frontend_affiliate/login_affiliate/index/error');
 }else{
		if($is_login){
			
			$session_set = array(
				
				'is_login'	=> true,
				'nama'		=> $is_login->nama,
				//'nik'		=> $is_login->nik,
				'id_user'	=> $is_login->id_user,
				'id_affiliate'	=> $is_login->id_affiliate,
				'username'	=> $is_login->username,
				'npwp'	=> $is_login->npwp,
				'id_affiliate_type'	=> $is_login->id_affiliate_type,
				'jabatan_id'=> $is_login->jabatan_id,
				'nama_affiliate'=> $is_login->nama_affiliate,
				'id_aff'=> $is_login->id_aff,
				'wilayah'=> $is_login->wilayah,
				'nama'=> $is_login->nama,
				'nama_affiliate'=> $is_login->nama_affiliate,
				'tempat_lahir'=> $is_login->tempat_lahir,
				'tanggal_lahir'=> $is_login->tanggal_lahir,
				'kelamin'=> $is_login->kelamin,
				'ktp'=> $is_login->ktp,
				'provinsi_id'=> $is_login->provinsi_id,
				'kabupaten_id'=> $is_login->kabupaten_id,
				'kecamatan_id'=> $is_login->kecamatan_id,
				'alamat'=> $is_login->alamat,
				'alamat_kantor'=> $is_login->alamat_kantor,
				'alamat_kirim'=> $is_login->alamat_kirim,
				'agama'=> $is_login->agama,
				'telp'=> $is_login->telp,
				'email'=> $is_login->email,
				
				'status'=> $is_login->status,
				'update_by'=> $is_login->update_by,
				'create_by'=> $is_login->create_by,
				'jabatan_id'=> $is_login->jabatan_id,
				'provinsi'=> $is_login->provinsi,
				
				'mgm'=> $is_login->mgm,
				'sponsor'=> $is_login->sponsor,
				
			);
			$this->db->update('affiliate',array('last_login'=>date('Y-m-d H:i:s')),array('id_user'=>$is_login->id_user));
			$this->session->set_userdata($session_set);
			redirect('dashboard_affiliate/dashboard');
		}
		// else{
			
		// 	redirect('frontend_affiliate/login_affiliate/index/error');
		// }
	}
 }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
