<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Memberregistration_module extends CI_Model {

     private $_table="affiliate";
      private $_primary="id_aff";
     private $_aktivasi_affiliate="aktivasi_affiliate";
     private $_id_aktivasi_affiliate="id_aktivasi_affiliate";
   
   private $db2;
	public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }

      public function generate_kode($idx){
        $today=date('md');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }
	public function save($data){
        
        $initial = "GM";
        
        $user="input_registasi";
         $member = $this->input->post('member');
         $id_affiliate = $this->input->post('id_affiliate');

        if ($member == 1){


        $arr = array(
            'sponsor' => $data['sponsor'],
            'id_affiliate_type' => 5,
            'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
            'password' => md5($data['password']),
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['select_hub_ahliwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'alamat_kantor' => $data['alamat_kantor'],
            'alamat_kirim' => $data['alamat_kirim'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
            'bank' => $data['select_bank'],
            'status' => 0,
             'mgm' => 1,
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'jabatan_id'=>26,
            'create_by'=>$user,
            'pajak'=>'0.06'
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_aff =  $this->db->insert_id(); //get last insert ID
        
        //krena kode_client hrus digenerate, do it
        $this->db->update($this->_table,
                    array('id_user'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));

                $this->db->update($this->_table,
                    array('username'=> $this->generate_kode($initial.$id_aff)),
                    // array('username'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));
        $id_user =  $this->db->insert_id();
    

        
        $arr = array(
            'id_user' => $this->generate_kode($initial.$id_aff),
            'id_affiliate_type' => 5,
            'id_sahabat' => $data['id_affiliate'],
            'sponsor' => $data['sponsor'],
            'nama' => $data['nama'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['select_hub_ahliwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'id_product' => 4,
            'category' => 2,
            'wkt_tunggu' => 1,
            'status' => 0,
             'st_upgrade' => 1,
            'upgrade' => '5',
            'ket_upgrade' => 'REGISTRASI GM',
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'create_by'=>$user,
            'pin' => $data['pin'],
            'harga_satuan' => $data['harga_product'],
            'kode_unik' => $data['unique_digit'],
        );
        $this->db->insert('member_mgm',$arr);
         $id_mgm =  $this->db->insert_id();


         $pin = $this->input->post('pin');
         if ($pin == 50){
            $arr = array(
              'pin' => $data['pin'],
              'dp' => $data['DEPOSIT_GM_1'],
              'pembayaran_dp' => $data['DEPOSIT_GM_1'],
              'deposit' => $data['DEPOSIT_GM_1'],
              'biaya_pelunasan' => ($data['harga_product'] - $data['DEPOSIT_GM_1']),
              'deposit_sahabat'=> 0,
            );    
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 

            //  $arr = array(
            //     'userid' => $data['id_affiliate'], 
            //     'tgl_daftar' => date('Y-m-d H:i:s'),
            //     'tgl_post' => date('Y-m-d H:i:s'),
            //     'admin'=> 'KONVEN', 
            //     'st'=> 0, 
            //     'nominal' => 5000000,
            //     'sisa' => 0,
            //     );    
            // $this->db2->insert('t_perwakilan',$arr);
         }else{
              $arr = array(
              'pin' => $data['pin'],
              'dp' => $data['DEPOSIT_GM_1'],
              'pembayaran_dp' => $data['DEPOSIT_GM_2'],
              'deposit' => $data['DEPOSIT_GM_2'],
              'biaya_pelunasan' => ($data['harga_product'] - $data['DEPOSIT_GM_1']),
              'deposit_sahabat'=> 5000000,
            );    
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 

            // $arr = array(
            //     'userid' => $data['id_affiliate'], 
            //     'tgl_daftar' => date('Y-m-d H:i:s'),
            //     'tgl_post' => date('Y-m-d H:i:s'),
            //     'admin'=> 'KONVEN', 
            //     'st'=> 0, 
            //     'nominal' => 10000000,
            //     'sisa' => 5000000,
            //     );    
            // $this->db2->insert('t_perwakilan',$arr);
         }

    
     }else{

        $arr = array(
            'sponsor' => $data['sponsor'],
            'id_affiliate_type' => 5,
            'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
            'password' => md5($data['password']),
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['select_hub_ahliwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'alamat_kantor' => $data['alamat_kantor'],
            'alamat_kirim' => $data['alamat_kirim'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
            'bank' => $data['select_bank'],
            'status' => 0,
           
            'mgm' => 1,
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'jabatan_id'=>26,
            'create_by'=>$user,
            'pajak'=>'0.06'
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_aff =  $this->db->insert_id(); //get last insert ID
        
        //krena kode_client hrus digenerate, do it
        $this->db->update($this->_table,
                    array('id_user'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));

                $this->db->update($this->_table,
                    array('username'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));
        $id_user =  $this->db->insert_id();
    

        
        $arr = array(
            'id_user' => $this->generate_kode($initial.$id_aff),
            'id_affiliate_type' => 5,
            'id_sahabat' => $data['id_affiliate'],
            'sponsor' => $data['sponsor'],
            'nama' => $data['nama'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'waris' => $data['waris'],
            'hubwaris' => $data['select_hub_ahliwaris'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],
            'alamat' => $data['alamat'],
            'id_product' => 4,
            'category' => 2,
            'wkt_tunggu' => 1,
            'status' => 0,
            'st_upgrade' => 2,
            'upgrade' => 4,
            'ket_upgrade' => 'REGISTRASI PERWAKILAN SAHABAT KE GM',
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'create_by'=>'Input Reg Perwakilan',
            'pin' => $data['pin'],
            'harga_satuan' => $data['harga_product'],
            'kode_unik' => 0,
        );
        $this->db->insert('member_mgm',$arr);
         $id_mgm =  $this->db->insert_id();

    $sql = "SELECT * from t_perwakilan where userid ='$id_affiliate'";
        $query = $this->db2->query($sql)->result_array();
      foreach($query as $key=>$value){
        $nominal_perwakilan= $value['nominal'];
      }

         $pin = $this->input->post('pin');
         if ($pin == 50){
            $arr = array(
              'pin' => $data['pin'],
              'dp' => $data['DEPOSIT_GM_1'],
              'deposit' => $nominal_perwakilan,
              'deposit_sahabat'=> $nominal_perwakilan-$data['DEPOSIT_GM_1'],
              'pembayaran_dp' => 0,
              'biaya_pelunasan' => ($data['harga_product'] - $data['DEPOSIT_GM_1']),
            );    
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 

            // $arr = array(

            //     'nominal' => 5000000,
            //     'sisa' => 0,
            // ); 
            // $this->db2->update('t_perwakilan',$arr,array('userid'=>$data['id_affiliate'])); 
         }else{
              $arr = array(
              'pin' => $data['pin'],
              'dp' => $data['DEPOSIT_GM_1'],
              'deposit' =>  $nominal_perwakilan,
              'deposit_sahabat'=> $data['DEPOSIT_GM_2']-$nominal_perwakilan,
              // 'pembayaran_dp' => ($data['DEPOSIT_GM_2']-$nominal_perwakilan),
                'pembayaran_dp' => 0,
              'biaya_pelunasan' => ($data['harga_product'] - $data['DEPOSIT_GM_1']),
            );    
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 

            if ($nominal_perwakilan < 10000000){
               $arr = array(

                'pembayaran_dp' => ($data['DEPOSIT_GM_2']-$nominal_perwakilan)
            ); 
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$id_mgm)); 
            }
           // $arr = array(

           //      'nominal' => 10000000,
           //      'sisa' => 5000000,
           //  ); 
           //  $this->db2->update('t_perwakilan',$arr,array('userid'=>$data['id_affiliate'])); 
         }
     }

       
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            if ($member == 1){
            redirect('frontend/member_registration/registrasi_complete/'.$id_aff.'');
            }else{
                redirect('frontend/member_registration/registrasi_complete2/'.$id_aff.'');
            }
            return true;
        }
        
       
    }
	function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }
   function cek($ktp){
    $ktp = $this->input->post('ktp');
    $id_affiliate = $this->input->post('id_affiliate');
        $query = $this->db2->query("SELECT * FROM t_member Where ktp ='$ktp' ");
        return $query;
    }

   function cek_id_sahabat($id_affiliate){
     $id_affiliate = $this->input->post('id_affiliate');
        $query = $this->db2->query("SELECT * FROM t_member Where userid ='$id_affiliate' ");
        return $query;
    }
   function cek_perwakilan($id_affiliate){
    $id_affiliate = $this->input->post('id_affiliate');
        $query = $this->db->query("SELECT * FROM affiliate Where id_affiliate ='$id_affiliate' and status='1'");
        return $query;
    }

  function cek_perwakilan_sahabat($id_affiliate){
    $id_affiliate = $this->input->post('id_affiliate');
        $query = $this->db2->query("SELECT * FROM t_perwakilan Where userid ='$id_affiliate'");
        return $query;
 }
function cek_perwakilan_sahabat_2($id_affiliate){
    $id_affiliate = $this->input->post('id_affiliate');
        $query = $this->db2->query("SELECT * FROM t_perwakilan Where userid ='$id_affiliate' and st='1'
          ");
        return $query;
 }
     function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }

    public function get_pic_viewa_affiliate_noaktif($id_aff){

         $sql = "SELECT * from view_affiliate_noaktif where id_aff = {$id_aff}
                ";
        return $this->db->query($sql)->row_array();  
    }

    function add_perwakilan($id,$nominal){
        $query = $this->db->query("select f_addperwakilan('$id','$nominal') as hasil_add");
                if($query->num_rows === 1){
                        return $query->row();
                }else{
                        return '0';
                }
    }

    function get_id($affiliate){
    $hsl=$this->db2->query("SELECT * FROM t_member where userid='$affiliate'");
    return $hsl;
  }

   function searchItem_userid($userid){
     
      
          $sql = "SELECT * from t_member where userid='$userid'"; 
            $query = $this->db2->query($sql);
    
    if (empty($query->result_array())){
        
       echo'<h4 style="color: #9F6000;">Maaf ! USERID Yang Anda Cari Tidak Ada '.$jumlah_jamaah.'</h4>';
           exit;
       
       }else{

        
        foreach ($query as $key=>$value){    
        
            echo' <input type="text" name="nama" id="nama"  value="'.$value->nama.'" class="form-control input-lg" placeholder="Nama" tabindex="2"  required>

';

       }

       }
   }

      function get_nama($id_affiliate) {
      // $this->db->where("userid",$id_affiliate);
      // return $this->db2->get("t_member");

            $sql = "SELECT * from t_member where userid ='$id_affiliate'";
        $query = $this->db2->query($sql)->result_array();
    }
}