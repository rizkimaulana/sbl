<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Datajamaah_module extends CI_Model {
  var $table = 'view_jamaah';
    var $column_order = array(null, 'invoice','id_jamaah','nama','tahun_keberangkatan','bulan_keberangkatan','blnthn_keberangkatan','date_schedule','blnthn_keberangkatan'); //set column field database for datatable orderable
    var $column_search = array('invoice','id_jamaah','nama','tahun_keberangkatan','bulan_keberangkatan','blnthn_keberangkatan','date_schedule','blnthn_keberangkatan'); //set column field database for datatable searchable 
    var $order = array('date_schedule' => 'asc'); // default order 

    
	public function __construct()
    {
          parent::__construct();
         
    }

  function get_all_schedule() {
        // $this->db->select('*');
        // $this->db->from('view_jamaah');
        // $query = $this->db->get();
        
        // return $query->result();

     $query = $this->db->query("SELECT DISTINCT(blnthn_keberangkatan) from  view_jamaah ORDER BY  date_schedule DESC ");

        return $query->result();
    }

        private function _get_datatables_query()
    {
        
        //add custom filter here
       
        if($this->input->post('blnthn_keberangkatan'))
        {
            $this->db->like('blnthn_keberangkatan', $this->input->post('blnthn_keberangkatan'));
        }
        if($this->input->post('date_schedule'))
        {
            $this->db->like('date_schedule', $this->input->post('date_schedule'));
        }
       

        $this->db->from($this->table);
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}