<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends CI_Controller {

	
	public function index()
	{
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		// $this->load->view('frontend_affiliate/view_login');
		$this->load->view('template_frontend/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */