 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit Hubungan</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>family_relation/update">
             <div class="form-group">
                <label>Hubungan Keluarga</label>
                <input type="hidden" name="id_relation" value="<?php echo $id_relation;?>">
                <input class="form-control" name="keterangan" value="<?php echo $keterangan;?>" required autofocus>
            </div>
           
            
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>family_relation" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->
