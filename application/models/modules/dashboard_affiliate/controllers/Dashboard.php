<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	var $folder = "dashboard_affiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(DASHBOARD_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','dashboard_affiliate');	
		$this->load->model('dashboard_model');
		$this->load->helper('download');
	}
	
	public function index(){
	    


	    $this->data['jamaah_belum_aktif'] = $this->dashboard_model->get_jamaah_belum_aktif();
	    $this->data['jamaah_aktif'] = $this->dashboard_model->get_jamaah_aktif();
	    $this->data['jamaah_alumni'] = $this->dashboard_model->get_jamaah_alumni();
	    $this->data['total_jamaah'] = $this->dashboard_model->get_total_jamaah();
	    $this->template->load('template/template', $this->folder.'/dashboard_affiliate',$this->data);
	 //   $this->load->view('template/header');
		// $this->load->view('template/menu');
		// $this->load->view('template/nav');
		// $this->load->view('dashboard/dashboard');
		// $this->load->view('template/footer');
		
	}

	function downloadtutorial($filename = NULL) {
    // load download helder
    
    // read file contents
    $data = file_get_contents(base_url('/uploads/'.$filename));
    force_download($filename, $data);
	}

		public function download(){				
		force_download('uploads/tutoril_webkonven_new.pdf',NULL);
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */