<?php

function random_3_process() {

    return rand(0, 9999);
}

function create_digit() {
    $number = random_3_process();
    $result = (strlen($number) < 3) ? random_3_process() : $number;
    $hasil_generate = str_pad($result, 3, '0', STR_PAD_LEFT);
    return $hasil_generate;
}

function random_3_digit() {
    $CI = & get_instance();
    $number = create_digit();
    // $sql = "SELECT * FROM booking where  kode_unik = '$number' AND DATE_FORMAT(create_date, '%Y %m %d')>=DATE_FORMAT(NOW() - INTERVAL 1 day, '%Y %m %d')and DATE_FORMAT(create_date, '%Y %m %d')<=DATE_FORMAT(NOW(), '%Y %m %d') ";
    // $query = $CI->db->query($sql)->result_array();
    $sql = "call kode_unik_registrasi_jamaah (?)";
    // $query  = $CI->db->query($sql)->result_array(); 
    $query = $CI->db->query($sql, array('kode_unik' => $number))->result_array();
    // $query = $CI->db->query($sql,array('kode_unik'=>$kode))->result_array();
    if (count($query) > 0) {
        create_digit();
    } else {
        if (is_null($number)) {
            create_digit();
        } else {
            return $number;
        }
    }
    return $number;
}

function random_3_digit_perjamaah() {
    $CI = & get_instance();
    $number = create_digit();

    $sql = "SELECT * FROM registrasi_jamaah where kode_unik = '$number' AND DATE_FORMAT(create_date, '%Y %m %d') = DATE_FORMAT(sysdate(), '%Y %m %d')";
    $query = $CI->db->query($sql)->result_array();

    if (count($query) > 0) {
        create_digit();
    } else {
        if (is_null($number) && empty($number)) {
            create_digit();
        } else {
            return $number;
        }
    }
}

function random_3_digit_perwakilan() {
    $CI = & get_instance();
    $number = create_digit();

    $sql = "SELECT * FROM aktivasi_affiliate where kode_unik = '$number' AND DATE_FORMAT(create_date, '%Y %m %d') = DATE_FORMAT(sysdate(), '%Y %m %d')";
    $query = $CI->db->query($sql)->result_array();

    if (count($query) > 0) {
        create_digit();
    } else {
        if (is_null($number) && empty($number)) {
            create_digit();
        } else {
            return $number;
        }
    }
}

function random_3_digit_kuota() {
    $CI = & get_instance();
    $number = create_digit();

    $sql = "SELECT * FROM kuota_booking where kode_unik = '$number' AND DATE_FORMAT(create_date, '%Y %m %d') = DATE_FORMAT(sysdate(), '%Y %m %d')";
    $query = $CI->db->query($sql)->result_array();

    if (count($query) > 0) {
        create_digit();
    } else {
        if (is_null($number) && empty($number)) {
            create_digit();
        } else {
            return $number;
        }
    }
}

function random_3_digit_member_mgm() {
    $CI = & get_instance();
    $number = create_digit();

    // $sql = "SELECT * FROM booking where kode_unik = '$number' AND DATE_FORMAT(create_date, '%Y %m %d') = DATE_FORMAT(sysdate(), '%Y %m %d')";
    $sql = "SELECT * FROM member_mgm where DATE_FORMAT(tanggal_daftar, '%Y %m %d')>=DATE_FORMAT(NOW() - INTERVAL 2 day, '%Y %m %d')and DATE_FORMAT(tanggal_daftar, '%Y %m %d')<=DATE_FORMAT(NOW(), '%Y %m %d') and `status`='0'  

		";
    $query = $CI->db->query($sql)->result_array();



    return $number;
}

function random_3_booking_seats() {
    $CI = & get_instance();
    $number = create_digit();
    $sql = "call cek_kode_unik_booking_seats (?)";
    $query = $CI->db->query($sql, array('kode_unik' => $number))->result_array();
    if (count($query) > 0) {
        create_digit();
    } else {
        if (is_null($number) && empty($number)) {
            create_digit();
        } else {
            return $number;
        }
    }
}

function generate_kode($idx) {
    $today = time('s');

    $ret = '';

    $limit = 3;

    for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

        $ret .= '0';
    }

    return $today;
}

function rand_time($max, $min) {
    return date("Y-m-d H:i:s", mt_rand(strtotime($min), strtotime($max)));
}

function random2() {
    $rand = mt_rand(10, 100);
    return $rand;
}

function random_3_aktivasi_jamaah() {
    $CI = & get_instance();
    $number = create_digit();
    $sql = "SELECT kode_unik FROM pst_aktivasi WHERE kode_unik = '".$number."'";
    $query = $CI->db->query($sql)->result_array();
    if (count($query) > 0) {
        create_digit();
    } else {
        if (is_null($number) && empty($number)) {
            create_digit();
        } else {
            return $number;
        }
    }
}
