 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit Product Price</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>product_price/update">
             <div class="form-group">
                <input type="hidden" name="id_productprice" value="<?php echo $id_productprice;?>">
            </div>
            <div class="form-group">
                <label>Pilih Product </label>
                <select class="form-control" name="kode_product">
                   
                    <?php foreach($select_product as $sp){ 
                    
                        $selected = ($kode_product == $sp->kode)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $sp->kode;?>" <?php echo $selected;?>><?php echo $sp->nama;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Room Category</label>
                <select class="form-control" name="room_category">
                    <?php foreach($select_roomcategory as $se){ 
                    
                        $selected = ($category == $se->id_room_category)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_room_category;?>" <?php echo $selected;?>><?php echo $se->category;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Embarkasi</label>
                <select class="form-control" name="embarkasi">
                    <?php foreach($select_embarkasi as $se){ 
                    
                        $selected = ($embarkasi == $se->id_embarkasi)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_embarkasi;?>" <?php echo $selected;?>><?php echo $se->departure;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Bulan</label>
                <!-- <input class="form-control" name="bulan"value="<?php echo $bulan;?>"  required> -->
                 <select class="form-control" name="bulan">
                    <?php foreach($select_bulan as $sp){ 
                    
                        $selected = ($bulan == $sp->bulan)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $sp->bulan;?>" <?php echo $selected;?>><?php echo $sp->bulan;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Hari</label>
                <!-- <input class="form-control" name="hari"value="<?php echo $hari;?>"  required> -->
                <select class="form-control" name="select_hari">
                    <?php foreach($select_hari as $sj){ 
                                     $selected = ($hari == $sj->hari)  ? 'selected' :'';?>
                                    <option value="<?php echo $sj->hari;?>" <?php echo $selected;?>><?php echo $sj->hari;?> </option>
                                <?php } ?>
                </select>
            </div>

             <div class="form-group">
                <label>Destination</label>
                <!-- <input class="form-control" name="hari"value="<?php echo $hari;?>"  required> -->
                <select class="form-control" name="select_detination">
                    <option value="">- Select Destination -</option>
                    <?php foreach($select_detination as $sj){ 
                                     $selected = ($destination_id == $sj->destination_id)  ? 'selected' :'';?>
                                    <option value="<?php echo $sj->destination_id;?>" <?php echo $selected;?>><?php echo $sj->destination;?> </option>
                                <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input class="form-control" name="harga" value="<?php echo $harga;?>"required>
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" name="keterangan" value="<?php echo $keterangan;?>" >
            </div>
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>product_price" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->
