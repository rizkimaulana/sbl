<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productprice_model extends CI_Model{
   
   
    private $_table="product_price";
    private $_primary="id_productprice";

    public function save($data){
   
        // $cek = $this->db->select('kode')->where('nama',$data['nama'])->get('product')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'kode_product' => $data['kode_product'],
            'category' => $data['room_category'],
            'embarkasi' => $data['embarkasi'],
            'bulan' => $data['bulan'],
            'hari' => $data['select_hari'],
            'destination_id' => $data['select_detination'],
            'harga' => $data['harga'],
            'keterangan' => $data['keterangan'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user')

           
        );       
        
         return $this->db->insert('product_price',$arr);

       
    }


    public function update($data){
        
        $arr = array(
        
            'kode_product' => $data['kode_product'],
            'category' => $data['room_category'],
            'embarkasi' => $data['embarkasi'],
            'bulan' => $data['bulan'],
            'hari' => $data['select_hari'],
            'destination_id' => $data['select_detination'],
            'harga' => $data['harga'],
            'keterangan' => $data['keterangan'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user')
        );       
              
        return $this->db->update($this->_table,$arr,array('id_productprice'=>$data['id_productprice']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
 
          $sql = " SELECT a.*,e.category as hotel,b.nama,c.username,d.departure FROM product_price a 
                    LEFT JOIN product b ON b.kode = a.kode_product
                    LEFT JOIN user c ON c.id_user = a.create_by
                    LEFT JOIN embarkasi d ON d.id_embarkasi = a.embarkasi
                    LEFT JOIN room_category e ON e.id_room_category = a.category
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND a.kode_product LIKE '%{$q}%' 
            		OR a.bulan LIKE '%{$q}%'
                    OR d.departure LIKE '%{$q}%'
                    OR a.category LIKE '%{$q}%'
                    OR a.harga LIKE '%{$q}%'
            		OR b.nama LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_productprice DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     public function delete($id_productprice){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_productprice'=>$id_productprice));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
    
    public function delete_by_id($id_productprice)
    {
        $this->db->where('id_productprice', $id_productprice);
        $this->db->delete($this->_table);
    }
    
    
}
