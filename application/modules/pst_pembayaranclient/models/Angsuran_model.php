<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Angsuran_model extends CI_Model {
    var $_table = 'angsuran';
    
    function get_total_angsuran($anggota_id){
        $total = 26000000;
        return $total;
    }
    
    function get_data_angsuran($anggota_id){
        $hasil = array(
            'sudah_bayar' => '',
            'sisa_angsuranr' => '',
            'jumlah_angsuran' => ''
        );
        return $hasil;
    }

    function get_lap_angsuran($awal, $akhir)
    {
        $this->db->select('a.*, b.nama_lengkap');
        $this->db->from('angsuran_bayar AS a');
        $this->db->join('anggota AS b', 'b.id = a.anggota_id');
        if(!empty($awal) && !empty($akhir)){
            $this->db->where('DATE_FORMAT(b.tanggal,"%Y-%m-%d") >=', $awal);
            $this->db->where('DATE_FORMAT(b.tanggal,"%Y-%m-%d") <=', $akhir);
        }
        return $this->db->get()->result_array();
    }
    
    function get_lap_fee_anggota($awal, $akhir)
    {
        $this->db->select('*');
        $this->db->from('fee_anggota');
        if(!empty($awal) && !empty($akhir)){
            $this->db->where('tanggal_bayar >=', $awal);
            $this->db->where('tanggal_bayar <=', $akhir);
        }
        return $this->db->get()->result_array();
    }
}
?>