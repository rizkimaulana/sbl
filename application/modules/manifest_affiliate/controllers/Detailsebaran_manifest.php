<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detailsebaran_manifest extends CI_Controller{ 
	var $folder = "manifest_affiliate";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(MANIFEST_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','manifest_affiliate');	
		$this->load->model('detailsebaran_model');
		$this->load->model('detailsebaran_model','r');


	}



	function add_ajax_schedule($bulantahun_keberangkatan){
		    $id_user=  $this->session->userdata('id_user'); 
        $koordinator= $this->uri->segment(3.0);

		 $query = $this->db->query("SELECT DISTINCT(schedule) from  manifest_affiliate where 
		 	bulantahun_keberangkatan ='".$bulantahun_keberangkatan."' and cluster ='".$id_user."' and id_affiliate ='".$koordinator."' ORDER BY  schedule");
		    $data = "<option value=''>- Pilih tanggal Keberangkatan -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->schedule."'>".$value->schedule."</option>";
		    }
		    echo $data;
	}
	
	public function data_manifest(){

		
		$bulantahun_keberangkatans = $this->detailsebaran_model->get_list_bulantahun_keberangkatan();

		$opt = array('' => 'SEMUA PENCARIAN');
		foreach ($bulantahun_keberangkatans as $bulantahun_keberangkatan) {
			$opt[$bulantahun_keberangkatan] = $bulantahun_keberangkatan;
		}

		$get_bulantahun_keberangkatan['form_bulantahun_keberangkatan'] = form_dropdown('',$opt,'','id="bulantahun_keberangkatan" class="form-control"');

	
     $id_user = $this->uri->segment(4);
     $get_koordinator = $this->detailsebaran_model->get_data_koordinator($id_user);
		 $data = array(
	    			
	    			// 'schedule'=>$this->detailsebaran_model->get_all_schedule(),
            'family_relation'=>$this->_family_relation(),
	    			'get_keluarga'=>$this->detailsebaran_model->get_keluarga(),
	    			'get_koordinator'=>$get_koordinator
	    	);

		$this->template->load('template/template', $this->folder.'/detailsebaran_manifest',array_merge($get_bulantahun_keberangkatan,$data));	
	}


    private function _family_relation(){
    
    return $this->db->get('family_relation')->result();
  }
private function _status($take){
      $x='0';
     $status = array('0'=>'X','1'=>'Y');
      if($take)
          return $status[$take];
        // return $status;
 
  }
		public function ajax_list()
	{
 
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
    
		foreach ($list as $r) {
			$no++;
      $row[] ='<tr>'; 
			$row = array();
            
			$row[] = $no;
 
  $sql = "SELECT * from  wilayah_provinsi where provinsi_id ='".$r->provinsi_id."'
                     ";
                    $query = $this->db->query($sql)->result_array();
                    foreach($query as $key=>$value){
                      $namaprovinsi = $value['nama'];
                      
                    }
            $sql = "SELECT * from  wilayah_kabupaten where kabupaten_id ='".$r->kabupaten_id."'
                     ";
                    $query = $this->db->query($sql)->result_array();
                    foreach($query as $key=>$value){
                      $namakab = $value['nama'];
                      
                    }
                   // $row[] ='<td width="10%" >  <strong>'.$r->id_affiliate.'<strong>
                   //     </td>';
              
	                $row[] ='<td width="10%" > DATA JAMAAH DARI ID : <strong>'.$r->id_affiliate.'<strong><br>
                     <strong>('.$r->invoice.')</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
                      PROVINSI :  '.$namaprovinsi.'<br>
                     Kota/kab :  '.$namakab.'<br>
	             			 TGL KEBERANGKATAN :  '.$r->schedule.'<br>
	             			
	               			 </td>';

	               	$row[] = '<td width="10%">NAMA PASSPORT :'.$r->nama_passport.'<br>
	               	 		 NO PASSPORT : <strong>'.$r->no_pasport.'</strong><br>
	             			 ISSUE OFFICE : <strong>'.$r->issue_office.'</strong><br>
	             			 ISSUE DATE : <strong>'.$r->isui_date.' </strong>
	               			 </td>';
                  if ($r->status_manifest ==0){ 
                 $row[] = '<td width="10%">KRT IDENTITAS/KTP : <strong>'.$this->_status($r->status_identitas).'</strong><br>
                     PASSPORT : <strong>'.$this->_status($r->status_pasport).'</strong><br>
                     PHOTO : <strong>'.$this->_status($r->status_photo).' </strong><br>
                     KARTU KELUARGA : <strong>'.$this->_status($r->status_kk).'</strong><br>
                     BUKU NIKAH : <strong>'.$this->_status($r->status_buku_nikah).' </strong><br>
                     VAKSIN : <strong>'.$this->_status($r->status_vaksin).' </strong><br>
                     AKTE : <strong>'.$this->_status($r->status_akte).' </strong><br>

                       </td>';    
                     }else{
                        $row[] = '<td width="10%">KRT IDENTITAS/KTP : <strong>'.$this->_status($r->status_identitas).'</strong><br>
                     PASSPORT : <strong>'.$this->_status($r->status_pasport).'</strong><br>
                     PHOTO : <strong>'.$this->_status($r->status_photo).' </strong><br>
                     KARTU KELUARGA : <strong>'.$this->_status($r->status_kk).'</strong><br>
                     BUKU NIKAH : <strong>'.$this->_status($r->status_buku_nikah).' </strong><br>
                     VAKSIN : <strong>'.$this->_status($r->status_vaksin).' </strong><br>
                     AKTE : <strong>'.$this->_status($r->status_akte).' </strong><br>
                     <h5> <span class="label label-info">APPROVED</span></h5>
                       </td>'; 
                     }
	               	$row[] = '<td>
                  
                  
                  <a class="btn btn-sm btn-info" href="'.base_url().'manifest_affiliate/detail/'.$r->id_registrasi.'" title="DETAIL"><i class="glyphicon glyphicon-pencil"></i> DETAIL</a><br><br>

                
                  </td>';
              $row[] ='</tr>'; 
			$data[] = $row;
     
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


		

    private function _status2($take){
      $x='0';
     $status = array('0'=>'X','1'=>'Y');
      if($take)
          return $status[$take];
        // return $status;
 
  }

  public function detail(){
  


      if(!$this->general->privilege_check_affiliate(MANIFEST_AFFILIATE,'view'))
        $this->general->no_access();
      $id = $this->uri->segment(3);
      $detail = $this->detailsebaran_model->get_detail_manifest($id);
      if(!$detail){
          show_404();
      }
     
      $detail_keluarga = $this->detailsebaran_model->get_detail_hubkeluarga($id);
       
        $detail['status_pasport'] = $this->_status2($detail['status_pasport']);
        $detail['status_identitas'] = $this->_status2($detail['status_identitas']);
        $detail['status_kk'] = $this->_status2($detail['status_kk']);
        $detail['status_photo'] = $this->_status2($detail['status_photo']);
        $detail['status_vaksin'] = $this->_status2($detail['status_vaksin']);
        $detail['status_buku_nikah'] = $this->_status2($detail['status_buku_nikah']);
        $detail['status_akte'] = $this->_status2($detail['status_akte']);
      $data = array(

         
             'detail'=>$detail,
             'detail_keluarga' => $detail_keluarga
             );

      $this->template->load('template/template', $this->folder.'/detail_manifestaffiliate',($data));

  }

   public function get_data_pic(){
  


      if(!$this->general->privilege_check_affiliate(MANIFEST_AFFILIATE,'view'))
        $this->general->no_access();
      $id = $this->uri->segment(3);
      $detail = $this->detailsebaran_model->get_data_pic($id);
      if(!$detail){
          show_404();
      }
     
     
      $data = array(
         
             'detail'=>$detail,
             );

      $this->template->load('template/template', $this->folder.'/picture_manifest',($data));

  }





    public function cobadate(){
       $date_now = date('Y-m-d');
      // $isui_date = $this->input->post('isui_date');
      $orderdate = explode("-",'2013-02-04');
    $month = $orderdate[1]-'2';
    $day   = $orderdate[2];
    $year  = $orderdate[0]+ '5';
    // $expire_date = $year ."-".$month."-".$day;
    $expire_date=date_create($year ."-".$month."-".$day);
    echo date_format($expire_date,"Y-m-d");
    }
}
