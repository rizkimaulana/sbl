<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak_bilyetaffilaite extends CI_Controller{ 
	var $folder = "cetak_bilyetaffilaite";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(CETAK_BILYETAFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','cetak_bilyetaffilaite');	
		$this->load->model('cetak_bilyetmodel');
		$this->load->helper('fungsi');
		$this->load->library('terbilang');
		$this->load->library('ci_qr_code');
        $this->config->load('qr_code');
	}
	

	public function index 	(){
		 
	    $this->template->load('template/template', $this->folder.'/bilyet');
		
	}


    function search_bilyet(){
            
			 
             $search = $this->input->post('s');

             if(!empty($search)){
                 $this->cetak_bilyetmodel->search_bilyet($search);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }
	


	public function bilyet(){
	


	    if(!$this->general->privilege_check_affiliate(CETAK_BILYETAFFILIATE,'cetak'))
		    $this->general->no_access();
	    $id_user = $this->uri->segment(3);

	    $bilyet = $this->cetak_bilyetmodel->get_pic_detail_bilyet($id_user);
	    // if(!$bilyet){ 
	    //     show_404();
	       
	    //    }
	   			 $data = array(
	   			 	'bilyet'=>$bilyet,
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit_bilyet',$data);

	}
	public function cetak_bilyet(){

	   if(!$this->general->privilege_check_affiliate(CETAK_BILYETAFFILIATE,'cetak'))
		    $this->general->no_access();
	    
	    $id_user = $this->uri->segment(3);
	    $bilyet = $this->cetak_bilyetmodel->get_pic_detail_bilyet($id_user);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 // 'pic'=>$pic
	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/bilyet_barcode',($data));
	    $this->load->view('cetak_bilyet',($data));
	}

	public function save_cetak_bilyet(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->cetak_bilyetmodel->save_cetak_bilyet($data);
	     if($send)
	         // $id_user = $this->input->post('id_user');
          //   redirect('data_jamaah/bilyet/cetak/'.$id_user.'');
	     	
	     	$id_jamaah = $this->input->post('id_jamaah');
            redirect('cetak_bilyetaffilaite/cetak_bilyet/'.$id_jamaah.'');
	}

function print_qr($id_jamaah)
    {
        $qr_code_config = array();
        $qr_code_config['cacheable'] = $this->config->item('cacheable');
        $qr_code_config['cachedir'] = $this->config->item('cachedir');
        $qr_code_config['imagedir'] = $this->config->item('imagedir');
        $qr_code_config['errorlog'] = $this->config->item('errorlog');
        $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
        $qr_code_config['quality'] = $this->config->item('quality');
        $qr_code_config['size'] = $this->config->item('size');
        $qr_code_config['black'] = $this->config->item('black');
        $qr_code_config['white'] = $this->config->item('white');
        $this->ci_qr_code->initialize($qr_code_config);

        // get full name and user details
         $id_jamaah = $this->uri->segment(3);
        // $id_jamaah = $this->input->post('id_jamaah');
	    $bilyet = $this->cetak_bilyetmodel->get_pic_detail_bilyet($id_jamaah);
        $image_name = $id_jamaah . ".png";

        // create user content
        // $codeContents = "user_name:";
        // $codeContents .= "$user_details->user_name";
        // $codeContents .= " user_address:";
        // $codeContents .= "$user_details->user_address";
        // $codeContents .= "\n";
        // $codeContents .= "user_email :";
        $codeContents .= $bilyet['id_jamaah'];

        $params['data'] = $codeContents;
        $params['level'] = 'H';
        $params['size'] = 3;

        $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
        $this->ci_qr_code->generate($params);

        $this->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

        // save image path in tree table
        // $this->user->change_userqr($user_id, $image_name);
        // then redirect to see image link
        $file = $params['savename'];
        if(file_exists($file)){
            header('Content-Description: File Transfer');
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            unlink($file); // deletes the temporary file

            exit;
        }
    }

	
}
