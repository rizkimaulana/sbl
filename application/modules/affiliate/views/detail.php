 <div id="page-wrapper">
    
    
       <div class="row">
       	<!-- <div class="col-lg-7"> -->
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Edit Affiliate</h3>
       
        <!-- <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>affiliate/update"> -->
        	<form action="<?php echo base_url();?>affiliate/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
             <!-- <div class="col-lg-6"> -->
             <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <b>Data Koordinator</b>
                </div> -->
                <!-- /.panel-heading -->
                <div class="form-group">
			        <label class="col-lg-2 control-label">Image</label>
			        <div class="col-lg-5">
			        	
			            <img src="<?php echo base_url('./assets/images/affiliate/'.$detail['pic1']);?>" width="200px" height="200px">
			        </div>
			    </div>
	
                <div class="panel-body">  
                	 <div class="form-group">
	                    <label class="col-lg-2 control-label" >ID Affiliate</label>
	                    <div class="col-lg-5">
	                      <input name="id_user" value="<?php echo $detail['id_user'];?>" class="form-control" readonly="readonly">
	                    </div>
	                </div>
                	<input type="hidden" name="id_aff" value="<?php echo $detail['id_aff'];?>">
                	<div class="form-group">
		                    <label class="col-lg-2 control-label">SPONSOR</label>
		                  
		                 <div class="col-lg-5">
	                      <input type="text"name="sponsor" value="<?php echo $detail['sponsor'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>  
                		<div class="form-group">
		                    <label class="col-lg-2 control-label">Jenis affiliate</label>
		                  
		                 <div class="col-lg-5">
	                      <input type="text"name="affiliate_type" value="<?php echo $detail['affiliate_type'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>  
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label" >ID Affiliate</label>
	                    <div class="col-lg-5">
	                      <input name="id_affiliate" class="form-control" value="<?php echo $detail['id_affiliate'];?>" readonly="readonly">
	                    </div>
	                </div>
		                <div class="form-group">
		                    <label class="col-lg-2 control-label" >Nama</label>
		                    <div class="col-lg-5">
		                      <input name="nama" class="form-control" value="<?php echo $detail['nama'];?>" readonly="readonly" >
		                    </div>
		                </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">No Tlp</label>
	                    <div class="col-lg-5">
	                      <input name="telp" class="form-control" value="<?php echo $detail['telp'];?>"readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">E-Mail</label>
	                    <div class="col-lg-5">
	                      <input name="email" class="form-control" value="<?php echo $detail['email'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                 <div class="form-group">
	                    <label class="col-lg-2 control-label">No Identitas</label>
	                    <div class="col-lg-5">
	                      <input name="ktp" class="form-control" value="<?php echo $detail['ktp'];?>" readonly="readonly">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                   
	                   <label class="col-lg-2 control-label">Jenis Kelamin</label>
	               
		                <div class="col-lg-5">
	                      <input name="jeniskelamin" value="<?php echo $detail['jeniskelamin'];?>" class="form-control" readonly="readonly">
	                    </div>

	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Hubungan Keluarga</label>
	                    
		                <div class="col-lg-5">
	                      <input name="hubungan" value="<?php echo $detail['hubungan'];?>" class="form-control" readonly="readonly">
	                    </div>

	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Ahli waris</label>
	                    <div class="col-lg-5">
	                      <input name="waris" class="form-control" value="<?php echo $detail['waris'];?>"readonly="readonly">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Tempat tanggal Lahir</label>
	                    <div class="col-lg-5">
	                      <input name="tempat_lahir" class="form-control" value="<?php echo $detail['tempat_lahir'];?>"readonly="readonly"> 
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">Tanggal Lahir</label>
	                    <div class="col-lg-5">
	                     <div class='input-group date' id='datepicker'>
	                          <input type='text' name="tanggal_lahir" id="datepicker" value="<?php echo $detail['tanggal_lahir'];?>"class="form-control" readonly="readonly" />
	                          <span class="input-group-addon">
	                            <span class="glyphicon glyphicon-calendar"></span>
	                          </span>
                        </div>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Provinsi</label>
	                     <div class="col-lg-5">
	                      <input name="provinsi" value="<?php echo $detail['provinsi'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kabupaten</label>
	                     <div class="col-lg-5">
	                      <input name="kabupaten" value="<?php echo $detail['kabupaten'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kecamatan</label>
	                   <div class="col-lg-5">
	                      <input name="kecamatan" value="<?php echo $detail['kecamatan'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat</label>
	                    <div class="col-lg-5">
	                      <textarea  name="alamat" class="form-control"  readonly="readonly"><?php echo $detail['alamat'];?></textarea>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat kirim</label>
	                    <div class="col-lg-5">
	                      <textarea  name="alamat_kirim" class="form-control"  readonly="readonly"><?php echo $detail['alamat_kirim'];?></textarea>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">UserName</label>
	                    <div class="col-lg-5">
	                      <input name="username" class="form-control" value="<?php echo $detail['username'];?>"  readonly="readonly"> 
	                    </div>
	                  </div>
	               <!--    <div class="form-group">
	                    <label class="col-lg-2 control-label">Password</label>
	                    <div class="col-lg-5">
	                      <input name="password" class="form-control"  > 
	                    </div>
	                  </div> -->
                 <!-- </div>
                
               <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Bank</b>
                </div>
                <div class="panel-body">  -->
                	<div class="form-group">
	                    <label class="col-lg-2 control-label">NPWP</label>
	                    <div class="col-lg-5">
	                      <input name="npwp" class="form-control"  value="<?php echo $detail['npwp'];?>" readonly="readonly"> 
	                    </div>
	                  </div>
               		<div class="form-group">
	                    <label class="col-lg-2 control-label">No Rekening</label>
	                    <div class="col-lg-5">
	                      <input name="norek" class="form-control"  value="<?php echo $detail['norek'];?>" readonly="readonly"> 
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Atas Nama Rekening</label>
	                    <div class="col-lg-5">
	                      <input name="namarek" class="form-control" value="<?php echo $detail['namarek'];?>"  readonly="readonly"> 
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Bank</label>
	                    <div class="col-lg-5">
	                      <input name="namabank" value="<?php echo $detail['namabank'];?>" class="form-control" readonly="readonly">
	                    </div>
	                </div>
	                 <!--   <div class="form-group">
					        <label class="col-lg-2 control-label">FOTO</label>
					        <div class="col-lg-5">
					            <input type="file" name="pic[]"  class="form-control">
					        </div>
					    </div> -->
	                  <div class="form-group">
		                <label class="col-lg-2 control-label">Status</label>
		                <div class="col-lg-5">
	                      <input name="keterangan" value="<?php echo $detail['keterangan'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>
	              <!-- </div> -->
              </div>

            <!-- </div> -->
               </div>
            <!-- </div>  -->
            <!-- <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button> -->
            <a href="<?php echo base_url();?>affiliate" class="btn btn-danger"><i class="fa fa-times"></i> Back</a>
        </form>
    </div>
    </div>
       <!-- </div> -->
    </div>
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
			$(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
		</script>
<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('affiliate/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('affiliate/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('affiliate/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });
</script>