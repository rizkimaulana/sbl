<div class="page-head">
    <h2>Konfirmasi Fee Anggota </h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Pesantren</a></li>
        <li class="active">Konfirmasi Fee Anggota</li>
    </ol>
</div>  

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Konfirmasi Fee Anggota</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>


                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Penerima</th>
                                    <th>Keterangan</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>      -->                

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->


<div class="modal fade" id="modal-default" tabindex="-1">
    <div class="modal-dialog">
        <form method="POST" id="form_konfirmasi" name="form_konfirmasi">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Konfirmasi Fee Anggota</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kepada</label>
                                <input type="hidden" name="fee_anggota_id" id="fee_anggota_id" class="form-control" readonly="">
                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" readonly="">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input type="text" name="keterangan" id="keterangan" class="form-control" >

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nominal Pembayaran</label>
                                <input type="text" name="nominal_bayar" id="nominal_bayar" class="form-control" required="">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cara Bayar</label>
                                <select class="form-control" name="cara_bayar" id="cara_bayar" required="">
                                    <option value="CASH">CASH</option>
                                    <option value="TRANSFER">TRANSFER</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="hidden_div" style="display: none;">
                                <div class="form-group">
                                    <label>Bank Transfer</label>
                                    <select class="form-control" name="bank_transfer" id="bank_transfer" >
                                        <option value="">-- Pilih Bank --</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="MANDIRI">MANDIRI</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>No Rekening Tujuan</label>
                                    <input type="text" name="no_rek_bayar" id="no_rek_bayar" class="form-control">

                                </div>
                                <div class="form-group">
                                    <label>Nama Rekening Tujuan</label>
                                    <input type="text" name="nama_rek_bayar" id="nama_rek_bayar" class="form-control">

                                </div>
                                <div class="form-group">
                                    <label>Bukti Pembayaran</label>
                                    <input type="file" name="bukti_pembayaran" id="bukti_pembayaran" class="form-control">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Tanggal Bayar</label>
                                <input type="text" class="form-control" name="tgl_bayar" id="tgl_bayar" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="simpan_konfirmasi()">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#tgl_bayar').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('pst_pembayaran/pembayaran/ajax_fee_anggota') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
    });
    function konfirmasi(fee_anggota_id) {
        var link_rm = "<?php echo site_url('pst_pembayaran/pembayaran/get_data_fee') ?>";
        link_rm = link_rm + "/" + fee_anggota_id;
        $.get(link_rm, function (data) {
            $('#fee_anggota_id').val(data.fee_anggota_id);
            $('#nama_lengkap').val(data.to_anggota_nama);
            $('#keterangan').val(data.keterangan);
            $('#nominal_bayar').val(data.nominal_bayar);
            $('#bank_transfer').val(data.bank_transfer);
            $('#no_rek_bayar').val(data.no_rek_bayar);
            $('#bank_transfer').val(data.nama_rek_bayar);
        }, "json");
        $('#modal-default').modal('show');
    };
    document.getElementById('cara_bayar').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        cara_bayar = $(this).val();
        console.log(cara_bayar);
        if (cara_bayar == 'TRANSFER') {
            $("#bank_transfer").prop('required', true);
            $("#bukti_pembayaran").prop('required', true);
        } else {
            $("#bank_transfer").prop('required', false);
            $("#bukti_pembayaran").prop('required', false);
        }
    });
    function simpan_konfirmasi() {
        $('#form_konfirmasi').submit();
    };
</script>