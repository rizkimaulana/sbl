<div class="page-head">
    <h2>Aktivasi Anggota </h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">PESANTREN</a></li>
        <li class="active">Aktivasi Anggota</li>
    </ol>
</div> 

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Aktivasi Anggota</b>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Referal</th>
                                    <th>Telp</th>
                                    <th>Kode Unik</th>
                                    <th>Nominal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-default" tabindex="-1">
    <div class="modal-dialog">
        <form method="POST" id="form_aktivasi" name="form_aktivasi" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Aktivasi Anggota</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>Nama lengkap</b></label>
                                <input type="hidden" name="aktivasi_id" id="aktivasi_id" class="form-control" readonly="">
                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" readonly="">
                                <input type="hidden" name="id_user" id="id_user" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red">Referal</b></label>
                                <input type="text" name="parent_nama" id="parent_nama" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label style="color: red">Alamat</b></label>
                                <input type="text" name="alamat" id="alamat" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red">Cara Bayar</b></label>
                                <select class="form-control" name="cara_bayar" id="cara_bayar" required="">
                                    <option value="CASH">CASH</option>
                                    <option value="TRANSFER">TRANSFER</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red">Nominal Pembayaran</b></label>
                                <input type="text" name="nominal_bayar" id="nominal_bayar" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="hidden_div" style="display: none;">
                                <div class="form-group">
                                    <label style="color: red">Bank Transfer</b></label>
                                    <select required class="form-control" name="bank_transfer" id="bank_transfer">
                                        <option value="">-- Pilih Bank --</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="MANDIRI">MANDIRI</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label style="color: red">Bukti Pembayaran</b></label>
                                    <input type="file" name="pic[]" id="pic[]" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>Keterangan</b></label>
                                <input type="text" name="keterangan" id="keterangan" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Tanggal Bayar</b></label>
                                <input type="text" name="tgl_bayar" id="tgl_bayar" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="simpan_aktivasi()">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('pst_registrasi/registrasi/ajax_data_aktivasi') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
    });
    function aktivasi(id_anggota) {
        var link_rm = "<?php echo site_url('pst_registrasi/registrasi/get_data_anggota') ?>";
        link_rm = link_rm + "/" + id_anggota;
        $.get(link_rm, function (data) {
            $('#aktivasi_id').val(data.aktivasi_id);
            $('#id_user').val(data.id_user);
            $('#nama_lengkap').val(data.nama_lengkap);
            $('#parent_nama').val(data.parent_nama);
            $('#alamat').val(data.alamat);
            $('#nominal_bayar').val(data.nominal_bayar);
        }, "json");
        $('#modal-default').modal('show');
    }
    ;
    document.getElementById('cara_bayar').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        cara_bayar = $(this).val();
        console.log(cara_bayar);
        if (cara_bayar == 'TRANSFER') {
            $("#bank_transfer").prop('required', true);
            $("#bukti_pembayaran").prop('required', true);
        } else {
            $("#bank_transfer").prop('required', false);
            $("#bukti_pembayaran").prop('required', false);
        }
    });
    function simpan_aktivasi() {
        $('#form_aktivasi').submit();
    }
    ;
</script>

<script type="text/javascript">
    $(function () {
        $('#tgl_bayar').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>