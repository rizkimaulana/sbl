<div class="page-head">
    <h2>Konfirmasi Fee Anggota </h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Pesantren</a></li>
        <li class="active">Laporan Fee Anggota</li>
    </ol>
</div>  

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Laporan Fee Anggota</b>
                </div>
                <div class="panel-body">
                    <div class="form-group"  style="margin-bottom:50px;">
                        <div class="col-sm-1">
                            <label>Tanggal</label>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" >
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" >
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-success" id="btnSearch" id="btnSearch">Cari</button>
                            <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                        </div>
                    </div>
                    <div class="table-responsive" >
                        <table id="example1" class="table table-bordered table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Penerima</th>
                                    <th>Keterangan</th>
                                    <th>Nominal</th>
                                    <th>Status</th>
                                    <th>Cara Bayar</th>
                                    <th>Bank</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('pst_pembayaran/pembayaran/ajax_lap_fee_anggota') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
        $('#tgl_awal').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#tgl_akhir').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#btnSearch').click(function () {
            var awal = document.getElementById("tgl_awal").value;
            var akhir = document.getElementById("tgl_akhir").value;
            table.ajax.url("<?php echo site_url('pst_pembayaran/pembayaran/ajax_lap_fee_anggota'); ?>/"+awal+"/"+akhir);
            table.ajax.reload();
        });
    });
</script>