<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran_model extends CI_Model {
    
    function get_aktif_fee_anggota(){
        $this->db->select('a.id, a.nominal_bayar, a.status, a.keterangan, c.tanggal_bayar, b.userid, b.nama_lengkap');
        $this->db->from('pst_fee_anggota AS a');
        $this->db->join('pst_anggota AS b', 'b.id = a.to_anggota_id');
        $this->db->join('pst_aktivasi AS c', 'c.id = a.pst_aktivasi_id');
        $this->db->where('a.status', 'PROSES');
        return $this->db->get()->result_array();
    }
    
    function get_fee_anggota($id){
        return $this->db->get_where('pst_fee_anggota', array('id' => $id))->row_array();
    }
    
    function simpan_fee_anggota($data){
        try {
            $fee_anggota = $this->db->get_where('pst_fee_anggota', array('id' => $data['fee_anggota_id']))->row_array();
            $anggota = $this->db->get_where('pst_anggota', array('id' => $fee_anggota['to_anggota_id']))->row_array();
            
            // 1. Update tabel Fee Anggota
            $item_fee_anggota = array(
                'kode_transaksi' => '', // NEXT : generate
                'tanggal_bayar' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'cara_bayar' => $data['cara_bayar'],
                'nominal_bayar' => $data['nominal_bayar'],
                'bank_bayar' => !empty($data['bank_transfer']) ? $data['bank_transfer'] : '',
                'no_rek_bayar' => $data['no_rek_bayar'],
                'nama_rek_bayar' => $data['nama_rek_bayar'],
                'keterangan' => $data['keterangan'],
                'status' => 'KONFIRM',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->update('pst_fee_anggota', $item_fee_anggota, array('id' => $data['fee_anggota_id']));

            // 2. Insert Jurnal Harian
            $item_jurnal_harian = array(
                'pst_ref_akun_id' => '',
                'ref_akun_nama' => '',
                'tanggal' => date('Y-m-d H:i:s'),
                'kode_transaksi' => $item_fee_anggota['kode_transaksi'],
                'group_transaksi' => 'Fee Anggota',
                'keterangan' => 'Fee Anggota untuk '.$anggota['nama_lengkap'],
                'debet' => '',
                'kredit' => $data['nominal_bayar'],
                'saldo' => '',
                'status' => '',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username'),
                'table_id' => $data['fee_anggota_id'],
                'table_name' => 'pst_fee_anggota'
            );
            $this->db->insert('pst_jurnal_harian', $item_jurnal_harian);

            // 3. Insert Jurnal Anggota
            $item_jurnal_anggota = array(
                'pst_anggota_id' => $fee_anggota['to_anggota_id'],
                'tanggal' => date('Y-m-d H:i:s'),
                'kode_transaksi' => $item_fee_anggota['kode_transaksi'],
                'group_transaksi' => 'Fee Anggota',
                'keterangan' => $data['keterangan'],
                'debet' => $data['nominal_bayar'],
                'kredit' => '',
                'saldo' => '',
                'status' => '',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username'),
                'table_id' => $data['fee_anggota_id'],
                'table_name' => 'fee_anggota'
            );
            $this->db->insert('pst_jurnal_anggota', $item_jurnal_anggota);
            return TRUE;
        } catch (Exception $exc) {
            // echo $exc->getTraceAsString();
            return FALSE;
        }
    }
    
    function get_data_angsuran(){
        $this->db->select('');
        $this->db->from('pst_anggota AS a');
        $this->db->where('a.KELOMPOK', 'JAMAAH');
        return $this->db->get()->result_array();
    }
    
    function bayar_angsuran($data){
        // 1. insert to tabel angsuran bayar
        $item_angsuran_bayar = array(
            'pst_anggota_id' => $data['anggota_id'],
            'tanggal' => $data['tgl_bayar'],
            'kode_transaksi' => '',
            'angsuran_ke' => '',
            'keterangan' => $data['keterangan'],
            'cara_bayar' => $data['cara_bayar'],
            'nominal_bayar' => $data['nominal_bayar'],
            'bank_bayar' => $data['bank_transfer'],
            'bukti_bayar' => $data['bukti_pembayaran'],
            'status' => 'PROSES',
            'modified_date' => date('Y-m-d H:i:s'),
            'modified_by' => $this->session->userdata('username')
        );
        return $this->db->insert('pst_angsuran_bayar', $item_angsuran_bayar);
    }
    
    function get_aktif_angsuran_bayar(){
        $this->db->select('a.id, a.nominal_bayar, a.status, a.keterangan, a.tanggal, b.userid, b.nama_lengkap');
        $this->db->from('pst_angsuran_bayar AS a');
        $this->db->join('pst_anggota AS b', 'b.id = a.pst_anggota_id');
        $this->db->where('a.status', 'PROSES');
        //$this->db->where('b.affiliate_id_aff', '');
        return $this->db->get()->result_array();
    }
    
    function get_angsuran_bayar($angsuran_bayar_id){
        return $this->db->get_where('pst_angsuran_bayar', array('id' => $angsuran_bayar_id))->row_array();
    }
    
    function konfirmasi_angsuran($data){
        try {
            $angsuran_bayar = $this->db->get_where('pst_angsuran_bayar', array('id' => $data['angsuran_bayar_id']))->row_array();
            $anggota = $this->db->get_where('pst_anggota', array('id' => $angsuran_bayar['anggota_id']))->row_array();            
            // 1. Update tabel angsuran_bayar
            $item_angsuran_bayar = array(
                'kode_transaksi' => '',
                'tanggal' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'cara_bayar' => $data['cara_bayar'],
                'nominal_bayar' => $data['nominal_bayar'],
                'bank_bayar' => $data['bank_transfer'],
                'bukti_bayar' => $data['bukti_pembayaran'],
                'status' => 'KONFIRM',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->update('pst_angsuran_bayar', $item_angsuran_bayar, array('id' => $data['angsuran_bayar_id']));
            
            // 2. insert tabel angsuran
            $item_angsuran = array(
                'anggota_id' => $angsuran_bayar['anggota_id'],
                'tanggal' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'kode_transaksi' => $item_angsuran_bayar['kode_transaksi'],
                'keterangan' => 'PEMBAYARAN ANGSURAN ANGGOTA ',
                'debet' => $data['nominal_bayar'],
                'kredit' => '',
                'saldo' => '',
                'status' => '',
                'angsuran_ke' => '',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username')
            );
            $this->db->insert('pst_angsuran', $item_angsuran);
            
            // 3. insert tabel jurnal harian
            $item_jurnal_harian = array(
                'ref_akun_id' => '',
                'ref_akun_nama' => '',
                'tanggal' => date('Y-m-d H:i:s'),
                'kode_transaksi' => $item_angsuran_bayar['kode_transaksi'],
                'group_transaksi' => 'Angsuran Anggota',
                'keterangan' => 'Pembayaran Angsuran Anggota '.$anggota['userid'].' - '.$anggota['nama_lengkap'],
                'debet' => $data['nominal_bayar'],
                'kredit' => '',
                'saldo' => '',
                'status' => '',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username'),
                'table_id' => $data['angsuran_bayar_id'],
                'table_name' => 'pst_angsuran_bayar'
            );
            $this->db->insert('pst_jurnal_harian', $item_jurnal_harian);
            
            // 4. insert tabel jurnal anggota (?)
            return TRUE;
        } catch (Exception $exc) {
            // echo $exc->getTraceAsString();
            return FALSE;
        }
    }
    
    function get_jurnal_harian($awal, $akhir){
        $this->db->select('*');
        $this->db->from('pst_jurnal_harian');
        $this->db->order_by('id ASC');
        return $this->db->get()->result_array();
    }
}
?>