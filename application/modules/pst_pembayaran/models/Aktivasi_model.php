<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aktivasi_model extends CI_Model {

    var $_table = 'pst_aktivasi';

    function get_all_data() {
        $this->db->select('a.id, a.kode_unik, a.nominal, a.pst_anggota_id, b.userid, b.nama_lengkap, b.alamat, b.parent_userid, b.telp');
        $this->db->from('pst_aktivasi AS a');
        $this->db->join('pst_anggota AS b ', 'b.id = a.pst_anggota_id');
        $this->db->where('a.status', 'DAFTAR');
        return $this->db->get()->result_array();
    }
    
    function send_sms($nomer_tujuan, $pesan){
        $data = array(
            'nomer_tujuan' => $nomer_tujuan,
            'pesan' => $pesan
        );
        try {
            $return = '0';
            if(empty($nomer_tujuan)){
                throw new Exception('data nomer tujuan tidak valid');
            }
            if(empty($pesan)){
                throw new Exception('data pesan tidak valid');
            }
            $api_element = '/api/web/send/';
            $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=" . $nomer_tujuan . "&message=" . str_replace(' ', '+', $pesan);
            $smsgatewaydata = $api_params;
//            $url = $smsgatewaydata;
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_POST, false);
//            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            if(!curl_exec($ch)){
//                throw new Exception(file_get_contents($smsgatewaydata));
//            }
//            curl_close($ch);
//            $this->simpan_aktivitas('Kirim SMS Notifikasi', $data);
            return TRUE;
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->simpan_error('Kirim SMS Notifikasi', $data, $msg);
            return FALSE;
        }
    }
    

    function get_aktivasi($aktivasi_id) {
        return $this->db->get_where($this->_table, array('id' => $aktivasi_id))->row_array();
    }

    function aktivasi($data) {
        try {
            $aktivasi = $this->get_aktivasi($data['aktivasi_id']);
            if(empty($aktivasi)){
                throw new Exception('data aktivasi tidak ditemukan');
            }

            $anggota = $this->db->get_where('pst_anggota', array('id' => $aktivasi['pst_anggota_id']))->row_array();
            if(empty($anggota)){
                throw new Exception('data jamaah/anggota tidak ditemukan');
            }

            // 1. Update tabel aktivasi
            $item_aktivasi = array(
                'status' => 'BAYAR',
                'keterangan' => $data['keterangan'],
                'cara_bayar' => $data['cara_bayar'],
                'nominal_bayar' => $data['nominal_bayar'],
                'bank_bayar' => $data['bank_transfer'],
                'bukti_bayar' => !empty($data['pic1']) ? $data['pic1'] : '',
                'tanggal_bayar' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->update($this->_table, $item_aktivasi, array('id' => $data['aktivasi_id']));

            // 2. Update tabel anggota
            $item_anggota = array(
                'status' => 'AKTIF',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->update('pst_anggota', $item_anggota, array('id' => $aktivasi['pst_anggota_id']));

            // 3. Insert tabel angsuran
            $item_angsuran = array(
                'pst_anggota_id' => $aktivasi['pst_anggota_id'],
                'tanggal' => date('Y-m-d', strtotime($data['tgl_bayar'])),
                'kode_transaksi' => $aktivasi['kode_transaksi'],
                'keterangan' => 'AKTIVASI PENDAFTARAN',
                'debet' => '',
                'kredit' => 26000000,
                'saldo' => -26000000,
                'status' => '',
                'angsuran_ke' => 0,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username')
            );
            $this->db->insert('pst_angsuran', $item_angsuran);

            // 4. insert tabel jurnal harian
            $item_jurnal_harian = array(
                'pst_ref_akun_id' => '',
                'ref_akun_nama' => '',
                'tanggal' => date('Y-m-d H:i:s'),
                'kode_transaksi' => $aktivasi['kode_transaksi'],
                'group_transaksi' => 'Registrasi',
                'keterangan' => 'Pendaftaran Anggota ' . $anggota['nama_lengkap'],
                'debet' => $data['nominal_bayar'],
                'kredit' => '',
                'saldo' => '',
                'status' => '',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('username'),
                'table_id' => $aktivasi['id'],
                'table_name' => 'pst_aktivasi'
            );
            $this->db->insert('pst_jurnal_harian', $item_jurnal_harian);

            // 5. Update tabel pst_group_fee
            $this->db->update('pst_group_fee', array('status'=>'1'), array('invoice' => $aktivasi['kode_transaksi']));
            
            // 6. insert tabel fee anggota
            // 6.1 Fee Ustadz
            $ustadz = $this->db->get_where('pst_anggota', array('id' => $anggota['parent_id']))->row_array();
            if(empty($ustadz)){
                throw new Exception('data ustadz tidak diketahui');
            }
            $item_fee_ustadz = array(
                'pst_aktivasi_id' => $aktivasi['id'],
                'from_anggota_id' => $anggota['id'],
                'to_anggota_id' => $ustadz['id'],
                'tanggal_bayar' => '',
                'cara_bayar' => '',
                'nominal_bayar' => 350000,
                'bank_bayar' => '',
                'no_rek_bayar' => '',
                'nama_rek_bayar' => '',
                'keterangan' => 'Komisi Pendaftaran a/n ' . $anggota['nama_lengkap'],
                'status' => 'PROSES',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->insert('pst_fee_anggota', $item_fee_ustadz);
            
            // 6.2 Fee Kyai
            $kyai = $this->db->get_where('pst_anggota', array('id' => $ustadz['parent_id']))->row_array();
            if(empty($kyai)){
                throw new Exception('data kyai tidak diketahui');
            }
            $item_fee_kyai = array(
                'pst_aktivasi_id' => $aktivasi['id'],
                'from_anggota_id' => $ustadz['id'],
                'to_anggota_id' => $kyai['id'],
                'tanggal_bayar' => '',
                'cara_bayar' => '',
                'nominal_bayar' => 250000,
                'bank_bayar' => '',
                'no_rek_bayar' => '',
                'nama_rek_bayar' => '',
                'keterangan' => 'Komisi Pendaftaran a/n ' . $anggota['nama_lengkap'],
                'status' => 'PROSES',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->insert('pst_fee_anggota', $item_fee_kyai);
            
            // 6.3 Fee Founder
            $founder = $this->db->get_where('pst_anggota', array('id' => $kyai['parent_id']))->row_array();
            if(empty($founder)){
                throw new Exception('data founder tidak diketahui');
            }
            $item_fee_founder = array(
                'pst_aktivasi_id' => $aktivasi['id'],
                'from_anggota_id' => $kyai['id'],
                'to_anggota_id' => $founder['id'],
                'tanggal_bayar' => '',
                'cara_bayar' => '',
                'nominal_bayar' => 100000,
                'bank_bayar' => '',
                'no_rek_bayar' => '',
                'nama_rek_bayar' => '',
                'keterangan' => 'Komisi Pendaftaran a/n ' . $anggota['nama_lengkap'],
                'status' => 'PROSES',
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $this->session->userdata('username')
            );
            $this->db->insert('pst_fee_anggota', $item_fee_founder);
            
            // 7. CALL SP MENGHITUNG PAJAK FEE SPONSOR
            $stored_procedure = "CALL pajak_pesantren(?, ?)";
            $query = $this->db->query($stored_procedure, array('p_id_affiliate_ustad' => $ustadz['userid'], 'p_id_booking' => $aktivasi['id']));
            
            // 6.4 SEND SMS KE ?          
            if(!empty($anggota)){
                $no_telpon = str_replace(' ', '', $anggota['telp']);
                $nama = $anggota['nama_lengkap'];
                $username = $anggota['userid'];
                $message = $username.' '.$nama.' Terimakasih anda telah terdaftar sebagai member melalui (aktivasi manual) (office.sbl.co.id)';
                $kirim_sms = $this->send_sms($no_telpon, $message);
            }
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }

    function get_lap_registrasi($awal, $akhir) {
        $this->db->select('a.id, a.nominal_bayar, a.created_date as tanggal_daftar, a.tanggal_bayar as tanggal_aktif, a.cara_bayar, a.bank_bayar, a.kode_transaksi, b.parent_id, b.userid, b.nama_lengkap, b.status, b.kelompok');
        $this->db->from('pst_aktivasi_anggota AS a');
        $this->db->join('pst_anggota AS b', 'b.id = a.anggota_id');
        if (!empty($awal) && !empty($akhir)) {
            $this->db->where('DATE_FORMAT(b.tanggal,"%Y-%m-%d") >=', $awal);
            $this->db->where('DATE_FORMAT(b.tanggal,"%Y-%m-%d") <=', $akhir);
        }
        return $this->db->get()->result_array();
    }

}

?>