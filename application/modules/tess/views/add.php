 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-8">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Hotel</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>hotel/save">
           <div class="form-group">
                <label>Room Type</label>
                <select required class="form-control" name="select_bintang">
                    <option value="">-Pilih Bintang</option>
                    <?php foreach($select_bintang as $st){?>
                        <option value="<?php echo $st->id_bintang;?>"><?php echo $st->bintang;?></option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="form-group">
                <label>Nama Hotel </label>
                <input class="form-control" name="nama_hotel" required>
            </div>
              <div class="form-group">
                <label>Keterangan</label>
                
                  <textarea class="form-control" name="keterangan" ></textarea>
                
              </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>hotel" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
