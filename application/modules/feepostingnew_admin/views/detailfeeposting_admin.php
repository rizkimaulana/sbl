
<style type="text/css">
    body {
        font-family: verdana,arial,sans-serif;
        margin: 0px;
        padding: 0px;
    }

    .wrap { 
        width:50%; 
        background:#F0F0F0; 
        margin:auto;
        padding: 25px;
        overflow: hidden;
    }

    h1 {
        text-align: center;
    }

    input.pemberangkatan {
        font-size:28px; 
        width:380px;
    }

    input, textarea {
        border: 1px solid #CCC;
    }
</style>
<div id="page-wrapper">

    <form   class="form-horizontal" role="form" action='<?= base_url(); ?>feepostingnew_admin/save' method='POST'>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Detail Jamaah Fee </b>
                    </div>
                    <div class="panel-body">                       

                        <input type="hidden" name="id_booking" value="<?php echo $fee_posting['id_booking'] ?>">
                        <input type="hidden" name="id_schedule" value="<?php echo $fee_posting['id_schedule'] ?>">
                        <input type="hidden" name="id_affiliate" value="<?php echo $fee_posting['id_affiliate'] ?>">
                        <input type="hidden" name="id_product" value="<?php echo $fee_posting['id_product'] ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>List Fee Posting Affiliate</b>
                                    </div>

                                    <div class="panel-body">


                                        <div class="table-responsive">
                                            <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                            <!-- <th><input id="selecctall"  type="checkbox">&nbsp;Check All</th> -->

                                    <!-- <th>No</th> -->
                                                        <th></th>
                                                        <th>Data Jamaah</th>
                                                        <th>Ket Pembayaran</th>
                                                        <th>Sub Pembayaran </th>
                                                        <th>Fee </th>


                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 0;
                                                    foreach ($pic as $pi) {
                                                        $no++ ?>
                                                        <tr>
                                                            <td width="2"><input size="5" type="hidden" name="data2[<?php echo $no; ?>][id_registrasi]" class="form-control" value="<?php echo $pi['id_registrasi'] ?>" id="id_registrasi" readonly="true"><?php echo $no; ?></td>

    <!-- 	<td width="2"> <input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="<?php echo $pi['id_registrasi'] ?>"></td> -->


                                                            <td width="10"><?php echo $pi['invoice'] ?> <br>
                                                                ID Jamaah = <?php echo $pi['id_jamaah'] ?><br>
                                                                Nama Jamaah = <?php echo $pi['nama_jamaah'] ?><br>
                                                                Paket = <?php echo $pi['paket'] ?> <?php echo $pi['category'] ?><br>
                                                                Bulan Menunggu = <?php echo $pi['bulan_menunggu'] ?> Bulan<br>
                                                                Tipe Jamaah = <?php echo $pi['tipe_jamaah'] ?> <br>
                                                            </td width="20"> 
                                                            <td width="30"><?php echo $pi['ket_pembayaran'] ?>  </td>
                                                            <td width="10">muhrim = <?php echo $pi['muhrim'] ?><br> 
                                                                Cashback = <?php echo number_format($pi['refund']) ?><br>
                                                                Akomodasi = <?php echo number_format($pi['akomodasi']) ?><br>
                                                                Handling = <?php echo number_format($pi['handling']) ?><br>
                                                                Dp = <?php echo number_format($pi['dp_angsuran']) ?><br>
                                                                Jml Angsuran = <?php echo number_format($pi['jml_angsuran']) ?><br>
                                                                Angsuran Perbulan = <?php echo number_format($pi['angsuran']) ?><br>
                                                                Harga Paket = <?php echo number_format($pi['harga_paket']) ?><br>
                                                            </td>
                                                            <td width="20">
                                                                <!-- <?php echo number_format($pi['total_fee']) ?>  -->
                                                                <input size="5" type="text" name="data2[<?php echo $no; ?>][total_fee]" class="form-control" value="<?php echo $pi['total_fee'] ?>" id="total_fee" readonly="true">
                                                                <br>
                                                                user aktivasi : <?php echo $pi['user'] ?>  </td>

                                                        </tr>
<?php } ?> 
                                                </tbody>
                                            </table>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-10">

                                <div class="panel-body">
                                    <!--      <div class="form-group">
                                                   <label class="col-lg-2 control-label">PILIH ACTION </label>
                                                   <div class="col-lg-10">
                                                      <select required class="form-control" name="proses_dana" id="proses_dana">
                                                       <option value="">- PILIH -</option>
                                                       <option value="PROSES_PENGAJUAN">PROSES PENGAJUAN</option>
                                                       <option value="PROSES_TRANSFER">PROSES TRANSFER</option>
                                                 </select> 
                                                   </div>
                                                 </div> -->
                                    <!-- <div id="hidden_div" style="display: none;"> -->
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"for="inputPassword3" >Transfer Date</label>
                                        <div class="col-sm-10">

                                            <input type="text" class="form-control" name="transfer_date" id="datepicker2"  value="<?php echo date('Y-m-d');?>" placeholder="YYYY-MM-DD" required/>

                                        </div>
                                    </div>

                                    <!--<div class="form-group">
                                        <label class="col-lg-2 control-label">Dari Dana Bank </label>
                                        <div class="col-lg-10">
                                            <select required class="form-control" name="dana_bank">
                                                <option value="">- Select Bank -</option>
                                                <?php foreach ($dana_bank as $sb) { ?>
                                                    <option value="<?php echo $sb->id; ?>"><?php echo $sb->nama; ?></option>
<?php } ?>
                                            </select> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Bank Transfer </label>
                                        <div class="col-lg-10">
                                            <select required class="form-control" name="bank_transfer">
                                                <option value="">- Select Bank -</option>
                                                <?php foreach ($bank_transfer as $sb) { ?>
                                                    <option value="<?php echo $sb->id; ?>"><?php echo $sb->nama; ?></option>
<?php } ?>
                                            </select> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">No Rekening</label>
                                        <div class="col-lg-10">
                                            <input name="no_rek" class="form-control" > 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Atas Nama Rekening</label>
                                        <div class="col-lg-10">
                                            <input name="nama_rek" class="form-control"  > 
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Nominal</label>
                                        <div class="col-lg-10">

                                            <input type="test" name="fee" class="form-control" value="<?php echo $fee_posting['jumlah']; ?>"  required> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Keterangan</label>
                                        <div class="col-lg-10">
                                            <textarea class="form-control" name="keterangan" required></textarea>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Approved</button>
                                    <a href="<?php echo base_url(); ?>feepostingnew_admin"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>



                            </div>
                        </div>
                        </form>       

                    </div>
                    <script type="text/javascript">
                        // $('#selecctall').click(function(event) {  //on click
                        //        if(this.checked) { // check select status
                        //            $('.checkbox1').each(function() { //loop through each checkbox
                        //                this.checked = true;  //select all checkboxes with class "checkbox1"              
                        //            });
                        //        }else{
                        //            $('.checkbox1').each(function() { //loop through each checkbox
                        //                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
                        //            });        
                        //        }
                        //    });
                    </script>


                    <script>
                        $(document).ready(function () {
                            $('#dataTables-example').dataTable({
                                "pagingType": "full_numbers",
                                "iDisplayLength": 45});

                        });
                    </script>

                    <script type="text/javascript">
                    // 	$("#clime").on('click', function (e){
                    //     e.preventDefault();
                    // var checkValues = $('.checkbox1:checked').map(function()
                    //     {
                    //         return $(this).val();
                    //     }).get();
                    // //console.log(checkValues); return  false;
                    // $.ajax({
                    //         url: '<?php echo base_url() ?>finance/fee_input/save',
                    //         type: 'post',
                    //         data: 'id_affiliate='+checkValues
                    //         }).done(function(data){
                    //             window.location.reload();
                    // //            $("#respose-text").html(data);
                    //         });
                    //     });
                    //  $('#datepicker2').datetimepicker({
                    //           format: 'YYYY-MM-DD',
                    //         });

                    </script>

                    <script type="text/javascript">
                        //   document.getElementById('proses_dana').addEventListener('change', function () {
                        // var style = this.value == 'PROSES_TRANSFER' ? 'block' : 'none';
                        // document.getElementById('hidden_div').style.display = style;
                        // // document.getElementById('hidden_div2').style.display = style;
                        // // $("#datepicker").prop('required',true);
                        //  proses_dana = $(this).val();

                        //   console.log(proses_dana);

                        //   if(proses_dana=='PROSES_TRANSFER' ){
                        //      $("#dana_bank").prop('required',true);
                        //     $("#pic1").prop('required',true);

                        //   }else{
                        //     $("#dana_bank").prop('required',false);
                        //      $("#pic1").prop('required',false);

                        //   }
                        // });

                        $('#datepicker2').datetimepicker({
                            format: 'YYYY-MM-DD',
                        });

                    </script>