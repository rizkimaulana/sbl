
<div class="page-head">
    <h2>FEE</h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">FEE POSTING JAMAAH</li>

    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>FEE POSTING JAMAAH</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">

                    <form role="form">

                        <!--      <div class="form-group input-group col-lg-4">
                                
                                 <input type="text" class="form-control" id="search" placeholder="Search..." x-webkit-speech>
                                 <span class="input-group-btn">
                                     <button class="btn btn-default" id="btn-search"><i class="fa fa-search"></i></button>
                                 </span>
                               
                             </div> -->
                    </form>           

                    <div class="table-responsive">

                        <table id="data-table" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>


                                    <th>DATA JAMAAH</th>
                                    <th>POSTING DATE</th>
                                    <th>JML</th>
                                    <th>FEE</th>
                                    <th>PAJAK</th>
                                    <th>SETEALAH POTONG PAJAK</th>
                                    <th>TRANSFERRED</th>
                                    <th>PROSES</th>

                                </tr>
                            </thead>
                            <tbody>
                                <!--   <?php $no = 0;
foreach ($pic as $pi) {
    $no++ ?>
                                        <tr>
                                      <td><?php echo $no; ?></td>
                                      <td width="10%"><?php echo $pi['invoice'] ?><br>
    <?php echo $pi['id_affiliate'] ?> <br><?php echo $pi['affiliate'] ?></td>
                                          <td width="10%"><?php echo $pi['create_date'] ?></td>
                                          <td width="8%"><?php echo $pi['jml_jamaah'] ?></td>
                                          <td width="10%"><?php echo number_format($pi['total_fee']) ?></td>
                                           <td width="10%"> <?php echo $pi['pajak'] ?><br>
    <?php echo number_format($pi['Potongan_pajak']) ?><br>
    <?php echo $pi['npwp'] ?>
                                           </td>
                                          <td width="15%"><?php echo number_format($pi['setelah_potong_pajak']) ?></td>
                                          <td>
                                          <a title="TRANSFERRED" class="btn btn-sm btn-primary" href="<?php echo base_url(); ?>feepostingnew_admin/detail/<?php echo $pi['id_booking'] ?>/<?php echo $pi['id_affiliate'] ?>">
                                                      TRANSFERRED
                                                  </a>  </td>
                                                  <td>
                                                    <a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan('<?php echo $pi['id_booking'] ?>')"> <?php echo $pi['proses'] ?></a>
                                                  </td>
                                                   </tr>
<?php } ?> -->
                            </tbody> 
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <!--     <div class="pull-right">
                           <ul class="pagination"></ul>    
                        </div>    -->                  

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script>


    function update_pengajuan(id)
    {
        if (confirm('Apakah Anda Akan Proses Fee Ini '))

        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('feepostingnew_admin/update_pengajuan') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table

                    // location.reload();
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();

                    // location.reload();

                    //  alert('Error deleting data');
                    // window.location.href="<?php echo site_url('no_access') ?>";
                }
            });

        }
    }

    function potong_diawal(id)
    {
        if (confirm('Apakah ANDA Yakin Fee Ini Sudah DiPotong DI AWAL??? '))

        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('feepostingnew_admin/potong_diawal') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table

                    // location.reload();
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();

                    // location.reload();

                    //  alert('Error deleting data');
                    // window.location.href="<?php echo site_url('no_access') ?>";
                }
            });

        }
    }
</script>


<script type="text/javascript">

    var table;

    $(document).ready(function () {

        //datatables
        table = $('#data-table').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('feepostingnew_admin/ajax_list') ?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

    });


</script>


<script>
    // $(document).ready(function () {
    //    $('#data-table').dataTable({
    //      "pagingType": "full_numbers",
    //      "iDisplayLength": 15});

    //  });
</script>


