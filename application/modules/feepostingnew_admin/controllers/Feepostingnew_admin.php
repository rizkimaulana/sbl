<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feepostingnew_admin extends CI_Controller {

    var $folder = "feepostingnew_admin";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(KELOLAH_FEE_POSTING_NEW, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'fee_affiliate');
        $this->load->model('feepostingnewadmin_model');
        $this->load->model('feepostingnewadmin_model', 'fee');
    }

    public function index() {
        //    	      $data = array(
        // 	'pic' => $this->feepostingnewadmin_model->get_data_fee('order by invoice desc')->result_array(),
        // );
        $this->template->load('template/template', $this->folder . '/feepostingnew_admin');
        // $this->load->helper('url');
        // $this->load->view('feepostingnew_admin');
    }

    public function ajax_list() {
        $list = $this->fee->get_datatables();
        
        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : 0 ;
        foreach ($list as $fee) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $affiliate = $this->db->get_where('affiliate', array('id_user' => $fee->id_affiliate))->row_array();
            
            $row[] = $fee->invoice . '-' . $fee->id_affiliate . ' - ' . $affiliate['nama'] ;
            // $row[] = $fee->affiliate;
            $row[] = date('d M Y', strtotime($fee->create_date));
            $row[] = $fee->jml_jamaah;
            $row[] = 'Total Fee : '.number_format($fee->total_fee) . ' <br> Pajak : ' . $fee->pajak . '%<br>'. $fee->npwp;
            $row[] = number_format($fee->potongan_pajak);
            $row[] = number_format($fee->jumlah);

            if ($fee->status == 2) {
                $row[] = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan(' . "'" . $fee->id_booking . "'" . ')">  <i class="fa fa-pencil"></i> PROSES</a> ';
            } elseif ($fee->status == 3) {
                $row[] = '<a title="TRANSFERRED" class="btn btn-sm btn-primary" href="' . base_url() . 'feepostingnew_admin/detail/' . $fee->id_booking . '/' . $fee->id_affiliate . '">
	                            <i class="fa fa-pencil"></i> TRANSFERRED
	                        </a>  ';
            }else{
                $row[] = '';
            }

            $row[] = '<a title="DETAIL" class="btn btn-sm btn-primary" href="' . base_url() . 'feepostingnew_admin/detail_jamaah/' . $fee->id_booking . '/' . $fee->id_affiliate . '">
	                            <i class="fa fa-pencil"></i> DETAIL
	                        </a>  
	                        <a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="potong_diawal(' . "'" . $fee->id_booking . "'" . ')"> Validasi Sudah Potong</a> ';


            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->fee->count_all(),
            "recordsFiltered" => $this->fee->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    private function _select_bank() {

        return $this->db->get('bank')->result();
    }

    public function detail() {

        if (!$this->general->privilege_check(KELOLAH_FEE_POSTING_NEW, 'edit'))
            $this->general->no_access();

        $id_booking = $this->uri->segment(3);
        $id_affiliate = $this->uri->segment(4);
        $fee_posting = $this->feepostingnewadmin_model->get_pic_posting($id_booking, $id_affiliate);
        $pic = array();
        // $pic_booking= array();
        if (!$fee_posting) {
            show_404();
        } else {

            $pic = $this->feepostingnewadmin_model->get_pic($id_booking, $id_affiliate);
        }

        $data = array(
            'dana_bank' => $this->_select_bank(),
            'bank_transfer' => $this->_select_bank(),
            'fee_posting' => $fee_posting, 'pic' => $pic
        );

        $this->template->load('template/template', $this->folder . '/detailfeeposting_admin', ($data));
    }

    public function detail_jamaah() {

        if (!$this->general->privilege_check(KELOLAH_FEE_POSTING_NEW, 'edit'))
            $this->general->no_access();

        $id_booking = $this->uri->segment(3);
        $id_affiliate = $this->uri->segment(4);
        $fee_posting = $this->feepostingnewadmin_model->get_pic_posting($id_booking, $id_affiliate);
        $pic = array();
        // $pic_booking= array();
        if (!$fee_posting) {
            show_404();
        } else {

            $pic = $this->feepostingnewadmin_model->get_pic($id_booking, $id_affiliate);
        }

        $data = array(
            //'dana_bank' => $this->_select_bank(),
            //'bank_transfer' => $this->_select_bank(),
            'fee_posting' => $fee_posting, 'pic' => $pic
        );

        $this->template->load('template/template', $this->folder . '/detail', ($data));
    }

    function save() {
        // 	$this->form_validation->set_rules('checkbox','checkbox','required|trim');
        // 	if($this->form_validation->run()==true){
        //     $inputan = '';
        //     foreach($data2 as $value){
        //     	$inputan .= ($inputan!=='') ? ',' : '';
        //     	$inputan .= $value;
        //     }
        //     $data = $this->input->post(null,true);
        //     $data2 = $this->input->post('checkbox');
        //     $simpan = $this->feepostingnewadmin_model->save($data,$inputan);
        //     if($simpan)
        //         redirect('feepostingnew_admin');
        // }else{
        // print_r("error Anda Belum Memilih Jamaah");
        // }
        // $data = $this->input->post(null,true);
        //    $data2 = $this->input->post('checkbox');
        //    $inputan = '';
        //    foreach($data2 as $value){
        //    	$inputan .= ($inputan!=='') ? ',' : '';
        //    	$inputan .= $value;
        //    }
        //    $simpan = $this->feepostingnewadmin_model->save($data,$inputan);
        //    if($simpan)
        //        redirect('feepostingnew_admin');

        $id_booking = $this->input->post('id_booking');
        $data = $this->input->post(null, true);
        $data2 = $this->input->post('id_registrasi');

        // print_r($data);
        if ($this->feepostingnewadmin_model->save($data)) {

            // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
            redirect('feepostingnew_admin');
        }
    }

    public function update_pengajuan($id_booking) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->feepostingnewadmin_model->update_pengajuan($id_booking);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('feepostingnew_admin');
    }

    public function potong_diawal($id_booking) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->feepostingnewadmin_model->potong_diawal($id_booking);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('feepostingnew_admin');
    }

}
