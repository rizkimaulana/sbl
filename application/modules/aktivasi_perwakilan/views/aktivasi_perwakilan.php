
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Perwakilan Belum Aktif</b>
                </div>

                 <div class="panel-body">
                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID PERWAKILAN</th>
                                    <th>NAMA </th>
                                    <th>ID SAHABAT </th>
                                    <th>SPONSOR </th>
                                    <th>BIAYA AKTIVASI </th>
                                    <th>KETERANGAN </th>
                                   <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; $no<=10; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $pi['id_user']?>  </td>
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['id_sahabat']?>  </td>
                                      
                                      <td><?php echo $pi['sponsor']?>  </td>
                                      <td><?php echo $pi['nominal_dp']?>  </td>
                                     <td><?php echo $pi['ket']?>  </td>
                                       <td>
                                        <div class="btn-group">
                                            <a class="btn-sm btn-primary" title='Edit' href="<?php echo base_url();?>aktivasi_perwakilan/aktivasi/<?php echo $pi['id_mgm']; ?>">Aktivasi</a>
                                   
                                         </div> 
                                                                                         
                                  </td>
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
