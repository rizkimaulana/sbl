<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aktivasiperwakilan_model extends CI_Model{
   
   
    private $_table="aktivasi_affiliate";
   
    private $_primary="id_aff";
    public function __construct()
    {
          parent::__construct();
                 $this->db2 = $this->load->database('db2', TRUE);
    }


public function update($data){
     
        $arr = array(
          'id_mgm'=> $data['id_mgm'],
            'id_user'=> $data['id_user'],
            'sponsor'=> $data['sponsor'],
            'harga_aktivasi' => $data['nominal_pembayaran'],
            'pay_method' => $data['select_pay_method'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            'pic1' => isset($data['pic1']) ? $data['pic1']: '',
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
        );       
        $this->db->trans_begin();
      $this->db->insert('aktivasi_affiliate',$arr);

       $arr = array(
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
        );  

        
          $this->db->update('affiliate',$arr,array('id_user'=>$data['id_user']));  

        $arr = array(
             'status'=>1,
             // 'st_upgrade'=>2,
            'update_by'=>$this->session->userdata('id_user'),
        );  

        
          $this->db->update('member_mgm',$arr,array('id_mgm'=>$data['id_mgm']));  

          $st_upgrade= $this->input->post('st_upgrade');

          if ($st_upgrade == 1 ){


            $arr = array(
                'userid' => $data['id_sahabat'], 
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'tgl_post' => date('Y-m-d H:i:s'),
                'admin'=> 'KONVEN', 
                'st'=> 0, 
                'nominal' => $data['deposit'],
                'sisa' => $data['deposit_sahabat'],
                );    
            $this->db2->insert('t_perwakilan',$arr);
          }elseif ($st_upgrade == 2 ) {
            
            $arr = array(
                // 'st'=> 0, 
                'nominal' => $data['deposit'],
                'sisa' => $data['deposit_sahabat'],
            ); 
            $this->db2->update('t_perwakilan',$arr,array('userid'=>$data['id_sahabat'])); 
          }

          


          $sponsor= $this->input->post('sponsor');

          //



         $sql = "SELECT a.*,COUNT(b.id_user) as jml_downline,
         if((COUNT(b.id_user))<=5,CONCAT('GR',CEILING(COUNT(b.id_user)/5)),
          CONCAT('GR',CEILING(COUNT(b.id_user))))as grouping
            from affiliate as a
            LEFT JOIN member_mgm b ON b.sponsor = a.id_user
            where a.id_affiliate_type in('1','5') and b.status='1' and a.id_user = '".$sponsor."'
            GROUP BY a.id_user";
        $query = $this->db->query($sql)->result_array();

          foreach($query as $key=>$value){
            $grouping = $value['grouping'];

        }
        $arr = array(
          'group_id' => $grouping,
         
        );    
        $this->db->update('member_mgm',$arr,array('id_mgm'=>$data['id_mgm'])); 

        if ($grouping == 'GR1'){
            $arr = array(
              'id_reward' => 1,
             
            );    
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$data['id_mgm'])); 

        }else{

             $arr = array(
              'id_reward' => 2,
             
            );    
            $this->db->update('member_mgm',$arr,array('id_mgm'=>$data['id_mgm'])); 

        }

          //

        // $sql = "SELECT sponsor,group_id,COUNT(*) as jml_mgm, id_reward from member_mgm where status ='1' and sponsor='".$sponsor."' GROUP BY group_id, sponsor";
    // $sql = "SELECT * from affiliate where userid ='$id_affiliate'";
    //     $query = $this->db2->query($sql)->result_array();
    //   foreach($query as $key=>$value){
    //     $nominal_perwakilan= $value['nominal'];
    //   }
          $sql = "SELECT a.sponsor,a.group_id,COUNT(*) as jml_mgm, a.id_reward, b.cash from member_mgm as a,
                  reward_mgm as b where a.status ='1' and a.id_reward = b.id_reward  and a.sponsor='".$sponsor."' and a.group_id ='".$grouping."'
                   GROUP BY a.group_id, a.sponsor";

        $query = $this->db->query($sql)->result_array();

        // if(count($query) > 0){
          foreach($query as $key=>$value){
            $jml_mgm = $value['jml_mgm'];
            $group_id = $value['group_id'];
            $id_reward = $value['id_reward'];
            $cash = $value['cash'];
         
          }
           if ($id_reward ==1 && $jml_mgm ==5){
              $arr = array(
                'id_user' => $sponsor,
                'peringkat' =>1,
                'kelas' =>1,
                'reward' =>'Tiket Umrah',
                'cash' => $cash,
                'sponsor'=> $data['upline'],
                'reward_sponsor'=>'1250000',
                'tipe' =>'UNIT',
                'status' =>1,
                'group_id' =>$group_id,
              );  
              $this->db->insert('reward_dp',$arr);

            }elseif ($id_reward ==2) {
              $arr = array(
                'id_user' => $sponsor,
                'peringkat' =>2,
                'kelas' =>2,
                'reward' =>'UANG',
                'cash' => $cash,
                'sponsor'=> $data['upline'],
                'reward_sponsor'=>'250000',
                'tipe' =>'CASH',
                'status' =>1,
                'group_id' =>$group_id,
              );  
               $this->db->insert('reward_dp',$arr);
              $id =  $this->db->insert_id();

                $sql = "SELECT * from affiliate where id_user='".$sponsor."'";
                $query = $this->db->query($sql)->result_array();
                foreach($query as $key=>$value){
                $sponsor_ = $value['sponsor'];
                $group_id = $value['group_id'];
                $id_reward = $value['id_reward'];
                $cash = $value['cash'];
         
          }

            $arr = array(
                
                'peringkat' =>2,
                'kelas' =>2,
                'reward' =>'UANG',
                'cash' => $cash,
                'tipe' =>'CASH',
                'status' =>1,
                'group_id' =>$group_id,
              );  
                }

 //        $sql = "SELECT * from affiliate
 //          WHERE  id_user = '".$data['id_user']."'";
 //        $query = $this->db->query($sql)->result_array();
 // foreach($query as $key=>$value){
 //            $notlp = $value['telp'];
 //            $nama =$value['nama'];
 // }         

 //      $return = '0';
 //      $api_element = '/api/web/send/';

 //      $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=".$notlp."&message=".str_replace(' ', '+', $nama)."+Terimakasih+anda+telah+terdaftar+sebagai+GM+atau+MGM+SBL+dengan+ID+".$this->input->post('id_user')."+melalaui(aktivasi+manual)+(office.sbl.co.id)";
 //      $smsgatewaydata = $api_params;

 //      $url = $smsgatewaydata;

 //      $ch = curl_init();

 //      curl_setopt($ch, CURLOPT_POST, false);

 //      curl_setopt($ch, CURLOPT_URL, $url);

 //      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $output = curl_exec($ch);

 //      curl_close($ch);

 //      if(!$output){ $output = file_get_contents($smsgatewaydata); }

 //      if($return == '1'){
 //       return $output; 
 //       }
    
      if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;

        }
}
public function get_pic($id_booking){
    
       $sql ="SELECT * from detail_jamaahaktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

 public function get_pic_booking($id_booking){
 

         $sql = "SELECT * from view_booking_aktif where id_booking = {$id_booking}";
 	
        return $this->db->query($sql)->row_array();  
    }


public function get_perwakilan($where= "") {
	// $data = $this->db->query('select * from detail_jamaahnoaktif where id_affiliate="'.$id_user.'" '.$where);
        $data = $this->db->query('select * from transaksi_mgm '.$where);
        return $data;
}

public function get_pic_aktivasi($id_mgm=''){

         $sql = "SELECT * from transaksi_mgm where id_mgm = {$id_mgm}
                ";
        return $this->db->query($sql)->row_array();  
    }
  
}
