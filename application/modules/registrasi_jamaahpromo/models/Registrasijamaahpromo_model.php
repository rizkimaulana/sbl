<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registrasijamaahpromo_model extends CI_Model {

    private $_table = "booking";
    private $_primary = "id_booking";
    private $_registrasi_jamaah = "registrasi_jamaah";
    private $_id_registrasi = "id_registrasi";
    private $_schedule = "schedule";
    private $_id_schedule = "id_schedule";

    function noinvoice() {

        $out = array();
        $this->db->select('id_booking,invoice');
        $this->db->from('booking');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_booking] = $value->invoice;
            }
            return $out;
        } else {
            return array();
        }
    }

    public function generate_kode($idx) {
        $today = date('ymd');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $idx . $today;
    }

    public function save_new($data) {
        $p_initial = "P1";

        $p_id_user_affiliate = $this->input->post('id_user');
        $p_id_product = $this->input->post('id_product');
        $p_id_schedule = $this->input->post('id_schedule');
        $p_id_productprice = $this->input->post('id_productprice');
        $p_category = $this->input->post('category');
        $p_embarkasi = $this->input->post('embarkasi');
        $p_tgl_daftar = date('Y-m-d H:i:s');
        $p_schedule = $this->input->post('schedule');
        $p_harga = $this->input->post('harga');
        $p_create_date = date('Y-m-d H:i:s');
        $p_status_claim_fee = $this->input->post('select_status_fee');
        $p_create_by = $this->session->userdata('id_user');
        $p_kode_unik = $this->input->post('unique_digit');
        // 'ket_keberangkatan'=>$data['select_status_hubungan'],
        $p_tempjmljamaah = $this->input->post('jumlah_jamaah');
        // 'cashback'=>$data['status_cashback'],
        $p_status_fee = $this->input->post('status_fee');
        $p_tipe_jamaah = 1;
        $p_status = 0;

        //SP
        $sql_query = $this->db->query("call [NAMA_SP]('" . $p_initial . "','" . $p_id_user_affiliate . "','" . $p_id_product . "','" . $p_id_schedule . "','" . $p_id_productprice . "','" . $p_category . "','" . $p_embarkasi . "','" . $p_tgl_daftar . "','" . $p_schedule . "','" . $p_harga . "','" . $p_create_date . "','" . $p_status_claim_fee . "','" . $p_create_by . "','" . $p_kode_unik . "','" . $p_tempjmljamaah . "','" . $p_status_fee . "','" . $p_tipe_jamaah . "','" . $p_status . "'");
    }

    public function save($data) {
        $initial = "P1";
        $id_affiliate_type = $this->input->post('select_affiliate_type');

        $arr = array(
            'id_user_affiliate' => $data['id_user'],
            'id_product' => $data['id_product'],
            'id_schedule' => $data['id_schedule'],
            'id_productprice' => $data['id_productprice'],
            'category' => $data['category'],
            'embarkasi' => $data['embarkasi'],
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'schedule' => $data['schedule'],
            'harga' => $data['harga'],
            'create_date' => date('Y-m-d H:i:s'),
            'status_claim_fee' => $data['select_status_fee'],
            'create_by' => $this->session->userdata('id_user'),
            'kode_unik' => $data['unique_digit'],
            // 'ket_keberangkatan'=>$data['select_status_hubungan'],
            'tempjmljamaah' => $data['jumlah_jamaah'],
            // 'cashback'=>$data['status_cashback'],
            'status_fee' => $data['status_fee'],
            'tipe_jamaah' => 1,
            'status' => 0
        );

        $this->db->trans_begin();

        $this->db->insert($this->_table, $arr);
        $id_booking = $this->db->insert_id();

        $invoice = $this->db->insert_id();

        $this->db->update($this->_table, array('invoice' => $this->generate_kode($initial . $id_booking)), array('id_booking' => $id_booking));



        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            redirect('registrasi_jamaahpromo/detail/' . $id_booking . '');
            return true;
        }
    }

    function cek($no_identitas) {
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where no_identitas ='$no_identitas' and id_booking='$id_booking'");
        return $query;
    }

    function cek_room($id_booking) {
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM room_order Where id_booking='$id_booking'");
        return $query;
    }

    public function generate_id_jamaah($idx) {
        $today = date('ym');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $idx . $today;
    }

    public function generate_angka_unik($angkax) {
        $rand = "0123456789";

        $ret = '';

        $limit = 3;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $angkax . $rand;
    }

    public function RandUnik($panjang) {
        $pstring = "0123456789";
        $plen = strlen($pstring); // plen = 10
        for ($i = 1; $i <= $panjang; $i++) {
            $start = rand(0, $plen); // random 0-10 ... semisal dapet 10
            $unik .= substr($pstring, $start, 1);
        }
        return $unik;
    }

    public function save_registrasi_new($data) {
        $id_affiliate_type = $this->input->post('id_affiliate_type');
        if ($id_affiliate_type == 4) {
            $p_id_booking = $this->input->post('id_booking');
            $p_invoice = $this->input->post('invoice');
            $p_id_schedule = $this->input->post('id_schedule');
            $p_id_affiliate = $this->input->post('id_affiliate');
            $p_id_product = $this->input->post('id_product');
            $p_embarkasi = $this->input->post('embarkasi');
            $p_tgl_daftar = date('Y-m-d H:i:s');
            $p_nama = $this->input->post('nama');
            $p_tempat_lahir = $this->input->post('tempat_lahir');
            $p_tanggal_lahir = $this->input->post('tanggal_lahir');
            $p_status_diri = $this->input->post('select_statuskawin');
            $p_kelamin = $this->input->post('select_kelamin');
            $p_rentang_umur = $this->input->post('rentang_umur');
            $p_no_identitas = $this->input->post('no_identitas');
            $p_provinsi_id = $this->input->post('provinsi');
            $p_kabupaten_id = $this->input->post('kabupaten');
            $p_kecamatan_id = $this->input->post('kecamatan');
            $p_alamat = $this->input->post('alamat');
            $p_telp = $this->input->post('telp');
            $p_telp_2 = $this->input->post('telp_2');
            $p_email = $this->input->post('email');
            $p_ket_keberangkatan = $this->input->post('select_status_hubungan');
            $p_ahli_waris = $this->input->post('waris');
            $p_hub_waris = $this->input->post('select_hub_ahliwaris');
            $p_room_type = 1;
            $p_category = $this->input->post('category');
            $p_merchandise = $this->input->post('select_merchandise');
            $p_id_bandara = $this->input->post('id_bandara');
            $p_refund = $this->input->post('refund');
            $p_handling = $this->input->post('handling');
            $p_akomodasi = $this->input->post('akomodasi');
            $p_fee = 0;
            $p_fee_input = 0;
            $p_harga_paket = $this->input->post('harga');
            $p_create_by = $this->session->userdata('id_user');
            $p_create_date = date('Y-m-d H:i:s');
            $p_status = 0;
            $p_tipe_jamaah = 1;
            $p_kode_unik = $this->input->post('unique_digit');
            $p_status_fee = 0;


            //$p_id_registrasi = $id_registrasi;
            //$p_id_jamaah = $this->generate_id_jamaah($id_registrasi);

            $p_no_pasport = $this->input->post('no_pasport');
            $p_issue_office = $this->input->post('issue_office');
            $p_isui_date = $this->input->post('isue_date');
            $p_status_identitas = $this->input->post('status_identitas');
            $p_status_kk = $this->input->post('status_kk');
            $p_status_photo = $this->input->post('status_photo');
            $p_status_pasport = $this->input->post('status_pasport');
            $p_status_vaksin = $this->input->post('status_vaksin');
            $p_hubkeluarga = $this->input->post('hubungan');

            $p_status_visa = $this->input->post('select_status_visa');
            $p_tgl_visa = $this->input->post('tanggal_visa');

            //SP
            $sql_query = $this->db->query("call [NAMA_SP]('" . $p_id_booking . "','" . $p_invoice . "','" . $p_id_schedule . "','" . $p_id_affiliate . "','" . $p_id_product . "','" . $p_embarkasi . "','" . $p_tgl_daftar . "','" . $p_nama . "','" . $p_tempat_lahir . "','" . $p_tanggal_lahir . "','" . $p_status_diri . "','" . $p_kelamin . "','" . $p_rentang_umur . "','" . $p_no_identitas . "','" . $p_provinsi_id . "','" . $p_kabupaten_id . "','" . $p_kecamatan_id . "','" . $p_alamat . "','" . $p_telp . "','" . $p_telp_2 . "','" . $p_email . "','" . $p_ket_keberangkatan . "','" . $p_ahli_waris . "','" . $p_hub_waris . "','" . $p_room_type . "','" . $p_category . "','" . $p_merchandise . "','" . $p_id_bandara . "','" . $p_refund . "','" . $p_handling . "','" . $p_akomodasi . "','" . $p_fee . "','" . $p_fee_input . "','" . $p_harga_paket . "','" . $p_create_by . "','" . $p_create_date . "','" . $p_status . "','" . $p_tipe_jamaah . "','" . $p_kode_unik . "','" . $p_status_fee . "','" . $p_no_pasport . "','" . $p_issue_office . "','" . $p_isui_date . "','" . $p_status_identitas . "','" . $p_status_kk . "','" . $p_status_photo . "','" . $p_status_pasport . "','" . $p_status_vaksin . "','" . $p_hubkeluarga . "','" . $p_status_visa . "','" . $p_tgl_visa . "'");
        } else {

            $p_id_booking = $this->input->post('id_booking');
            $p_invoice = $this->input->post('invoice');
            $p_id_schedule = $this->input->post('id_schedule');
            $p_id_affiliate = $this->input->post('id_affiliate');
            $p_id_product = $this->input->post('id_product');
            $p_embarkasi = $this->input->post('embarkasi');
            $p_tgl_daftar = date('Y-m-d H:i:s');
            $p_nama = $this->input->post('nama');
            $p_tempat_lahir = $this->input->post('tempat_lahir');
            $p_tanggal_lahir = $this->input->post('tanggal_lahir');
            $p_status_diri = $this->input->post('select_statuskawin');
            $p_kelamin = $this->input->post('select_kelamin');
            $p_rentang_umur = $this->input->post('rentang_umur');
            $p_no_identitas = $this->input->post('no_identitas');
            $p_provinsi_id = $this->input->post('provinsi');
            $p_kabupaten_id = $this->input->post('kabupaten');
            $p_kecamatan_id = $this->input->post('kecamatan');
            $p_alamat = $this->input->post('alamat');
            $p_telp = $this->input->post('telp');
            $p_telp_2 = $this->input->post('telp_2');
            $p_email = $this->input->post('email');
            $p_ket_keberangkatan = $this->input->post('select_status_hubungan');
            $p_ahli_waris = $this->input->post('waris');
            $p_hub_waris = $this->input->post('select_hub_ahliwaris');
            $p_room_type = 1;
            $p_category = $this->input->post('category');
            $p_merchandise = $this->input->post('select_merchandise');
            $p_id_bandara = $this->input->post('id_bandara');
            $p_refund = $this->input->post('refund');
            $p_handling = $this->input->post('handling');
            $p_akomodasi = $this->input->post('akomodasi');
            $p_fee = $this->input->post('fee_posting_paketpromo');
            $p_fee_input = 0;
            $p_harga_paket = $this->input->post('harga');
            $p_create_by = $this->session->userdata('id_user');
            $p_create_date = date('Y-m-d H:i:s');
            $p_status = 0;
            $p_tipe_jamaah = 1;
            $p_kode_unik = $this->input->post('unique_digit');
            $p_status_fee = $this->input->post('status_fee');
            //$p_id_jamaah = $this->generate_id_jamaah($id_registrasi);

            $p_no_pasport = $this->input->post('no_pasport');
            $p_issue_office = $this->input->post('issue_office');
            $p_isui_date = $this->input->post('isue_date');
            $p_status_identitas = $this->input->post('status_identitas');
            $p_status_kk = $this->input->post('status_kk');
            $p_status_photo = $this->input->post('status_photo');
            $p_status_pasport = $this->input->post('status_pasport');
            $p_status_vaksin = $this->input->post('status_vaksin');
            $p_hubkeluarga = $this->input->post('hubungan');

            $p_sponsor = $this->input->post('id_refrensi');
            $p_fee_sponsor = $this->input->post('fee_sponsor_paketpromo');
            $p_status_free = 0;
            $p_status_visa = $this->input->post('select_status_visa');
            $p_tgl_visa = $this->input->post('tanggal_visa');

            //SP
            $sql_query = $this->db->query("call [NAMA_SP]('" . $p_id_booking . "','" . $p_invoice . "','" . $p_id_schedule . "','" . $p_id_affiliate . "','" . $p_embarkasi . "','" . $p_tgl_daftar . "','" . $p_nama . "','" . $p_tempat_lahir . "','" . $p_tanggal_lahir . "','" . $p_status_diri . "','" . $p_kelamin . "','" . $p_rentang_umur . "','" . $p_no_identitas . "','" . $p_provinsi_id . "','" . $p_kabupaten_id . "','" . $p_kecamatan_id . "','" . $p_alamat . "','" . $p_telp . "','" . $p_telp_2 . "','" . $p_email . "','" . $p_ket_keberangkatan . "','" . $p_ahli_waris . "','" . $p_hub_waris . "','" . $p_room_type . "','" . $p_category . "','" . $p_merchandise . "','" . $p_id_bandara . "','" . $p_refund . "','" . $p_handling . "','" . $p_akomodasi . "','" . $p_fee . "','" . $p_fee_input . "','" . $p_harga_paket . "','" . $p_create_by . "','" . $p_create_date . "','" . $p_status . "','" . $p_tipe_jamaah . "','" . $p_kode_unik . "','" . $p_status_fee . "','" . $p_no_pasport . "','" . $p_issue_office . "','" . $p_isui_date . "','" . $p_status_identitas . "','" . $p_status_kk . "','" . $p_status_photo . "','" . $p_status_pasport . "','" . $p_status_vaksin . "','" . $p_hubkeluarga . "','" . $p_id_booking . "','" . $p_sponsor . "','" . $p_fee_sponsor . "','" . $p_status_free . "','" . $p_status_visa . "','" . $p_tgl_visa . "'");
        }
    }

    public function save_registrasi($data) {

        $id_affiliate_type = $this->input->post('id_affiliate_type');

        if ($id_affiliate_type == 4) {
            $arr = array(
                'id_booking' => $data['id_booking'],
                'invoice' => $data['invoice'],
                'id_schedule' => $data['id_schedule'],
                'id_affiliate' => $data['id_affiliate'],
                'id_product' => $data['id_product'],
                'embarkasi' => $data['embarkasi'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tanggal_lahir' => $data['tanggal_lahir'],
                'status_diri' => $data['select_statuskawin'],
                'kelamin' => $data['select_kelamin'],
                'rentang_umur' => $data['rentang_umur'],
                'no_identitas' => $data['no_identitas'],
                'provinsi_id' => $data['provinsi'],
                'kabupaten_id' => $data['kabupaten'],
                'kecamatan_id' => $data['kecamatan'],
                'alamat' => $data['alamat'],
                'telp' => $data['telp'],
                'telp_2' => $data['telp_2'],
                'email' => $data['email'],
                'ket_keberangkatan' => $data['select_status_hubungan'],
                'ahli_waris' => $data['waris'],
                'hub_waris' => $data['select_hub_ahliwaris'],
                'room_type' => 1,
                'category' => $data['category'],
                'merchandise' => $data['select_merchandise'],
                'id_bandara' => $data['id_bandara'],
                'refund' => $data['refund'],
                'handling' => $data['handling'],
                'akomodasi' => $data['akomodasi'],
                'fee' => 0,
                'fee_input' => 0,
                'harga_paket' => $data['harga'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'tipe_jamaah' => 1,
                'kode_unik' => $data['unique_digit'],
                'status_fee' => 0,
            );
            $this->db->trans_begin();

            $this->db->insert($this->_registrasi_jamaah, $arr);
            $id_registrasi = $this->db->insert_id();

            $this->db->update($this->_registrasi_jamaah, array('id_jamaah' => $this->generate_id_jamaah($id_registrasi)), array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_registrasi' => $id_registrasi,
                'id_jamaah' => $this->generate_id_jamaah($id_registrasi),
                'id_affiliate' => $data['id_affiliate'],
                'no_pasport' => $data['no_pasport'],
                'issue_office' => $data['issue_office'],
                'isui_date' => $data['isue_date'],
                'status_identitas' => $data['status_identitas'],
                'status_kk' => $data['status_kk'],
                'status_photo' => $data['status_photo'],
                'status_pasport' => $data['status_pasport'],
                'status_vaksin' => $data['status_vaksin'],
                'hubkeluarga' => $data['hubungan'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
            );
            $this->db->insert('manifest', $arr);

            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status' => 0
            );
            $this->db->insert('pengiriman', $arr);



            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status_visa' => $data['select_status_visa'],
                'tgl_visa' => $data['tanggal_visa'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'status' => 0
            );
            $this->db->insert('visa', $arr);


            $sql = "call hitung_muhrim(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                $id_registrasi = $value['id_registrasi'];
                $biaya_muhrim = $value['muhrim'];
            }

            $arr = array(
                'muhrim' => $biaya_muhrim,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));



            $sql = "call v_visa(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                $id_registrasi = $value['id_registrasi'];
                $biaya_visa = $value['Total_VISA'];
            }

            $arr = array(
                'visa' => $biaya_visa,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
                return false;
            } else {

                $this->db->trans_complete();

                return true;
            }
        } else {


            $arr = array(
                'id_booking' => $data['id_booking'],
                'invoice' => $data['invoice'],
                'id_schedule' => $data['id_schedule'],
                'id_affiliate' => $data['id_affiliate'],
                'id_product' => $data['id_product'],
                'embarkasi' => $data['embarkasi'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tanggal_lahir' => $data['tanggal_lahir'],
                'status_diri' => $data['select_statuskawin'],
                'kelamin' => $data['select_kelamin'],
                'rentang_umur' => $data['rentang_umur'],
                'no_identitas' => $data['no_identitas'],
                'provinsi_id' => $data['provinsi'],
                'kabupaten_id' => $data['kabupaten'],
                'kecamatan_id' => $data['kecamatan'],
                'alamat' => $data['alamat'],
                'telp' => $data['telp'],
                'telp_2' => $data['telp_2'],
                'email' => $data['email'],
                'ket_keberangkatan' => $data['select_status_hubungan'],
                'ahli_waris' => $data['waris'],
                'hub_waris' => $data['select_hub_ahliwaris'],
                'room_type' => 1,
                'category' => $data['category'],
                'merchandise' => $data['select_merchandise'],
                'id_bandara' => $data['id_bandara'],
                'refund' => $data['refund'],
                'handling' => $data['handling'],
                'akomodasi' => $data['akomodasi'],
                'fee' => $data['fee_posting_paketpromo'],
                'fee_input' => 0,
                'harga_paket' => $data['harga'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'tipe_jamaah' => 1,
                'kode_unik' => $data['unique_digit'],
                'status_fee' => $data['status_fee'],
            );
            $this->db->trans_begin();

            $this->db->insert($this->_registrasi_jamaah, $arr);
            $id_registrasi = $this->db->insert_id();

            $this->db->update($this->_registrasi_jamaah, array('id_jamaah' => $this->generate_id_jamaah($id_registrasi)), array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_registrasi' => $id_registrasi,
                'id_jamaah' => $this->generate_id_jamaah($id_registrasi),
                'id_affiliate' => $data['id_affiliate'],
                'no_pasport' => $data['no_pasport'],
                'issue_office' => $data['issue_office'],
                'isui_date' => $data['isue_date'],
                'status_identitas' => $data['status_identitas'],
                'status_kk' => $data['status_kk'],
                'status_photo' => $data['status_photo'],
                'status_pasport' => $data['status_pasport'],
                'status_vaksin' => $data['status_vaksin'],
                'hubkeluarga' => $data['hubungan'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
            );
            $this->db->insert('manifest', $arr);


            $arr = array(
                'invoice' => $data['invoice'],
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'id_schedule' => $data['id_schedule'],
                'id_product' => $data['id_product'],
                'id_affiliate' => $data['id_affiliate'],
                'sponsor' => $data['id_refrensi'],
                'fee' => $data['fee_posting_jamaahpaketpromo'],
                'fee_sponsor' => $data['fee_sponsor_paketpromo'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'status_fee' => $data['status_fee'],
                'status_free' => 0
            );

            $this->db->insert('fee', $arr);

            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'id_schedule' => $data['id_schedule'],
                'id_product' => $data['id_product'],
                'id_affiliate' => $data['id_affiliate'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0
            );

            $this->db->insert('list_gaji', $arr);



            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status' => 0
            );
            $this->db->insert('pengiriman', $arr);


            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status_visa' => $data['select_status_visa'],
                'tgl_visa' => $data['tanggal_visa'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'status' => 0
            );
            $this->db->insert('visa', $arr);

            $sql = "call hitung_muhrim(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                $id_registrasi = $value['id_registrasi'];
                $biaya_muhrim = $value['muhrim'];
            }

            $arr = array(
                'muhrim' => $biaya_muhrim,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));

            $sql = "call v_visa(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                $id_registrasi = $value['id_registrasi'];
                $biaya_visa = $value['Total_VISA'];
            }

            $arr = array(
                'visa' => $biaya_visa,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
                return false;
            } else {

                $this->db->trans_complete();

                return true;
            }
        }

        // }
    }

    public function update_setting_keluarga($data, $data2 = '') {
        $id_jamaah = $this->input->post('id_jamaah');
        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_jamaah = $pi['id_jamaah'];
                $tmp = array(
                    'id_jamaah' => $pi['id_jamaah'],
                    'muhrim' => $pi['muhrim'],
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('registrasi_jamaah', $pic, 'id_jamaah');
            // print_r($pic);
        }


        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_jamaah = $pi['id_jamaah'];
                $tmp = array(
                    'id_jamaah' => $pi['id_jamaah'],
                    'hubkeluarga' => $pi['hubungan'],
                    'keluarga' => $pi['select_keluarga'],
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('manifest', $pic, 'id_jamaah');
        }



        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            return true;
        }
    }

    public function get_pic($id_booking) {

        $sql = "SELECT * from registrasi_jamaah WHERE  id_booking  = ? 
              ";
        return $this->db->query($sql, array($id_booking))->result_array();
    }

    public function get_pic_keluarga($id_booking, $id) {

        $stored_procedure = "call setting_keluarga(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking, 'create_by ' => $id
                ))->result_array();
    }

    public function get_pic_booking($id_booking, $id) {

        $stored_procedure = "call view_booking(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    public function get_pic_order_view($id_booking, $id) {


        $stored_procedure = "call room_order_view(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    public function get_pic_room_type($id_booking) {

        $stored_procedure = "call list_order_room_view(?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking))->result_array();
    }

    public function get_pic_booking_room($id_booking, $id) {
        $stored_procedure = "call room_order_view(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    public function get_pic_room_price($category_id) {
        $category = $this->input->post('category');
        $sql = "SELECT * from room_price where category_id = ?
         ";
        return $this->db->query($sql, array($category_id))->result_array();
    }

    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->opsi_setting] = $value->key_setting;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_biaya_room() {

        $out = array();
        $this->db->select('id_room,room,harga_idr,kapasitas');
        $this->db->from('view_get_room_price');
        // $this->db->where('room_type','2');
        // $this->db->where('category','1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->room] = $value->harga_idr;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_biaya_refund() {
        $this->db->select('*');
        $this->db->from('view_refund');
        // $this->db->where('tampil', 'Y');
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function get_allrefund() {
        $this->db->from('view_refund');
        // $this->db->from($this->tabel);
        $query = $this->db->get();

        //cek apakah ada data
        if ($query->num_rows() > 0) { //jika ada maka jalankan
            return $query->result();
        }
    }

    public function get_booking($id_booking) {

        $sql = "SELECT a.*,b.nama as koordinator, c.nama as paket FROM booking a
              LEFT JOIN affiliate b ON b.id_user = a.id_user_affiliate
              LEFT JOIN product c ON c.id_product = a.id_product
              WHERE a.id_booking = ?
              ";

        return $this->db->query($sql, array($id_booking))->row_array();
    }

    public function searchrefrensi() {

        // $segment = intval($this->uri->segment(4));
        $sql = "SELECT * from view_schedule ";
        $query = $this->db->query($sql);
        return $query;

        //   $sql = 'SELECT * from view_schedule  where id_product = '.$paket.' and BulanKeberangkatan='.$datepicker_keberangkatan.' 
        // and TahunKeberangkatan='.$datepicker_tahun_keberangkatan.' and embarkasi='.$departure.'';
        // $query = $this->db->query($sql);
        // return $query;
    }

    function searchItem($datepicker_tahun_keberangkatan, $datepicker_keberangkatan, $departure) {


        $jumlah_jamaah = $this->input->post('jumlah_jamaah');
        $sql = "SELECT * from view_schedule_promo where  BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'";
        $query = $this->db->query($sql);

        if (empty($query->result())) {

            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong ' . $jumlah_jamaah . '</h2></td></tr>';
            exit;
        } else {


            foreach ($query->result() as $row) {


                echo'<tr>
                   
                   <td><strong>' . $row->paket . '</strong></td>
                   <td><strong>' . $row->departure . '</strong></td>
                   <td><strong>' . $row->date_schedule . '</strong></td>
                  
                   <td><strong>' . $row->seats . '</strong></td>
                   <td><strong>' . $row->type . '</strong></td>
                   <td><strong>' . $row->category . ' - ' . $row->hari . ' hari</strong></td>
                   
                   <td><strong>' . number_format($row->harga) . '</strong></td>
              
                  
                   <td><div class="btn-group"><button type="submit" formaction="' . base_url() . 'registrasi_jamaahpromo/schedule/' . $row->id_schedule . '"  class="btn btn-info btn-sm" title="Pilih Waktu Keberangkatan"><i class="glyphicon glyphicon-pencil"></i></a>
                </a></div>
                </td>
                </tr>';
            }
        }
    }

    function get_keluarga() {

        $query = $this->db->get('registrasi_jamaah');
        return $query->result();
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_all_kabupaten() {
        $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");

        return $query->result();
    }

    function get_provinsi() {

        $query = $this->db->get('wilayah_provinsi');
        return $query->result();
    }

    function get_kabupaten() {

        $query = $this->db->get('wilayah_kabupaten');
        return $query->result();
    }

    function get_kecamatan() {

        $query = $this->db->get('wilayah_kecamatan');
        return $query->result();
    }

    public function get_data($offset, $limit, $q = '') {

        $id = $this->session->userdata('id_user');
        $sql = " SELECT * from data_laporan_jamah_belumaktif where 1=1 
                    ";

        if ($q) {

            $sql .= "  and affiliate LIKE '%{$q}%'
                     OR invoice LIKE '%{$q}%'
                     OR departure LIKE '%{$q}%'
                     OR tgl_daftar LIKE '%{$q}%'
                     OR paket LIKE '%{$q}%'
                      OR user_create LIKE '%{$q}%'
            ";
        }
        $sql .= " ORDER BY tgl_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .= " LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    function get_data_bandara($pemberangkatan = '') {
        $this->db->where("pemberangkatan", $pemberangkatan);
        return $this->db->get("view_refund");
    }

    function get_nama_jamaah() {
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status', '0');
        $this->db->order_by('id_registrasi', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function get_data_jamaah($id) {
        $this->db->select('*');
        $this->db->from('data_laporan_jamah_belumaktif_all');
        $this->db->where('id_booking', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    public function delete($id_aff) {

        $this->db->trans_begin(); //transaction initialize

        $this->db->delete($this->table, array('id_aff' => $id_aff));
        // $this->db->delete('user',array('id_user'=>$id_user));

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            return true;
        }
    }

    public function delete_by_id($id_aff) {
        $this->db->where('id_aff', $id_aff);
        $this->db->delete($this->_table);
    }

    function lap_data_jamaah($id_booking) {
        $stored_procedure = "call faktur_jamaah(?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking))->result();
    }

    function lap_data_perjamaah($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_booking', $id_booking);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }

    public function get_report_registrasi($id_booking, $id) {

        $stored_procedure = "call data_laporan_jamah_belumaktif_all(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    function get_all_provinsi_manasik() {
        $this->db->distinct();
        $this->db->select('provinsi_id,provinsi');
        $this->db->from('view_manasik');
        $query = $this->db->get();

        return $query->result();
    }

    public function update_room_order_new($data) {
        $p_id_booking = $this->input->post('id_booking');
        $p_category = $this->input->post('category');
        $p_id_product = $this->input->post('id_product');
        $p_room_type = $this->input->post('room_type');
        $p_double = $this->input->post('double');
        $p_triple = $this->input->post('triple');
        $p_pre_double = $this->input->post('pre_double');
        $p_pre_triple = $this->input->post('pre_triple');
        $p_jumlah_jamaah = $this->input->post('jumlah_jamaah');

        //$p_id_booking = $this->input->post('id_booking');
        //$p_id_product = $this->input->post('id_product');
        //$p_category = $this->input->post('category');
        $p_room_type1 = 2;
        $p_jumlah = $this->input->post('double');
        //$p_harga = $total_harga_double;
        $p_status = 0;
        $p_status_room_order = 0;
        $p_create_date = date('Y-m-d H:i:s');
        $p_create_by = $this->session->userdata('id_user');
        //$p_id_booking = $this->input->post('id_booking');
        //$p_id_product = $this->input->post('id_product');
        //$p_category = $this->input->post('category');
        $p_room_type2 = 3;
        //$p_jumlah = $this->input->post('triple');
        //$p_harga = $total_harga_triple;
        //$p_status = 0;
        //$p_status_room_order = 0;
        //$p_create_date = date('Y-m-d H:i:s');
        //$p_create_by = $this->session->userdata('id_user');
        //$p_id_booking = $this->input->post('id_booking');
        //$p_category = $this->input->post('category');
        //$p_harga = $harga_double;
        //$p_status = 0;
        //$p_create_date = date('Y-m-d H:i:s');
        //$p_create_by = $this->session->userdata('id_user');
        //$p_id_booking = $this->input->post('id_booking');
        //$p_category = $this->input->post('category');
        //$p_harga = $harga_triple;
        //$p_status = 0;
        //$p_create_date = date('Y-m-d H:i:s');
        //$p_create_by = $this->session->userdata('id_user');
        //SP
        $sql_query = $this->db->query("call [NAMA_SP]('" . $p_id_booking . "','" . $p_category . "','" . $p_id_product . "','" . $p_room_type . "','" . $p_double . "','" . $p_triple . "','" . $p_pre_double . "','" . $p_pre_triple . "','" . $p_jumlah_jamaah . "','" . $p_room_type1 . "','" . $p_jumlah . "','" . $p_status . "','" . $p_status_room_order . "','" . $p_create_date . "','" . $p_create_by . "','" . $p_room_type2 . "'");
    }

    public function update_room_order($data) {
        $id_booking = $this->input->post('id_booking');
        $category = $this->input->post('category');
        $id_product = $this->input->post('id_product');
        $room_type = $this->input->post('room_type');
        $double = $this->input->post('double');
        $triple = $this->input->post('triple');
        $pre_double = $this->input->post('pre_double');
        $pre_triple = $this->input->post('pre_triple');
        $jumlah_jamaah = $this->input->post('jumlah_jamaah');

        $sql = "SELECT * from room where category = '$category' and room_type ='2'";
        $query = $this->db->query($sql)->result_array();
        foreach ($query as $key => $value) {
            $harga_double = $value['harga_idr'];
        }

        $sql2 = "SELECT * from room where category = '$category' and room_type ='3'";
        $query = $this->db->query($sql2)->result_array();
        foreach ($query as $key => $value) {
            $harga_triple = $value['harga_idr'];
        }

        $pilihan_double = $double * 2;
        $pilihan_triple = $triple * 3;


        $total_pilihan = $pilihan_double + $pilihan_triple;
        if ($total_pilihan > $jumlah_jamaah) {
            $this->session->set_flashdata('info', "Jamaah yang anda input = " . $jumlah_jamaah . " Orang,  
          <br>Jumlah Kamar yang and Input tidak Sesuai dengan jumlah jamaah yang anda input, isi jumlah kamar sesuai jumlah jamaah yang anda input!!");
            redirect('registrasi_jamaahpromo/room_question/' . $id_booking . '/' . $category . '');
        } else {



            $total_harga_double = $harga_double * $double;
            $total_harga_triple = $harga_triple * $triple;
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_product' => $data['id_product'],
                'category' => $data['category'],
                'room_type' => 2,
                'jumlah' => $data['double'],
                'harga' => $total_harga_double,
                'status' => 0,
                'status_room_order' => 0,
                'create_date' => date('Y-m-d H:i:s'),
                'create_by' => $this->session->userdata('id_user'),
            );

            $this->db->insert('room_order', $arr);

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_product' => $data['id_product'],
                'category' => $data['category'],
                'room_type' => 3,
                'jumlah' => $data['triple'],
                'harga' => $total_harga_triple,
                'status' => 0,
                'status_room_order' => 0,
                'create_date' => date('Y-m-d H:i:s'),
                'create_by' => $this->session->userdata('id_user'),
            );

            $this->db->insert('room_order', $arr);

            for ($i = 0; $i < $pilihan_double; $i++) {
                $arr = array(
                    'id_booking' => $data['id_booking'],
                    'category' => $data['category'],
                    'room_type' => 2,
                    'harga' => $harga_double,
                    'status' => 0,
                    'create_date' => date('Y-m-d H:i:s'),
                    'create_by' => $this->session->userdata('id_user'),
                );
                $this->db->insert('list_order_room', $arr);
            }
            for ($i = 0; $i < $pilihan_triple; $i++) {
                $arr = array(
                    'id_booking' => $data['id_booking'],
                    'category' => $data['category'],
                    'room_type' => 3,
                    'harga' => $harga_triple,
                    'status' => 0,
                    'create_date' => date('Y-m-d H:i:s'),
                    'create_by' => $this->session->userdata('id_user'),
                );
                $this->db->insert('list_order_room', $arr);
            }
            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                return true;
            }
        }
    }

    public function update_room_setting($data, $data2 = '', $data3 = '') {
        $id_jamaah = $this->input->post('select_jamaah');
        $id_booking = $this->input->post('id_booking');

        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_jamaah = $pi['select_jamaah'];
                $room_order = $pi['room_order'];
                $room_type = $pi['room_type'];
                $tmp = array(
                    'id_jamaah' => $id_jamaah,
                    'room_order' => $room_order,
                        // 'room_type' => $room_type,
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('registrasi_jamaah', $pic, 'id_jamaah');
        }

        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_listorderroom = $pi['id_listorderroom'];
                $id_jamaah = $pi['select_jamaah'];

                $tmp = array(
                    'id_listorderroom' => $id_listorderroom,
                    'id_jamaah' => $id_jamaah,
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('list_order_room', $pic, 'id_listorderroom');
        }


        // $arr = array(
        //       'room_type' => $pi['select_type']
        //   );     
        //     $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>IN ($data2))); 

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            return true;
        }
    }

    function get_id_group_concat_keluarga() {

        $out = array();
        $this->db->select('id_con,id_relation');
        $this->db->from('group_concat_keluarga');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_con] = $value->id_relation;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_ket_group_concat_keluarga() {

        $out = array();
        $this->db->select('ket, keterangan');
        $this->db->from('group_concat_keluarga');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->ket] = $value->keterangan;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_group_concat_id_jamaah() {

        $out = array();
        $this->db->select('id_booking, id_jamaah');
        $this->db->from('group_concat_jamaah');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_booking] = $value->id_jamaah;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_group_concat_jamaah() {

        $out = array();
        $this->db->select('id_booking, jamaah');
        $this->db->from('group_concat_jamaah');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_booking] = $value->jamaah;
            }
            return $out;
        } else {
            return array();
        }
    }

    function getaffiliatetype() {
        $this->db->select('id_affiliate_type,affiliate_type');
        $this->db->from('affiliate_type');
        $this->db->order_by('id_affiliate_type', 'asc');
        $query = $this->db->get();
        return $query;
    }

    function getData($loadType, $loadId) {
        if ($loadType == "affiliate") {
            $fieldList = 'id_user,nama';
            $table = 'affiliate';
            $fieldName = 'id_user';
            $orderByField = 'id_user';
        }

        $this->db->select($fieldList);
        $this->db->from($table);
        $this->db->where($fieldName, $loadId);
        $this->db->order_by($orderByField, 'asc');
        $query = $this->db->get();
        return $query;
    }

}
