<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi_jamaahpromo extends CI_Controller{
	var $folder = "registrasi_jamaahpromo";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'view'))
		    $this->general->no_access();	
		$this->session->set_userdata('menu','registration');	
		$this->load->model('registrasijamaahpromo_model');
		$this->load->helper('fungsi');
		$this->load->library('terbilang');
	}
	
	public function get_registrasi2(){

		$this->template->load('template/template', $this->folder.'/index_jamaah');
	}
	public function index(){
		 $data = array(

	        	
		    	'select_embarkasi'=>$this->_select_embarkasi(),

	       		 );

	    $this->template->load('template/template', $this->folder.'/registrasi_jamaahpromo',$data);	
		
	}

	public function get_registrasi(){
	  $id_booking = $this->uri->segment(3);
	  $category = $this->uri->segment(4);
	  // $tempjmljamaah = $this->input->post('tempjmljamaah');
	  // redirect('registrasi_jamaah/detail/'.$id_booking.'');
				  $sql = "SELECT a.id_booking as id_booking,a.tempjmljamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking) as jumlah_jamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking and registrasi_jamaah.ket_keberangkatan='8') as jml_keluarga 
				         from booking as a where id_booking = '".$id_booking."'";
		 	$query = $this->db->query($sql)->result_array();
	     	 foreach($query as $key=>$value){
	         $jumlah_jamaah = $value['jumlah_jamaah'];
	         $jml_keluarga = $value['jml_keluarga'];
	         $booking = $value['id_booking'];
	         $tempjmljamaah = $value['tempjmljamaah'];
			if ($jumlah_jamaah < $tempjmljamaah){
				$this->session->set_flashdata('warning', "Data jamaah yang anda input = '".$jumlah_jamaah."' tidak sesuai dengan banyak jamaah yang anda input di awal sebanyak = '".$tempjmljamaah."'");
            	redirect('registrasi_jamaahpromo/detail/'.$id_booking.'');
       		}else{
				
					
					// redirect('registrasi_jamaah/report_registrasi/'.$id_booking.'');
       			if ($jml_keluarga > 0){
					redirect('registrasi_jamaahpromo/setting_keluarga/'.$id_booking.'');
				}elseif ($jumlah_jamaah > 1) {
					redirect('registrasi_jamaahpromo/room_question/'.$id_booking.'/'.$category.'');
				}elseif($jumlah_jamaah == 1){
					redirect('registrasi_jamaahpromo/report_registrasi/'.$id_booking.'');
				}
			}
	     }
	    
		
	}



	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('view_refund')->like('pemberangkatan',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->pemberangkatan,
				'refund'	=>$row->refund
				

			);
		}
		
		echo json_encode($arr);
	}

	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($datepicker_tahun_keberangkatan)){
                 $this->registrasijamaahpromo_model->searchItem($datepicker_tahun_keberangkatan,$datepicker_keberangkatan,$departure);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

 //    public function get_data(){
	    	    
	//     $limit = $this->config->item('limit');
	//     $offset= $this->uri->segment(3,0);
	//     $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	//     $data  = $this->registrasijamaahpromo_model->get_data($offset,$limit,$q);
	//     $rows  = $paging = '';
	//     $total = $data['total'];
	    
	//     if($data['data']){
	        
	//         $i= $offset+1;
	//         $j= 1;
	//         foreach($data['data'] as $r){
	            
	//             $rows .='<tr>';
	                
	//                 $rows .='<td>'.$i.'</td>';
	//                 $rows .='<td width="10%">'.$r->invoice.'</td>';
	//                 $rows .='<td width="10%">'.$r->affiliate.'</td>';
	//                 $rows .='<td width="20%">'.$r->paket.' '.$r->category.'<br>
	//                 			Waktu Tunggu : '.$r->bulan_menunggu.' Bulan
	//                 		</td>';
	//                 $rows .='<td width="10%">'.$r->departure.'</td>';
	//                 $rows .='<td width="10%">'.$r->tgl_daftar.'</td>';
	//                 $rows .='<td width="10%">'.$r->tgl_keberangkatan.'</td>';
	//                 $rows .='<td width="10%">'.$r->jumlah_jamaah.'</td>';
	//                 $rows .='<td width="10%">'.$r->user_create.'</td>';
	//                 $rows .='<td width="20%" align="center">';
	                
	               
	//                 if($r->jumlah_jamaah ==1){
	//                 	$rows .='<a class="btn btn-sm btn-primary"  title="cetak" target="_blank" href="'.base_url().'registrasi_jamaah/cetak_registrasijamaah/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak Faktur PerJamaah</a> ';
	//                 }else{
	//                 	 $rows .='<a class="btn btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'registrasi_jamaah/cetak_registrasi/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak Faktur</a> ';
	//                  $rows .='<a class="btn btn-sm btn-primary"  title="cetak" target="_blank" href="'.base_url().'registrasi_jamaah/cetak_registrasijamaah/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak Faktur PerJamaah</a> ';
	               
	//                 }
	                 
	//                $rows .='</td>';
	            
	//             $rows .='</tr>';
	            
	//             ++$i;
	//             ++$j;
	//         }
	        
	//         $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
 //            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	//     }else{
	        
	//         $rows .='<tr>';
	//             $rows .='<td colspan="6">No Data</td>';
	//         $rows .='</tr>';
	        
	//     }
	    
	//     echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	// }

	// private function _paging($total,$limit){
	
	//     $config = array(
                
 //            'base_url'  => base_url().'registrasi_jamaah/get_data/',
 //            'total_rows'=> $total, 
 //            'per_page'  => $limit,
	// 		'uri_segment'=> 3
        
 //        );
 //        $this->pagination->initialize($config); 

 //        return $this->pagination->create_links();
	// }


	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	private function _select_status_fee(){
	    
	    return $this->db->get('status_claim_fee')->result();
	}
	private function _select_affiliate_type(){
	
	    return $this->db->get('affiliate_type')->result();
	}

    private function _select_embarkasi(){
	 	 $embarkasi = array('1','3');
		$this->db->where_in('id_embarkasi', $embarkasi);
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_keluarga($id_booking=''){

		$this->db->where_in('id_booking', $id_booking);
	    return $this->db->get('registrasi_jamaah')->result();
		
		// $id_booking = $this->input->post('id_booking');
// $id_booking = $this->uri->segment(3);
// 	     $query = $this->db->query("SELECT * FROM registrasi_jamaah where id_booking ='".$id_booking."'");
//         return $query->result();
	}
	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}
	

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3','5');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		$this->db->order_by("nama", "asc");
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	
	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->order_by("nama", "asc")->get_where(
		    	'affiliate',array(
		    		'id_affiliate_type'=>$id_affiliate_type,
		    		'status'=>1
		    		)

		    	);
		    $this->db->order_by("nama", "asc");
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama." ".$value->id_user."</option>";
		    }
		    echo $data;
	}

	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	
	function add_ajax_kab_manasik($id_prov){
		    $query = $this->db->get_where('view_manasik',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->kabupaten."</option>";
		    }
		    echo $data;
	}
	
	function add_ajax_kec_manasik($id_kab){
	    $query = $this->db->get_where('view_manasik',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->kecamatan."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des_manasik($id_kec){
	    $query = $this->db->get_where('view_manasik',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->kecamatan."</option>";
	    }
	    echo $data;
	}

	private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}
	public function save(){
		$jumlah_jamaah = $this->input->post('jumlah_jamaah');
        $seats = $this->input->post('seats');

        
        if ($seats < $jumlah_jamaah){
            $this->session->set_flashdata('Warning', "Jumlah Jamaah yang anda masukkan melebihi Seats yang Tersedia..Pilih Jadwal lagi, dengan jumlah seats yang anda ingginkan!!");
            
            	redirect('registrasi_jamaahpromo/index');
        }else{ 
			$data = $this->input->post(null,true);
	    	 $send = $this->registrasijamaahpromo_model->save($data);
	    
	    }
	

       // 		$data = $this->input->post(null,true);
	    	 // $send = $this->registrasijamaahpromo_model->save($data);
	}

	public function save_jammaah(){

		$id_booking = $this->input->post('id_booking');
      $tempjmljamaah = $this->input->post('tempjmljamaah');
      $select_status_visa = $this->input->post('select_status_visa');
    	 $tanggal_visa = $this->input->post('tanggal_visa');
    	 // $category = $this->uri->segment(4);
    	 $category = $this->input->post('category');
	 //  $sql = "SELECT a.id_booking as id_booking,
		//          (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking) as jumlah_jamaah 
		//          from booking as a where id_booking = '".$id_booking."'";
 	// $query = $this->db->query($sql)->result_array();
    	 $stored_procedure = "call cek_jumlah_jamaah(?)";
         $query  = $this->db->query($stored_procedure,array('id_booking'=>$id_booking
            ))->result_array();  

 	 foreach($query as $key=>$value){
     $jumlah_jamaah = $value['jumlah_jamaah'];
     $booking = $value['id_booking'];
	}

	  $this->form_validation->set_rules('nama','nama','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            $no_identitas=$this->input->post('no_identitas'); // mendapatkan input dari kode
            $cek=$this->registrasijamaahpromo_model->cek($no_identitas); // cek kode di database
            if($cek->num_rows()>0){ // jika kode sudah ada, maka tampilkan pesan
                 
               // echo'<div class="alert alert-dismissable alert-danger"><small>No Identitas sudah digunakan</small></div>';
            	$this->session->set_flashdata('info', "No Identitas sudah digunakan");
            	 $id_booking = $this->input->post('id_booking');
            	redirect('registrasi_jamaahpromo/detail/'.$id_booking.'');
                  
            }elseif ($jumlah_jamaah >= $tempjmljamaah) {
            	$this->session->set_flashdata('info', "Data Yang anda Input melebihi jumlah jamaah yg anda masukkan diregistrasi jamaah");
            	redirect('registrasi_jamaahpromo/detail/'.$id_booking.'');

            }
            else{
			 
			  $data = $this->input->post(null,true);
			  $send = $this->registrasijamaahpromo_model->save_registrasi($data);


			 $stored_procedure = "call cek_ket_keberangkatan(?)";
             $query  = $this->db->query($stored_procedure,array('id_booking'=>$id_booking
            ))->result_array();  

	     	 foreach($query as $key=>$value){
	         $jumlah_jamaah = $value['jumlah_jamaah'];
	         $jml_keluarga = $value['jml_keluarga'];
	         $booking = $value['id_booking'];

			if ($jumlah_jamaah == $tempjmljamaah){
           
				if ($jml_keluarga > 0){
					redirect('registrasi_jamaahpromo/setting_keluarga/'.$id_booking.'');
				}elseif ($jumlah_jamaah > 1) {
					redirect('registrasi_jamaahpromo/room_question/'.$id_booking.'/'.$category.'');
				}elseif ($jumlah_jamaah == 1){
					redirect('registrasi_jamaahpromo/report_registrasi/'.$id_booking.'');
				}

            	
       		}else{
				
					$this->session->set_flashdata('success', "Transaction Successfull");
					redirect('registrasi_jamaahpromo/detail/'.$id_booking.'');
			}
	     }


		}

	}
}

	public function add(){   
	     if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'add'))
		    $this->general->no_access();
	   		$data = array('select_product'=>$this->_select_product());
       	 $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function schedule(){
	     
	     if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'edit'))
		    $this->general->no_access();
	    
	     
	    $id = $this->uri->segment(3);
	    // $get = $this->db->get_where('view_schedule',array('id_schedule'=>$id))->row_array();
	    $stored_procedure = "CALL view_schedule_promo(?) ";
	    $get = $this->db->query($stored_procedure,array('id'=>$id))->row_array();
	    if(!$get)
	        show_404();
	         $data = array(
	         	// 'select_affiliate'=>$this->_select_affiliate(),
	         	// 'select_affiliate_type'=>$this->_select_affiliate_type(),
	         	// // 'list'=>$this->registrasijamaah_model->getaffiliatetype(),
	         	
	  		);
	      $result['list']=$this->registrasijamaahpromo_model->getaffiliatetype();
        $this->template->load('template/template', $this->folder.'/edit_jamaah',array_merge($get,$data,$result));
	}



	
	 public function detail(){
	


	    if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'view'))
		    $this->general->no_access();
	    $id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->registrasijamaahpromo_model->get_pic_booking($id_booking,$id);
	    $pic    = array();
	    $pic_booking= array();
	    // if(!$booking){
	    //     show_404();
	    // }
	    // else{
	        
	        // $pic_booking = $this->registrasijamaahpromo_model->get_pic_booking($id_booking);
	        $pic = $this->registrasijamaahpromo_model->get_pic($id_booking);
	    // }    

	      
	    $data = array(

	        	'family_relation'=>$this->_family_relation(),
				'select_product'=>$this->_select_product(),
				'select_merchandise'=>$this->_select_merchandise(),
				'select_rentangumur'=>$this->_select_rentangumur(),
				'select_status_hubungan'=>$this->_select_status_hubungan(),
				'select_kelamin'=>$this->_select_kelamin(),
				'select_statuskawin'=>$this->_select_statuskawin(),
		    	'provinsi'=>$this->registrasijamaahpromo_model->get_all_provinsi(),
		    	'kabupaten'=>$this->registrasijamaahpromo_model->get_all_kabupaten(),
		    	'select_embarkasi'=>$this->_select_embarkasi(),

		    	'id_bandara' => $this->registrasijamaahpromo_model->get_data_bandara(),
		    	// 'select_keluarga'=>$this->_select_keluarga($id_booking),
		    	// 'select_affiliate'=>$this->_select_affiliate(),
	  			 'select_pemberangkatan'=>$this->_select_pemberangkatan(),
	  			 'select_type'=>$this->_select_type(),
	  			 'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic,
	       		 //'provinsi_manasik'=>$this->registrasijamaahpromo_model->get_all_provinsi_manasik(),
	       		 'select_status_visa'=>$this->_select_status_visa(),
	       		 );
	 
	    $this->template->load('template/template', $this->folder.'/data_jamaah',($data));

	}
	

	public function data_jamaah(){
	     

	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('view_booking',array('id_booking'=>$id))->row_array();
	    if(!$get)
	        show_404();

	        	$this->data['family_relation']=$this->_family_relation();
				$this->data['select_product']=$this->_select_product();
				$this->data['select_merchandise']=$this->_select_merchandise();
				$this->data['select_rentangumur']=$this->_select_rentangumur();
				$this->data['select_status_hubungan']=$this->_select_status_hubungan();
				$this->data['select_kelamin']=$this->_select_kelamin();
				$this->data['select_statuskawin']=$this->_select_statuskawin();
		    	$this->data['provinsi']=$this->registrasijamaahpromo_model->get_all_provinsi();
		    	$this->data['kabupaten']=$this->registrasijamaahpromo_model->get_all_kabupaten();
		    	$this->data['select_embarkasi']=$this->_select_embarkasi();
		    	$this->data['select_keluarga']=$this->_select_keluarga($id);
		    	$this->data['select_affiliate']=$this->_select_affiliate();
		    	$this->data['select_pemberangkatan']=$this->_select_pemberangkatan();
	         	
				// $this->data['id_booking'] = $id_booking;

	    	$biaya_setting_ = $this->registrasijamaahpromo_model->get_key_val();
		  
			foreach ($biaya_setting_ as $key => $value){
				$out[$key] = $value;
			}

		
        $this->template->load('template/template', $this->folder.'/data_jamaah',array_merge($get,$out,$this->data));
	}
  


	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$category = $this->input->post('category');
		$refund = $this->input->post('refund');
		
		$select_pemberangkatan = $this->registrasijamaahpromo_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            

            echo $select_pemberangkatan['refund'];
           
    
        }
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->registrasijamaahpromo_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->registrasijamaahpromo_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}

	function generate_bot_jamaah(){
		for($i=0; $i< 120; $i++){
			echo $i;
			$data = array(
				'kode_unik' => random_3_digit(),
				'create_date' => date('Y-m-d H:i:s'),
			);
			echo ' : ';
			echo '<b>'.$data['kode_unik'].'</b>';
			echo '<br>';
			$this->db->insert('booking', $data);
		}
	}
	
	
public function detail_jamaahnonaktif(){
	


	    // if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'view'))
		   //  $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->registrasijamaahpromo_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->registrasijamaahpromo_model->get_pic($id_booking);

	    }    

	    $data = array(

	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahnonaktif',($data));

	}

	public function piker(){
	

		// $this->load->view('piker');
	
	     $this->template->load('template/template', $this->folder.'/piker');

	}

	public function report_registrasi(){  
		$id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $report_registrasi = $this->registrasijamaahpromo_model->get_report_registrasi($id_booking,$id);
	    if(!$report_registrasi){
	        show_404();
	    }
	   
      
	    $data = array(

	       		 'report_registrasi'=>$report_registrasi,
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_registrasi',($data));
	    // $this->load->view('bilyet_barcode',($data));
	}


	public function setting_keluarga(){
	


	    if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'view'))
		    $this->general->no_access();
	    $id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->registrasijamaahpromo_model->get_pic_booking($id_booking,$id);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaahpromo_model->get_pic_booking($id_booking);
	        $pic = $this->registrasijamaahpromo_model->get_pic_keluarga($id_booking,$id);
	    }    

	      
	    $data = array(


		    	'select_keluarga'=>$this->_select_keluarga($id_booking),
		    	'family_relation'=>$this->_family_relation(),
	       		 'pic'=>$pic,
	       		 'booking'=>$booking,
	       	
	       		 );
	 
	    $this->template->load('template/template', $this->folder.'/setting_keluarga',($data));

	}

	public function update_keluarga(){
		$id_booking = $this->input->post('id_booking');
	    $data = $this->input->post(null,true);
	    $data2 = $this->input->post('id_jamaah');
	    $category = $this->input->post('category');
	    
	  // print_r($data);
	    if($this->registrasijamaahpromo_model->update_setting_keluarga($data)){
	        
	        // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
	      	redirect('registrasi_jamaahpromo/room_question/'.$id_booking.'/'.$category.'');  
	    }else{
	        
	        show_error("Error occured, please try again");
	    }

	    
	}


	public function setting_room_type(){
	


	    if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'view'))
		    $this->general->no_access();
	    $id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->registrasijamaahpromo_model->get_pic_booking_room($id_booking,$id);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaahpromo_model->get_pic_booking($id_booking);
	        $pic = $this->registrasijamaahpromo_model->get_pic_room_type($id_booking);
	    }    

	      
	    $data = array(



		    	'select_type'=>$this->_select_type(),
	       		 'pic'=>$pic,
	       		 'booking'=>$booking,
	       		'select_jamaah'=>$this->_select_jamaah($id_booking),
	       		// 'select_keluarga'=>$this->_select_keluarga(),
	       		 );
	 
	    $this->template->load('template/template', $this->folder.'/setting_room',($data));

	}

	public function room_question(){
	


	    if(!$this->general->privilege_check(REGISTRASI_JAMAAH_PROMO,'view'))
		    $this->general->no_access();
	    $id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $category = $this->uri->segment(4);
	    $booking = $this->registrasijamaahpromo_model->get_pic_order_view($id_booking,$id);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	    	// $category = $this->input->post('category');
	        $pic = $this->registrasijamaahpromo_model->get_pic_room_price($category);
	    }    

	      
	    $data = array(



		    	// 'select_type'=>$this->_select_type(),
	       		 'pic'=>$pic,
	       		 'booking'=>$booking,
	       	
	       		 );
	 
	    $this->template->load('template/template', $this->folder.'/room_question',($data));

	}
	public function update_room(){
	// $id_booking = $this->uri->segment(3);

	//  $stored_procedure = "call cek_room(?)";
 //     $query  = $this->db->query($stored_procedure,array('id_booking'=>$id_booking
 //     ))->result_array();  
 // 	 foreach($query as $key=>$value){
 //   		 $double_registrasi = $value['double'];
 //     	 $triple_registrasi = $value['triple'];
	// }

	//  $stored_procedure = "call cek_room_order(?)";
 //     $query  = $this->db->query($stored_procedure,array('id_booking'=>$id_booking
 //     ))->result_array();  
 // 	 foreach($query as $key=>$value){
 //   		 $double_room_order = $value['double'];
 //     	 $triple_room_order = $value['triple'];
	// }


 $this->form_validation->set_rules('id_booking','id_booking','required|trim');
	   if($this->form_validation->run()==true){
           $id_booking = $this->input->post('id_booking');
            $cek_room=$this->registrasijamaahpromo_model->cek_room($id_booking); 
            if($cek_room->num_rows()>0){ 
            	redirect('registrasi_jamaahpromo/setting_room_type/'.$id_booking.'');
            }else{
            	  $id_booking = $this->input->post('id_booking');
	    	 $data = $this->input->post(null,true);
	    	 $send = $this->registrasijamaahpromo_model->update_room_order($data);
	   		if($send)
			redirect('registrasi_jamaahpromo/setting_room_type/'.$id_booking.'');
            }

         }
      
}
	

	public function update_room_setting(){


		$id_booking = $this->input->post('id_booking');
	    $data = $this->input->post(null,true);
	    $data2 = $this->input->post('select_jamaah');
	    

	    $send = $this->registrasijamaahpromo_model->update_room_setting($data);
 	if($send)
		redirect('registrasi_jamaahpromo/report_registrasi/'.$id_booking.'');
	   

	   
	}

	private function _select_jamaah($id_booking=''){

		$this->db->where_in('id_booking', $id_booking);
	    return $this->db->get('registrasi_jamaah')->result();
		
	}
}
