<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
       <!-- <form  class="form-horizontal" id="Add_transaction" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>registrasi_jamaah/registrasi_jamaah/save_jammaahkeluarga"> -->
        <form action="<?php echo base_url();?>registrasi_jamaahpromo/save_jammaah" id="Add_transaction" class="form-horizontal" role="form" method="post" >
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Booking</b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                    <input type="hidden"  name="category" value="<?php echo $booking['category']?>" >
                    <!-- <input type="hidden"  name="status_claim_fee" value="<?php echo $booking['status_claim_fee']?>" > -->
                    <input type="hidden"  name="id_affiliate_type" value="<?php echo $booking['id_affiliate_type']?>" >
                    <input type="hidden"  name="tempjmljamaah" value="<?php echo $booking['tempjmljamaah']?>" >
                    <input type="hidden"  name="cashback" value="<?php echo $booking['cashback']?>" >
                    <input type="hidden"  name="status_fee" value="<?php echo $booking['status_fee']?>" >
                    
                    <input type="hidden" name="schedule" value="<?php echo $booking['schedule']?>">
                    <input type="hidden"  name="embarkasi" value="<?php echo $booking['embarkasi']?>" >
                      <?php $tiga_digit = random_3_digit_perjamaah(); ?>
                        <input type='hidden' name='unique_digit' value='<?= $tiga_digit; ?>'>

                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="<?php echo $booking['invoice']?>" readonly="readonly">

                      </div>

                      <div class="col-sm-4">
                        <label>Affiliate</label> 
                        <input type="text" name="affiliate" class="form-control" value="<?php echo $booking['affiliate']?>" readonly="readonly" >
                            <input type="hidden" name="id_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?>" readonly="readonly" >
                            <input type="hidden" name="id_product" class="form-control" value="<?php echo $booking['id_product']?>" readonly="readonly" >
                      </div>

                     

                       <div class="col-sm-4">
                        <label>Total Seats</label> 
                        
                        <input name="seats" class="form-control" value="<?php echo $booking['seats']?>" readonly="readonly">
                      </div>
                    <!--   <div class="col-sm-4">
                      
                        <input name="status_keberangkatan" class="form-control" value="<?php echo $booking['status_keberangkatan']?>" readonly="readonly">
                      </div> -->
                    <?php if ($booking['status_keberangkatan'] == 6){ ?>

                      <div class="col-sm-4">
                        <label>Tanggal </label>

                        <input name="tgl_Keberangkatan" class="form-control" value="<?php echo $booking['tgl_keberangkatan']?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                      <label>Jam </label>
                        <input name="jam_Keberangkatan" class="form-control" value="<?php echo $booking['jam_keberangkatan'];?>" readonly="readonly">
                      </div>
                  <?php }else{ ?>
                    <div class="col-sm-4" >  <!-- style="visibility:hidden"> -->
                        <label >Bulan </label>

                        <input  type="hidden" name="tgl_Keberangkatan" class="form-control" value="<?php echo $booking['tgl_keberangkatan'];?>" readonly="readonly">
                         <input  name="BulanKeberangkatan" class="form-control" value="<?php echo $booking['BulanKeberangkatan'];?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4" ><!-- style="visibility:hidden"> -->
                      <label>Tahun </label>
                        <input type="hidden" name="jam_Keberangkatan" class="form-control" value="<?php echo $booking['jam_keberangkatan'];?>" readonly="readonly">
                        <input  name="TahunKeberangkatan" class="form-control" value="<?php echo  $booking['TahunKeberangkatan'];?>" readonly="readonly">

                      </div>
                 <?php } ?> 
                      <div class="col-sm-4">
                        <label>Harga</label> 
                        <!-- <input type="text" name="harga" class="form-control" value="<?php echo $harga;?>" readonly="readonly" > -->
                        <!-- <?php $tiga_digit = random_3_digit(); ?> -->
                          <div class="input-group">
                              <div class="input-group-addon">Rp</div>
                              <input type="text" class="form-control"  name="harga" id="harga" value="<?php echo $booking['harga'];?>"  readonly="readonly">
                              <!-- <input type="text" class="form-control"  name="harga" id="harga" value="<?php echo number_format($harga + $tiga_digit);?>"  readonly="readonly"> -->
                              <div class="input-group-addon">.00</div>
                            </div>
                        </div>

                        <input type='hidden' name='unique_digit' value='<?= $tiga_digit; ?>'>
                        <?php 
                          $biaya_setting_ = $this->registrasijamaahpromo_model->get_key_val();
                            foreach ($biaya_setting_ as $key => $value){
                              $out[$key] = $value;
                            }
                        ?>
                         <div class="col-sm-4">
                            
                            <input type="hidden" class="form-control" name="pria" value="<?php echo $out['Pria'];?>" >
                        </div>
                        
                         <div class="col-sm-4">
                            
                            <input type="hidden" class="form-control" name="Wanita" value="<?php echo $out['Wanita'];?>" >
                        </div>

                        <div class="col-sm-4">
                            <label>Handling</label>
                           
                            <div class="input-group">
                              <div class="input-group-addon">Rp</div>
                              <input type="text" class="form-control"  name="handling" id="handling" value="<?php echo $out['handling'];?>"  readonly="readonly">
                              <div class="input-group-addon">.00</div>
                            </div>
                        </div>
                         <?php
                           $stored_procedure = "CALL all_refrensi_fee(?)";
                            $query =  $this->db->query($stored_procedure,array('id_user'=>$booking['id_user_affiliate']))->result_array();

                            // $sql = "SELECT *
                            // FROM view_all_refrensi_fee 
                            // WHERE id_user = '".$booking['id_user_affiliate']."'";
                            // $query = $this->db->query($sql)->result_array();

                          foreach($query as $key=>$value){
                           // $sponsor = $value['id_refrensi'];
                             // $fee_sponsor_jamaah_kuota  = $value['fee_sponsor_jamaah_kuota'];

                            }

                        ?>

                        <!-- <input type="hidden"class="form-control" name="fee_promo" value="<?php echo $out['FEE_PROMO_1'];?>"> -->
                       <?php if($booking['status_fee']==4){ ?>
   
                        <div class="col-sm-4">
                            <input type="hidden"class="form-control" name="fee_posting_paketpromo" value="<?php echo $value['fee_posting_paketpromo'] ;?>">
                            <!-- <input type="hidden"class="form-control" name="FEE_PROMO_1" value="<?php echo $out['FEE_PROMO_1'];?>"> -->
                        </div>
                        <?php }else{?>
                            
                        <div class="col-sm-4">
                            
                            <input type="hidden"class="form-control" name="fee_posting_paketpromo" value="0">
                        </div>
                       <?php } ?>

                       
                         <div class="col-sm-4">
                            
                            <input type="hidden"class="form-control" name="id_refrensi" value="<?php echo $value['id_refrensi'] ;?>">
                        </div>
                         <div class="col-sm-4">
                            <input type="hidden"class="form-control" name="fee_posting_jamaahpaketpromo" value="<?php echo $value['fee_posting_paketpromo'] ;?>">
                        </div>
                        <div class="col-sm-4">
                            
                            <input type="hidden"class="form-control" name="fee_sponsor_paketpromo" value="<?php echo $value['fee_sponsor_paketpromo'] ;?>">
                        </div>
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Client</b>
                </div>
                <?php if($this->session->flashdata('info')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                                  </div>
                              <?php } ?>

                     <?php if($this->session->flashdata('warning')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('warning'); ?>  
                                  </div>
                              <?php } ?>

                <?php if($this->session->flashdata('success')) { ?>
                                  <div class="alert alert-success">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('success'); ?>  
                                  </div>
                  <?php } ?>
                 <div class="panel-body">
                     <div class="form-group input-group col-lg-8">
                      <span>
                      <button data-toggle="modal" data-target="#md-fullWidth" type="button" class="btn btn-primary btn-flat"> Create new entry</button>
                      </span>

                          <span>
                             <?php
                                echo '
                                
                                <a href="'.site_url('registrasi_jamaahpromo').'/get_registrasi/' . $booking['id_booking'].'/'. $booking['category']. '"  title="Cetak Detail" class="btn btn-sm btn-success" >  Jika Sudah Entry data Jamaah Lanjut ke step berikutnya</a>';
                              ?>
                          </span>

                      </div>
                          <div class="table-responsive">
                               <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    
                                    <th>Nama </th>
                                    <th>No Identitas </th>
                                   <th>Telp </th>
                                   <th>Status </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      
                                      <!-- <td><?php echo $pi['id_jamaah']?>  </td> -->
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['no_identitas']?>  </td>
                                      <td><?php echo $pi['telp']?>  </td>
                                      <td><?php echo $pi['status']?>  </td>
                                  </tr>
                                  <?php } ?> 
                            
                            </tbody>
                     </table>
                   </div>
                  
                     <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div> 
              </div>
          </div>
      </div>
    </div>

 <!-- -->
<!-- NEW MODAL -->

    <div class="modal fade " id="md-fullWidth" tabindex="-1" role="dialog">
       <div class="modal-dialog full-width">
          <div class="modal-content">
          <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
        <div class="modal-body">
          <div class="col-sm-6 col-md-6">
              <div class="block-flat">
                <div class="header">              
                  <h4>DATA JAMAAH SESUAI di KTP </h4>
                </div>
                <div class="content">
                   <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">NAMA</label>
                             <div class="col-sm-10">
                                  <input name="nama" type="text" class="form-control" id="nama" placeholder="Nama Jamaah" required/>
                              </div>
                        </div>

                        <div class="form-group">
                           <label for="inputPassword3" class="col-sm-2 control-label">Tempat Lahir</label>
                               <div class="col-sm-10">
                                  <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat / kota Kelahiaran " required/>
                               </div>
                        </div>

                        <div class="form-group">
                             <label for="inputPassword3" class="col-sm-2 control-label">Tanggal Lahir</label>
                              <div class="col-sm-10">
                                <div class='input-group date' id='datepicker1'>
                                 <input type="text" data-mask="date" class="form-control  " name="tanggal_lahir" placeholder="YYYY-MM-DD " required/>
                                   <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar">
                                      </span>
                                  </span>
                                 </div> 
                              </div>
                        </div>
                   <!--       <div class="form-group">
            <div class='input-group date' id='datepicker1'>
                <input type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
            </div>
        </div> -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                  for="inputPassword3" >Jenis Kelamin</label>
                              <div class="col-sm-10">
                                  <select required class="form-control" id="jenis_kelamin" name="select_kelamin">
                                  <option value="">- PILIH JENIS KELAMIN -</option>
                                    <?php foreach($select_kelamin as $sk){?>
                                        <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
                                    <?php } ?>
                                </select>
                              </div>
                       </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >No KTP</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control"
                                id="no_identitas" name="no_identitas" placeholder="KTP,Dll" required/>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Provinsi</label>
                        <div class="col-sm-10">
                            <select required name="provinsi" class="select_prov form-control " id="provinsi">
                                <option value=''>Select Provinsi</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Kabupaten</label>
                        <div class="col-sm-10">
                            <select required name="kabupaten" class="select_kap form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Kecamatan</label>
                        <div class="col-sm-10">
                             <select required name="kecamatan" class="select_kec form-control" id="kecamatan">
                                  <option value=''>Select Kecamatan</option>
                              </select>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Alamat</label>
                        <div class="col-sm-10">
                            <textarea id="alamat" type="text" class="form-control" name="alamat" placeholder="Input Alamat" required ></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                           <label class="col-sm-2 control-label" >Status Perkawinan</label>
                           <div class="col-sm-10">
                          <select required class="select_status form-control" name="select_statuskawin">
                            <option value="">- PILIH STATUS -</option>
                              <?php foreach($select_statuskawin as $sk){?>
                                  <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Telp</label>
                        <div class="col-sm-10">
                            <input id="telp" name="telp" type="type" class="input_telp form-control" onKeyPress="return numbersonly(this, event)" placeholder="Input No. Telp (harus karakter angka)" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Telp 2</label>
                        <div class="col-sm-10">
                            <input id="telp" name="telp_2" type="type" class="input_telp form-control" onKeyPress="return numbersonly(this, event)" placeholder="No. Telp Orang Serumah dll (harus karakter angka)" required>
                        </div>
                      </div>
                        <div class="form-group">
                           <label class=" col-sm-2 control-label" >Rentang Umur</label>
                          <div class="col-sm-10">
                            <select required class="form-control" name="rentang_umur">
                              <option value="">- PILIH RENTANG UMUR -</option>
                                <?php foreach($select_rentangumur as $sk){?>
                                    <option value="<?php echo $sk->id_rentang_umur;?>"><?php echo $sk->keterangan;?></option>
                                <?php } ?>
                            </select>
                          </div>
                        </div>
                     
                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Email</label>
                        <div class="col-sm-10">
                            <input id="email" name="email" type="email" class="input_telp form-control" placeholder="Input Email">
                        </div>
                      </div>
                  </div>
                </div>        
              </div>

           
              <div class="col-sm-6 col-md-6">
                <div class="block-flat">
                  <div class="header">              
                    <h4>DATA MANIFEST<h4>
                  </div>
                   <div class="content">
                    <div class="form-group">
                    <label class="col-lg-4 control-label">Berangkat dengan : </label>
                    <div class="col-lg-8">
                     <select required class="form-control" name="select_status_hubungan" id="select_status_hubungan" >
                      <option value="">- PILIH BERANGKAT DENGAN -</option>
                        <?php foreach($select_status_hubungan as $st){?>
                            <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                      <div class="form-group">
                        <label class="col-sm-3 control-label">Berangkat Dari</label>
                        <div class="col-sm-9">
                            <select required name="pemberangkatan" class="form-control chosen-select" id="pemberangkatan">
                            <option value=""></option>
                            <?php foreach($select_pemberangkatan as $select_pemberangkatan):?>
                            <option value="<?php echo $select_pemberangkatan->pemberangkatan;?> "><?php echo $select_pemberangkatan->pemberangkatan;?></option>
                            <?php endforeach;?>
                        </select>
                                 <input  type="hidden" class="form-control" id="id_bandara" name="id_bandara"  readonly="true" />
                                  <input type="hidden" class="form-control"  name="refund" value='0' readonly="true" />
                                  <input type="hidden" class="form-control" id="akomodasi" name="akomodasi"  readonly="true" />

                        </div> 
                      </div>
                       <!--  <div class="form-group">
                        <label class="col-sm-4 control-label">Room Type</label>
                        <div class="col-sm-8">
                       <select required class="form-control" name="select_type">
                         <option  value="">- PILIH ROOM TYPE -</option>
                          <?php foreach($select_type as $st){?>
                              <option value="<?php echo $st->id_room_type;?>"><?php echo $st->type;?></option>
                          <?php } ?>
                      </select>
                      </div>
                      </div> -->

                  <div class="form-group">
                      <label class="col-sm-4 control-label"
                            for="inputPassword3" >Ahli Waris</label>
                      <div class="col-sm-8">
                          <input id="waris" name="waris" type="text" class="input_telp form-control" placeholder="Input Ahli Waris" required>
                      </div>
                  </div>

                  <div class="form-group">
                         <label class="col-sm-4 control-label" >Hubungan Ahli waris </label>
                         <div class="col-sm-8">
                        <select required class="form-control" name="select_hub_ahliwaris">
                          <option  value="">- PILIH Hubungan Ahli Waris -</option>
                            <?php foreach($select_hub_ahliwaris as $sk){?>
                                <option value="<?php echo $sk->id_hubwaris;?>"><?php echo $sk->hubungan;?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>

                       <div class="form-group">
                         <label class="col-sm-4 control-label" for ="select_merchandise">Merchandise </label>
                         <div class="col-sm-8">
                        <select required class="select_merchandise form-control" name="select_merchandise">
                          <option  value="">- PILIH MERCHANDISE -</option>
                            <?php foreach($select_merchandise as $sk){?>
                                <option value="<?php echo $sk->id_merchandise;?>"><?php echo $sk->merchandise;?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="keluarga" >No Pasport</label>
                        <div class="col-sm-8">
                          <input name="no_pasport" type="text" id="no_pasport"class="input_keluarga form-control" placeholder="Input No Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="keluarga" >issue office</label>
                        <div class="col-sm-8">
                          <input name="issue_office" type="text" id="issue_office"class="input_keluarga form-control" placeholder="Input Issue Office">
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"for="inputPassword3" >Isue Date</label>
                    <div class="col-sm-8">
                    
                         <input type="text" class="form-control" data-mask="date" name="isue_date" id="datepicker2"   placeholder="YYYY-MM-DD" />
                
                    </div>
                  </div>

                                       <div class="form-group">
                        <label  class="col-lg-5 control-label">FotoCopy Kartu Identitas</label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_identitas" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_identitas" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-lg-5 control-label">FotoCopy Kartu Keluarga </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_kk" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_kk" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                     <div class="form-group">
                        <label  class="col-lg-5 control-label">FotoCopy Pas Photo </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_photo" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_photo" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                      <div class="form-group">
                        <label  class="col-lg-5 control-label">FotoCopy Pasport </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_pasport" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_pasport" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-lg-5 control-label">FotoCopy Vaksin </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_vaksin" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_vaksin" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                       <div class="form-group">
                        <label  class="col-lg-5 control-label">FotoCopy Buku Nikah </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_buku_nikah" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_buku_nikah" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                 <!--    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/> -->
                  </div>
                 </div>
                 </div>




            

<!--               <div class="col-sm-6 col-md-6">
                <div class="block-flat">
                  <div class="header">              
                    <h4>DATA DOMISILI JAMAAH</h4>
                  </div>
                   <div class="content">
                        <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Provinsi</label>
                        <div class="col-sm-10">
                            <select required name="provinsi_domisili" class="select_prov form-control " id="provinsi_domisili">
                                <option value=''>Select Provinsi</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Kabupaten</label>
                        <div class="col-sm-10">
                            <select required name="kabupaten_domisili" class="select_kap form-control" id="kabupaten_domisili">
                                <option value=''>Select Kabupaten</option>
                            </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Kecamatan</label>
                        <div class="col-sm-10">
                             <select required name="kecamatan_domisili" class="select_kec form-control" id="kecamatan_domisili">
                                  <option value=''>Select Kecamatan</option>
                              </select>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Alamat</label>
                        <div class="col-sm-10">
                            <textarea id="alamat_domisili" type="text" class="form-control" name="alamat_domisili" placeholder="JIKA ALAMAT SAMA DENGAN DATA KTP DI SAMAKAN sAJA" required ></textarea>
                        </div>
                      </div> 

                  </div>
      
               </div>        
            </div>  -->

              <div class="col-sm-6 col-md-6">
                <div class="block-flat">
                  <div class="header">              
                    <h4><strong>VISA</strong> APAKAH JAMAAH INI PERNAH BERANGKAN UMBROH SEBELUMNYA ?</h4>
                  </div>
                   <div class="content">
                     <div class="form-group">
                        <div class="col-sm-12">  
                            <select required class="form-control" name="select_status_visa" id="select_status_visa" >
                              <option value="">- PILIH -</option>
                                <?php foreach($select_status_visa as $sk){?>
                                    <option value="<?php echo $sk->id;?>"><?php echo $sk->ket;?></option>
                                <?php } ?>
                            </select>
                        </div>
                      </div>
              <div id="hidden_div" style="display: none;">

                               <div class="col-sm-10">
                                <div class='input-group date' id='tanggal_visa'>
                                 <input type="text" data-mask="date" class="form-control  " name="tanggal_visa" id="tanggal_visa" placeholder="YYYY-MM-DD " />
                                   <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar">
                                      </span>
                                  </span>
                                 </div> 
                              </div>
                </div>    
    
              </div>
            </div>
           </div> 
                
          
            
                  <div class="text-center">
                  <div class="i-circle primary"><i class="fa fa-check"></i></div>
                  <h4>Pastikan!</h4>
                  <p>Data Jamaah Yang Anda Input BENAR</p>

                </div> 
              
            </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                  </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
     
            <!--END MODAL -->



        

  </form>       
                
</div>



<script type="text/javascript">
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });
</script>

<script type="text/javascript">
        $(document).ready(function(){
            $("#provinsi_domisili").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten_domisili').load(url);
                return false;
            })
            
            $("#kabupaten_domisili").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan_domisili').load(url);
                return false;
            })
            
            $("#kecamatan_domisili").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });
</script>

<script type="text/javascript">
        $(document).ready(function(){
            $("#provinsi_manasik").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_kab_manasik');?>/"+$(this).val();
                $('#kabupaten_manasik').load(url);
                return false;
            })
            
            $("#kabupaten_manasik").change(function (){
                var url = "<?php echo site_url('registrasi_jamaah/add_ajax_kec_manasik');?>/"+$(this).val();
                $('#kecamatan_manasik').load(url);
                return false;
            })
            
            // $("#kecamatan_manasik").change(function (){
            //     var url = "<?php echo site_url('registrasi_jamaah/add_ajax_des_manasik');?>/"+$(this).val();
            //     $('#desa').load(url);
            //     return false;
            // })
        });
</script>
    




  <script type="text/javascript">
function numbersonly(myfield, e, dec) { var key; var keychar; if (window.event) key = window.event.keyCode; else if (e) key = e.which; else return true; keychar = String.fromCharCode(key); // control keys 
if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) return true; // numbers 
else if ((("0123456789").indexOf(keychar) > -1)) return true; // decimal point jump 
else if (dec && (keychar == ".")) { myfield.form.elements[dec].focus(); return false; } else return false; }

</script>



<script type="text/javascript">
      $(function () {
             $('#datepicker1').datetimepicker({
          format: 'YYYY-MM-DD',
          viewMode: 'years'
        });
                $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });

        $('#tanggal_visa').datetimepicker({
          format: 'YYYY-MM-DD',
          viewMode: 'years'
        });

            });
    </script>


    <script type="text/javascript">

            $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('registrasi_jamaah/get_biaya_refund_');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#refund").val(html);
                    
                }
            })
        })


            $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('registrasi_jamaah/get_biaya_refund_2');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#id_bandara").val(html);
                    
                }
            })
        })


        $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
          
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('registrasi_jamaah/get_biaya_refund_3');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#akomodasi").val(html);
                    
                }
            })
        })

    </script>
 <script type="text/javascript">
  jQuery(document).ready(function(){
  $(".chosen-select").chosen({width: "100%"}); 
});

// document.getElementById('XIyes').onchange = displayTextBox;
// document.getElementById('XIno').onchange = displayTextBox;

// var textBox = document.getElementById('muhrim');

// function displayTextBox(evt){
//     console.log(evt.target.value)
//     if(evt.target.value=="Yes"){
//         textBox.value = '<?php echo $out['MUHRIM'];?>';
//     }else{
//         textBox.value = 0;
//     }
// }



</script>


<script type="text/javascript">
      document.getElementById('select_status_visa').addEventListener('change', function () {
    var style = this.value == 1 ? 'block' : 'none';
    document.getElementById('hidden_div').style.display = style;
    // document.getElementById('hidden_div2').style.display = style;
    // $("#datepicker").prop('required',true);
     select_status_visa = $(this).val();

      console.log(select_status_visa);

      if(select_status_visa=='1' ){
        $("#tanggal_visa").prop('required',true);

      }else{
         $("#tanggal_visa").prop('required',false);
      }
    });


  </script>


</script> <script src="<?php echo base_url();?>assets/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    $("[data-mask='date']").mask("9999-99-99");
    $("[data-mask='phone']").mask("(999) 999-9999");
    $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
    $("[data-mask='phone-int']").mask("+33 999 999 999");
    $("[data-mask='taxid']").mask("99-9999999");
    $("[data-mask='ssn']").mask("999-99-9999");
    $("[data-mask='product-key']").mask("a*-999-a999");
    $("[data-mask='percent']").mask("99%");
    $("[data-mask='currency']").mask("$999,999,999.99");
  });
</script>


        
<script type="text/javascript">
    (function (global) { 

    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };          
    }

})(window);
</script>