
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style type="text/css">
td, div {
	font-family: "Arial","​Helvetica","​sans-serif";
}
.datagrid-header-row * {
	font-weight: bold;
}
.messager-window * a:focus, .messager-window * span:focus {
	color: blue;
	font-weight: bold;
}
.daterangepicker * {
	font-family: "Source Sans Pro","Arial","​Helvetica","​sans-serif";
	box-sizing: border-box;
}
.glyphicon	{font-family: "Glyphicons Halflings"}
</style>

<div class="page-head">
            <h2>Data Jamaah</h2>
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Registrasi</a></li>
              <li class="active">Data Jamaah</li>
            </ol>
        </div>      
<div id="page-wrapper">
   <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Jamaa</b>
                </div>
                <!-- /.panel-heading -->
                 <div class="panel-body">



			<!-- Data Grid -->
			<!-- <table   id="dg" 
			class="easyui-datagrid"
			title="Data Jamaah" 
			style="width:auto; height: auto;" 
			url="<?php echo site_url('registrasi_jamaah/list_jamaah/ajax_list'); ?>" 
			pagination="true" rownumbers="true" 
			fitColumns="true" singleSelect="true" collapsible="true"
			sortName="create_date" sortOrder="desc"
			toolbar="#tb"
			striped="true">
			<thead>
				<tr>
					<th data-options="field:'id_booking',halign:'center', align:'center'" hidden="true">ID</th>
					<th data-options="field:'invoice', width:'13',halign:'center', align:'center'">Kode</th>
					<th data-options="field:'paket_txt', width:'25', halign:'center', align:'center'">Tanggal Pinjam</th>
					<th data-options="field:'departure', width:'14', halign:'center', align:'center'">ID Anggota</th>
					<th data-options="field:'create_date', width:'35', halign:'center', align:'left'">Nama Anggota</th>
					<th data-options="field:'tgl_keberangkatan', width:'15', halign:'center', align:'right'" >Pokok <br> Pinjaman</th>
					<th data-options="field:'jumlah_jamaah', width:'14', halign:'center', align:'center'">Lama <br> Pinjam</th>
		
				</tr>
			</thead>
			</table>

			
			<div id="tb" style="height: 35px;">
				<div class="pull-right" style="vertical-align: middle;">
				<div id="filter_tgl" class="input-group" style="display: inline;">
					<button class="btn btn-default" id="daterange-btn">
						<i class="fa fa-calendar"></i> <span id="reportrange"><span>Pilih Tanggal</span></span>
						<i class="fa fa-caret-down"></i>
					</button>
				</div>

				<span>Cari :</span>
				<input name="kode_transaksi" id="kode_transaksi" size="23"  style="line-height:23px;border:1px solid #ccc">

				<a href="javascript:void(0);" id="btn_filter" class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="doSearch()">Cari</a>
				<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-clear" plain="false" onclick="clearSearch()">Hapus Filter</a>
			</div>
			</div> -->


 <div class="table-responsive">
                              <table id="data-table" class="table table-striped table-bordered table-hover " >
                               <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Invoice</th>
                                    <th class="text-center">Affiliate</th>
                                    <th class="text-center">Paket</th>
                                    <th class="text-center">Embarkasi</th>
                                   <th class="text-center">tgl daftar</th>
                                   <th class="text-center">tgl Keberangkatan</th>
                                   <th class="text-center">jumlah jamaah</th>
                                    
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <!--Appended by Ajax-->
                            </tbody>
                     </table>
                   </div>
         </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->


<script type="text/javascript">
 
    function get_data(url,q){
        
        if(!url)
            url = base_url+'registrasi_jamaah/get_data';
        
        $.ajax({
            
            url:url,type:'post',dataType:'json',
            data:{q:q},
            success:function(result){
                
                $("#data-table tbody").html(result.rows);
                $("ul.pagination").html(result.paging);
                $(".page-info").html(result.page_info);
            }
        
        });
    }

    function get_data(url,q){
        
        if(!url)
            url = base_url+'list_jamaah/ajax_list';
        
        $.ajax({
            
            url:url,type:'post',dataType:'json',
            data:{q:q},
            success:function(result){
                
                $("#data-table tbody").html(result.rows);
                $("ul.pagination").html(result.paging);
                $(".page-info").html(result.page_info);
            }
        
        });
    } 
</script>
<!-- 
<script type="text/javascript">
	$(document).ready(function() {
    $('#ajax_list').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> -->