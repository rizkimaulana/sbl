<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery.gritter/css/jquery.gritter.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/font-awesome-4/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/jquery-ui/jquery-ui.css">


<link href="<?php echo base_url();?>assets/css/front/style.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   
  <!--dashboard-->

    <!--dashboard-->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" /> 


<script>
  function cetak() {
      window.print();
  }
</script>
<style>
	@media print {
		#print{
			display: none;	
		}
		
	}
</style>
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 7pt; text-align: center;}
		</style>
<div id="print">
	<button type="button" class="btn btn-default" onclick="cetak()">Print</button>
    <a href="<?= base_url() ?>member_mgm" id="back" class="btn btn-default">Kembali</a>
</div>
		<table border='2' width="100%">
			<tr>
				<td colspan="2" class="h_kanan"><strong>PT SOLUSI BALAD LUMAMPAH</strong></td>
			</tr>
			<tr>
				<td width="20%"><strong>STRUK</strong>
					<hr width="500%">
				</td>
				<td class="h_kanan" width="80%">WISMA BUMI PUTRA- 5&7th Floor Jl. Asia Afrika No 141-149 Bandung</td>
			</tr>

		</table>
		<table border='2' width="100%">
		 <?php 
              $biaya_setting_ = $this->membermgm_model->get_key_val();
                foreach ($biaya_setting_ as $key => $value){
                  $out[$key] = $value;
                }
          ?>
			<tr>
				<td width="25%"> KODE MEMBER  </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri"><strong><?php echo $report_member['id_user']?></strong></td>

				<td colspan="5" class="h_kiri">BANK Transfer A/N SBL</td>
			</tr>
		
			<tr>
				<td width="25%"> Tanggal Transaksi </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri"><strong><?php echo $report_member['tanggal_daftar']?></strong></td>

				<td> BANK MANDIRI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong><?php echo $out['MANDIRI_DP5JT'];?></strong></td>
			</tr>
		
			<tr>
				<td> NAMA MEMBER </td>`
				<td>:</td>
				<td><strong><?php echo $report_member['nama']?></strong></td>
				
				
				
				<td></td>
				<td width="1%">:</td>
				<td class="h_kiri"></td>
			</tr>
			<tr>
				<td> SPONSOR </td>
				<td>:</td>
				<td><strong><?php echo $report_member['sponsor']?></strong></td>

				<td></td>
				<td width="1%">:</td>
				<td class="h_kiri"></td>
			</tr>

			<tr>
				<td> Tmpt Lahir </td>
				<td>:</td>
				<td><strong><?php echo $report_member['tempat_lahir']?></strong></td>

				
				<td> </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong></strong></td>
			</tr>
			<tr>
				<td> PEMBAYARNAN DP </td>
				<td>:</td>
				<td><strong>Rp. <?php echo number_format($report_member['nominal_dp'])?></strong></td>

				
				<td></td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong></strong></td>
			</tr>
		<!-- 	<tr>
				<td> Total Yang Harus Dibayar  </td>
				<td>:</td>
				<td><strong>Rp. <?php echo number_format ($report_member['booking_seats']);?></strong></td>


				<td> </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong></strong></td>
			</tr> -->
				<!-- <tr>
				<td> Tanggal Pelunasan Terakhir  </td>
				<td>:</td>
				<td><strong><?php echo $report_member['end_date'];?></strong></td>
			
			
				<td> </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong></strong></td>
			
			</tr>
				<td colspan="3" text-align:center;> Apabila Pelunasan Lewat Tanggal Yang telah di Tentukan Oleh SBL, Uang Booking Seats Akan Hangus </td> 
				
				<td> </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong></strong></td>
			</tr> -->
			<tr>
				<td colspan="3" text-align:center;> Ketentuan dan Syarat</td> 
				
				<td> </td>
				<td width="1%">:</td>
				<td class="h_kiri"><strong></strong></td>
			</tr>
				<tr>
				
				
				
				<td colspan="6"><strong> Untuk Waktu Pembayaran Dibatasi 4jam, Apabila Pembayaran lebih dari 3 hari Kerja <br>data yang anda Registrasikan akan Terhapus Otomatis dan pembayaran lebih 3 hari harus Registrasi ulang.</strong></td>
				
				
			</tr>
	</table> 
<!-- 
  <span class="input-group-btn">
  	 <?php
      echo '
      
      <a href="'.site_url('kuota_promoadmin/cetak_jamaahkuota').'/cetak/' . $report_member['id_booking_kuota'] . '"  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank"> <i class="glyphicon glyphicon-print"></i> CETAK KE PDF</a>';
    ?>
    
</span> -->
 <script>
  window.onload = function() {
      window.print();
    document.getElementById("back").focus();
  };
</script>