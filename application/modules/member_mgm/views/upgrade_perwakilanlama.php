<style type="text/css">
  
/* Credit to bootsnipp.com for the css for the color graph */
.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}
</style>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>member_mgm/save_upgrade_perwakilan_lama" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        


      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>UPGRADE GM</b>
                </div>
                       <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>


                 <div class="panel-body">
                  <input type="hidden" name="id_aff" value="<?php echo $upgrade_gm['id_aff']?>"  class="form-control" readonly="true">
                  <input type="hidden" name="nama" value="<?php echo $upgrade_gm['nama']?>"  class="form-control" readonly="true">
             
                   <input type="hidden" name="tempat_lahir" value="<?php echo $upgrade_gm['tempat_lahir']?>"  class="form-control" readonly="true">
                   <input type="hidden" name="tanggal_lahir" value="<?php echo $upgrade_gm['tanggal_lahir']?>"  class="form-control" readonly="true">
                  <input type="hidden" name="kelamin" value="<?php echo $upgrade_gm['kelamin']?>"  class="form-control" readonly="true">
                   <input type="hidden" name="ktp" value="<?php echo $upgrade_gm['ktp']?>"  class="form-control" readonly="true">
                   <input type="hidden" name="provinsi_id" value="<?php echo $upgrade_gm['provinsi_id']?>"  class="form-control" readonly="true">
                    <input type="hidden" name="kabupaten_id" value="<?php echo $upgrade_gm['kabupaten_id']?>"  class="form-control" readonly="true">
                    <input type="hidden" name="kecamatan_id" value="<?php echo $upgrade_gm['kecamatan_id']?>"  class="form-control" readonly="true">

                  <input type="hidden" name="alamat" value="<?php echo $upgrade_gm['alamat']?>"  class="form-control" readonly="true">

                  <input type="hidden" name="hubwaris" value="<?php echo $upgrade_gm['hubwaris']?>"  class="form-control" readonly="true">

                  <input type="hidden" name="telp" value="<?php echo $upgrade_gm['telp']?>"  class="form-control" readonly="true">
                  <input type="hidden" name="email" value="<?php echo $upgrade_gm['email']?>"  class="form-control" readonly="true">
                  <input type="hidden" name="waris" value="<?php echo $upgrade_gm['waris']?>"  class="form-control" readonly="true">

                   <input type="hidden" name="password" value="<?php echo $upgrade_gm['password']?>"  class="form-control" readonly="true">

                              <?php
                                $biaya_setting_ = $this->membermgm_model->get_key_val();
                                  foreach ($biaya_setting_ as $key => $value){
                                    $out[$key] = $value;
                                  }
                              ?>

                         <input type="hidden" name="biaya_registrasi" class="form-control input-sm" id="biaya_registrasi" value="<?php echo $out['DEPOSIT_GM_1'];?>" />

                           <input type="hidden" name="harga_product" class="form-control input-sm" id="harga_product" value="<?php echo $out['HARGA_PRODUCT_DP5JT'];?>" />

                  <div class="form-group">
                      <label class="col-lg-2 control-label">ID USER PERWAKILAN LAMA </label>
                      <div class="col-lg-5">
                         <input type="text" name="id_user" value="<?php echo $upgrade_gm['id_user']?>"  class="form-control" readonly="true">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">NAMA </label>
                      <div class="col-lg-5">
                         <input type="text" name="nama" value="<?php echo $upgrade_gm['nama']?>"  class="form-control" readonly="true">
                      </div>
                    </div>

                  <div class="form-group">
                      <!-- <label class="col-lg-2 control-label">Deposit</label> -->
                      <div class="col-lg-5">
                         <input type="hidden" name="deposit" value="<?php echo $upgrade_gm['nominal']?>"  class="form-control" readonly="true">
                      </div>
                    </div> 


                    <div class="form-group">
                      <label class="col-lg-2 control-label">ID SAHABAT</label>
                      <div class="col-lg-5">
                         <input type="text" name="id_affiliate" value="<?php echo $upgrade_gm['id_affiliate']?>"  class="form-control" readonly="true">
                      </div>
                    </div>

         <!--            <?php if ($upgrade_gm['nominal']=='10000000') { ?>
                         <div class="form-group">
                          <label class="col-lg-2 control-label">Check Apabila Anda Ingin Membagikan Saldo Ke Member Anda Sebesar 5jt</label>
                          <div class="col-lg-5">

                                  <div class="checkbox">
                                  <label><input type="checkbox" name="MyCheckBox" id="MyCheckBox" value="1">Check</label>
                                </div>
                          </div>
                      </div>
                    <?php } ?>
             -->

 <!-- <div id="ShowMeDIV" style="display: none;">
                            <?php
                                $biaya_setting_ = $this->membermgm_model->get_key_val();
                                  foreach ($biaya_setting_ as $key => $value){
                                    $out[$key] = $value;
                                  }
                              ?>
<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                         <input type="hidden" name="biaya_registrasi" class="form-control input-sm" id="biaya_registrasi" value="<?php echo $out['DEPOSIT_GM_1'];?>" />

                           <input type="hidden" name="harga_product" class="form-control input-sm" id="harga_product" value="<?php echo $out['HARGA_PRODUCT_DP5JT'];?>" />
          <div class="form-group">
          <label >Kolom ID sahabat Dibawah Ini Harus diisi, Dan ID sahabat dibawa ini belum status Perwakilan di Sahabat </label>
            <input type="text" name="id_sahabat" id="id_sahabat" class="form-control input-lg" placeholder="ID SAHABAT" tabindex="1" >
          </div>
       <div class="form-group">
            <input type="text" name="nama2" id="nama2" class="form-control input-lg" placeholder="Nama" tabindex="1" >
          </div>



       <div class="form-group">
        <input type="email" name="email2" id="email2" class="form-control input-lg" placeholder="EMAIL" tabindex="2" >
      </div>

      <div class="form-group">
        <input type="text" name="alamat2" id="alamat2" class="form-control input-lg" placeholder="Alamat Rumah" tabindex="3" >
      </div>
      

          <div class="form-group">
            <input type="text" name="telp2" id="telp2" class="form-control input-lg" placeholder="No Telpon/HP" tabindex="4" >
          </div>
        
         
          <div class="form-group">
            <input type="text" name="tempat_lahir2" id="tempat_lahir2" class="form-control input-lg" placeholder="Tempat Lahir" tabindex="5" >
          </div>
     
          
          <div class="form-group">
            <input type="text" name="tanggal_lahir2" id="datepicker" class="form-control input-lg" placeholder="Tanggal Bulan Tahun Kelahiran" tabindex="6" >
          </div>
        
         <div class="form-group">
            <input type="text" name="ktp2" id="ktp2" class="form-control input-lg" placeholder="No Identitas / KTP" tabindex="7" >
          </div>

         <div class="form-group">

         <select  class="form-control input-lg"  name="select_kelamin2" tabindex="8" >
             <option  value=''>- PILIH JENIS KELAMIN -</option>
                <?php foreach($select_kelamin as $sk){?>
                    <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
                <?php } ?>
          </select>
      </div>

      <div class="form-group">
         <select  class="form-control input-lg"  name="provinsi_id2" id="provinsi" tabindex="9" >
            <option  value=''>- PILIH Provinsi -</option>
                                  <?php foreach($provinsi as $Country){
                                      echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                  } ?>
          </select>
      </div>
      <div  class="form-group">
        <select  name="kabupaten_id2" class="form-control input-lg" id="kabupaten" tabindex="10">
            <option  value=''>- PILIH Kabupaten -</option>
             </select>
      </div>
      <div class="form-group">
         <select  class="form-control input-lg"  name="kecamatan_id2" id="kecamatan" tabindex="11">
            <option  value=''>- PILIH Kabupaten -</option>
            
          </select>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <input type="text" class="form-control input-lg" name="waris2" placeholder="Input Ahli waris"  tabindex="12" />
           
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
             <select   class="form-control input-lg" name="select_hub_ahliwaris2" tabindex="13">
                <option   value="">Hubungan Ahli Waris</option>
                  <?php foreach($select_hub_ahliwaris as $sk){?>
                      <option value="<?php echo $sk->id_hubwaris;?>"><?php echo $sk->hubungan;?></option>
                  <?php } ?>
              </select>
          </div>
        </div>
      </div>


         <div class="form-group">
            <input type="password" name="password2" id="password2" class="form-control input-lg" placeholder="No Identitas / KTP" tabindex="14" >
          </div>

  </div>    
</div> 
</div>  -->
                   <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> UPGRADE GM</button>
                 <a href="<?php echo base_url();?>member_mgm/upgrade_perwakilanlama" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   
      
  </form>       
                
</div>
       <script type="text/javascript">

$(function(){
    
    $('#thedate').datepicker({
        dateFormat: 'YYYY-MM-DD',
        altField: '#thealtdate',
        altFormat: 'YYYY-MM-DD'
    });
    
});
$("#datepicker").datepicker({
  dateFormat: 'yy-mm-dd',

    changeMonth: true,
    changeYear: true,
    
});
    </script>
  <script type="text/javascript">
//     $('#MyCheckBox').click(function() {
//     if ($(this).is(':checked')) {
//         $("#ShowMeDIV").show();
//          $("#id_sahabat").prop('',true);
//     } else {
//         $("#ShowMeDIV").hide();
//          $("#id_sahabat").prop('required',false);
//     }
// });
  </script>

      <script type="text/javascript">
       $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('member_mgm/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('member_mgm/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('member_mgm/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })

             jQuery(document).ready(function(){
              $(".chosen-select").chosen({width: "95%"}); 
            });
        });

    </script>

    <script type="text/javascript">
    (function (global) { 

    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };          
    }

})(window);
</script>