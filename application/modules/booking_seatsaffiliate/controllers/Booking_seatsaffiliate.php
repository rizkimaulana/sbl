<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Booking_seatsaffiliate extends CI_Controller {

    var $folder = "booking_seatsaffiliate";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend_affiliate/login_affiliate');
        if (!$this->general->privilege_check_affiliate(BOOKING_SEATS_AFFILIATE, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'registration');
        $this->load->helper('fungsi');
        $this->load->library('terbilang');
        $this->load->model('bookingseatsaffiliate_model');
        $this->load->model('bookingseatsaffiliate_model', 'r');
    }

    public function index() {

        $this->template->load('template/template', $this->folder . '/booking_seats');
    }

    public function cetak_data_kuota() {
        $id_booking_kuota = $this->uri->segment(3);

        $this->template->load('template/template', $this->folder . '/bookingseatsaffiliate_model/report_kuota/$id_booking_kuota ');
    }

    private function _select_embarkasi() {
        $embarkasi = array('1', '3');
        $this->db->where_in('id_embarkasi', $embarkasi);
        return $this->db->get('embarkasi')->result();
    }

    private function _select_affiliate_type() {
        $id_affiliate_type = array('1', '5');
        $this->db->where_in('id_affiliate_type', $id_affiliate_type);
        return $this->db->get('affiliate_type')->result();
    }

    function add_ajax_affiliate_type($id_affiliate_type) {
        $query = $this->db->order_by("nama", "asc")->get_where(
                'affiliate', array(
            'id_affiliate_type' => $id_affiliate_type,
            'status' => 1
                )
        );
        $this->db->order_by("nama", "asc");
        $data = "<option value=''>- Select affiliate -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->id_user . "'>" . $value->nama . " " . $value->id_user . "</option>";
        }
        echo $data;
    }

    public function ajax_list() {
        $list = $this->r->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<td width="20%" >' . $r->invoice . '</strong></td>';
            $row[] = '<td width="20%" >' . $r->id_affiliate . ' </strong> </td>';
            $row[] = '<td width="20%" >' . $r->tgl_pembelian . ' </strong> </td>';
            $row[] = '<td width="20%" >' . $r->schedule . ' </strong> </td>';
            $row[] = '<td width="20%" >' . $r->jml_booking_seats . ' </strong> </td>';

            if ($r->jml_booking_seats == 0) {
                $row[] .= '<a title="ID BOOKING SEATS SUDAH DIMUTASI SEMUA" class="btn btn-sm btn-danger" href="">
	                            <i class="fa fa-pencil"></i> Mutasi
	                        </a> ';
            } else {
                $row[] .= '<a title="Mutasi " class="btn btn-sm btn-primary"  href="' . base_url() . 'booking_seatsaffiliate/detail/' . $r->id_booking_seats . '">
	                            <i class="fa fa-pencil"></i> Mutasi
	                        </a> ';
            }





            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->r->count_all(),
            "recordsFiltered" => $this->r->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function get_data() {

        $limit = $this->config->item('limit');
        $offset = $this->uri->segment(3, 0);
        $q = isset($_POST['q']) ? $_POST['q'] : '';
        $data = $this->bookingseatsaffiliate_model->get_data($offset, $limit, $q);
        $rows = $paging = '';
        $total = $data['total'];

        if ($data['data']) {

            $i = $offset + 1;
            $j = 1;
            foreach ($data['data'] as $r) {

                $rows .= '<tr>';

                // $rows .='<td width="5%">'.$i.'</td>';
                $rows .= '<td width="2"><input size="5" type="hidden" name="id_schedule" class="form-control" value="' . $r->id_schedule . '" id="id_registrasi" readonly="true">' . $i . '</td>';


                $rows .= '<td width="10%">' . $r->invoice . '</td>';
                $rows .= '<td width="20%">' . $r->affiliate . '</td>';
                $rows .= '<td width="10%">' . $r->id_affiliate . '</td>';
                $rows .= '<td width="20%">' . $r->tgl_pembelian . '</td>';
                $rows .= '<td width="9%">' . $r->jml_booking_seats . '</td>';
                $rows .= '<td width="30%" align="center">';
                if ($r->status == 1) {
                    $rows .= '<a title="Mutasi" class="btn btn-sm btn-danger" href="">
	                            <i class="fa fa-pencil"></i> Mutasi
	                        </a> ';
                } elseif ($r->jml_booking_seats == 0) {
                    $rows .= '<a title="ID BOOKING SEATS SUDAH DIMUTASI SEMUA" class="btn btn-sm btn-danger" href="">
	                            <i class="fa fa-pencil"></i> Mutasi
	                        </a> ';
                } else {
                    $rows .= '<a title="Mutasi" class="btn btn-sm btn-primary" href="' . base_url() . 'booking_seatsaffiliate/detail/' . $r->id_booking_seats . '">
	                            <i class="fa fa-pencil"></i> Mutasi
	                        </a> ';
                }
                $rows .= '</td>';

                $rows .= '</tr>';

                ++$i;
                ++$j;
            }

            $paging .= '<li><span class="page-info">Displaying ' . ($j - 1) . ' Of ' . $total . ' items</span></i></li>';
            $paging .= $this->_paging($total, $limit);
        } else {

            $rows .= '<tr>';
            $rows .= '<td colspan="6">No Data</td>';
            $rows .= '</tr>';
        }

        echo json_encode(array('rows' => $rows, 'total' => $total, 'paging' => $paging));
    }

    private function _paging($total, $limit) {

        $config = array(
            'base_url' => base_url() . 'room/get_data/',
            'total_rows' => $total,
            'per_page' => $limit,
            'uri_segment' => 3
        );
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }

    public function generate_data_seats() {

        $schedule = $this->input->post('radio');
        $stored_procedure = "CALL view_schedule_promo(?)";
        $query = $this->db->query($stored_procedure, array('id_schedule' => $schedule))->result_array();

        foreach ($query as $key => $value) {

            $seats = $value['seats'];
        }
        $jml_seats = $this->input->post('jml_seats');

        $this->form_validation->set_rules('radio', 'radio', 'required|trim');
        if ($this->form_validation->run() == true) {//jika validasi dijalankan dan benar
            if ($jml_seats > $seats) {

                $this->session->set_flashdata('Warning', "Jumlah Seats Yang Anda Pilih Melebihi Jumlah Seats Yang Tersedia");
                redirect('booking_seatsaffiliate/add');
            } else {
                $data = $this->input->post(null, true);
                $schedule = $this->input->post('radio');

                $send = $this->bookingseatsaffiliate_model->save_get_bookingseats($data);
            }
        }
        //          else{
        // print_r('expression');
        // } 
    }

    public function save_booking_mutasi() {



        $data = $this->input->post(null, true);
        $send = $this->bookingseatsaffiliate_model->save_booking_mutasi($data);

        if ($send) {
            $this->session->set_flashdata('info', "Successfull");
            //$id_booking = $this->uri->segment(4);
            $id_booking = $this->input->post('id_booking');
            // redirect('bookingseatsaffiliate_model/detail/'.$id_booking.'');
        }
    }

    public function add() {

        // if(!$this->general->privilege_check(JAMAAH_REGISTRATION_FOR_AFFILIATE,'add'))
        // $this->general->no_access();


        $data = array('select_affiliate_type' => $this->_select_affiliate_type(),
            'select_embarkasi' => $this->_select_embarkasi(),
            'select_roomcategory' => $this->_select_roomcategory(),
        );
        $this->template->load('template/template', $this->folder . '/add', $data);
    }

    public function report_booking_seats() {
        $id_user = $this->session->userdata('id_user');
        $id_booking_seats = $this->uri->segment(3);
        $report_booking_seats = $this->bookingseatsaffiliate_model->faktur_booking_seats($id_booking_seats, $id_user);
        // $pic    = array();
        // $pic_booking= array();
        if (!$report_booking_seats) {
            // show_404();
            $this->template->load('template/template', $this->folder . '/report_booking_seats_delete');
        }



        $data = array(
            // 'booking'=>$booking,
            'report_booking_seats' => $report_booking_seats,
                // 'pic'=>$pic
        );
        if ($report_booking_seats['status'] == 0) {
            $this->template->load('template/template', $this->folder . '/report_booking_seats', ($data));
        } elseif ($report_booking_seats['status'] == 1) {
            $this->template->load('template/template', $this->folder . '/report_booking_seats_aktif', ($data));
        } else {
            $this->template->load('template/template', $this->folder . '/report_booking_seats_delete');
        }

        // $this->load->view('bilyet_barcode',($data));
    }

    public function detail() {
        // if(!$this->general->privilege_check(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view'))
        //  $this->general->no_access();
        $id_user = $this->session->userdata('id_user');
        $id_booking_seats = $this->uri->segment(3);
        $booking = $this->bookingseatsaffiliate_model->get_booking_seats($id_booking_seats, $id_user);

        // if(!$booking){
        //     show_404();
        // }

        $data = array(
            'booking' => $booking,
        );

        $this->template->load('template/template', $this->folder . '/booking', ($data));
    }

    function searchItem() {


        $paket = $this->input->post('q');
        $departure = $this->input->post('l');
        $datepicker_tahun_keberangkatan = $this->input->post('s');
        $datepicker_keberangkatan = $this->input->post('t');

        if (!empty($paket)) {
            $this->bookingseatsaffiliate_model->searchItem($paket, $departure, $datepicker_tahun_keberangkatan, $datepicker_keberangkatan);
        } else {
            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
        }
    }

    //     private function _select_embarkasi(){
    //     return $this->db->get('embarkasi')->result();
    // }
    private function _select_product() {

        $status = array('1');
        $this->db->where_in('status', $status);
        return $this->db->get('product')->result();
    }

    private function _select_roomcategory() {
        //  $id_room_category = array('1');
        // $this->db->where_in('id_room_category', $id_room_category);
        return $this->db->get('category')->result();
    }

    function add_ajax_kab($id_prov) {
        $query = $this->db->get_where('wilayah_kabupaten', array('provinsi_id' => $id_prov));
        $data = "<option value=''>- Select Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->kabupaten_id . "'>" . $value->nama . "</option>";
        }
        echo $data;
    }

    function add_ajax_kec($id_kab) {
        $query = $this->db->get_where('wilayah_kecamatan', array('kabupaten_id' => $id_kab));
        $data = "<option value=''> - Pilih Kecamatan - </option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->kecamatan_id . "'>" . $value->nama . "</option>";
        }
        echo $data;
    }

    function add_ajax_des($id_kec) {
        $query = $this->db->get_where('wilayah_desa', array('kecamatan_id' => $id_kec));
        $data = "<option value=''> - Pilih Desa - </option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->kecamatan_id . "'>" . $value->nama . "</option>";
        }
        echo $data;
    }

    private function _select_kelamin() {
        $kdstatus = array('2', '3');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result();
    }

    private function _select_rentangumur() {

        return $this->db->get('rentang_umur')->result();
    }

    private function _select_statuskawin() {
        $kdstatus = array('4', '5');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result();
    }

    private function _select_status_hubungan() {
        $kdstatus = array('8', '9');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result();
    }

    private function _family_relation() {

        return $this->db->get('family_relation')->result();
    }

    private function _select_keluarga($id_booking = '') {
        $this->db->where_in('id_booking', $id_booking);
        return $this->db->get('registrasi_jamaah')->result();
    }

    private function _select_pemberangkatan() {
        return $this->db->get('view_refund')->result();
    }

    private function _select_type() {

        return $this->db->get('room_type')->result();
    }

    private function _select_hub_ahliwaris() {

        return $this->db->get('hub_ahli_waris')->result();
    }

    private function _select_merchandise() {

        return $this->db->get('merchandise')->result();
    }

    function get_biaya_refund_() {
        $pemberangkatan = $this->input->post('pemberangkatan');
        $select_pemberangkatan = $this->bookingseatsaffiliate_model->get_data_bandara($pemberangkatan);
        if ($select_pemberangkatan->num_rows() > 0) {
            $select_pemberangkatan = $select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
        }
    }

    function get_biaya_refund_2() {
        $pemberangkatan = $this->input->post('pemberangkatan');
        $select_pemberangkatan = $this->bookingseatsaffiliate_model->get_data_bandara($pemberangkatan);
        if ($select_pemberangkatan->num_rows() > 0) {
            $select_pemberangkatan = $select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
        }
    }

    function get_biaya_refund_3() {
        $pemberangkatan = $this->input->post('pemberangkatan');
        $select_pemberangkatan = $this->bookingseatsaffiliate_model->get_data_bandara($pemberangkatan);
        if ($select_pemberangkatan->num_rows() > 0) {
            $select_pemberangkatan = $select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
        }
    }

    function searchItem_paket_promo() {


        $paket = $this->input->post('q');
        $departure = $this->input->post('l');
        $datepicker_tahun_keberangkatan = $this->input->post('s');
        $datepicker_keberangkatan = $this->input->post('t');

        if (!empty($datepicker_tahun_keberangkatan)) {
            $this->bookingseatsaffiliate_model->searchItem_paket_promo($datepicker_tahun_keberangkatan, $datepicker_keberangkatan, $departure);
        } else {
            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
        }
    }

    private function _select_status_visa() {

        return $this->db->get('status_visa')->result();
    }

    function searchItem_paket_reguller() {


        $paket = $this->input->post('q');
        $departure = $this->input->post('l');
        $datepicker_tahun_keberangkatan = $this->input->post('s');
        $datepicker_keberangkatan = $this->input->post('t');

        if (!empty($paket)) {
            $this->bookingseatsaffiliate_model->searchItem_paket_reguller($datepicker_tahun_keberangkatan, $datepicker_keberangkatan, $departure, $paket);
        } else {
            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
        }
    }

    private function _select_kode_booking() {
        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        $booking = $this->bookingseatsaffiliate_model->get_pic_booking($id_booking, $id);
        $id_booking_seats = $this->input->post('id_booking_seats');
        $id_affiliate = $this->session->userdata('id_user');

        $this->db->where_in('id_booking_seats', $booking['id_booking_seats']);
        $this->db->where_in('status_mutasi', '0');
        return $this->db->get('booking_seats_list')->result();
    }

    public function registrasi() {



        // if(!$this->general->privilege_check(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view'))
        //  $this->general->no_access();

        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        // $id_booking = $this->input->post('id_booking');
        $booking = $this->bookingseatsaffiliate_model->get_pic_booking($id_booking, $id);
        $pic = array();
        $pic_booking = array();
        // if(!$booking){
        //     show_404();
        // }
        // else{
        // $pic_booking = $this->bookingseatsaffiliate_model->get_pic_booking($id_booking);
        $pic = $this->bookingseatsaffiliate_model->get_pic($id_booking);
        // }    


        $data = array(
            'family_relation' => $this->_family_relation(),
            'select_product' => $this->_select_product(),
            'select_merchandise' => $this->_select_merchandise(),
            'select_rentangumur' => $this->_select_rentangumur(),
            'select_status_hubungan' => $this->_select_status_hubungan(),
            'select_kelamin' => $this->_select_kelamin(),
            'select_statuskawin' => $this->_select_statuskawin(),
            'provinsi' => $this->bookingseatsaffiliate_model->get_all_provinsi(),
            'kabupaten' => $this->bookingseatsaffiliate_model->get_all_kabupaten(),
            'select_embarkasi' => $this->_select_embarkasi(),
            'id_bandara' => $this->bookingseatsaffiliate_model->get_data_bandara(),
            //'select_keluarga'=>$this->_select_keluarga($id_booking),
            'select_kode_booking' => $this->_select_kode_booking(),
            'select_pemberangkatan' => $this->_select_pemberangkatan(),
            'select_type' => $this->_select_type(),
            'select_hub_ahliwaris' => $this->_select_hub_ahliwaris(),
            // 'pic_booking'=>$pic_booking,
            'booking' => $booking, 'pic' => $pic,
            'select_status_visa' => $this->_select_status_visa(),
        );

        $this->template->load('template/template', $this->folder . '/mutasi_data', ($data));
    }

    public function save_registrasi() {


        $id_booking_seats = $this->input->post('id_booking_seats');
        $id_booking = $this->input->post('id_booking');
        //   $data = $this->input->post(null,true);
        //  $send = $this->bookingseatsaffiliate_model->save_registrasi($data);
        //  if($send)
        //  	$this->session->set_flashdata('success', "Transaction Successfull");
        // redirect('booking_seatsaffiliate/registrasi/'.$id_booking.'/'.$id_booking_seats .'');
        $this->form_validation->set_rules('nama', 'nama', 'required|trim');
        if ($this->form_validation->run() == true) {//jika validasi dijalankan dan benar
            $no_identitas = $this->input->post('no_identitas'); // mendapatkan input dari kode
            $cek = $this->bookingseatsaffiliate_model->cek($no_identitas); // cek kode di database


            $id_sahabat = $this->input->post('id_sahabat');
            $cek_pindah_paket = $this->bookingseatsaffiliate_model->cek_pindah_paket($id_sahabat);

            if ($cek->num_rows() > 0) { // jika kode sudah ada, maka tampilkan pesan
                // echo'<div class="alert alert-dismissable alert-danger"><small>No Identitas sudah digunakan</small></div>';
                $this->session->set_flashdata('info', "No Identitas sudah digunakan");
                $id_booking = $this->input->post('id_booking');
                redirect('registrasi_affiliatepromo/detail/' . $id_booking . '');
            } elseif ($this->input->post('MyCheckBox') == '1' && $cek_pindah_paket->num_rows() > 0) {
                $this->session->set_flashdata('info', "ID Sahabat Yang anda input status sudah Pindah Paket");
                redirect('registrasi_affiliatepromo/detail/' . $id_booking . '');
            } else {

                $data = $this->input->post(null, true);
                $send = $this->bookingseatsaffiliate_model->save_registrasi($data);

                if ($send)
                    $this->session->set_flashdata('success', "Transaction Successfull");
                redirect('booking_seatsaffiliate/registrasi/' . $id_booking . '/' . $id_booking_seats . '');
            }
        }
    }

    public function get_registrasi() {
        $id_booking = $this->uri->segment(3);
        $category = $this->uri->segment(4);
        $sql = "SELECT a.id_booking as id_booking,a.tempjmljamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking) as jumlah_jamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking and registrasi_jamaah.ket_keberangkatan='8') as jml_keluarga 
				         from booking as a where id_booking = '" . $id_booking . "'";
        $query = $this->db->query($sql)->result_array();
        foreach ($query as $key => $value) {
            $jumlah_jamaah = $value['jumlah_jamaah'];
            $jml_keluarga = $value['jml_keluarga'];
            $booking = $value['id_booking'];
            $tempjmljamaah = $value['tempjmljamaah'];
        }

        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        $booking = $this->bookingseatsaffiliate_model->get_pic_booking($id_booking, $id);

        $stored_procedure = "CALL cek_sisa_pembayaran_booking_seats(?)";
        $query = $this->db->query($stored_procedure, array('id_booking_seats' => $booking['id_booking_seats']
                ))->result_array();
        foreach ($query as $key => $value) {
            $pelunasan = $value['pelunasan'];
        }
        // print_r($pelunasan);
        if ($pelunasan == 1) {
            redirect('booking_seatsaffiliate');
        } else {
            if ($jml_keluarga > 1) {
                redirect('booking_seatsaffiliate/setting_keluarga/' . $id_booking . '');
            } elseif ($jumlah_jamaah > 1) {
                redirect('booking_seatsaffiliate/room_question/' . $id_booking . '/' . $category . '');
            } elseif ($jumlah_jamaah == 1) {
                redirect('booking_seatsaffiliate/report_registrasi/' . $id_booking . '');
            }
        }
    }

    public function report_registrasi() {
        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        $report_registrasi = $this->bookingseatsaffiliate_model->get_report_registrasi($id_booking, $id);
        if (!$report_registrasi) {
            // show_404();
            $this->template->load('template/template', $this->folder . '/notif_delete_jamaah');
        }


        $data = array(
            'report_registrasi' => $report_registrasi,
        );

        if ($report_registrasi['status'] == 0) {
            $this->template->load('template/template', $this->folder . '/report_registrasi', ($data));
        } elseif ($report_registrasi['status'] == 1) {
            $this->template->load('template/template', $this->folder . '/report_registrasi_aktif', ($data));
        }
    }

    public function setting_keluarga() {



        // if(!$this->general->privilege_check_affiliate(booking_seatsaffiliate_AFFILIATE,'view'))
        // 		$this->general->no_access();

        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        $booking = $this->bookingseatsaffiliate_model->get_pic_booking($id_booking, $id);
        $pic = array();
        // $pic_booking= array();
        if (!$booking) {
            show_404();
        } else {

            // $pic_booking = $this->Bookingseatsaffiliate_model->get_pic_booking($id_booking);
            $pic = $this->bookingseatsaffiliate_model->get_pic_keluarga($id_booking, $id);
        }


        $data = array(
            'select_keluarga' => $this->_select_keluarga($id_booking),
            'family_relation' => $this->_family_relation(),
            'pic' => $pic,
            'booking' => $booking,
        );

        $this->template->load('template/template', $this->folder . '/setting_keluarga', ($data));
    }

    public function update_keluarga() {
        $id_booking = $this->input->post('id_booking');
        $data = $this->input->post(null, true);
        $data2 = $this->input->post('id_jamaah');
        $category = $this->input->post('category');

        // print_r($data);
        if ($this->bookingseatsaffiliate_model->update_setting_keluarga($data, $data2)) {


            // redirect('booking_seatsaffiliate/report_registrasi/'.$id_booking.'');  
            redirect('booking_seatsaffiliate/room_question/' . $id_booking . '/' . $category . '');
        } else {

            show_error("Error occured, please try again");
        }
    }

    public function setting_room_type() {



        // if(!$this->general->privilege_check_affiliate(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view'))
        //  $this->general->no_access();

        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        $booking = $this->bookingseatsaffiliate_model->get_pic_booking_room($id_booking, $id);
        $pic = array();
        // $pic_booking= array();
        if (!$booking) {
            show_404();
        } else {

            // $pic_booking = $this->bookingseatsaffiliate_model->get_pic_booking($id_booking);
            $pic = $this->bookingseatsaffiliate_model->get_pic_room_type($id_booking);
        }


        $data = array(
            'select_type' => $this->_select_type(),
            'pic' => $pic,
            'booking' => $booking,
            'select_jamaah' => $this->_select_jamaah($id_booking),
                // 'select_keluarga'=>$this->_select_keluarga(),
        );

        $this->template->load('template/template', $this->folder . '/setting_room', ($data));
    }

    public function room_question() {



        // if(!$this->general->privilege_check(BOOKING_SEATS_ADMIN,'view'))
        //  $this->general->no_access();
        $id = $this->session->userdata('id_user');
        $id_booking = $this->uri->segment(3);
        $category = $this->uri->segment(4);
        $booking = $this->bookingseatsaffiliate_model->get_pic_order_view($id_booking, $id);
        $pic = array();
        // $pic_booking= array();
        if (!$booking) {
            show_404();
        } else {

            // $category = $this->input->post('category');
            $pic = $this->bookingseatsaffiliate_model->get_pic_room_price($category);
        }


        $data = array(
            // 'select_type'=>$this->_select_type(),
            'pic' => $pic,
            'booking' => $booking,
        );

        $this->template->load('template/template', $this->folder . '/room_question', ($data));
    }

    public function update_room() {
        $this->form_validation->set_rules('id_booking', 'id_booking', 'required|trim');
        if ($this->form_validation->run() == TRUE) {
            $id_booking = $this->input->post('id_booking');
            $cek_room = $this->bookingseatsaffiliate_model->cek_room($id_booking);
            if ($cek_room->num_rows() > 0) {
                redirect('booking_seatsaffiliate/setting_room_type/' . $id_booking . '');
            } else {
                $id_booking = $this->input->post('id_booking');
                $data = $this->input->post(null, true);
                $send = $this->bookingseatsaffiliate_model->update_room_order($data);
                if ($send)
                    redirect('booking_seatsaffiliate/setting_room_type/' . $id_booking . '');
            }
        }
    }

    public function update_room_setting() {


        $id_booking = $this->input->post('id_booking');
        $data = $this->input->post(null, true);
        $data2 = $this->input->post('select_jamaah');


        $send = $this->bookingseatsaffiliate_model->update_room_setting($data);
        if ($send)
            redirect('booking_seatsaffiliate/report_registrasi/' . $id_booking . '');
    }

    private function _select_jamaah($id_booking = '') {

        $this->db->where_in('id_booking', $id_booking);
        return $this->db->get('registrasi_jamaah')->result();
    }

    function searchItem_pindahpaket() {


        $id_sahabat = $this->input->post('q');

        if (!empty($id_sahabat)) {
            $this->bookingseatsaffiliate_model->searchItem_pindahpaket($id_sahabat);
        } else {

            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Maaf ! MASUKAN ID SAHABAT  </h2></td></tr>
                 	<tr>
                   
                   <td><input name="nama_dp" type="text"  id="nama_dp" maxlength="0" class="form-control" required/></td>
                   <td><input name="dp" type="text"  id="dp" class="form-control" maxlength="0" required/></td>
                   <td><input name="jml_cicilan" type="text"  class="form-control" maxlength="0"  id="jml_cicilan"   required/></td>
                   <td><input name="cicilan_perbulan" type="text" class="form-control"  maxlength="0" id="cicilan_perbulan"   required /></td>
                    
                 </tr>';
        }
    }

}
