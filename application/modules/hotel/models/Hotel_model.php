<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hotel_model extends CI_Model{
   
  
    private $_table="hotel";
    private $_primary="id_hotel";
private $db2;
public function __construct()
 {
  parent::__construct();
         $this->db2 = $this->load->database('db2', TRUE);
 }
    public function save($data){
     
      
        $arr = array(
            'id_bintang' => $data['select_bintang'],
            'nama_hotel' => $data['nama_hotel'],
            'keterangan' => $data['keterangan'],
           
        );       
        
         return $this->db2->insert('hotel',$arr);
    }


    public function update($data){
        
    $arr = array(
            'id_bintang' => $data['select_bintang'],
            'nama_hotel' => $data['nama_hotel'],
            'keterangan' => $data['keterangan'],
           
        );        
              
        return $this->db2->update($this->_table,$arr,array('id_hotel'=>$data['id_hotel']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
 
 
         $sql = " SELECT a.*, b.bintang FROM hotel a
                        LEFT JOIN bintang b ON b.id_bintang = a.id_bintang
                    WHERE 1=1
                    ";
        
        if($q){
         

            $sql .=" AND nama_hotel LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY id_hotel DESC ";

        $ret['total'] = $this->db2->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db2->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_hotel)
    {
        $this->db2->where('id_hotel', $id_hotel);
        $this->db2->delete($this->_table);
    }
    
      public function Ambil($where= "") {
        // $db2 = $this->load->database('db2', TRUE);
        // $data =  $db2->get('hotel'.$where)->result();
      $data2 = $this->db2->query('SELECT * from hotel '.$where);
      return $data2;

        // return $this->db2->get('hotel');
  }
}


