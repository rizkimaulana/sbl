<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_Handling extends CI_Controller {

    var $folder = "finance";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(OPERASIONAL_HANDLING, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('Operasional_model');
    }

    public function index() {

        $this->template->load('template/template', $this->folder . '/operasional_handling');
    }

    public function ajax_list() {
        $id_user = $this->session->userdata('id_user');
        $sql = "SELECT a.invoice, a.id_affiliate, b.nama as affiliate, COUNT(*) as jml_jamaah,
                sum(if((`a`.`status_manifest` = '1'),1,0)) as status_manifest,
                (if(ISNULL(d.id),0,1)) as status_approval
                FROM registrasi_jamaah as a
                JOIN affiliate b ON b.id_user = a.id_affiliate
                JOIN additional_cost c ON (c.id_registrasi = a.id_registrasi and c.invoice = a.invoice 
                AND c.jns_trans = '144' AND c.status = '2')
                LEFT JOIN t_handling d ON d.invoice = a.invoice
                WHERE a.`status` = '1'  AND c.id_user in (SELECT id_user FROM admin_cabang WHERE admin = '".$id_user."') ".
                "GROUP BY a.id_booking";
        
        $list = $this->db->query($sql)->result();
        
        $data = array();
        $no = 0;
        foreach ($list as $row) {
            $no++;
            $item = array();
            $item[] = $no;
            $item[] = 'INVOICE : '.$row->invoice.'<br> CABANG : '.$row->id_affiliate .'<br>'.$row->affiliate;
            $item[] = $row->jml_jamaah;
            $biaya = $this->db->get_where('setting', array('id_setting' => '33'))->row_array();
            $item[] = number_format($biaya['key_setting'] * $row->jml_jamaah);
            
            if(($row->status_manifest==$row->jml_jamaah) ){
                $ket = 'Data Manifest Sudah Lengkap';
            }elseif($row->status_manifest <> $row->jml_jamaah){
                $ket = 'Jumlah Jamaah : '.$row->jml_jamaah.'<br> Jumlah Manifest : '.$row->status_manifest;
            }
            
            $item[] = $ket;
            
            $btnProcess = '<center><a disabled class="btn btn-sm btn-success" href="javascript:void()" title="Approval Finance">Approved</a></center>';
            if(($row->status_manifest==$row->jml_jamaah) && $row->status_approval <> 1 ){
                $btnProcess = '<center><a class="btn btn-sm btn-warning" href="javascript:void()" title="Approval Finance" onclick="konfirmasi(' . "'" . $row->invoice . "'" . ')"><i class="fa fa-pencil"></i> Approval</a></center>';
            }
            $item[] = $btnProcess;
            
            $data[] = $item;
            
        }
        $output = array(
            "data" => $data,
        );
        //output to json format
        //$this->myDebug($data);
        echo json_encode($output);
    }

    function approval(){
        $invoice = $this->uri->segment(4);
        
        $data = $this->input->post();
        
        $sql = "SELECT a.invoice, b.nama as affiliate, COUNT(*) as jml_jamaah,
                sum(if((`a`.`status_manifest` = '1'),1,0)) as status_manifest, c.id_user, c.keterangan from registrasi_jamaah as a
                JOIN affiliate b ON b.id_user = a.id_affiliate
                JOIN additional_cost c ON (c.id_registrasi = a.id_registrasi and c.invoice = a.invoice 
                AND c.jns_trans = '144' AND c.status = '2')
                WHERE a.`status` = '1'
                AND c.invoice = '".$invoice."' GROUP BY a.id_booking";
        
        $item = $this->db->query($sql)->row_array();
        
        $biaya = $this->db->get_where('setting', array('id_setting' => '33'))->row_array();
        
        $insert_arr = array(
            'invoice' => $item['invoice'],
            'id_affiliate' => $item['id_user'],
            'jml_jamaah' => $item['jml_jamaah'],
            'biaya' => $biaya['key_setting'] * $item['jml_jamaah'],
            'keterangan' => 'BIAYA PENGURUSAN JAMAAH',
            'status' => '1',
            'create_by' => $this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s')
        );
       
        $result = $this->db->insert('t_handling', $insert_arr);
        $id = $this->db->insert_id();
        
        $kd_trans = $this->generate_kode($id);
        $this->db->update('t_handling', array('kd_trans' => $kd_trans), array('id' => $id));
        
        //upadate status di table additional_cost = 3
        $update_arr = array('status' => 3);
        $this->db->update('additional_cost', $update_arr, array('invoice'=> $invoice, 'jns_trans' => '144'));
        
        
//        //SP HANDLING
//        $p_invoice = $invoice;
//        //$p_id_affiliate = $id_user;
//        $p_jns_trans = '144';
//        $p_id_user = $this->session->userdata('id_user');
//                
//        $query ="call sp_t_handling ('" . $p_invoice . "','" . $p_jns_trans . "','" . $p_id_user . "',@hasil)";
//        
//        $sql_query = $this->db->query($query)->row_array();
//        //print_r($sql_query['@result']);die();
//        if($sql_query['@result']==1) {
//            //$this->db->trans_complete();
//            return true;
//        }else{
//            //$this->db->trans_rollback();
//            return false;
//        }
    }
    
    public function generate_kode($idx) {
        $today = date('ymd');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return 'HDL'. $today . $idx;
    }
}
