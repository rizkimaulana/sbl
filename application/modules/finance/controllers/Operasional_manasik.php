<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_manasik extends CI_Controller {

    var $folder = "finance";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(OPERASIONAL_MANASIK, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('Operasional_model');
    }

    public function index() {

        $this->template->load('template/template', $this->folder . '/operasional_manasik');
    }

    public function ajax_list() {
        $search = $this->input->post('s');

        $list = $this->Operasional_model->get_data_manasik($search);

        $data = array();
        $no = 0;
        foreach ($list as $row) {
            $no++;
            $item = array();

            //get data refrensi untuk table registrasi jamaah 
            $affiliate = $this->get_data_affiliate($row->id_affiliate);

            //get data additional cost untuk untuk table registrasi jamaah
            $cabang = $this->get_data_cabang($row->invoice);
            
            //get data refrensi untuk table cabang
            $btnCabang = '<button class="btn btn-sm btn-success">' . $row->id_affiliate. ' - '.$affiliate['nama']. '</button>';
            if (!empty($cabang['id_user'])) {
                $affcabang = $this->get_data_affiliate($cabang['id_user']);
                if ($row->id_affiliate != $affcabang['id_user']) {
                    $btnCabang = '<button class="btn btn-sm btn-danger">' . $affcabang['id_user']. ' - '.$affcabang['nama'] . '</button>';
                }
            }

            //if($row->id_affiliate != $affcabang['id_user']){
            //}
//            $item[] = $no;
//            $item[] .= '<td width="15%" >' . $row->invoice . '</td>';
//            $item[] .= '<td width="10%" ><b>' . $row->id_affiliate . '</b><br>' . $affiliate['nama'] . '</td>';
//            //$item[] .= '<td width="1%"></td>';
//            $item[] .= '<td width="1%">' . $btnCabang . '</td>';
//            $item[] .= '<td width="10%" >' . number_format($row->jml_jamaah) . '</td>';
//            $item[] .= '<td width="20%">Rp ' . number_format($row->jml_jamaah * 70000) . '</td>';
//            $item[] .= '<td width="25%">BIAYA MANASIK</td>';
//
//            $cluster = '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="PINDAH DATA" onclick="edit_pindah_cabang(' . "'" . $row->invoice . "'" . ')"><i class="glyphicon glyphicon-move"></i> PINDAH DATA</a>';
//
//            $item[] .= $cluster;
//            $data[] = $item;
            if ($row->status == 4) {
                $btnProcess = '<a disabled class="btn btn-sm btn-warning" href="javascript:void(0)" title="PINDAH DATA" onclick="edit_pindah_cabang(' . "'" . $row->invoice . "'" . ')"><i class="glyphicon glyphicon-move"></i> PINDAH DATA</a>';
            }else{
                $btnProcess = '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="PINDAH DATA" onclick="edit_pindah_cabang(' . "'" . $row->invoice . "'" . ')"><i class="glyphicon glyphicon-move"></i> PINDAH DATA</a>';
            }
            echo '<tr> 
                    <td>' . $no . '</td>
                    <td><strong>' . $row->invoice . '</strong></td>
                       <td>' . $row->id_affiliate . '</b><br>' . $affiliate['nama'] . '</td>    
                       <td>' . $btnCabang . '</td>
                       <td>' . number_format($row->jml_jamaah) . '</td>
                       <td>' . number_format($row->jml_jamaah * 70000) . '</td>
                       <td>BIAYA OPERASIONAL MANASIK</td>

                        <td>' . $btnProcess . '</td>
                    </tr>';
        }

//        $output = array(
//            "data" => $data,
//        );
        //output to json format
        //$this->myDebug($data);
        //echo json_encode($output);
    }

    function get_data_cabang($invoice) {
        $this->db->select('additional_cost.id_user, additional_cost.status');
        //$this->db->join('affiliate', 'affiliate.id_user = additional_cost.id_user');
        $this->db->where('additional_cost.invoice', $invoice);
        $this->db->where('additional_cost.jns_trans', '155');
        $this->db->order_by('additional_cost.id_user', 'DESC');
        $hasil = $this->db->get('additional_cost')->row_array();
        $result = array(
            'id_user' => !empty($hasil['id_user']) ? $hasil['id_user'] : '',
//            'nama_cabang' => !empty($hasil['nama_cabang']) ? $hasil['nama_cabang'] : '',
            'status_additional_cost' => !empty($hasil['status']) ? $hasil['status'] : ''
        );
        //$this->myDebug($result);
        return $result;
    }

    function get_data_affiliate($id_affiliate) {
        $this->db->select('id_user, nama');
        $this->db->where('id_user', $id_affiliate);
        $this->db->order_by('id_user', 'DESC');
        $hasil = $this->db->get('t_refrensi_member')->row_array();
        $result = array(
            'id_user' => !empty($hasil['id_user']) ? $hasil['id_user'] : '',
            'nama' => !empty($hasil['nama']) ? $hasil['nama'] : ''
        );
        //$this->myDebug($result);
        return $result;
    }

    function get_data($inv) {
        //get id_user from addtional_cost
        $addcost = $this->Operasional_model->get_data_invoice($inv);
        $affiliate = $this->get_data_affiliate($addcost['id_user']);
        $hasil = array(
            'invoice' => $inv,
            'affiliate' => $affiliate['nama']// . ' - ' . $affiliate['nama_affiliate']
        );
        //$this->myDebug($hasil);
        echo json_encode($hasil);
    }

    function ajax_cabang() {
        $list = $this->Operasional_model->get_data_cabang();

        $data = "<option value=''>- Pilih Cabang -</option>";
        foreach ($list->result() as $value) {
            $data .= "<option value='" . $value->id_user . "'>" . $value->nama . ' - ' . $value->nama_affiliate . "</option>";
        }
        echo $data;
    }

    public function update_data1() {
        $data = $this->input->post();
        //$this->myDebug($data);

        $data_update = array(
            'invoice' => $data['invoice'],
            'id_user' => $data['cabang']
        );
        if (!$this->Operasional_model->update_manasik($data_update)) {
            throw new Exception('Error ketika update data registrasi ustadz');
        }
        redirect($this->folder . '/Operasional_manasik');
    }

    public function update_data() {
        //$data = $this->input->post();
        $invoice = $this->uri->segment(4);
        $id_user = $this->uri->segment(5);
        $data = array('invoice' => $invoice, 'id_user' => $id_user);
        //$this->myDebug($data);
        $send = $this->Operasional_model->update_manasik($data);
    }

}
