<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Operasional Handling</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form name="form1" action="<?php echo base_url(); ?>" method="post">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th>NO.</th>
                                            <th>DATA HANDLING</th>
                                            <th>JML JAMAAH</th>
                                            <th>BIAYA</th>
                                            <th>KETERANGAN</th>
                                            <th>ACTION</th>
    <!--                                        <th><input type="checkbox" id="checkAll" name="checkAll"> All</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--Appended by Ajax-->
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="pull-right">
                                <ul class="pagination"></ul>    
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        //datatables
        table = $('#data-table').DataTable({
            "processing": true,
            "ajax": {
                "url": "<?php echo site_url('finance/operasional_handling/ajax_list') ?>",
                "type": "POST"
            },
        });
    });

    function konfirmasi(invoice) {
        if (confirm('Apakah Anda Akan Approve Data Ini (' + invoice + ') '))

        {
            $.ajax({
                url: "<?php echo site_url('finance/operasional_handling/approval') ?>/" + invoice,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();
                }
            });
        }
    }
</script>