
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        <form   class="form-horizontal" role="form"  >
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Jamaah Aktif</b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                   
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="#<?php echo $booking['invoice']?>" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Affiliate</label>
                        <input name="affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?> <?php echo $booking['affiliate']?> " readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Paket</label>
                         <input type="text" name="paket" class="form-control" value="<?php echo $booking['paket']?> dengan Waktu tunggu <?php echo $booking['bulan_menunggu']?> Bulan " readonly="readonly" >
                      </div>
                      <div class="col-sm-4">
                        <label >Tgl Keberangkatan</label>
                        <input name="tgl_keberangkatan" class="form-control" value="<?php echo $booking['tgl_keberangkatan']?>  " readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Embarkasi</label>
                        <input name="departure" class="form-control" value="<?php echo $booking['departure']?>  " readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Jmlh Jamaah</label>
                        <input name="jumlah_jamaah" class="form-control" value="<?php echo $booking['jumlah_jamaah']?>  Orang" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Quard</label>
                        <input name="quard_2" class="form-control" value="<?php echo $booking['quard_2']?>  Kamar" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Double</label>
                        <input name="double_2" class="form-control" value="<?php echo $booking['double_2']?>  Kamar" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Triple</label>
                        <input name="triple_2" class="form-control" value="<?php echo $booking['triple_2']?>  Kamar" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Harga Quard</label>
                        <input name="harga_quard_2" class="form-control" value="Rp <?php echo number_format($booking['harga_quard_2'])?> " readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Harga Double</label>
                        <input name="harga_double_2" class="form-control" value="Rp <?php echo number_format($booking['harga_double_2'])?> " readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label >Harga Triple</label>
                        <input name="harga_Triple_2" class="form-control" value="Rp <?php echo number_format($booking['harga_Triple_2'])?> " readonly="readonly">
                      </div>
                        <div class="col-sm-4">
                        <label >Total Harga Kamar</label>
                        <input name="Room_Price_2" class="form-control" value="Rp <?php echo number_format($booking['Room_Price_2'])?> " readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Total Harga Kamar</label>
                        <input name="refund" class="form-control" value="Rp <?php echo number_format($booking['refund'])?> " readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                        <label >FEE</label>
                        <input name="fee" class="form-control" value="Rp <?php echo number_format($booking['fee'])?> " readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Fee Input</label>
                        <input name="fee_input" class="form-control" value="Rp <?php echo number_format($booking['fee_input'])?> " readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Muhrim</label>
                        <input name="muhrim" class="form-control" value="Rp <?php echo number_format($booking['muhrim'])?> " readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Harga Satuan</label>
                        <input name="harga" class="form-control" value="Rp <?php echo number_format($booking['harga'])?> " readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label >Total Bayar</label>
                        <input name="Total_Bayar" class="form-control" value="Rp <?php echo number_format($booking['Total_Bayar'])?> " readonly="readonly">
                      </div>
               
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>

                 <div class="panel-body">
                     <span class="input-group-btn">
              
                     <!-- <a href="'.site_url('finance/booking_report/payment_reporting').'/' . $booking['id_booking'] . '"  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank"> <i class="glyphicon glyphicon-print"></i> Cetak Detail</a> -->
                      <?php
                          echo '<a href="'.site_url().'booking_report" class="btn btn-sm btn-danger" title="Kembali"> <i class="glyphicon glyphicon-circle-arrow-left"></i> Kembali </a>
                          
                          <a href="'.site_url('finance/booking_report/export_reporting_payment').'/' . $booking['id_booking'] . '"  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank"> <i class="glyphicon glyphicon-print"></i> Cetak Detail</a>
                         
                          ';
                        ?>
                        <!-- <td><a href="'.base_url().'finance/booking_report/detail/'.$row->id_booking.'" class="btn-sm btn-success" title="Detail Reporting">Detail</a> -->
                           <!-- <button type='submit'  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank" ><i class="glyphicon glyphicon-print"></i> Export Excell</button> -->
                          </span>
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                   
                                    <th>Nama Jamaah</th>
                                   <th>ID Jamaah </th>
                                   <th>Paket</th>
                                    <th>Category</th>
                                    <th>Room Type</th>
                                    <th>Muhrim</th>
                                    <th>refund</th>
                                  <th>Fee </th>
                                 
                                  <th>Fee Input </th>
                                   <th>Status </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                     
                                        <td><?php echo $pi['nama']?></td>
                                        <td><?php echo $pi['id_jamaah']?></td>
                                        
                                        <td><?php echo $pi['paket']?> </td>
                                        <td><?php echo $pi['category']?> </td>
                                        <td><?php echo $pi['room_type']?> </td>
                                        <td><?php echo number_format($pi['muhrim'])?></td>
                                        <td><?php echo number_format($pi['refund'])?></td>
                                      
                                        <td><?php echo number_format($pi['fee'])?></td>
                                        <td><?php echo number_format($pi['fee_input'])?></td>
                                        
                                        <td><?php echo $pi['status']?> </td>
                                  
                                  
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
