<div class="page-head">
    <h2>OPERASIONAL MANASIK</h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">FINANCE</a></li>
        <li class="active">Operasional Manasik</li>
    </ol>
</div>      

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Operasional Manasik</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form role="form">
                        <div class="form-group input-group col-lg-4">
                            

                            <input type="text" class="form-control" id="search" placeholder="MASUKAN NO INVOICE" x-webkit-speech>
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="check"><i class="fa fa-search"></i></button>
                            </span>

                        </div>
                    </form>
                    
                    <div class="table-responsive">
                        <table id="data-table" class="table table-bordered table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>NO</ath>
                                    <th>INVOICE</th>
                                    <!--<th>ID USER</th>-->
                                    <th>AFFILIATE</th>
                                    <th>CABANG</th>
                                    <th>JML</th>
                                    <th>BIAYA</th>
                                    <th>KETERANGAN</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody id='response-table'>
                                <tr><td colspan="8"><h2 style="color: #f5b149">Cari Invoice</h2></td></tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>      -->                

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->



<div class="modal fade" id="md-custom" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="POST" id="form_mutasi" name="form_mutasi" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Data Operasional Manasik</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>NO. INVOICE </b></label>
                                <input type="text" name="invoice" id="invoice" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>AFFILIATE</b></label>
                                <input type="text" name="affiliate" id="affiliate" class="form-control" readonly="">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>PINDAH CABANG</b></label>
                                <select class="form-control" id="cabang" name="cabang">
                                    <option value="">- Pilih Cabang -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<a class="btn btn-sm btn-primary" href="javascript:void()" onclick="delete_data()">Save</a>-->
                        <button id="btnsave" type="button" class="btn btn-primary" onclick="update_data()">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<!--</div>-->

<script type="text/javascript">
    $(function () {
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });

</script>

<script type="text/javascript">
    var table;
    $(document).ready(function () {

//        table = $('#example1').DataTable({
//            "ajax": {
//                "url": "<?php echo site_url('finance/operasional_manasik/ajax_list') ?>",
//            },
//            "columnDefs": [
//                {
//                    "targets": [0], //first column / numbering column
//                    "orderable": false, //set not orderable
//                },
//            ],
//        });

        var url = "<?php echo base_url('finance/operasional_manasik/ajax_cabang') ?>";
        $("#cabang").load(url);
    });

    function edit_pindah_cabang(inv) {
        var link_rm = "<?php echo site_url('finance/operasional_manasik/get_data') ?>";
        link_rm = link_rm + "/" + inv;
        $.get(link_rm, function (data) {
            $('#invoice').val(data.invoice);
            $('#affiliate').val(data.affiliate);
        }, "json");
        $('#md-custom').modal('show');
    }

    function update_data1() {
        $('#form_mutasi').submit();
    }

    function update_data()
    {
        if (confirm('Apakah Anda Akan Pindah Data Ini? '))
        {
            var invoice = $("#invoice").val();
            var cabang = $("#cabang").val();
            //alert('invoice'+invoice);
            $.ajax({
                url: "<?php echo site_url('finance/operasional_manasik/update_data') ?>/" + invoice + "/" + cabang,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //alert('success');
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    //alert('error');
                    $('#md-custom').modal('hide');
                    table.ajax.reload();
                }
            });
        }
    }

</script>

<script>

    $("#check").on('click', function (e) {
        e.preventDefault();
        var search = $("#search").val();
        //alert(search);
        // var departure = $("#departure").val();
        // var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
        // var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
        console.log(search);
        // console.log(departure);
        // console.log(datepicker_tahun_keberangkatan);
        // console.log(datepicker_keberangkatan);

        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>finance/operasional_manasik/ajax_list',
            // data:'from='+from+'&to='+to
            data: 's=' + search
                    // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data) {
            $("#response-table").html(data);
        });

    });
</script>

