                         <?php 
  
                                     header("Content-type: application/vnd.ms-excel");
                                     header("Content-Disposition: attachment; filename=laporan_detail_payment_reporting.xls");

                                  ?> 
 

          <table border=1 bordercolor="#000000">
            <tr><td>Invoice</td></td><td>#<?php echo $booking['invoice']?></td></tr>
            <tr><td>Affiliate</td></td><td><?php echo $booking['affiliate']?></td></tr>
            <tr><td>Paket</td></td><td><?php echo $booking['paket']?> dengan Waktu tunggu <?php echo $booking['bulan_menunggu']?></td></tr>
            <tr><td>Tgl Keberangkatan</td></td><td><?php echo $booking['tgl_keberangkatan']?></td></tr>
            <tr><td>Embarkasi</td></td><td><?php echo $booking['departure']?></td></tr>
            <tr><td>Jmlh Jamaah</td></td><td><?php echo $booking['jumlah_jamaah']?></td></tr>
            <tr><td>Quard</td></td><td><?php echo $booking['quard_2']?></td></tr>
            <tr><td>Double</td></td><td><?php echo $booking['double_2']?></td></tr>
            <tr><td>Triple</td></td><td><?php echo $booking['triple_2']?></td></tr>
            <tr><td>Harga Quard</td></td><td><?php echo $booking['harga_quard_2']?></td></tr>
            <tr><td>Harga Double</td></td><td><?php echo $booking['harga_double_2']?></td></tr>
            <tr><td>Harga Triple</td></td><td><?php echo $booking['harga_Triple_2']?></td></tr>
            <tr><td>Total Harga Kamar</td></td><td><?php echo $booking['Room_Price_2']?></td></tr>
            <tr><td>Total Cast Back</td></td><td><?php echo $booking['refund']?></td></tr>
            <tr><td>Total Fee</td></td><td><?php echo $booking['fee']?></td></tr>
            <tr><td>Total Fee Input</td></td><td><?php echo $booking['fee_input']?></td></tr>
            <tr><td>Total Muhrim</td></td><td><?php echo $booking['muhrim']?></td></tr>
            <tr><td>Harga Paket</td></td><td><?php echo $booking['harga']?></td></tr>
            <tr><td>Total Harga</td></td><td><?php echo $booking['Total_Bayar']?></td></tr>
           
          </table>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
              </br>
            </br>
                 <div class="panel-body">

                          <div class="table-responsive">
                              <table  border=1 class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                   
                                    <th>Nama Jamaah</th>
                                   <th>ID Jamaah </th>
                                   <th>Paket</th>
                                    <th>Category</th>
                                    <th>Room Type</th>
                                    <th>Muhrim</th>
                                    <th>refund</th>
                                  <th>Fee </th>
                                 
                                  <th>Fee Input </th>
                                  <th>Harga Paket </th>
                                   <th>Status </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                     
                                        <td><?php echo $pi['nama']?></td>
                                        <td><?php echo $pi['id_jamaah']?></td>
                                        
                                        <td><?php echo $pi['paket']?> </td>
                                        <td><?php echo $pi['category']?> </td>
                                        <td><?php echo $pi['room_type']?> </td>
                                        <td><?php echo number_format($pi['muhrim'])?></td>
                                        <td><?php echo number_format($pi['refund'])?></td>
                                      
                                        <td><?php echo number_format($pi['fee'])?></td>
                                        <td><?php echo number_format($pi['fee_input'])?></td>
                                        <td><?php echo number_format($pi['harga_paket'])?></td>
                                        <td><?php echo $pi['status']?> </td>
                                  
                                  
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                
              </div>
          </div>
      </div>
    </div>