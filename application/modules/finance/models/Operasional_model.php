<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Operasional_model extends CI_Model{ 
    var $table = 'additional_cost';
    
    public function get_data_manasik($search){
        //
//        $sql = "SELECT a.invoice as invoice, a.id_user, b.nama_affiliate as affiliate, COUNT(*) as jml_jamaah, 
//                SUM(a.jumlah) as biaya_operasional, a.keterangan FROM additional_cost a
//                JOIN affiliate b ON b.id_user = a.id_user
//                WHERE a.status in ('1','1A','2','3','4') AND a.status_manifest = '1' AND a.exe = '1' and a.jns_trans = '155'
//                AND b.id_user LIKE '%GM%'
//                GROUP BY id_booking";    JOIN affiliate b ON b.id_user = a.id_affiliate
        $sql = "SELECT a.invoice, a.id_affiliate, COUNT(*) as jml_jamaah,
                sum(if((`a`.`status_manifest` = '1'),1,0)) as status_manifest, a.status 
                FROM registrasi_jamaah as a
                where a.`status` in ('1','2','3','4') AND a.id_affiliate LIKE '%GM%' AND a.invoice = '".$search."' 
                GROUP BY a.id_booking";
        return $this->db->query($sql)->result();
    }
    
    function get_data_invoice($inv){
        //$this->db->join('affiliate','affiliate.id_user = additional_cost.id_user');
        return $this->db->get_where('additional_cost',array('invoice'=>$inv))->row_array();
    }
    
    function get_data_cabang(){
        $this->db->like('id_user', 'C');
        $hasil = $this->db->get('affiliate');
        return $hasil;
    }
    
    function update_manasik($data){
        $arr = array(
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user'),
            'id_user' => $data['id_user'],
        );
        $param = array(
            'invoice' => $data['invoice'],
            'jns_trans' => '155',
        );
        return $this->db->update('additional_cost', $arr, $param);
    }
    
    public function save($data, $data2 = '') {


        $arr = array(
            'id_booking' => $data['id_booking'],
            'id_affiliate' => $data['sponsor'],
            // 'id_sponsor' => $data['sponsor'],
            'id_schedule' => $data['id_schedule'],
            'id_product' => $data['id_product'],
            'transfer_date' => $data['transfer_date'],
            'dana_bank' => $data['dana_bank'],
            'bank_transfer' => $data['bank_transfer'],
            'nama_rek' => $data['nama_rek'],
            'no_rek' => $data['no_rek'],
            'nominal' => $data['fee'],
            'keterangan' => $data['keterangan'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'status' => 1,
                // 'status_feefree'=>2,
        );

        $this->db->trans_begin(); //transaction initialize

        $this->db->insert($this->_claim_feefree, $arr);
        $id_claim_feefree = $this->db->insert_id();

        //  $sql = "UPDATE fee SET status_fee_sponsor = '3',id_claim='".$id_claim_feefree."',update_date='".date('Y-m-d H:i:s')."',update_by='".$this->session->userdata('id_user')."' WHERE id_registrasi IN ($data2)";
        // $update_data = $this->db->query($sql);
        // $update_data;

        $id_registrasi = $this->input->post('id_registrasi');
        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_registrasi = $pi['id_registrasi'];
                $tmp = array(
                    'id_registrasi' => $pi['id_registrasi'],
                    'status_fee_sponsor' => 4,
                    'proses_feesponsor' => 'TRANSFERRED',
                    'id_claimsponsor' => $id_claim_feefree,
                    'update_date' => date('Y-m-d H:i:s'),
                    'update_by' => $this->session->userdata('id_user'),
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('fee', $pic, 'id_registrasi');
        }
        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            return false;
        } else {

            $this->db->trans_complete();

            return true;
        }
    }

    public function get_pic($id_booking, $sponsor = '') {

        // $sql ="SELECT * from detail_fee_posting_sponsor WHERE  id_booking  = ? and sponsor = ?
        //       ";
        // return $this->db->query($sql,array($id_booking,$sponsor))->result_array();    
        $stored_procedure = "call detail_fee_posting_sponsor(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'sponsor' => $sponsor
                        )
                )->result_array();
    }

    public function get_pic_posting($id_booking, $sponsor = '') {


        $sql = "SELECT * from fee_posting_sponsor_admin where id_booking = '$id_booking' and sponsor = '$sponsor'";
        return $this->db->query($sql)->row_array();
    }

    public function update_pengajuan($id_booking) {

        $arr = array(
            'status_fee_sponsor' => '3',
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user'),
        );
        $this->db->update($this->_table, $arr, array('id_booking' => $id_booking));
    }

    private function _get_datatables_query() {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_data_fee($where = "") {
        // $data = $this->db->query('select * from detail_jamaahnoaktif where id_affiliate="'.$id_user.'" '.$where);
        $data = $this->db->query('select * from fee_posting_sponsor_admin ' . $where);
        return $data;
    }
   
}
