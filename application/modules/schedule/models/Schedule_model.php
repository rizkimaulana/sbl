<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schedule_model extends CI_Model{
    
    private $_table="schedule";
    private $_primary="id_schedule";

    public function save($data){
        
        // $cek = $this->db->select('id_user')->where('username',$data['username'])
        //                 ->or_where('nik',$data['nik'])->get('user')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
            
            // 'kode_product' => $data['kode'],
            'date_schedule' => $data['date_schedule'],
            'time_schedule' => $data['time_schedule'],
            'hari' => $data['select_hari'],
            'seats' => $data['seats'],
            'room' => $data['room'],
            'room_category' => $data['room_category'],
            'embarkasi' => $data['embarkasi'],
            'keterangan' => $data['keterangan'],
            'status_keberangkatan' => $data['select_status_keberangkatan'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => $data['select_status'],
            'maskapai' => $data['select_maskapai'],
            'destination_id' => $data['select_detination'],
            'tipe_jamaah' => $data['select_tipejamaah'],
            'create_by' => $this->session->userdata('id_user')
        );       
        
        return $this->db->insert('schedule',$arr);
    }
    public function update($data){
        
        $arr = array(
        
           
            'kode_product' => $data['kode_product'],
            'date_schedule' => $data['date_schedule'],
            'time_schedule' => $data['time_schedule'],
             'hari' => $data['select_hari'],
            // 'bulan_keberangkatan' => $data["bulan_keberangkatan"],
            'seats' => $data['seats'],
            'room' => $data['room'],
            'room_category' => $data['room_category'],
            'embarkasi' => $data['embarkasi'],
            'keterangan' => $data['keterangan'],
            'status_keberangkatan' => $data['select_status_keberangkatan'],
            'update_date' => date('Y-m-d H:i:s'),
            'status' => $data['select_status'],
            'maskapai' => $data['select_maskapai'],
            'destination_id' => $data['select_detination'],
            'tipe_jamaah' => $data['select_tipejamaah'],
            'update_by' => $this->session->userdata('nama')
        );       
      
                
        return $this->db->update('schedule',$arr,array('id_schedule'=>$data['id_schedule']));
    }
    
    public function get_data($offset,$limit,$q=''){
    
        $sql = "SELECT * from input_schedule
                where 1=1";

        if($q){
            
            $sql .=" AND date_schedule LIKE '%{$q}%'
                    OR time_schedule LIKE '%{$q}%'
                    OR departure LIKE '%{$q}%'
                    OR seats LIKE '%{$q}%'
                    OR type LIKE '%{$q}%'
                    OR category LIKE '%{$q}%'
                    OR keterangan LIKE '%{$q}%'
                    OR status_jadwal LIKE '%{$q}%'
                    OR ket_status LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY id_schedule DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    
    
    public function delete_by_id($id_schedule)
    {
        $this->db->where('id_schedule', $id_schedule);
        $this->db->delete($this->_table);
    }
    
    
}
