<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule extends CI_Controller{
	var $folder = "schedule";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(SCHEDULE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','settings');	
		$this->load->model('schedule_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/schedule');
		
	}
	
	
	
	// private function _status($take=''){
	    
	//     $status = array('8'=>'AGUSTUS');
	//     if($take)
	//         return $status[$take];
	//     return $status;
	// }

	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	   
	}

	private function _select_status_keberangkatan(){
		$kdstatus = array('6', '7');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	   
	}

	private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}

	private function _select_roomtype(){
	
	    return $this->db->get('room_type')->result();
	}
	private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}
	private function _select_maskapai(){
	
	    return $this->db->get('maskapai')->result();
	}
	private function _select_tipejamaah(){
	
	    return $this->db->get('tipe_jamaah')->result();
	}
	private function _select_hari(){
	    
	    return $this->db->get('hari')->result();
	}

	private function _select_detination(){
	
	    return $this->db->get('destinations')->result();
	}
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->schedule_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	               // $rows .='<td width="15%">'.$r->paket.'</td>';
	               
	                $rows .='<td width="13%">'.$r->departure.'</td>';
	                $rows .='<td width="9%">'.$r->date_schedule.'</td>';
	                $rows .='<td width="5%">'.$r->time_schedule.'</td>';
	                // $rows .='<td width="7%">'.$r->bulan_keberangkatan.'</td>';
	                $rows .='<td width="7%">'.$r->seats.'</td>';
	                $rows .='<td width="7%">'.$r->type.'</td>';
	                $rows .='<td width="10%">'.$r->category.' '.$r->hari.' hari</td>';
	                $rows .='<td width="9%">'.$r->keterangan.'</td>';
	                $rows .='<td width="9%">'.$r->status_jadwal.'</td>';
	                $rows .='<td width="9%">'.$r->ket_status.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'schedule/edit/'.$r->id_schedule.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_schedule('."'".$r->id_schedule."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'schedule/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->schedule_model->save($data);
	     if($send)
	        redirect('schedule');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(SCHEDULE,'add'))
		    $this->general->no_access();

	    
	     $data = array(
	     	'select_product'=>$this->_select_product(),
	     	'select_embarkasi'=>$this->_select_embarkasi(),
	     	'select_roomtype'=>$this->_select_roomtype(),
	     	'select_roomcategory'=>$this->_select_roomcategory(),
	     	'select_status'=>$this->_select_status(),
	     	'select_status_keberangkatan'=>$this->_select_status_keberangkatan(),
	     	'select_maskapai'=>$this->_select_maskapai(),
	     	'select_tipejamaah'=>$this->_select_tipejamaah(),
	     	'select_hari'=>$this->_select_hari(),
	     	'select_detination'=>$this->_select_detination()
	     	);
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(SCHEDULE,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('schedule',array('id_schedule'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	    $data = array('select_embarkasi'=>$this->_select_embarkasi(),
	    	'select_roomtype'=>$this->_select_roomtype(),
	     	'select_roomcategory'=>$this->_select_roomcategory(),
	     	'select_status_keberangkatan'=>$this->_select_status_keberangkatan(),
	     	'select_status'=>$this->_select_status(),
	    	'select_product'=>$this->_select_product(),
	    	'select_maskapai'=>$this->_select_maskapai(),
	    	'select_tipejamaah'=>$this->_select_tipejamaah(),
	    	'select_hari'=>$this->_select_hari(),
	    	'select_detination'=>$this->_select_detination()
	    	);
	    				
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->schedule_model->update($data);
	     if($send)
	        redirect('schedule');
		
	}
	
	

	
	public function ajax_delete($id_schedule)
	{
		if(!$this->general->privilege_check(SCHEDULE,'remove'))
		    $this->general->no_access();
		$send = $this->schedule_model->delete_by_id($id_schedule);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('schedule');
	}


}
