 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit Schedule</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>schedule/update">
            <div class="form-group">
                
                <input type="hidden" name="id_schedule" value="<?php echo $id_schedule;?>">
                
            </div>
           <!--   <div class="form-group">
                <label>Pilih Product </label>
                <select class="form-control" name="kode_product">
                    <option></option>
                    <?php foreach($select_product as $sp){ 
                    
                        $selected = ($kode_product == $sp->kode)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $sp->kode;?>" <?php echo $selected;?>><?php echo $sp->nama;?> </option>
                    <?php } ?>
                </select>
            </div> -->
            <div class="form-group">
                <label>Embarkasi</label>
                <select class="form-control" name="embarkasi">
                    <?php foreach($select_embarkasi as $se){ 
                    
                        $selected = ($embarkasi == $se->id_embarkasi)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_embarkasi;?>" <?php echo $selected;?>><?php echo $se->departure;?></option>
                    <?php } ?>
                </select>
            </div>
           <div class="form-group">
            	<label>Date </label>
                <div class='input-group date' id='datetimepicker'>
	                  <input type='text' name="date_schedule" id="date_schedule" class="form-control" value="<?php echo $date_schedule;?>"/>
	                  <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                  </span>
                </div>
            </div>
            <div class="form-group">
	            <label>Time </label>	
				<div class='input-group date' id='timepicker'>
					<input type='text' class="form-control" name="time_schedule" class="form-control" value="<?php echo $time_schedule;?>" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>

            <!-- <div class="form-group">
                <label>Bulan Keberangkatan </label>    
                <div class='input-group date' id='bulankeberangkatan'>
                    <input type='text' class="form-control" name="bulan_keberangkatan" class="form-control" value="<?php echo $time_schedule;?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div> -->
			
	          <div class="form-group">
                <label>Total Seats</label>
                <input class="form-control" name="seats" value="<?php echo $seats;?>"required>
            </div>
            
             <div class="form-group">
                <label>Room</label>
                <select class="form-control" name="room">
                    <?php foreach($select_roomtype as $se){ 
                    
                        $selected = ($room == $se->id_room_type)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_room_type;?>" <?php echo $selected;?>><?php echo $se->type;?></option>
                    <?php } ?>
                </select>
            </div>
          
            <div class="form-group">
                <label>Room Category</label>
                <select class="form-control" name="room_category">
                    <?php foreach($select_roomcategory as $se){ 
                    
                        $selected = ($room_category == $se->id_room_category)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_room_category;?>" <?php echo $selected;?>><?php echo $se->category;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" name="keterangan" value="<?php echo $keterangan;?>"required>
            </div>

      

             <div class="form-group">
                <label>Status Waktu</label>
                <select class="form-control" name="select_status_keberangkatan">
                  

                     <?php foreach($select_status_keberangkatan as $se){ 
                    
                        $selected = ($status_keberangkatan == $se->kdstatus)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->kdstatus;?>" <?php echo $selected;?>><?php echo $se->keterangan;?></option>
                    <?php } ?>
                </select>
            </div>

             <div class="form-group">
                <label>Maskapai</label>
                <select class="form-control" name="select_maskapai">
                  

                     <?php foreach($select_maskapai as $se){ 
                    
                        $selected = ($id_maskapai == $se->id_maskapai)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_maskapai;?>" <?php echo $selected;?>><?php echo $se->nama;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Destination</label>
                <!-- <input class="form-control" name="hari"value="<?php echo $hari;?>"  required> -->
                <select class="form-control" name="select_detination">
                    <option value="">- Select Destination -</option>
                    <?php foreach($select_detination as $sj){ 
                                     $selected = ($destination_id == $sj->destination_id)  ? 'selected' :'';?>
                                    <option value="<?php echo $sj->destination_id;?>" <?php echo $selected;?>><?php echo $sj->destination;?> </option>
                                <?php } ?>
                </select>
            </div>
             <div class="form-group">
                <label>Hari</label>
                <select class="form-control" name="select_hari">
                    <?php foreach($select_hari as $sj){ 
                                     $selected = ($hari == $sj->hari)  ? 'selected' :'';?>
                                    <option value="<?php echo $sj->hari;?>" <?php echo $selected;?>><?php echo $sj->hari;?> </option>
                                <?php } ?>
                </select>
            </div>
             <div class="form-group">
                <label>Tipe Jamaah</label>
                <select class="form-control" name="select_tipejamaah">
                  

                     <?php foreach($select_tipejamaah as $se){ 
                    
                        $selected = ($id_tipe_jamaah == $se->id_tipe_jamaah)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $se->id_tipe_jamaah;?>" <?php echo $selected;?>><?php echo $se->nama;?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Status </label>
                <select class="form-control" name="select_status">
                  
                    <?php foreach($select_status as $st){ 
                                $selected = ($status == $st->kdstatus)  ? 'selected' :'';?>
                                <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                            <?php } ?>
                </select>
            </div>

            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>schedule" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
			$(function () {
				$('#datetimepicker').datetimepicker({
					format: 'YYYY-MM-DD',
                });
				
				$('#datepicker').datetimepicker({
					format: 'DD MMMM YYYY',
				});
				
				$('#bulankeberangkatan').datetimepicker({
					format: 'M',
				});
				
				$('#timepicker').datetimepicker({
					format: 'HH:mm'
				});
			});
		</script>