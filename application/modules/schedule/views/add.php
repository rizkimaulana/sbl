 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Schedule</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>schedule/save">
        	<!-- <div class="form-group">
                <label>Pilih Product</label>
                <select class="form-control" name="kode">
                    <option></option>
                    <?php foreach($select_product as $sp){?>
                        <option required value="<?php echo $sp->kode;?>"><?php echo $sp->nama;?> </option>
                    <?php } ?>
                </select>
            </div> -->

            <div class="form-group">
                <label>Embarkasi</label>
                <select required class="form-control" name="embarkasi">
                    <option></option>
                    <?php foreach($select_embarkasi as $se){?>
                        <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
            	<label>Date </label>
                <div class='input-group date' id='datetimepicker'>
	                  <input type='text' name="date_schedule" id="date_schedule" class="form-control" placeholder="yyyy/mm/dd "required/>
	                  <span class="input-group-addon">
	                    <span class="glyphicon glyphicon-calendar"></span>
	                  </span>
                </div>
            </div>
            <div class="form-group">
	            <label>Time </label>	
				<div required class='input-group date' id='timepicker'>
					<input type='text' class="form-control" name="time_schedule" class="form-control" placeholder="HH:mm " />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>

        
<!-- 
             <div class="form-group">
                <label>Bulan Keberangkatan </label>    
                <div class='input-group date' id='bulantimepicker'>
                    <input type='text' class="form-control" name="bulan_keberangkatan" class="form-control" placeholder="MMMM" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
			 -->
	          <div class="form-group">
                <label>Total Seats</label>
                <input class="form-control" name="seats" required>
            </div>

            <div class="form-group">
                <label>Room Type</label>

                <select required class="form-control" name="room">
                    <option></option>
                    <?php foreach($select_roomtype as $se){?>
                        <option value="<?php echo $se->id_room_type;?>"><?php echo $se->type;?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Room category</label>

                <select required class="form-control" name="room_category">
                    <option></option>
                    <?php foreach($select_roomcategory as $se){?>
                        <option value="<?php echo $se->id_room_category;?>"><?php echo $se->category;?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" name="keterangan" required>
            </div>
            
            <div class="form-group">
                <label>Status waktu</label>
                <select required class="form-control" name="select_status_keberangkatan">
                    <option></option>
                    <?php foreach($select_status_keberangkatan as $se){?>
                        <option value="<?php echo $se->kdstatus;?>"><?php echo $se->keterangan;?></option>
                    <?php } ?>
                </select>
            </div>
             <div class="form-group">
                <label>Maskapai</label>

                <select required class="form-control" name="select_maskapai">
                    <option></option>
                    <?php foreach($select_maskapai as $se){?>
                        <option value="<?php echo $se->id_maskapai;?>"><?php echo $se->nama;?></option>
                    <?php } ?>
                </select>
            </div>
             <div class="form-group">
                <label>Destination</label>
                <!-- <input class="form-control" name="hari" required> -->
                <select class="form-control" name="select_detination">
                     <option></option>
                     <?php foreach($select_detination as $sp){?>
                        <option required value="<?php echo $sp->destination_id;?>"><?php echo $sp->destination;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Lama Hari</label>
                <!-- <input class="form-control" name="hari" required> -->
                <select required class="form-control" name="select_hari">
                     <option></option>
                     <?php foreach($select_hari as $sp){?>
                        <option required value="<?php echo $sp->hari;?>"><?php echo $sp->hari;?> </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Tipe Jamaah</label>

                <select class="form-control" name="select_tipejamaah">
                    <option></option>
                    <?php foreach($select_tipejamaah as $se){?>
                        <option value="<?php echo $se->id_tipe_jamaah;?>"><?php echo $se->nama;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                 <select required class="form-control" name="select_status">
                            <option value="" >- PILIH STATUS -</option>
                            <?php foreach($select_status as $st){?>
                                <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                            <?php } ?>
                        </select>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>schedule" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
			$(function () {
				$('#datetimepicker').datetimepicker({
					format: 'YYYY-MM-DD',
                });
				
				$('#datepicker').datetimepicker({
					format: 'DD MMMM YYYY',
				});
				
				$('#bulantimepicker').datetimepicker({
					format: 'M',
				});
				
				$('#timepicker').datetimepicker({
					format: 'HH:mm'
				});
			});
		</script>