<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jamaahaktifaffiliate_model extends CI_Model{
   
    var $table = 'jamaah_aktif';
    var $column_order = array(null, 'id_registrasi','id_sahabat','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','no_identitas','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','id_user','tipe_jamaah','status','kd_tipe','tgl_aktivasi','update_date','tc_bilyet','id_product','cluster'); //set column field database for datatable orderable
    var $column_search = array('id_affiliate','invoice','id_jamaah','nama','telp','no_identitas','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','id_user','tipe_jamaah','status','kd_tipe','tgl_aktivasi','update_date','id_product','cluster'); //set column field database for datatable searchable 
    var $order = array('id_registrasi' => 'asc'); // default order 

    private $_table="booking";
    private $_primary="id_booking";
    private $_bilyet="bilyet";

public function get_pic($id_booking){
    
       $sql ="SELECT * from detail_jamaahaktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

 public function get_pic_booking($id_booking){
 
        $id_user=  $this->session->userdata('id_user');
         $sql = "SELECT * from view_booking_aktif where id_booking = {$id_booking} and id_user_affiliate='$id_user'
         ";
 	
        return $this->db->query($sql)->row_array();  
    }

public function get_pic_aktif($where= "") {
        $id_user=  $this->session->userdata('id_user');
         $data = $this->db->query('select * from detail_jamaahaktif where id_affiliate="'.$id_user.'" '.$where);
        return $data;
    }
    

      
 function data_jamaahaktif($id_jamaah) {
        $this->db->select('*');
        $this->db->from('detail_jamaahaktif');
        $this->db->where('id_jamaah',$id_jamaah);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }


    function get_biaya_refund() {
        $this->db->select('*');
        $this->db->from('view_refund');
        // $this->db->where('tampil', 'Y');
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }


   






    public function get_data($offset,$limit,$q=''){
    
        $id_user=  $this->session->userdata('id_user');
          $sql = " SELECT * from view_booking_aktif where id_user_affiliate='".$id_user."' 
                    ";
        
        if($q){
            
            $sql .=" AND affiliate LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_booking DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     

    function get_nama_jamaah() {
        $id_user=  $this->session->userdata('id_user_affiliate');
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status', '0');
        $this->db->order_by('id_registrasi', 'ASC');
        $this->db->where('id_affiliate',$id_user);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    //     function get_data_jamaah($id) {
    //     $this->db->select('*');
    //     $this->db->from('view_lap_pembayaran');
    //     $this->db->where('id_booking',$id);
    //     $query = $this->db->get();
    //     if($query->num_rows() > 0){
    //         $out = $query->row();
    //         return $out;
    //     } else {
    //         return FALSE;
    //     }
    // }


     function lap_data_jamaah($id_booking) {
        $id_user=  $this->session->userdata('id_user_affiliate');
        $this->db->select('*');
        $this->db->from('view_booking_aktif');
        $this->db->where('id_booking',$id_booking);
        $this->db->where('id_affiliate',$id_user);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }



    function get_data_booking($id_booking) {
        $id_user=  $this->session->userdata('id_user_affiliate');
        $this->db->select('*');
        $this->db->from('view_booking_aktif');
        $this->db->where('id_booking',$id_booking);
        $this->db->where('id_affiliate',$id_user);
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_jamaahaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahaktif');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        } else {
            return FALSE;
        }
    }
    
     public function get_jamaah_aktif($offset,$limit,$q=''){
    
        $id_user=  $this->session->userdata('id_user');
          $sql = "SELECT * FROM jamaah_aktif
                   where (1=1) and id_affiliate='".$id_user."' ";
        
        if($q){
            
            $sql .=" AND invoice LIKE '%{$q}%'
                    OR id_jamaah LIKE '%{$q}%'
                     OR nama LIKE '%{$q}%'
                     OR id_affiliate LIKE '%{$q}%'
                     OR telp LIKE '%{$q}%'   
                     OR tgl_daftar LIKE '%{$q}%' 
                     OR paket LIKE '%{$q}%'
                     OR bulan_menunggu LIKE '%{$q}%'   
                      
                ";
        }
        $sql .=" ORDER BY tgl_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    public function save_cetak_bilyet($data){
         $total_cetak_bilyet = $this->input->post('total_cetak_bilyet');

        if ($total_cetak_bilyet <= 0){ 
        $arr = array(
            'id_jamaah' => $data['id_jamaah'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['select_user'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
        
          // $this->db->insert('bilyet',$arr);
        $this->db->trans_begin(); 
        
        $this->db->insert($this->_bilyet,$arr);
        // $id_jamaah =  $this->db->insert_id(); 

           if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
 
            return true;

        }

    }else{
      $arr = array(
            'id_jamaah' => $data['id_jamaah'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['select_user'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user'),
        );       
        
              $this->db->update($this->_bilyet,$arr,array('id_jamaah'=>$data['id_jamaah']));
        // $id_jamaah =  $this->db->insert_id(); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            $id_jamaah = $this->input->post('id_jamaah');
            redirect('data_jamaahaffiliate/bilyet/cetak/'.$id_jamaah.'');
            return true;

        }


    }


    
}
    
     private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->where('id_affiliate', $id_user);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
