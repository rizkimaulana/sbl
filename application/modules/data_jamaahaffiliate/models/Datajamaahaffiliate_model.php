<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datajamaahaffiliate_model extends CI_Model{
   
         var $table = 'data_jamaah_belumaktif_affiliate';
    var $column_order = array(null, 'id_registrasi','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','no_identitas','tempat_lahir','umur','tanggal_lahir','email','id_kelamin','id_rentang_umur','id_status_diri','id_ket_keberangkatan','ahli_waris','hub_waris','id_merchandise','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','tipe_jamaah','alamat','kelamin','namaprovinsi','namakabupaten','namakecamatan','status_jamaah','status','harga_paket','muhrim','refund','handling','create_by'); //set column field database for datatable orderable
    var $column_search = array('id_registrasi','id_booking','id_affiliate','invoice','id_jamaah','nama','telp','no_identitas','tempat_lahir','umur','tanggal_lahir','email','id_kelamin','id_rentang_umur','id_status_diri','id_ket_keberangkatan','ahli_waris','hub_waris','id_merchandise','tgl_daftar','paket','category','date_schedule','bulan_menunggu','bulan_keberangkatan','departure','tipe_jamaah','alamat','kelamin','namaprovinsi','namakabupaten','namakecamatan','status_jamaah','status','harga_paket','muhrim','refund','handling','create_by'); //set column field database for datatable searchable 
    var $order = array('id_registrasi' => 'asc'); // default order 

    private $_table="booking";
    private $_primary="id_booking";

     private $_registrasi_jamaah="registrasi_jamaah";
     private $_id_registrasi="id_registrasi";




public function update($data){
         
    $arr = array(
                // 'id_booking'=> $data['id_booking'],
                
                'nama'=> $data['nama'],
                'nama_passport'=> $data['nama_passport'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'no_identitas'=> $data['no_identitas'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
                'kelamin'=> $data['select_kelamin'],
                'status_diri'=> $data['select_statuskawin'],
                'rentang_umur'=> $data['select_rentangumur'],

                'ket_keberangkatan'=>$data['select_status_hubungan'],
                'hubkeluarga'=> $data['hubungan'],
                'keluarga'=> $data['select_keluarga'],

                 'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],

                
                'merchandise'=> $data['select_merchandise'],
                'no_pasport'=> $data['no_pasport'],
                 'issue_office'=> $data['issue_office'],
                'isui_date'=> $data['isui_date'],

                'update_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                
                'status_identitas'=> $data['select_status_identitas'],
                'status_kk'=> $data['select_status_kk'],
                'status_photo'=> $data['select_photo'],
                'status_pasport'=> $data['select_pasport'],
                'status_vaksin'=> $data['select_vaksin'],
                 'status_buku_nikah'=> $data['select_buku_nikah'],
              


        );   

         if(isset($data['pic1'])){
            
            $arr['pic1'] = $data['pic1'];
        }
        if(isset($data['pic2'])){
            
            $arr['pic2'] = $data['pic2'];
        }
        if(isset($data['pic3'])){
            
            $arr['pic3'] = $data['pic3'];
        }
        if(isset($data['pic4'])){
            
            $arr['pic4'] = $data['pic4'];
        }
        if(isset($data['pic5'])){
            
            $arr['pic5'] = $data['pic5'];
        }
         if(isset($data['pic6'])){
            
            $arr['pic6'] = $data['pic6'];
        }
                    
         if($data['provinsi_id']!='' && $data['kecamatan_id']!='' && $data['kabupaten_id']!='') {
          
            // $arr['password'] = ($data['password']);
            $arr['provinsi_id'] = ($data['provinsi_id']);
            $arr['kecamatan_id'] = ($data['kecamatan_id']);
            $arr['kabupaten_id'] = ($data['kabupaten_id']);

        }
         $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));    


    
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
}

 public function get_pic($id_booking){
         $id_user=  $this->session->userdata('id_user');
        $sql ="SELECT * from detail_jamaahnoaktif WHERE  id_booking  = ?  
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

 // public function get_pic_booking($id_booking){
 
 //        $id_user=  $this->session->userdata('id_user');
 //         $sql = "SELECT * from view_lap_pembayaran where id_booking = {$id_booking} and id_user_affiliate='$id_user'
 //                ";
 //        return $this->db->query($sql)->row_array();  
 //    }

public function get_pic_belumaktif($where= "") {
         $id_user=  $this->session->userdata('id_user');
         $data = $this->db->query('select * from detail_jamaahnoaktif where id_affiliate="'.$id_user.'" '.$where);
        return $data;

     
    
    }
      
  public function get_jamaah_belum_aktif($offset,$limit,$q=''){
    
        $id_user=  $this->session->userdata('id_user');
          $sql = "SELECT * FROM data_jamaah_belum_aktif  where id_affiliate='".$id_user."'
                    ";
        
        if($q){
            
            $sql .=" AND invoice LIKE '%{$q}%'
                     OR id_jamaah LIKE '%{$q}%'
                     OR nama LIKE '%{$q}%'
                     OR id_affiliate LIKE '%{$q}%'
                     OR telp LIKE '%{$q}%'   
                     OR tgl_daftar LIKE '%{$q}%' 
                     OR paket LIKE '%{$q}%'
                     OR bulan_menunggu LIKE '%{$q}%'   
                     OR create_by LIKE '%{$q}%' 
                ";
        }
        $sql .=" ORDER BY tgl_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }


    // public function get_data($offset,$limit,$q=''){
    
    //     $id_user=  $this->session->userdata('id_user');
    //       $sql = " SELECT * from view_lap_pembayaran where id_user_affiliate='".$id_user."'
    //                 ";
        
    //     if($q){
            
    //         $sql .=" AND affiliate LIKE '%{$q}%'
    //                 OR invoice like '%{$q}%'
    //         ";
    //     }
    //     $sql .=" ORDER BY id_booking DESC ";
    //     $ret['total'] = $this->db->query($sql)->num_rows();
        
    //         $sql .=" LIMIT {$offset},{$limit} ";
        
    //     $ret['data']  = $this->db->query($sql)->result();
       
    //     return $ret;
        
    // }

    
    function lap_data_jamaah($id_booking) {
        $id_user=  $this->session->userdata('id_user');
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id_booking);
        $this->db->where('id_user_affiliate',$id_user);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }



    function get_data_booking($id_booking) {
         $id_user=  $this->session->userdata('id_user');
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id_booking);
        $this->db->where('id_user_affiliate',$id_user);
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_jamaahnoaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result();
        } else {
            return FALSE;
        }
    }
    

     function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    
    function get_kabupaten() {
    
    $query = $this->db->get('wilayah_kabupaten');
    return $query->result();
    
    }
    
    function get_kecamatan()    {
    
    $query = $this->db->get('wilayah_kecamatan');
    return $query->result();
    
    }

    private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->where('create_by', $id_user);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
