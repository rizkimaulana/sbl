<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jamaah_aktifaffiliate extends CI_Controller{
	var $folder = "data_jamaahaffiliate";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(DATA_JAMAAH_AKTIF_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','jamaah_aktifaffiliate');	
		$this->load->model('jamaahaktifaffiliate_model');
		$this->load->model('jamaahaktifaffiliate_model','r');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/jamaah_aktifaffiliate');
		
	}

	 public function data_jamaahaktif(){
	
	 //   $data = array(
		// 	'pic' => $this->jamaahaktifaffiliate_model->get_pic_aktif('order by id_jamaah desc')->result_array(),
		// );
	   $this->template->load('template/template', $this->folder.'/data_jamaah_aktif');
		
	}

	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;

  			$sql = "SELECT * from  t_refrensi_member where id_user ='".$r->cluster."'
                     ";
                    $query = $this->db->query($sql)->result_array();
                    foreach($query as $key=>$value){
                      $namaaffiliate = $value['nama'];
                      $provinsi = $value['provinsi'];
                      $kabupaten = $value['kabupaten'];
                    }

	                $row[] ='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 ID SAHABAT :  '.$r->id_sahabat.'<br>
	             			 Telp : '.$r->telp.'<br>
	             			 Embarkasi : '.$r->departure.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $row[] ='<td width="18%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $row[] ='<td width="18%" >Tgl Daftar : '.$r->tgl_daftar.'<br>
	             			 MANIFEST MASUK KE <br>CLUSTER CABANG : '.$namaaffiliate.' <br> '.$kabupaten.'
	             			
	               			 </td>';
	                			
	               

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

    public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->jamaahaktifaffiliate_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="10%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->paket.'</td>';
	                $rows .='<td width="10%">'.$r->departure.'</td>';
	                $rows .='<td width="20%">'.$r->tgl_daftar.'</td>';
	                $rows .='<td width="10%">'.$r->tgl_keberangkatan.'</td>';
	                $rows .='<td width="10%">'.$r->jumlah_jamaah.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'data_jamaahaffiliate/jamaah_aktifaffiliate/detail/'.$r->id_booking.'">
	                            <i class="fa fa-pencil"></i> Detail
	                        </a> ';
	                  $rows .='<a class="btn btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'data_jamaahaffiliate/cetak_jamaahaktif/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'data_jamaahaffiliate/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	  public function get_jamaah_aktif(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->jamaahaktifaffiliate_model->get_jamaah_aktif($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="3%" >'.$i.'</td>';
	                // $rows .='<td width="10%" style="vertical-align:middle"><img src="'.base_url('data_jamaah/jamaah_aktif/gambar/'.$r->id_jamaah.'?height=80&width=1').'"></td>';
	                $rows .='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $rows .='<td width="18%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $rows .='<td width="18%" >Tgl Daftar : '.$r->tgl_daftar.'<br>
	             			 Tgl Aktivasi : '.$r->tgl_aktivasi.'<br>
	             			
	               			 </td>';
	                			
	                // $rows .='<td width="18%">'.$r->id_user.'<br>
	             			 

	                		// </td>';
	               

	              // if ($r->id_affiliate_type== 1)
	              // { 
	              // 	 $rows .='<td width="18%" align="center">';
	              //   $rows .='<a class=" btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'data_jamaahaffiliate/jamaah_aktifaffiliate/bilyet/'.$r->id_jamaah.'">
	              //   		 Cetak Bilyet</a>';
	              //  $rows .='</td>';
	              // }
	                // $rows .='<a class=" btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'bilyet/bilyet_barcode/detail_bilyet_barcode/'.$r->id_jamaah.'">
	                // 		 Bilyet2</a>';

	               	
	            
	               
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging_jamaah_aktif($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="10">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging_jamaah_aktif($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'data_jamaahaffiliate/jamaah_aktifaffiliate/get_jamaah_aktif/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	
	 public function detail(){
	


	    if(!$this->general->privilege_check_affiliate(JAMAAH_AKTIF_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	    $booking = $this->jamaahaktifaffiliate_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	        $pic = $this->jamaahaktifaffiliate_model->get_pic($id_booking);
	    }    

	      
	    $data = array(

	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahaktif',($data));

	}
	


	 public function bilyet(){
	


	    if(!$this->general->privilege_check_affiliate(DATA_JAMAAH_AKTIF_FOR_AFFILIATE,'add'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(4);
	    $get = $this->db->get_where('data_jamaah_aktif',array('id_jamaah'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   			 $data = array(
	   			 	// 'select_user'=>$this->_select_user(),
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/cetak_bilyet',array_merge($get,$data));

	}


	public function save_cetak_bilyet(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->jamaahaktifaffiliate_model->save_cetak_bilyet($data);
	     if($send)
	         // $id_jamaah = $this->input->post('id_jamaah');
          //   redirect('data_jamaah/bilyet/cetak/'.$id_jamaah.'');
	     	
	     	$id_jamaah = $this->input->post('id_jamaah');
            redirect('bilyet_affiliate/bilyet_barcode/detail_bilyet_barcode/'.$id_jamaah.'');
	}
	



}
