<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bilyet_model extends CI_Model{
   
    private $_primary="id_cetak_bilyet";
    private $_table="bilyet";

  public function update($data){
        // $id_jamaah= $this->uri->segment(4,0);

        $arr = array(
            'id_jamaah' => $data['id_jamaah'],
            'keterangan' => $data['keterangan'],
            'id_user' => $data['select_user'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
        );       
        
              $this->db->update($this->_table,$arr,array('id_jamaah'=>$data['id_jamaah']));
        // $id_jamaah =  $this->db->insert_id(); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();

            return true;

        }
    }


   

    public function get_pic_bilyet($id_jamaah){
    
       $sql ="SELECT * from data_jamaah_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

    }

   
       public function get_data($offset,$limit,$q=''){
    
        $id=  $this->session->userdata('id_user');
          $sql = "SELECT * FROM view_bilyet
                   where (1=1) ";
        
        if($q){
            
            $sql .=" AND id_jamaah LIKE '%{$q}%'
                    OR affiliate LIKE '%{$q}%'
                    OR user LIKE '%{$q}%'
                    OR keterangan LIKE '%{$q}%'
                    OR menerima LIKE '%{$q}%'
                     OR id_user LIKE '%{$q}%'
                     OR create_by LIKE '%{$q}%'
                     OR create_date LIKE '%{$q}%'   
                     
                ";
        }
        $sql .=" ORDER BY create_date DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }


 public function get_pic_detail_bilyet($id_jamaah){
 

         $sql = "SELECT * from data_jamaah_aktif where id_jamaah = '$id_jamaah'";
  
        return $this->db->query($sql)->row_array();  
    }
    
    public function get_pic_log_bilyet($id_jamaah){
    
       $sql ="SELECT * from view_log_bilyet WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_jamaah))->result_array();    

}
   
}
