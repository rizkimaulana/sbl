<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bilyet_barcode extends CI_Controller{
	var $folder = "bilyet";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(INFO_BILYET,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','bilyet');	
		$this->load->model('bilyet_model');
		//$this->load->helper('fungsi');
		$this->load->library('terbilang');
		$this->load->library('ci_qr_code');
        $this->config->load('qr_code');
	}
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/bilyet');
		
	}

	public function bilyet_barcode(){
	
		
	   // $this->template->load('template/template', $this->folder.'/bilyet_barcode');
		// $this->load->view('bilyet_barcode');
		
	}
		public function cetak(){
	
		
	   // $this->template->load('template/template', $this->folder.'/bilyet_barcode');
		$this->load->view('cetak');
		
	}

    function pdf_bilyet_barcode(){
    	
        // load dompdf
        $this->load->helper('dompdf');
        //load content html
        $html = $this->load->view('bilyet_barcode', '', true);
        // create pdf using dompdf
        $filename = 'bilyet_barcode';
        $paper = 'A4';
        $orientation = 'potrait';
        pdf_create($html, $filename, $paper, $orientation);
    }
     function gambar($kode){

			   $height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '25';  $width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst

			 $this->load->library('zend');

			        $this->zend->load('Zend/Barcode');

			  $barcodeOPT = array(

			     'text' => $kode,

			     'barHeight'=> $height,

			     'factor'=>$width,

			 );

			  $renderOPT = array();

			 $render = Zend_Barcode::factory(

			'code128', 'image', $barcodeOPT, $renderOPT

				)->render();
	}
 



	public function detail_bilyet_barcode(){

	   
	    
	    $id_jamaah = $this->uri->segment(4);
	    $bilyet = $this->bilyet_model->get_pic_detail_bilyet($id_jamaah);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 // 'pic'=>$pic
	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/bilyet_barcode',($data));
	    $this->load->view('bilyet_barcode',($data));
	}

	
public function detail_bilyet_barcode_pdf(){

	    $this->load->helper('dompdf');
	    
	    $id_jamaah = $this->uri->segment(4);
	    $bilyet = $this->bilyet_model->get_pic_detail_bilyet($id_jamaah);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 // 'pic'=>$pic
	       		 );
	

	   $html = $this->load->view('bilyet_barcode', $data, true);
        // create pdf using dompdf
        $filename = 'bilyet_barcode';
        $paper = 'A4';
        $orientation = 'potrait';
        pdf_create($html, $filename, $paper, $orientation);
	}
	
	function print_qr($id_jamaah)
    {
        $qr_code_config = array();
        $qr_code_config['cacheable'] = $this->config->item('cacheable');
        $qr_code_config['cachedir'] = $this->config->item('cachedir');
        $qr_code_config['imagedir'] = $this->config->item('imagedir');
        $qr_code_config['errorlog'] = $this->config->item('errorlog');
        $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
        $qr_code_config['quality'] = $this->config->item('quality');
        $qr_code_config['size'] = $this->config->item('size');
        $qr_code_config['black'] = $this->config->item('black');
        $qr_code_config['white'] = $this->config->item('white');
        $this->ci_qr_code->initialize($qr_code_config);

        // get full name and user details
         $id_jamaah = $this->uri->segment(4);
	    $bilyet = $this->bilyet_model->get_pic_detail_bilyet($id_jamaah);
        $image_name = $id_jamaah . ".png";

        // create user content
        // $codeContents = "user_name:";
        // $codeContents .= "$user_details->user_name";
        // $codeContents .= " user_address:";
        // $codeContents .= "$user_details->user_address";
        // $codeContents .= "\n";
        // $codeContents .= "user_email :";
        $codeContents .= $bilyet['id_jamaah'];

        $params['data'] = $codeContents;
        $params['level'] = 'H';
        $params['size'] = 2;

        $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
        $this->ci_qr_code->generate($params);

        $this->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

        // save image path in tree table
        // $this->user->change_userqr($user_id, $image_name);
        // then redirect to see image link
        $file = $params['savename'];
        if(file_exists($file)){
            header('Content-Description: File Transfer');
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            unlink($file); // deletes the temporary file

            exit;
        }
    }
	
 
}
