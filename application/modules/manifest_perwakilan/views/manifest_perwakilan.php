
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Aktif </b>
                </div>
<?php if($this->session->flashdata('info')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                                  </div>
                              <?php } ?>
                 <div class="panel-body">

                 <div class="row">
                  <div class="col-lg-12">
                  <div class="panel panel-default">
                  <div class="panel-heading">
                    <b>DATA AFFILIATE</b>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >USER ID</label>
                    <div class="col-sm-6">
                    
                         <input type="text" class="form-control" data-mask="date"  name="id_koordinator" id="id_koordinator" value="<?php echo $get_koordinator['id_user']?>" readonly/>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >NAMA</label>
                    <div class="col-sm-6">
                    
                         <input type="text" class="form-control" data-mask="date"  name="nama_koordinator" id="nama_koordinator" value="<?php echo $get_koordinator['nama']?>" readonly/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >ALAMAT</label>
                    <div class="col-sm-6">
                    
                      <textarea  name="alamat_koordinator" class="form-control"  readonly="readonly"><?php echo $get_koordinator['alamat']?> </textarea>    
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >NO TLP</label>
                    <div class="col-sm-6">
                    
                         <input type="text" class="form-control" data-mask="date"  name="tlp_koordinator" id="tlp_koordinator" value="<?php echo $get_koordinator['telp']?>" readonly/>
                    </div>
                  </div>

                   </div>
                   </div>
                   </div> 

         <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>FILTER DATA MANIFEST </b>
                </div>

                      <div class="form-group">
                        <label for="bulantahun_keberangkatan" class="col-sm-2 control-label">BULAN TAHUN KEBERANGKATAN</label>
                        <div class="col-sm-4">
                            <?php echo $form_bulantahun_keberangkatan; ?>
                        </div>
                    </div>
              <!--   <div class="form-group">
                           <label for="schedules" class="col-sm-2 control-label">BULAN TAHUN KEBERANGKATAN</label>
                        <div class="col-sm-4">
                            <?php echo $form_get_list_date_schedule; ?>
                        </div>
                    </div> -->

                    <input type="hidden" value="<?php echo (trim($this->uri->segment(3.0))); ?>" name="cluster" id="cluster"/>  
                <!--     <div class="form-group">
                     <label class="col-lg-2 control-label">TAHUN BULAN KEBERANGKATAN: </label>
                     <div class="col-lg-4">
                     <select class="form-control "  name="schedule" id="schedule" tabindex="9" >
                        <option value=''>- PILIH TAHUN BULAN KEBERANGKATAN -</option>
                                              <?php foreach($schedule as $schedule){
                                                  echo '<option value="'.$schedule->schedule.'">'.$schedule->schedule.'</option>';
                                              } ?>
                      </select>
                      </div>  
      </div>
 -->
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-8">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <!-- <button type="button" id="btn-reset" class="btn btn-default">Reset</button> -->
                         
                    </div>
                  </div>

                   </div>
                   </div>
                   </div> 

                    </div>
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>No</th>
                                     <!-- <th>DATA GM/CABANG</th> -->
                                    <th>DATA JAMAAH</th>
                                    <th>DATA PASSPORT </th>
                                    <th>DATA PERLENGKAPAN </th>
                                    <th>ACTION </th>
                                </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                     </table>
                   </div>
                  
                      <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
              </div>
          </div>
      </div>
     </form> 
    </div>

      






 
<script type="text/javascript">


var save_method;
var table;

$(document).ready(function() {

    //datatables
    table = $('#data-table').DataTable({ 

        // "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('manifest_perwakilan/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.bulantahun_keberangkatan = $('#bulantahun_keberangkatan').val();
                // data.schedule = $('#schedule').val();
               data.cluster = $('#cluster').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
      
    });

    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });

      $('#cluster').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });

});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}







</script>

<script type="text/javascript">

 $(function () {
            
        $('#isui_date').datetimepicker({
          format: 'YYYY-MM-DD',
        });

        $('#tanggal_lahir').datetimepicker({
          format: 'YYYY-MM-DD',
        });

            });

     $(document).ready(function(){
            
           $("#bulantahun_keberangkatan").change(function (){
                var url = "<?php echo site_url('manifest_perwakilan/add_ajax_schedule');?>/"+$(this).val();
                $('#schedule').load(url);
                return false;
            })

            
        });



</script>
