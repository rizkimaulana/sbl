<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model{
   public function __construct() {
		parent::__construct();
	}


	function get_jamaah_belum_aktif() {
		$id_user=  $this->session->userdata('id_user');
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('id_affiliate',$id_user);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_jamaah_aktif() {
		$id_user=  $this->session->userdata('id_user');
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('id_affiliate',$id_user);
		$this->db->where('status','1');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_jamaah_alumni() {
		$id_user=  $this->session->userdata('id_user');
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('id_affiliate',$id_user);
		$this->db->where('status','2');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_total_jamaah() {
		$id_user=  $this->session->userdata('id_user');
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('id_affiliate',$id_user);
		
		$query = $this->db->get();
		return $query->num_rows();
	}
    
}
