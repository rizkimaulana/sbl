<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settingdata_model extends CI_Model{
   
   
    private $_table="setting";
    private $_primary="id_setting";



    function get_key_val() {
		$out = array();
		$this->db->select('id_setting,opsi_setting,key_setting');
		$this->db->from('setting');
		$query = $this->db->get();
		if($query->num_rows()>0){
				$result = $query->result();
				foreach($result as $value){
					$out[$value->opsi_setting] = $value->key_setting;
				}
				return $out;
		} else {
			return array();
		}
	}


    public function save($data,$data2){
    
        $cek = $this->db->select('keterangan')->where('keterangan',$data['keterangan'])->get('family_relation')->num_rows();
        if($cek)
            return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'keterangan' => $data['keterangan'],
        );       
        
         return $this->db->insert('family_relation',$arr);
    }


    public function update($data){
        
        $arr = array(
        
           'keterangan' =>$data['keterangan'],
            // 'nama' =>$data['nama'],
            // 'status' =>$data['status'],
        );       
              
        return $this->db->update($this->_table,$arr,array('id_relation'=>$data['id_relation']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT * FROM family_relation  
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND keterangan LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_relation DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_relation)
    {
        $this->db->where('id_relation', $id_relation);
        $this->db->delete($this->_table);
    }
    
    
}
