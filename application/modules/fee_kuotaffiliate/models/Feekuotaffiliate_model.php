<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feekuotaffiliate_model extends CI_Model{
    
    private $_table="jamaah_kuota";
    private $_primary="id_booking";

    private $_kuota_booking="kuota_booking";
    private $_id_booking="id_booking";

    public function get_data($offset,$limit,$q=''){
    
     $id_user=  $this->session->userdata('id_user');
          $sql = " SELECT * FROM grouping_jamaah_kuota_aktif where sponsor='".$id_user." ' 
          ";
        
        if($q){
            
            $sql .=" AND invoice LIKE '%{$q}%' 
            		OR fee_refrensi LIKE '%{$q}%'
                    OR pajak_fee LIKE '%{$q}%'
                    OR fee_dipotong_pajak LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY invoice DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   
     public function update_status_fee($id_booking){
    
      
        $arr = array(
        
           'status_fee' => 2,
            'update_date' => date('Y-m-d H:i:s'),
           
        );       
              
         $this->db->update($this->_table,$arr,array('id_booking'=>$id_booking));

        $arr = array(
        
           'status_fee' => 2,
            'update_date' => date('Y-m-d H:i:s'),
            
           
        );       
              
         $this->db->update($this->_kuota_booking,$arr,array('id_booking_kuota'=>$id_booking));
       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }
    
}
