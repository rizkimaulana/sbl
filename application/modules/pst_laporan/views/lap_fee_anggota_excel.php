<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=lap_fee_anggota_".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>AKTIVASI ID</th>
                    <th>KODE TRANS</th>
                    <th>TGL. BAYAR</th>
                    <th>CARA BAYAR</th>
                    <th>NOMINAL BAYAR</th>
                    <th>BANK BAYAR</th>
                    <th>NO. REK.</th>
                    <th>NAMA REK.</th>
                    <th>KETERANGAN</th>
                    <th>STATUS</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['aktivasi_id'] ?>  </td>
                        <td><?php echo $pi['kode_transaksi'] ?>  </td>
                        <td><?php echo $pi['tanggal_bayar'] ?>  </td>
                        <td><?php echo $pi['cara_bayar'] ?>  </td>
                        <td><?php echo $pi['nominal_bayar'] ?>  </td>
                        <td><?php echo $pi['bank_bayar'] ?>  </td>
                        <td><?php echo $pi['no_rek_bayar'] ?>  </td>
                        <td><?php echo $pi['nama_rek_bayar'] ?>  </td>
                        <td><?php echo $pi['keterangan'] ?>  </td>
                        <td><?php echo $pi['status'] ?>  </td>
                        
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>

