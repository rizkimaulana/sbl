<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=lap_registrasi_".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>ID</th>
                    <th>SPONSOR</th>
                    <th>USER ID</th>
                    <th>TGL DAFTAR</th>
                    <th>NAMA LENGKAP</th>
                    <th>TGL AKTIF</th>
                    <th>NOMINAL BAYAR</th>
                    <th>CARA BAYAR</th>
                    <th>BANK BAYAR</th>
                    <th>STATUS</th>
                    <th>KELOMPOK</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['id'] ?>  </td>
                        <td><?php echo $pi['parent_id'] ?>  </td>
                        <td><?php echo $pi['userid'] ?>  </td>
                        <td><?php echo $pi['tgl_daftar'] ?>  </td>
                        <td><?php echo $pi['nama_lengkap'] ?>  </td>
                        <td><?php echo $pi['tgl_aktif'] ?>  </td>
                        <td><?php echo $pi['nominal_bayar'] ?>  </td>
                        <td><?php echo $pi['cara_bayar'] ?>  </td>
                        <td><?php echo $pi['bank_bayar'] ?>  </td>
                        <td><?php echo $pi['status'] ?>  </td>
                        <td><?php echo $pi['kelompok'] ?>  </td>
                        
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>

