<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=lap_angsuran_".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>ID</th>
                    <th>ANGGOTA</th>
                    <th>TGL. BAYAR</th>
                    <th>KODE TRANS</th>
                    <th>ANGSURAN KE-</th>
                    <th>KETERANGAN</th>
                    <th>CARA BAYAR</th>
                    <th>NOMINAL BAYAR</th>
                    <th>BANK BAYAR</th>
                    <th>STATUS</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['id'] ?>  </td>
                        <td><?php echo $pi['anggota'] ?>  </td>
                        <td><?php echo $pi['tanggal'] ?>  </td>
                        <td><?php echo $pi['kode_transaksi'] ?>  </td>
                        <td><?php echo $pi['angsuran_ke'] ?>  </td>
                        <td><?php echo $pi['keterangan'] ?>  </td>
                        <td><?php echo $pi['cara_bayar'] ?>  </td>
                        <td><?php echo $pi['nominal_bayar'] ?>  </td>
                        <td><?php echo $pi['bank_bayar'] ?>  </td>
                        <td><?php echo $pi['status'] ?>  </td>
                        
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>

