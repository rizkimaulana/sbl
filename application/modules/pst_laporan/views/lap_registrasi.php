<div class="page-head">
    <h2>Data Anggota </h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">PESANTREN</a></li>
        <li class="active">Data Anggota</li>
    </ol>
</div> 

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Anggota</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>

                    <form method="POST" name="form1" action="<?php echo base_url(); ?>pst_registrasi/registrasi/export_lap_registrasi">
                        <div class="row">
                            <div class="col-md-1">
                                <label><strong>Filter : </strong></label>   
                                               
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="awal" id="awal" class="form-control" placeholder="yyyy-mm-dd">
                            </div>
                            <div class="col-md-2">
                                <input type="text" name="akhir" id="akhir" class="form-control" placeholder="yyyy-mm-dd">
                            </div>
                            <div class="col-md-1">
                                <button id="btn-filter" name="btn-filter" type="button" class="btn btn-primary">Filter</button>
              
                            </div>
                            <div class="col-md-1">
                                <button id="btn-export" name="btn-export" type="submit" class="btn btn-success">Export to Excel</button>
                                                    
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th width="15%">Tanggal</th>
                                        <th>Nama</th>
                                        <th>Sponsor</th>
                                        <th>Status</th>
                                        <th>Nominal</th>
                                        <!--<th>Aksi</th>-->
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </form>
                    <!-- /.table-responsive -->
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>      -->                

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->

<script type="text/javascript">
    $(function () {
        $('#awal').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#akhir').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>


<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('pst_registrasi/registrasi/ajax_lap_registrasi') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });

        $('#btn-filter').click(function () { //button filter event click
            var awal = document.getElementById("awal").value;
            var akhir = document.getElementById("akhir").value;
            table.ajax.url("<?php echo site_url('pst_registrasi/registrasi/ajax_lap_registrasi'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table
        });
    });

</script>