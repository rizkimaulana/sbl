<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registrasi_model extends CI_Model {

    var $_table_anggota = 'pst_anggota';
    var $_table_aktivasi = 'pst_aktivasi';
    var $_table_affiliate = 'affiliate';
    var $_table_referensi_member = 't_refrensi_member';
    
    public function __construct() {
        parent::__construct();
        $this->load->library('webservice');
    }
    
    /**
     * Menyimpan data registrasi Founder, Kyai dan Ustad
     * @param type $anggota
     * @return type
     */
    function simpan_anggota($anggota){
        if(!empty($anggota['nama_lengkap'])){
            $anggota['nama_lengkap'] = strtoupper($anggota['nama_lengkap']);
        }
        if(!empty($anggota['tempat_lahir'])){
            $anggota['tempat_lahir'] = strtoupper($anggota['tempat_lahir']);
        }
        if(!empty($anggota['alamat'])){
            $anggota['alamat'] = strtoupper($anggota['alamat']);
        }
        if(!empty($anggota['nama_rek'])){
            $anggota['nama_rek'] = strtoupper($anggota['nama_rek']);
        }
        
        if(empty($anggota['id'])){
            return $this->db->insert($_table_anggota, $anggota);
        }else{
            return $this->db->update($_table_anggota, $anggota, array('id' => $anggota['id']));
        }
    }
    
    /**
     * Menyimpan data registrasi Jamaah Pesantren
     * @param type $anggota
     * @return type
     */
    function simpan_jamaah($anggota){
        if(!empty($anggota['nama_lengkap'])){
            $anggota['nama_lengkap'] = strtoupper($anggota['nama_lengkap']);
        }
        if(!empty($anggota['tempat_lahir'])){
            $anggota['tempat_lahir'] = strtoupper($anggota['tempat_lahir']);
        }
        if(!empty($anggota['alamat'])){
            $anggota['alamat'] = strtoupper($anggota['alamat']);
        }
        if(!empty($anggota['nama_rek'])){
            $anggota['nama_rek'] = strtoupper($anggota['nama_rek']);
        }
        
        if(empty($anggota['id'])){
            $this->db->insert($_table_anggota, $anggota);
            $pst_anggota_id = $this->db->insert_id();
            $initial = 'JMH'.date('ym');
            $update_anggota = array(
                'userid' => sprintf($initial . '%06d', $pst_anggota_id)
            );
            $this->db->update($_table_anggota, $update_anggota, array('id' => $pst_anggota_id));
            return $pst_anggota_id;
        }else{
            return $this->db->update($_table_anggota, $anggota, array('id' => $anggota['id']));
        }
    }
    
    /**
     * Menyimpan data affiliate Founder, Kyai dan Ustad
     * @param type $affiliate
     * @param type $anggota_id
     */
    function simpan_affiliate($affiliate, $anggota_id){
        if(empty($affiliate['id_aff'])){
            $this->db->insert($_table_affiliate, $affiliate);
        }else{
            $this->db->update($_table_affiliate, $affiliate, array('id_aff' => $affiliate['id_aff']));
        }
    }
    
    /**
     * API untuk kirim sms notifikasi
     * @param type $nomer_tujuan
     * @param type $pesan
     * @return boolean
     * @throws Exception
     */
    function send_sms($nomer_tujuan, $pesan){
        $data = array(
            'nomer_tujuan' => $nomer_tujuan,
            'pesan' => $pesan
        );
        try {
            $return = '0';
            if(empty($nomer_tujuan)){
                throw new Exception('data nomer tujuan tidak valid');
            }
            if(empty($pesan)){
                throw new Exception('data pesan tidak valid');
            }
            $api_element = '/api/web/send/';
            $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=" . $nomer_tujuan . "&message=" . str_replace(' ', '+', $pesan);
            $smsgatewaydata = $api_params;
            $url = $smsgatewaydata;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if(!curl_exec($ch)){
                throw new Exception(file_get_contents($smsgatewaydata));
            }
            curl_close($ch);
            return TRUE;
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            return FALSE;
        }
    }
    
    /**
     * Generate Kode Unik di Tabel PST_AKTIVASI agar tidak duplikat
     * @return type
     */
    function generate_kode_unik(){
        $is_duplicate = TRUE;
        $kode = '0';
        while ($is_duplicate) {
            $kode = rand(100, 999);
            $kode_aktivasi = $this->db->get_where($this->_table_aktivasi, 
                array('kode_unik' => $kode, 'status' => 'DAFTAR'))->row_array();
            if(empty($kode_aktivasi)){
                $is_duplicate = FALSE;
            }
        }
        return $kode;
    }
    
    
    function get_data_parent($id){
        return $this->db->get_where($this->_table_anggota, array('userid' => $id))->row_array();
    }
    
    
    
    
    
    

    public function generate_kode($idx) {
        $today = date('md');
        $ret = '';
        $limit = 8;
        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {
            $ret .= '0';
        }
        return $idx . $today;
    }

    
    
    
    
    function simpan($data, $affiliate) {
        if (!empty($data['id'])) {
            $hasil = $this->db->update($this->_table_anggota, $data, array('id' => $data['id']));
        } else {
            $this->db->trans_begin();
            // 1. Insert ke tabel PST_ANGGOTA
            $hasil = $this->db->insert($this->_table_anggota, $data);
            $id = $this->db->insert_id();
            $initial = date('ym');
            
            $id_aktivasi = 0;
            // 2. Insert ke tabel PST_AKTIVASI (jika kelompok Jamaah)
            if ($data['kelompok'] == 'JAMAAH') {
                $initial = 'JAM'.date('ym');
                $id_user = sprintf($initial . '%04d', $id);
                $this->db->update($this->_table_anggota, array('userid' => $id_user), array('id' => $id));
                
                $kode_unik = $this->generate_kode_unik();
                $aktivasi = array(
                    'pst_anggota_id' => $id,
                    'pst_anggota_userid' => $id_user,
                    'member_id' => $data['parent_id'],
                    'member_userid' => $data['parent_userid'],
                    'kode_unik' => $kode_unik,
                    'nominal' => 1000000,
                    'status' => 'DAFTAR'
                );
                $this->db->insert($this->_table_aktivasi, $aktivasi);
                $id_aktivasi = $this->db->insert_id();
                
                $initial = 'PSBL'.date('ym');
                $kode_transaksi = sprintf($initial . '%06d', $id_aktivasi);
                $this->db->update($this->_table_aktivasi, array('kode_transaksi' => $kode_transaksi), array('id' => $id_aktivasi));
                
                // Insert ke tabel PST_GROUP_FEE (fee untuk masing-masing upline)
                // a. Fee ustadz
                $ustadz = $this->db->get_where('pst_anggota', array('id' => $data['parent_id']))->row_array();
                $kyai = $this->db->get_where('pst_anggota', array('id' => $ustadz['parent_id']))->row_array();
                $founder = $this->db->get_where('pst_anggota', array('id' => $kyai['parent_id']))->row_array();
                $fee_ustads = array(
                    'id_registrasi' => $id,
                    'id_group' => $id_aktivasi,
                    'invoice' => $kode_transaksi,
                    'id_member' => $ustadz['id'],
                    'sponsor' => $kyai['id'],
                    'id_product' => '',
                    'tipe_fee' => '1111',
                    'tipe_registrasi' => '1313',
                    'fee' => 350000,
                    'keterangan' => 'Fee Registrasi Jamaah Pesantren '.$id_user,
                    //'claim_date' => '',
                    'create_date' => date('Y-m-d H:i:s'),
                    //'update_date' => '',
                    'create_by' => $this->session->userdata('id_user'),
                    //'update_by' => '',
                    //'payment_date' => '',
                    'status' => '0',
                    'status_claim' => '0',
                );
                $this->db->insert('pst_group_fee', $fee_ustads);
                
                // b. Fee Kyai
                
                $fee_kyai = array(
                    'id_registrasi' => $id,
                    'id_group' => $id_aktivasi,
                    'invoice' => $kode_transaksi,
                    'id_member' => $kyai['id'],
                    'sponsor' => $founder['id'],
                    'id_product' => '',
                    'tipe_fee' => '1122',
                    'tipe_registrasi' => '1313',
                    'fee' => 250000,
                    'keterangan' => 'Fee Registrasi Jamaah Pesantren '.$id_user. ' Ustadz '.$ustadz['userid'],
                    //'claim_date' => '',
                    'create_date' => date('Y-m-d H:i:s'),
                    //'update_date' => '',
                    'create_by' => $this->session->userdata('id_user'),
                    //'update_by' => '',
                    //'payment_date' => '',
                    'status' => '0',
                    'status_claim' => '0',
                );
                $this->db->insert('pst_group_fee', $fee_kyai);
                
                // c. Fee Founder
                $fee_founder = array(
                    'id_registrasi' => $id,
                    'id_group' => $id_aktivasi,
                    'invoice' => $kode_transaksi,
                    'id_member' => $founder['id'],
                    'sponsor' => '',
                    'id_product' => '',
                    'tipe_fee' => '1133',
                    'tipe_registrasi' => '1313',
                    'fee' => 100000,
                    'keterangan' => 'Fee Registrasi Jamaah Pesantren '.$id_user. ' Kyai '.$kyai['userid'],
                    //'claim_date' => '',
                    'create_date' => date('Y-m-d H:i:s'),
                    //'update_date' => '',
                    'create_by' => $this->session->userdata('id_user'),
                    //'update_by' => '',
                    //'payment_date' => '',
                    'status' => '0',
                    'status_claim' => '0',
                );
                $this->db->insert('pst_group_fee', $fee_founder);
                
                // d. Fee SBL (?)
                $fee_sbl = array(
                    'id_registrasi' => $id,
                    'id_group' => $id_aktivasi,
                    'invoice' => $kode_transaksi,
                    'id_member' => '',
                    'sponsor' => '',
                    'id_product' => '',
                    'tipe_fee' => '1144',
                    'tipe_registrasi' => '1313',
                    'fee' => 0,
                    'keterangan' => 'Fee Registrasi Jamaah Pesantren '.$id_user,
                    //'claim_date' => '',
                    'create_date' => date('Y-m-d H:i:s'),
                    //'update_date' => '',
                    'create_by' => $this->session->userdata('id_user'),
                    //'update_by' => '',
                    //'payment_date' => '',
                    'status' => '0',
                    'status_claim' => '0',
                );
                $this->db->insert('pst_group_fee', $fee_sbl);
                
            }

            // 3. Insert ke tabel AFFILIATE di DATABASE KONVEN
            if (!empty($affiliate)) {
                $get_initial = $this->db->select('affiliate_code')
                    ->where('id_affiliate_type', $affiliate['id_affiliate_type'])->get('affiliate_type')->row();

                $initial = ($get_initial) ? $get_initial->affiliate_code : '';
                $this->db->insert($this->_table_affiliate, $affiliate);
                $id_aff = $this->db->insert_id();
                
                // Update tabel AFFILIATE untuk generate ID_User dan Username sesuai dengan
                $id_user = $this->generate_kode($initial . $id_aff);
                $this->db->update($this->_table_affiliate, 
                    array('id_user' => $id_user, 'username' => $id_user),
                    array('id_aff' => $id_aff));

                // Update table PST_ANGGOTA relasi ke tabel AFFILIATE
                $this->db->update($this->_table_anggota, 
                    array('userid' => $id_user, 'affiliate_id_aff' => $id_aff), 
                    array('id' => $id));
                
                //4. INSERT ke table t_refrensi_member
                $arr_ref = array(
                    'id_user' => $id_user,
                    'sponsor' => $affiliate['sponsor'],
                    'id_affiliate_type' => $affiliate['id_affiliate_type'],
                    'nama' => $affiliate['nama'],
                    'telp' => $affiliate['telp'],
                    'alamat' => $affiliate['alamat'],
                    'provinsi' => $data['provinsi'],
                    'kabupaten' => $data['kabupaten'],
                    'kecamatan' => ''
                );
                $this->db->insert($this->_table_referensi_member, $arr_ref);
            }
            
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_complete();
                
                if ($data['kelompok'] != 'JAMAAH') {
                    // 4. Send SMS ke user terkait login dan password (jika bukan kelompok Jamaah)
                    $member = $this->db->get_where('affiliate', array('id_user' => $id_user))->row_array();
                    if(!empty($member)){
                        $no_telpon = str_replace(' ', '', $member['telp']);
                        $nama = $member['nama'];
                        $username = $member['id_user'];
                        $pass = $member['ket_upgrade'];
                        $message = $data['kelompok'].' '.$nama.' telah terdaftar di program pesantren sbl dengan username : '.$username.', password : '.$pass.'. Silahkan login di office.sbl.co.id';
                        $kirim_sms = $this->send_sms($no_telpon, $message);
                    }
                    
                    //5. REGISTRASI SBL MOBILE
                    $upline = $this->db->get_where('affiliate', array('id_user' => $affiliate['sponsor']))->row_array();
                    $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $affiliate['provinsi_id']))->row_array();
                    $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $affiliate['kabupaten_id']))->row_array();
                    $kelamin = ($affiliate['kelamin'] === '2') ? '1' : '2';
                    $status_kawin = ($data['status_nikah'] === '4') ? '0' : '1';
                    $mobile_anggota = array(
                        'userid' => $id_user,
                        'sponsor' => $affiliate['sponsor'],
                        'upline' => !empty($upline['sponsor']) ? $upline['sponsor'] : '',
                        'posisi' => '1',
                        'password1' => md5($member['ket_upgrade']),
                        'password2' => md5($member['ket_upgrade']),
                        'nama' =>  $affiliate['nama'],
                        'jeniskelamin' => $kelamin,
                        'ktp' => $affiliate['ktp'],
                        'tempatlahir' => $affiliate['ktp'],
                        'tanggallahir' => $affiliate['ktp'],
                        'propinsi' => !empty($provinsi['nama']) ? strtoupper($provinsi['nama']) : '',
                        'kota' => !empty($kabupaten['nama']) ? strtoupper($kabupaten['nama']) : '',
                        'alamat' => $affiliate['alamat'],
                        'kodepos' => '',
                        'telepon' => $affiliate['telp'],
                        'email' => !empty($affiliate['email'])? $affiliate['email']: '',
                        'waris' => '',
                        'hubunganahliwaris' => '',
                        'ibukandung' => '',
                        'npwp' => !empty($affiliate['npwp'])? $affiliate['npwp'] : '',
                        'statuskawin' => $status_kawin,
                        'jumlahtanggungan' => '0',
                        'namabank' => '',
                        'norekening' => '',
                        'fileattachmentnpwp' => '',
                        'fileattachmentktp' => ''
                    );
                    // 1. Registrasi Ke Mobile
                    $reg_mobile = $this->webservice->service_registrasi_mobile($mobile_anggota);

                    // 2. Simpan Response proses registrasi
                    if($reg_mobile['responCode'] === '000'){
                        $konten = array(
                            'data' => $mobile_anggota,
                            'response' => $reg_mobile
                        );
                        $log_aktivitas = array(
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'username' => $this->session->userdata('username'),
                            'aktivitas' => 'Registrasi Mobile Pesantren Berhasil',
                            'konten' => json_encode($konten),
                            'created' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('log_aktivitas', $log_aktivitas);
                    }else{
                        $konten = array(
                            'data' => $mobile_anggota,
                            'response' => $reg_mobile
                        );
                        $log_aktivitas = array(
                            'ip_address' => $_SERVER['REMOTE_ADDR'],
                            'username' => $this->session->userdata('username'),
                            'aktivitas' => 'Registrasi Mobile Pesantren TIDAK BERHASIL',
                            'konten' => json_encode($konten),
                            'created' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('log_aktivitas', $log_aktivitas);
                    }
                }
                return $id_aktivasi;
            }
        }
        return $hasil;
    }
    
    function get_anggota($id) {
        return $this->db->get_where($this->_table_anggota, array('id' => $id))->row_array();
    }
    
    function get_anggota_by_userid($userid){
        return $this->db->get_where($this->_table_anggota, array('userid' => $userid))->row_array();
    }

    function get_data() {
        $this->db->from($this->_table_anggota);
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result_array();
    }
    
    function get_data_in_client($id){
        $this->db->select('*');
        $this->db->where('parent_userid', $id);
        return $this->db->get($this->_table_anggota)->result_array(); 
    }

    function get_data_edit($id) {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }
        return $this->db->get($this->_table_anggota)->row_array();
    }

    function search_founder($keyword) {
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%' . $keyword . '%');
        $this->db->where('kelompok', 'FOUNDER');
        return $this->db->get($this->_table_anggota)->result_array();
    }

    function search_kyai($keyword) {
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%' . $keyword . '%');
        $this->db->where('kelompok', 'KYAI');
        return $this->db->get($this->_table_anggota)->result_array();
    }

    function search_ustadz($keyword) {
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%' . $keyword . '%');
        $this->db->where('kelompok', 'USTADZ');
        return $this->db->get($this->_table_anggota)->result_array();
    }

    function delete($id) {
        $data = array('status' => 'HAPUS');
        return $this->db->update($this->_table_anggota, $data, array('id' => $id));
    }

    function get_data_prov($id) {
        if (!empty($id)) {
            $this->db->where('provinsi_id', $id);
        }
        $hasil = $this->db->get('wilayah_provinsi')->row_array();
        $result = array(
            'provinsi' => !empty($hasil['nama']) ? $hasil['nama'] : ''
        );

        return $result;
    }
    
    function get_data_aktivasi($id_aktivasi){
        $sql = "SELECT a.*, b.nama_lengkap, b.alamat FROM (pst_aktivasi a JOIN pst_anggota b) ".
                "WHERE (a.pst_anggota_id = b.id) and (a.id = '".$id_aktivasi."') ";
        return $this->db->query($sql);
    }
}

?>