<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {
    var $folder = 'pst_registrasi';
    
    public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('is_login'))redirect('frontend/login');
        if(!$this->general->privilege_check(PESANTREN,'view'))
            $this->general->no_access();
        $this->load->library('unit_test');
        $this->session->set_userdata('menu', 'pesantren');
        $this->load->model('Registrasi_model');
        $this->load->model('pst_pembayaran/Aktivasi_model');
    }
    
    function send_sms($nomer_tujuan, $pesan){
        $data = array(
            'nomer_tujuan' => $nomer_tujuan,
            'pesan' => $pesan
        );
        try {
            $return = '0';
            if(empty($nomer_tujuan)){
                throw new Exception('data nomer tujuan tidak valid');
            }
            if(empty($pesan)){
                throw new Exception('data pesan tidak valid');
            }
            $api_element = '/api/web/send/';
            $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=" . $nomer_tujuan . "&message=" . str_replace(' ', '+', $pesan);
            $smsgatewaydata = $api_params;
            $url = $smsgatewaydata;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if(!curl_exec($ch)){
                throw new Exception(file_get_contents($smsgatewaydata));
            }
            curl_close($ch);
            $this->simpan_aktivitas('Kirim SMS Notifikasi', $data);
            return TRUE;
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            $this->simpan_error('Kirim SMS Notifikasi', $data, $msg);
            return FALSE;
        }
    }
    
    function cek_nama($nama){
        $anggota = $this->db->get_where('pst_anggota', array('nama_lengkap' => $nama))->result_array();
        if(!empty($anggota)){
            $hasil = array(
                'hasil'=>'Nama Tersebut sudah ada, pastikan bahwa orang tersebut tidak sama dengan data yang sudah ada'
            );
        }else{
            $hasil = array(
                'hasil'=>''
            );            
        }
        echo json_encode($hasil);
    }
    
    function index() {
        $data = $this->input->post();
        if (!empty($data)) {
            // $this->myDebug($data);
            try {
                // 1. Update Data Anggota
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                
                $anggota = array(
                    'id'=>$data['id'],
                    //'parent_id' => $data['kyai_id'], 
                    //'userid' => '', //generate
                    //'parent_userid' => $data['kyai_id'], //select parent
                    //'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => strtoupper($data['nama_lengkap']),
                    'tgl_lahir' => $data['tgl_lahir'],
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'alamat' => strtoupper($data['alamat']),
                    'kabupaten_id' => $data['kabupaten'],
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => $data['provinsi'],
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    //'status' => 'DAFTAR',
                    //'kelompok' => 'USTADZ', //kelompok ustadz
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                if (!$this->Registrasi_model->simpan($anggota)) {
                    throw new Exception('Error ketika update data registrasi ustadz');
                }                
                $this->simpan_aktivitas('Update Data Registrasi', $data);                
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Diperbaharui');
                redirect($this->folder.'/registrasi/index');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->session->set_flashdata('message_ok', 'Data Registrasi Tidak Dapat Diperbaharui!!');
                $this->simpan_error('Update Data Registrasi', $data, $exc);
            }
        }        
        $this->template->load('template/template', $this->folder . '/index');
    }
    
    function register_founder(){
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $anggota = array(
                    'parent_id' => '',
                    'userid' => '',
                    'parent_userid' => '',
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => strtoupper($data['nama_lengkap']),
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'tgl_lahir' => date('Y-m-d', strtotime($data['tgl_lahir'])),
                    'alamat' => strtoupper($data['alamat']),
                    'kabupaten_id' => !empty($data['kabupaten']) ? $data['kabupaten'] : '',
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => !empty($data['provinsi']) ? $data['provinsi'] : '',
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'npwp' => $data['npwp'],
                    'bank' => $data['bank'],
                    'no_rek' => $data['no_rek'],
                    'nama_rek' => strtoupper($data['nama_rek']),
                    'status' => 'AKTIF',
                    'kelompok' => 'FOUNDER',
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                
                //foto
                $flag = 0;
                $rename_file = array();
                for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                    if ($_FILES['pic']['name'][$i]) {
                        $rename_file[$i] = 'pic' . ($i + 1) . '_' . $_FILES['pic']['name'][$i];
                        $flag++;
                    } else {
                        $rename_file[$i] = '';
                    }
                }

                //if files are selected
                if ($flag > 0) {
                    $this->load->library('upload');
                    $this->upload->initialize(array(
                        "file_name" => $rename_file,
                        'upload_path' => './assets/images/affiliate/',
                        'allowed_types' => 'gif|jpg|jpeg|png',
                        'max_size' => '2000' //Max 2MB
                    ));

                    if ($this->upload->do_multi_upload("pic")) {
                        $info = $this->upload->get_multi_upload_data();
                        foreach ($info as $in) {
                            $picx = substr($in['file_name'], 0, 4);
                            $data[$picx] = $in['file_name'];
                        }
                    } else {
                        $error = array('error' => $this->upload->display_errors());
                        echo "Errors Occured : "; //sini aja lah
                        print_r($error);
                    }
                }
                
                $affiliate = array(
                    'sponsor' => 'SBL01',
                    'id_affiliate_type' => $data['id_affiliate_type'],
                    'nama' => strtoupper($data['nama_lengkap']),
                    'password' => md5($data['password']),
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'tanggal_lahir' => date('Y-m-d', strtotime($data['tgl_lahir'])),
                    'kelamin' => $data['jenis_kelamin'],
                    'ktp' => $data['no_identitas'],
                    'provinsi_id' => !empty($data['provinsi'])? $data['provinsi'] : '',
                    'kabupaten_id' => !empty($data['kabupaten'])? $data['kabupaten'] : '',
                    'kecamatan_id' => !empty($data['kecamatan'])? $data['kecamatan'] : '',
                    'alamat' => strtoupper($data['alamat']),
                    'alamat_kantor' => strtoupper($data['alamat']),
                    'alamat_kirim' => strtoupper($data['alamat']),
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    //'waris' => $data['waris'],
                    //'hubwaris' => $data['hubwaris'],
                    'bank' => $data['bank'],
                    'norek' => $data['no_rek'],
                    'namarek' => strtoupper($data['nama_rek']),
                    'pic1' => isset($data['pic1']) ? $data['pic1'] : '',
                    'tanggal_daftar' => date('Y-m-d H:i:s'),
                    'status' => 1,
                    'create_by' => $this->session->userdata('username'),
                    'npwp' => $data['npwp'],
                    'jabatan_id' => 38,
                    'ket_upgrade' => $data['password'],
                    'pajak' => 0.06
                    
                );
                
                if (!$this->Registrasi_model->simpan($anggota,$affiliate)) {
                    throw new Exception('Error ketika simpan data registrasi Founder');
                }

                $this->simpan_aktivitas('Registrasi Founder', $data);
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Tersimpan');
                redirect($this->folder.'/registrasi/index');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Founder', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        
        $this->template->load('template/template', $this->folder . '/register_founder');
    }
            
    function register_kyai() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $founder = $this->Registrasi_model->get_anggota($data['founder_id']);
                $anggota = array(
                    'parent_id' => $data['founder_id'],
                    'userid' => '',
                    'parent_userid' => $founder['userid'],
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => strtoupper($data['nama_lengkap']),
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'tgl_lahir' => date('Y-m-d', strtotime($data['tgl_lahir'])),
                    'alamat' => strtoupper($data['alamat']),
                    'kabupaten_id' => !empty($data['kabupaten'])? $data['kabupaten'] : '',
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => !empty($data['provinsi']) ? $data['provinsi'] : '',
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'npwp' => $data['npwp'],
                    'bank' => $data['bank'],
                    'no_rek' => $data['no_rek'],
                    'nama_rek' => strtoupper($data['nama_rek']),
                    'status' => 'AKTIF',
                    'kelompok' => 'KYAI',
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                
                //foto
                $flag = 0;
                $rename_file = array();
                for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                    if ($_FILES['pic']['name'][$i]) {
                        $rename_file[$i] = 'pic' . ($i + 1) . '_' . $_FILES['pic']['name'][$i];
                        $flag++;
                    } else {
                        $rename_file[$i] = '';
                    }
                }

                //if files are selected
                if ($flag > 0) {
                    $this->load->library('upload');
                    $this->upload->initialize(array(
                        "file_name" => $rename_file,
                        'upload_path' => './assets/images/affiliate/',
                        'allowed_types' => 'gif|jpg|jpeg|png',
                        'max_size' => '2000' //Max 2MB
                    ));

                    if ($this->upload->do_multi_upload("pic")) {
                        $info = $this->upload->get_multi_upload_data();
                        foreach ($info as $in) {
                            $picx = substr($in['file_name'], 0, 4);
                            $data[$picx] = $in['file_name'];
                        }
                    } else {
                        $error = array('error' => $this->upload->display_errors());
                        echo "Errors Occured : "; //sini aja lah
                        print_r($error);
                    }
                }
                
                $affiliate = array(
                    'sponsor' => $founder['userid'],//KYAI sponsor FOUNDER
                    'id_affiliate_type' => $data['id_affiliate_type'],
                    'nama' => strtoupper($data['nama_lengkap']),
                    'password' => md5($data['password']),
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'tanggal_lahir' => date('Y-m-d', strtotime($data['tgl_lahir'])),
                    'kelamin' => $data['jenis_kelamin'],
                    'ktp' => $data['no_identitas'],
                    'provinsi_id' => !empty($data['provinsi'])? $data['provinsi'] : '',
                    'kabupaten_id' => !empty($data['kabupaten'])? $data['kabupaten'] : '',
                    'kecamatan_id' => !empty($data['kecamatan'])? $data['kecamatan'] : '',
                    'alamat' => strtoupper($data['alamat']),
                    'alamat_kantor' => strtoupper($data['alamat']),
                    'alamat_kirim' => strtoupper($data['alamat']),
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    //'waris' => $data['waris'],
                    //'hubwaris' => $data['hubwaris'],
                    'bank' => $data['bank'],
                    'norek' => $data['no_rek'],
                    'namarek' => strtoupper($data['nama_rek']),
                    'pic1' => isset($data['pic1']) ? $data['pic1'] : '',
                    'tanggal_daftar' => date('Y-m-d H:i:s'),
                    'status' => 1,
                    'create_by' => $this->session->userdata('username'),
                    'npwp' => $data['npwp'],
                    'jabatan_id' => 35,
                    'ket_upgrade' => $data['password'],
                    'pajak' => 0.06
                    
                );
                
                if (!$this->Registrasi_model->simpan($anggota,$affiliate)) {
                    throw new Exception('Error ketika simpan data registrasi Kyai');
                }
                $this->simpan_aktivitas('Registrasi Kyai', $data);
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Tersimpan');
                redirect($this->folder.'/registrasi/index');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Kyai', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        
        $this->template->load('template/template', $this->folder . '/register_kyai');
    }

    function register_ustadz()  {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $kyai = $this->Registrasi_model->get_anggota($data['kyai_id']);
                $anggota = array(
                    'parent_id' => $data['kyai_id'], 
                    'userid' => '',
                    'parent_userid' => $kyai['userid'],
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => strtoupper($data['nama_lengkap']),
                    'tgl_lahir' => $data['tgl_lahir'],
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'alamat' => strtoupper($data['alamat']),
                    'kabupaten_id' => !empty($data['kabupaten']) ? $data['kabupaten'] : '',
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => !empty($data['provinsi']) ? $data['provinsi'] : '',
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'bank' => $data['bank'],
                    'no_rek' => $data['no_rek'],
                    'nama_rek' => strtoupper($data['nama_rek']),
                    'status' => 'AKTIF',
                    'kelompok' => 'USTADZ', 
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                );
                
                //foto
                $flag = 0;
                $rename_file = array();
                for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                    if ($_FILES['pic']['name'][$i]) {
                        $rename_file[$i] = 'pic' . ($i + 1) . '_' . $_FILES['pic']['name'][$i];
                        $flag++;
                    } else {
                        $rename_file[$i] = '';
                    }
                }

                //if files are selected
                if ($flag > 0) {
                    $this->load->library('upload');
                    $this->upload->initialize(array(
                        "file_name" => $rename_file,
                        'upload_path' => './assets/images/affiliate/',
                        'allowed_types' => 'gif|jpg|jpeg|png',
                        'max_size' => '5000' //Max 2MB
                    ));

                    if ($this->upload->do_multi_upload("pic")) {
                        $info = $this->upload->get_multi_upload_data();
                        foreach ($info as $in) {
                            $picx = substr($in['file_name'], 0, 4);
                            $data[$picx] = $in['file_name'];
                        }
                    } else {
                        $error = array('error' => $this->upload->display_errors());
                        echo "Errors Occured : "; //sini aja lah
                        print_r($error);
                    }
                }
                
                $affiliate = array(
                    'sponsor' => $kyai['userid'],
                    'id_affiliate_type' => $data['id_affiliate_type'],
                    'nama' => strtoupper($data['nama_lengkap']),
                    'password' => md5($data['password']),
                    'tempat_lahir' => strtoupper($data['tempat_lahir']),
                    'tanggal_lahir' => date('Y-m-d', strtotime($data['tgl_lahir'])),
                    'kelamin' => $data['jenis_kelamin'],
                    'ktp' => $data['no_identitas'],
                    'provinsi_id' => !empty($data['provinsi'])? $data['provinsi'] : '',
                    'kabupaten_id' => !empty($data['kabupaten'])? $data['kabupaten'] : '',
                    'kecamatan_id' => !empty($data['kecamatan'])? $data['kecamatan'] : '',
                    'alamat' => strtoupper($data['alamat']),
                    'alamat_kantor' => strtoupper($data['alamat']),
                    'alamat_kirim' => strtoupper($data['alamat']),
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    //'waris' => $data['waris'],
                    //'hubwaris' => $data['hubwaris'],
                    'bank' => $data['bank'],
                    'norek' => $data['no_rek'],
                    'namarek' => strtoupper($data['nama_rek']),
                    'pic1' => isset($data['pic1']) ? $data['pic1'] : '',
                    'tanggal_daftar' => date('Y-m-d H:i:s'),
                    'status' => 1,
                    'create_by' => $this->session->userdata('username'),
                    'npwp' => $data['npwp'],
                    'jabatan_id' => 36,
                    'ket_upgrade' => $data['password'],
                    'pajak' => 0.06
                );
                
                if (!$this->Registrasi_model->simpan($anggota,$affiliate)) {
                    throw new Exception('Error ketika simpan data registrasi Ustadz');
                }
                $this->simpan_aktivitas('Registrasi & Aktivasi Ustadz', $data);
                $this->session->set_flashdata('message_ok', 'Data Registrasi Telah Tersimpan');
                redirect($this->folder.'/registrasi/index');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Ustadz', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        
       $this->template->load('template/template', $this->folder . '/register_ustadz');
    }
    
    function register_jamaah() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $provinsi = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $data['provinsi']))->row_array();
                $kabupaten = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $data['kabupaten']))->row_array();
                $ustadz = $this->Registrasi_model->get_anggota($data['ustadz_id']);
                $anggota = array(
                    'parent_id' => $data['ustadz_id'], 
                    'userid' => '',
                    'parent_userid' => $ustadz['userid'],
                    'tanggal' => date('Y-m-d H:i:s'),
                    'nama_lengkap' => strtoupper($data['nama_lengkap']),
                    'tgl_lahir' => $data['tgl_lahir'],
                    'tempat_lahir' => $data['tempat_lahir'],
                    'alamat' => strtoupper($data['alamat']),
                    'kabupaten_id' => $data['kabupaten'],
                    'kabupaten' => !empty($kabupaten['nama']) ? $kabupaten['nama'] : '',
                    'provinsi_id' => $data['provinsi'],
                    'provinsi' => !empty($provinsi['nama']) ? $provinsi['nama'] : '',
                    'kode_pos' => $data['kode_pos'],
                    'telp' => $data['no_telp'],
                    'email' => $data['email'],
                    'status_nikah' => $data['status_nikah'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'no_identitas' => $data['no_identitas'],
                    'status' => 'DAFTAR',
                    'kelompok' => 'JAMAAH', 
                    'modified_date' => date('Y-m-d H:i:s'),
                    'modified_by' => $this->session->userdata('username')
                ); 
                
                $id_aktivasi = $this->Registrasi_model->simpan($anggota,'');
                             
                $this->simpan_aktivitas('Registrasi & Aktivasi Jamaah', $data);                
                $this->session->set_flashdata('message_ok', 'Data Registrasi Jamaah Telah Tersimpan');
                
                redirect($this->folder.'/registrasi/cetak_invoice/'.$id_aktivasi);
                //redirect($this->folder.'/registrasi/index');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Registrasi Jamaah', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
       $this->template->load('template/template', $this->folder . '/register_jamaah');
    }
    
    function cetak_invoice($id_aktivasi){
        $data = array();
        $getdata = $this->Registrasi_model->get_data_aktivasi($id_aktivasi)->result();
        $data = array();
        foreach($getdata as $row){
            $data = array(
                'kd_transaksi' => $row->kode_transaksi,
                'pst_anggota_userid' => $row->pst_anggota_userid,
                'nama' => $row->nama_lengkap,
                'alamat' => $row->alamat,
                'id' => $row->id,
                'pay_method' => '-',
                'bank_transfer' => '',
                'nominal' => $row->nominal,
            );
           
        }
        $this->load->view('cetak_invoice', $data);
    }
    
    function ws_simpan_anggota($data){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/pst_simpan_affiliate');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'data' => json_encode($data)
        ));

        // Optional, delete this lsine if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);

        $result = json_decode($buffer);
        return $result;
    }
    
    function ajax_jenis_kelamin() {
        $kdstatus = array('2', '3');
        $this->db->where_in('kdstatus', $kdstatus);
        $query = $this->db->get('status');
        $data = "<option value=''>- Pilih Jenis Kelamin -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kdstatus."'>".$value->keterangan."</option>";
        }
        echo $data;
        
    }
    
    function ajax_family_relation() {
        $query = $this->db->get('family_relation');
        $data = "<option value=''>- Pilih Hubungan Keluarga -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->id_relation."'>".$value->keterangan."</option>";
        }
        echo $data;
    }
    
    function add_ajax_prov(){
        $query = $this->db->get('wilayah_provinsi');
        $data = "<option value=''>- Select Provinsi -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->provinsi_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function add_ajax_kab($id_prov){
        $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=> $id_prov ));
        $data = "<option value=''>- Select Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function add_ajax_kec($id_kab){
        $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=> $id_kab ));
        $data = "<option value=''>- Select Kecamatan -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function add_ajax_kab_detail(){
        $query = $this->db->get('wilayah_kabupaten');
        $data = "<option value=''>- Select Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    function search_founder(){
        $keyword = $this->uri->segment(4);
        $data = $this->Registrasi_model->search_founder($keyword);
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row['nama'],
                'anggota_id' => $row['id'],
            );
        }
        echo json_encode($arr);
    }
    
    function search_kyai(){
        $keyword = $this->uri->segment(4);
        $data = $this->Registrasi_model->search_kyai($keyword);
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row['nama'],
                'anggota_id' => $row['id'],
            );
        }
        echo json_encode($arr);
    }
    
    function search_ustadz(){
        $keyword = $this->uri->segment(4);
        $data = $this->Registrasi_model->search_ustadz($keyword);
        foreach ($data as $row) {
            $arr['query'] = $keyword;
            $arr['suggestions'][] = array(
                'value' => $row['nama'],
                'anggota_id' => $row['id'],
            );
        }
        echo json_encode($arr);
    }
    
    function get_number_from_string($string){
        return preg_replace("/[^0-9]/","",$string);
    }
    
    function aktivasi() {
        $data = $this->input->post();
        if (!empty($data)) {
            try {
                $flag = 0;
                $rename_file = array();
                for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                    if ($_FILES['pic']['name'][$i]) {
                        $rename_file[$i] = 'pic' . ($i + 1) . '_PSTACT_' .date('ym').'_'. $_FILES['pic']['name'][$i];
                        $flag++;
                    } else {
                        $rename_file[$i] = '';
                    }
                }

                //if files are selected
                if ($flag > 0) {
                    $this->load->library('upload');
                    $this->upload->initialize(array(
                        "file_name" => $rename_file,
                        'upload_path' => './assets/images/pesantren_aktivasi/',
                        'allowed_types' => 'gif|jpg|jpeg|png',
                        'max_size' => '5000' //Max 5MB
                    ));
                    if ($this->upload->do_multi_upload("pic")) {
                        $info = $this->upload->get_multi_upload_data();
                        foreach ($info as $in) {
                            $picx = substr($in['file_name'], 0, 4);
                            $data[$picx] = $in['file_name'];
                        }
                    } else {
                        $error = array('error' => $this->upload->display_errors());
                        echo "Errors Occured : "; //sini aja lah
                        print_r($error);
                    }
                }
                
                $data['nominal_bayar'] = $this->get_number_from_string($data['nominal_bayar']);
                if(!$this->Aktivasi_model->aktivasi($data)){
                    throw new Exception('Ada kesalahan ketika simpan data aktivasi anggota');
                }
                $this->simpan_aktivitas('Aktivasi Anggota', $data);
                $this->session->set_flashdata('message_ok', 'Data Aktivasi Berhasil Disimpan');
            } catch (Exception $exc) {
                $msg = $exc->getMessage();
                $this->simpan_error('Aktivasi Anggota', $data, $exc);
                $this->session->set_flashdata('message_err', $msg);
            }
        }
        $this->template->load('template/template', 'pst_pembayaran/aktivasi');
    }

    function ajax_data_aktivasi() {
        $list_data = $this->Aktivasi_model->get_all_data();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['nama_lengkap'];
            $row[] = $item['parent_userid'];
            $row[] = $item['telp'];
            $row[] = $item['kode_unik'];
            $row[] = number_format($item['nominal'],0,',','.');
            $row[] = '<button class="btn btn-warning" onclick="aktivasi('.$item['id'].')"> Aktivasi </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function get_data_anggota($aktivasi_id){
        $aktivasi = $this->Aktivasi_model->get_aktivasi($aktivasi_id);
        $anggota = $this->Registrasi_model->get_anggota($aktivasi['pst_anggota_id']);
        if(!empty($anggota)){
            $referal = $this->Registrasi_model->get_anggota($anggota['parent_id']);
            $referal_nama = !empty($referal['userid']) ? $referal['userid'].' - '.$referal['nama_lengkap'] : '';
            $hasil = array(
                'aktivasi_id'=>$aktivasi_id,
                'nama_lengkap'=>$anggota['nama_lengkap'],
                'parent_nama'=>$referal_nama,
                'alamat'=>$anggota['alamat'],
                'nominal_bayar'=>number_format($aktivasi['nominal']+$aktivasi['kode_unik'],0,',','.'),
                'id_user'=>$anggota['userid']
            );
        }else{
            $hasil = array(
                'aktivasi_id'=>'',
                'nama_lengkap'=>'',
                'parent_nama'=>'',
                'alamat'=>'',
                'nominal_bayar'=>'',
                'id_user'=> ''
            );            
        }
        echo json_encode($hasil);
    }

    function lap_registrasi(){
        $this->template->load('template/template', 'pst_laporan/lap_registrasi');
    }
    
    function ajax_lap_registrasi() {
        $awal = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        $akhir = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : '';
        //$this->myDebug($awal);
        $list_data = $this->Aktivasi_model->get_lap_registrasi($awal, $akhir);
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $referal = $this->Registrasi_model->get_anggota($item['parent_id']);
            $referal_name = !empty($referal) ? $referal['userid'].' - '.$referal['nama_lengkap'] : '';
            $row = array();
            $row[] = $no;
            $row[] = '<strong>Daftar</strong> <br>'.date('Y-m-d', strtotime($item['tanggal_daftar'])).'<br><strong>Aktif</strong> <br>'.$item['tanggal_aktif'];
            $row[] = $item['userid'].' - '.$item['nama_lengkap'];
            $row[] = $referal_name;
            $row[] = '<strong>Kelompok</strong> '.$item['kelompok'].'<br><strong>Status</strong> '.$item['status'];
            $row[] = number_format($item['nominal_bayar'],0,',','.');
            //$row[] = '<button class="btn btn-warning" onclick="detail('.$item['id'].')"> Detail </button>';
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    function export_lap_registrasi() {
        $post_data = $this->input->post(NULL, FALSE);
        
        
        $awal = !empty($post_data['awal']) ? $post_data['awal'] : '';
        $akhir = !empty($post_data['akhir']) ? $post_data['akhir'] : '';
        
        $hasil = $this->Aktivasi_model->get_lap_registrasi($awal, $akhir);
        //$data = array();
        $no = 1;
        foreach ($hasil as $item) {
            $no++;
//            $aktivasi = $this->get_data_aktivasi($item['id_booking']);
//            $booking = $this->get_data_booking($item['id_booking']);
//            $product = $this->get_data_product($item['id_product']);
//            $tipe_jamaah = $this->get_data_tipe_jamaah($item['id_tipe_jamaah']);
            
            $referal = $this->Registrasi_model->get_anggota($item['parent_id']);
            $referal_nama = !empty($referal['userid']) ? $referal['userid'].' - '.$referal['nama_lengkap'] : '';
            
            $row = array();
            $item['id'] = $item['id'];
            $item['parent_id'] = $referal_nama;//$item['parent_id'];
            $item['userid'] = $item['userid'];
            $item['tgl_daftar'] = date('d/m/Y', strtotime($item['tanggal_daftar']));
            $item['nama_lengkap'] = $item['nama_lengkap'];
            $item['tgl_aktif'] = date('d/m/Y', strtotime($item['tanggal_aktif']));
            $item['nominal_bayar'] = number_format($item['nominal_bayar']);
            $item['cara_bayar'] = $item['cara_bayar'];
            $item['bank_bayar'] = $item['bank_bayar'];
            $item['status'] = $item['status'];
            $item['kelompok'] = $item['kelompok'];
            
            $data[] = $item;
        }
        $data['list'] = $data;
        
        $this->load->view('pst_laporan/lap_registrasi_excel', ($data));
    }
    
    function ajax_data_anggota(){
        $list_data = $this->Registrasi_model->get_data();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            $row = array();
            $row[] = $no;
            $row[] = $item['userid'];
            $row[] = $item['nama_lengkap'];
            $row[] = $item['kabupaten'];
            $row[] = ($item['tanggal']!='0000-00-00 00:00:00')? date('d-m-Y', strtotime($item['tanggal'])) : '-';            $row[] = $item['telp'];
            $row[] = $item['email'];
            $row[] = $item['status'];
            $row[] = $item['kelompok'];
            
            $btnDetail = '<button class="btn btn-warning" onclick="detail('.$item['id'].')"> Detail </button>';
            $btnEdit = '<button class="btn btn-info" onclick="edit_data('.$item['id'].')"> Edit </button>';
            $disabled = ($item['status']=="HAPUS") ? 'disabled' : '';
            // $btnDelete = '<button class="btn btn-danger" onclick="delete_data('.$item['id'].')" '.$disabled.'> Delete </button>';
            $btnDelete = '';
            
            $row[] = $btnDetail. ' ' .$btnEdit.' '.$btnDelete;
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
       
        echo json_encode($output);
    }
    
    function detail($id){
        $data = $this->Registrasi_model->get_data_edit($id);
        $tanggal = ($data['tanggal'])!='0000-00-00 00:00:00'? date('d-m-Y', strtotime($data['tanggal'])) : '-';
        //$provinsi = $this->wilayah_provinsi($data['provinsi']);
        
        $hasil = array(
                'id'=>$data['id'],
                'nama_lengkap'=>$data['nama_lengkap'],
                'tempat_lahir'=>$data['tempat_lahir'],
                'tgl_lahir'=>$data['tgl_lahir'],
                'jenis_kelamin'=>$data['jenis_kelamin'],
                'tanggal'=>$tanggal,
                'alamat'=>$data['alamat'],
                'kabupaten_id'=>$data['kabupaten_id'],
                'provinsi_id'=>$data['provinsi_id'],                
                'kode_pos'=>$data['kode_pos'],
                'telp'=>$data['telp'],
                'email'=>$data['email'],
                'no_identitas'=>$data['no_identitas'],
                'status_nikah'=>$data['status_nikah'],
                'kelompok'=>$data['kelompok'],
                
            );
        
        //$this->myDebug($hasil);
        echo json_encode($hasil);
    }
    
    function delete_data($id_anggota){
        $send = $this->Registrasi_model->delete($id_anggota);
    }
    
    function wilayah_provinsi($id){
        return $this->Registrasi_model->get_data_prov($id);
    }
    
    function add_ajax_bank(){
        $query = $this->db->get('bank');
        $data = "<option value=''>- Select Bank -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->id."'>".$value->nama."</option>";
        }
        echo $data;
    }
    
    // <editor-fold defaultstate="collapsed" desc=" UNIT TESTING ">
    function test1(){
        $test = 1 + 1;
        $expected_result = 2;
        $test_name = 'Test-1';
        $this->unit->run($test, $expected_result, $test_name);
    }

    function test2(){
        $test = 1 + 1;
        $expected_result = 2;
        $test_name = 'Test-2';
        $this->unit->run($test, $expected_result, $test_name);
    }
    
    function test_test(){
        $this->test1();
        $this->test2();
        echo $this->unit->report();
    }
    // </editor-fold>
}

?>