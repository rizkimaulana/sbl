 <div id="page-wrapper">
      <div class="row">
   <div class="col-lg-12">
        
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Registration PROMO UMROH</b>
        </div>
        <div class="panel-body">
        <form  role="form" method="POST"  class="form-horizontal" action="<?php echo base_url();?>kuota_promoadmin/generate_data_kuota">
 
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>PILIH SEATS SESUAI JADWAL YANG TERSEDIA</b>
                </div>
                 <?php if($this->session->flashdata('Warning')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Warning! </strong><?php echo $this->session->flashdata('Warning'); ?>  
                                  </div>
                              <?php } ?>
                <!-- /.panel-heading -->
                  <div class="panel-body">


                 <div class="form-group">
                      <label class="col-lg-2 control-label">Embarkasi</label>
                      <div class="col-lg-5">
                      <select required class="form-control" name="departure" id="departure" >
                          <option value="">Pilih Embarkasi</option>
                          <?php foreach($select_embarkasi as $se){?>
                              <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
               
                <div class="form-group">
                          <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                                <input type='text' name="datepicker_tahun_keberangkatan" id="datepicker_tahun_keberangkatan1" class="form-control" placeholder="YYYY/2016" required/>
                        
                          </div>
                    </div>

                     <div class="form-group">
                          <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                <input type='text' name="datepicker_keberangkatan" id="datepicker_keberangkatan1" class="form-control" placeholder="MM/January" required/>
                               <!--  <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                          </div>

                    </div>
                    
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-10">
                        <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check Waktu Keberangkatan</button>
                         
                    </div>
                  </div>
                
                  <div class="table-responsive">
                   <table id="data-table" style="width:100%" align="center" class="table table-striped table-bordered table-hover" >
                    <caption>Pilih Jadwal</caption>
                    <thead>
                        <tr>
                            <!-- <th width="2%">Check </th> -->
                            <th width="8%"><strong>Paket</strong></th>
                            <th width="10%"><strong>Embarkasi</strong></th>
                            <th width="10%"><strong>Tgl/Bln </strong></th>
                            <!-- <th width="8%"><strong>Waktu </strong></th> -->
                            <th width="5%"><strong>Jml Kursi</strong></th>
                            <th width="5%"><strong>Tipe Kamar</strong></th>
                            <th width="5%"><strong>Category</strong></th>
                            <!-- <th width="5%"><strong>Keterangan</strong></th> -->
                            <th width="5%"><strong>Harga</strong></th>
                            <!-- <th width="5%"><strong>Status Jadwal</strong></th>
                            <th width="5%"><strong>maskapai</strong></th> -->
                            <th width="5%"><strong>PILIH</strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <tr><td colspan="12"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>

                    </tbody>
                </table>
                </div>
                   

                   </div>
                </div>
              </div>
            </div><!--end col-->
    <!-- <form action="<?php echo base_url();?>kuota_promoadmin/generate_data_kuota"  role="form" method="post">   -->
       <div class="row">
       	<div class="col-lg-12">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <!-- <h3 class="page-header">TAMBAH JAMAAH KUOTA</h3> -->

 
       <?php $tiga_digit = random_3_digit_kuota(); ?>
        <input type='hidden' name='unique_digit' value='<?= $tiga_digit; ?>'>

            <div class="form-group">
                 <label class="col-lg-2 control-label">Affiliate Type :</label>
                  <div class="col-lg-5">
                <select  required name="affiliate_type" id="affiliate_type" class="form-control ">
                  <div class="col-lg-5">
                      <option value="">- PILIH AFFILIATE TYPE -</option>
                        <?php foreach($select_affiliate_type as $st){?>
                            <option  value="<?php echo $st->id_affiliate_type;?>"required><?php echo $st->affiliate_type;?> </option>
                        <?php } ?>
                      </select>
                </div>
                      <input type ="hidden" name="fee_jamaah_kuota" id="fee_jamaah_kuota" class="form-control" >
            </div> 
            <div class="form-group">
                 <label class="col-lg-2 control-label">Affiliate : </label>
                 <div class="col-lg-5">
                <select required name="id_affiliate" id="id_affiliate" class="form-control ">
                      <option value="">- PILIH Affiliate yg Beli Kuota -</option>
                </select>
              </div>
            </div>
             <div class="form-group">
                <label class="col-lg-2 control-label">Jumlah Kuota </label>
                <div class="col-lg-5">
                <select required name="jml_kuota"  class="form-control ">
               <?php 
                  for($i=10; $i<=100; $i++){
               ?>
             <option value="<?php echo $i;?>"><?php echo $i;?></option>
              <?php
               }
              ?>
             </select> 
           </div>
            </div> 
            <?php 
              $biaya_setting_ = $this->kuotapromoadmin_model->get_key_val();
                foreach ($biaya_setting_ as $key => $value){
                  $out[$key] = $value;
                }
            ?>
             <div class="form-group">
                <input type='hidden'class="form-control" name="harga_jamaah_kuota" value="<?php echo $out['HARGA_JAMAAH_KUOTA'];?>" required>
            </div>

            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lanjut</button>
            <a href="<?php echo base_url();?>kuota_promoadmin" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
<script>

        $(document).ready(function(){
           
             $("#affiliate_type").change(function (){
                var url = "<?php echo site_url('kuota_promoadmin/add_ajax_affiliate_type');?>/"+$(this).val();
                $('#id_affiliate').load(url);
                return false;
            })


            $("#affiliate_type").change(function(){
            var affiliate_type=$("#affiliate_type").val();
            console.log(affiliate_type);
            $.ajax({
                url:"<?php echo site_url('kuota_promoadmin/get_fee_kuota');?>",
                type:"POST",
                data:"affiliate_type="+affiliate_type,
                cache:false,
                success:function(html){
                    $("#fee_jamaah_kuota").val(html);
                    
                }
            })
        })
        });


    $("#check").on('click', function (e){
    e.preventDefault();
    // var paket = $("#paket").val();
    var departure = $("#departure").val();
    var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
    var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
    // console.log(paket);
    console.log(departure);
    console.log(datepicker_tahun_keberangkatan);
    console.log(datepicker_keberangkatan);
    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>kuota_promoadmin/searchItem_paket_promo',
             // data:'from='+from+'&to='+to
            data:'s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan+'&l='+departure
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});


      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
      
       $(function () {
                $('#datepicker_keberangkatan1').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
</script>
