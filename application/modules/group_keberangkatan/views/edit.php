<div id="page-wrapper">
    <div class="row">
        <!-- <div class="col-lg-7"> -->
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Update Group Keberangkatan</h3>
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert alert-danger">  
                        <a class="close" data-dismiss="alert">x</a>  
                        <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                    </div>
                <?php } ?>
                <form action="<?php echo base_url(); ?>group_keberangkatan/update" class="form-horizontal" role="form" method="post" >
                    <div class="panel panel-default">
                        <input type="hidden" name="id_grp_keberangkatan" value="<?php echo $group_manifest['id_grp_keberangkatan']; ?>">
                        <div class="panel-body">  
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Embarkasi</label>
                                <div class="col-lg-5">
                                    <select required class="form-control"  name="embarkasi" id="embarkasi">
                                        <?php foreach ($list_embarkasi as $embarkasi) : ?>
                                            <?php $checked = ($group_manifest['id_embarkasi'] == $embarkasi['id_embarkasi']) ? 'selected' : ''; ?>
                                            <option value="<?php echo $embarkasi['id_embarkasi']; ?>" <?php echo $checked; ?> ><?php echo $embarkasi['departure']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tanggal</label>
                                <div class="col-lg-5">
                                    <select name="tanggal" class="form-control" id="tanggal">
                                        <option value="<?php echo $group_manifest['date_schedule']; ?>" ><?php echo $group_manifest['date_schedule']; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Nama Group</label>
                                <div class="col-lg-5">
                                    <input type ="text" name="nama" id="nama" class="form-control" value="<?php echo $group_manifest['nama']; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Keterangan</label>
                                <div class="col-lg-5">
                                    <textarea  name="keterangan" class="form-control"  ><?php echo $group_manifest['keterangan']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Jumlah Group</label>
                                <div class="col-lg-5">
                                    <input type ="text" name="jumlah" id="jumlah" class="form-control" value="<?php echo $group_manifest['jumlah']; ?>" >
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                            <a href="<?php echo base_url(); ?>group_keberangkatan" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#embarkasi").change(function () {
            var url = "<?php echo site_url('group_keberangkatan/add_ajax_tanggal'); ?>/" + $(this).val();
            $('#tanggal').load(url);
            return false;
        })
    });
</script>