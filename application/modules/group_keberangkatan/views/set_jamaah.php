<script>
    function hitungCheck(){
        var numberChecked = $('input:checkbox:checked').length;
        if(numberChecked > parseInt(<?php echo $group_manifest['jumlah'];?>)){
            alert('Jumlah Jamaah Sudah Melebihi Kuota!');
        }
    };
</script>
<div id="page-wrapper">
    <div class="row">
        <!-- <div class="col-lg-7"> -->
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Set Jamaah Group Keberangkatan</h3>
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert alert-danger">  
                        <a class="close" data-dismiss="alert">x</a>  
                        <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                    </div>
                <?php } ?>
                <form action="<?php echo base_url(); ?>group_keberangkatan/save_set_jamaah" class="form-horizontal" role="form" method="post" >
                    <div class="panel panel-default">
                        <div class="panel-body">  
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Embarkasi</label>
                                <div class="col-lg-8">
                                    <input type ="hidden" name="id_grp_keberangkatan" id="id_grp_keberangkatan" value="<?php echo $group_manifest['id_grp_keberangkatan']; ?>" >
                                    <input type ="text" name="embarkasi" id="embarkasi" class="form-control" value="<?php echo $group_manifest['id_embarkasi']; ?>" readonly="true">
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tgl Keberangkatan</label>
                                <div class="col-lg-8">
                                    <input type ="text" name="date_schedule" id="date_schedule" class="form-control" value="<?php echo date('d M Y', strtotime($group_manifest['date_schedule'])) ?>" readonly="true">
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Nama Group</label>
                                <div class="col-lg-5">
                                    <input type ="text" name="nama" id="nama" class="form-control" value="<?php echo $group_manifest['nama']; ?>" readonly="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Keterangan</label>
                                <div class="col-lg-5">
                                    <textarea  name="keterangan" class="form-control" disabled="disabled" ><?php echo $group_manifest['keterangan']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Jumlah Group</label>
                                <div class="col-lg-5">
                                    <input type ="text" name="jumlah" id="jumlah" class="form-control" value="<?php echo $group_manifest['jumlah']; ?>" readonly="true">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Check</th>
                                    <th>No</th>
                                    <th>ID Jamaah</th>
                                    <th>Nama</th>
                                    <th>Kota</th>
                                    <th>Produk</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $nomer = 1; ?>
                                <?php foreach ($list_manifest as $manifest) : ?>
                                <tr>
                                    <td>
                                        <?php $is_checked = ($manifest['id_grp_keberangkatan'] == $group_manifest['id_grp_keberangkatan'])? 'checked' : ''; ?>
                                        <input type="hidden" class="checkbox_handler" name="is_check[]" value="<?php echo !empty($is_checked) ? '1':'0'; ?>"  />
                                        <input type="checkbox" name="is_check_ck[]" value="1" <?php echo $is_checked; ?> onchange="hitungCheck()" />
                                        <input type="hidden" name="id_registrasi[]" value="<?php echo $manifest['id_registrasi']; ?>" />
                                    </td>
                                    <td><?php echo $nomer; ?></td>
                                    <td><?php echo $manifest['id_jamaah']; ?></td>
                                    <td><?php echo $manifest['nama']; ?></td>
                                    <td><?php echo $manifest['nama_kabupaten']; ?></td>
                                    <td><?php echo $manifest['nama_product']; ?></td>
                                    <td><?php echo $manifest['keterangan']; ?></td>
                                </tr>
                                <?php $nomer++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    <button type="button" class="btn btn-primary" onclick="hitungCheck();"><i class="fa fa-save"></i> CHECK</button>
                    <a href="<?php echo base_url(); ?>group_keberangkatan" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>