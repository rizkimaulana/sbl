<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends CI_Controller {

	
	public function index()
	{
		$data = array(
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
			);
		$this->load->view('template_frontend/header');
		// $this->load->view('template_frontend/menu');
		$this->load->view('frontend/view_login',$data);
		$this->load->view('template_frontend/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */