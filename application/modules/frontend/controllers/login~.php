<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	// public function cek_aktif() {
	// 		if ($this->session->userdata('admin_valid') == false && $this->session->userdata('admin_id') == "") {
	// 			redirect('login/masuk');
	// 		} 
	// }
	
	public function index()
	{

		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/view_login');
		$this->load->view('template_frontend/footer');
		
		// $this->cek_aktif();
		// $a['sess_level'] = $this->session->userdata('admin_level');
		// $a['sess_user'] = $this->session->userdata('admin_user');
		// //$a['sess_konid'] = $this->session->userdata('admin_konid');
		// // $this->load->view('template/header');
		// // $this->load->view('template/menu');
		// // $this->load->view('template/nav');
		// $this->load->view('dashboard', $a);
		// // $this->load->view('template/footer');
		
	}


	public function masuk() {
		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/view_login');
		$this->load->view('template_frontend/footer');
	}

	public function act_login() {
		
		$username	= $this->bersih($_POST['username']);
		$password	= $this->bersih($_POST['password']);
		
		$password2	= md5($password);
		
		$q_data		= $this->db->query("SELECT * FROM tb_admin WHERE User = '".$username."' AND Password = '$password2'");
		$j_data		= $q_data->num_rows();
		$a_data		= $q_data->row();
		
		$_log		= array();
		if ($j_data === 1) {

			$sess_nama_user = "";

			// if ($a_data->level == "siswa") {
			// 	$det_user = $this->db->query("SELECT nama FROM m_siswa WHERE id = '".$a_data->kon_id."'")->row();
			// 	if (!empty($det_user)) {
			// 		$sess_nama_user = $det_user->nama;
			// 	}
			// } else if ($a_data->level == "guru") {
			// 	$det_user = $this->db->query("SELECT nama FROM m_guru WHERE id = '".$a_data->kon_id."'")->row();
			// 	if (!empty($det_user)) {
			// 		$sess_nama_user = $det_user->nama;
			// 	}
			// } else {
			// 	$sess_nama_user = "Administrator Pusat";
			// }

			$data = array(
                    'admin_id' => $a_data->Id,
                    'admin_user' => $a_data->User,
                    'admin_level' => $a_data->Level,
                    //'admin_konid' => $a_data->kon_id,
                    'admin_nama' => $sess_nama_user,
					'admin_valid' => true
                    );
            $this->session->set_userdata($data);
			$_log['log']['status']			= "1";
			$_log['log']['keterangan']		= "Login berhasil";
			$_log['log']['detil_admin']		= $this->session->userdata;
		} else {
			$_log['log']['status']			= "0";
			$_log['log']['keterangan']		= "Maaf, username dan password tidak ditemukan";
			$_log['log']['detil_admin']		= null;
		}
		
		$this->j($_log);
	}

	public function bersih($teks) {
		return mysql_real_escape_string($teks);
	}
	
	public function j($data) {
		header('Content-Type: application/json');
		echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */