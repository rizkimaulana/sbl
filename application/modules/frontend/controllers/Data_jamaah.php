<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_jamaah extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('datajamaah_module');
		$this->load->model('datajamaah_module','datajamaah_module');
	}
	
	
	public function index()
	{
		 $data = array(
	    			
	    			'blnthn_keberangkatan'=>$this->datajamaah_module->get_all_schedule(),
	    			
	    			
	    	);

		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/data_jamaah',$data);
		$this->load->view('template_frontend/footer');
	}


	
	function add_ajax_schedule($blnthn_keberangkatan){
		    // $query = $this->db->get_where('view_jamaah',
		    	
		    // 	array('blnthn_keberangkatan'=>$blnthn_keberangkatan)
		    // 	);
		 $query = $this->db->query("SELECT DISTINCT(date_schedule) from  view_jamaah where 
		 	blnthn_keberangkatan ='".$blnthn_keberangkatan."' ORDER BY  date_schedule");
		    $data = "<option value=''>- Pilih tanggal Keberangkatan -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->date_schedule."'>".$value->date_schedule."</option>";
		    }
		    echo $data;
	}


	public function ajax_list()
	{
		$list = $this->datajamaah_module->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $datajamaah_module) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $datajamaah_module->invoice;
			$row[] = $datajamaah_module->id_jamaah;
			$row[] = $datajamaah_module->nama;
			$row[] = $datajamaah_module->date_schedule;


			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->datajamaah_module->count_all(),
						"recordsFiltered" => $this->datajamaah_module->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

 public function detail(){
	

		
		$this->load->view('template_frontend/header');
		$this->load->view('template_frontend/menu');
		$this->load->view('frontend/detail_jamaah');
		$this->load->view('template_frontend/footer');
	}

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */