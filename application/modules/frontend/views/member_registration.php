<style type="text/css">
  
/* Credit to bootsnipp.com for the css for the color graph */
.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);

  #custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
}
</style>


  
<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <form action="<?php echo base_url(); ?>frontend/member_registration/save" method="POST">
      <h2>Please Sign Up <small>to become a General Marketing  </small> </h2>
      <hr class="colorgraph">

      <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
             <?php $tiga_digit = random_3_digit_member_mgm(); ?>
                        <input type='hidden' name='unique_digit' value='<?= $tiga_digit; ?>'>
                            <?php
                                $biaya_setting_ = $this->memberregistration_module->get_key_val();
                                  foreach ($biaya_setting_ as $key => $value){
                                    $out[$key] = $value;
                                  }
                              ?>

                         <input type="hidden" name="DEPOSIT_GM_1" class="form-control input-sm" id="DEPOSIT_GM_1" value="<?php echo $out['DEPOSIT_GM_1'];?>" />

                         <input type="hidden" name="DEPOSIT_GM_2" class="form-control input-sm" id="DEPOSIT_GM_2" value="<?php echo $out['DEPOSIT_GM_2'];?>" />


                           <input type="hidden" name="harga_product" class="form-control input-sm" id="harga_product" value="<?php echo $out['HARGA_PRODUCT_DP5JT'];?>" />


      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <label class="radio-inline">
                <input type="radio" name="member" id="member"  value="1" required>MEMBER BARU SAHABAT
              </label>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <label class="radio-inline">
                <input type="radio" name="member" id="member"  value="2" required>MEMBER PERWAKILAN SAHABAT
              </label>
        </div>
      </div>


         <div class="form-group">
              <select required class="form-control input-lg chosen-select" name="sponsor" id='sponsor'>
                      <option required value="">- PILIH ID REFRENSI -</option>
                    <?php foreach($select_affiliate as $fr){?>
                        <option value="<?php echo $fr->id_user;?>"><?php echo $fr->id_user;?>  <?php echo $fr->username;?> <?php echo $fr->nama;?></option>
                    <?php } ?>
               </select>
          </div>

          
         <!--  <div id="detail_barang" style="position:absolute;">
                    </div> -->
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
         <div class="form-group">
                <input type="text" name="id_affiliate" id="id_affiliate" class="form-control input-lg" placeholder="ID SAHABAT" tabindex="1" required>  
                  <!-- <input type="text" class="form-control input-lg" id="search" placeholder="Search..." x-webkit-speech> -->
            
                  <button class="btn btn-primary btn-block btn-lg" id="btn-search">CARI USERID <i class="fa fa-search"></i></button>
                             
          </div>
          </div>
       
 
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <input type="text" name="nama" id="nama"  class="form-control input-lg" placeholder="Nama" tabindex="2"  required readonly="true">
          </div>
        </div>
      </div>

       <div class="form-group">
        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="EMAIL" tabindex="3" required>
      </div>

      <div class="form-group">
        <input type="text" name="alamat" id="alamat" class="form-control input-lg" placeholder="Alamat Rumah" tabindex="3" required>
      </div>
      <div class="form-group">
        <input type="text" name="alamat_kantor" id="alamat_kantor" class="form-control input-lg" placeholder="Alamat Kantor" tabindex="4" required>
      </div>

        <div class="form-group">
        <input type="text" name="alamat_kirim" id="alamat_kirim" class="form-control input-lg" placeholder="Alamat Kiriman" tabindex="5" required>
      </div>

          <div class="form-group">
            <input type="text" name="telp" id="telp" class="form-control input-lg" onKeyPress="return numbersonly(this, event)" placeholder="Input No. Telp (harus karakter angka)" tabindex="6">
          </div>
        
         
          <div class="form-group">
            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control input-lg" placeholder="Tempat Lahir" tabindex="7">
          </div>
     
          
          <div class="form-group">
            <input type="text" name="tanggal_lahir" id="datepicker" class="form-control input-lg" placeholder="Tanggal Bulan Tahun Kelahiran" tabindex="8">
          </div>
        
         <div class="form-group">
            <input type="text" name="ktp" id="ktp" class="form-control input-lg" placeholder="No Identitas /" tabindex="9">
          </div>

         <div class="form-group">

         <select class="form-control input-lg"  name="select_kelamin" tabindex="8" >
             <option value=''>- PILIH JENIS KELAMIN -</option>
                <?php foreach($select_kelamin as $sk){?>
                    <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
                <?php } ?>
          </select>
      </div>

      <div class="form-group">
         <select class="form-control input-lg"  name="provinsi_id" id="provinsi" tabindex="9" >
            <option value=''>- PILIH Provinsi -</option>
                                  <?php foreach($provinsi as $Country){
                                      echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                  } ?>
          </select>
      </div>
      <div class="form-group">
        <select required name="kabupaten_id" class="form-control input-lg" id="kabupaten" tabindex="10">
            <option value=''>Select Kabupaten</option>
             </select>
      </div>
      <div class="form-group">
         <select class="form-control input-lg"  name="kecamatan_id" id="kecamatan" tabindex="11">
            <option value=''>- PILIH Kabupaten -</option>
            
          </select>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <input type="text" class="form-control input-lg" name="waris" placeholder="Input Ahli waris"  tabindex="12"/>
           
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
             <select required class="form-control input-lg" name="select_hub_ahliwaris" tabindex="13">
                <option  value="">Hubungan Ahli Waris</option>
                  <?php foreach($select_hub_ahliwaris as $sk){?>
                      <option value="<?php echo $sk->id_hubwaris;?>"><?php echo $sk->hubungan;?></option>
                  <?php } ?>
              </select>
          </div>
        </div>
      </div>
      
     <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <select required class="form-control input-lg" name="select_bank" tabindex="14">
                  <option values="">- Select Bank -</option>
                <?php foreach($select_bank as $sb){?>
                    <option value="<?php echo $sb->id;?>"><?php echo $sb->nama;?></option>
                <?php } ?>
              </select>
       </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
             <input type="text" class="form-control input-lg" name="norek" placeholder="Input No Rekening" required tabindex="15"/>
          </div>
        </div>
      </div>
  
       <div class="form-group">
            <input type="text" name="namarek" id="namarek" class="form-control input-lg" placeholder="Nama di Rekening" tabindex="16">
          </div>

            <div class="form-group">
            <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="17">
          </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <label class="radio-inline">
                <input type="radio" name="pin" id="pin"  value="50" required>DEPOSIT 5.000.000
              </label>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <label class="radio-inline">
                <input type="radio" name="pin" id="pin"  value="100" required>DEPOSIT 10.000.000
              </label>
        </div>
      </div>
      <hr class="colorgraph">
      <div class="row">
        <div class="col-xs-12 col-md-12"><input type="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="7"> </div>
        <!-- <div class="col-xs-12 col-md-6"><a href="#" class="btn btn-success btn-block btn-lg">Sign In</a></div> -->
        <!-- <button type="submit" class="col-xs-12 col-md-12"><i class="fa fa-save"></i> Aktivasi</button> -->
      </div>
    </form>
  </div>
</div>


</div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css" />
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">

$(function(){
    
    $('#thedate').datepicker({
        dateFormat: 'YYYY-MM-DD',
        altField: '#thealtdate',
        altFormat: 'YYYY-MM-DD'
    });
    
});
$("#datepicker").datepicker({
  dateFormat: 'yy-mm-dd',

    changeMonth: true,
    changeYear: true,
    
});
    </script>

    <script type="text/javascript">
       $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('frontend/member_registration/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('frontend/member_registration/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('frontend/member_registration/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })

             jQuery(document).ready(function(){
              $(".chosen-select").chosen({width: "95%"}); 
            });
        });

    </script>
   
<script type="text/javascript">
  
  $(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});


   // $("#id_affiliate").change(function(){
   //          var id_affiliate=$("#id_affiliate").val();
   //          console.log(id_affiliate);
   //          $.ajax({
   //              url:"<?php echo site_url('member_registration/get_nama');?>",
   //              type:"POST",
   //              data:"id_affiliate="+id_affiliate,
   //              cache:false,
   //              success:function(html){
   //                  $("#nama").val(html);
                    
   //              }
   //          })
   //      })


       $("#btn-search").on('click', function (e){
      var id_affiliate=$("#id_affiliate").val();
            console.log(id_affiliate);
            $.ajax({
                url:"<?php echo site_url('frontend/member_registration/get_nama');?>",
                type:"POST",
                data:"id_affiliate="+id_affiliate,
                cache:false,
                success:function(html){
                    $("#nama").val(html);
                    
                }
            })
    
});
</script>


 
   <script type="text/javascript">
function numbersonly(myfield, e, dec) { var key; var keychar; if (window.event) key = window.event.keyCode; else if (e) key = e.which; else return true; keychar = String.fromCharCode(key); // control keys 
if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) return true; // numbers 
else if ((("0123456789").indexOf(keychar) > -1)) return true; // decimal point jump 
else if (dec && (keychar == ".")) { myfield.form.elements[dec].focus(); return false; } else return false; }

</script>
  