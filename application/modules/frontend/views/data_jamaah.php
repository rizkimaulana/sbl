<div id="page-wrapper">
  <div class="row">
   <div class="col-lg-12">
        
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>DATA JAMAAH</b>
        </div>
        <div class="panel-body">
        <form  role="form" method="POST"  class="form-horizontal">
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>DATA JAMAAH AKTIF </b>
                </div>
               
                  <div class="panel-body">

         <div class="form-group">
         <label class="col-lg-3 control-label">TAHUN BULAN KEBERANGKATAN: </label>
         <div class="col-lg-5">
         <select class="form-control input-lg"  name="blnthn_keberangkatan" id="blnthn_keberangkatan" tabindex="9" >
            <option value=''>- PILIH TAHUN BULAN KEBERANGKATAN -</option>
                                  <?php foreach($blnthn_keberangkatan as $thnbulan){
                                      echo '<option value="'.$thnbulan->blnthn_keberangkatan.'">'.$thnbulan->blnthn_keberangkatan.'</option>';
                                  } ?>
          </select>
          </div>  
      </div>
                  
          <div class="form-group">
                          <label class="col-lg-3 control-label">TGL KEBERANGKATAN: </label>
                           <div class="col-lg-5">
					        <select required name="date_schedule" class="form-control input-lg" id="date_schedule" tabindex="10">
					            <option value=''>- PILIH TANGGAL KEBERANGKATAN</option>
					             </select>
					             </div>
					      </div>
                    <div class="form-group">
                      <label class="col-lg-3 control-label"> </label>
                        <div class="col-lg-5">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>   
                    </div>
                  </div>
                 <div class="table-responsive">
                   <table class="table table-bordered" id="datatable" >
                  
                    <thead>
                        <tr>
                            <th width="2%">No </th>
                            <th width="8%"><strong>INVOICE</strong></th>
                            <th width="5%"><strong>ID JAMAAH </strong></th>

                            <th width="8%"><strong>NAMA </strong></th>
                             <th width="8%"><strong>JADWAL </strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <!-- <tr><td colspan="12"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr> -->

                    </tbody>
                </table>
                </div>
                   

                   </div>
                </div>
              </div>
            </div><!--end col-->

        </form>
           
           </div>

     </div>
     </div>
     </div>
  </div><!--end row-->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>    

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.min.js')?>"></script>
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
      
       $(function () {
                $('#bulan_keberangkatan').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#tahun_keberangkatan').datetimepicker({
          format: 'YYYY',
        });
            });
    </script>

    
<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables
    table = $('#datatable').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('frontend/data_jamaah/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.blnthn_keberangkatan = $('#blnthn_keberangkatan').val();
                data.date_schedule = $('#date_schedule').val();
           
               
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });

});

</script>

<script type="text/javascript">

 

     $(document).ready(function(){
            
           $("#blnthn_keberangkatan").change(function (){
                var url = "<?php echo site_url('frontend/data_jamaah/add_ajax_schedule');?>/"+$(this).val();
                $('#date_schedule').load(url);
                return false;
            })

            
        });

</script>
