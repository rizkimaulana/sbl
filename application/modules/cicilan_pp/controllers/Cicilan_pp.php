<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cicilan_pp extends CI_Controller{ 
	var $folder = "cicilan_pp";
	private $db2;
	public function __construct(){
	
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(JAMAAH_PINDAH_PAKET,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('cicilanpp_model');
		 $this->db2 = $this->load->database('db2', TRUE);
		 $this->load->library('terbilang');
		  $this->load->library('ci_qr_code');
        $this->config->load('qr_code');
	}
	
	public function index(){
	   
	  //  $this->template->load('template/template', $this->folder.'/cicilan_pp');
		print_r('MENU PINDAH PAKET INI DALAM TAHAP PERBAIKAN, HUBUNGI PIHAK IT');
	}
	
	private function _select_reward(){
	$id = array('4','5','6','8','10','12','13');
		$this->db2->where_in('id', $id);
	    return $this->db2->get('t_reward_old')->result();
	}
	

	
	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->cicilanpp_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

	function search_pp(){
            
			 
             $search = $this->input->post('s');

             if(!empty($search)){
                 $this->cicilanpp_model->search_pp($search);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }
	
	



	 private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}

	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_roomcategory(){
		 $id_room_category = array('1');
		$this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

	function add_ajax_kab($id_prov){
	    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
	    $data = "<option value=''>- Select Kabupaten -</option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_keluarga($id_booking=''){
	$id_booking = $this->uri->segment(4);
	$this->db->where_in('id_booking', $id_booking);
    return $this->db->get('jamaah_kuota')->result();
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}


	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}

	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}

	function get_biaya_refund_4(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['handling'];
           
          
        }
        
	}



	private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}

	private function _select_affiliate_type(){
		$id_affiliate_type = array('1','2','3', '5');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
	    return $this->db->get('affiliate_type')->result();
	}
	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3','5');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		$this->db->order_by("nama", "asc");
		return $this->db->get('affiliate')->result();
	    
	}
	public function pindah_paket(){
	    if(!$this->general->privilege_check(JAMAAH_PINDAH_PAKET,'view'))
		    $this->general->no_access();

		

	    $id = $this->uri->segment(3);
	    $get = $this->db2->get_where('refrensi_cicilan_sahabat',array('userid'=>$id))->row_array();
		 $cicilan = $this->cicilanpp_model->get_cicilan($id);
	    $data = array(
	    		 'select_product'=>$this->_select_product(),
				'select_embarkasi'=>$this->_select_embarkasi(),
				'select_roomcategory'=>$this->_select_roomcategory(),
	    		 'cicilan'=>$cicilan,
	    		'provinsi'=>$this->cicilanpp_model->get_all_provinsi(),
		    	'kabupaten'=>$this->cicilanpp_model->get_all_kabupaten(),		
		    	'select_kelamin'=>$this->_select_kelamin(),	
		    	'select_rentangumur'=>$this->_select_rentangumur(),
		    	'select_statuskawin'=>$this->_select_statuskawin(),
		    	'select_status_hubungan'=>$this->_select_status_hubungan(),
		    	'family_relation'=>$this->_family_relation(),
		    	'select_keluarga'=>$this->_select_keluarga(),
		    	'select_pemberangkatan'=>$this->_select_pemberangkatan(),
		    	'select_type'=>$this->_select_type(),
		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'select_merchandise'=>$this->_select_merchandise(),
		    	// 'provinsi_manasik'=>$this->cicilanpp_model->get_all_provinsi_manasik(),
	       		 'select_status_visa'=>$this->_select_status_visa(),
	       		'select_affiliate'=>$this->_select_affiliate(),
	         	'select_affiliate_type'=>$this->_select_affiliate_type(),
	    	);
		$this->template->load('template/template', $this->folder.'/pindah_paket',array_merge($get,$data));

	}

	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->order_by("nama", "asc")->get_where(
		    	'affiliate',array(
		    		'id_affiliate_type'=>$id_affiliate_type,
		    		'status'=>1
		    		)

		    	);
		    $this->db->order_by("nama", "asc");
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama." ".$value->id_user."</option>";
		    }
		    echo $data;
	}


	public function save_pp(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            $userid=$this->input->post('userid'); // mendapatkan input dari kode
            $idsahabat=$this->input->post('idsahabat');
            $cek=$this->cicilanpp_model->cek($idsahabat);
            if($cek->num_rows()>0){ // jika kode sudah ada, maka tampilkan pesan
            	$this->session->set_flashdata('Warning', "ID INI SUDAH DI PINDAH PAKET");
            	$userid = $this->input->post('userid'); 
            	redirect('cicilan_pp/pindah_paket/'.$userid.'');
            }else{
			 
		$data = $this->input->post(null,true);
	   
	 $this->cicilanpp_model->save_pp($data);

	}
}
}
	public function report_registrasi(){  
	    	 $id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $report_registrasi = $this->cicilanpp_model->get_report_registrasi($id_booking,$id);
	    //     show_404();
	    // }
	   
      
	    $data = array(

	       		 'report_registrasi'=>$report_registrasi,
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_pp',($data));
	   
	}



}
