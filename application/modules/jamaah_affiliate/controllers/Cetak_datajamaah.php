<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak_datajamaah extends CI_Controller {
	var $folder = "data_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','data_jamaah');	
		$this->load->model('datajamaah_model');
		
		$this->load->helper('fungsi');
		
	}


	function cetak($id_booking) {
		$row = $this->datajamaah_model->get_data_booking($id_booking);
		$data_booking = $this->datajamaah_model->get_data_booking($row->id_booking);
		$lap_jamaah = $this->datajamaah_model->get_data_jamaahnoaktif($row->id_booking);

		// print_r($lap_jamaah);
		// foreach ($lap_jamaah as $key => $value) {
		// 	echo $value->muhrim;
		// 	# code...
		// }
		// die();
		
		if($row == FALSE) {
			echo 'DATA KOSONG';
        //redirect('angsuran_detail');
			exit();
		}

		// $opsi_val_arr = $this->setting_m->get_key_val();
		// foreach ($opsi_val_arr as $key => $value){
		// 	$out[$key] = $value;
		// }

		$this->load->library('Pdf');
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		// $pdf->set_nsi_header(TRUE);
		$pdf->AddPage('P');
		$html = '';
		$html .= '
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 10pt; font-style: arial;}
		</style>
		'.$pdf->nsi_box($text = '<span class="txt_judul">Detail Registrasi Jamaah <br></span>', $width = '100%', $spacing = '0', $padding = '1', $border = '0', $align = 'center').'
		<table width="100%" cellspacing="0" cellpadding="3" border="1" border-collapse= "collapse">';

			// $anggota = $this->general_m->get_data_anggota($row->anggota_id);
		
			// print_r($lap_jamaah);
			//die();
			// $hitung_denda = $this->general_m->get_jml_denda($row->id);
			// $hitung_dibayar = $this->general_m->get_jml_bayar($row->id);
			// $sisa_ags = $this->general_m->get_record_bayar($row->id);
			// $angsuran = $this->angsuran_m->get_data_angsuran($row->id);

			// $tgl_bayar = explode(' ', $row->tgl_pinjam);
			// $txt_tanggal = jin_date_ina($tgl_bayar[0]);   

			// $tgl_tempo = explode(' ', $row->tempo);
			// $tgl_tempo = jin_date_ina($tgl_tempo[0]); 

			//AG'.sprintf('%05d', $row->anggota_id).'
			$html .='<table width="100%">   
			<tr>
				<td width="18%"> Invoice </td>
				<td width="2%"> : </td>
				<td width="45%"> #'.$data_booking->invoice.'</td>

				<td> Status Pembayaran </td>
				<td width="5%"> : </td>
				<td width="10%" class="h_kanan"> BELUM LUNAS </td>
			</tr>
			<tr>
				<td> Affiliate </td>
				<td> : </td>
				<td> <strong>'.strtoupper($data_booking->affiliate).'</strong></td>

				
			</tr>
			<tr>
				<td> ID Affiliate </td>
				<td> : </td>
				<td> '.$data_booking->id_user_affiliate.'</td>

				
			</tr>
			<tr>
				<td> Alamat </td>
				<td> : </td>
				<td> '.$data_booking->paket.'</td>

				
			</tr>
			<tr>
				<td > Bulan Menunggu </td>
				<td > :  </td>
				<td > '.$data_booking->bulan_menunggu.'</td>

				
			</tr>
			<tr>
				<td> Tanggal Keberangkatan </td>
				<td> : </td>
				<td> '.$data_booking->tgl_keberangkatan.'</td>
			</tr>
			<tr>
				<td> Jumlah Jamaah </td>
				<td> : </td>
				<td> '.$data_booking->jumlah_jamaah.' Orang</td>
			</tr>

			<tr>
				<td> Emabarkasi </td>
				<td> : </td>
				<td> '.$data_booking->departure.' Bulan</td>
			</tr>';
			

			// $tagihan = $row->ags_per_bulan * $row->lama_angsuran;
			// $dibayar = $hitung_dibayar->total;
			// $jml_denda = $hitung_denda->total_denda;
			// $sisa_bayar = $tagihan - $dibayar;
			// $total_bayar = $sisa_bayar + $jml_denda;
			// $sisa_angsuran = $row->lama_angsuran - $sisa_ags;

			
		$html .='<table width="100%" cellspacing="0" cellpadding="3" border="1" border-collapse= "collapse">
		<tr class="header_kolom" >
			<th style=" width:5%;"> No. </th>
			<th style=" width:12%;"> ID Jamaah</th>
			<th style=" width:20%;"> Nama </th>
		
			<th style=" width:10%;"> Biaya Muhrim </th>
			<th style=" width:10%;"> Refund</th>
			<th style=" width:10%;"> Handling  </th>
			<th style=" width:15%;"> Harga Paket </th>
			
			
		</tr>';

		$no=1;
		$t_muhrim = 0;
		$t_refund = 0;
		$t_handling = 0;
		$t_harga_paket = 0;

		if(!empty($lap_jamaah)) {
			foreach ($lap_jamaah as $rows) {
				// $tgl_bayar      = explode(' ', $rows->tgl_bayar);
				// $txt_tanggal    = jin_date_ina($tgl_bayar[0],'p');
				$t_muhrim        += $rows->muhrim;
				$t_refund      += $rows->refund;
				$t_handling        += $rows->handling;
				$t_harga_paket      += $rows->harga_paket;

				$html.= '<tr>
				<td class="h_tengah"> '.$no++.'</td>
				<td class="h_tengah"> '.$rows->id_jamaah.'</td>
				<td class="h_tengah"> '.$rows->nama.'</td>
				<td class="h_tengah"> '.number_format(nsi_round($rows->muhrim)).'</td>
				<td class="h_kanan"> '.number_format(nsi_round($rows->refund)).'</td>
				<td class="h_kanan"> '.number_format(nsi_round($rows->handling)).'</td>
				<td class="h_kanan"> '.number_format(nsi_round($rows->harga_paket)).'</td>
				
			</tr>';
		}
		$html.='

	</table>';
}else{
	$html.='Tidak Ada Data Transkasi';
}

$pdf->nsi_html($html);
$pdf->Output('detail'.date('Ymd_His') . '.pdf', 'I');
}
}