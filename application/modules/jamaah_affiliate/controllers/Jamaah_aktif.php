<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jamaah_aktif extends CI_Controller{
	var $folder = "data_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','jamaah_aktif');	
		$this->load->model('jamaahaktif_model');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/jamaah_aktif');
		
	}



    public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->jamaahaktif_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="10%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->paket.'</td>';
	                $rows .='<td width="10%">'.$r->departure.'</td>';
	                $rows .='<td width="20%">'.$r->tgl_daftar.'</td>';
	                $rows .='<td width="10%">'.$r->tgl_keberangkatan.'</td>';
	                $rows .='<td width="10%">'.$r->jumlah_jamaah.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'data_jamaah/jamaah_aktif/detail/'.$r->id_booking.'">
	                            <i class="fa fa-pencil"></i> Detail
	                        </a> ';
	                  $rows .='<a class="btn btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'registrasi_jamaah/cetak_registrasi/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'data_jamaah/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}



	
	 public function detail(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	    $booking = $this->jamaahaktif_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	        $pic = $this->jamaahaktif_model->get_pic($id_booking);
	    }    

	      
	    $data = array(

	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahaktif',($data));

	}
	






	



}
