<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fee_postingsponsoradmin extends CI_Controller {

    var $folder = "fee_postingsponsoradmin";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(KELOLAH_FEE_POSTING_SPONSOR, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('feepostingsponsoradmin_model');
        $this->load->model('feepostingsponsoradmin_model', 'fee');
    }

    public function index() {
        $this->template->load('template/template', $this->folder . '/fee_postingsponsoradmin');
    }

    public function ajax_list() {
        $list = $this->fee->get_datatables();
        //$this->myDebug($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $fee) {
            $no++;
            $row = array();
            
            $affiliate = $this->db->get_where('affiliate', array('id_user' => $fee->id_affiliate))->row_array();
            $row[] = '<td>'.$no.'</td>';
            $row[] = '<td width="10%">' . $fee->invoice . '</td>';
            $row[] = '<td width="20%" >' . $fee->id_affiliate . ' - ' . $affiliate['nama'] . '</td>';
            $row[] = '<td width="8%">' . $fee->jml_jamaah . '</td>';
            $row[] = '<td width="20%" >' . number_format($fee->total_fee) . ' <br> ' . $fee->pajak . '<br> ' . number_format($fee->potongan_pajak) . '<br> ' . $fee->npwp . '</td>';
            $row[] = '<td width="10%">Rp ' . number_format($fee->potongan_pajak) . '</td>';
            $row[] = '<td width="15%">Rp ' . number_format($fee->jumlah) . '</td>';
            
            if ($fee->status == 2) {
                $row[] = '<td><a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan(' . "'" . $fee->id_booking . "'" . ')"><i class="fa fa-pencil"></i> PROSES</a> </td>';
            } elseif ($fee->status == 3) {
                $row[] = '<td><a title="TRANSFERRED" class="btn btn-sm btn-primary" href="' . base_url() . 'fee_postingsponsoradmin/detail/' . $fee->id_booking . '/' . $fee->id_affiliate . '">
	                            <i class="fa fa-pencil"></i> TRANSFERRED
	                        </a> </td> ';
            }else{
                $row[] = '<td>Sudah Di Transfer</td>';
            }
           
            
            $data[] = $row;
        }

        $output = array(
            "draw" => isset($_POST['draw'])? $_POST['draw'] : '',
            "recordsTotal" => $this->fee->count_all(),
            "recordsFiltered" => $this->fee->count_filtered(),
            "data" => $data,
        );
        //output to json format
        //$this->myDebug($output);
        echo json_encode($output);
    }

    private function _select_bank() {
        return $this->db->get('bank')->result();
    }

    public function detail() {

        if (!$this->general->privilege_check(KELOLAH_FEE_POSTING_SPONSOR, 'edit'))
            $this->general->no_access();

        $id_booking = $this->uri->segment(3);
        $sponsor = $this->uri->segment(4);
        $fee_posting = $this->feepostingsponsoradmin_model->get_pic_posting($id_booking, $sponsor);
        $pic = array();
        // $pic_booking= array();
        // if(!$fee_posting ){
        //     show_404();
        // }
        // else{

        $pic = $this->feepostingsponsoradmin_model->get_pic($id_booking, $sponsor);

        //   }    

        $data = array(
            'dana_bank' => $this->_select_bank(),
            'bank_transfer' => $this->_select_bank(),
            'fee_posting' => $fee_posting, 'pic' => $pic
        );

        $this->template->load('template/template', $this->folder . '/detailpostingsponsor_admin', ($data));
    }

    function save() {
        // $data = $this->input->post(null,true);
        //    $data2 = $this->input->post('checkbox');
        //    $inputan = '';
        //    foreach($data2 as $value){
        //    	$inputan .= ($inputan!=='') ? ',' : '';
        //    	$inputan .= $value;
        //    }
        //    $simpan = $this->feepostingsponsoradmin_model->save($data,$inputan);
        //    if($simpan)
        //        redirect('fee_postingsponsoradmin');

        $id_booking = $this->input->post('id_booking');
        $data = $this->input->post(null, true);
        $data2 = $this->input->post('id_registrasi');
        //$this->myDebug($data);
        // print_r($data);
        if ($this->feepostingsponsoradmin_model->save($data)) {

            // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
            redirect('fee_postingsponsoradmin');
        }
    }

    public function update_pengajuan($id_booking) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->feepostingsponsoradmin_model->update_pengajuan($id_booking);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('fee_postingsponsoradmin');
    }

}
