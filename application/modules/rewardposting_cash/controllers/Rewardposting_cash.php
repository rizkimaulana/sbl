<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rewardposting_cash extends CI_Controller {

    var $folder = "rewardposting_cash";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(REWARD_POSTING_MGM, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'fee_affiliate');
        $this->load->model('rewardpostingcash_model');
    }

    public function index() {
//        $data = array(
//            'pic' => $this->rewardpostingcash_model->get_data_fee('order by datewin ASC')->result_array(),
//        );
        $this->template->load('template/template', $this->folder . '/rewardposting_cash');
        // $this->load->helper('url');
        // $this->load->view('rewardmgm_umroh');
    }

    public function ajax_list() {
        $list = $this->rewardpostingcash_model->get_data_fee()->result();
        //$this->myDebug($list);
        $data = array();
        $no = 0;
        foreach ($list as $reward) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $reward->nama .'<br>'.$reward->id_user;
            $row[] = $reward->reward.'<br>'.number_format($reward->total_reward).'<br>'.$reward->pajak.'%'.'<br>'.$reward->npwp;
            $row[] = number_format($reward->potongan_pajak);
            $row[] = number_format($reward->jumlah);
           
            if ($reward->status == 1) {
                $row[] = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan('.$reward->id.')"> APPROVED</a>';
            }elseif ($reward->status == 2) {
                $row[] = '<a class="btn btn-sm btn-info" href="'.base_url().'rewardposting_cash/detail/'.$reward->id.'" title="TRANSFER" > TRANSFERED </a>';
            }else{
                 $row[] = 'Sudah di transfer';
            }
            
            $data[] = $row;
        }

        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->fee->count_all(),
//            "recordsFiltered" => $this->fee->count_filtered(),
            "data" => $data,
        );
        
        //output to json format
        echo json_encode($output);
    }

    private function _select_bank() {

        return $this->db->get('bank')->result();
    }

    public function detail() {

        if (!$this->general->privilege_check(REWARD_POSTING_MGM, 'edit'))
            $this->general->no_access();
        $data = array();
        $id_reward = $this->uri->segment(3);
        //$fee_posting = $this->rewardpostingcash_model->get_pic_posting($id_reward);
        $reward_posting = $this->rewardpostingcash_model->get_data_fee($id_reward)->row_array();
//        //$this->myDebug($this->db->last_query());
//        // $pic    = array();
        $pic_booking = array();
        if (!$reward_posting) {
            show_404();
        }

        $data = array(
            //'dana_bank' => $this->_select_bank(),
            //'bank_transfer' => $this->_select_bank(),
            'reward_posting' => $reward_posting
        );
        
        //$this->myDebug($data);
        $this->template->load('template/template', $this->folder . '/detailfeeposting_admin',($data));
    }

    function save() {
        $data = $this->input->post(null, true);
        // print_r($data);
        if ($this->rewardpostingcash_model->save($data)) {
            // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
            redirect('rewardposting_cash');
        }
    }

    public function update_pengajuan($id) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->rewardpostingcash_model->update_pengajuan($id);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('rewardposting_cash');
    }

}
