
<style type="text/css">
    body {
        font-family: verdana,arial,sans-serif;
        margin: 0px;
        padding: 0px;
    }

    .wrap { 
        width:50%; 
        background:#F0F0F0; 
        margin:auto;
        padding: 25px;
        overflow: hidden;
    }

    h1 {
        text-align: center;
    }

    input.pemberangkatan {
        font-size:28px; 
        width:380px;
    }

    input, textarea {
        border: 1px solid #CCC;
    }
</style>

<div id="page-wrapper">
    <form   class="form-horizontal" role="form" action='<?= base_url(); ?>rewardposting_cash/save' method='POST' >
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Detail Jamaah Fee MGM </b>
                    </div>
                    <div class="panel-body">                       
                        <input type="hidden" name="id_user" value="<?php echo $reward_posting['id_user'] ?>">
                        <input type="hidden" name="id" value="<?php echo $reward_posting['id_reward'] ?>">

                        <div class="row">
                            <div class="col-lg-10">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"for="inputPassword3" >Transfer Date</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="transfer_date" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"   placeholder="YYYY-MM-DD" />
                                        </div>
                                    </div>
                                    <!--<div class="form-group">
                                        <label class="col-lg-2 control-label">Dari Dana Bank </label>
                                        <div class="col-lg-10">
                                            <select required class="form-control" name="dana_bank">
                                                <option value="">- Select Bank -</option>
                                                <?php foreach ($dana_bank as $sb) { ?>
                                                    <option value="<?php echo $sb->nama; ?>"><?php echo $sb->nama; ?></option>
                                                <?php } ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Bank Transfer </label>
                                        <div class="col-lg-10">
                                            <select required class="form-control" name="bank_transfer">
                                                <option value="">- Select Bank -</option>
                                                <?php foreach ($bank_transfer as $sb) { ?>
                                                    <option value="<?php echo $sb->nama; ?>"><?php echo $sb->nama; ?></option>
                                                <?php } ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">No Rekening</label>
                                        <div class="col-lg-10">
                                            <input name="no_rek" class="form-control" > 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Atas Nama Rekening</label>
                                        <div class="col-lg-10">
                                            <input name="nama_rek" class="form-control"  > 
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Nominal</label>
                                        <div class="col-lg-10">
                                            <input name="nominal" class="form-control" value="Rp <?php echo number_format($reward_posting['jumlah']); ?>" readonly="true"  > 
                                            <input type="hidden" name="reward" class="form-control" value="<?php echo $reward_posting['jumlah']; ?>"  > 
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Keterangan</label>
                                        <div class="col-lg-10">
                                            <textarea class="form-control" name="keterangan" ></textarea>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Approved</button>
                                    <a href="<?php echo base_url(); ?>rewardposting_cash"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>    
</div>

<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable({
            "pagingType": "full_numbers",
            "iDisplayLength": 45});

    });
</script>

<script type="text/javascript">

    $('#datepicker2').datetimepicker({
        format: 'YYYY-MM-DD',
    });

</script>

