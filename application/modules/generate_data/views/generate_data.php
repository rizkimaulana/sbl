<div id="page-wrapper">
  <div class="row">
   <div class="col-lg-12">
        
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>GENERATE Data Jamaah </b>
        </div>
        <div class="panel-body">
        <form  role="form" method="POST"  class="form-horizontal" action="<?php echo base_url();?>generate_data/save_mutasi">
        <!-- <form  role="form" method="POST"  class="form-horizontal" > -->
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>GENERATE Jamaah</b>
                </div>
                 <?php if($this->session->flashdata('Warning')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Warning! </strong><?php echo $this->session->flashdata('Warning'); ?>  
                                  </div>
                              <?php } ?>
                <!-- /.panel-heading -->
                  <div class="panel-body">
                  

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Embarkasi</label>
                      <div class="col-lg-5">
                      <select required class="form-control" name="departure" id="departure" >
                          <option value="">Pilih Embarkasi</option>
                          <?php foreach($select_embarkasi as $se){?>
                              <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                 <!--   <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div> -->
                <div class="form-group">
                          <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' id='datepicker_tahun_keberangkatan1'> -->
                                <input type='text' name="datepicker_tahun_keberangkatan" id="datepicker_tahun_keberangkatan1" class="form-control" placeholder="YYYY/2016" required/>
                             <!--    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                             
                          </div>
                    </div>

                     <div class="form-group">
                          <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                <input type='text' name="datepicker_keberangkatan" id="datepicker_keberangkatan1" class="form-control" placeholder="MM/January" required/>
                               <!--  <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                          </div>

                    </div>
                    
             
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-10">
                        <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check Waktu Keberangkatan</button>
                         
                    </div>
                  </div>
                
                  <div class="table-responsive">
                   <table id="data-table" style="width:100%" align="center" class="table table-striped table-bordered table-hover" >
                    <caption>Pilih Jadwal</caption>
                    <thead>
                        <tr>
                            <!-- <th width="2%">Check </th> -->
                            <th width="8%"><strong>Paket</strong></th>
                            <th width="10%"><strong>Embarkasi</strong></th>
                            <th width="10%"><strong>Tgl/Bln </strong></th>
                            <th width="8%"><strong>Waktu </strong></th>
                            <th width="5%"><strong>Jml Kursi</strong></th>
                            <th width="5%"><strong>Tipe Kamar</strong></th>
                        
                            
                            <!-- <th width="5%"><strong>Status Jadwal</strong></th> -->
                            <th width="5%"><strong>PILIH</strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <tr><td colspan="11"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>

                    </tbody>
                </table>
                </div>
                   

                   <div class="form-group">
                    <label class="col-lg-2 control-label">Affiliate Type : </label>
                    <div class="col-lg-7">
                  
                        <select name="select_affiliate_type" id="select_affiliate_type" class="form-control " onchange="select_affiliate_type(this.options[this.selectedIndex].value)">
                        <option value="0">PILIH AFFILIATE TYPE</option>
                        <?php
                        foreach($list->result() as $listElement){
                          ?>
                          <option value="<?php echo $listElement->id_affiliate_type?>"><?php echo $listElement->affiliate_type?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                        <label class="col-lg-2 control-label">Affiliate  : </label>
                          <div class="col-lg-7">
                        
                              <select required name="id_user" id="affiliate" class="form-control " onchange="select_affiliate(this.options[this.selectedIndex].value)">
                      <option value="0">- PILIH AFFILIATE -</option>
                    </select>
                    </div>
                  </div>
                <div class="form-group">
                    <label  class="col-sm-2 control-label" >Nama</label>
                    <div class="col-sm-10">
                        <input name="nama" type="text" class="form-control" id="nama" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3" >Tempat Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Tanggal Lahir</label>
                    <div class="col-sm-10">
                     
                         <input type="text" data-mask="date" class="form-control" name="tanggal_lahir" id="datepicker" value/>
                
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                            id="no_identitas" name="no_identitas"  required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Provinsi</label>
                    <div class="col-sm-10">
                          <select required name="provinsi_id" class="form-control " id="provinsi" >
                                <option  value="">- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                         
                            
                        
                    </div>

                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Kabupaten</label>
                    <div class="col-sm-10">
                       <select required name="kabupaten_id" class="form-control" id="kabupaten">
                                <option required value=''>Select Kabupaten</option>
                            </select>
                       
                            <div class="col-sm-5">
                            
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Kecamatan</label>
                    <div class="col-sm-10">
                        <select required name="kecamatan_id" class="form-control" id="kecamatan">
                                <option required value=''>Select Kecamatan</option>
                            </select>
                            <div class="col-sm-5">
                           
                            </div>
                        
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Alamat</label>
                    <div class="col-sm-10">
                        <textarea  name="alamat" class="form-control"   required></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Telp</label>
                    <div class="col-sm-10">
                        <input id="telp" name="telp" type="type" class="input_telp form-control" onKeyPress="return numbersonly(this, event)" placeholder="Input No. Telp (harus karakter angka)"  required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Email</label>
                    <div class="col-sm-10">
                        <input  name="email" type="text" class="form-control"  >
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select required class="form-control" id="jenis_kelamin" name="select_kelamin">
                        <option value="">- PILIH JENIS KELAMIN -</option>
               
                         <?php foreach($select_kelamin as $sj){?>
                              <option value="<?php echo $sj->kdstatus;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                      </select>
                     
                    </div>
                  </div>

                  <div class="form-group">
                     <label class=" col-sm-2 control-label" >Rentang Umur</label>
                     <div class="col-sm-10">
                    <select required class="form-control" name="select_rentangumur">
                      <option value="">- PILIH RENTANG UMUR -</option>
                

                       <?php foreach($select_rentangumur as $sj){?>
                              <option value="<?php echo $sj->id_rentang_umur;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-2 control-label" for="status">Status :</label>
                   <div class="col-sm-10">
                  <select required class="select_status form-control" name="select_statuskawin">
                    <option value="">- PILIH STATUS -</option>
                  

                       <?php foreach($select_statuskawin as $sj){?>
                              <option value="<?php echo $sj->kdstatus;?>"><?php echo $sj->keterangan;?></option>
                          <?php } ?>
                  </select>
                </div>
              </div>

   
 
                   <div class="form-group">
                      <label class="col-sm-2 control-label">Berangkat Dari</label>
                        <div class="col-sm-10">
                          <select required name="pemberangkatan" class="form-control chosen-select" id="pemberangkatan">
                            <option value="">PILIH BERANGKAT DARI BANDARA</option>
                            <?php foreach($select_pemberangkatan as $select_pemberangkatan):?>
                            <option value="<?php echo $select_pemberangkatan->pemberangkatan;?> "><?php echo $select_pemberangkatan->pemberangkatan;?></option>
                            <?php endforeach;?>
                          </select>
                          <input  type="hidden" class="form-control" id="id_bandara" name="id_bandara"  readonly="true" />
                          <!-- <input type="text" class="form-control" id="refund" name="refund" readonly="true"   /> -->
                          <input type="hidden" class="form-control" id="akomodasi" name="akomodasi"  readonly="true"  />
                          <input type="hidden" class="form-control" id="handling" name="handling"  readonly="true" />
                        </div> 
                      </div>



               
                   
                  <div class="form-group">
                      <label class="col-sm-2 control-label"
                            for="inputPassword3" >Ahli Waris</label>
                      <div class="col-sm-10">
                          <input id="waris" name="waris" type="text" class="input_telp form-control"  required>
                      </div>
                  </div>

                  <div class="form-group">
                         <label class="col-sm-2 control-label" >Hubungan Ahli waris </label>
                         <div class="col-sm-10">
                        <select required class="form-control" name="select_hub_ahliwaris">
                          <option  value="">- PILIH Hubungan Ahli Waris -</option>

                          <?php foreach($select_hub_ahliwaris as $sj){?>
                              <option value="<?php echo $sj->id_hubwaris;?>"><?php echo $sj->hubungan;?></option>
                          <?php } ?>
                        </select>
                                            
                      </div>
                    </div>

                       <div class="form-group">
                         <label class="col-sm-2 control-label" for ="select_merchandise">Merchandise </label>
                         <div class="col-sm-10">
                        <select required class="select_merchandise form-control" name="select_merchandise">
                          <option  value="">- PILIH MERCHANDISE -</option>
      
                            <?php foreach($select_merchandise as $sj){?>
                              <option value="<?php echo $sj->id_merchandise;?>"><?php echo $sj->merchandise;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >No Pasport</label>
                        <div class="col-sm-10">
                          <input name="no_pasport" type="text" id="no_pasport" class="input_keluarga form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >issue office</label>
                        <div class="col-sm-10">
                          <input name="issue_office" type="text" id="issue_office"class="input_keluarga form-control" >
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3" >Isue Date</label>
                    <div class="col-sm-10">
                    
                         <input type="text" class="form-control" name="isue_date" id="datepicker2"    />
                
                    </div>
                  </div>
                  <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Kartu Identitas</label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_identitas" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_identitas" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Kartu Keluarga </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_kk" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_kk" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                     <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Pas Photo </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_photo" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_photo" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Pasport </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_pasport" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_pasport" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Vaksin </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_vaksin" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_vaksin" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-lg-4 control-label">FotoCopy Buku Nikah </label>
                        <div >

                             <label class="radio-inline">
                              <input type="radio" name="status_buku_nikah" required value="1">Sudah Ada
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status_buku_nikah" required value="0">Belum Ada
                            </label>
                            
                        </div>
                    </div>
                    
                  </div>
         

     

                   <div class="col-sm-6 col-md-12">
                <div class="block-flat">
                  <div class="header">              
                    <h4><strong>VISA</strong> APAKAH JAMAAH INI PERNAH BERANGKAN UMBROH SEBELUMNYA ?</h4>
                  </div>
                   <div class="content">
                     <div class="form-group">
                        <div class="col-sm-12">  
                            <select required class="form-control" name="select_status_visa" id="select_status_visa" >
                              <option value="">- PILIH -</option>
                                <?php foreach($select_status_visa as $sk){?>
                                    <option value="<?php echo $sk->id;?>"><?php echo $sk->ket;?></option>
                                <?php } ?>
                            </select>
                        </div>
                      </div>
              <div id="hidden_div2" style="display: none;">

                       <div class="col-sm-10">
                                <div class='input-group date' id='datepicker1'>
                                 <input type="text" data-mask="date" class="form-control" name="tanggal_visa" id="tanggal_visa" placeholder="YYYY-MM-DD " required/>
                                   <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar">
                                      </span>
                                  </span>
                                 </div> 
                              </div>
                </div>    
              </div>
            </div>
           </div> 

            <div class="form-group">
                <label class="col-lg-2 control-label">Jumlah SEATS </label>
                <div class="col-lg-5">
                <select required name="jml_seats"  class="form-control ">
               <?php 
                  for($i=3; $i<=300; $i++){
               ?>
             <option value="<?php echo $i;?>"><?php echo $i;?></option>
              <?php
               }
              ?>
             </select> 
           </div>
            </div> 
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            <a href="<?php echo base_url();?>bilyet_sahabat"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>

                   </div>
                </div>

              </div>
            </div><!--end col-->




        </form>
           
           </div>

     </div>
     </div>
     </div>
  </div><!--end row-->
</div>
 <script type="text/javascript">
function numbersonly(myfield, e, dec) { var key; var keychar; if (window.event) key = window.event.keyCode; else if (e) key = e.which; else return true; keychar = String.fromCharCode(key); // control keys 
if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) return true; // numbers 
else if ((("0123456789").indexOf(keychar) > -1)) return true; // decimal point jump 
else if (dec && (keychar == ".")) { myfield.form.elements[dec].focus(); return false; } else return false; }

</script>
<script type="text/javascript">
  $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('generate_data/get_biaya_refund_');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#refund").val(html);
                    
                }
            })
        })


            $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('generate_data/get_biaya_refund_2');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#id_bandara").val(html);
                    
                }
            })
        })


        $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('generate_data/get_biaya_refund_3');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#akomodasi").val(html);
                    
                }
            })
        })

         $("#pemberangkatan").change(function(){
            var pemberangkatan=$("#pemberangkatan").val();
            console.log(pemberangkatan);
            $.ajax({
                url:"<?php echo site_url('generate_data/get_biaya_refund_4');?>",
                type:"POST",
                data:"pemberangkatan="+pemberangkatan,
                cache:false,
                success:function(html){
                    $("#handling").val(html);
                    
                }
            })
        })
</script>

<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker_keberangkatan1').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });

            });
  $("#select_affiliate_type").change(function (){
                var url = "<?php echo site_url('generate_data/add_ajax_affiliate_type');?>/"+$(this).val();
                $('#affiliate').load(url);
                return false;
            })

   function select_affiliate_type(id_affiliate_type){
  if(id_affiliate_type!="-1"){
    loadData('affiliate',id_user);
    $("#select_affiliate_type").html("<option value='-1'>Select city</option>");  
  }else{
    $("#select_affiliate").html("<option value='-1'>Select state</option>");
    
  }
}
    </script>

    <script type="text/javascript">
      $(function () {
            
          $('#datepicker1').datetimepicker({
          format: 'YYYY-MM-DD',
          viewMode: 'years'
        });
        $('#tanggal_visa').datetimepicker({
          format: 'YYYY-MM-DD',
        });

            });
    </script>

<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('generate_data/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('generate_data/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('generate_data/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });

    $("#check").on('click', function (e){
    e.preventDefault();

    var departure = $("#departure").val();
    var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
    var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();

    console.log(departure);
    console.log(datepicker_tahun_keberangkatan);
    console.log(datepicker_keberangkatan);
    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>generate_data/searchItem',
             // data:'from='+from+'&to='+to
              data:'s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan+'&l='+departure
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});


   // document.getElementById('select_status_hubungan').addEventListener('change', function () {
   //  var style = this.value == 8 ? 'block' : 'none';
   //  document.getElementById('hidden_div').style.display = style;
   //  });



            

</script>
<script type="text/javascript">


</script>
 <script type="text/javascript">
  jQuery(document).ready(function(){
  $(".chosen-select").chosen({width: "95%"}); 
});

 </script>





<script type="text/javascript">
      document.getElementById('select_status_visa').addEventListener('change', function () {
    var style = this.value == 1 ? 'block' : 'none';
    document.getElementById('hidden_div2').style.display = style;
    // document.getElementById('hidden_div2').style.display = style;
    // $("#datepicker").prop('required',true);
     select_status_visa = $(this).val();

      console.log(select_status_visa);

      if(select_status_visa=='1' ){
        $("#tanggal_visa").prop('required',true);

      }else{
         $("#tanggal_visa").prop('required',false);
      }
    });


</script>

</script> <script src="<?php echo base_url();?>assets/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    $("[data-mask='date']").mask("9999-99-99");
    $("[data-mask='phone']").mask("(999) 999-9999");
    $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
    $("[data-mask='phone-int']").mask("+33 999 999 999");
    $("[data-mask='taxid']").mask("99-9999999");
    $("[data-mask='ssn']").mask("999-99-9999");
    $("[data-mask='product-key']").mask("a*-999-a999");
    $("[data-mask='percent']").mask("99%");
    $("[data-mask='currency']").mask("$999,999,999.99");
  });
</script>