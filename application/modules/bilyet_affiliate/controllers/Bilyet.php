<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bilyet extends CI_Controller{
	var $folder = "bilyet";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(INFO_BILYET,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','bilyet');	
		$this->load->model('bilyet_model');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/bilyet');
		
	}

	public function bilyet_barcode(){
	
		
	   // $this->template->load('template/template', $this->folder.'/bilyet_barcode');
		$this->load->view('bilyet_barcode');
		
	}
	public function cetak(){
	
		
	   // $this->template->load('template/template', $this->folder.'/bilyet_barcode');
		$this->load->view('cetak');
		
	}

    function pdf_bilyet_barcode(){
    	
        // load dompdf
        $this->load->helper('dompdf');
        //load content html
        $html = $this->load->view('bilyet_barcode', '', true);
        // create pdf using dompdf
        $filename = 'bilyet_barcode';
        $paper = 'A4';
        $orientation = 'potrait';
        pdf_create($html, $filename, $paper, $orientation);
    }
    
     function gambar($kode){

		   $height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '25';  $width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst

		 $this->load->library('zend');

		        $this->zend->load('Zend/Barcode');

		  $barcodeOPT = array(

		     'text' => $kode,

		     'barHeight'=> $height,

		     'factor'=>$width,

		 );

		  $renderOPT = array();

		 $render = Zend_Barcode::factory(

		'code128', 'image', $barcodeOPT, $renderOPT

		)->render();
    }
 
  public function data_jamaahaktif(){
	
		
	   $this->template->load('template/template', $this->folder.'/data_jamaah_aktif');
		
	}

    public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->bilyet_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%" >'.$r->id_jamaah.'</td>';
	                $rows .='<td width="10%" >'.$r->nama.'</td>';
	                $rows .='<td width="30%">'.$r->keterangan.'</td>';
	                $rows .='<td width="10%">'.$r->menerima.'</td>';
	                $rows .='<td width="10%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->user.'</td>';
	                $rows .='<td width="10%">'.$r->create_date.'</td>';
	             
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Log Bilyet" class="btn sm btn-primary" href="'.base_url().'bilyet/detail_bilyet/'.$r->id_jamaah.'">
	                            <i class="fa fa-pencil"></i> Log Bilyet
	                        </a> ';
	                  $rows .='<a class="btn sm btn-success"  title="SERAHTERIMA"  href="'.base_url().'bilyet/serah_terima/'.$r->id_jamaah.'"></i> SERAHTERIMA</a> ';
	               	
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="10">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'bilyet/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}



	 public function detail_bilyet(){

	    if(!$this->general->privilege_check(INFO_BILYET,'view'))
		    $this->general->no_access();
	    
	    $id_jamaah = $this->uri->segment(3);
	    $bilyet = $this->bilyet_model->get_pic_log_bilyet($id_jamaah);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        show_404();
	    }
	    else{

	        $pic = $this->bilyet_model->get_pic_log_bilyet($id_jamaah);
	    }    

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 'pic'=>$pic
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/log_bilyet',($data));

	}



	

	private function _select_user(){
		
		return $this->db->get('user')->result();
	}
	 public function serah_terima(){
	


	    if(!$this->general->privilege_check(INFO_BILYET,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('view_bilyet',array('id_jamaah'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   			 $data = array('select_user'=>$this->_select_user(),
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit_bilyet',array_merge($get,$data));

	}
	
	public function update(){
	    
	    $data = $this->input->post(null,true);
	    				
		

	  
	   $send = $this->bilyet_model->update($data);
	   if($send)
	     // $id_booking = $this->input->post('id_booking');
            	redirect('bilyet');

	// print_r($_POST);

	}

	

	
}
