<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datamanasik_perwakilan extends CI_Controller{
	var $folder = "datamanasik_perwakilan";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','jamaah_registration');	
		$this->load->model('datamasnasikperwakilan_model');
		$this->load->library('terbilang');
		$this->load->helper('fungsi');
	}
	
	public function index(){
	
		// $id=  $this->session->userdata('id_user');
	 //    $pic = $this->datamasnasikperwakilan_model->get_pic_manifest($id);

	 //    if(!$pic){
	 //        show_404();
	 //    }
	    
	      
	 //    $data = array(


		//     	// 'select_keluarga'=>$this->_select_keluarga($id_booking),
		//     	'family_relation'=>$this->_family_relation(),
	 //       		 'pic'=>$pic,
	       	
	 //       		 );


	    $this->template->load('template/template', $this->folder.'/datamanasik_perwakilan' );	
		
	}

	function searchItem_bulankeberangkatan(){
            
			 
             $filter = $this->input->post('s');

             if(!empty($filter)){
                 $this->datamasnasikperwakilan_model->searchItem_bulankeberangkatan($filter);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

    function searchItem(){
            
			 
             $search = $this->input->post('s');

             if(!empty($search)){
                 $this->datamasnasikperwakilan_model->searchItem($search);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

	    public function get_data(){
	     // $limit = 5;
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	     $filter= isset($_POST['filter']) ? $_POST['filter'] : '';
	    $data  = $this->datamasnasikperwakilan_model->get_data($offset,$limit,$q,$filter);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="20%">'.$r->id_jamaah.'<br>
	              	'.$r->nama.'<br>
	              	'.$r->bulan_keberangkatan.'
	              	</td>';
	                $rows .='<td width="10%">'.$r->tempat_lahir.' <br>
	                '.$r->tanggal_lahir.'</td>';
	                $rows .='<td width="15%">'.$r->nama_passport.'<br>
	                '.$r->no_pasport.'<br>
	                '.$r->issue_office.'<br>
	                '.$r->isui_date.'
	                </td>';
	                $rows .='<td width="10%">'.$r->ket_hubkeluarga.'<br>
	                '.$r->keluarga.'
	                </td>';
	                $rows .='<td width="20%">KTP = '.$r->status_identitas.'<br>
	                KrtKlrg = '.$r->status_kk.' <br>
	                Photo = '.$r->status_photo.' <br>
	                Pport = '.$r->status_pasport.'<br>
	                Vaksin = '.$r->status_vaksin.' <br>
	                BNikah = '.$r->status_buku_nikah.'<br>
	                Akte = '.$r->status_akte.'<br>
	                </td>';
	                $rows .='<td width="20%">TGLKTP = '.$r->tgl_status_identitas.'<br>
	                TGLKrtKlrg = '.$r->tgl_status_kk.' <br>
	                TGLPhoto = '.$r->tgl_status_photo.' <br>
	                TGLPport = '.$r->tgl_status_pasport.'<br>
	                TGLVaksin = '.$r->tgl_status_vaksin.' <br>
	                TGLBNikah = '.$r->tgl_status_buku_nikah.'<br>
	                TGLAkte = '.$r->tgl_akte.'
	                </td>';

	                $rows .='<td width="10%">'.$r->ket_manifest.'</td>';

	                $rows .='<td width="10%" align="center">';
	                
	                $rows .='<a title="Update"  href="#">
	                            <i ></i> Update
	                        </a> ';   
	                // $rows .='<a title="Detail" class="btn-sm btn-primary" href="'.base_url().'data_jamaah/detail/'.$r->id_booking.'">
	                //             <i ></i> Detail
	                //         </a> ';
	                  // $rows .='<a class="btn btn-sm btn-danger"  title="cetak" target="_blank" href="'.base_url().'data_jamaah/cetak_datajamaah/cetak/'.$r->id_booking.'"><i class="glyphicon glyphicon-print"></i> Cetak</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="24">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'datamanasik_perwakilan/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}

     private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}
	private function _select_keluarga($id_booking=''){

		$this->db->where_in('id_booking', $id_booking);
	    return $this->db->get('registrasi_jamaah')->result();
	}
	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	
	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_status_fee(){
	    $id_status_claim_fee = array('0','1', '2','5');
		$this->db->where_in('id_aktivasi_manual', $id_status_claim_fee);
	    return $this->db->get('status_aktivasi_manual')->result();
	}
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}

	



	public function setting_keluarga(){
	


	   if(!$this->general->privilege_check_affiliate(JAMAAH_REGISTRATION_FOR_AFFILIATE,'view'))
    		$this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);

	        // $pic_booking = $this->registrasiaffiliate_model->get_pic_booking($id_booking);
	        $pic = $this->registrasiaffiliate_model->get_pic_keluarga($id_booking);
	      

	      
	    $data = array(


		    	'select_keluarga'=>$this->_select_keluarga($id_booking),
		    	'family_relation'=>$this->_family_relation(),
	       		 'pic'=>$pic,
	       		
	       	
	       		 );
	 
	    $this->template->load('template/template', $this->folder.'/setting_keluarga',($data));

	}

	
}
