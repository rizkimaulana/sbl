


<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>LENGKAPI DATA MANIFEST</b>
                </div>

                 <div class="panel-body">
                   
                        <div class="form-group input-group col-lg-4">
                               
                                <input type="text" class="form-control" id="search" placeholder="Cari Berdasarkan ID JAMAAH..." x-webkit-speech>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="check"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div>
                             <div class="form-group input-group col-lg-4">
                               
                                <input type='text' name="datepicker_keberangkatan" id="filter" class="form-control" placeholder="Cari Berdasarkan Bulan Keberangkatan " required/>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="check_bulankeberangkatan"><i class="fa fa-search"></i></button>
                                </span>
                              
                            </div>
                         
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <!-- <th>No</th> -->
                                   <th>DataJamaah</th>
                                  <th>TTG</th>
                                   <th>Data Passport</th>
                                  <th>HubKeluarga</th>
                                  <th>PhotoCopy</th>
                                  <th>TGL_TerimaPhotoCopy</th>
                                  <th>KetManifest</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                <tbody id='response-table'>
                        <tr><td colspan="8"><h2 style="color: #f5b149">Cari Data Pindah Paket</h2></td></tr>

                    </tbody>
                     </table>
                   </div>
                  
                      <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>
 <script>

     $("#check").on('click', function (e){
    e.preventDefault();
    var search = $("#search").val();

    console.log(search);

    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>datamanasik_perwakilan/searchItem',
             // data:'from='+from+'&to='+to
            data:'s='+search
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});


     $("#check_bulankeberangkatan").on('click', function (e){
    e.preventDefault();
    var filter = $("#filter").val();

    console.log(filter);

    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>datamanasik_perwakilan/searchItem_bulankeberangkatan',
             // data:'from='+from+'&to='+to
            data:'s='+filter
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});

// function get_data(url,q,filter){
        
//         if(!url)
//             url = base_url+'datamanasik_perwakilan/get_data';
        
//         $.ajax({
            
//             url:url,type:'post',dataType:'json',
//             data:{q:q,filter:filter},
//             success:function(result){
                
//                 $("#data-table tbody").html(result.rows);
//                 $("ul.pagination").html(result.paging);
//                 $(".page-info").html(result.page_info);
//             }
        
//         });
//     } 
//     function do_search(){
    
//          var flter = $("#filter").val();         
//         get_data('',$("#search").val(),flter);
//         // alert ($(this).val());
//         // $("#search").val('');

//     }
//     $(function(){
    
//         get_data();//initialize
        
//         $(document).on('click',"ul.pagination>li>a",function(){
        
//             var href = $(this).attr('href');
//             get_data(href);
            
//             return false;
//         });
        
//         $("#search").keypress(function(e){
            
//             var key= e.keyCode ? e.keyCode : e.which ;
//             if(key==13){ //enter
                
//                 do_search();
//                 // $(this).val('');

//             }
//             // if(do_search());{ 
//             // alert ($(this).val());
//             //  }



//         });
        
//         $("#btn-search").click(function(){
            
//             do_search();
             
//             return false;

//         });
        
//     });

</script>
<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
</script>
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
      
       $(function () {
                $('#filter').datetimepicker({
          format: 'MMMM YYYY',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
    </script>