<div id="page-wrapper">
    
    
       <div class="row">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Edit Affiliate</h3>

        	<form action="<?php echo base_url();?>affiliate/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
             <!-- <div class="col-lg-6"> -->
             <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <b>Data Koordinator</b>
                </div> -->
                <!-- /.panel-heading -->
               <!--  <div class="form-group">
			        <label class="col-lg-2 control-label">Image</label>
			        <div class="col-lg-5">
			        	
			            <img src="<?php echo base_url('./assets/images/affiliate/'.$detail['pic1']);?>" width="200px" height="200px">
			        </div>
			    </div>
	 -->
                <div class="panel-body">

                 <div class="form-group">
	                    <label class="col-lg-2 control-label" >Jamaah dari PERWAKILAN/CABANG</label>
	                    <div class="col-lg-5">
	                      <input name="affiliate" value="<?php echo $detail['id_affiliate'];?> - <?php echo $detail['affiliate'];?>" class="form-control" readonly="readonly">
	                    </div>
	                </div>  
                	 <div class="form-group">
	                    <label class="col-lg-2 control-label" >ID Jamaah</label>
	                    <div class="col-lg-5">
	                      <input name="id_jamaah" value="<?php echo $detail['id_jamaah'];?>" class="form-control" readonly="readonly">
	                    </div>
	                </div>

                	<div class="form-group">
		                    <label class="col-lg-2 control-label">NO PASSPORT</label>
		                  
		                 <div class="col-lg-5">
	                      <input type="text" name="no_pasport" value="<?php echo $detail['no_pasport'];?>" class="form-control" readonly="readonly">
	                    </div>
	                  </div>  
                		 
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label" >NAMA</label>
	                    <div class="col-lg-5">
	                      <input name="nama" class="form-control" value="<?php echo $detail['nama'];?>" readonly="readonly">
	                    </div>
	                </div>
		                <div class="form-group">
		                    <label class="col-lg-2 control-label" >Nama di PASSPORT</label>
		                    <div class="col-lg-5">
		                      <input name="nama_passport" class="form-control" value="<?php echo $detail['nama_passport'];?>" readonly="readonly" >
		                    </div>
		                </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">ISSUE OFFICE</label>
	                    <div class="col-lg-5">
	                      <input name="issue_office" class="form-control" value="<?php echo $detail['issue_office'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">ISSUE DATE</label>
	                    <div class="col-lg-5">
	                      <input name="isui_date" class="form-control" value="<?php echo $detail['isui_date'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">KELAMIN</label>
	                    <div class="col-lg-5">
	                      <input name="kelamin" class="form-control" value="<?php echo $detail['kelamin'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">TANGGAL LAHIR</label>
	                    <div class="col-lg-5">
	                      <input name="tanggal_lahir" class="form-control" value="<?php echo $detail['tanggal_lahir'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">TEMPAT LAHIR</label>
	                    <div class="col-lg-5">
	                      <input name="tempat_lahir" class="form-control" value="<?php echo $detail['tempat_lahir'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">HUBUNGAN KELUARGA</label>
	                    <div class="col-lg-5">
	                      <input name="family_relation" class="form-control" value="<?php echo $detail_keluarga['family_relation'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">NAMA KELUARGA</label>
	                    <div class="col-lg-5">
	                      <input name="keluarga" class="form-control" value="<?php echo $detail_keluarga['keluarga'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">KETERANGAN KELUARGA</label>
	                    <div class="col-lg-5">
	                      <input name="ket_keluarga" class="form-control" value="<?php echo $detail['ket_keluarga'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                <div class="form-group">
	                    <label class="col-lg-2 control-label">STATUS PASSPORT</label>
	                    <div class="col-lg-5">
	                      <input name="status_pasport" class="form-control" value="<?php echo $detail['status_pasport'];?>" readonly="readonly">
	                    </div>
	                  </div>

	                    
	                  <div class="form-group">
	                   
	                   <label class="col-lg-2 control-label">STATUS KARTU IDENTITAS / KTP</label>
	               
		                <div class="col-lg-5">
	                      <input name="status_identitas" value="<?php echo $detail['status_identitas'];?>" class="form-control" readonly="readonly">
	                    </div>

	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">STATUS KARTU KELUARGA</label>
	                    
		                <div class="col-lg-5">
	                      <input name="status_kk" value="<?php echo $detail['status_kk'];?>" class="form-control" readonly="readonly">
	                    </div>

	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">STATUS PHOTO</label>
	                    <div class="col-lg-5">
	                      <input name="status_photo" class="form-control" value="<?php echo $detail['status_photo'];?>" readonly="readonly">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">STATUS VAKSIN</label>
	                    <div class="col-lg-5">
	                      <input name="status_vaksin" class="form-control" value="<?php echo $detail['status_vaksin'];?>" readonly="readonly"> 
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">STATUS BUKU NIKAH</label>
	                    <div class="col-lg-5">
	                      <input name="status_buku_nikah" class="form-control" value="<?php echo $detail['status_buku_nikah'];?>" readonly="readonly"> 
	                    </div>
	                  </div>
	                   
	                  
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">STATUS AKTE</label>
	                    <div class="col-lg-5">
	                      <input name="status_akte" class="form-control" value="<?php echo $detail['status_akte'];?>"  readonly="readonly"> 
	                    </div>
	                  </div>
	                   
	                  
	                

                	 <div class="form-group">
			        <label class="col-lg-2 control-label">Image PASSPORT</label>
			        <div class="col-lg-5">
			            
			            <img src="<?php echo base_url('./assets/images/foto_copy/' . $detail['pic1']); ?>" width="200px" height="200px" >
			        </div>
			    </div>

			     <div class="form-group">
			        <label class="col-lg-2 control-label">Image KARTU IDENTITAS/MANIFEST</label>
			        <div class="col-lg-5">
			            	
			            <img src="<?php echo base_url('./assets/images/foto_copy/' . $detail['pic2']); ?>" width="200px" height="200px" >
			        </div>
			    </div>

			     <div class="form-group">
			        <label class="col-lg-2 control-label">Image KARTU KELUARGA</label>
			        <div class="col-lg-5">
			            	
			            <img src="<?php echo base_url('./assets/images/foto_copy/' . $detail['pic3']); ?>" width="200px" height="200px" >
			        </div>
			    </div>
			     <div class="form-group">
			        <label class="col-lg-2 control-label">Image PHOTO</label>
			        <div class="col-lg-5">
			            <img src="<?php echo base_url('./assets/images/foto_copy/' . $detail['pic4']); ?>" width="200px" height="200px" >
			            
			        </div>
			    </div>
			     <div class="form-group">
			        <label class="col-lg-2 control-label">Image VAKSIN</label>
			        <div class="col-lg-5">
			            <img src="<?php echo base_url('./assets/images/foto_copy/' . $detail['pic5']); ?>" width="200px" height="200px" >
			            
			        </div>
			    </div>
			     <div class="form-group">
			        <label class="col-lg-2 control-label">Image BUKU NIKAH</label>
			        <div class="col-lg-5">
			            <img src="<?php echo base_url('./assets/images/foto_copy/' . $detail['pic6']); ?>" width="200px" height="200px" >	
			            
			        </div>
			    </div>
			     
	              <!-- </div> -->
              </div>

            <!-- </div> -->
               </div>

           
        </form>
    </div>
    </div>
       <!-- </div> -->
    </div>
</div>