<style type="text/css">
  /* Credit to bootsnipp.com for the css for the color graph */
/*.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}*/
</style>
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah Aktif</b>
                </div>
<?php if($this->session->flashdata('info')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                                  </div>
                              <?php } ?>
                 <div class="panel-body">
                      <div class="form-group">
                     <label for="embarkasi" class="col-sm-2 control-label">EMBARKASI</label>
                        <div class="col-sm-4">
                            <?php echo $form_embarkasi; ?>
                        </div>
                    </div>
           
                    <div class="form-group">
                     <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                                <input type='text' name="tahun_keberangkatan" id="tahun_keberangkatan" class="form-control" placeholder="YYYY/2016" required/>    
                          </div>
                      </div>  

                    <div class="form-group">
                    <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                                <input type='text' name="bulan_keberangkatan" id="bulan_keberangkatan" class="form-control" placeholder="MM/January" required/>
                          </div>
                      </div>  
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-8">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <!-- <button type="button" id="btn-reset" class="btn btn-default">Reset</button> -->
                         
                    </div>
                  </div>
      </div>

                   
                    </div>
                          <div class="table-responsive">
                             <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>DATA JAMAAH</th>
                                    <th>DATA PASSPORT </th>
                                    <th>DATA PERLENGKAPAN </th>
                                    <th>ACTION </th>
                                </tr>
                            </thead>
                            <tbody>
                             
                            </tbody>
                     </table>
                   </div>
                  
                      <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  
              </div>
          </div>
      </div>
        </form> 
    </div>

      
                


 <!-- MODAL EDIT -->
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Person Form</h3>
            </div>
             <form action="<?php echo base_url();?>manifest_affiliate/save" id="form" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">

           <!--   <form action="<?php echo base_url();?>manifest_affiliate/save" id="form" class="form-horizontal"  enctype="multipart/form-data"> -->
            <div class="modal-body form">
               
                    <input type="hidden" value="" name="id"/> 
                    <input type="hidden" value="" name="id_registrasi"/> 
                    <input type="hidden" value="" name="id_booking"/> 
                    <input type="hidden" value="" name="id_jamaah"/> 
                    <input type="hidden" value="" name="id_affiliate"/> 
                    <div class="form-body">
                         <div class="form-group">
                        <label class="col-sm-3 control-label">Nama di Passport</label>
                        <div class="col-sm-9">
                          <input name="nama_passport" type="text" id="nama_passport" class=" form-control" placeholder="Nama Passport " required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">No Passport</label>
                        <div class="col-sm-9">
                          <input name="no_pasport" type="text" id="no_pasport" class=" form-control" placeholder="No Passport Office" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">issue office</label>
                        <div class="col-sm-9">
                          <input name="issue_office" type="text" id="issue_office" class="input_keluarga form-control" placeholder="Input Issue Office" required>
                        </div>

                    </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >Isue Date</label>
                    <div class="col-sm-9">
                    
                         <input type="text" class="form-control" data-mask="date" name="isui_date" id="isui_date"   placeholder="YYYY-MM-DD" required/>
                
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >TELP</label>
                    <div class="col-sm-9">
                    
                         <input type="text" class="form-control" data-mask="date" name="telp" id="telp"   placeholder="NO TELP" required/>
                
                    </div>
                  </div>

                    <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >TELP 2</label>
                    <div class="col-sm-9">
                    
                         <input type="text" class="form-control" data-mask="date" name="telp_2" id="telp_2"   placeholder="NO TELP KELUARGA SERUMAH ATAU TIDAK SERUMAH" required/>
                
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >Tanggal Lahir</label>
                    <div class="col-sm-9">
                    
                         <input type="text" class="form-control" data-mask="date" name="tanggal_lahir" id="tanggal_lahir"   placeholder="YYYY-MM-DD" required/>
                
                    </div>
                  </div>

                    <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >Tempat Lahir</label>
                    <div class="col-sm-9">
                    
                         <input type="text" class="form-control" data-mask="date" name="tempat_lahir" id="tempat_lahir"   placeholder="Alamat Lahir" required/>
                
                    </div>
                  </div>

                  <div class="form-group">
                     <label class="control-label col-md-3">Jenis Kelamin</label>
                       <div class="col-md-9">
                        <select required name="kelamin" class="form-control"  >
                                    <option value="">--PILIH JENIS KELAMIN--</option>
                                    <option value="2">PRIA</option>
                                    <option value="3">WANITA</option>
                                </select>
                                <span class="help-block"></span>
                        </div>
                  </div>

                  <div class="form-group">
                     <label class="control-label col-md-3">PERGI DENGAN</label>
                       <div class="col-md-9">
                        <select required name="ket_keberangkatan" class="form-control"  >
                                    <option value="">--PILIH BERANGKAT DENGAN--</option>
                                    <option value="8">KELUARGA</option>
                                    <option value="9">SENDIRI/GROUP</option>
                                </select>
                                <span class="help-block"></span>
                        </div>
                      </div>

                   <div class="form-group">
                     <label class="control-label col-md-3">HUBANGAN KELUARGA</label>
                       <div class="col-md-9">
                        <select name="hubkeluarga" class="form-control"  >
                                    <option value="">--PILIH BERANGKAT DENGAN--</option>
                                      <?php foreach($family_relation as $fr){?>
                                                  <option value='<?php echo $fr->id_relation;?>'><?php echo $fr->keterangan;?></option>
                                              <?php } ?> 
                               
                                </select>
                                <span class="help-block"></span>
                        </div>
                      </div>

                    

                    <!--   <div class="form-group">
                     <label class="control-label col-md-3">INPUT KELUARGA</label>
                     <div class="col-md-9">
                       <select class="form-control chosen-select"  name="keluarga" id="keluarga" tabindex="9" >
                        <option value=''>- PILIH KELUARGA -</option>
                                              <?php foreach($get_keluarga as $kl){
                                                  echo '<option value="'.$kl->id_jamaah.'">'.$kl->nama.'-'.$kl->id_jamaah.'</option>';
                                              } ?>
                      </select>
                                <span class="help-block"></span>
                        </div>
                      </div> -->
<!--  <label class="col-sm-4 control-label" for="inputPassword3" >ID KELUARGA</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control"  name="keluarga" id="keluarga"   />
                             </div> -->
                        <div class="form-group">
                        
                       <label class="control-label col-md-3">INPUT KELUARGA</label>
                       <div class="col-md-9">
                         <input type="text" class="form-control"  name="keluarga" id="keluarga"   readonly="true" />   
                       <input type="search" name="autocomplete" id="autocomplete1" class="form-control autocomplete id_jamaah" placeholder="INPUT ID ATAU NAMA KELUARGA" style="width: 400px;  max-height: 200px; z-index: 9999; display: block;"tabindex="1" required>
                          </div> 
                         
                        </div>

                      <div class="form-group">
                    <label class="col-sm-3 control-label" for="inputPassword3" >KETERANGAN KELUARGA</label>
                    <div class="col-sm-9">
                    
                         <input type="text" class="form-control" data-mask="date" name="ket_keluarga" id="ket_keluarga"   placeholder="KETERANGAN KELUARGA" />
                
                    </div>
                  </div>

                   <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy Pasport </label>
                            <div class="col-md-9">
                                <select name="status_pasport" class="form-control">
                                    <option value="">--PILIH STATUS PASSPORT--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima Passport</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_passport" id="tgl_passport"   placeholder="YYYY-MM-DD" required/>
                             </div>
                        </div>
         
                  
                    <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy Kartu Identitas/KTP</label>
                            <div class="col-md-9">
                                <select name="status_identitas" class="form-control">
                                    <option value="">--PILIH STATUS IDENTITAS/KTP--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                          <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima KTP</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_ktp" id="tgl_ktp"   placeholder="YYYY-MM-DD" required/>
                             </div>
                        </div>
                    <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy Kartu Keluarga</label>
                            <div class="col-md-9">
                                <select name="status_kk" class="form-control">
                                    <option value="">--PILIH STATUS KARTU KELUARGA--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                             <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima KK</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_kk" id="tgl_kk"   placeholder="YYYY-MM-DD" />
                             </div>
                        </div>
                 
                  <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy Pas Photo</label>
                            <div class="col-md-9">
                                <select name="status_photo" class="form-control">
                                    <option value="">--PILIH STATUS PHOTO--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima Pas PHOTO</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_photo" id="tgl_photo"   placeholder="YYYY-MM-DD" />
                             </div>
                        </div>

       
                        <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy Terima Vaksin</label>
                            <div class="col-md-9">
                                <select name="status_vaksin" class="form-control">
                                    <option value="">--PILIH STATUS BUKU NIKAH--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                              <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima Vaksin</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_vaksin" id="tgl_vaksin"   placeholder="YYYY-MM-DD" />
                             </div>
                        </div>
                   
                         <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy Buku Nikah</label>
                            <div class="col-md-9">
                                <select name="status_buku_nikah" class="form-control">
                                    <option value="">--PILIH STATUS BUKU NIKAH--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                              <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima BN   </label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_bn" id="tgl_bn"   placeholder="YYYY-MM-DD" />
                             </div>
                        </div>
                     
                        <div class="form-group">
                            <label class="control-label col-md-3">FotoCopy AKTE LAHIR</label>
                            <div class="col-md-9">
                                <select name="status_akte" class="form-control">
                                    <option value="">--PILIH STATUS AKTE LAHIR--</option>
                                    <option value="0">BELUM ADA</option>
                                    <option value="1">SUDAH ADA</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                              <label class="col-sm-4 control-label" for="inputPassword3" >Tgl Terima Akte</label>
                             <div class="col-sm-5">
                                <input type="text" class="form-control" data-mask="date" name="tgl_akte" id="tgl_akte"   placeholder="YYYY-MM-DD" />
                             </div>
                        </div>




                    </div>
               
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                 <!-- <button type="submit" name="myButton" id="myButton" class="btn btn-primary"><i class="fa fa-save"></i> Save</button> -->
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
             </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
      
       $(function () {
                $('#bulan_keberangkatan').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#tahun_keberangkatan').datetimepicker({
          format: 'YYYY',
        });
            });

        jQuery(document).ready(function(){
              $(".chosen-select").chosen({width: "95%"}); 
            });
    </script>

<script type="text/javascript">


var save_method;
var table;

$(document).ready(function() {

    //datatables
    table = $('#data-table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('manifest/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.embarkasi = $('#embarkasi').val();
                data.tahun_keberangkatan = $('#tahun_keberangkatan').val();
                data.bulan_keberangkatan = $('#bulan_keberangkatan').val();
               
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function edit_manifest(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('manifest/edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="id_registrasi"]').val(data.id_registrasi);
            $('[name="id_booking"]').val(data.id_booking);
            $('[name="id_jamaah"]').val(data.id_jamaah);
            $('[name="id_affiliate"]').val(data.id_affiliate);
            $('[name="no_pasport"]').val(data.no_pasport);
            $('[name="nama_passport"]').val(data.nama_passport);
            $('[name="issue_office"]').val(data.issue_office);
            $('[name="isui_date"]').val(data.isui_date);
            $('[name="status_pasport"]').val(data.status_pasport);
            $('[name="status_identitas"]').val(data.status_identitas);
            $('[name="status_kk"]').val(data.status_kk);
            $('[name="status_photo"]').val(data.status_photo);
            $('[name="status_vaksin"]').val(data.status_vaksin);
            $('[name="status_buku_nikah"]').val(data.status_buku_nikah);
            $('[name="status_akte"]').val(data.status_akte);

            $('[name="tanggal_lahir"]').val(data.tanggal_lahir);
            $('[name="tempat_lahir"]').val(data.tempat_lahir);
            $('[name="kelamin"]').val(data.kelamin);
            $('[name="ket_keberangkatan"]').val(data.ket_keberangkatan);
             $('[name="hubkeluarga"]').val(data.hubkeluarga);
             $('[name="autocomplete1"]').val(data.keluarga);
              $('[name="keluarga"]').val(data.keluarga);
             $('[name="telp"]').val(data.telp);
             $('[name="telp_2"]').val(data.telp_2);
            $('[name="tgl_passport"]').val(data.tgl_passport);
             $('[name="tgl_ktp"]').val(data.tgl_ktp);
            $('[name="tgl_kk"]').val(data.tgl_kk);
            $('[name="tgl_photo"]').val(data.tgl_photo);
           
            $('[name="tgl_vaksin"]').val(data.tgl_vaksin);
            $('[name="tgl_bn"]').val(data.tgl_bn);
            $('[name="tgl_akte"]').val(data.tgl_akte);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

url = "<?php echo site_url('manifest/update')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>

<script type="text/javascript">

 $(function () {
            
          $('#isui_date').datetimepicker({
          format: 'YYYY-MM-DD',
        });
         $('#tgl_passport').datetimepicker({
          format: 'YYYY-MM-DD',
        });
        $('#tgl_bn').datetimepicker({
          format: 'YYYY-MM-DD',
        });
         $('#tgl_photo').datetimepicker({
          format: 'YYYY-MM-DD',
        });
         $('#tgl_kk').datetimepicker({
          format: 'YYYY-MM-DD',
        });
         $('#tgl_vaksin').datetimepicker({
          format: 'YYYY-MM-DD',
        });
          $('#tgl_ktp').datetimepicker({
          format: 'YYYY-MM-DD',
        });
       $('#tgl_akte').datetimepicker({
                format: 'YYYY-MM-DD',
              });

            });

     $(document).ready(function(){
            
           $("#bulantahun_keberangkatan").change(function (){
                var url = "<?php echo site_url('manifest/add_ajax_schedule');?>/"+$(this).val();
                $('#schedule').load(url);
                return false;
            })

            
        });



</script>

<script type="text/javascript">

  function checkForm(form)
  {
    //
    // validate form fields
    //
     $('#myButton').text('saving...');
    form.myButton.disabled = true;
    return true;
  }

</script>

<script>
function update_pengajuan(id)
    {
       
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('manifest/save')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    
                   // table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                      
                      // table.ajax.reload();
                    
                    alert('Error deleting data');
                   // window.location.href="<?php echo site_url('no_access')?>";
                }
            });

    
}


    

</script>

 <script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'manifest/search_autocomplete',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    // $('#sponsor').val(''+suggestion.userid); // membuat id 'v_nim' untuk ditampilkan
                  
                }
            });
        });
    </script>