<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Detailmanifestcabang_model extends CI_Model{
    var $table = 'manifest_affiliate';
    var $column_order = array(null, 'id','id_registrasi','invoice','id_jamaah','no_pasport','id_affiliate','nama','id_booking','nama_passport','issue_office','tanggal_lahir','tempat_lahir','kelamin','isui_date','telp','telp_2','bulan_keberangkatan','tahun_keberangkatan','bulantahun_keberangkatan','schedule','create_date','ket_keberangkatan','hubkeluarga','keluarga','ket_keluarga','cluster','koordinator','status_manifest','departure','tipe_jamaah','provinsi_id','kabupaten_id'); //set column field database for datatable orderable
    var $column_search = array('id','id_registrasi','invoice','id_jamaah','no_pasport','id_affiliate','nama','id_booking','nama_passport','issue_office','tanggal_lahir','tempat_lahir','kelamin','isui_date','telp','telp_2','bulan_keberangkatan','tahun_keberangkatan','bulantahun_keberangkatan','schedule','create_date','ket_keberangkatan','hubkeluarga','keluarga','ket_keluarga','cluster','koordinator','status_manifest','departure','tipe_jamaah','provinsi_id','kabupaten_id'); //set column field database for datatable searchable 
    var $order = array('schedule' => 'asc'); // default order 
   

    private $_manifest="manifest";
    private $_id_manifest="id";

    private $_additional_cost ="additional_cost";
    private $id_additional_cost ="id";
    public function __construct()
    {
          parent::__construct();
          $this->load->database();
         
    }

    private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');

         if($this->input->post('bulantahun_keberangkatan'))
        {
            $this->db->where('bulantahun_keberangkatan', $this->input->post('bulantahun_keberangkatan'));
        }
        // if($this->input->post('schedule'))
        // {
        //     $this->db->like('schedule', $this->input->post('schedule'));
        // }
         if($this->input->post('cabang'))
        {
            $this->db->like('cabang', $this->input->post('cabang'));
        }
        // $tipe ='1,6,8,9,10';
        $this->db->where_in('tipe_jamaah',array('1','6','8','9','10'));
        $this->db->order_by("schedule", "asc");
        $this->db->from('manifest_affiliate');
  
       

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // -----------------------------------------------------

    
      public function get_list_bulantahun_keberangkatan()
    {
        $id_user=  $this->session->userdata('id_user');
       $koordinator= $this->uri->segment(4.0);
        $this->db->select('bulantahun_keberangkatan');
        $this->db->from($this->table);
        $this->db->where('cluster', $koordinator);
        // $this->db->where('id_affiliate', $koordinator);
        // $this->db->order_by('bulantahun_keberangkatan','desc');
        $query = $this->db->get();
        $result = $query->result();

        $bulantahun_keberangkatans = array();
        foreach ($result as $row) 
        {
            $bulantahun_keberangkatans[] = $row->bulantahun_keberangkatan;
        }
        return $bulantahun_keberangkatans;
    }

      public function get_list_date_schedule()
    {
        $id_user=  $this->session->userdata('id_user');
         $koordinator= $this->uri->segment(4.0);
        $this->db->select('schedule');
        
        $this->db->from($this->table);
        $this->db->where('cluster', $koordinator);
        // $this->db->order_by('bulantahun_keberangkatan','desc');
        $query = $this->db->get();
        $result = $query->result();

        $schedules = array();
        foreach ($result as $row) 
        {
            $schedules[] = $row->schedule;
        }
        return $schedules;
    }


      function get_all_schedule() {
        

     $query = $this->db->query("SELECT DISTINCT(schedule) from  manifest_affiliate ORDER BY  schedule DESC ");

        return $query->result();
    }

        public function get_by_id($id)
    {
        // $this->db->from($this->table);
        // $this->db->where('id',$id);
        // $query = $this->db->get();

        // return $query->row();

         $stored_procedure = "call detail_input_manifest_admin(?)";
         return $this->db->query($stored_procedure,array('id'=>$id
            ))->row();    

    }

    public function pindah_cluster($id_registrasi)
    {
        $this->db->from('pindah_cluster');
        $this->db->where('id_registrasi',$id_registrasi);
        $query = $this->db->get();

        return $query->row();
    }


    public function generate_kode($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 11;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }

    public function update_data($data){
$v_cabang  = $this->session->userdata('id_user');
$v_id  = $this->input->post('id_registrasi');
$p_nama_passport = $this->input->post('nama_passport');
$p_no_pasport = $this->input->post('no_pasport');
$p_status_identitas = $this->input->post('status_identitas');
$p_status_kk = $this->input->post('status_kk');
$p_status_photo = $this->input->post('status_photo');
$p_status_pasport = $this->input->post('status_pasport');
$p_status_vaksin = $this->input->post('status_vaksin');
$p_status_buku_nikah = $this->input->post('status_buku_nikah');
$p_status_akte = $this->input->post('status_akte');
$p_isui_date = $this->input->post('isui_date');
$p_issue_office = $this->input->post('issue_office');
$p_hubkeluarga =$this->input->post('hubkeluarga');
$p_ket_keluarga = $this->input->post('ket_keluarga');
$p_keluarga = $this->input->post('keluarga');
$p_tanggal_lahir = $this->input->post('tanggal_lahir');
$p_tempat_lahir = $this->input->post('tempat_lahir');
$p_kelamin = $this->input->post('kelamin');
$p_ket_keberangkatan = $this->input->post('ket_keberangkatan');
$p_telp = $this->input->post('telp');
$p_telp_2 = $this->input->post('telp_2');
$p_tgl_status_pasport = $this->input->post('tgl_passport');
$p_tgl_status_identitas = $this->input->post('tgl_ktp');
$p_tgl_status_kk = $this->input->post('tgl_kk');
$p_tgl_status_photo = $this->input->post('tgl_photo');
$p_tgl_status_vaksin = $this->input->post('tgl_vaksin');
$p_tgl_status_buku_nikah = $this->input->post('tgl_bn');
$p_tgl_akte = $this->input->post('tgl_akte');
 
 
// $sql_query=$this->db->query("call SP_AUTOSISTEM('$nominal','".$bank."','".$no_rek."','".$tgl_bank."','".$ket_transfer."')");
$sql_query=$this->db->query("call UPDATE_MANIFEST_ADMIN('".$v_cabang."','".$v_id ."','".$p_nama_passport ."','".$p_no_pasport."','".$p_status_identitas ."','".$p_status_kk ."','".$p_status_photo ."','".$p_status_pasport."','".$p_status_vaksin."','".$p_status_buku_nikah."','".$p_status_akte."','".$p_isui_date."','".$p_issue_office."','".$p_hubkeluarga."','".$p_ket_keluarga."','".$p_keluarga."','".$p_tanggal_lahir."','".$p_tempat_lahir."','".$p_kelamin."','".$p_ket_keberangkatan."','".$p_telp."','".$p_telp_2."','".$p_tgl_status_pasport."','".$p_tgl_status_identitas."','".$p_tgl_status_kk."','".$p_tgl_status_photo."','".$p_tgl_status_vaksin."','".$p_tgl_status_buku_nikah."','".$p_tgl_akte."')");

    //      $arr = array(
          
    //        'nama_passport'=> $this->input->post('nama_passport'),
    //         'no_pasport'=> $this->input->post('no_pasport'),
    //         'issue_office'=> $this->input->post('issue_office'),
    //         'isui_date'=> $this->input->post('isui_date'),
    //         'status_identitas'=> $this->input->post('status_identitas'),
    //         'status_kk'=> $this->input->post('status_kk'),
    //         'status_photo'=> $this->input->post('status_photo'),
    //         'status_pasport'=> $this->input->post('status_pasport'),
    //         'status_vaksin'=> $this->input->post('status_vaksin'),
    //         'status_buku_nikah'=> $this->input->post('status_buku_nikah'),
    //         'status_akte'=> $this->input->post('status_akte'),
    //         'hubkeluarga'=> $this->input->post('hubkeluarga'),
    //         'keluarga'=> $this->input->post('keluarga'),
    //         'ket_keluarga'=> $this->input->post('ket_keluarga'),
    //         'update_by'=>$this->session->userdata('id_user'),
    //         'update_date'=>date('Y-m-d H:i:s'),
    //     );       
       
      
    //    if(isset($data['pic1'])){
            
    //         $arr['pic1'] = $data['pic1'];
    //     }
    //     if(isset($data['pic2'])){
            
    //         $arr['pic2'] = $data['pic2'];
    //     }
    //     if(isset($data['pic3'])){
            
    //         $arr['pic3'] = $data['pic3'];
    //     }
    //     if(isset($data['pic4'])){
            
    //         $arr['pic4'] = $data['pic4'];
    //     }
    //     if(isset($data['pic5'])){
            
    //         $arr['pic5'] = $data['pic5'];
    //     }
    //      if(isset($data['pic6'])){
            
    //         $arr['pic6'] = $data['pic6'];
    //     }

                    
 
    //     $this->db->update($this->_manifest,$arr,array('id_registrasi'=>$this->input->post('id_registrasi')));

    //      $arr = array(
          
    //        'tanggal_lahir'=> $this->input->post('tanggal_lahir'),
    //         'tempat_lahir'=> $this->input->post('tempat_lahir'),
    //         'kelamin'=> $this->input->post('kelamin'),
    //         'ket_keberangkatan'=> $this->input->post('ket_keberangkatan'),
    //        'telp'=> $this->input->post('telp'),
    //        'telp_2'=> $this->input->post('telp_2'),
    //     );       
    //      $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$this->input->post('id_registrasi')));

    // $data = array
    //         (   
    //             'id_user'=> $this->input->post('id_affiliate'),
    //             'id_jamaah'=> $this->input->post('id_jamaah'),
    //             'id_registrasi'=> $this->input->post('id_registrasi'),
    //             'id_booking'=> $this->input->post('id_booking'),
    //             'jns_pengeluaran'=>'155',
    //             'jumlah'=>'70000',
    //             'akun'=>'PENDAPATAN',
    //             'dk'=>'D',
    //             'keterangan'=>'BIAYA MANASIK',
    //             'status'=>'1',
    //             'create_by'=>$this->session->userdata('id_user'),
    //             'create_date'=>date('Y-m-d H:i:s')
    //         );

    //     $this->db->insert($this->_additional_cost,$data);

    //      $kd_trans =  $this->db->insert_id(); //get last insert ID

    //     $this->db->update($this->_additional_cost,
    //                 array('kd_trans'=> $this->generate_kode(sprintf('%07d',$kd_trans))),
    //                 array('id'=>$kd_trans));

    //     $id_registrasi = $this->input->post('id_registrasi');
    //     $sql = "SELECT * from registrasi_jamaah where id_registrasi ='$id_registrasi' ";
    //     $query = $this->db->query($sql)->result_array();
    //       foreach($query as $key=>$value){
    //         $id_affiliate = $value['id_affiliate'];
    //       }

    //     $sql = "SELECT * from affiliate where id_user ='$id_affiliate' ";
    //     $query = $this->db->query($sql)->result_array();
    //       foreach($query as $key=>$value){
    //         $id_affiliate_type = $value['id_affiliate_type'];
    //       }

    //        $sql = "SELECT COUNT(*) jml from additional_cost where id_registrasi  ='$id_registrasi' and dk='D' ";
    //     $query = $this->db->query($sql)->result_array();
    //       foreach($query as $key=>$value){
    //         $jml = $value['jml'];
    //       }

    //       if ($id_affiliate_type == 1 && $jml < 2){ 

    //     //   $data = array
    //     //     (   
    //     //         'id_user'=> $this->input->post('id_affiliate'),
    //     //         'id_jamaah'=> $this->input->post('id_jamaah'),
    //     //         'id_registrasi'=> $this->input->post('id_registrasi'),
    //     //         'id_booking'=> $this->input->post('id_booking'),
    //     //         'jns_pengeluaran'=>'133',
    //     //         'jumlah'=>'150000',
    //     //         'akun'=>'PENDAPATAN',
    //     //         'dk'=>'D',
    //     //         'keterangan'=>'FEE INPUT CABANG',
    //     //         'status'=>'1',
    //     //         'create_by'=>$this->session->userdata('id_user'),
    //     //         'create_date'=>date('Y-m-d H:i:s')
    //     //     );

    //     // $this->db->insert($this->_additional_cost,$data);
    //     // $kd_trans =  $this->db->insert_id(); //get last insert ID

    //     // $this->db->update($this->_additional_cost,
    //     //             array('kd_trans'=> $this->generate_kode(sprintf('%07d',$kd_trans))),
    //     //             array('id'=>$kd_trans));

    //     $data = array
    //         (   
    //             'id_user'=> $this->input->post('id_affiliate'),
    //             'id_jamaah'=> $this->input->post('id_jamaah'),
    //             'id_registrasi'=> $this->input->post('id_registrasi'),
    //             'id_booking'=> $this->input->post('id_booking'),
    //             'jns_pengeluaran'=>'144',
    //             'jumlah'=>'150000',
    //             'akun'=>'PENDAPATAN',
    //             'dk'=>'D',
    //             'keterangan'=>'BIAYA PENGURUSAN JAMAAH',
    //             'status'=>'1',
    //             'create_by'=>$this->session->userdata('id_user'),
    //             'create_date'=>date('Y-m-d H:i:s')
    //         );

    //     $this->db->insert($this->_additional_cost,$data);
    //     $kd_trans =  $this->db->insert_id(); //get last insert ID

    //     $this->db->update($this->_additional_cost,
    //                 array('kd_trans'=> $this->generate_kode(sprintf('%07d',$kd_trans))),
    //                 array('id'=>$kd_trans));


    // }


    //     if($this->db->trans_status()===false){
            
    //         $this->db->trans_rollback();
           
    //         return false;    
            
    //     }else{
            
    //         $this->db->trans_complete();
            
    //         return true;
    //     }
        
}


 public function update_data_pic($data){

        //  $arr = array(
          
        //         'pic1' => isset($data['pic1']) ? $data['pic1']: '',
        //         'pic2' => isset($data['pic2']) ? $data['pic2']: '',
        //         'pic3' => isset($data['pic3']) ? $data['pic3']: '',
        //         'pic4' => isset($data['pic4']) ? $data['pic4']: '',
        //         'pic5' => isset($data['pic5']) ? $data['pic5']: '',
        //         'pic6' => isset($data['pic6']) ? $data['pic6']: '',
        //        'pic7' => isset($data['pic7']) ? $data['pic7']: '',
        // );       
       
      
       if(isset($data['pic1'])){
            
            $arr['pic1'] = $data['pic1'];
        }
        if(isset($data['pic2'])){
            
            $arr['pic2'] = $data['pic2'];
        }
        if(isset($data['pic3'])){
            
            $arr['pic3'] = $data['pic3'];
        }
        if(isset($data['pic4'])){
            
            $arr['pic4'] = $data['pic4'];
        }
        if(isset($data['pic5'])){
            
            $arr['pic5'] = $data['pic5'];
        }
         if(isset($data['pic6'])){
            
            $arr['pic6'] = $data['pic6'];
        }
         if(isset($data['pic7'])){
            
            $arr['pic7'] = $data['pic7'];
        }

                    
 
        $this->db->update($this->_manifest,$arr,array('id_registrasi'=>$data['id_registrasi']));

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
}

 public function update_data_pindah_cluster($data){

      $arr = array(
          
              'cluster'=> $this->input->post('pindah_cluster')
        );       
        $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$this->input->post('id_registrasi2')));

      $arr = array(
          
              'admin_cluster'=> $this->session->userdata('id_user')
        );       
        $this->db->update('manifest',$arr,array('id_registrasi'=>$this->input->post('id_registrasi2')));


      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
}
function cek_issue_date($id_registrasi){

        $query = $this->db->query("SELECT isui_date,DATE_FORMAT(now(), '%Y-%m-%d')as date_now ,DATE_FORMAT(DATE_ADD(isui_date, INTERVAL   5 YEAR),'%Y-%m-%d') as expire_date from manifest where id_registrasi = '$id_registrasi'");
        return $query;
}

public function update($where, $data)
    {
        $this->db->update($this->_table, $data, $where);
        return $this->db->affected_rows();
    }


  public function save_additional_cost($data)
    {
        $this->db->insert($this->_additional_cost, $data);
        return $this->db->insert_id();
    }


     public function get_detail_manifest($id){

          $stored_procedure = "call detail_manifest_affiliate(?)";
         return $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array();

    }

    public function get_detail_hubkeluarga($id){

          $stored_procedure = "call refrensi_hubungankeluarga(?)";
         return $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array();

    }

    function get_keluarga() {
        $id_booking =$this->input->post('id_booking');
        $id_affiliate =$id = $this->uri->segment(4);
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status','1');
        $this->db->where('id_affiliate', $id_affiliate);
        $query = $this->db->get();
        
        return $query->result();
    }

     public function get_data_koordinator($id_user){
    
           $sql = "SELECT * from affiliate where id_user = '$id_user'
                ";
        return $this->db->query($sql)->row_array();   

        
    }

     public function get_data_pic($id){
    
           $sql = "SELECT * from manifest_affiliate where id_registrasi = '$id'
                ";
        return $this->db->query($sql)->row_array();   

        
    }

 public function approved($id_registrasi){
    
        $arr = array(
          
              'status_manifest'=> '1'
        );       
        $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));

      $arr = array(
          
              'admin_approved'=> $this->session->userdata('id_user')
        );       
        $this->db->update('manifest',$arr,array('id_registrasi'=>$id_registrasi));

         $arr = array(
          
              'status_manifest'=> '1'
        );       
        $this->db->update('additional_cost',$arr,array('id_registrasi'=>$id_registrasi));
  
    }

    public function unapproved($id_registrasi, $alasan) {
        $arr = array(
            'status_manifest' => '0'
        );
        $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));
        $arr = array(
            'admin_unapproved' => $this->session->userdata('id_user'),
            'unapproved_date' => date('Y-m-d H:i:s')
        );
        $this->db->update('manifest', $arr, array('id_registrasi' => $id_registrasi));
        $arr = array(
            'status_manifest' => '0'
        );
        $this->db->update('additional_cost', $arr, array('id_registrasi' => $id_registrasi));
    }
}
