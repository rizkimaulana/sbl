<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manifest_model extends CI_Model{
 var $table = 'manifest_admin';
    var $column_order = array(null, 'id','id_registrasi','invoice','id_jamaah','no_pasport','id_affiliate','nama','id_booking','nama_passport','issue_office','tanggal_lahir','tempat_lahir','kelamin','isui_date','telp','telp_2','embarkasi','date_schedule','status_identitas','status_kk','status_photo','status_pasport','status_vaksin','status_buku_nikah','status_akte','pic1','pic2','pic3','pic4','pic5','pic6','bulan_keberangkatan','tahun_keberangkatan','bulantahun_keberangkatan','schedule','create_date','ket_keberangkatan','hubkeluarga','keluarga','ket_keluarga'); //set column field database for datatable orderable
    var $column_search = array('id','id_registrasi','invoice','id_jamaah','no_pasport','id_affiliate','nama','id_booking','nama_passport','issue_office','tanggal_lahir','tempat_lahir','kelamin','isui_date','telp','telp_2','embarkasi','date_schedule','status_identitas','status_kk','status_photo','status_pasport','status_vaksin','status_buku_nikah','status_akte','pic1','pic2','pic3','pic4','pic5','pic6','bulan_keberangkatan','tahun_keberangkatan','bulantahun_keberangkatan','schedule','create_date','ket_keberangkatan','hubkeluarga','keluarga','ket_keluarga'); //set column field database for datatable searchable 
    var $order = array('id_registrasi' => 'asc'); // default order 
   

    private $_manifest="manifest";
    private $_id_manifest="id";

    private $_additional_cost ="additional_cost";
    private $id_additional_cost ="id";
    public function __construct()
    {
          parent::__construct();
          $this->load->database();
         
    }
public function update($data){
 $v_cabang  = $this->session->userdata('id_user');
$v_id  = $this->input->post('id_registrasi');
$p_nama_passport = $this->input->post('nama_passport');
$p_no_pasport = $this->input->post('no_pasport');
$p_status_identitas = $this->input->post('status_identitas');
$p_status_kk = $this->input->post('status_kk');
$p_status_photo = $this->input->post('status_photo');
$p_status_pasport = $this->input->post('status_pasport');
$p_status_vaksin = $this->input->post('status_vaksin');
$p_status_buku_nikah = $this->input->post('status_buku_nikah');
$p_status_akte = $this->input->post('status_akte');
$p_isui_date = $this->input->post('isui_date');
$p_issue_office = $this->input->post('issue_office');
$p_hubkeluarga =$this->input->post('hubkeluarga');
$p_ket_keluarga = $this->input->post('ket_keluarga');
$p_keluarga = $this->input->post('id_keluarga');
$p_tanggal_lahir = $this->input->post('tanggal_lahir');
$p_tempat_lahir = $this->input->post('tempat_lahir');
$p_kelamin = $this->input->post('kelamin');
$p_ket_keberangkatan = $this->input->post('ket_keberangkatan');
$p_telp = $this->input->post('telp');
$p_telp_2 = $this->input->post('telp_2');
$p_tgl_status_pasport = $this->input->post('tgl_passport');
$p_tgl_status_identitas = $this->input->post('tgl_ktp');
$p_tgl_status_kk = $this->input->post('tgl_kk');
$p_tgl_status_photo = $this->input->post('tgl_photo');
$p_tgl_status_vaksin = $this->input->post('tgl_vaksin');
$p_tgl_status_buku_nikah = $this->input->post('tgl_bn');
$p_tgl_akte = $this->input->post('tgl_akte');
 
 
// $sql_query=$this->db->query("call SP_AUTOSISTEM('$nominal','".$bank."','".$no_rek."','".$tgl_bank."','".$ket_transfer."')");
$sql_query=$this->db->query("call UPDATE_MANIFEST_ADMIN('".$v_cabang."','".$v_id ."','".$p_nama_passport ."','".$p_no_pasport."','".$p_status_identitas ."','".$p_status_kk ."','".$p_status_photo ."','".$p_status_pasport."','".$p_status_vaksin."','".$p_status_buku_nikah."','".$p_status_akte."','".$p_isui_date."','".$p_issue_office."','".$p_hubkeluarga."','".$p_ket_keluarga."','".$p_keluarga."','".$p_tanggal_lahir."','".$p_tempat_lahir."','".$p_kelamin."','".$p_ket_keberangkatan."','".$p_telp."','".$p_telp_2."','".$p_tgl_status_pasport."','".$p_tgl_status_identitas."','".$p_tgl_status_kk."','".$p_tgl_status_photo."','".$p_tgl_status_vaksin."','".$p_tgl_status_buku_nikah."','".$p_tgl_akte."')");


  $arr = array(
          
                'pic1' => isset($data['pic1']) ? $data['pic1']: '',
                'pic2' => isset($data['pic2']) ? $data['pic2']: '',
                'pic3' => isset($data['pic3']) ? $data['pic3']: '',
                'pic4' => isset($data['pic4']) ? $data['pic4']: '',
                'pic5' => isset($data['pic5']) ? $data['pic5']: '',
                'pic6' => isset($data['pic6']) ? $data['pic6']: '',
               'pic7' => isset($data['pic7']) ? $data['pic7']: '',
        );       
       $this->db->update($this->_manifest,$arr,array('id_registrasi'=>$data['id_registrasi']));
      
       if(isset($data['pic1'])){
            
            $arr['pic1'] = $data['pic1'];
        }
        if(isset($data['pic2'])){
            
            $arr['pic2'] = $data['pic2'];
        }
        if(isset($data['pic3'])){
            
            $arr['pic3'] = $data['pic3'];
        }
        if(isset($data['pic4'])){
            
            $arr['pic4'] = $data['pic4'];
        }
        if(isset($data['pic5'])){
            
            $arr['pic5'] = $data['pic5'];
        }
         if(isset($data['pic6'])){
            
            $arr['pic6'] = $data['pic6'];
        }
         if(isset($data['pic7'])){
            
            $arr['pic7'] = $data['pic7'];
        }

                    
 
        $this->db->update($this->_manifest,$arr,array('id_registrasi'=>$data['id_registrasi']));
        
}

 function cek($id_jamaah){
        $query = $this->db->query("SELECT * FROM detail_manifest Where id_jamaah ='$id_jamaah'");
        return $query;
    }

public function get_pic($id_booking){
    
        $sql ="SELECT * from detail_manifest WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    

}

public function get_view_manifest($tahun_keberangkatan, $bulan_keberangkatan){
    
        // $sql ="SELECT * from data_manifest_excel WHERE  tahun_keberangkatan = ? and bulan_keberangkatan  = ?
        //       ";
        // return $this->db->query($sql,array($tahun_keberangkatan,$bulan_keberangkatan))->result_array();    
    $sql = "call detail_manifest_tahun_bulan_keberangkatan(?,?) ";
                     $query = $this->db->query($sql,array(
                        'tahun_keberangkatan'=>$tahun_keberangkatan,
                        'bulan_keberangkatan'=>$bulan_keberangkatan
                        ))->result_array();
}

public function get_jamaah_manifest($tahun_keberangkatan,$bulan_keberangkatan){
    
        // $sql ="SELECT * from detail_manifest_excel where tahun_keberangkatan = ? and bulan_keberangkatan= ?

        //       ";
        // return $this->db->query($sql,array($tahun_keberangkatan,$bulan_keberangkatan))->result_array();    
    
          $stored_procedure = "call export_data_manifest(?,?)";
         return $this->db->query($stored_procedure,array('tahun_keberangkatan'=>$tahun_keberangkatan,
            'bulan_keberangkatan'=>$bulan_keberangkatan
            ))->result_array();

}

 public function get_pic_booking($id_booking){
 

         $sql = "SELECT * from view_booking_manifest where id_booking = {$id_booking}
                ";
        return $this->db->query($sql)->row_array();  
    }

    public function get_manifest($id){
    
        $stored_procedure = "call detail_manifest(?)";
         $get = $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array(); 
    } 

     public function get_detail_manifest($id){

          $stored_procedure = "call detail_manifest_affiliate(?)";
         return $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array();

    }

    public function get_detail_hubkeluarga($id){

          $stored_procedure = "call refrensi_hubungankeluarga(?)";
         return $this->db->query($stored_procedure,array('id_registrasi'=>$id
            ))->row_array();

    }

    public function get_detail_tglpersyaratan($id){

        $sql = "SELECT * from tgl_persyaratan where id_registrasi = '$id'
                ";
        return $this->db->query($sql)->row_array();  

    }

    public function get_detail($id){
    
         $sql = "SELECT * from detail_manifest
                WHERE id_registrasi = {$id}
                ";
        return $this->db->query($sql)->row_array();
    }    




    

     function carikapasitas($room_group){
        $this->db->where("room_group",$room_group);
        return $this->db->get("view_concat_room_group");
    }

     function getroom_group(){
        return $this->db->get("view_concat_room_group");
    }

    function get_data_keluarga() {
        if(!isset($_POST)) {
            show_404();
        }
        $id_booking = $this->input->post('id_booking');
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        // $this->db->where('aktif', 'Y');
        $this->db->where('id_booking', $id_booking );
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }

       
    }

    public function get_data($offset,$limit,$q=''){
    
        $id=  $this->session->userdata('id_user');
          $sql = " SELECT * from data_manifest where 1=1
                    ";
        
        if($q){
            
            $sql .=" AND bulan_keberangkatan LIKE '%{$q}%'
                     OR tahun_keberangkatan LIKE '%{$q}%'
            ";
        }
        // $sql .=" ORDER BY tahun_keberangkatan DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }


        public function get_data_manifest($offset,$limit,$q=''){
        
        $id=  $this->session->userdata('id_user');
          $sql = " SELECT * from data_jamaah_manifest where 1=1
                    ";
        
        if($q){
            
            $sql .=" AND invoice LIKE '%{$q}%'
                    OR id_jamaah LIKE '%{$q}%'
                     OR nama LIKE '%{$q}%'
                     OR id_affiliate LIKE '%{$q}%'
                     OR telp LIKE '%{$q}%'   
                      
                     OR date_schedule LIKE '%{$q}%'   
                    
            ";
        }
        $sql .=" ORDER BY date_schedule DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   // function get_keluarga()  {
    
   //  $query = $this->db->get('registrasi_jamaah');
   //  return $query->result();
    
   //  }
     function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    
    
    function get_kabupaten() {
    
    $query = $this->db->get('wilayah_kabupaten');
    return $query->result();
    
    }
    
    function get_kecamatan()    {
    
    $query = $this->db->get('wilayah_kecamatan');
    return $query->result();
    
    }

    function lap_data_jamaah($id_booking) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_booking($id_booking) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();

        if($query->num_rows() > 0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_jamaahnoaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahnoaktif');
        $this->db->where('id_booking',$id_booking);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

     public function delete($id){
        
        $get_img  = $this->db->select('pic1,pic2,pic3,pic4,pic5')
                                ->where('id_registrasi',$id)->get('registrasi_jamaah')->row_array();
        
        //remove all images
        if($get_img){
            $img = array('pic1','pic2','pic3','pic4','pic5');
            foreach($img as $im){
                
                if($get_img[$im])
                    unlink('./assets/images/foto_copy/'.$get_img[$im]);
            }
        
        }
        
        return $this->db->delete('registrasi_jamaah',array('id_registrasi'=>$id));
    }



    private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');

         if($this->input->post('embarkasi'))
        {
            $this->db->where('embarkasi', $this->input->post('embarkasi'));
        }
        if($this->input->post('tahun_keberangkatan'))
        {
            $this->db->like('tahun_keberangkatan', $this->input->post('tahun_keberangkatan'));
        }
        if($this->input->post('bulan_keberangkatan'))
        {
            $this->db->like('bulan_keberangkatan', $this->input->post('bulan_keberangkatan'));
        }

        // $this->db->where('id_affiliate', $id_user);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // -----------------------------------------------------

    
      public function get_list_bulantahun_keberangkatan()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->select('bulantahun_keberangkatan');
        
        $this->db->from($this->table);
        $this->db->where('id_affiliate', $id_user);
        // $this->db->order_by('bulantahun_keberangkatan','desc');
        $query = $this->db->get();
        $result = $query->result();

        $bulantahun_keberangkatans = array();
        foreach ($result as $row) 
        {
            $bulantahun_keberangkatans[] = $row->bulantahun_keberangkatan;
        }
        return $bulantahun_keberangkatans;
    }

      public function get_list_date_schedule()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->select('schedule');
        
        $this->db->from($this->table);
        $this->db->where('id_affiliate', $id_user);
        // $this->db->order_by('bulantahun_keberangkatan','desc');
        $query = $this->db->get();
        $result = $query->result();

        $schedules = array();
        foreach ($result as $row) 
        {
            $schedules[] = $row->schedule;
        }
        return $schedules;
    }


      function get_all_schedule() {
        

     $query = $this->db->query("SELECT DISTINCT(schedule) from  manifest_affiliate ORDER BY  schedule DESC ");

        return $query->result();
    }
    
      public function get_list_embarkasih()
    {
        $this->db->select('embarkasi');
        $this->db->from($this->table);
        $this->db->order_by('date_schedule','asc');
        $query = $this->db->get();
        $result = $query->result();

        $embarkasis = array();
        foreach ($result as $row) 
        {
            $embarkasis[] = $row->embarkasi;
        }
        return $embarkasis;
    }

    public function get_by_id($id)
    {
        //        $this->db->from('detail_data_manifest_admin');
        // $this->db->where('id',$id);
        // $query = $this->db->get();

        // return $query->row();
        
         $stored_procedure = "call detail_input_manifest_admin(?)";
         return $this->db->query($stored_procedure,array('id'=>$id
            ))->row();
    }


    function get_keluarga() {
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status','1');
        $query = $this->db->get();
        
        return $query->result();
    }
}
