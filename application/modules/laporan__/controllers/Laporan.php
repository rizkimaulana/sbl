<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan extends CI_Controller {

    var $folder = "laporan";

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(LAPORAN, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'laporan');
        $this->load->model('laporan_model');
        $this->load->model('laporan_model', 'r');
    }

    public function index() {
        $this->template->load('template/template', $this->folder . '/laporan');
    }

    public function lap_rekap_registrasi() {
        $this->template->load('template/template', $this->folder . '/lap_rekap_registrasi');
    }

    public function json_rekap_registrasi() {
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : date('Y-m-d');
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : date('Y-m-d');
        $tipe_tanggal = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : 'payment_date';
        $filter_query = ($tipe_tanggal === 'create_date') ?
                " AND b.create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' " :
                " AND a.payment_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        $sql = "SELECT a.id_booking, a.id_product, a.id_schedule, a.invoice, a.id_affiliate as id_affiliate, 
                a.nama as nama_affiliate,COUNT(*)as jumlah_jamaah, a.tipe_jamaah as id_tipe_jamaah, 
                a.payment_date as payment_date,
                DATE_FORMAT(schedule.date_schedule,'%M %Y') as tgl_keberangkatan,
                a.tgl_daftar, a.create_date,                
                sum(a.refund) as refund,
                sum(a.fee) as fee,
                sum(a.fee_input) as fee_input,
                sum(a.muhrim)  as muhrim,
                sum(a.akomodasi) as akomodasi,
                sum(a.handling)  as handling,
                sum(a.visa) as visa,
                sum(a.harga_paket) as harga,
                sum(a.room_order) as Room_Price_2,
                sum(a.dpbooking_seats) as dpbooking_seats,
                (a.jml_angsuran) as jml_angsuran,
                (a.angsuran) as angsuran,
                (a.dp_angsuran) as dp_angsuran,
                (((sum(a.muhrim) + sum(a.harga_paket) + sum(a.akomodasi) + sum(a.handling)+ sum(a.visa) + sum(a.room_order))- sum(((a.jml_angsuran)* (a.angsuran))+(a.dp_angsuran)))-(sum(a.refund) + sum(a.fee)+ sum(a.dpbooking_seats)))
                AS total,
                b.create_by, b.payment_date as tgl_bayar, b.create_date as tgl_aktivasi, 
		b.update_by, b.pay_method as cara_bayar, b.keterangan
                FROM registrasi_jamaah a 
                LEFT JOIN `schedule` on `schedule`.id_schedule = a.id_schedule
                AND `schedule`.embarkasi = a.embarkasi
                LEFT JOIN aktivasi b ON b.id_booking = a.id_booking AND b.id_product = b.id_product 
		AND b.id_schedule = a.id_schedule
                where a.status in('1')  " . $filter_query . "
                GROUP BY a.id_booking 
                ORDER BY schedule.create_date ";
        $hasil = $this->db->query($sql)->result_array();
        //$this->myDebug($hasil);
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking'], $item['id_product'], $item['id_schedule']);
            $booking = $this->get_data_booking($item['id_booking']);
            $product = $this->get_data_product($item['id_product']);
            $tipe_jamaah = $this->get_data_tipe_jamaah($item['id_tipe_jamaah']);
            //LEFT JOIN tipe_jamaah tipe_jamaah  on tipe_jamaah.id_tipe_jamaah = a.tipe_jamaah
           
            $row = array();
            $row[] = $no;
            //$row[] = date('d-m-Y',strtotime($aktivasi['tgl_bayar']));
            //$row[] = date('d-m-Y',strtotime($aktivasi['tgl_aktivasi']));
            $tgl_bayar = !empty($item['tgl_bayar'])? date('d-m-Y',strtotime($item['tgl_bayar'])) : '';
            $tgl_aktivasi = !empty($item['tgl_aktivasi'])? date('d-m-Y',strtotime($item['tgl_aktivasi'])) : '';
            $row[] = '<b>Nomor : </b>'.$item['invoice'].
                     '<br><b>Tgl.Bayar : </b>'.$tgl_bayar.
                     '<br><b>Tgl.Aktivasi : </b>'.$tgl_aktivasi.
                     '<br><b>Total : </b>Rp. '.number_format($item['total'], 0, ',', '.');
            $row[] = '<b>ID : </b>'.$item['id_affiliate'].
                     '<br><b>Jml.Jamaah : </b>'.$item['jumlah_jamaah'].
                     '<br><b>Tgl.Daftar : </b>'.date('d-m-Y',strtotime($item['tgl_daftar'])).
                     '<br><b>Paket : </b>' . $product['paket'].' - '.$tipe_jamaah['tipe_jamaah'];
            //$row[] = 'Jml : '.$item['jumlah_jamaah'].'<br>Tgl.Daftar : '.date('d-m-Y',strtotime($item['tgl_daftar'])).'<br>Tipe : '.$item['tipe_jamaah'] . '<br>Paket : ' . $product['paket'];
            //$row[] = ;
            //$row[] = '';
            //$row[] = number_format($item['total'], 0, ',', '.');
            //$row[] = $booking['kode_unik'];
            $kode_unik = !empty($booking['kode_unik'])? $booking['kode_unik'] : '-';
            $user = ($item['create_by'] === $item['update_by']) ? $item['create_by'] : $item['create_by'] . '<br>' . $item['update_by'];
            $row[] = '<b>Cara Bayar : </b>'.$item['cara_bayar'].
                     '<br><b>Jml.Bayar : </b>Rp. '.number_format($item['total'], 0, ',', '.').
                     '<br><b>Kode.Unik : </b>'.$kode_unik.
                     '<br><b>User : </b>'.$user;
            //$row[] = number_format($item['total'], 0, ',', '.');
            $row[] = $item['keterangan'];
            //$row[] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'] . '<br>' . $aktivasi['update_by'];
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function export_rekap_registrasi() {
        $post_data = $this->input->post(NULL, FALSE);
        
        $tipe_tanggal = !empty($post_data['tipe_tanggal']) ? $post_data['tipe_tanggal'] : 'payment_date';
        $awal = !empty($post_data['awal']) ? $post_data['awal'] : date('Y-m-d');
        $akhir = !empty($post_data['akhir']) ? $post_data['akhir'] : date('Y-m-d');
        
        $filter_query = ($tipe_tanggal === 'create_date') ?
                " AND b.create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' " :
                " AND a.payment_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        $sql = "SELECT a.id_booking, a.id_product, a.id_schedule, a.invoice, a.id_affiliate as id_affiliate, 
                a.nama as nama_affiliate,COUNT(*)as jumlah_jamaah, a.tipe_jamaah as id_tipe_jamaah, 
                a.payment_date as payment_date,
                DATE_FORMAT(schedule.date_schedule,'%M %Y') as tgl_keberangkatan,
                a.tgl_daftar, a.create_date,                
                sum(a.refund) as refund,
                sum(a.fee) as fee,
                sum(a.fee_input) as fee_input,
                sum(a.muhrim)  as muhrim,
                sum(a.akomodasi) as akomodasi,
                sum(a.handling)  as handling,
                sum(a.visa) as visa,
                sum(a.harga_paket) as harga,
                sum(a.room_order) as Room_Price_2,
                sum(a.dpbooking_seats) as dpbooking_seats,
                (a.jml_angsuran) as jml_angsuran,
                (a.angsuran) as angsuran,
                (a.dp_angsuran) as dp_angsuran,
                (((sum(a.muhrim) + sum(a.harga_paket) + sum(a.akomodasi) + sum(a.handling)+ sum(a.visa) + sum(a.room_order))- sum(((a.jml_angsuran)* (a.angsuran))+(a.dp_angsuran)))-(sum(a.refund) + sum(a.fee)+ sum(a.dpbooking_seats)))
                AS total, sum(if(room_type=1,1,0)) as quard_2,sum(if(room_type=2,1,0)) as double_2,
		sum(if(room_type=3,1,0)) as triple_2,
                b.create_by, b.payment_date as tgl_bayar, b.create_date as tgl_aktivasi, 
		b.update_by, b.pay_method as cara_bayar, b.keterangan
                FROM registrasi_jamaah a 
                LEFT JOIN `schedule` on `schedule`.id_schedule = a.id_schedule
                AND `schedule`.embarkasi = `schedule`.embarkasi
                LEFT JOIN aktivasi b ON b.id_booking = a.id_booking AND b.id_product = b.id_product 
		AND b.id_schedule = a.id_schedule
                where a.status in('1')  " . $filter_query . "
                GROUP BY a.id_booking 
                ORDER BY schedule.create_date ";

        $hasil = $this->db->query($sql)->result_array();
       
        $rekap = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            $booking = $this->get_data_booking($item['id_booking']);
            $product = $this->get_data_product($item['id_product']);
            $tipe_jamaah = $this->get_data_tipe_jamaah($item['id_tipe_jamaah']);
            
            $row = array();
            $item['tgl_bayar'] = date('d/m/Y', strtotime($item['tgl_bayar']));
            $item['tgl_aktivasi'] = date('d/m/Y', strtotime($item['tgl_aktivasi']));
            $item['kode_unik'] = $booking['kode_unik'];
            $item['cara_bayar'] = $item['cara_bayar'];
            $item['nominal'] = $item['total'];
            $item['keterangan'] = $item['keterangan'];
            $item['user'] = ($item['create_by'] === $item['update_by']) ? $item['create_by'] : $item['create_by'] . ' - ' . $item['update_by'];
            $item['tipe_jamaah'] = $tipe_jamaah['tipe_jamaah'];
            $item['paket'] = $product['paket'];
            
            $rekap[] = $item;
        }
        $data['list'] = $rekap;
        
        $this->load->view('laporan/lap_rekap_registrasi_excel', ($data));
    }

    public function lap_detail_registrasi() {
        $this->template->load('template/template', $this->folder . '/lap_detail_registrasi');
    }

    public function json_detail_registrasi() {
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : date('Y-m-d');
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : date('Y-m-d');
        $tipe_tanggal = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : 'payment_date';
        $filter_query = ($tipe_tanggal === 'create_date') ?
                " AND b.create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' " :
                " AND a.payment_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        $sql = "SELECT a.invoice, a.id_booking, a.id_affiliate, a.nama as nama_jamaah,
            a. tgl_daftar, a.payment_date, a.dp_angsuran, a.angsuran, a.jml_angsuran,
            a.kode_unik, a.refund, a.muhrim, a.akomodasi, a.handling, a.fee, a.visa, 
            a.fee_input, a.harga_paket, a.dpbooking_seats, a.room_order,
            a.create_date, d.room_type, rt.type as tipe_kamar, h.nama as produk,
            j.category as category, l.departure as nama_embarkasi, s.date_schedule as tgl_berangkat,
            s.time_schedule as waktu_berangkat, tj.nama as tipe_jamaah,
            ((a.muhrim + a.harga_paket + a.akomodasi + a.handling + a.visa + a.room_order) - ((a.jml_angsuran * a.angsuran) + a.dp_angsuran) - (a.refund + a.fee + dpbooking_seats)) as total,
            b.create_by, b.payment_date as tgl_bayar, b.create_date as tgl_aktivasi, 
            b.update_by, b.pay_method as cara_bayar, b.keterangan
            FROM registrasi_jamaah a 
            LEFT JOIN room d ON d.category = a.category and d.room_type = a.room_type
            LEFT JOIN product h on h.id_product = a.id_product
            LEFT JOIN room_category j ON j.id_room_category = a.category
            LEFT JOIN room_type rt ON rt.id_room_type = d.room_type
            LEFT JOIN embarkasi l On l.id_embarkasi = a.embarkasi
            LEFT JOIN schedule s on s.id_schedule = a.id_schedule
            LEFT JOIN tipe_jamaah tj  on tj.id_tipe_jamaah = a.tipe_jamaah
            LEFT JOIN aktivasi b ON b.id_booking = a.id_booking AND b.id_product = b.id_product 
		AND b.id_schedule = a.id_schedule
            where a.status in('1') " . $filter_query . "
            ORDER BY a.create_date ";
        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            
            $row = array();
            $row[] = $no;
            
            $user = ($item['create_by'] === $item['update_by']) ? $item['create_by'] : $item['create_by'] . '<br>' . $item['update_by'];
            
            $row[] = '<b>Tgl.Daftar : </b>'.date('d/m/Y',strtotime($item['tgl_daftar'])).
                     '<br><b>Nama Jamaah : </b>'.$item['nama_jamaah'].
                     '<br><b>Tipe-Paket : </b>'.$item['tipe_jamaah'] . '-' . $item['produk'];
            //$row[] = date('d/m/Y',strtotime($item['payment_date']));
            $row[] = '<b>Invoice : </b>'.$item['invoice'].
                     '<br><b>Payment Date : </b>'.date('d/m/Y',strtotime($item['payment_date'])).
                     '<br><b>Total Bayar : </b>'.number_format($item['total'], 0, ',', '.').
                     '<br><b>Cara Bayar-Bank : </b>'.$item['cara_bayar'].
                     '<br><b>User : </b>'.$user;
            //$row[] = $item['nama_jamaah'];
            //$row[] = $item['tipe_jamaah'] . '<br>' . $item['produk'];
            //$row[] = number_format($item['total'], 0, ',', '.');
            //$row[] = $aktivasi['cara_bayar'];
            $row[] = $item['keterangan'];
            //$row[] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'] . '<br>' . $aktivasi['update_by'];
            $data[] = $row;
        }
        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function get_data_aktivasi($id_booking, $id_product, $id_schedule) {
        $this->db->select('pay_method, bank, nominal_pembayaran, keterangan, payment_date, create_date, create_by, update_by');
        $this->db->where('id_booking', $id_booking);
        $this->db->where('id_product', $id_product);
        $this->db->where('id_schedule', $id_schedule);
        $this->db->order_by('id_aktivasi', 'DESC');
        $hasil = $this->db->get('aktivasi')->row_array();
        $nama_update = $this->db->get_where('user', array('id_user' => $hasil['update_by']))->row_array();
        $nama_create = $this->db->get_where('user', array('id_user' => $hasil['create_by']))->row_array();
        $result = array(
            'cara_bayar' => !empty($hasil) ? $hasil['pay_method'] . ' ' . $hasil['bank'] : 'Tidak Ketemu',
            'nominal' => !empty($hasil['nominal_pembayaran']) ? $hasil['nominal_pembayaran'] : '0',
            'keterangan' => !empty($hasil['keterangan']) ? $hasil['keterangan'] : '',
            'tgl_bayar' => !empty($hasil['payment_date']) ? $hasil['payment_date'] : '',
            'tgl_aktivasi' => !empty($hasil['create_date']) ? $hasil['create_date'] : '',
            'update_by' => !empty($nama_update['nama']) ? $nama_update['nama'] : $hasil['update_by'],
            'create_by' => !empty($nama_create['nama']) ? $nama_create['nama'] : $hasil['create_by']
        );
        return $result;
    }

    function get_data_booking($id_booking) {
        $this->db->select('kode_unik');
        $this->db->where('id_booking', $id_booking);
        $this->db->order_by('id_booking', 'DESC');
        $hasil = $this->db->get('booking')->row_array();
        $result = array(
            'kode_unik' => !empty($hasil['kode_unik']) ? $hasil['kode_unik'] : ''
        );
        return $result;
    }
    
    function get_data_product($id_product) {
        $this->db->select('nama as paket');
        $this->db->where('id_product', $id_product);
        $this->db->order_by('id_product', 'DESC');
        $hasil = $this->db->get('product')->row_array();
        $result = array(
            'paket' => !empty($hasil['paket']) ? $hasil['paket'] : ''
        );
        return $result;
    }
    
    function get_data_tipe_jamaah($id_tipe_jamaah) {
        $this->db->select('nama as tipe_jamaah');
        $this->db->where('id_tipe_jamaah', $id_tipe_jamaah);
        $this->db->order_by('id_tipe_jamaah', 'DESC');
        $hasil = $this->db->get('tipe_jamaah')->row_array();
        $result = array(
            'tipe_jamaah' => !empty($hasil['tipe_jamaah']) ? $hasil['tipe_jamaah'] : ''
        );
        return $result;
    }

    function export_detail_registrasi() {
        
        $post_data = $this->input->post(NULL, FALSE);
        $tipe_tanggal = !empty($post_data['tipe_tanggal']) ? $post_data['tipe_tanggal'] : 'payment_date';
        $awal = !empty($post_data['awal']) ? $post_data['awal'] : date('Y-m-d');
        $akhir = !empty($post_data['akhir']) ? $post_data['akhir'] : date('Y-m-d');

        $filter_query = ($tipe_tanggal === 'tgl_daftar') ?
                " AND b.tgl_daftar BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' " :
                " AND a.payment_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        $sql = "SELECT a.invoice, a.id_booking, a.id_affiliate, a.nama as nama_jamaah,
            a. tgl_daftar, a.payment_date, a.dp_angsuran, a.angsuran, a.jml_angsuran,
            a.kode_unik, a.refund, a.muhrim, a.akomodasi, a.handling, a.fee, a.visa, 
            a.fee_input, a.harga_paket, a.dpbooking_seats, a.room_order,
            a.create_date, d.room_type, rt.type as tipe_kamar, h.nama as produk,
            j.category as category, l.departure as nama_embarkasi, s.date_schedule as tgl_berangkat,
            s.time_schedule as waktu_berangkat, tj.nama as tipe_jamaah,
            ((a.muhrim + a.harga_paket + a.akomodasi + a.handling + a.visa + a.room_order) - ((a.jml_angsuran * a.angsuran) + a.dp_angsuran) - (a.refund + a.fee + dpbooking_seats)) as total,
            b.create_by, b.payment_date as tgl_bayar, b.create_date as tgl_aktivasi, 
            b.update_by, b.pay_method as cara_bayar, b.keterangan
            FROM registrasi_jamaah a 
            LEFT JOIN room d ON d.category = a.category and d.room_type = a.room_type
            LEFT JOIN product h on h.id_product = a.id_product
            LEFT JOIN room_category j ON j.id_room_category = a.category
            LEFT JOIN room_type rt ON rt.id_room_type = d.room_type
            LEFT JOIN embarkasi l On l.id_embarkasi = a.embarkasi
            LEFT JOIN schedule s on s.id_schedule = a.id_schedule
            LEFT JOIN tipe_jamaah tj  on tj.id_tipe_jamaah = a.tipe_jamaah
            LEFT JOIN aktivasi b ON b.id_booking = a.id_booking AND b.id_product = b.id_product 
		AND b.id_schedule = a.id_schedule
            where a.status in('1') " . $filter_query . "
            ORDER BY a.create_date ";
        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            $item['cara_bayar'] = $item['cara_bayar'];
            $item['keterangan'] = $item['keterangan'];
            $item['user'] = ($item['create_by'] === $item['update_by']) ? $item['create_by'] : $item['create_by'] . ' - ' . $item['update_by'];
            $data[] = $item;
        }
        $data['list'] = $data;
        $this->load->view('laporan/lap_detail_registrasi_excel', ($data));
    }

    function lap_booking_seat() {
        
        $this->template->load('template/template', $this->folder . '/lap_booking_seat');
    }

    function lap_pindah_paket() {
        $this->template->load('template/template', $this->folder . '/lap_pindah_paket');
    }

    public function json_pindah_paket() {
        //$filter = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';

        $filter_query = '';
        if(!empty($awal) && !empty($akhir)){
            $filter_query = " AND create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        }
            /*$sql = "SELECT id_jamaah, nama, id_sahabat FROM registrasi_jamaah
        }
                where status = '1'  " . $filter_query . " AND id_sahabat <> 'null' AND tipe_jamaah = '2'
                ORDER BY create_date ";*/
        $sql = "SELECT id_affiliate, invoice, id_jamaah, nama, id_sahabat, tipe_jamaah, create_date from registrasi_jamaah where tipe_jamaah in(6,10)
AND  id_sahabat is not NULL AND status in (1,2) ".$filter_query;
        $hasil = $this->db->query($sql)->result_array();
        
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            $affiliate = $this->db->get_where('affiliate', array('id_user'=> $item['id_affiliate']))->row_array();
            //$tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah'=> $item['tipe_jamaah']))->row_array();
            $sql = "SELECT nama as tipe_paket FROM tipe_jamaah WHERE id_tipe_jamaah = '".$item['tipe_jamaah']."' ";
            $tipe = $this->db->query($sql)->row_array();
            
            $row = array();
            $row[] = $no;
            //$row[] = $aktivasi['tgl_bayar'];
            //$row[] = $aktivasi['tgl_aktivasi'];
            $row[] = $item['id_affiliate'].'<br>'.$affiliate['nama'];
            
            $row[] = $item['invoice'];
            $row[] = $item['id_jamaah'].'<br>'.$item['nama'].'<br>Tipe : '. $tipe['tipe_paket'];
            $row[] = $item['id_sahabat'];
            
            $row[] = date('d-m-Y',strtotime($item['create_date']));
            
            $data[] = $row;
        }
        //echo'<pre>';
        //print_r($data);
        //die();
        $output = array(
            "data" => $data,
        );
        //output to json format
        
        echo json_encode($output);
    }

    function export_pindah_paket() {
        //$filter = !empty($this->input->post('filter')) ? $this->input->post('filter') : date('Y-m-d');
        $awal = !empty($this->input->post('awal')) ? $this->input->post('awal') : '';
        $akhir = !empty($this->input->post('akhir')) ? $this->input->post('akhir') : '';
        $filter_query = '';
        if(!empty($awal) && !empty($akhir)){
            $filter_query = " AND create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        }

        $sql = "SELECT id_affiliate, invoice, id_jamaah, nama, id_sahabat, tipe_jamaah, create_date from registrasi_jamaah where tipe_jamaah in(6,10)
AND  id_sahabat is not NULL AND status in (1,2) ".$filter_query;
        $hasil = $this->db->query($sql)->result_array();
        
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            $affiliate = $this->db->get_where('affiliate', array('id_user'=> $item['id_affiliate']))->row_array();
            //$tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah'=> $item['tipe_jamaah']))->row_array();
            $sql = "SELECT nama as tipe_paket FROM tipe_jamaah WHERE id_tipe_jamaah = '".$item['tipe_jamaah']."' ";
            $tipe = $this->db->query($sql)->row_array();
            
            $row = array();
            $row[] = $no;
            $item['nama_affiliate'] = $affiliate['nama'];
            $item['tipe_paket'] = $tipe['tipe_paket'];
            //$row[] = $aktivasi['tgl_aktivasi'];
            $row[] = $item['id_affiliate'];
            $row[] = $item['nama_affiliate'];
            $row[] = $item['invoice'];
            $row[] = $item['id_jamaah'];
            $row[] = $item['nama'];
            $row[] = $item['tipe_paket'];
            $row[] = $item['id_sahabat'];
            
            $row[] = date('d-m-Y',strtotime($item['create_date']));
            
            $data[] = $item;
        }
        $data['list'] = $data;
//        echo '<pre>';
//        print_r($data['list']);
//        die();
        $this->load->view('laporan/lap_pindah_paket_excel', ($data));
    }
    
    //--------lap registrasi GM/MGM------//
    function lap_reg_gm_mgm() {
        $this->template->load('template/template', $this->folder . '/lap_reg_gm_mgm');
    }

    public function json_reg_gm_mgm() {
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : date('Y-m-d');
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : date('Y-m-d');

        $filter_query = " AND a.payment_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        
        $sql = "select b.nama,a.id_user,a.sponsor,b.id_sahabat,a.harga_aktivasi,a.keterangan, a.create_date,a.payment_date, a.pic1 from aktivasi_affiliate  as a, member_mgm as b
where a.id_user = b.id_user and a.`status`='1' ".$filter_query."  ORDER BY a.payment_date";
        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            //$booking = $this->get_data_booking($item['id_booking']);
            $row = array();
            $row[] = $no;
            //$row[] = $aktivasi['tgl_bayar'];
            //$row[] = $aktivasi['tgl_aktivasi'];
            $pic = $item['pic1'];
            $link = '';
            if($pic)
                $link = '<a class="btn-xs btn-primary" href="'. base_url().'assets/images/foto_copy/'.$pic.'" target="_blank">Image</a>';
            $row[] = $item['nama'];
            $row[] = $item['id_user'];
            $row[] = $item['sponsor'];
            $row[] = $item['id_sahabat'];
            $row[] = $item['harga_aktivasi'];
            $row[] = $item['keterangan'].'<br>'.$link;
            $row[] = $item['create_date'];
            $row[] = $item['payment_date'];
            
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        //print_r($data);
        echo json_encode($output);
    }

    function export_reg_gm_mgm() {
        $awal = !empty($this->input->post('awal')) ? $this->input->post('awal') : date('Y-m-d');
        $akhir = !empty($this->input->post('akhir')) ? $this->input->post('akhir') : date('Y-m-d');

        $filter_query = " AND a.payment_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        
        $sql = "select b.nama,a.id_user,a.sponsor,b.id_sahabat,a.harga_aktivasi,a.keterangan, a.create_date,a.payment_date from aktivasi_affiliate  as a, member_mgm as b
where a.id_user = b.id_user and a.`status`='1' ".$filter_query."  ORDER BY a.payment_date";

        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            //$item['cara_bayar'] = $aktivasi['cara_bayar'];
            //$item['keterangan'] = $aktivasi['keterangan'];
            //$item['user'] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'].' - '.$aktivasi['update_by'];
            $row[] = $no;
            //$row[] = $aktivasi['tgl_bayar'];
            //$row[] = $aktivasi['tgl_aktivasi'];
            $row[] = $item['nama'];
            $row[] = $item['id_user'];
            $row[] = $item['sponsor'];
            $row[] = $item['id_sahabat'];
            $row[] = $item['harga_aktivasi'];
            $row[] = $item['keterangan'];
            $row[] = $item['create_date'];
            $row[] = $item['payment_date'];
            //$row[] = '<form method="POST">'
            //        . '<button type="submit" value="edit"><input type="hidden" na value="'.$item['id_jamaah'].'"></form>';

            $data[] = $item;
        }
        $data['list'] = $data;
        //print_r($data);
        $this->load->view('laporan/lap_reg_gm_mgm_excel', ($data));
    }
    //--------lap registrasi GM/MGM------//
    
    //--------history mutasi nama------//
    function history_mutasi_nama() {
        $this->template->load('template/template', $this->folder . '/history_mutasi_nama');
    }
    
    public function json_history_mutasi_nama() {
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';

        if($awal=='' && $akhir==''){
            $filter_query = "WHERE create_date >= '2017-08-26'";
        }else{
            $filter_query = " WHERE create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        }
        
        $sql = "SELECT * FROM historis_mutasi_nama ".$filter_query."  ORDER BY create_date DESC";
        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            
            $kab = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id'=> $item['kabupaten_id']))->row_array();
            $prov = $this->db->get_where('wilayah_provinsi', array('provinsi_id'=> $item['provinsi_id']))->row_array();
            
            $reg = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $item['id_jamaah']))->row_array();
            
            $row = array();
            $row[] = $no;
            
            $pic = $item['pic1'];
            $link = '';
            if($pic)
                $link = '<a class="btn-xs btn-primary" href="'. base_url().'assets/images/foto_copy/'.$pic.'" target="_blank">Image</a>';
            
            $row[] = $item['no_transaksi'];
            $row[] = $item['id_jamaah'].'<br><b>'.$item['nama'].'</b><br><br>'.
                     'Invoice :<br>'.$reg['invoice'].'<br>';
            //$row[] = $item['no_pasport'];
            //$row[] = $item['status'];
            $row[] = number_format($item['biaya_mutasi_nama']);
            $row[] = $item['keterangan_pembayaran'];
            $row[] = '<b>'.$item['pay_method'].' '.$item['bank_transfer'].'</b><br>'.
                    date('d-m-Y',strtotime($item['payment_date'])).'<br>'.
                    $link;
            
            //$row[] = $reg['invoice'];
            $row[] = date('d-m-Y', strtotime($item['create_date']));
            //$row[] = $item['harga_aktivasi'];
            //$row[] = $item['keterangan'];
            //$row[] = $item['create_date'];
            //$row[] = $item['payment_date'];
            
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        //print_r($data);
        echo json_encode($output);
    }
    
    function export_history_mutasi_nama() {
        $awal = !empty($this->input->post('awal')) ? $this->input->post('awal') : '';
        $akhir = !empty($this->input->post('akhir')) ? $this->input->post('akhir') : '';

        if($awal=='' && $akhir==''){
            $filter_query = "WHERE create_date >= '2017-08-26'";
        }else{
            $filter_query = " WHERE create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        }
        
        $sql = "SELECT * FROM historis_mutasi_nama ".$filter_query."  ORDER BY create_date DESC";
        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        
        foreach ($hasil as $item) {
            $no++;
            
            $reg = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $item['id_jamaah']))->row_array();
            $item['invoice'] = $reg['invoice'];
            
            $row[] = $no;
            //$row[] = $aktivasi['tgl_bayar'];
            $row[] = $item['no_transaksi'];
            $row[] = $item['id_jamaah'];
            $row[] = $item['nama'];
            $row[] = $item['invoice'];
            $row[] = $item['biaya_mutasi_nama'];
            $row[] = $item['keterangan_pembayaran'];
            $row[] = $item['pay_method'];
            $row[] = $item['bank_transfer'];
            $row[] = date('d-m-Y',strtotime($item['payment_date']));
            $row[] = date('d-m-Y', strtotime($item['create_date']));
            //$row[] = $item['payment_date'];
            //$row[] = '<form method="POST">'
            //        . '<button type="submit" value="edit"><input type="hidden" na value="'.$item['id_jamaah'].'"></form>';

            $data[] = $item;
        }
        $data['list'] = $data;
        //print_r($data);
        $this->load->view('laporan/history_mutasi_nama_excel', ($data));
    }
    //--------history mutasi nama------//
}
