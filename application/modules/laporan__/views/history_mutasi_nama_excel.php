<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=history_mutasi_nama_".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>NO. TRANS</th>
                    <th>ID JAMAAH</th>
                    <th>NAMA JAMAAH</th>
                    <th>INVOICE</th>
                    <th>BIAYA MUTASI</th>
                    <th>KETERANGAN</th>
                    <th>PAY. METHOD</th>
                    <th>BANK TRANSFER</th>
                    <th>PAY. DATE</th>
                    <th>CREATE DATE</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['no_transaksi'] ?>  </td>
                        <td><?php echo $pi['id_jamaah'] ?>  </td>
                        <td><?php echo $pi['nama'] ?>  </td>
                        <td><?php echo $pi['invoice'] ?>  </td>
                        <td><?php echo $pi['biaya_mutasi_nama'] ?>  </td>
                        <td><?php echo $pi['keterangan_pembayaran'] ?>  </td>
                        <td><?php echo $pi['pay_method'] ?>  </td>
                        <td><?php echo $pi['bank_transfer'] ?>  </td>
                        <td><?php echo date('d-m-Y', strtotime($pi['payment_date'])) ?>  </td>
                        <td><?php echo date('d-m-Y', strtotime($pi['create_date'])) ?>  </td>
                        
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url();?>assets/css/opensans.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/raleway.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
