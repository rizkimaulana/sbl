<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feepostingadmin_model extends CI_Model{
    
    private $_table="fee";
    private $_primary="id_booking";

    private $_kuota_booking="kuota_booking";
    private $_id_booking="id_booking";

     private $_claim_feefree="claim_feefree";
    private $_id_claim_feefree="id_claim_feefree";

    public function save($data,$data2=''){
        

        $arr = array(
           'id_booking' => $data['id_booking'],
           'id_affiliate' => $data['id_affiliate'],
           'id_schedule' => $data['id_schedule'],
           'id_product' => $data['id_product'],
            'transfer_date' => $data['transfer_date'],
            'dana_bank' => $data['dana_bank'],
            'bank_transfer' => $data['bank_transfer'],
            'nama_rek' => $data['nama_rek'],
            'no_rek' => $data['no_rek'],
            'nominal' => $data['fee'],
            'keterangan' => $data['keterangan'],
            'claim_date' => date('Y-m-d H:i:s'),
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree'=>2,
        );       
        
        $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();

         $sql = "UPDATE FEE SET STATUS_FEE = '3',id_claim='".$id_claim_feefree."',update_date='".date('Y-m-d H:i:s')."',update_by='".$this->session->userdata('id_user')."' WHERE id_registrasi IN ($data2)";
        // $sql = "UPDATE FEE_INPUT SET STATUS = '3',fee_input='0',update_date='".date('Y-m-d H:i:s')."',update_by='".$this->session->userdata('id_user')."' WHERE id_registrasi IN ($data2)";
        $update_data = $this->db->query($sql);
        $update_data;
       if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }



    public function get_data($offset,$limit,$q=''){
    
     
          $sql = " SELECT * FROM fee_posting_jamaah_admin where 1=1
          ";
        
        if($q){
            
            $sql .=" AND affiliate LIKE '%{$q}%' 
            	    	OR invoice LIKE '%{$q}%'
                    OR id_affiliate LIKE '%{$q}%'
                    OR create_date LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY create_date DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   
     public function update_status_fee_posting_jamaah($id_booking){    
        $arr = array(
        
           'status_fee' => 2,

        );       
              
         $this->db->update($this->_table,$arr,array('id_booking'=>$id_booking));
    }
    
      public function get_pic($id_booking,$id_affiliate=''){
      
        $sql ="SELECT * from detail_fee_posting_jamaah_admin WHERE  id_booking  = ? and id_affiliate = ?
              ";
        return $this->db->query($sql,array($id_booking,$id_affiliate))->result_array();    

    }

   

    public function get_pic_posting($id_booking,$id_affiliate=''){
 

         $sql = "SELECT * from fee_posting_jamaah_admin where id_booking = '$id_booking' and id_affiliate = '$id_affiliate'";
        return $this->db->query($sql)->row_array();  
    }
}
