<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feeposting_admin extends CI_Controller{
	var $folder = "feeposting_admin";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(JAMAAH_KUOTA_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_affiliate');	
		$this->load->model('feepostingadmin_model');
	}
	
	public function index(){

	    $this->template->load('template/template', $this->folder.'/feeposting_admin');
		
	}
	

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->feepostingadmin_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="10%">'.$r->id_affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->create_date.'</td>';
	                $rows .='<td width="8%">'.$r->jml_jamaah.'</td>';
	                $rows .='<td width="10%">Rp '.number_format($r->total_fee).'</td>';
	             	$rows .='<td width="10%">Rp '.number_format($r->Potongan_pajak).'</td>';
	                $rows .='<td width="15%">Rp '.number_format($r->setelah_potong_pajak).'</td>';
	                 
	                $rows .='<td width="20%" align="center">';
	                    $rows .='<a title="Proses Fee" class="btn btn-sm btn-primary" href="'.base_url().'feeposting_admin/detail/'.$r->id_booking.'/'.$r->id_affiliate.'">
	                            <i class="fa fa-pencil"></i> Proses Fee
	                        </a> ';
	                // if ($r->status_fee == 1){ 
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Claim Fee" onclick="update_status_fee_posting_jamaah('."'".$r->id_booking."'".')"> Claim Fee</a> ';
	                // }elseif($r->status_fee == 2){ 
	                // 	$rows .='<a title="Menunggu Konfirmasi" class="btn btn-sm btn-primary" href="#">
	                //             </i> Menunggu Konfirmasi
	                //         </a> ';
	                // }else{
	                // 	$rows .='<a title="OK" class="btn btn-sm btn-success" href="#">
	                //             </i> OK
	                //         </a> ';
	                // }
	              
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'feeposting_admin/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	

	
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	

		public function detail(){

	    if(!$this->general->privilege_check(KELOLAH_FEE_POSTING,'edit'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $id_affiliate = $this->uri->segment(4);
	    $fee_posting = $this->feepostingadmin_model->get_pic_posting($id_booking,$id_affiliate);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$fee_posting ){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->feepostingadmin_model->get_pic($id_booking,$id_affiliate);
	       
	    }    

	    $data = array(
	    		'dana_bank'=>$this->_select_bank(),
	    		'bank_transfer'=>$this->_select_bank(),
	       		 'fee_posting'=>$fee_posting,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detailfeeposting_admin',($data));

	}
	
	function save(){
		$data = $this->input->post(null,true);
	    $data2 = $this->input->post('checkbox');
	    $inputan = '';
	    foreach($data2 as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;
	    }
	    $simpan = $this->feepostingadmin_model->save($data,$inputan);
	    if($simpan)
	        redirect('feeposting_admin');
	}

}
