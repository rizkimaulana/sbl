<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Refrensi_reward extends CI_Controller{ 
	var $folder = "bilyet_sahabat";
	private $db2;
	public function __construct(){
	
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(INSERT_REWARD_SAHABAT,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('bilyetsahabat_model');
		 $this->db2 = $this->load->database('db2', TRUE);
		
	}
	
	public function index(){
		$this->template->load('template/template', $this->folder.'/insert_reward');
	}



	public function detail(){
	    // if(!$this->general->privilege_check(JAMAAH_KUOTA_ADMIN,'view'))
		   //  $this->general->no_access();
	    
	    $id_groupbilyet = $this->uri->segment(3);
	    $get_bilyet = $this->bilyetsahabat_model->get_bilyet($id_groupbilyet);
	    if(!$get_bilyet){
	        show_404();
	    }
	      
	    $detail = $this->bilyetsahabat_model->get_detail_bilyet($id_groupbilyet);
	    $data = array(

	       		 'get_bilyet'=>$get_bilyet,
	       		 'detail'=>$detail
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_bilyet',($data));

	}
	
	

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->bilyetsahabat_model->get_data_refrensi_reward($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="5%">'.$r->userid.'</td>';
	                $rows .='<td width="10%">'.$r->nama.'</td>';
	                $rows .='<td width="5%">'.$r->kelas.'</td>';
	             	 $rows .='<td width="20%">'.$r->reward.'</td>';
	                $rows .='<td width="9%">'.$r->datewin.'</td>';
	                 $rows .='<td width="20%">'.$r->keterangan.'</td>';
	                  $rows .='<td width="9%">'.$r->admin.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="INSERT" class="btn btn-sm btn-primary" href="'.base_url().'bilyet_sahabat/refrensi_reward/proses_insert/'.$r->userid.'/'.$r->peringkat.'">
	                            <i class="fa fa-pencil"></i> INSERT
	                        </a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room('."'".$r->id_room."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	

	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'bilyet_sahabat/refrensi_reward/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	
	public function add_bilyet(){

		  if(!$this->general->privilege_check(INSERT_REWARD_SAHABAT,'add'))
		    $this->general->no_access();
		
		$data = $this->input->post(null,true);
		 $send = $this->bilyetsahabat_model->add_bilyet($data);
	  if($send)
	  	$this->session->set_flashdata('info', "Successfull");
			
			$id_groupbilyet = $this->input->post('id_groupbilyet');
	        redirect('bilyet_sahabat/detail/'.$id_groupbilyet.'');
	}




	 public function proses_insert($userid){
	 	if(!$this->general->privilege_check(INSERT_REWARD_SAHABAT,'add'))
		    $this->general->no_access();
	    
	    $userid = $this->uri->segment(4);
	    $peringkat = $this->uri->segment(5);

	    $insert_prestasi = $this->bilyetsahabat_model->insert_reward($userid);
	    $pic    = array();
	    if(!$insert_prestasi){
	        show_404();
	    }
	    $peringkat = $this->bilyetsahabat_model->peringkat($peringkat);
	      
	    $data = array(

	       		 'insert_prestasi'=>$insert_prestasi,
	       		 'peringkat'=>$peringkat
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/proses_insert_reward',($data));

	}

	public function save_insert_reward(){    
	 $userid = $this->input->post('userid');
	 $kelas = $this->input->post('peringkat_reward'); 
    // printf($kelas);
   	 $cek_bilyet=$this->bilyetsahabat_model->cek_bilyet($userid,$kelas);
      $cek_reward=$this->bilyetsahabat_model->cek_reward($userid,$kelas);
     $sql = "SELECT COUNT(*) as jumlah FROM t_req_bilyet Where userid ='$userid' and peringkat_reward='$kelas'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah = $value['jumlah'];

     }
     printf($cek_reward->num_rows());
     	$this->form_validation->set_rules('userid','userid','required|trim');
	   $this->form_validation->set_rules('peringkat_reward','peringkat_reward','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
             // cek kode di database
           if ($cek_bilyet->num_rows()> 0) {
            	$this->session->set_flashdata('info', "USERID DAN KELAS SUDAH ADA DI LIST DATA BILYET");
            	redirect('bilyet_sahabat/refrensi_reward');
           	// printf('format');
           }elseif ($cek_reward->num_rows() > 0) {
           		$this->session->set_flashdata('info', "USERID DAN KELAS SUDAH ADA DI LIST DATA REWARD");
            	redirect('bilyet_sahabat/refrensi_reward');
           	// printf('format a');
           }
           else{
            	$data = $this->input->post(null,true);
				$send = $this->bilyetsahabat_model->save_insert_reward($data);
				if($send)
				$this->session->set_flashdata('info', "DATA SUKSES INSERT");
			    redirect('bilyet_sahabat/refrensi_reward');
           }
	}

}
}
