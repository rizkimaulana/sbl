<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bilyet_sahabat extends CI_Controller{ 
	var $folder = "bilyet_sahabat";
	private $db2;
	public function __construct(){
	
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(BILYET_REWARD_SAHABAT,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('bilyetsahabat_model');
		 $this->db2 = $this->load->database('db2', TRUE);
		 $this->load->library('terbilang');
		  $this->load->library('ci_qr_code');
        $this->config->load('qr_code');
	}
	
	public function index(){
	 //      $data = array(
		// 	'select_reward' => $this->_select_reward(),
		// );
	    // $this->template->load('template/template', $this->folder.'/bilyet_sahabat',$data);
		$this->template->load('template/template', $this->folder.'/bilyet_sahabat');
	}
	
	public function generate_old(){
	      $data = array(
			'select_reward' => $this->_select_reward(),
		);
	    $this->template->load('template/template', $this->folder.'/bilyet_sahabatold',$data);

	}
	private function _select_reward(){
	$id = array('5');
		$this->db2->where_in('id', $id);
	    return $this->db2->get('t_reward_old')->result();
	}
	

	
	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->bilyetsahabat_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

public function save_old(){    
	 $userid = $this->input->post('userid');
	 $kelas = $this->input->post('kelas'); 
    
   	 $cek_bilyet=$this->bilyetsahabat_model->cek_bilyet_old($userid,$kelas);
     $cek_data_reward=$this->bilyetsahabat_model->cek_data_reward_old($userid,$kelas);

     $sql = "SELECT COUNT(*) as jumlah FROM t_reward_member Where userid='".$userid."' and peringkat = '".$kelas."'";
        $query = $this->db2->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah = $value['jumlah'];

     }
	   $this->form_validation->set_rules('userid','userid','required|trim');
	   $this->form_validation->set_rules('kelas','kelas','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
          
          
             // cek kode di database
           if ($cek_bilyet->num_rows()>0) {
            	$this->session->set_flashdata('info', "USERID DAN KELAS SUDAH ADA DI LIST DATA BILYET");
            	redirect('bilyet_sahabat/generate_old');
            }elseif ($jumlah == 0) {
            	$this->session->set_flashdata('info', "USERID DAN KELAS YANG ANDA INPUT BELUM ADA REWARD");            
            	redirect('bilyet_sahabat/generate_old');
            }elseif ($jumlah > 1) {
            	$this->session->set_flashdata('info', "USERID DAN KELAS INI Lebih dari 1");            
            	redirect('bilyet_sahabat/generate_old');
            }else{
            	$data = $this->input->post(null,true);
				$send = $this->bilyetsahabat_model->save_generate_old($data);
				if($send)
					$this->session->set_flashdata('success', "Create bilyet sukses");   
					$sql = "SELECT *  FROM t_group_bilyetprestasi Where userid='".$userid."' and peringkat = '".$kelas."'";
			        $query = $this->db2->query($sql)->result_array();
				      foreach($query as $key=>$value){
			         $id_groupbilyet = $value['id_groupbilyet'];
					}
			    redirect('bilyet_sahabat/detail/'.$id_groupbilyet.'');
            }
	}

}
	public function save(){    
	 $userid = $this->input->post('userid');
	 $kelas = $this->input->post('peringkat_reward'); 
    
   	 $cek_bilyet=$this->bilyetsahabat_model->cek_bilyet($userid,$kelas);
     $cek_data_reward=$this->bilyetsahabat_model->cek_data_reward($userid,$kelas);

     $sql = "SELECT COUNT(*) as jumlah FROM generate_bilyet_prestasi Where userid='".$userid."' and peringkat_reward = '1'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah = $value['jumlah'];

     }
	   $this->form_validation->set_rules('userid','userid','required|trim');
	   $this->form_validation->set_rules('peringkat_reward','peringkat_reward','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
          
          
             // cek kode di database
           if ($cek_bilyet->num_rows()>0) {
            	$this->session->set_flashdata('info', "USERID DAN KELAS SUDAH ADA DI LIST DATA BILYET");
            	redirect('bilyet_sahabat');
            }

            else{
            	$data = $this->input->post(null,true);
            	// print_r('expression');
				$send = $this->bilyetsahabat_model->save($data);
				if($send)
					$this->session->set_flashdata('success', "Create bilyet sukses");   
					$sql = "SELECT *  FROM t_group_bilyetprestasi Where userid='".$userid."' and peringkat = '".$kelas."'";
			        $query = $this->db2->query($sql)->result_array();
				      foreach($query as $key=>$value){
			         $id_groupbilyet = $value['id_groupbilyet'];
					}
			    redirect('bilyet_sahabat/detail/'.$id_groupbilyet.'');
            }
	}

}


public function save_cetak_bilyet(){    
	 $id_bilyet = $this->input->post('id_bilyet');
	 $id_sahabat = $this->input->post('id_sahabat'); 

     $userid = $this->input->post('id_sahabat');
	 $kelas = $this->input->post('kelas'); 
   	 $cek_cetak_bilyet=$this->bilyetsahabat_model->cek_cetak_bilyet($id_bilyet,$id_sahabat);
     
	   $this->form_validation->set_rules('keterangan','keterangan','required|trim');
	 
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
          
             // cek kode di database
           if ($cek_cetak_bilyet->num_rows()>0) {
	           	$data = $this->input->post(null,true);
				$send = $this->bilyetsahabat_model->update_cetak($data);
				if($send)
            	redirect('bilyet_sahabat/cetak_bilyet/'.$id_bilyet.'');
            
            }else{
            	$data = $this->input->post(null,true);
				$send = $this->bilyetsahabat_model->save_cetak($data);
				if($send)
            	redirect('bilyet_sahabat/cetak_bilyet/'.$id_bilyet.'');
            }
	}

}
	public function detail(){
	    // if(!$this->general->privilege_check(JAMAAH_KUOTA_ADMIN,'view'))
		   //  $this->general->no_access();
	    
	    $id_groupbilyet = $this->uri->segment(3);
	    $get_bilyet = $this->bilyetsahabat_model->get_bilyet($id_groupbilyet);
	    if(!$get_bilyet){
	        show_404();
	    }
	      
	    $detail = $this->bilyetsahabat_model->get_detail_bilyet($id_groupbilyet);
	    $data = array(

	       		 'get_bilyet'=>$get_bilyet,
	       		 'detail'=>$detail
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_bilyet',($data));

	}
	
	


	function print_qr($id_bilyet)
    {
        $qr_code_config = array();
        $qr_code_config['cacheable'] = $this->config->item('cacheable');
        $qr_code_config['cachedir'] = $this->config->item('cachedir');
        $qr_code_config['imagedir'] = $this->config->item('imagedir');
        $qr_code_config['errorlog'] = $this->config->item('errorlog');
        $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
        $qr_code_config['quality'] = $this->config->item('quality');
        $qr_code_config['size'] = $this->config->item('size');
        $qr_code_config['black'] = $this->config->item('black');
        $qr_code_config['white'] = $this->config->item('white');
        $this->ci_qr_code->initialize($qr_code_config);

        // get full name and user details
         $id_bilyet = $this->uri->segment(3);
	    $bilyet = $this->bilyetsahabat_model->cetak_bilyet($id_bilyet);
        $image_name = $id_bilyet . ".png";

        // create user content
        // $codeContents = "user_name:";
        // $codeContents .= "$user_details->user_name";
        // $codeContents .= " user_address:";
        // $codeContents .= "$user_details->user_address";
        // $codeContents .= "\n";
        // $codeContents .= "user_email :";
        $codeContents .= $bilyet['id_bilyet'];

        $params['data'] = $codeContents;
        $params['level'] = 'H';
        $params['size'] = 2;

        $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
        $this->ci_qr_code->generate($params);

        $this->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;

        // save image path in tree table
        // $this->user->change_userqr($user_id, $image_name);
        // then redirect to see image link
        $file = $params['savename'];
        if(file_exists($file)){
            header('Content-Description: File Transfer');
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            unlink($file); // deletes the temporary file

            exit;
        }
    }

	private function _select_user(){
		
		return $this->db->get('user')->result();
	}

	 public function bilyet(){
	


	    if(!$this->general->privilege_check(BILYET_REWARD_SAHABAT,'add'))
		    $this->general->no_access();
	    
	    $id_bilyet = $this->uri->segment(3);
	    $get = $this->db2->get_where('detail_bilyet_reward',array('id_bilyet'=>$id_bilyet))->row_array();
	    if(!$get)
	        show_404();
	        
	   			 $data = array('select_user'=>$this->_select_user(),
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/cetak_bilyet_sahabat',array_merge($get,$data));

	}



	public function cetak_bilyet(){

	   
	    
	    $id_bilyet = $this->uri->segment(3);
	    $bilyet = $this->bilyetsahabat_model->cetak_bilyet($id_bilyet);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$bilyet){
	        show_404();
	    }
	   

	      
	    $data = array(

	       		 // 'booking'=>$booking,
	       		 'bilyet'=>$bilyet,
	       		 // 'pic'=>$pic
	       		 );
	
	    // $this->template->load('template/template', $this->folder.'/bilyet_barcode',($data));
	    $this->load->view('cetak',($data));
	}

	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->bilyetsahabat_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->nama.'</td>';
	                $rows .='<td width="20%">'.$r->userid.'</td>';
	                $rows .='<td width="10%">'.$r->kelas.'</td>';
	             	 $rows .='<td width="20%">'.$r->harga.'</td>';
	                $rows .='<td width="9%">'.$r->sisa.'</td>';
	                 $rows .='<td width="9%">'.$r->total_bilyet.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="detail" class="btn btn-sm btn-primary" href="'.base_url().'bilyet_sahabat/detail/'.$r->id_groupbilyet.'">
	                            <i class="fa fa-pencil"></i> DETAILS
	                        </a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room('."'".$r->id_room."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	

	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'bilyet_sahabat/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

		public function groping_bilyet(){
	    
	    if(!$this->general->privilege_check(GROUPING_BILYET_REWARD_SAHABAT,'view'))
		    $this->general->no_access();
	    $this->template->load('template/template', $this->folder.'/grouping_bilyet_reward');
		
	}

	   private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}

	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_roomcategory(){
		 $id_room_category = array('1','2');
		$this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

	function add_ajax_kab($id_prov){
	    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
	    $data = "<option value=''>- Select Kabupaten -</option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_keluarga(){
	// $id_booking = $this->uri->segment(4);
	// $this->db->where_in('id_booking', $id_booking);
    return $this->db->get('jamaah_kuota')->result();
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}


	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}

	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->bilyetsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->bilyetsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->bilyetsahabat_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}

function add_ajax_kab_manasik($id_prov){
		    $query = $this->db->get_where('view_manasik',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->kabupaten."</option>";
		    }
		    echo $data;
	}
	
	function add_ajax_kec_manasik($id_kab){
	    $query = $this->db->get_where('view_manasik',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->cabang."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des_manasik($id_kec){
	    $query = $this->db->get_where('view_manasik',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->cabang.  "</option>";
	    }
	    echo $data;
	}

	private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}

	private function _select_affiliate_type(){
		$id_affiliate_type = array('1','2','3', '5');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
	    return $this->db->get('affiliate_type')->result();
	}
	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3','5');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		$this->db->order_by("nama", "asc");
		return $this->db->get('affiliate')->result();
	    
	}
	public function mutasi_data_jamaah(){
	    if(!$this->general->privilege_check(BILYET_REWARD_SAHABAT,'view'))
		    $this->general->no_access();

		

	    $id = $this->uri->segment(3);
	    $get = $this->db2->get_where('t_bilyet_prestasi',array('id_bilyet'=>$id))->row_array();
	   // if(!$get)
	   //      show_404();
	        // $grouping_kuota = $this->bilyetsahabat_model->get_booking_kuota_aktif($id_booking);
	    $data = array(
	    		 'select_product'=>$this->_select_product(),
				'select_embarkasi'=>$this->_select_embarkasi(),
				'select_roomcategory'=>$this->_select_roomcategory(),
	    		// 'grouping_kuota'=>$grouping_kuota,
	    		'provinsi'=>$this->bilyetsahabat_model->get_all_provinsi(),
		    	'kabupaten'=>$this->bilyetsahabat_model->get_all_kabupaten(),		
		    	'select_kelamin'=>$this->_select_kelamin(),	
		    	'select_rentangumur'=>$this->_select_rentangumur(),
		    	'select_statuskawin'=>$this->_select_statuskawin(),
		    	'select_status_hubungan'=>$this->_select_status_hubungan(),
		    	'family_relation'=>$this->_family_relation(),
		    	// 'select_keluarga'=>$this->_select_keluarga(),
		    	'select_pemberangkatan'=>$this->_select_pemberangkatan(),
		    	'select_type'=>$this->_select_type(),
		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'select_merchandise'=>$this->_select_merchandise(),
		    	'provinsi_manasik'=>$this->bilyetsahabat_model->get_all_provinsi_manasik(),
	       		 'select_status_visa'=>$this->_select_status_visa(),
	       		'select_affiliate'=>$this->_select_affiliate(),
	         	'select_affiliate_type'=>$this->_select_affiliate_type(),
	    	);
		$this->template->load('template/template', $this->folder.'/mutasi_reward',array_merge($get,$data));

	}

	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->order_by("nama", "asc")->get_where(
		    	'affiliate',array(
		    		'id_affiliate_type'=>$id_affiliate_type,
		    		'status'=>1
		    		)

		    	);
		    $this->db->order_by("nama", "asc");
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama." ".$value->id_user."</option>";
		    }
		    echo $data;
	}


	public function save_mutasi(){    
	 


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            $no_identitas=$this->input->post('no_identitas'); // mendapatkan input dari kode
            $cek=$this->bilyetsahabat_model->cek($no_identitas);

          
            if($cek->num_rows()>0){ // jika kode sudah ada, maka tampilkan pesan
                 
               // echo'<div class="alert alert-dismissable alert-danger"><small>No Identitas sudah digunakan</small></div>';
            	$this->session->set_flashdata('Warning', "No Identitas sudah digunakan");
            	 $id_bilyet = $this->input->post('id_bilyet');
            	 
            	redirect('bilyet_sahabat/mutasi_data_jamaah/'.$id_bilyet.'');

            }else{
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    // $inputan = '';
	    // foreach($schedule as $value){
	    // 	$inputan .= ($inputan!=='') ? ',' : '';
	    // 	$inputan .= $value;
	    // }
	   
	  $send = $this->bilyetsahabat_model->save_mutasi($data);
	  if($send)
	  	$this->session->set_flashdata('info', "Successfull");
			
			$id_groupbilyet = $this->input->post('id_groupbilyet');
	        redirect('bilyet_sahabat/detail/'.$id_groupbilyet.'');
	         // redirect('bilyet_sahabat/groping_bilyet');
					// echo'<div class="alert alert-dismissable alert-success"><h4>Transaction Successfull</h4></div>';
		}

	}else{
		
		// $this->session->set_flashdata('Warning', "Pilih Jadwal Keberangkatan");
		// $id_booking = $this->input->post('id_booking');
		// $id_jamaah = $this->input->post('id_jamaah');
	    redirect('bilyet_sahabat/groping_bilyet');
	}
	}

	public function add_bilyet(){

		  if(!$this->general->privilege_check(BILYET_REWARD_SAHABAT,'add'))
		    $this->general->no_access();
		
		$data = $this->input->post(null,true);
		 $send = $this->bilyetsahabat_model->add_bilyet($data);
	  if($send)
	  	$this->session->set_flashdata('info', "Successfull");
			
			$id_groupbilyet = $this->input->post('id_groupbilyet');
	        redirect('bilyet_sahabat/detail/'.$id_groupbilyet.'');
	}


		public function get_data_generate_bilyet_prestasi(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->bilyetsahabat_model->get_data_generate_bilyet_prestasi($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->nama.'</td>';
	                $rows .='<td width="10%">'.$r->userid.'</td>';
	                $rows .='<td width="20%">'.$r->ket.'</td>';
	             	$rows .='<td width="8%">'.$r->jml_bilyet.'</td>';
	                $rows .='<td width="9%">'.$r->created_date.'</td>';
	                $rows .='<td width="9%">'.$r->tgl_generate.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                if ($r->tgl_skrng >= $r->tgl_generate){ 
	                $rows .='<a title="GENERATE BILYET" class="btn btn-sm btn-primary" href="'.base_url().'bilyet_sahabat/generate_bilyet_prestasi/'.$r->userid.'/'.$r->peringkat_reward.'">
	                            <i class="fa fa-pencil"></i> GENERATE BILYET
	                        </a> ';
	                  }else{ 
	                $rows .='<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-pencil"></i> BELUM BISA GENERATE</a> ';
	               }
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging_generate_bilyet($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	
	private function _paging_generate_bilyet($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'bilyet_sahabat/get_data_generate_bilyet_prestasi/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}



	 public function generate_bilyet_prestasi($userid){
	


	    // if(!$this->general->privilege_check(BILYET_REWARD_SAHABAT,'view'))
		   //  $this->general->no_access();
	    
	    $userid = $this->uri->segment(3);

	    $bilyet_prestasi = $this->bilyetsahabat_model->get_pic_generate_bilyet($userid);
	    // $pic    = array();
	    // if(!$bilyet_prestasi){
	    //     show_404();
	    // }
	    

	      
	    $data = array(

	       		 'bilyet_prestasi'=>$bilyet_prestasi
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/bilyet_sahabat_generate',($data));

	}
}
