<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Approved_manasik extends CI_Controller {

    var $folder = "approved_manasik";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(BIAYA_OPERASIONAL_MANASIK, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('approvedmanasik_model');
        $this->load->model('approvedmanasik_model', 'r');
    }

    public function index() {
        //    $data = array(
        // 	'pic' => $this->approvedmanasik_model->get_data_fee('order by id_booking desc')->result_array(),
        // );
        $this->template->load('template/template', $this->folder . '/approved_manasik');
    }

    public function ajax_list() {
        $list = $this->r->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;

            // $sql = "SELECT * from  affiliate where id_user ='".$r->id_affiliate."'
            //                   ";
            //                  $query = $this->db->query($sql)->result_array();
            //                  foreach($query as $key=>$value){
            //                    $nama = $value['nama'];
            //                  }

            $row = array();
            $row[] = $no;
            $row[] = '<td width="10%">' . $r->kd_manasik . '</td>';
            $row[] = '<td width="20%" >' . $r->id_affiliate . ' - ' . $r->affiliate . '</td>';
            $row[] = '<td width="8%">' . $r->tgl_claim . '</td>';
            $row[] = '<td width="8%">' . $r->jml_jamaah . '' . ' Orang' . '</td>';
            $row[] = '<td width="10%">Rp ' . number_format($r->biaya_manasik) . '</td>';


            $row[] = '<a title="Detail" class="btn btn-sm btn-primary" href="' . base_url() . 'approved_manasik/detail/' .
                    $r->id_manasik . '/' . $r->kd_manasik . '/' . $r->id_affiliate . '">
	                             <i class="fa fa-pencil"></i> DETAIL
	                        </a>  ';


            if ($r->status == 0) {
                $row[] = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="Approved" onclick="approved(' . "'" . $r->id_manasik . "'" . ')"><i class="fa fa-pencil"></i> Approved</a> ';
            } else {
                $row[] = '<a title="DONE" class="btn btn-sm btn-primary" href="#">
	                            <i class="fa fa-pencil"></i> DONE
	                        </a>  ';
            }


            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->r->count_all(),
            "recordsFiltered" => $this->r->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    private function _select_bank() {

        return $this->db->get('bank')->result();
    }

    public function detail() {

        if (!$this->general->privilege_check(BIAYA_OPERASIONAL_MANASIK, 'edit'))
            $this->general->no_access();

        $id_manasik = $this->uri->segment(3);
        $detail = $this->approvedmanasik_model->get_pic_posting($id_manasik);
        $pic = array();


        $data = array(
            'dana_bank' => $this->_select_bank(),
            'bank_transfer' => $this->_select_bank(),
            'detail' => $detail
        );

        $this->template->load('template/template', $this->folder . '/detailapproved_manasik', ($data));
    }

    // function save(){
    // 	// $data = $this->input->post(null,true);
    //  //    $data2 = $this->input->post('checkbox');
    //  //    $inputan = '';
    //  //    foreach($data2 as $value){
    //  //    	$inputan .= ($inputan!=='') ? ',' : '';
    //  //    	$inputan .= $value;
    //  //    }
    //  //    $simpan = $this->approvedmanasik_model->save($data,$inputan);
    //  //    if($simpan)
    //  //        redirect('fee_postingsponsoradmin');
    // 	$id_booking = $this->input->post('id_booking');
    //     $data = $this->input->post(null,true);
    //     $data2 = $this->input->post('id_registrasi');
    //   // print_r($data);
    //     if($this->approvedmanasik_model->save($data)){
    //         // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
    //       	redirect('fee_postingsponsoradmin');  
    //     }
    // }


    public function approved($id_manasik) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->approvedmanasik_model->update_pengajuan($id_manasik);
        echo json_encode(array("status" => TRUE));
    }

}
