<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Detailapproved_manasik extends CI_Controller {

    var $folder = "approved_manasik";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(BIAYA_OPERASIONAL_MANASIK, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('detailapprovedmanasik_model');
        $this->load->model('detailapprovedmanasik_model', 'r');
    }

    public function index() {
        //    $data = array(
        // 	'pic' => $this->detailapprovedmanasik_model->get_data_fee('order by id_booking desc')->result_array(),
        // );
        $this->template->load('template/template', $this->folder . '/detailapproved_manasik');
    }

    public function ajax_list() {
        $list = $this->r->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $list2 = array();
        foreach ($list as $r) {
            $no++;

            // $sql = "SELECT * from  affiliate where id_user ='".$r->id_affiliate."'
            //                   ";
            //                  $query = $this->db->query($sql)->result_array();
            //                  foreach($query as $key=>$value){
            //                    $nama = $value['nama'];
            //                  }

            $row = array();
            $row[] = $no;
            $row[] = '<td width="10%">' . $r->kd_manasik . 
                    //'<input type="hidden" name="id" value="'.$r->id_manasik.'">'.
                    //'<input type="hidden" name="kd" value="'.$r->kd_manasik.'">'.
                    //'<input type="hidden" name="affiliate" value="'.$r->id_affiliate.'">'.
                    '</td>';
            $row[] = '<td width="20%" >' . $r->id_jamaah . ' - ' . $r->nama . '</td>';
            $row[] = '<td width="8%">' . $r->tgl_claim . '</td>';
            $row[] = '<td width="10%">Rp ' . number_format($r->biaya_manasik) . '</td>';


            $row[] = '<a title="DELETE" class="btn btn-sm btn-danger" href="javascript:void(0)" title="DELETE" onclick="delete_data(' . "'" . $r->id_registrasi . "'" . ')">
	                             <i class="fa fa-pencil"></i> DELETE
	                        </a>  ';


            // if ($r->status_fee_sponsor == 2){ 
            // 	 $row[] ='<a class="btn btn-sm btn-danger" href="javascript:void()" title="PROSES" onclick="update_pengajuan('."'".$r->id_booking."'".')"><i class="fa fa-pencil"></i> PROSES</a> ';
            //          }elseif ($r->status_fee_sponsor == 3){
            //          	$row[] ='<a title="TRANSFERRED" class="btn btn-sm btn-primary" href="'.base_url().'fee_postingsponsoradmin/detail/'.$r->id_booking.'/'.$r->sponsor.'">
            //                        <i class="fa fa-pencil"></i> TRANSFERRED
            //                    </a>  ';
            //          }

            
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->r->count_all(),
            "recordsFiltered" => $this->r->count_filtered(),
            "data" => $data,
        );
        
        //output to json format
        echo json_encode($output);
    }

    public function delete_jamaah($id_registrasi) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->detailapprovedmanasik_model->delete_jamaah($id_registrasi);

        echo json_encode(array("status" => TRUE));
        // if($send)
        //       redirect('fee_postingsponsor');
    }
    
    public function export_excel(){
        $data = $this->input->post();
        
        $id_manasik = !empty($data['id_manasik']) ? $data['id_manasik'] : '';
        $kd_manasik = !empty($data['kd_manasik']) ? $data['kd_manasik'] : '';
        $id_affiliate = !empty($data['id_affiliate']) ? $data['id_affiliate'] : '';
        
        if(!empty($id_manasik) && !empty($kd_manasik) && !empty($id_affiliate)){
            $filter_query = "id_manasik = '" . $id_manasik . "' AND kd_manasik = '" . $kd_manasik . "' AND id_affiliate = '".$id_affiliate."'";
        }
        
        $hasil = $this->r->get_detail_manasik($filter_query);
        
        $data = array();
        foreach ($hasil as $row) {
            $item = array();
            $item['kd_manasik'] = $row->kd_manasik;
            $item['id_jamaah'] = $row->id_jamaah;
            $item['nama'] = $row->nama;
            $item['tgl_claim'] = $row->tgl_claim;
            $item['biaya_manasik'] = $row->biaya_manasik;
            
            $data[] = $item;
        }
        $data['list'] = $data;
        
        $this->load->view('approved_manasik/detailapproved_manasik_excel', ($data));
    }

}
