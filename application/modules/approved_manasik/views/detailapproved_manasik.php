<style type="text/css">
    body {
        font-family: verdana,arial,sans-serif;
        margin: 0px;
        padding: 0px;
    }

    .wrap { 
        width:50%; 
        background:#F0F0F0; 
        margin:auto;
        padding: 25px;
        overflow: hidden;
    }

    h1 {
        text-align: center;
    }

    input.pemberangkatan {
        font-size:28px; 
        width:380px;
    }

    input, textarea {
        border: 1px solid #CCC;
    }
</style>
<form role="form" action="<?php echo base_url(); ?>/approved_manasik/detailapproved_manasik/export_excel" method="post">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>DETAIL JAMAAH MANASIK </b>
                    </div>
                    <div class="panel-body">                       
                        <input type="hidden" name="id_manasik" value="<?php echo $detail['id_manasik']; ?>" id="id_manasik" >
                        <input type="hidden" name="kd_manasik" value="<?php echo $detail['kd_manasik']; ?>">
                        <input type="hidden" name="id_affiliate" value="<?php echo $detail['id_affiliate']; ?>" id="id_affiliate" >
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Jamaah Manasik</b>
                                    </div>
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="form-group">                                                
                                                <div class="col-sm-4">

                                                    <button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="table-responsive">
                                            <table id="data-table" class="table table-striped table-bordered table-hover" >
                                                <thead>
                                                    <tr>
                                                            <!-- <th><input id="selecctall" type="checkbox">&nbsp;Check All</th> -->
                                                        <th>No</th>
                                                        <th>KODE</th>
                                                        <th>DATA JAMAAH</th>
                                                        <th>TGL CLAIM</th>
                                                        <th>Biaya </th>
                                                        <th>ACTION </th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>   

                    </div>
                </div>
            </div>
        </div>
    </div>
</form> 
<script type="text/javascript">
    var save_method;
    var table;

    $(document).ready(function () {

        //datatables
        table = $('#data-table').DataTable({

            // "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('approved_manasik/detailapproved_manasik/ajax_list') ?>",
                "type": "POST",
                "data": function (data) {
                    data.id_manasik = $('#id_manasik').val();
                    // data.schedule = $('#schedule').val();
                    data.id_affiliate = $('#id_affiliate').val();
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });

        $('#id_affiliate').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });


        $('#id_manasik').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
    });

    function reload_table()
    {
        table.ajax.reload(null, false); //reload datatable ajax 
    }


    function delete_data(id_registrasi)
    {
        if (confirm('APAKAH ANDA AKAN DELETE DATA JAMAAH INI? '))
        {
            // ajax delete data to database
            $.ajax({
                url: "<?php echo site_url('approved_manasik/detailapproved_manasik/delete_jamaah') ?>/" + id_registrasi,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    reload_table();
                    alert('Error APPROVED data');
                    // window.location.href="<?php echo site_url('no_access') ?>";
                }
            });

        }
    }
</script>