
<div class="page-head">
            <h2>FEE</h2>
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Master</a></li>
              <li class="active">APPROVED DATA MANASIK</li>
            </ol>
        </div>      
<div id="page-wrapper">
   <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>APPROVED DATA MANASIK</b>
                </div>
                <!-- /.panel-heading -->
                 <div class="panel-body">
                    
                      <form role="form">
                           
                           
                        </form>           
                          
                          <div class="table-responsive">
                              <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                    <th>#</th>
                                    <th>KD TRANSAKSI</th>
                                  
                                    <th>Data Affiliate</th>
                                    <th>Tgl Claim</th>
                                    <th>Jml Jamaah</th>
                                    <th>Total Biaya</th>
                                    <th>detail</th>
                                    <th>Approved</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                     </table>
                   </div>
              
                                      
                       
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/bootstrap-editable/js/bootstrap-editable.min.js"></script> -->


<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables
    table = $('#data-table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('approved_manasik/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function approved(id_manasik)
    {
        if(confirm('APAKAH ANDA approved kd transaksi ini? '))
        {
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('approved_manasik/approved')?>/"+id_manasik,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();
                    alert('Error APPROVED data');
                   // window.location.href="<?php echo site_url('no_access')?>";
                }
            });

    }
}

</script>
   
 

