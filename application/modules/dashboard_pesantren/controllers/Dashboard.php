<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    var $folder = "dashboard_pesantren";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend_affiliate/login_affiliate');
        if (!$this->general->privilege_check_affiliate(DASHBOARD_PESANTREN, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'dashboard_pesantren');
        $this->load->model('dashboard_model');
        $this->load->helper('download');
    }

    public function index() {
        $id_user = $this->session->userdata('id_user');
        $this->data['complete'] = $this->cek_profil($id_user);

        $this->data['jamaah_belum_aktif'] = $this->dashboard_model->get_jamaah_belum_aktif();
        $this->data['jamaah_aktif'] = $this->dashboard_model->get_jamaah_aktif();
        $this->data['jamaah_alumni'] = $this->dashboard_model->get_jamaah_alumni();
        $this->data['total_jamaah'] = $this->dashboard_model->get_total_jamaah();
        $this->template->load('template/template', $this->folder . '/dashboard_pesantren', $this->data);
        //   $this->load->view('template/header');
        // $this->load->view('template/menu');
        // $this->load->view('template/nav');
        // $this->load->view('dashboard/dashboard');
        // $this->load->view('template/footer');
    }
    
    
    
    function cek_profil($id_user) {
        $getdtata = $this->dashboard_model->get_data_profil($id_user);
        $result = 0;
        if (!empty($getdtata['bank']))
            $result++;
        if (!empty($getdtata['norek'])) 
            $result++;
        if (!empty($getdtata['namarek'])) 
            $result++;
        if (!empty($getdtata['telp'])) 
            $result++;
        if (!empty($getdtata['alamat'])) 
            $result++;
        if (!empty($getdtata['ktp'])) 
            $result++;

        if ($result == 6) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function downloadtutorial($filename = NULL) {
        // load download helder
        // read file contents
        $data = file_get_contents(base_url('/uploads/' . $filename));
        force_download($filename, $data);
    }

    public function download() {
        force_download('uploads/tutoril_webkonven_new.pdf', NULL);
    }
    
    public function ajax_list() {
        $id_user = $this->session->userdata('id_user');
        
        //1 SELECT jml_kyai
        $sql = "SELECT COUNT(*) jml_kyai FROM affiliate WHERE id_affiliate_type = 7 ".
               "AND sponsor = '".$id_user."'";
        $hasil = $this->db->query($sql)->result_array();

        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = '<td width="20%" >KYAI<td>';
            $row[] = '<td width="20%" >' . $item['jml_kyai'] . '<td>';          
            $row[] = '<button class="btn btn-warning" id="detail" >Detail</button>';
            
            $data[] = $row;
        }
        
        //2 SELECT jml_ustadz
        $sql = "SELECT COUNT(*) jml_ustadz FROM affiliate WHERE id_affiliate_type = 8 ".
               "AND sponsor IN (SELECT id_user FROM affiliate WHERE id_affiliate_type = 7 AND sponsor = '".$id_user."') ";
                                
        $hasil2 = $this->db->query($sql)->result_array();
        //$data = array();
        //$no = 0;
        foreach ($hasil2 as $item) {
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = '<td width="20%" >USTADZ<td>';
            $row[] = '<td width="20%" >' . $item['jml_ustadz'] . '<td>';          
            $row[] = '<button class="btn btn-warning" id="detail" >Detail</button>';
            
            $data[] = $row; 
        }
        
        //3 SELECT jml_jamaah
        $sql = "SELECT COUNT(*) jml_jamaah FROM pst_anggota WHERE parent_id IN 
                (SELECT id FROM pst_anggota WHERE parent_id IN 
                        (SELECT id FROM pst_anggota WHERE parent_id IN 
                                (SELECT id FROM pst_anggota WHERE userid = '".$id_user."')
                        ) 
                )";
        $hasil3 = $this->db->query($sql)->result_array();

        //$data = array();
        //$no = 0;
        foreach ($hasil3 as $item) {
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = '<td width="20%" >JAMAAH<td>';
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] . '<td>';          
            $row[] = '<button class="btn btn-warning" id="detail" >Detail</button>';
            
            $data[] = $row; 
        }
        
        //$this->myDebug($data);
        $output = array(
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
    }
    
    function get_kyai($id_user){
        $rows = $this->db->get_where('affiliate', array('sponsor' => $id_user))->result_array();
        $kyai = array();
        foreach($rows as $r){
            $kyai[] = $r['id_user'];
        }
        return $kyai;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */