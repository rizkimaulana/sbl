 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add PRODUCT</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>product/save">
            
            <div class="form-group">
                <label>Kode</label>
                <input class="form-control" name="kode" required>
            </div>
            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" name="nama" required>
            </div>
        <!--      <div class="form-group">
                <label>Harga</label>
                <input class="form-control" name="harga" required>
            </div> -->
<!--             <div class="form-group">
                <label>Jumlah Hari</label>
                <select class="form-control" name="select_hari">
                    <option></option>
                     <?php foreach($select_hari as $sp){?>
                        <option required value="<?php echo $sp->hari;?>"><?php echo $sp->hari;?> </option>
                    <?php } ?>
                </select>
            </div> -->
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="select_status">
                    <?php foreach($select_status as $st){?>
                        <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                    <?php } ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>product" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
