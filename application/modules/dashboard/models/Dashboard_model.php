<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model{
   public function __construct() {
		parent::__construct();
	}


	function get_jamaah_belum_aktif() {
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_jamaah_aktif() {
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('status','1');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_jamaah_alumni() {
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		$this->db->where('status','2');
		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_total_jamaah() {
		$this->db->select('*');
		$this->db->from('registrasi_jamaah');
		// $this->db->where('aktif','Y');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function get()
	{

		$this->db->select('*');
		// $this->db->like('date_schedule','2016-');
		$this->db->order_by('date_schedule','asc');
		return $this->db->get('grafik_keberangkatan');

		 // $query = $this->db->get('grafik_keberangkatan');
   //  return $query->result();

	}

	public function get_grafik_jamah_all()
	{
		$this->db->select('*');
		// $this->db->like('tanggal','2013-');
		// $this->db->order_by('tanggal','asc');
		return $this->db->get('grafik_keberangkatan');
	}
    
        public function get_total_manifest()
        {
            return $this->db->get('grafik_total_manifest');
        }
        
        public function get_total_manifest_periode()
        {
            return $this->db->get('grafik_total_manifest_period');
        }
        
        public function get_total_manifest_appr()
        {
            return $this->db->get('grafik_manifest_appr');
        }
        
        public function grafik_manifest_appr_period()
        {
            return $this->db->get('grafik_manifest_appr_period');
        }
        
        public function get_manifest_bydate()
        {
            return $this->db->get('group_concat_progress_manifest');
        }
        
        public function get_data_manifest()
        {
            return $this->db->get('grafik_progress_manifest_period');
        }
        
        
        public function get_jumlah_jamaah()
        {
            return $this->db->get('v_group_concat_total_jamaah_all');
        }
        
        public function get_total_manifest_all()
        {
            return $this->db->get('v_group_concat_total_manifest');
        }
        
        public function get_total_manifest_approved()
        {
            return $this->db->get('v_group_concat_total_manifest_appr');
        }
        
}
