

<div class="row">

    <div class="col-md-3 col-sm-6">
        <div class="fd-tile detail tile-green">
            <div class="content"><h1 class="text-left">
                    <?php
                    // $sql = "SELECT count(*) as jamaah_belum_aktif 
                    //              from registrasi_jamaah where status='0'";
                    //   $query = $this->db->query($sql)->result_array();
                    //    foreach($query as $key=>$value){
                    //      $jamaah_belum_aktif = $value['jamaah_belum_aktif'];
                    //     }
                    $jamahnon_aktif = array();
                    $query = $this->db->query("SELECT count(*) as jamaah_belum_aktif  from registrasi_jamaah where status='0'");
                    if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                            $jamahnon_aktif[$row->jamaah_belum_aktif] = $row->jamaah_belum_aktif;
                        }
                    }
                    $json = json_encode($jamahnon_aktif);
                    ?>

                    <?php echo $jamahnon_aktif[$row->jamaah_belum_aktif] = $row->jamaah_belum_aktif; ?></h1><p>Jamaah Belum Aktif</p></div>
            <div class="icon"><i class="fa fa-flag"></i></div>
            <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a>
        </div>
    </div>


    <div class="col-md-3 col-sm-6">
        <div class="fd-tile detail tile-red">
            <div class="content"><h1 class="text-left">
                    <?php
// $sql = "SELECT count(*) as jamaah_aktif 
//            from registrasi_jamaah where status='1'";
// $query = $this->db->query($sql)->result_array();
//  foreach($query as $key=>$value){
//    $jamaah_aktif = $value['jamaah_aktif'];
// }

                    $jamaahaktif = array();
                    $query = $this->db->query("SELECT count(*) as jamaah_aktif from registrasi_jamaah where status='1'");
                    if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                            $jamaahaktif[$row->jamaah_aktif] = $row->jamaah_aktif;
                        }
                    }
                    $json = json_encode($jamaahaktif);
                    ?>
                    <?php echo $jamaahaktif[$row->jamaah_aktif] = $row->jamaah_aktif; ?></h1><p>Jamaah Aktif</p></div>
            <!-- <div class="content"><h1 class="text-left"><?php echo $jamaah_aktif; ?></h1><p>Jamaah Aktif</p></div> -->
            <div class="icon"><i class="fa fa-comments"></i></div>
            <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a>
        </div>
    </div>



    <div class="col-md-3 col-sm-6">
        <div class="fd-tile detail tile-prusia">
            <div class="content"><h1 class="text-left">

                    <?php
// $sql = "SELECT count(*) as jamaah_alumni 
//       from registrasi_jamaah where status='0'";
//    $query = $this->db->query($sql)->result_array();
//     foreach($query as $key=>$value){
//       $jamaah_alumni = $value['jamaah_alumni'];
// }
                    $jamaahalumni = array();
                    $query = $this->db->query("SELECT count(*) as jamaah_alumni from registrasi_jamaah where status='2'");
                    if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                            $jamaahalumni[$row->jamaah_alumni] = $row->jamaah_alumni;
                        }
                    }
                    $json = json_encode($jamaahalumni);
                    ?>
                    <?php echo $jamaahalumni[$row->jamaah_alumni]; ?></h1><p>Jamaah Alumni</p></div> 
          <!-- <div class="content"><h1 class="text-left"><?php echo $jamaah_alumni; ?></h1><p>Jamaah Alumni</p></div> -->
            <div class="icon"><i class="fa fa-heart"></i></div>
            <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="fd-tile detail tile-purple">

            <div class="content"><h1 class="text-left">
                    <?php
// $sql = "SELECT count(*) as total_jamaah 
//          from registrasi_jamaah ";
//     $query = $this->db->query($sql)->result_array();
//      foreach($query as $key=>$value){
//        $total_jamaah = $value['total_jamaah'];
//  }

                    $totaljamaah = array();
                    $query = $this->db->query("SELECT count(*) as total_jamaah from registrasi_jamaah where status in('1','2')");
                    if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                            $totaljamaah[$row->total_jamaah] = $row->total_jamaah;
                        }
                    }
                    $json = json_encode($totaljamaah);
                    ?>

                    <?php echo $totaljamaah[$row->total_jamaah] = $row->total_jamaah; ?></h1><p>Total Jamaah</p></div> 
          <!-- <div class="content"><h1 class="text-left"><?php echo $total_jamaah; ?></h1><p>Total Jamaah</p></div> -->
            <div class="icon"><i class="fa fa-eye"></i></div>
            <a class="details" href="#">Details <span><i class="fa fa-arrow-circle-right pull-right"></i></span></a>
        </div>
    </div>

</div>

<!-- GRAFIK -->
<!-- -->

<div class="row">
    <div class="col-md-6">
        <div class="block-flat">
            <div class="header">                          
                <h3>Grafik Garis Jamaah Aktif Perbulan</h3>
            </div>
            <div class="content full-width">
                <div id="chart"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="block-flat">
            <div class="header">                          
                <h3>Grafik Batang Jamaah Aktif Perbulan</h3>
            </div>
            <div class="content full-width">
                <div id="chart2"></div>     
            </div>
        </div>
    </div>
</div>

<!-- END GARFIK -->

<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">                          
            </div>
            <div class="content full-width">
                <div id="chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">                          
            </div>
            <div class="content full-width">
                <div id="chart4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">                          
            </div>
            <div class="content full-width">
                <div id="chart5" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">                          
            </div>
            <div class="content full-width">
                <div id="chart7" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <!--<div class="header">                          
                <h3>Grafik Garis Jamaah Aktif Perbulan</h3>
            </div>-->
            <div class="content full-width">
                <div id="chart6"></div>
            </div>
        </div>
    </div>
</div>

<!--<div class="row">
    <div class="col-md-6">
        <div class="block-flat dark-box visitors">
            <div class="header">
                <h4 class="no-margin">Progress Manifest All</h4>
            </div>

            <div class="row">
                <div class="counters col-md-4">
                    <h1><?php echo isset($manifest['totalmanifest']) ? $manifest['totalmanifest'] : 0; ?></h1>
                    <h1><?php echo isset($manifest['totalmanifest_not_appr']) ? $manifest['totalmanifest_not_appr'] : 0; ?></h1>
                </div>							
                <div class="col-md-8">
                    <div id="ticket-chart" style="height: 140px;"></div>
                </div>							
            </div>
            <div class="row footer">
                <div class="col-md-6"><p><i class=" fa fa-square"></i> Not Approved : <?php echo isset($manifest['persen_not_appr']) ? $manifest['persen_not_appr'] : 0; ?> %</p></div>
                <div class="col-md-6"><p><i class=" return fa fa-square"></i> Approved : <?php echo isset($manifest['persen_appr']) ? $manifest['persen_appr'] : 0; ?> %</p></div>
            </div>
        </div>
    </div>
</div>-->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.flot/jquery.flot.pie.js"></script>

<script type="text/javascript">
    /*Pie Chart*/
    var data = [
        {label: "Google", data: <?php echo $manifest['persen_appr']; ?>},
        {label: "Dribbble", data: <?php echo $manifest['persen_not_appr']; ?>},
    ];

    $.plot('#ticket-chart', data, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.55,
                shadow: {
                    top: 5,
                    left: 15,
                    alpha: 0.3
                },
                stroke: {
                    color: '#333',
                    width: 0
                },
                label: {
                    show: false
                },
                highlight: {
                    opacity: 0.08
                }
            }
        },
        grid: {
            hoverable: true,
            clickable: true
        },
        colors: ["#1DD2AF", "#FFFFFF"],
        legend: {
            show: false
        }
    });


</script>


<?php
$sql = "SELECT * from group_concat_grafik_jamaah_all ";
$query = $this->db->query($sql)->result_array();
foreach ($query as $key => $value) {
    $bulan_keberangkatan = $value['bulan_keberangkatan'];
    $tahun_keberangkatan = $value['tahun_keberangkatan'];
    $bulan = $value['bulan'];
    $tunai_bisnis = (int) $value['tunai_bisnis'];
}
?>

<?php
$sql = "SELECT * from group_concat_grafik_jamaah_all ";
$query = $this->db->query($sql)->result_array();
foreach ($query as $key => $value) {


    $tunai_bisnis = $value['tunai_bisnis'];
    $tunai_exekutif = $value['tunai_exekutif'];
    $tunai_ekonomi = $value['tunai_ekonomi'];

    $hemat_bisnis = $value['hemat_bisnis'];
    $hemat_exekutif = $value['hemat_exekutif'];
    $hemat_ekonomi = $value['hemat_ekonomi'];

    $jakarta = $value['jakarta'];
    $surabaya = $value['surabaya'];
    $singapore = $value['singapore'];
    $makassar = $value['makassar'];
    $medan = $value['medan'];
    $aceh = $value['aceh'];
    $balik_papan = $value['balik_papan'];
    $solo = $value['solo'];
}
?> 

<?php
$tb = array();
$query = $this->db->query("SELECT * from group_concat_grafik_jamaah_all");
if ($query->num_rows() > 0) {
    foreach ($query->result() as $row) {
        $tb[$row->tunai_bisnis] = $row->tunai_bisnis;
        $tb[$row->tunai_exekutif] = $row->tunai_exekutif;
        $tb[$row->tunai_ekonomi] = $row->tunai_ekonomi;

        $tb[$row->hemat_bisnis] = $row->hemat_bisnis;
        $tb[$row->hemat_exekutif] = $row->hemat_exekutif;
        $tb[$row->hemat_ekonomi] = $row->hemat_ekonomi;

        $tb[$row->jakarta] = $row->jakarta;
        $tb[$row->surabaya] = $row->surabaya;
        $tb[$row->singapore] = $row->singapore;
        $tb[$row->makassar] = $row->makassar;
        $tb[$row->medan] = $row->medan;
        $tb[$row->aceh] = $row->aceh;
        $tb[$row->balik_papan] = $row->balik_papan;
        $tb[$row->solo] = $row->solo;
    }
}
$json = json_encode($tb);
?>

<script type="text/javascript">
    jQuery(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart',
                type: 'line',
            },
            title: {
                text: 'Tahun Keberangkatan <?php echo $tahun_keberangkatan; ?>',
                x: -20
            },
            subtitle: {
                text: 'JAMAAH SBL',
                x: -20
            },
            xAxis: {
                categories: [<?php echo $bulan_keberangkatan; ?>]
            },
            yAxis: {
                title: {
                    text: 'Jumlah Jamaah'
                }
            },
            series: [{
                    name: 'Total Jamaah',
                    data: <?php echo json_encode($data); ?>
                }]

        });
    });
</script>

<script type="text/javascript">
    jQuery(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart2',
                type: 'column',
            },
            title: {
                text: 'Tahun Keberangkatan <?php echo $tahun_keberangkatan; ?>',
                x: -20
            },
            subtitle: {
                text: 'JAMAAH SBL',
                x: -20
            },
            xAxis: {
                categories: [<?php echo $bulan; ?>]
            },
            yAxis: {
                title: {
                    text: 'Jumlah Jamaah'
                }
            },
            series: [{
                    name: 'Total Jamaah',
                    data: <?php echo json_encode($data); ?>,
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]

        });
    });
</script>

<script type="text/javascript">
    $(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart3',
                type: 'column',
            },
            title: {
                text: 'Grafik Product Tunai'
            },
            subtitle: {
                text: 'Jamaah SBL'
            },
            xAxis: {
                categories: [<?php echo $bulan; ?>],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Jamaah'
                }
            },

            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: 'Tunai Bisnis',
                    data: [<?php echo $tb[$row->tunai_bisnis] = $row->tunai_bisnis; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Tunai Excekutif',
                    data: [<?php echo $tb[$row->tunai_exekutif] = $row->tunai_exekutif; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Tunai Ekonomi',
                    data: [<?php echo $tb[$row->tunai_ekonomi] = $row->tunai_ekonomi; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }
                // , {
                //     name: 'Berlin',
                //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

                // }
            ]
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart4',
                type: 'column',
            },
            title: {
                text: 'Grafik Product Hemat'
            },
            subtitle: {
                text: 'Jamaah SBL'
            },
            xAxis: {
                categories: [<?php echo $bulan; ?>],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Jamaah'
                }
            },

            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: 'Hemat Bisnis',
                    data: [<?php echo $tb[$row->hemat_bisnis] = $row->hemat_bisnis; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Hemat Excekutif',
                    data: [<?php echo $tb[$row->hemat_exekutif] = $row->hemat_exekutif; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Hemat Ekonomi',
                    data: [<?php echo $tb[$row->hemat_ekonomi] = $row->hemat_ekonomi; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }
                // , {
                //     name: 'Berlin',
                //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

                // }
            ]
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart5',
                type: 'column',
            },
            title: {
                text: 'Grafik Total Jamaah PerEmbarkasi'
            },
            subtitle: {
                text: 'Jamaah SBL'
            },
            xAxis: {
                categories: [<?php echo $bulan; ?>],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Jamaah'
                }
            },

            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: 'Jakarta',
                    data: [<?php echo $tb[$row->jakarta] = $row->jakarta; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Surabaya',
                    data: [<?php echo $tb[$row->surabaya] = $row->surabaya; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Singapore',
                    data: [<?php echo $tb[$row->singapore] = $row->singapore; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Makassar',
                    data: [<?php echo $tb[$row->makassar] = $row->makassar; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Medan',
                    data: [<?php echo $tb[$row->medan] = $row->medan; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Banda Aceh',
                    data: [<?php echo $tb[$row->aceh] = $row->aceh; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Balik Papan',
                    data: [<?php echo $tb[$row->balik_papan] = $row->balik_papan; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Solo',
                    data: [<?php echo $tb[$row->solo] = $row->solo; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }
            ]
        });
    });
</script>

<script type="text/javascript">
    jQuery(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart6',
                type: 'line',
            },
            title: {
                text: 'Progress Manifest 30 Hari Terakhir',
                x: -20
            },
            subtitle: {
                text: '', //'JAMAAH SBL',
                x: -20
            },
            xAxis: {
                categories: [<?php echo $manifest['tgl']; ?>] //hari
            },
            yAxis: {
                title: {
                    text: 'Jumlah Manifest'
                }
            },
            series: [{
                    name: 'Total Manifest',
                    data: [<?php echo $manifest['jumlah']; ?>] //jml
                }]

        });
    });

</script>

<script type="text/javascript">
    $(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'chart7',
                type: 'column',
            },
            title: {
                text: 'Grafik Progress Manifest Periode '
            },
            subtitle: {
                text: <?php echo $manifest['bulan_tahun']; ?>
            },
            xAxis: {
                categories: [<?php echo $manifest['bulan']; ?>],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Progress Manifest'
                }
            },

            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            colors: ['#3F51B5', '#4CAF50', '#FFEB3B', '#f44336'],
            series: [{
                    name: 'Total Jamaah',
                    data: [<?php echo $manifest['jumlah_jamaah']; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Manifest',
                    data: [<?php echo $manifest['total_manifest']; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }, {
                    name: 'Manifest Apporoved',
                    data: [<?php echo $manifest['total_manifest_appr']; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }
                , {
                    name: 'Manifest Not Apporoved',
                    data: [<?php echo $manifest['total_manifest_not_appr']; ?>],
                    dataLabels: {
                        enabled: true,
                        // rotation: -90,
                        color: '#FF0000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '15px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }

                }
                // , {
                //     name: 'Berlin',
                //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

                // }
            ]
        });
    });
</script>