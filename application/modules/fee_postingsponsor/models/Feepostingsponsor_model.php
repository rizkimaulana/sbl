<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feepostingsponsor_model extends CI_Model{
        var $table = 'fee_posting_sponsor';
    var $column_order = array(null, 'id_booking','id_schedule','id_product','invoice','id_affiliate_type','sponsor','nama','jml_fee','Potongan_pajak','setelah_potong_pajak','jml_jamaah','create_date','status_fee_sponsor','payment_date','id_affiliate'); //set column field database for datatable orderable
    var $column_search = array('id_booking','id_schedule','id_product','invoice','id_affiliate_type','sponsor','nama','jml_fee','Potongan_pajak','setelah_potong_pajak','jml_jamaah','create_date','status_fee_sponsor','payment_date','id_affiliate'); //set column field database for datatable searchable 
    var $order = array('create_date' => 'asc'); // default order 
    private $_table="fee";
    private $_primary="id_booking";

    private $_kuota_booking="kuota_booking";
    private $_id_booking="id_booking";

    public function get_data($offset,$limit,$q=''){
    
     $id_user=  $this->session->userdata('id_user');
          $sql = " SELECT * FROM fee_posting_sponsor where sponsor='".$id_user." ' 
          ";
        
        if($q){
            
            $sql .=" AND jml_fee LIKE '%{$q}%' 
            	    	OR Potongan_pajak LIKE '%{$q}%'
                    OR setelah_potong_pajak LIKE '%{$q}%'
                    
                    ";
        }
        $sql .=" ORDER BY create_date DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   
     public function update_status_fee_posting_jamaah($id_booking){
    
      
        $arr = array(
        
           'status_fee_sponsor' => 2,
            
            
           
        );       
              
         $this->db->update($this->_table,$arr,array('id_booking'=>$id_booking));

  
    }

    private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->where('sponsor', $id_user);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
}
