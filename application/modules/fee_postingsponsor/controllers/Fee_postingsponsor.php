<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fee_postingsponsor extends CI_Controller{
	var $folder = "fee_postingsponsor";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend_affiliate/login_affiliate');
		if(!$this->general->privilege_check_affiliate(FEE_POSTING_SPONSOR_AFFILIATE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_affiliate');	
		$this->load->model('feepostingsponsor_model');
		$this->load->model('feepostingsponsor_model','r');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/fee_postingsponsor');
	  // print_r('MOHON MAAF UNTUK SEMENTARA DIKARENAKAN, SAAT INI SEDANG DALAM PROSES PENARIAKAN DATA UNTUK FEE YANG BELUM DITRANSFER SEMUA AKAN DI PROSES 1 AGUSTUS, BESOK PROSES CLAIM FEE AKAN BERJALAN SEPERTI BIASA..SEKIAN TERIMAKASI');
		
	}
	
	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	              	$row[] ='<td width="20%" >'.'INVOICE = '.$r->invoice.' <br> '.$r->id_affiliate.'</td>';
	                $row[] ='<td width="10%">Rp '.number_format($r->jml_fee).'</td>';
	             	$row[] ='<td width="20%">Rp '.number_format($r->Potongan_pajak).'</td>';
	                $row[] ='<td width="20%">Rp '.number_format($r->setelah_potong_pajak).'</td>';
	                $row[] ='<td width="10%">'.$r->jml_jamaah.'</td>';
	                			
	                // if ($r->status_fee_sponsor == 1){ 
	                // $row[] .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Claim Fee" onclick="update_status_fee_posting_jamaah('."'".$r->id_booking."'".')"> Claim Fee</a> ';
	                // }elseif($r->status_fee_sponsor == 2){ 
	                // 	$row[] .='<a title="Menunggu Konfirmasi" class="btn btn-sm btn-primary" href="#">
	                //             </i> Menunggu Konfirmasi
	                //         </a> ';
	                // }else{
	                // 	$row[] .='<a title="OK" class="btn btn-sm btn-success" href="#">
	                //             </i> OK
	                //         </a> ';
	                // }

	                  if ($r->status_fee_sponsor == 1){ 
	                $row[] .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Claim Fee" onclick="update_status_fee_posting_jamaah('."'".$r->id_booking."'".')"> Claim Fee</a> ';
	                }elseif($r->status_fee_sponsor == 2 ){ 
	                	$row[] .='<a title="Menunggu Konfirmasi" class="btn btn-sm btn-primary" href="#">
	                            </i> MENUNGGU KONFIRMASI
	                        </a> ';
	                }elseif($r->status_fee_sponsor == 3 ){ 
	                	$row[] .='<a title="ON PROSES" class="btn btn-sm btn-info" href="#">
	                            </i> ON PROSES
	                        </a> ';
	                }else{
	                	$row[] .='<a title="Telah Di Uangkan / Sudah di Transfer" class="btn btn-sm btn-success" href="#">
	                            </i>  Telah Di Uangkan / Sudah di Transfer
	                        </a> ';
	                }
	              
	              

			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->feepostingsponsor_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td width="5%">'.$i.'</td>';
	               
	                $rows .='<td width="10%">Rp '.number_format($r->jml_fee).'</td>';
	             	 $rows .='<td width="20%">Rp '.number_format($r->Potongan_pajak).'</td>';
	                $rows .='<td width="20%">Rp '.number_format($r->setelah_potong_pajak).'</td>';
	                 $rows .='<td width="10%">'.$r->jml_jamaah.'</td>';
	                $rows .='<td width="20%" align="center">';
	                if ($r->status_fee_sponsor == 1){ 
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Claim Fee" onclick="update_status_fee_posting_jamaah('."'".$r->id_booking."'".')"> Claim Fee</a> ';
	                }elseif($r->status_fee_sponsor == 2){ 
	                	$rows .='<a title="Menunggu Konfirmasi" class="btn btn-sm btn-primary" href="#">
	                            </i> Menunggu Konfirmasi
	                        </a> ';
	                }else{
	                	$rows .='<a title="OK" class="btn btn-sm btn-success" href="#">
	                            </i> OK
	                        </a> ';
	                }
	              
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'fee_postingsponsor/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	

	public function update_status_fee_posting_jamaah($id_booking)
	{
		// if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
		//     $this->general->no_access();
		$send = $this->feepostingsponsor_model->update_status_fee_posting_jamaah($id_booking);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('fee_postingsponsor');

	}

	

	
	


}
