<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Privilege_model extends CI_Model {

    public function check_privilege($id) {

        return $this->db->limit(1)->get_where('akses_user', array('jabatan_id' => $id))->num_rows();
    }

    public function get_role($modul, $id) {

        $sql = "SELECT * FROM akses_user WHERE modul = '%s' AND jabatan_id = %d ";
        $q = $this->db->query(sprintf($sql, $modul, $id));
        if ($q->num_rows() > 0)
            return $q->row_array();
        else
            return $q = array('view' => '', 'add' => '', 'edit' => '', 'remove' => '');
    }

    private function _olah_modul($data) {

        $module = $this->modul();
        $m = array();
        $n = array();
        foreach ($data['data'] as $page_id => $x) {

            $m[] = $page_id;
        }

        foreach ($module as $mdl) {

            if (!in_array($mdl['const'], $m)) {

                $n[] = $mdl['const'];
            }

            if (isset($mdl['anak'])) {

                foreach ($mdl['anak'] as $k) {

                    if (!in_array($k['const'], $m)) {
                        $n[] = $k['const'];
                    }
                }
            }
        }

        return $n;
    }

    public function save($data) {


        $arr = array();

        foreach ($data['data'] as $page_id => $page_akses) {

            if (!isset($page_akses['view']))
                $page_akses['view'] = 0;
            if (!isset($page_akses['add']))
                $page_akses['add'] = 0;
            if (!isset($page_akses['edit']))
                $page_akses['edit'] = 0;
            if (!isset($page_akses['remove']))
                $page_akses['remove'] = 0;
            if (!isset($page_akses['cetak']))
                $page_akses['cetak'] = 0;

            $tmp = array(
                'jabatan_id' => $data['jabatan_id'],
                'modul' => $page_id,
                'view' => $page_akses['view'],
                'add' => $page_akses['add'],
                'edit' => $page_akses['edit'],
                'remove' => $page_akses['remove'],
                'cetak' => $page_akses['cetak'],
            );

            $arr[] = $tmp;
        }

        /* meski tidak di centang...maka ttep sertakan array nya... */
        //take CONST where the modul is not checked (add,view etc) nya
        $n = $this->_olah_modul($data);
        $vv = array();
        foreach ($n as $o) {
            $vv[] = array(
                'jabatan_id' => $data['jabatan_id'],
                'modul' => $o,
                'view' => 0,
                'add' => 0,
                'edit' => 0,
                'remove' => 0,
                'cetak' => 0,
            );
        }

        //remove first if exist
        $this->db->trans_begin();
        $this->db->delete('akses_user', array('jabatan_id' => $data['jabatan_id']));
        $this->db->insert_batch('akses_user', array_merge($arr, $vv));
        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_commit();
            return true;
        }
    }

    /* COnfigure modul */

    public function modul() {

        $module = array(
            array(
                'const' => 'DASHBOARD',
                'induk' => true,
                'name' => 'DASHBOARD untuk halaman admin ',
            ),
            array(
                'const' => 'DASHBOARD_AFFILIATE',
                'induk' => true,
                'name' => 'DASHBOARD untuk halaman Affiliate cabang dll ',
            ),
            array(
                'const' => 'DASHBOARD_PESANTREN',
                'induk' => true,
                'name' => 'DASHBOARD untuk halaman Pesantren ',
            ),
            //HALAMAN PESANTREN
            
            array(
                'const' => 'PESANTREN_CLIENT',
                'induk' => true,
                'name' => 'Pesantren',
                'anak' => array(
                    array(
                        'const' => 'REG_KYAI',
                        'name' => 'Registrasi untuk Kyai'
                    ),
                    array(
                        'const' => 'REG_USTADZ',
                        'name' => 'Registrasi untuk Ustadz'
                    ),
                    array(
                        'const' => 'REG_ANGGOTA',
                        'name' => 'Registrasi untuk anggota'
                    ),
                    array(
                        'const' => 'LIST_ANGGOTA',
                        'name' => 'List Anggota'
                    ),
                    array(
                        'const' => 'ANGSURAN_PESANTREN',
                        'name' => 'Angsuran Anggota'
                    )
                )
            ),
            
            //BATAS HALAMAN PESANTREN
            
            // HALAMAN AFFILLIATE 
            array(
                'const' => 'JAMAAH_REGISTRATION',
                'induk' => true,
                'name' => 'Jamaah Registration Affiliate',
                'anak' => array(
                    array(
                        'const' => 'JAMAAH_REGISTRATION_FOR_AFFILIATE',
                        'name' => 'Registrasi jamaah untuk cabang dll'
                    ),
                    array(
                        'const' => 'REGISTRASI_AFFILIATE_PROMO',
                        'name' => 'Registrasi Promo Umroh'
                    ),
                    array(
                        'const' => 'FAKTUR_REGISTRASI_FOR_AFFILIATE',
                        'name' => 'Faktur Registrasi untuk cabang dll'
                    ),
                    array(
                        'const' => 'JAMAAH_KUOTA_AFFILIATE',
                        'name' => 'Registrasi Jamaah Kuota 25JT'
                    ),
                    array(
                        'const' => 'JAMAAH_KUOTA_PROMO_AFFILIATE',
                        'name' => 'Registrasi Jamaah Kuota PROMO'
                    ),
                    array(
                        'const' => 'BOOKING_SEATS_AFFILIATE',
                        'name' => 'Registrasi Booking Seats'
                    ),
                )
            ),
            array(
                'const' => 'DATA_JAMAAH_FOR_AFFILIATE',
                'induk' => true,
                'name' => 'Data Jamaah untuk cabang dll',
                'anak' => array(
                    array(
                        'const' => 'JAMAAH_NOTAKTIF_FOR_AFFILIATE',
                        'name' => 'Jamaah Belum Aktif untuk cabang dll'
                    ),
                    array(
                        'const' => 'JAMAAH_AKTIF_FOR_AFFILIATE',
                        'name' => 'Jamaah Aktif untuk cabang dll'
                    ),
                    array(
                        'const' => 'JAMAAH_ALUMNI_FOR_AFFILIATE',
                        'name' => 'Jamaah Alumni untuk cabang dll'
                    ),
                    array(
                        'const' => 'DATA_JAMAAH_NOTAKTIF_FOR_AFFILIATE',
                        'name' => 'Data Jamaah Belum Aktif untuk cabang dll'
                    ),
                    array(
                        'const' => 'DATA_JAMAAH_AKTIF_FOR_AFFILIATE',
                        'name' => 'Data Jamaah Aktif untuk cabang dll'
                    ),
                    array(
                        'const' => 'DATA_JAMAAH_ALUMNI_FOR_AFFILIATE',
                        'name' => 'Data Jamaah Alumni untuk cabang dll'
                    ),
                )
            ),
            array(
                'const' => 'FEE_FREE_HEMAT_REGISTRASI_JAMAAH_FOR_AFFILIATE',
                // 'induk'=>true,
                'name' => 'claim fee dan free registrasi untuk paket hemat untuk cabang dll ',
            ),
            array(
                'const' => 'FEE_FREE_TUNAI_REGISTRASI_JAMAAH_FOR_AFFILIATE',
                // 'induk'=>true,
                'name' => 'claim fee dan free registrasi untuk paket TUNAI untuk cabang dll ',
            ),
            array(
                'const' => 'CLAIM_FEE_INPUT',
                // 'induk'=>true,
                'name' => 'Clime fee input untuk cabang dan perwakilan ',
            ),
            array(
                'const' => 'VIEW_GAJI_CABANG',
                // 'induk'=>true,
                'name' => 'View Gaji khusus cabang ',
            ),
            array(
                'const' => 'DATA_MANIFEST_AFFILIATE',
                'induk' => true,
                'name' => 'MANIFEST Affiliate',
                'anak' => array(
                    array(
                        'const' => 'MANIFEST_AFFILIATE',
                        'name' => 'KELOLAH MANIFEST AFFILIATE'
                    ),
                    array(
                        'const' => 'INPUT_MANIFEST',
                        'name' => 'INPUT MANIFEST'
                    ),
                )
            ),
            array(
                'const' => 'MANIFEST_PERWAKILAN',
                'induk' => true,
                'name' => 'MANIFEST PERWAKILAN',
                'anak' => array(
                    array(
                        'const' => 'MANIFEST_PERWAKILAN',
                        'name' => 'MANIFEST PERWAKILAN'
                    ),
                )
            ),
            array(
                'const' => 'REFRENSI_AFFILIATE',
                // 'induk'=>true,
                'name' => 'Data Refrensi Affiliate ',
            ),
            array(
                'const' => 'CETAK_BILYETAFFILIATE',
                // 'induk'=>true,
                'name' => 'BILYET AFFILIATE',
            ),
            array(
                'const' => 'MEMBER_MGM',
                // 'induk'=>true,
                'name' => 'MEMBER GET MEMBER ',
            ),
            array(
                'const' => 'PROFIL',
                // 'induk'=>true,
                'name' => 'Profil Affiliate ',
            ),
            array(
                'const' => 'FEE_AFFILIATE',
                'induk' => true,
                'name' => 'Fee Affiliate',
                'anak' => array(
                    array(
                        'const' => 'FEE_KUOTA_AFFILIATE',
                        'name' => 'FEE Pembelian Kuota Affiliate'
                    ),
                    array(
                        'const' => 'FEE_POSTING_SPONSOR_AFFILIATE',
                        'name' => 'FEE Refrensi Posting Jamaah '
                    ),
                    array(
                        'const' => 'FEE_POSTING_JAMAAH_AFFILIATE',
                        'name' => 'FEE Posting Jamaah Affiliate '
                    ),
                )
            ),
            array(
                'const' => 'FEE_MGM',
                'induk' => true,
                'name' => 'Fee MGM',
                'anak' => array(
                    array(
                        'const' => 'FEE_POSTING_MGM',
                        'name' => 'FEE posting MGM '
                    ),
                    array(
                        'const' => 'FEE_SPONSOR_MGM',
                        'name' => 'FEE Sponsor MGM '
                    ),
                )
            ),
            array(
                'const' => 'OPERASIONAL',
                'induk' => true,
                'name' => 'Operational Handling',
                'anak' => array(
                    array(
                        'const' => 'CLAIM_BIAYA_OPERASIONAL_CABANG',
                        'name' => 'Claim Biaya Operasional Cabang'
                    ),
                    array(
                        'const' => 'CLAIM_BIAYA_OPERASIONAL_INVOICE',
                        'name' => 'Claim Biaya Operasional By Invoice'
                    ),
                    array(
                        'const' => 'LIST_CLAIM_OPERASIONAL',
                        'name' => 'List Claim Biaya Operasional'
                    ),
                    array(
                        'const' => 'APPROVAL_CLAIM_BIAYA_OPERASIONAL',
                        'name' => 'Approved Handling'
                    ),
                    array(
                        'const' => 'CLAIM_OPERASIONAL_MANASIK',
                        'name' => 'Claim Operasional Manasik'
                    ),
                    array(
                        'const' => 'CLAIM_OPERASIONAL_MANASIK_GM',
                        'name' => 'Claim Operasional Manasik GM'
                    ),
                    array(
                        'const' => 'LIST_CLAIM_MANASIK',
                        'name' => 'List Claim Manasik'
                    ),
                )
            ),
            //BATAS HALAMAN AFFILLIATE
            //
            //HALAMAN PESANTREN
            array(
                'const' => 'PESANTREN',
                'induk' => true,
                'name' => 'Pesantren',
                'anak' => array(
                    array(
                        'const' => 'REGISTRASI_FOUNDER',
                        'name' => 'Registrasi Founder'
                    ),
                    array(
                        'const' => 'REGISTRASI_KYAI',
                        'name' => 'Registrasi Kyai'
                    ),
                    array(
                        'const' => 'REGISTRASI_USTADZ',
                        'name' => 'Registrasi Ustadz'
                    ),
                    array(
                        'const' => 'REGISTRASI_ANGGOTA',
                        'name' => 'Registrasi Jamaah'
                    ),
                    array(
                        'const' => 'AKTIVASI_ANGGOTA',
                        'name' => 'Aktivasi Anggota'
                    ),
                    array(
                        'const' => 'DATA_ANGGOTA',
                        'name' => 'List Data Anggota'
                    ),
                    array(
                        'const' => 'ANGSURAN_ANGGOTA',
                        'name' => 'Angsuran Anggota'
                    ),
                    array(
                        'const' => 'KONFIRMASI_ANGSURAN_ANGGOTA',
                        'name' => 'Konfirmasi Angsuran Anggota'
                    ),
                    array(
                        'const' => 'KONFIRMASI_FEE_ANGGOTA',
                        'name' => 'Konfirmasi Fee Anggota'
                    )
                )
            ),
            //BATAS HALAMAN PESANTREN
            
            array(
                'const' => 'REGISTRATION',
                'induk' => true,
                'name' => 'Registration',
                'anak' => array(
                    array(
                        'const' => 'AFFILIATE',
                        'name' => 'Affiliate'
                    ),
                    array(
                        'const' => 'REGISTRASI_JAMAAH',
                        'name' => 'Registrasi Jamaah'
                    ),
                    array(
                        'const' => 'REGISTRASI_JAMAAH_PROMO',
                        'name' => 'Registrasi Jamaah PROMO'
                    ),
                    array(
                        'const' => 'REGISTRASI_JAMAAH_VOUCHER',
                        'name' => 'Registrasi Voucher'
                    ),
                    array(
                        'const' => 'JAMAAH_REWARD_SAHABAT',
                        'name' => 'Registrasi Reward Sahabat'
                    ),
                    array(
                        'const' => 'JAMAAH_PINDAH_PAKET',
                        'name' => 'Registrasi Pindah Paket'
                    ),
                    array(
                        'const' => 'FAKTUR_REGISTRASI',
                        'name' => 'Faktur Registrasi'
                    ),
                    array(
                        'const' => 'BOOKING_SEATS_ADMIN',
                        'name' => 'Booking seats admin'
                    ),
                    array(
                        'const' => 'JAMAAH_KUOTA_CABANG',
                        'name' => 'Registrasi Jamaah Cabang'
                    ),
                    array(
                        'const' => 'JAMAAH_KUOTA_ADMIN',
                        'name' => 'Registrasi Jamaah Kuota'
                    ),
                    array(
                        'const' => 'JAMAAH_KUOTA_PROMO_ADMIN',
                        'name' => 'Registrasi Jamaah Kuota Promo'
                    ),
                )
            ),
            array(
                'const' => 'REWARD_UMROH_MGM',
                // 'induk'=>true,
                'name' => 'REWARD 5 free 1 ',
            ),
            array(
                'const' => 'AKTIVASI',
                'induk' => true,
                'name' => 'Aktivasi',
                'anak' => array(
                    array(
                        'const' => 'AKTIVASI_JAMAAH_KUOTA',
                        'name' => 'Aktivasi jamaah Kuota'
                    ),
                    array(
                        'const' => 'AKTIVASI_MANUAL',
                        'name' => 'Aktivasi Manual'
                    ),
                    array(
                        'const' => 'AKTIVASI_PERWAKILAN',
                        'name' => 'Aktivasi Perwakilan'
                    ),
                    array(
                        'const' => 'AKTIVASI_RESCHEDULE',
                        'name' => 'Aktivasi Reschedule Jamaah'
                    ),
                    array(
                        'const' => 'EDIT_AKTIVASI_BY_INVOICE',
                        'name' => 'Edit Aktivasi Berdasarkan Invoice '
                    ),
                    array(
                        'const' => 'EDIT_AKTIVASI_BY_JAMAAH',
                        'name' => 'Edit Aktivasi Berdasarkan Jamaah '
                    ),
                    array(
                        'const' => 'AKTIVASI_BOOKING_SEATS',
                        'name' => 'AKTIVASI BOOKING SEATS '
                    ),
                    array(
                        'const' => 'AKTIVASI_PELUNSAN_BOOKING_SEATS',
                        'name' => 'AKTIVASI PELUNASAN BOOKING SEATS '
                    ),
                )
            ),
            array(
                'const' => 'DATA_JAMAAH',
                'induk' => true,
                'name' => 'Data Jamaah',
                'anak' => array(
                    array(
                        'const' => 'JAMAAH_NOTAKTIF',
                        'name' => 'Jamaah Belum Aktif'
                    ),
                    array(
                        'const' => 'JAMAAH_AKTIF',
                        'name' => 'Jamaah Aktif'
                    ),
                    array(
                        'const' => 'JAMAAH_ALUMNI',
                        'name' => 'Jamaah Alumni'
                    ),
                    array(
                        'const' => 'DATA_JAMAAH_NOTAKTIF',
                        'name' => 'Data Jamaah Belum Aktif'
                    ),
                    array(
                        'const' => 'DATA_JAMAAH_AKTIF',
                        'name' => 'Data Jamaah Aktif'
                    ),
                    array(
                        'const' => 'DATA_JAMAAH_ALUMNI',
                        'name' => 'Data Jamaah Alumni'
                    ),
                )
            ),
            array(
                'const' => 'MONITORING',
                'induk' => true,
                'name' => 'Monitoring',
                'anak' => array(
                    array(
                        'const' => 'MONITORING_KEBERANGKATAN',
                        'name' => 'Monitoring Data Jamaah'
                    ),
                )
            ),
            array(
                'const' => 'PENGAJUAN_TALANGAN_CICILAN',
                'induk' => true,
                'name' => 'Pengajuan Talnagan Cicilan',
                'anak' => array(
                    array(
                        'const' => 'INPUT_CICILAN_TALANGAN',
                        'name' => 'Input data Cicilan Talangan'
                    ),
                )
            ),
            array(
                'const' => 'BILYET',
                'induk' => true,
                'name' => 'bilyet',
                'anak' => array(
                    array(
                        'const' => 'INFO_BILYET',
                        'name' => 'info bilyet view log cetak bilyet'
                    ),
                    array(
                        'const' => 'BILYET_REWARD_SAHABAT',
                        'name' => 'cetak bilyet Reward Sahabat SBl'
                    ),
                    array(
                        'const' => 'GROUPING_BILYET_REWARD_SAHABAT',
                        'name' => 'GROUPING / data bilyet Reward Sahabat SBl'
                    ),
                    array(
                        'const' => 'INSERT_REWARD_SAHABAT',
                        'name' => 'INSERT DATA BILYET'
                    ),
                )
            ),
            array(
                'const' => 'LOGISTIK',
                'induk' => true,
                'name' => 'Logistik',
                'anak' => array(
                    array(
                        'const' => 'PENGIRIMAN_BARANG',
                        'name' => 'Pengiriman Barang'
                    ),
                )
            ),
            array(
                'const' => 'MANIFEST',
                'induk' => true,
                'name' => 'Manifest',
                'anak' => array(
                    array(
                        'const' => 'HOTEL',
                        'name' => 'Data Hotel'
                    ),
                    array(
                        'const' => 'ROOM_GROUP',
                        'name' => 'Data Room Group'
                    ),
                    array(
                        'const' => 'GROUP_KEBERANGKATAN',
                        'name' => 'Group Keberangkatan'
                    ),
                    array(
                        'const' => 'UPDATE_DATA_MANIFEST',
                        'name' => 'Update atau melengkapi data Jamaah'
                    ),
                    array(
                        'const' => 'DATA_MANIFEST',
                        'name' => 'Data Manifest'
                    ),
                    array(
                        'const' => 'MANIFEST_CABANG',
                        'name' => 'Data Manifest CABANG'
                    ),
                    array(
                        'const' => 'MANIFEST_GM',
                        'name' => 'Data Manifest GM'
                    ),
                )
            ),
            array(
                'const' => 'DATA_PRODUCT',
                'induk' => true,
                'name' => 'Data Product',
                'anak' => array(
                    array(
                        'const' => 'PRODUCT',
                        'name' => 'Product'
                    ),
                    array(
                        'const' => 'PRODUCT_PRICE',
                        'name' => 'Product Price'
                    ),
                )
            ),
            array(
                'const' => 'LAPORAN',
                'induk' => true,
                'name' => 'Laporan',
                'anak' => array(
                    array(
                        'const' => 'LAPORAN_REKAP_REGISTRASI',
                        'name' => 'Laporan Rekap Registrasi'
                    ),
                    array(
                        'const' => 'LAPORAN_DETAIL_REGISTRASI',
                        'name' => 'Laporan Detail Registrasi'
                    ),
                    array(
                        'const' => 'LAPORAN_PINDAH_PAKET',
                        'name' => 'Laporan Jamaah Pidah Paket'
                    ),
                    array(
                        'const' => 'LAPORAN_REG_GM',
                        'name' => 'Laporan Registrasi GM/MGM'
                    ),
                    array(
                        'const' => 'HISTORY_MUTASI_NAMA',
                        'name' => 'History Mutasi Nama'
                    ),
                    
                    //LAP PESANTREN
                    array(
                        'const' => 'LAP_REGISTRASI',
                        'name' => 'Lap Registrasi Anggota'
                    ),
                    
                    array(
                        'const' => 'LAP_ANGSURAN_ANGGOTA',
                        'name' => 'Lap Angsuran Anggota'
                    ),
                    
                    array(
                        'const' => 'LAP_FEE_ANGGOTA',
                        'name' => 'Lap Fee Anggota'
                    ),
                    
                    array(
                        'const' => 'JURNAL_HARIAN',
                        'name' => 'Jurnal Harian'
                    ),
                    //BATAS LAPORAN PESANTREN
                    array(
                        'const' => 'LAP_BAYAR_MANASIK',
                        'name' => 'Laporan Pembayaran Manasik'
                    ),
                    array(
                        'const' => 'LAPORAN_BIAYA',
                        'name' => 'Laporan Biaya Lain-lain'
                    )
                )
            ),
            array(
                'const' => 'FINANCE',
                'induk' => true,
                'name' => 'Finance',
                'anak' => array(
                    array(
                        'const' => 'KELOLAH_FEE_POSTING',
                        'name' => 'kelolah Fee Posting Jamaah OLD'
                    ),
                    array(
                        'const' => 'KELOLAH_FEE_POSTING_NEW',
                        'name' => 'kelolah Fee Posting Jamaah'
                    ),
                    array(
                        'const' => 'KELOLAH_FEE_POSTING_SPONSOR',
                        'name' => 'kelolah Fee Posting Sponsor'
                    ),
                    array(
                        'const' => 'KELOLAH_FEE_KUOTA_SPONSOR',
                        'name' => 'kelolah Fee Refrensi Sponsor'
                    ),
                    array(
                        'const' => 'REWARD_POSTING_MGM',
                        'name' => 'Reward posting jamaah atau Gm MGM'
                    ),
                    array(
                        'const' => 'FEE',
                        'name' => 'Fee'
                    ),
                    array(
                        'const' => 'FEE_INPUT',
                        'name' => 'Fee Input'
                    ),
                    array(
                        'const' => 'GAJI_CABANG',
                        'name' => 'Gaji Cabang'
                    ),
                    array(
                        'const' => 'BOOKING_REPORT',
                        'name' => 'Reporting Keuangan'
                    ),
                    array(
                        'const' => 'DETAIL_JAMAAH_REPORTING',
                        'name' => 'Reporting Detail Jamaah Keuangan'
                    ),
//                    array(
//                        'const' => 'OPERASIONAL',
//                        'name' => 'Operational'
//                    ),
                    array(
                        'const' => 'OPERASIONAL_HANDLING',
                        'name' => 'Approval Biaya Handling'
                    ),/*,array(
                        'const' => 'OPERASIONAL_MANASIK',
                        'name' => 'Claim Biaya Operasional Manasik'
                    ),*/
                    array(
                        'const' => 'BIAYA_OPERASIONAL_MANASIK',
                        'name' => 'Biaya Operasional Manasik'
                    ),
                    
                )
            ),
            array(
                'const' => 'MUTASI',
                'induk' => true,
                'name' => 'Mutasi',
                'anak' => array(
                    array(
                        'const' => 'MUTASI_NAMA',
                        'name' => 'Mutasi Nama'
                    ),
                    array(
                        'const' => 'MUTASI_PAKET',
                        'name' => 'Mutasi Paket'
                    ),
                )
            ),
            array(
                'const' => 'BIAYA_LAIN_LAIN',
                'induk' => true,
                'name' => 'Biaya Lain-lain',
                'anak' => array(
                    array(
                        'const' => 'BIAYA_AKOMODASI',
                        'name' => 'Biaya Akomodasi'
                    ),
                    array(
                        'const' => 'BIAYA_MUHRIM',
                        'name' => 'Biaya Muhrim'
                    ),
                    array(
                        'const' => 'LAP_AKOMODASI',
                        'name' => 'Lap. Biaya Akomodasi'
                    ),
                    array(
                        'const' => 'LAP_MUHRIM',
                        'name' => 'Lap. Biaya Muhrim'
                    ),
                )
            ),
            array(
                'const' => 'MASTER_DATA',
                'induk' => true,
                'name' => 'Master Data',
                'anak' => array(
                    array(
                        'const' => 'BANDARA',
                        'name' => 'Data Bandara'
                    ),
                    array(
                        'const' => 'CLUSTER',
                        'name' => 'Data Cluster'
                    ),
                    array(
                        'const' => 'FAMILY_RELATION',
                        'name' => 'Family Relation'
                    ),
                    array(
                        'const' => 'MUHRIM',
                        'name' => 'Indikator Muhrim'
                    ),
                    array(
                        'const' => 'REFERENSI_MUHRIM',
                        'name' => 'Refrensi Muhrim'
                    ),
                    array(
                        'const' => 'ROOM',
                        'name' => 'Room'
                    ),
                    array(
                        'const' => 'ROOM_TYPE',
                        'name' => 'Room Type'
                    ),
                    array(
                        'const' => 'ROOM_CATEGORY',
                        'name' => 'Room Category'
                    ),
                    array(
                        'const' => 'PAKET',
                        'name' => 'Pindah Paket'
                    ),
                )
            ),
            array(
                'const' => 'SETTINGS',
                'induk' => true,
                'name' => 'Setting',
                'anak' => array(
                    array(
                        'const' => 'SCHEDULE',
                        'name' => 'Schedule'
                    ),
                    array(
                        'const' => 'SETTING_DATA',
                        'name' => 'Setting Data'
                    ),
                )
            ),
            array(
                'const' => 'USER_MANAGEMENT',
                'induk' => true,
                'name' => 'User Management',
                'anak' => array(
                    array(
                        'const' => 'USER',
                        'name' => 'User'
                    ),
                    array(
                        'const' => 'JABATAN',
                        'name' => 'Jabatan'
                    ),
                )
            ),
        );

        return $module;
    }

}
