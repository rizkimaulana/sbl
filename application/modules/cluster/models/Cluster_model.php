<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cluster_model extends CI_Model {

    var $table = 'cluster';
    var $column_order = array(null, 'id_user', 'nama', 'provinsi_id', 'kabupaten_id'); //set column field database for datatable orderable
    var $column_search = array('id_user', 'nama', 'nama_affiliate', 'provinsi_nama', 'kabupaten_nama'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); 
    
    private function _get_datatables_query() {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    
    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_list_affiliate() {
        $query = $this->db->query("SELECT id_aff, id_user, nama FROM affiliate where id_affiliate_type = 1 ");
        return $query->result();
    }
    
    public function insert($data) {
        $cek_duplicate_sql = "SELECT id, id_aff, nama "
                . "FROM cluster "
                . "WHERE id_aff = {$data['id_aff']} AND kabupaten_id = '{$data['kabupaten_id']}' ";
        $cek_duplicate = $this->db->query($cek_duplicate_sql)->row_array();
        if(empty($cek_duplicate)){
            $affiliate_query = "SELECT id_user, nama, nama_affiliate from affiliate WHERE id_aff = {$data['id_aff']} ";
            $affiliate = $this->db->query($affiliate_query)->row_array();

            $provinsi_query = "SELECT nama from wilayah_provinsi WHERE provinsi_id = {$data['provinsi_id']} ";
            $provinsi = $this->db->query($provinsi_query)->row_array();

            $kabupaten_query = "SELECT nama from wilayah_kabupaten WHERE kabupaten_id = {$data['kabupaten_id']} ";
            $kabupaten = $this->db->query($kabupaten_query)->row_array();

            $arr = array(
                'id_aff' => $data['id_aff'],
                'id_user' => $affiliate['id_user'],
                'nama' => $affiliate['nama'],
                'nama_affiliate' => $affiliate['nama_affiliate'],
                'provinsi_id' => $data['provinsi_id'],
                'provinsi_nama' => $provinsi['nama'],
                'kabupaten_id' => $data['kabupaten_id'],
                'kabupaten_nama' => $kabupaten['nama'],
            );
            $this->db->trans_begin();
            $this->db->insert($this->table, $arr);
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_complete();
                return true;
            }
        }else{
            return false;
        }
    }
    
    public function update($data) {
        $affiliate_query = "SELECT id_user, nama, nama_affiliate from affiliate WHERE id_aff = {$data['id_aff']} ";
        $affiliate = $this->db->query($affiliate_query)->row_array();
        
        $provinsi_query = "SELECT nama from wilayah_provinsi WHERE provinsi_id = {$data['provinsi_id']} ";
        $provinsi = $this->db->query($provinsi_query)->row_array();
        
        $kabupaten_query = "SELECT nama from wilayah_kabupaten WHERE kabupaten_id = {$data['kabupaten_id']} ";
        $kabupaten = $this->db->query($kabupaten_query)->row_array();
        
        $arr = array(
            'id_aff' => $data['id_aff'],
            'id_user' => $affiliate['id_user'],
            'nama' => $affiliate['nama'],
            'nama_affiliate' => $affiliate['nama_affiliate'],
            'provinsi_id' => $data['provinsi_id'],
            'provinsi_nama' => $provinsi['nama'],
            'kabupaten_id' => $data['kabupaten_id'],
            'kabupaten_nama' => $kabupaten['nama'],
        );
        $this->db->trans_begin();
        $this->db->update('cluster', $arr, array('id' => $data['id_cluster']));
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_complete();
            return true;
        }
    }
    
    public function get_detail($id) {
        $sql = " SELECT * from cluster WHERE id = {$id} ";
        return $this->db->query($sql)->row_array();
    }
    
    public function delete($id){
        $this->db->trans_begin(); //transaction initialize
        $this->db->delete($this->table,array('id'=>$id));
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_complete();
            return true;
        }
    }
    
    

    

    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->opsi_setting] = $value->key_setting;
            }
            return $out;
        } else {
            return array();
        }
    }

    public function get_data($offset, $limit, $q = '') {

        $sql = " SELECT * from jamaah_aktif where 1=1
                    ";

        if ($q) {

            $sql .=" AND id_jamaah LIKE '%{$q}%'
                    OR nama LIKE '%{$q}%'
                    OR nama_affiliate LIKE '%{$q}%'

            ";
        }
        $sql .=" ORDER BY id_jamaah DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .=" LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    

    function get_keluarga() {

        $query = $this->db->get('registrasi_jamaah');
        return $query->result();
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_all_kabupaten() {
        $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        return $query->result();
    }

    function get_provinsi() {

        $query = $this->db->get('wilayah_provinsi');
        return $query->result();
    }

    function get_kabupaten() {

        $query = $this->db->get('wilayah_kabupaten');
        return $query->result();
    }

    function get_kecamatan() {

        $query = $this->db->get('wilayah_kecamatan');
        return $query->result();
    }

    

    

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
