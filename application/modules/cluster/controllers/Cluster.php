<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cluster extends CI_Controller {
    var $folder = "cluster";

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(CLUSTER, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'cluster');
        $this->load->model('cluster_model');
        $this->load->model('cluster_model', 'r');
    }

    public function index() {
        $this->template->load('template/template', $this->folder.'/cluster');
    }

    public function ajax_list() {
        $list = $this->r->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<td width="20%" >'.$r->id_user.'</td>';
            $row[] = '<td width="20%" >'.$r->nama.'</td>';
            $row[] = '<td width="18%" >'.$r->provinsi_nama.'</td>';
            $row[] = '<td width="18%" >'.$r->kabupaten_nama.'</td>';
            $row[] = '<a title="Edit Cluster" class="btn btn-sm btn-primary" href="'.base_url()
                .'cluster/cluster/edit_cluster/'.$r->id.'"><i class="fa fa-pencil"></i>Edit</a> '
                . '<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_cluster('."'".$r->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->r->count_all(),
            "recordsFiltered" => $this->r->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function add_ajax_kab($id_prov) {
        $query = $this->db->get_where('wilayah_kabupaten', array('provinsi_id' => $id_prov));
        $data = "<option value=''>- Select Kabupaten -</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
        }
        echo $data;
    }

    public function add_cluster(){
        if (!$this->general->privilege_check(MUTASI_NAMA, 'add'))
            $this->general->no_access();
        
        $data = array('provinsi'=>$this->get_all_provinsi(),
            'list_affiliate'=>$this->cluster_model->get_list_affiliate(),
            'kabupaten'=>$this->get_all_kabupaten());
        $this->template->load('template/template', $this->folder.'/add_cluster', $data);
    }
    
    public function insert(){
        $data = $this->input->post(null, true);        
        $send = $this->cluster_model->insert($data);
        if ($send){
            // $id_booking = $this->input->post('id_booking');
        }
        redirect('cluster/cluster/');
    }
    
    public function edit_cluster(){
        if (!$this->general->privilege_check(MUTASI_NAMA, 'edit'))
            $this->general->no_access();
        
        $id = $this->uri->segment(4);
        $cluster = $this->cluster_model->get_detail($id);
        $test = $this->get_kabupaten_by_provinsi($cluster['provinsi_id']);
        $data = array('provinsi'=>$this->get_all_provinsi(),
            'cluster'=>$cluster,
            'list_affiliate'=>$this->cluster_model->get_list_affiliate(),
            'list_kabupaten'=>$this->get_kabupaten_by_provinsi($cluster['provinsi_id']));
        $this->template->load('template/template', $this->folder.'/edit_cluster', $data);
    }
    
    public function update(){
        $data = $this->input->post(null, true);
        $send = $this->cluster_model->update($data);
        if ($send){
            // $id_booking = $this->input->post('id_booking');
        }
        redirect('cluster/cluster/');
    }
    
    function get_kabupaten_by_provinsi($provinsi_id) {
        $query = $this->db->query("SELECT kabupaten_id, nama FROM wilayah_kabupaten WHERE provinsi_id = {$provinsi_id} ");
        return $query->result();
    }
    
    public function ajax_delete($id)
    {
        // if(!$this->general->privilege_check(BANDARA,'remove'))
        //     $this->general->no_access();
        $send = $this->cluster_model->delete($id);
        echo json_encode(array("status" => TRUE));
        if($send)
        redirect('cluster/cluster/');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    private function _select_embarkasi() {
        return $this->db->get('embarkasi')->result();
    }

    private function _select_product() {
        $status = array('1');
        $this->db->where_in('status', $status);
        return $this->db->get('product')->result();
    }

    
    private function _family_relation() {
        return $this->db->get('family_relation')->result();
    }

    private function _select_merchandise() {
        return $this->db->get('merchandise')->result();
    }

    private function _select_kelamin() {
        $kdstatus = array('2', '3');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result();
    }

    private function _select_statuskawin() {
        $kdstatus = array('4', '5');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result();
    }

    private function _select_rentangumur() {
        return $this->db->get('rentang_umur')->result();
    }

    
    private function _select_status_hubungan() {
        $kdstatus = array('8', '9');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result();
    }

    private function _select_room_group() {
        return $this->db->get('view_concat_room_group')->result();
    }

    
    function add_ajax_kec($id_kab) {
        $query = $this->db->get_where('wilayah_kecamatan', array('kabupaten_id' => $id_kab));
        $data = "<option value=''> - Pilih Kecamatan - </option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
        }
        echo $data;
    }

    function add_ajax_des($id_kec) {
        $query = $this->db->get_where('wilayah_desa', array('kecamatan_id' => $id_kec));
        $data = "<option value=''> - Pilih Desa - </option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
        }
        echo $data;
    }

    
    
    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_all_kabupaten() {
        $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        return $query->result();
    }

    public function edit() {
        if (!$this->general->privilege_check(MUTASI_NAMA, 'edit'))
            $this->general->no_access();

        $id = $this->uri->segment(4);
        // $get = $this->db->get_where('view_mutasi',array('id_registrasi'=>$id))->row_array();
        $stored_procedure = "call detail_jamaah_aktif(?)";
        $get = $this->db->query($stored_procedure, array('id_registrasi' => $id))->row_array();
        if (!$get)
            show_404();

        $data = array('family_relation' => $this->_family_relation(),
            'select_product' => $this->_select_product(),
            'select_merchandise' => $this->_select_merchandise(),
            'select_rentangumur' => $this->_select_rentangumur(),
            'select_status_hubungan' => $this->_select_status_hubungan(),
            'select_kelamin' => $this->_select_kelamin(),
            'select_statuskawin' => $this->_select_statuskawin(),
            'provinsi' => $this->get_all_provinsi(),
            'kabupaten' => $this->get_all_kabupaten(),
            'select_embarkasi' => $this->_select_embarkasi(),
            // 'select_status_identitas'=>$this->_select_status_pic(),
            // 'select_status_kk'=>$this->_select_status_pic(),
            // 'select_photo'=>$this->_select_status_pic(),
            // 'select_pasport'=>$this->_select_status_pic(),
            // 'select_vaksin'=>$this->_select_status_pic(),
            // 'id_bandara' => $this->mutasinama_model->get_data_bandara(),
            // 'select_keluarga'=>$this->_select_keluarga(),
            // 'select_affiliate'=>$this->_select_affiliate(),
            // 'select_pemberangkatan'=>$this->_select_pemberangkatan(),
            // 'select_shedule'=>$this->_select_shedule(),
            'select_room_group' => $this->_select_room_group(),
        );



        $this->template->load('template/template', $this->folder.'/edit_mutasi_nama', array_merge($get, $data));
    }

}
