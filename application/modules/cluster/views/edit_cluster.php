<div id="page-wrapper">
    <div class="row">
        <!-- <div class="col-lg-7"> -->
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Add Data Cluster</h3>
                <form action="<?php echo base_url(); ?>cluster/cluster/update" class="form-horizontal" role="form" method="post">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Affiliate </label>
                        <div class="col-lg-5">
                            <input name="id_cluster" type="hidden" value="<?php echo $cluster['id'];?>">
                            <select name="id_aff" class="form-control" id="id_aff">
                                <option value=''>Select Affiliate</option>
                                <?php
                                    foreach ($list_affiliate as $item) {
                                        $selected = ($cluster['id_aff'] == $item->id_aff)  ? 'selected' :'';
                                        echo '<option value="' . $item->id_aff . '" '.$selected.'>' . $item->id_user . ' - ' . $item->nama . '</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Provinsi</label>
                        <div class="col-lg-5">
                            <select name="provinsi_id" class="form-control" id="provinsi" onchange="changeProv();">
                                <option>- Select Provinsi -</option>
                                <?php
                                foreach ($provinsi as $Country) {
                                    $selected = ($cluster['provinsi_id'] == $Country->provinsi_id)  ? 'selected' :'';
                                    echo '<option value="' . $Country->provinsi_id . '" '.$selected.'>' . $Country->nama . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kabupaten</label>
                        <div class="col-lg-5">
                            <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <?php
                                foreach ($list_kabupaten as $kabupaten) {
                                    $select_kab = ($cluster['kabupaten_id'] == $kabupaten->kabupaten_id)  ? 'selected' :'';
                                    echo '<option value="' . $kabupaten->kabupaten_id . '" '.$select_kab.'>' . $kabupaten->nama . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                    <a href="<?php echo base_url(); ?>cluster/cluster"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    function changeProv(){
        var e = document.getElementById("provinsi").value;
        var url = "<?php echo site_url('cluster/cluster/add_ajax_kab'); ?>/" + e;
        $('#kabupaten').load(url);
    };
</script>

