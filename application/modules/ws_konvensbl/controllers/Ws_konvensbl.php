<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Ws_konvensbl extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('Wskonvensbl_model');
    }
    
    // <editor-fold defaultstate="collapsed" desc=" WEB SERVICE SBL KONVEN ">
    
    /**
     * Mengambil data schedule berdasarkan id_schedule
     * @throws Exception
     */
    function view_fee_konven_get() {
        try {
            $awal = $this->get('awal');
            $akhir = $this->get('akhir');
            $list_fee_konven = $this->Wskonvensbl_model->view_fee_konven($awal, $akhir);
            $this->response($list_fee_konven, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($awal, 400);
        }
    }    
    function view_fee_konven_post() {
        try {
            $awal = $this->post('awal');
            $akhir = $this->post('akhir');
            $list_fee_konven = $this->Wskonvensbl_model->view_fee_konven($awal, $akhir);
            $this->response($list_fee_konven, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    function update_fee_konven_post() {
        try {
            $data = $this->post('data');
            if(empty($data)){
                throw new Exception('Data tidak ada');
            }
            $data = json_decode($data, TRUE);
            $response = $this->Wskonvensbl_model->update_fee_konven($data);
            if($response !== TRUE){
                throw new Exception($response);
            }
            $this->response($response, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    function view_reward_konven_get() {
        try {
            $awal = $this->get('awal');
            $akhir = $this->get('akhir');
            $list_reward_konven = $this->Wskonvensbl_model->view_reward_konven($awal, $akhir);
            $this->response($list_reward_konven, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }    
    function view_reward_konven_post() {
        try {
            $awal = $this->post('awal');
            $akhir = $this->post('akhir');
            $list_reward_konven = $this->Wskonvensbl_model->view_reward_konven($awal, $akhir);
            $this->response($list_reward_konven, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    function update_reward_konven_post() {
        try {
            $data = $this->post('data');
            if(empty($data)){
                throw new Exception('Data tidak ada');
            }
            $data = json_decode($data, TRUE);
            $response = $this->Wskonvensbl_model->update_reward_konven($data);
            if($response !== TRUE){
                throw new Exception($response);
            }
            $this->response($response, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    // </editor-fold>
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc=" WEB SERVICE PESANTREN ">
    
    /**
     * Menyimpan data anggota pesantren (Founder, Kyai, Ustadz) dimana sekaligus
     * insert data ke tabel affiliate
     */
    function pst_simpan_affiliate_post(){
        $data = $this->post('data');
        $response = array(
            'status' => 'ERROR',
            'konten' => 'Ada Kesalahan' 
        );
        if(!empty($data)){
            $data = json_decode($data, TRUE);
            $anggota = $data['anggota'];
            $affiliate = $data['affiliate'];
            $hasil = $this->Wskonvensbl_model->simpan_affiliate($affiliate, $anggota);
            $response['status'] = 'SUCEESS';
            $response['konten'] = 'Data Anggota Berhasil Disimpan!';
            $this->response($response, 200);
        }else{
            $this->response($response, 400);
        }
    }
    
    /**
     * Menyimpan data registrasi jamaah pesantren
     */
    function pst_registrasi_jamaah_post(){
        $anggota = $this->post('data');
        $response = array(
            'status' => 'ERROR',
            'konten' => 'Ada Kesalahan' 
        );
        if(!empty($anggota)){
            $anggota = json_decode($anggota, TRUE);
            $hasil = $this->Wskonvensbl_model->registrasi_jamaah($anggota);
            $response['status'] = 'SUCEESS';
            $response['konten'] = 'Data Affiliate Berhasil Disimpan!';
            $this->response($response, 200);
        }else{
            $response = 'data tidak ada';
            $this->response($response, 400);
        }
    }
    
    /**
     * Melakukan update data anggota pesantren
     */
    function pst_update_anggota_post(){
        $anggota = $this->post('data');
        $response = array(
            'status' => 'ERROR',
            'konten' => 'Ada Kesalahan' 
        );
        if(!empty($anggota)){
            $anggota = json_decode($anggota, TRUE);
            $hasil = $this->Wskonvensbl_model->update_anggota($anggota);
            $response['status'] = 'SUCEESS';
            $response['konten'] = 'Data Anggota Berhasil Disimpan!';
            $this->response($response, 200);
        }else{
            $response = 'data tidak ada';
            $this->response($response, 400);
        }
    }
    
    function pst_aktivasi_jamaah_post(){
        $aktivasi = $this->post('data');
        $response = array(
            'status' => 'ERROR',
            'konten' => 'Ada Kesalahan' 
        );
        if(!empty($aktivasi)){
            $aktivasi = json_decode($aktivasi, TRUE);
            $hasil = $this->Wskonvensbl_model->aktivasi_jamaah($aktivasi);
            $response['status'] = 'SUCEESS';
            $response['konten'] = 'Data Anggota Berhasil Disimpan!';
            $this->response($response, 200);
        }else{
            $response = 'data tidak ada';
            $this->response($response, 400);
        }
    }
    
    
    
    

    function embarkasi_get(){
        $id_embarkasi = $this->get('id_embarkasi');
        $this->db->select('id_embarkasi, departure');
        if(!empty($id_embarkasi)){
            $this->db->where_in('id_embarkasi', $id_embarkasi);
        }
        $list_embarkasi = $this->db->get('embarkasi')->result_array();
        $this->response($list_embarkasi, 200);
    }
    
    function embarkasi_post(){
        $id_embarkasi = $this->post('id_embarkasi');
        $this->db->select('id_embarkasi, departure');
        if(!empty($id_embarkasi)){
            $this->db->where_in('id_embarkasi', $id_embarkasi);
        }
        $list_embarkasi = $this->db->get('embarkasi')->result_array();
        $this->response($list_embarkasi, 200);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" WEB SERVICE KOPERASI ">
    
    /**
     * Mengambil data schedule berdasarkan id_schedule
     * @throws Exception
     */
    function get_schedule_get() {
        try {
            $id_schedule = $this->get('id_schedule');
            if(empty($id_schedule)){
                throw new Exception('Data id_schedule tidak ada');
            }
            $this->db->get_where('schedule', array('id_schedule' => $id_schedule))->row_array();
            $this->response($departure, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }    
    function get_schedule_post() {
        try {
            $id_schedule = $this->post('id_schedule');
            if(empty($id_schedule)){
                throw new Exception('Data id schedule tidak ada');
            }
            $response = $this->db->get_where('schedule', array('id_schedule' => $id_schedule))->row_array();
            $this->response($response, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    /**
     * Mengurangi seat schedule
     * @throws Exception
     */
    function take_schedule_post() {
        try {
            $id_schedule = $this->post('id_schedule');
            $jumlah_seat = $this->post('jumlah_seat');
            if(empty($id_schedule)){
                throw new Exception('Data id_schedule tidak ada');
            }
            if(empty($jumlah_seat)){
                throw new Exception('Data jumlah seat tidak ada');
            }
            $schedule = $this->db->get_where('schedule', array('id_schedule' => $id_schedule))->row_array();
            if(empty($schedule['seats'])){
                throw new Exception('Data schedule tidak ditemukan');
            }
            $update_schedule = array(
                'seats' => $schedule['seats'] - $jumlah_seat
            );
            $this->db->update('schedule', $update_schedule, array('id_schedule' => $id_schedule));
            $this->response($departure, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    
    function get_jamaah_booking_post() {
        try {
            $invoice = $this->post('invoice');
            if(empty($invoice)){
                throw new Exception('Data iinvoice tidak ada');
            }
            $list_jamaah = $this->Wskonvensbl_model->get_list_jamaah($invoice);
            $this->response($list_jamaah, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    function get_list_kelamin_get() {
        $list_data = $this->Wskonvensbl_model->get_list_kelamin();
        $this->response($list_data, 200);
    }
    
    function get_list_nikah_get() {
        $list_data = $this->Wskonvensbl_model->get_list_nikah();
        $this->response($list_data, 200);
    }
    
    function get_list_umur_get() {
        $list_data = $this->Wskonvensbl_model->get_list_umur();
        $this->response($list_data, 200);
    }
    
    function get_list_berangkat_get() {
        $list_data = $this->Wskonvensbl_model->get_list_berangkat();
        $this->response($list_data, 200);
    }
    
    function get_list_pemberangkatan_get() {
        $list_data = $this->Wskonvensbl_model->get_list_pemberangkatan();
        $this->response($list_data, 200);
    }
    
    function get_list_waris_get() {
        $list_data = $this->Wskonvensbl_model->get_list_waris();
        $this->response($list_data, 200);
    }
    
    function get_list_merchandise_get() {
        $list_data = $this->Wskonvensbl_model->get_list_merchandise();
        $this->response($list_data, 200);
    }
    
    function get_list_visa_get() {
        $list_data = $this->Wskonvensbl_model->get_list_visa();
        $this->response($list_data, 200);
    }
    
    function save_registrasi_jamaah_post() {
        try {
            $data = $this->post('data');
            if(empty($data)){
                throw new Exception('Data tidak ada');
            }
            $data = json_decode($data, TRUE);
            $response = $this->Wskonvensbl_model->save_registrasi_jamaah($data);
            $this->response($response, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($response, 400);
        }
    }
    
    function get_data_embarkasi_post() {
        try {
            $data = $this->post('data');
            if(empty($data)){
                throw new Exception('Data tidak ada');
            }
            $response = $this->Wskonvensbl_model->get_data_embarkasi($data);
            $this->response($response, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($data, 400);
        }
    }
    
    function get_registrasi_jamaah_post() {
        $registrasi_jamaah = array();
        try {
            $id_jamaah = $this->post('id_jamaah');
            if(empty($id_jamaah)){
                throw new Exception('Data tidak ada');
            }
            $registrasi_jamaah = $this->Wskonvensbl_model->get_registrasi_jamaah($id_jamaah);
            $this->response($registrasi_jamaah, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($registrasi_jamaah, 400);
        }
    }
    
    function get_manifest_jamaah_post() {
        $manifest = array();
        try {
            $id_jamaah = $this->post('id_jamaah');
            if(empty($id_jamaah)){
                throw new Exception('Data tidak ada');
            }
            $manifest = $this->Wskonvensbl_model->get_manifest_jamaah($id_jamaah);
            $this->response($manifest, 200);
        } catch (Exception $exc) {
            $response = $exc->getMessage();
            $this->response($manifest, 400);
        }
    }
    
    
    
    
    function departure_get() {
        $id_embarkasi = array('1','2','3','9','10','11');
        // $id_embarkasi = $this->get('id_embarkasi');
       
        if ($id_embarkasi == '') {
            $departure = $this->db->get('embarkasi')->result();
        } else {           
            $this->db->where_in('id_embarkasi', $id_embarkasi);
            $departure = $this->db->get('embarkasi')->result();
        }
        $this->response($departure, 200);
    }
    
    function search_waktu_keberangkatan_post(){
        $bulan = $this->post('bulan');
        $tahun = $this->post('tahun');
        $embarkasi = $this->post('embarkasi');
        $sql = "SELECT * from view_schedule_promo where BulanKeberangkatan='".$bulan."' and TahunKeberangkatan='".$tahun."' and embarkasi='".$embarkasi."'";
        $query = $this->db->query($sql)->result();
        $this->response($query, 200);
    }
    
    
    // </editor-fold>
    
    
    
    
    
    
    
    
    //Menampilkan data kontak
    function schedule_get() {
        // $id = $this->get('id_user');
        $TahunKeberangkatan = $this->get('TahunKeberangkatan');
        $BulanKeberangkatan = $this->get('BulanKeberangkatan');
        $departure = $this->get('departure');
        if ($TahunKeberangkatan == '' && $BulanKeberangkatan == '' && $departure == '') {
            // $schedule = $this->db->get('view_schedule_promo')->result();

            $this->response('data Yang Anda Cari Kosong', 200);
        } else {
            // $this->db->where('id_user', $id);
            // $schedule = $this->Wskonvensbl_model->get_schedule_promo();
            $this->db->where('TahunKeberangkatan', $TahunKeberangkatan);
            $this->db->where('BulanKeberangkatan', $BulanKeberangkatan);
            $this->db->where('departure', $departure);
            $schedule = $this->db->get('view_schedule_promo')->result();
            $this->response($schedule, 200);
        }
        
    }

    
}
?>