<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wskonvensbl_model extends CI_Model {

    public function get_schedule_promo($datepicker_tahun_keberangkatan, $datepicker_keberangkatan, $departure) {
        $query = "SELECT * from view_schedule_promo where  BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'";
        return $query->result();
    }

    // <editor-fold defaultstate="collapsed" desc=" WEB SERVICE SBL KONVEN ">
    function view_fee_konven($awal, $akhir){
        $this->db->select('a.*, c.nama_affiliate');
        $this->db->from('fee_konven a');
        $this->db->join('affiliate c', 'c.id_user = a.id_affiliate', 'LEFT');
        if(!empty($awal)){
            $this->db->where('DATE(create_date) >= ', $awal);
        }
        if(!empty($akhir)){
            $this->db->where('DATE(create_date) <= ', $akhir);
        }
        $this->db->order_by('create_date', 'DESC');
        return $this->db->get()->result_array();
    }
    
    function update_fee_konven($data){
        try {
            if(!is_array($data)){
                throw new Exception('Format Data Tidak Dikenali');
            }
            if(empty($data['id'])){
                throw new Exception('data id fee konven tidak ada!');
            }
            $fee_konven = array();
            if(!empty($data['invoice'])){
                $fee_konven['invoice'] = $data['invoice'];
            }
            if(!empty($data['id_booking'])){
                $fee_konven['id_booking'] = $data['id_booking'];
            }
            if(!empty($data['id_affiliate'])){
                $fee_konven['id_affiliate'] = $data['id_affiliate'];
            }
            if(!empty($data['npwp'])){
                $fee_konven['npwp'] = $data['npwp'];
            }
            if(!empty($data['pajak'])){
                $fee_konven['pajak'] = $data['pajak'];
            }
            if(!empty($data['jml_jamaah'])){
                $fee_konven['jml_jamaah'] = $data['jml_jamaah'];
            }
            if(!empty($data['total_fee'])){
                $fee_konven['total_fee'] = $data['total_fee'];
            }
            if(!empty($data['potongan_pajak'])){
                $fee_konven['potongan_pajak'] = $data['potongan_pajak'];
            }
            if(!empty($data['jumlah'])){
                $fee_konven['jumlah'] = $data['jumlah'];
            }
            if(!empty($data['keterangan'])){
                $fee_konven['keterangan'] = $data['keterangan'];
            }
            if(!empty($data['tipe_fee'])){
                $fee_konven['tipe_fee'] = $data['tipe_fee'];
            }
            if(!empty($data['ket_tipe_fee'])){
                $fee_konven['ket_tipe_fee'] = $data['ket_tipe_fee'];
            }
            if(!empty($data['status'])){
                $fee_konven['status'] = $data['status'];
            }
            if(!empty($data['update_by'])){
                $fee_konven['update_by'] = $data['update_by'];
            }
            $fee_konven['update_date'] = date('Y-m-d H:i:s');
            return $this->db->update('fee_konven', $fee_konven, array('id' => $data['id']));
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }
    
    function view_reward_konven(){
        $this->db->select('a.*, c.nama_affiliate');
        $this->db->from('reward_konven a');
        $this->db->join('affiliate c', 'c.id_user = a.id_affiliate', 'LEFT');
        if(!empty($awal)){
            $this->db->where('DATE(create_date) >= ', $awal);
        }
        if(!empty($akhir)){
            $this->db->where('DATE(create_date) <= ', $akhir);
        }
        $this->db->order_by('create_date', 'DESC');
        return $this->db->get()->result_array();
    }
    
    function update_reward_konven($data){
        try {
            if(!is_array($data)){
                throw new Exception('Format Data Tidak Dikenali');
            }
            if(empty($data['id'])){
                throw new Exception('data id reward konven tidak ada!');
            }
            $reward_konven = array();
            if(!empty($data['id_reward'])){
                $reward_konven['id_reward'] = $data['id_reward'];
            }
            if(!empty($data['kd_transaksi'])){
                $reward_konven['kd_transaksi'] = $data['kd_transaksi'];
            }
            if(!empty($data['id_affiliate'])){
                $reward_konven['id_affiliate'] = $data['id_affiliate'];
            }
            if(!empty($data['npwp'])){
                $reward_konven['npwp'] = $data['npwp'];
            }
            if(!empty($data['pajak'])){
                $reward_konven['pajak'] = $data['pajak'];
            }
            if(!empty($data['total_reward'])){
                $reward_konven['total_reward'] = $data['total_reward'];
            }
            if(!empty($data['potongan_pajak'])){
                $reward_konven['potongan_pajak'] = $data['potongan_pajak'];
            }
            if(!empty($data['jumlah'])){
                $reward_konven['jumlah'] = $data['jumlah'];
            }
            if(!empty($data['peringkat'])){
                $reward_konven['peringkat'] = $data['peringkat'];
            }
            if(!empty($data['reward'])){
                $reward_konven['reward'] = $data['reward'];
            }
            if(!empty($data['keterangan'])){
                $reward_konven['keterangan'] = $data['keterangan'];
            }
            if(!empty($data['tipe_reward'])){
                $reward_konven['tipe_reward'] = $data['tipe_reward'];
            }
            if(!empty($data['tipe_reward'])){
                $reward_konven['tipe_reward'] = $data['tipe_reward'];
            }
            if(!empty($data['ket_tipe_reward'])){
                $reward_konven['ket_tipe_reward'] = $data['ket_tipe_reward'];
            }
            if(!empty($data['status'])){
                $reward_konven['status'] = $data['status'];
            }
            if(!empty($data['update_by'])){
                $reward_konven['update_by'] = $data['update_by'];
            }
            $reward_konven['update_date'] = date('Y-m-d H:i:s');
            return $this->db->update('reward_konven', $reward_konven, array('id' => $data['id']));
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" FUNGSI UNTUK WEB SERVICE PESANTREN ">
    
    /**
     * Menyimpan data registrasi Founder, Kyai dan Ustadz di Pesantren
     * @param type $affiliate
     * @param type $anggota
     * @return type boolean
     */
    function simpan_affiliate($affiliate, $anggota) {
        try {
            //  1. Insert table AFFILIATE
            $this->db->insert('affiliate', $affiliate);
            $id_aff = $this->db->insert_id();

            //  1a. generate ID USER di table AFFILIATE
            $this->db->select('affiliate_code');
            $this->db->from('affiliate_type');
            $this->db->where('id_affiliate_type', $affiliate['id_affiliate_type']);
            $get_initial = $this->db->get()->row_array();
            $initial = !empty($get_initial['affiliate_code']) ? $get_initial['affiliate_code'] : '';
            $id_user = sprintf($initial . '%04d' . date('md'), $id_aff);
            $this->db->update('affiliate', 
                array('id_user' => $id_user, 'username'=>$id_user),
                array('id_aff' => $id_aff));

            // 2. Insert tabel PST_ANGGOTA
            $anggota['userid'] = $id_user;
            $anggota['affiliate_id_aff'] = $id_aff;
            $this->db->insert('pst_anggota', $anggota);

            // 3. Kirim SMS Notifikasi Ke Anggota
            $notlp = str_replace(' ', '', $affiliate['telp']);
            $nama = $affiliate['nama'];
            $username = $id_user;
            $pass = $affiliate['ket_upgrade'];
            
            $api_element = '/api/web/send/';
            $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=" . $notlp . "&message=" . $anggota['kelompok'] . "+" . str_replace(' ', '+', $nama) . "+telah+telah+terdaftar+di+program+pesantren+sbl+username+" . $username . "+password+" . $pass . "+login+di+office.sbl.co.id";
            $smsgatewaydata = $api_params;

            $url = $smsgatewaydata;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);

            if (!$output) {
                $output = file_get_contents($smsgatewaydata);
            }

            return TRUE;
        } catch (Exception $ex) {
            return FALSE;
        }
    }
    
    /**
     * Menyimpan data registrasi Jamaah Pesantren
     * @param type $jamaah
     * @return boolean
     */
    function registrasi_jamaah($anggota){
        try {
            // 1. insert table pst_anggota
            $this->db->insert('pst_anggota', $anggota);
            $pst_anggota_id = $this->db->insert_id();
            
            //  a. Generate USERID di tabel pst_anggota
            $this->db->select('affiliate_code');
            $this->db->from('affiliate_type');
            $this->db->where('id_affiliate_type', $anggota['id_affiliate_type']);
            $get_initial = $this->db->get()->row_array();
            $initial = !empty($get_initial['affiliate_code']) ? $get_initial['affiliate_code'] : '';
            $userid = sprintf($initial . '%04d' . date('md'), $pst_anggota_id);
            $this->db->update('pst_anggota', 
                array('userid' => $userid),
                array('id' => $pst_anggota_id));
            
            // 2. insert table pst_aktivasi
            $aktivasi = array(
                'pst_anggota_id' => $pst_anggota_id,
                'kode_transaksi' => '',
                'kode_unik' => '',
                'nominal' => '',
                'status' => '',
                'keterangan' => '',
                'cara_bayar' => '',
                'nominal_bayar' => '',
                'bank_bayar' => '',
                'bukti_bayar' => '',
                'tanggal_bayar' => '',
                'trailer' => '',
                'created_date' => '',
                'created_by' => '',
                'modified_date' => '',
                'modified_by' => '',
            );
            $this->db->insert('pst_aktivasi', $aktivasi);
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    
    /**
     * Meng-update data anggota pesantren
     * Juga mengupdate tabel affiliate untuk kelompok Founder, Kyai dan Ustadz (affiliate_id_aff)
     * @param type $anggota
     * @return boolean
     */
    function update_anggota($anggota){
        try {
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    
    function aktivasi_jamaah($aktivasi){
        try {
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    
    function konfirmasi_fee_anggota($aktivasi){
        try {
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    
    function bayar_angsuran($angsuran){
        try {
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    
    function konfirmasi_angsuran($angsuran){
        try {
            
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc=" FUNGSI UNTUK WEB SERVICE KOPERASI ">
    function get_data_embarkasi($id_embarkasi){
        $embarkasi = array();
        try {
            $embarkasi = $this->db->get_where('embarkasi', array('id_embarkasi' => $id_embarkasi))->row_array();
            return $embarkasi;
        } catch (Exception $exc) {
            return $embarkasi;
        }
    }
    
    function get_list_jamaah($invoice){
        $list_jamaah = array();
        try {
            $list_jamaah = $this->db->get_where('registrasi_jamaah', array('invoice' => $invoice))->result_array();
            return $list_jamaah;
        } catch (Exception $exc) {
            return $list_jamaah;
        }
    }
    
    function get_list_kelamin(){
        $kdstatus = array('2', '3');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result_array();
    }
    
    function get_list_nikah(){
        $kdstatus = array('4', '5');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result_array();
    }
    
    function get_list_umur(){
        return $this->db->get('rentang_umur')->result_array();
    }
    
    function get_list_berangkat(){
        $kdstatus = array('8', '9');
        $this->db->where_in('kdstatus', $kdstatus);
        return $this->db->get('status')->result_array();
    }
    
    function get_list_pemberangkatan(){
        return $this->db->get('view_refund')->result_array();
    }
    
    function get_list_waris(){
        return $this->db->get('hub_ahli_waris')->result_array();
    }
    
    function get_list_merchandise(){
        return $this->db->get('merchandise')->result_array();
    }
    
    function get_list_visa(){
        return $this->db->get('status_visa')->result_array();
    }
    
    function save_registrasi_jamaah($data){
        $response = 'gagal';
        try {
            $schedule = $this->db->get_where('schedule', array('id_schedule' => $data['id_schedule']))->row_array();
            if(empty($schedule)){
                throw new Exception('data schedule tidak diketahui');
            }
            
            // 1. Insert/Update tabel registrasi_jamaah
            $registrasi_jamaah = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $data['id_jamaah'], 'invoice' => $data['invoice']))->row_array();
            $id_jamaah = $data['id_jamaah'];
            $item_jamaah = array(
                'invoice' => $data['invoice'],
                'id_booking' => $data['id_booking_seats'],
                'id_schedule' => $data['id_schedule'],
                'id_affiliate' => $data['affiliate_id'],
                'id_jamaah' => $data['id_jamaah'],
                //'id_sahabat' => '',
                //'id_product' => '',
                'embarkasi' => $schedule['embarkasi'],
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tanggal_lahir' => $data['tgl_lahir'],
                'status_diri' => $data['select_statuskawin'],
                'kelamin' => $data['select_kelamin'],
                'rentang_umur' => $data['rentang_umur'],
                'no_identitas' => $data['no_identitas'],
                'provinsi_id' => $data['provinsi'],
                'kabupaten_id' => $data['kabupaten'],
                'kecamatan_id' => $data['kecamatan'],
                'alamat' => $data['alamat'],
                'telp' => $data['telp'],
                'telp_2' => $data['telp_2'],
                'email' => $data['email'],
                'ket_keberangkatan' => $data['select_status_hubungan'],
                'ahli_waris' => $data['waris'],
                'hub_waris' => $data['select_hub_ahliwaris'],
                'merchandise' => $data['select_merchandise'],
                'room_type' => '1',
                'category' => '1',
                'room_order' => '0',
                //'refund' => '0',
                //'muhrim' => '',
                //'akomodasi' => '',
                //'handling' => '',
                //'fee' => '',
                'visa' => $data['select_status_visa'],
                //'fee_input' => '',
                'harga_paket' => $data['harga'],
                'id_bandara' => $data['pemberangkatan'],
                'tgl_daftar' => $data['tgl_daftar'],
                'update_by' => $data['update_by'],
                //'create_by' => '',
                'update_date' => date('Y-m-d H:i:s'),
                //'create_date' => '',
                //'payment_date' => '',
                'status' => '7',
                'tipe_jamaah' => '12',  // NEXT : Make sure with tabel tipe_jamaah dengan id jamaah koperasi
                //'id_historis_mutasi_nama' => '',
                //'dp_angsuran' => '',
                //'angsuran' => '',
                //'jml_angsuran' => '',
                'kode_unik' => $data['kode_unik'],
                //'ket_edit' => '',
                //'cashback' => '',
                //'status_fee' => '',
                //'tc_bilyet' => '',
                //'id_mgm' => '',
                //'dpbooking_seats' => '',
                //'pengiriman' => '',
                //'cluster' => '',
                'status_manifest' => '0',
            );
            if(empty($registrasi_jamaah)){
                $this->db->insert('registrasi_jamaah', $item_jamaah);
                $id_registrasi = $this->db->insert_id();
                /*
                $update_registrasi_jamaah = array(
                    'id_jamaah' => $id_registrasi.date('ym')
                );
                $this->db->update('registrasi_jamaah', $update_registrasi_jamaah, array('id_registrasi' => $id_registrasi));
                $id_jamaah = $id_registrasi.date('ym');
                */
            }else{
                $id_registrasi = $registrasi_jamaah['id_registrasi'];
                $this->db->update('registrasi_jamaah', $item_jamaah, array('id_jamaah' => $id_jamaah));
            }
            
            // 2. Insert/Update tabel manifest
            $manifest = $this->db->get_where('manifest', array('id_jamaah' => $id_jamaah))->row_array();
            $item_manifest = array(
                'nama_passport' => $data['nama'],
                'id_booking' => $data['id_booking_seats'],
                'id_registrasi' => $id_registrasi, 
                'id_jamaah' => $id_jamaah,
                'id_affiliate' => $data['affiliate_id'],
                //'id_grp_keberangkatan' => '',
                'no_pasport' => $data['no_pasport'],
                'status_identitas' => $data['status_identitas'],
                'status_kk' => $data['status_kk'],
                'status_photo' => $data['status_photo'],
                'status_pasport' => $data['status_pasport'],
                'status_vaksin' => $data['status_vaksin'],
                'status_buku_nikah' => $data['status_buku_nikah'],
                //'status_akte' => $data['kode_unik'],
                'isui_date' => $data['date_issue'],
                'issue_office' => $data['issue_office'],
                //'hubkeluarga' => '',
                //'keluarga' => '',
                //'ket_keluarga' => '',
                //'pic1' => '',
                //'pic2' => '',
                //'pic3' => '',
                //'pic4' => '',
                //'pic5' => '',
                //'pic6' => '',
                //'pic7' => '',
                //'keterangan' => '',
                'update_by' => $data['update_by'],
                'create_by' => $data['update_by'],
                'update_date' => date('Y-m-d H:i:s'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => '7',
                //'admin_approved' => '',
                //'admin_cluster' => '',
                //'admin_unapproved' => '',
                //'unapproved_date' => ''
            );
            if(empty($manifest)){
                $this->db->insert('manifest', $item_manifest);
            }else{
                $this->db->update('manifest', $item_manifest, array('id_jamaah' => $id_jamaah));
            }            
            
            // 3. Insert/Update tabel booking
            $booking = $this->db->get_where('booking', array('invoice' => $data['invoice']))->row_array();
            $item_booking = array(
                'invoice' => $data['invoice'],
                'id_user_affiliate' => $data['affiliate_id'],
                'id_schedule' => $data['id_schedule'],
                //'id_product' => '',
                //'category' => '',
                //'id_productprice' => '',
                'embarkasi' => $schedule['embarkasi'],
                'tgl_daftar' => $data['tgl_daftar'],
                'create_date' => $data['tgl_daftar'],
                //'payment_date' => '',
                'schedule' => $schedule['date_schedule'],
                'update_date' => date('Y-m-d H:i:s'),
                'create_by' => $data['update_by'],
                //'ket_keberangkatan' => '',
                'status' => '7',
                'update_by' => $data['update_by'],
                'harga' => $data['harga'],
                'kode_unik' => $data['kode_unik'],
                //'status_claim_fee' => '',
                'tipe_jamaah' => '0',
                'tempjmljamaah' => '0',
                //'status_fee' => '',
                //'cashback' => '',
                //'id_grp_keberangkatan' => '',
                'id_booking_seats' => $data['id_booking_seats']
            );
            if(empty($booking)){
                $this->db->insert('booking', $item_booking);
            }else{
                $this->db->update('booking', $item_booking, array('invoice' => $data['invoice']));
            }
            
            // 4. insert table additional cost
            $response = 'Berhasil';
            return $response;
        } catch (Exception $exc) {
            return $response;
        }
    }
    
    function get_registrasi_jamaah($id_jamaah){
        return $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $id_jamaah))->row_array();
    }
    
    function get_manifest_jamaah($id_jamaah){
        return $this->db->get_where('manifest', array('id_jamaah' => $id_jamaah))->row_array();
    }
    
    // </editor-fold>
    
}
