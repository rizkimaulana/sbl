<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rewardsponsor_cash extends CI_Controller {

    var $folder = "rewardsponsor_cash";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(REWARD_POSTING_MGM, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'fee_affiliate');
        $this->load->model('rewardsponsorcash_model');
    }

    public function index() {
//        $data = array(
//            'pic' => $this->rewardsponsorcash_model->get_data_fee('order by datewin ASC')->result_array(),
//        );
        $this->template->load('template/template', $this->folder . '/rewardsponsor_cash');
        // $this->load->helper('url');
        // $this->load->view('rewardmgm_umroh');
    }

    public function ajax_list() {
        $list = $this->rewardsponsorcash_model->get_data_reward()->result();
        //$this->myDebug($this->db->last_query());
        $data = array();
        $no = 0;
        foreach ($list as $reward) {
            $no++;
            $row = array();
            //1.get nama affiliate
            $affiliate = $this->db->get_where('affiliate',array('id_user' => $reward->id_affiliate))->row_array();
            
            $row[] = $no;
            //$row[] = $reward->id_affiliate;
            $row[] = $reward->id_affiliate.'<br>'.$affiliate['nama'];
            $row[] = number_format($reward->total_reward);
            $row[] = $reward->pajak.'%<br>'.number_format($reward->potongan_pajak).'<br>'.$reward->npwp;
            $row[] = number_format($reward->jumlah);
            $btnProcess = '';
            if($reward->status==0){
                $btnProcess = 'Belum Aktif';
            }elseif($reward->status==1){
                //proses
                $btnProcess = '<button class="btn btn-sm btn-warning" onclick="update_pengajuan('.$reward->id_reward.')">APPROVED</button>';
            }elseif($reward->status==2){
                //transfer
                $btnProcess = '<a class="btn btn-sm btn-info" href="'.base_url().'rewardsponsor_cash/detail/'.$reward->id_reward.'" title="TRANSFER" > TRANSFERED </a>';
                
            }elseif($reward->status==3){
                //sudah di transfer
                $btnProcess = '<font style="color:green"><strong>Sudah di transfer</strong></font>';
            }
            $row[] = $btnProcess;
            
            $data[] = $row;
        }
        
        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->fee->count_all(),
//            "recordsFiltered" => $this->fee->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    private function _select_bank() {

        return $this->db->get('bank')->result();
    }

    public function detail() {
        
        if (!$this->general->privilege_check(REWARD_POSTING_MGM, 'edit'))
            $this->general->no_access();
        
        $id_reward = $this->uri->segment(3);
        $fee_posting = $this->rewardsponsorcash_model->get_pic_posting($id_reward);
        // $pic    = array();
        $pic_booking = array();
        if (!$fee_posting) {
            show_404();
        }

        $data = array(
            'dana_bank' => $this->_select_bank(),
            'bank_transfer' => $this->_select_bank(),
            'fee_posting' => $fee_posting
        );

        $this->template->load('template/template', $this->folder . '/detailfeeposting_admin', ($data));
    }

    function save() {


        $data = $this->input->post(null, true);


        // print_r($data);
        if ($this->rewardsponsorcash_model->save($data)) {

            // redirect('registrasi_jamaah/setting_keluarga/'.$id_booking.'');
            redirect('rewardsponsor_cash');
        }
    }

    public function update_pengajuan($id) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        $send = $this->rewardsponsorcash_model->update_pengajuan($id);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('rewardsponsor_cash');
    }

}
