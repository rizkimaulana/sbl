
<div id="page-wrapper">
     
   <form   class="form-horizontal" role="form"  >
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List ORDER JAMAAH KUOTA</b>
                </div>

                 <div class="panel-body">
                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>INVOICE`</th>
                                    <th>AFFILIATE </th>
                                    <th>ID AFFIIATE </th>
                                    <th>TGL PEMBELIAN </th>
                                    <th>JML_KUOTA </th>
                                    <th>TOTAL BAYAR </th>
                                   <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      <td><?php echo $pi['invoice']?>  </td>
                                      <td><?php echo $pi['affiliate']?> </td>
                                      <td><?php echo $pi['id_affiliate']?>  </td>
                                      
                                      <td><?php echo $pi['tgl_pembelian']?>  </td>
                                      <td><?php echo $pi['jml_booking_seats']?>  </td>
                                      <td><?php echo number_format($pi['booking_seats'])?>  </td>
                                     
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn-sm btn-primary" title='Edit' href="<?php echo base_url();?>aktivasi_bookingseats/aktivasi/<?php echo $pi['id_booking_seats']; ?>">Aktivasi</a>
                                   
                                         </div> 
                                                                                         
                                  </td> 
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
