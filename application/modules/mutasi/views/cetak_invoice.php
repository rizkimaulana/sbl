<style>
    .box {border:2px solid #0094ff;margin:auto;font-size:12px;}
    .box h3 {background:#0094ff;color:white;padding:10px;}
    .box p {color:#333;padding:5px;}
    .box {
        -moz-border-radius-topright:5px;
        -moz-border-radius-topleft:5px;
        -webkit-border-top-right-radius:5px;
        -webkit-border-top-left-radius:5px;
    }
    .table {color:#424242;font-size:12px;}
</style>
<body style="font-family: sans-serif;"  >

    <div class="" align="center">
        <br><br>
        <table class="table" width="640" cellspacing="0" cellpadding="4" >
            <tr>
                <td colspan="2" align="center"><h3>PEMBAYARAN MUTASI NAMA</h3></td>
            </tr>
            <tr>
                <td width="50%"><p>ID. Trans : <?php echo $id_historis_mutasi_nama;?> </p></td>
               
                
                <td align="right" width="50%"><p><b>Date </b> : <?php echo date('d-m-Y',strtotime($payment_date)); ?></p></td>
               
            </tr>
            
            <tr>
                <td width="50%">
                    <p>
                        <?php 
                            echo '<b>'.$id_registrasi.'</b><br>';
                            echo $nama.'<br><br>'; 
                            echo $alamat.'<br>'; 
                        ?>
                    </p>
                </td>
                <td align="right" width="50%">
                    <p>
                        
                        <?php 
                            $bank_transfer = !empty($bank_transfer) ? $bank_transfer : ' - ';
                            echo 'Invoice# : <b>'.$no_transaksi.'</b><br>';
                            echo 'Payment Method:'.$pay_method.'<br><br>'; 
                            echo 'Bank Transfer:'.$bank_transfer.'<br>'; 
                        ?>
                    </p>
                </td>
               
            </tr>
            <tr height="35" style="background-color: #CFD8DC;">
                <td width="50%" align="center" ><b>Keterangan</b></td>
                <td width="50%" align="right" ><b>Jumlah Biaya</b></td>
            </tr>
            <tr>
                <td width="50%">
                    <p>
                        <?php 
                            echo $keterangan_pembayaran.'<br>';
                          
                        ?>
                    </p>
                </td>
                <td align="right" width="50%">
                    <p>
                        
                        <?php 
                            
                            echo 'Rp. '.number_format($biaya_mutasi_nama).'<br>'; 
                            
                        ?>
                    </p>
                </td>
               
            </tr
            <tr>
                <td width="50%">
                    <p>
                        
                    </p>
                </td>
                <td align="right" width="50%">
                    <p>
                        
                        <?php 
                            
                            echo '<b>Total Bayar : Rp. '.number_format($biaya_mutasi_nama).'</b><br>'; 
                            
                        ?>
                    </p>
                </td>
               
            </tr>
        </table>
    </div>
    
</body>

