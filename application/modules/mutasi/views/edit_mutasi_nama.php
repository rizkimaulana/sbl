
 <div id="page-wrapper">
    
    
       <div class="row">
        <!-- <div class="col-lg-7"> -->
          <div class="panel panel-default">
            <div class="panel-body">
        <h3 class="page-header">Periksa Data Jamaah</h3>

       
        <form action="<?php echo base_url();?>mutasi/mutasi_nama/update" class="form-horizontal" role="form" method="post">
                <?php if($cek_double == 1) { ?>
                <div class="alert alert-warning alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <div class="icon"><i class="fa fa-warning"></i></div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Warning!</strong> Jamaah Pernah Melakukan Mutasi Nama Sebelumnya. Pastikan ini adalah mutasi nama yang terakhir kalinya!
                </div>
                <?php } ?>
                <?php if($cek_double > 1) { ?>
                <div class="alert alert-danger alert-white-alt rounded">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <div class="icon"><i class="fa fa-warning"></i></div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Warning!</strong> Jamaah sudah melakukan 2X mutasi nama. Tidak diperbolehkan melakukan mutasi nama lagi!
                </div>
                <?php } ?>
                <div class="form-group">
                    <label  class="col-sm-2 control-label" >ID Jamaah</label>
                    <div class="col-sm-10">
                       <input name="id_registrasi" type="hidden" value="<?php echo $id_registrasi;?>">
                       <input name="id_booking" id="id_booking" type="hidden" value="<?php echo $id_booking;?>">
                       <input name="status" id="status" type="hidden" value="<?php echo $status;?>">
                        <input name="id_jamaah" type="text" class="form-control" id="nama" value="<?php echo $id_jamaah;?>" readonly="treu"/>
                    </div>
                  </div>
                <div class="form-group">
                    <label  class="col-sm-2 control-label" >Nama</label>
                    <div class="col-sm-10">
                      
                        <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $nama;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="inputPassword3" >Tempat Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $tempat_lahir;?>" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Tanggal Lahir</label>
                    <div class="col-sm-2">
                    
                         <input type="text" class="form-control" name="tanggal_lahir" id="datepicker"  value="<?php echo $tanggal_lahir;?>" required/>
                
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                            id="no_identitas" name="no_identitas"  value="<?php echo $no_identitas;?>" required/>
                    </div>
                  </div>

                 <div class="form-group">
                      <label class="col-lg-2 control-label">Provinsi</label>
                      <div class="col-lg-5">
                        
                            <select name="provinsi_id" class="form-control " id="provinsi" >
                                <option>- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Kabupaten</label>
                      <div class="col-lg-5">
                        
                            <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Kecamatan</label>
                      <div class="col-lg-5">
                        <select name="kecamatan_id" class="form-control" id="kecamatan">
                                <option value=''>Select Kecamatan</option>
                            </select>
                      </div>
                    </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Alamat</label>
                    <div class="col-sm-10">
                        <input id="alamat" name="alamat" type="type" class="input_telp form-control" value="<?php echo $alamat;?>" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Telp</label>
                    <div class="col-sm-10">
                        <input id="telp" name="telp" type="type" class="input_telp form-control" value="<?php echo $telp;?>" required>
                    </div>
                  </div>

                   <div class="form-group">
                        <label class="col-sm-2 control-label"
                              for="inputPassword3" >Telp 2</label>
                        <div class="col-sm-10">
                            <input id="telp" name="telp_2" type="type" class="input_telp form-control" onKeyPress="return numbersonly(this, event)" placeholder="No. Telp Orang Serumah dll (harus karakter angka)" required>
                        </div>
                      </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Email</label>
                    <div class="col-sm-10">
                        <input id="email" name="email" type="text" class="input_telp form-control" value="<?php echo $email;?>" required>
                    </div>
                  </div>

                   <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="inputPassword3" >Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select required class="form-control" name="select_kelamin">
                        <?php foreach($select_kelamin as $st){ 
                            $selected = ($kelamin == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                     <label class=" col-sm-2 control-label" >Rentang Umur</label>
                     <div class="col-sm-10">
                    <select required class="form-control" name="select_rentangumur">
                   
                        
                         <?php foreach($select_rentangumur as $st){ 
                            $selected = ($rentang_umur == $st->id_rentang_umur)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_rentang_umur;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Berangkat dengan : </label>
                    <div class="col-lg-10">
                     <select required name="select_status_hubungan" id="select_status_hubungan" class="form-control">
                      <option>- PILIH BERANGKAT DENGAN -</option>
                      <!-- <option></option> -->
                        <?php foreach($select_status_hubungan as $st){?>
                            <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <div class="form-group">
                   <label class="col-sm-2 control-label" for="status">Status :</label>
                   <div class="col-sm-10">
                  <select required class="select_status form-control" name="select_statuskawin">
                  
                    

                      <?php foreach($select_statuskawin as $st){ 
                            $selected = ($status_diri == $st->kdstatus)  ? 'selected' :'';?>
                            <option value="<?php echo $st->kdstatus;?>" <?php echo $selected;?>><?php echo $st->keterangan;?></option>
                        <?php } ?>
                  </select>
                </div>
              </div>


             

                  
   
                       <div class="form-group">
                         <label class="col-sm-2 control-label" for ="select_merchandise">Merchandise </label>
                         <div class="col-sm-10">
                        <select required class="select_merchandise form-control" name="select_merchandise">
                         
                        <?php foreach($select_merchandise as $st){ 
                            $selected = ($merchandise == $st->id_merchandise)  ? 'selected' :'';?>
                            <option value="<?php echo $st->id_merchandise;?>" <?php echo $selected;?>><?php echo $st->merchandise;?></option>
                        <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keluarga" >No Pasport</label>
                        <div class="col-sm-10">
                          <input name="no_pasport" type="text" id="no_pasport"class="input_keluarga form-control" value="<?php echo $no_pasport;?>">
                        </div>
                    </div>
                     

                 
                  <?php 
                          $biaya_setting_ = $this->mutasinama_model->get_key_val();
                            foreach ($biaya_setting_ as $key => $value){
                              $out[$key] = $value;
                            }
                        ?>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">Biaya Mutasi Nama</label>
                           <div class="col-sm-10">
                            <div class="input-group">
                              <div class="input-group-addon">Rp</div>
                              <input type="text" class="form-control"  name="biaya_mutasi" id="biaya_mutasi" value="<?php echo $out['BIAYA_MUTASI_NAMA'];?>"  readonly="readonly">
                              <div class="input-group-addon">.00</div>
                            </div>
                          </div>
                        </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ket_pembayaran" >Keterangan Pembayaran</label>
                        <div class="col-sm-10">
                            <textarea rows="2" class="form-control" name="ket_pembayaran" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Metode Pembayaran</label>
                        <div class="col-lg-5">
                            <select required class="form-control" name="select_pay_method" id='select_pay_method'>
                                <?php foreach($select_pay_method as $sk){?>
                                    <option value="<?php echo $sk->pay_method;?>"><?php echo $sk->pay_method;?></option>
                                <?php } ?> 
                            </select>
                        </div>
                    </div>
                    
                    <div id="hidden_div" style="display: none;">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Bukti Pembayaran</label>
                            <div class="col-lg-5">
                                <input type="file" name="pic[]"  id="pic1" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Bank Transfer</label>
                            <div class="col-lg-5">
                                <select class="form-control" name="select_bank" id="select_bank">
                                    <option value="">- PILIH BANK TRANSFER -</option>
                                    <?php foreach($select_bank as $sk){?>
                                        <option value="<?php echo $sk->nama;?>"><?php echo $sk->nama;?></option>
                                    <?php } ?> 
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Payment Date</label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control " name="payment_date" id="datepicker2"  placeholder="yyyy-mm-dd " required/>
                        </div>
                    </div>
                    <?php if($cek_double < 2) { ?>
                        
                    <?php } ?>
                           
           
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            <a href="<?php echo base_url();?>mutasi/mutasi_nama" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
            <!--<a href="javascript:window.print();" class="btn btn-success"><i class="fa fa-print"></i> Print</a>-->
        </form>

         </div>
    </div>
    </div>
       </div>


<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('mutasi/mutasi_nama/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('mutasi/mutasi_nama/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('mutasi/mutasi_nama/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });


</script>
<script type="text/javascript">
	$(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
		    format: 'YYYY-MM-DD',
		});
    });
</script>
<script type="text/javascript">
    document.getElementById('select_pay_method').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        // document.getElementById('hidden_div2').style.display = style;
        // $("#datepicker").prop('required',true);
        select_pay_method = $(this).val();

        console.log(select_pay_method);

        if(select_pay_method=='TRANSFER' ){
           $("#select_bank").prop('required',true);
          $("#pic1").prop('required',true);

        }else{
          $("#select_bank").prop('required',false);
           $("#pic1").prop('required',false);

        }
    });
</script>
