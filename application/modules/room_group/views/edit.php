 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-9">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Room Category</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>room_group/update">
        	<input type="hidden" name="id_room_group" value="<?php echo $id_room_group;?>">

            <div class="form-group">
                <label>Room Group Name</label>
                <input class="form-control" name="room_group_name" value="<?php echo $room_group_name;?>" required>
            </div>

            <div class="form-group">
            
                <label>Hotel Name</label>
                <select class="form-control" name="select_hotel">
                       <?php foreach($select_hotel as $st){ 
                    
                        $selected = ($id_hotel == $st->id_hotel)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $st->id_hotel;?>" <?php echo $selected;?>><?php echo $st->nama_hotel;?></option>
                    <?php } ?>
                </select>
            </div>

           <div class="form-group">
           	
                <label>Room Type</label>
                <select class="form-control" name="select_type">
                       <?php foreach($select_type as $st){ 
                    
                        $selected = ($id_room_type == $st->id_room_type)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $st->id_room_type;?>" <?php echo $selected;?>><?php echo $st->type;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Room Type</label>
                <select class="form-control" name="select_category">
                    <?php foreach($select_category as $st){ 
                    
                        $selected = ($id_room_category == $st->id_room_category)  ? 'selected' :'';
                    ?>
                        
                        <option value="<?php echo $st->id_room_category;?>" <?php echo $selected;?>><?php echo $st->category;?></option>
                    <?php } ?>
                </select>
            </div>
          
             <div class="form-group">
                <label>Kapasitas </label>
                <input class="form-control" name="kapasitas" value="<?php echo $kapasitas;?>" required>
            </div>
             <div class="form-group">
                <label>Keterangan</label>
                    <input class="form-control" name="keterangan" value="<?php echo $keterangan;?>"required>
                 
                </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>room_group" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
