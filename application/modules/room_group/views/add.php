 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-9">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Room Category</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>room_group/save">
            <div class="form-group">
                <label>Room Group Name</label>
                <input class="form-control" name="room_group_name" required>
            </div>
             <div class="form-group">
                <label>Hotel</label>
                <select required class="form-control" name="select_hotel">
                    <option value="">-- Pilih Hotel --</option>
                    <?php foreach($select_hotel as $st){?>
                        <option value="<?php echo $st->id_hotel;?>"><?php echo $st->nama_hotel;?></option>
                    <?php } ?>
                </select>
            </div>
           <div class="form-group">
                <label>Room Type</label>
                <select required class="form-control" name="select_type">
                    <option value="">-- Pilih Room Type --</option>
                    <?php foreach($select_type as $st){?>
                        <option value="<?php echo $st->id_room_type;?>"><?php echo $st->type;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Room Category</label>
                <select required class="form-control" name="select_category">
                    <option value="">-- Room Category --</option>
                    <?php foreach($select_category as $st){?>
                        <option value="<?php echo $st->id_room_category;?>"><?php echo $st->category;?></option>
                    <?php } ?>
                </select>
            </div>
           
             <div class="form-group">
                <label>Kapasitas </label>
                <input class="form-control" name="kapasitas" required>
            </div>
             <div class="form-group">
                <label>Keterangan</label>
                
                  <textarea class="form-control" name="keterangan" ></textarea>
                
              </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>room_group" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
