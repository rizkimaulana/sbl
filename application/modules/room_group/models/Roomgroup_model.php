<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roomgroup_model extends CI_Model{
   
   
    private $_table="room_group";
    private $_primary="id_room_group";

    public function save($data){
    
        // $cek = $this->db->select('ket_hubungan')->where('ket_hubungan',$data['ket_hubungan'])->get('hubungan_muhrim')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
            'room_group_name' => $data['room_group_name'],
            'id_hotel' => $data['select_hotel'],
            'id_room_type' => $data['select_type'],
            'id_room_category' => $data['select_category'],
            'kapasitas' => $data['kapasitas'],
            'keterangan' => $data['keterangan'],
        );       
        
         return $this->db->insert('room_group',$arr);
    }


    public function update($data){
        
        $arr = array(
           'room_group_name' => $data['room_group_name'],
            'id_hotel' => $data['select_hotel'],
            'id_room_type' => $data['select_type'],
            'id_room_category' => $data['select_category'],
            'kapasitas' => $data['kapasitas'],
            'keterangan' => $data['keterangan'],
            
           
        );       
              
        return $this->db->update($this->_table,$arr,array('id_room_group'=>$data['id_room_group']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
 
         $sql = " SELECT a.*, b.nama_hotel, c.type as room_type, d.category as room_category
                    from room_group as a
                            LEFT JOIN hotel b ON b.id_hotel = a.id_hotel
                            LEFT JOIN room_type c ON c.id_room_type = a.id_room_type
                            LEFT JOIN room_category d ON d.id_room_category = a.id_room_category
                    WHERE 1=1
                    ";
        
        if($q){
         

            $sql .=" AND room_type LIKE '%{$q}%'
                    OR room_category LIKE '%{$q}%'
                    OR room_group_name LIKE '%{$q}%'
                    ";
        }
        $sql .=" ORDER BY room_group_name DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($room_group_name)
    {
        $this->db->where('room_group_name', $room_group_name);
        $this->db->delete($this->_table);
    }
    
    
}
