<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>MONITORING KEBERANGKATAN</b>
                </div>
                <div class="panel-body">
                    <form action="<?php echo base_url();?>monitoring_keberangkatan/export_to_excel" role="form" method="POST"  class="form-horizontal">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>DATA MONITORING KEBERANGKATAN </b>
                                    </div>

                                    <div class="panel-body">


                                        <!--  <div class="form-group">
                                             <label class="col-lg-2 control-label">Embarkasi</label>
                                             <div class="col-lg-5">
                                             <select required class="form-control" name="embarkasi" id="embarkasi" >
                                                 <option value="">Pilih Embarkasi</option>
                                        <?php foreach ($select_embarkasi as $se) { ?>
                                                         <option value="<?php echo $se->id_embarkasi; ?>"><?php echo $se->departure; ?></option>
                                        <?php } ?>
                                             </select>
                                           </div>
                                         </div> -->
                                        <div class="form-group">
                                            <label for="embarkasi" class="col-sm-2 control-label">Embarkasi</label>
                                            <div class="col-sm-4">
                                                <?php echo $form_embarkasi; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                                            <div class="col-lg-5">
                                                <!-- <div class='input-group date' id='datepicker_tahun_keberangkatan1'> -->
                                                <input type='text' name="tahun_keberangkatan" id="tahun_keberangkatan" class="form-control" placeholder="YYYY/2016" />


                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                                            <div class="col-lg-5">
                                                <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                                <input type='text' name="bulan_keberangkatan" id="bulan_keberangkatan" class="form-control" placeholder="MM/January" />
                                               <!--  <span class="input-group-addon">
                                                  <span class="glyphicon glyphicon-calendar"></span>
                                                </span> -->
                                                <!-- </div> -->
                                            </div>

                                        </div>



                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"> </label>
                                            <div class="col-lg-10">
                                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                                <!-- <button type="button" id="btn-reset" class="btn btn-default">Reset</button> -->
                                                <button type="submit" id="btn-excel" class="btn btn-success">Export Excel</button>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="datatable" >

                                                <thead>
                                                    <tr>
                                                        <th width="2%">No </th>
                                                        <th width="8%"><strong>TANGGAL KEBERANGKATAN</strong></th>
                                                        <th width="5%"><strong>JUMLAH JAMAAH</strong></th>
                                                        <th width="10%"><strong>PRODUCT </strong></th>
                                                        <th width="8%"><strong>EMBARKASI </strong></th>
                                                        <th width="8%"><strong>MASKAPAI </strong></th>
                                                    </tr>

                                                </thead>


                                                <tbody id='response-table'>
                                                        <!-- <tr><td colspan="12"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr> -->

                                                </tbody>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div><!--end col-->

                    </form>

                </div>

            </div>
        </div>
    </div>
</div><!--end row-->
<script type="text/javascript">
    $(function () {
        $('#datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });

    $(function () {
        $('#bulan_keberangkatan').datetimepicker({
            format: 'MMMM',
        });
    });

    $(function () {
        $('#tahun_keberangkatan').datetimepicker({
            format: 'YYYY',
        });
    });
</script>


<script type="text/javascript">

    var table;

    $(document).ready(function () {

        //datatables
        table = $('#datatable').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('monitoring_keberangkatan/ajax_list') ?>",
                "type": "POST",
                "data": function (data) {
                    data.embarkasi = $('#embarkasi').val();
                    data.tahun_keberangkatan = $('#tahun_keberangkatan').val();
                    data.bulan_keberangkatan = $('#bulan_keberangkatan').val();

                }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });

        $('#btn-filter').click(function () { //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function () { //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });

    });

</script>


