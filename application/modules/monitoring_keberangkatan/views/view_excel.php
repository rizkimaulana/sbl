<html lang="en">
    <head>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                    header("Content-type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename=monitoring_keberangkatan_".date('d-M-Y').".xls");
                    ?> 
                    <th>No</th>
                    <th>TANGGAL KEBERANGKATAN</th>
                    <th>JUMLAH JAMAAH</th>
                    <th>PRODUCT</th>
                    <th>EMBARKASI</th>
                    <th>MASKAPAI</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['date_schedule'] ?>  </td>
                        <td><?php echo $pi['jml_jamaah'] ?>  </td>
                        <td><?php echo $pi['product'] ?>  </td>
                        <td><?php echo $pi['embarkasi'] ?>  </td>
                        <td><?php echo $pi['maskapai'] ?>  </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
