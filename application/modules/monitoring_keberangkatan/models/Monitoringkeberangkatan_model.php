<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Monitoringkeberangkatan_model extends CI_Model{
   
  var $table = 'monitoring_keberangkatan';
    var $column_order = array(null, 'date_schedule','jml_jamaah','product','embarkasi','maskapai','tahun_keberangkatan','bulan_keberangkatan'); //set column field database for datatable orderable
    var $column_search = array('date_schedule','jml_jamaah','product','embarkasi','maskapai','tahun_keberangkatan','bulan_keberangkatan'); //set column field database for datatable searchable 
    var $order = array('date_schedule' => 'asc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        
        //add custom filter here
        if($this->input->post('embarkasi'))
        {
            $this->db->where('embarkasi', $this->input->post('embarkasi'));
        }
        if($this->input->post('tahun_keberangkatan'))
        {
            $this->db->like('tahun_keberangkatan', $this->input->post('tahun_keberangkatan'));
        }
        if($this->input->post('bulan_keberangkatan'))
        {
            $this->db->like('bulan_keberangkatan', $this->input->post('bulan_keberangkatan'));
        }
       

        $this->db->from($this->table);
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_list_embarkasih()
    {
        $this->db->select('embarkasi');
        $this->db->from($this->table);
        $this->db->order_by('date_schedule','asc');
        $query = $this->db->get();
        $result = $query->result();

        $embarkasis = array();
        foreach ($result as $row) 
        {
            $embarkasis[] = $row->embarkasi;
        }
        return $embarkasis;
    }

    public function get_list_for_excel($embarkasi, $tahun, $bulan){
        if (!empty($embarkasi)) {
            $this->db->where('embarkasi', $this->input->post('embarkasi'));
        }
        if (!empty($tahun)) {
            $this->db->like('tahun_keberangkatan', $this->input->post('tahun_keberangkatan'));
        }
        if (!empty($bulan)) {
            $this->db->like('bulan_keberangkatan', $this->input->post('bulan_keberangkatan'));
        }
        $this->db->from($this->table);
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
        $query = $this->db->get();
        return $query->result_array();
    }
    
}
