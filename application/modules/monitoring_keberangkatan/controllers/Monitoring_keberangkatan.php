<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Monitoring_keberangkatan extends CI_Controller {

    var $folder = "monitoring_keberangkatan";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(MONITORING_KEBERANGKATAN, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'monitoring');
        $this->load->model('monitoringkeberangkatan_model');
        $this->load->model('monitoringkeberangkatan_model', 'monitoring_keberangkatan');
    }

    public function index() {
        $embarkasis = $this->monitoring_keberangkatan->get_list_embarkasih();

        $opt = array('' => 'All Emabarkasi');
        foreach ($embarkasis as $embarkasi) {
            $opt[$embarkasi] = $embarkasi;
        }

        $get['form_embarkasi'] = form_dropdown('', $opt, '', 'id="embarkasi" name="embarkasi" class="form-control"');
        $data = array(
            'select_embarkasi' => $this->_select_embarkasi(),
        );

        $this->template->load('template/template', $this->folder . '/monitoring_keberangkatan', array_merge($get, $data));
    }

    public function ajax_list() {
        $list = $this->monitoring_keberangkatan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $monitoring_keberangkatan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $monitoring_keberangkatan->date_schedule;
            $row[] = $monitoring_keberangkatan->jml_jamaah;
            $row[] = $monitoring_keberangkatan->product;
            $row[] = $monitoring_keberangkatan->embarkasi;
            $row[] = $monitoring_keberangkatan->maskapai;
            // $row[] = $monitoring_keberangkatan->tahun_keberangkatan;
            // $row[] = $monitoring_keberangkatan->bulan_keberangkatan;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->monitoring_keberangkatan->count_all(),
            "recordsFiltered" => $this->monitoring_keberangkatan->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    private function _select_embarkasi() {

        return $this->db->get('embarkasi')->result();
    }

    function export_to_excel() {
        $post_data = $this->input->post(null, true);
        $list = $this->monitoring_keberangkatan->get_list_for_excel($post_data['embarkasi'], $post_data['tahun_keberangkatan'], $post_data['bulan_keberangkatan']);
        $data['list'] = $list;
        $this->load->view('monitoring_keberangkatan/view_excel', ($data));
    }

}
