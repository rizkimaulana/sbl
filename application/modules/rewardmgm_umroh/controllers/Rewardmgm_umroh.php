<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rewardmgm_umroh extends CI_Controller{
	var $folder = "rewardmgm_umroh";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REWARD_UMROH_MGM,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','fee_affiliate');	
		$this->load->model('rewardmgmumroh_model');
		$this->load->helper('fungsi');
		$this->load->library('terbilang');
	}
	
	public function index(){
	    	      $data = array(
			'pic' => $this->rewardmgmumroh_model->get_data_fee('order by datewin desc')->result_array(),
		);
	    $this->template->load('template/template', $this->folder.'/rewardmgm_umroh',$data);
		// $this->load->helper('url');
		// $this->load->view('rewardmgm_umroh');
	}
	

	

	
	
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	

	public function detail(){

    if(!$this->general->privilege_check(REWARD_UMROH_MGM,'edit'))
	    $this->general->no_access();
    
    $id_booking = $this->uri->segment(3);
    $id_affiliate = $this->uri->segment(4);
    $fee_posting = $this->rewardmgmumroh_model->get_pic_posting($id_booking,$id_affiliate);
    $pic    = array();
    // $pic_booking= array();
    if(!$fee_posting ){
        show_404();
    }
    else{
        
        $pic = $this->rewardmgmumroh_model->get_pic($id_booking,$id_affiliate);
       
    }    

    $data = array(
    		'dana_bank'=>$this->_select_bank(),
    		'bank_transfer'=>$this->_select_bank(),
       		 'fee_posting'=>$fee_posting,'pic'=>$pic

       		 );

    $this->template->load('template/template', $this->folder.'/detailfeeposting_admin',($data));

	}
	
	function save(){
	
	   $data = $this->input->post(null,true);
	  $send = $this->rewardmgmumroh_model->save_jamaah($data);
	  // if($send)
	  // 	$this->session->set_flashdata('info', "Successfull");
			
	  //       redirect('rewardmgm_umroh');
}

	public function update_pengajuan($id)
	{
		// if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
		//     $this->general->no_access();
		$send = $this->rewardmgmumroh_model->update_pengajuan($id);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('rewardmgm_umroh');

	}


	public function report_registrasi(){  
	    $id_booking = $this->uri->segment(3);
	    $report_registrasi = $this->rewardmgmumroh_model->get_report_registrasi($id_booking);
	    if(!$report_registrasi){
	        show_404();
	    }
	   
      
	    $data = array(

	       		 'report_registrasi'=>$report_registrasi,
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_registrasi',($data));
	    // $this->load->view('bilyet_barcode',($data));
	}

	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->rewardmgmumroh_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

     private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}

	private function _select_roomcategory(){
		//  $id_room_category = array('1');
		// $this->db->where_in('id_room_category', $id_room_category);
	    return $this->db->get('room_category')->result();
	}

	function add_ajax_kab($id_prov){
	    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
	    $data = "<option value=''>- Select Kabupaten -</option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}

	private function _select_keluarga($id_booking=''){
	// $id_booking = $this->uri->segment(4);
	// $this->db->where_in('id_booking', $id_booking);
 //    return $this->db->get('jamaah_kuota')->result();
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}


	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}

	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}

	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardmgmumroh_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardmgmumroh_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->rewardmgmumroh_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}
   private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}

	public function mutasi_data_jamaah(){
	    if(!$this->general->privilege_check(REWARD_UMROH_MGM,'view'))
		    $this->general->no_access();

		$id= $this->uri->segment(4);

	    $id_user = $this->uri->segment(3);
	    $get = $this->db->get_where('affiliate',array('id_user'=>$id_user))->row_array();
	   if(!$get)
	        show_404();
	        $get_data_affiliate = $this->rewardmgmumroh_model->get_data_affiliate($id);
	    $data = array(
	    		 'select_product'=>$this->_select_product(),
				'select_embarkasi'=>$this->_select_embarkasi(),
				'select_roomcategory'=>$this->_select_roomcategory(),
	    		'get_data_affiliate'=>$get_data_affiliate,
	    		'provinsi'=>$this->rewardmgmumroh_model->get_all_provinsi(),
		    	'kabupaten'=>$this->rewardmgmumroh_model->get_all_kabupaten(),		
		    	'select_kelamin'=>$this->_select_kelamin(),	
		    	'select_rentangumur'=>$this->_select_rentangumur(),
		    	'select_statuskawin'=>$this->_select_statuskawin(),
		    	'select_status_hubungan'=>$this->_select_status_hubungan(),
		    	'family_relation'=>$this->_family_relation(),
		    	'select_keluarga'=>$this->_select_keluarga(),
		    	'select_pemberangkatan'=>$this->_select_pemberangkatan(),
		    	'select_type'=>$this->_select_type(),
		    	'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'select_merchandise'=>$this->_select_merchandise(),
		    	'select_status_visa'=>$this->_select_status_visa(),
	    	);
		$this->template->load('template/template', $this->folder.'/mutasi_data_jamaah',array_merge($get,$data));
	   
	}
}
