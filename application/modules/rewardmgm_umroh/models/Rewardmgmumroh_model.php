<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rewardmgmumroh_model extends CI_Model{

    
    var $table = 'fee_posting_jamaah_admin_new';
    var $column_order = array(null, 'invoice','id_affiliate','affiliate','create_date'); //set column field database for datatable orderable
    var $column_search = array('invoice','id_affiliate','affiliate','create_date'); //set column field database for datatable searchable 
    var $order = array('invoice' => 'asc'); // default order 

    private $_table="reward_dp";
    private $_primary="id";

    private $_booking="booking";
    private $_id_booking="id_booking";

 private $_registrasi_jamaah="registrasi_jamaah";
    private $_id_registrasi="id_registrasi";
    
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 public function generate_kode($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today;  
    }

    public function generate_kode_jamaah($idx){
        $today=date('md');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $today.$idx; 
    }

        public function generate_id_jamaah($idx){
        $today=date('ym');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }
 public function save_jamaah($data){

     
      $id_schedule = $this->input->post('radio');
      $schedule = $this->input->post('schedule');
      
   
         $sql = "SELECT *
         from view_schedule where id_schedule = $id_schedule";
        $query = $this->db->query($sql)->result_array();
      
      foreach($query as $key=>$value){
        $id_schedule= $value['id_schedule'];
        $id_product= $value['id_product'];
        $id_room_type = $value['id_room_type'];
        $room_category = $value['room_category'];
        $embarkasi = $value['embarkasi'];
        $harga = $value['harga'];
        $id_productprice = $value['id_productprice'];
        $seats = $value['seats'];
        $schedule = $value['date_schedule'];
      }
      $initial = "RDP";
        $arr = array(
                                          
            'id_user_affiliate' => $data['id_affiliate'],
            'id_product' => $id_product,
            'id_schedule' => $id_schedule,
            'id_productprice' => $id_productprice,
            'category' => $room_category,
            'embarkasi' => $embarkasi,
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'schedule' => $schedule,
            'harga' => 0,
            'create_date' => date('Y-m-d H:i:s'),
            // 'status_claim_fee'=>$data['select_status_fee'],
            'create_by'=>$this->session->userdata('id_user'),
            'tempjmljamaah'=>1,
            'status_fee'=>0,
            'tipe_jamaah'=>7,
            'status'=>1
        );       
        
         $this->db->trans_begin(); 
        
        $this->db->insert($this->_booking,$arr);
        $id_booking =  $this->db->insert_id(); 
        
        
        $this->db->update($this->_booking,
                    array('invoice'=> $this->generate_kode($initial.$id_booking)),
                    array('id_booking'=>$id_booking));

          $arr = array(
                'id_booking'=> $id_booking,
                'invoice'=>$this->generate_kode($initial.$id_booking),
                'id_schedule'=> $id_schedule,
                'id_affiliate'=> $data['id_affiliate'],
                'id_product'=> $id_product,
                'embarkasi'=> $embarkasi,
                'tgl_daftar'=>date('Y-m-d H:i:s'),
                'nama'=> $data['nama'],
                'tempat_lahir'=> $data['tempat_lahir'],
                'tanggal_lahir'=> $data['tanggal_lahir'],
                'status_diri'=> $data['select_statuskawin'],
                'kelamin'=> $data['select_kelamin'],
                'rentang_umur'=> $data['select_rentangumur'],
                'no_identitas'=> $data['no_identitas'],
                'provinsi_id'=> $data['provinsi_id'],
                'kabupaten_id'=> $data['kabupaten_id'],
                'kecamatan_id'=> $data['kecamatan_id'],
                'alamat'=> $data['alamat'],
                'telp'=> $data['telp'],
                'email'=> $data['email'],
                'ket_keberangkatan'=>$data['select_status_hubungan'],
                'ahli_waris'=> $data['waris'],
                'hub_waris'=> $data['select_hub_ahliwaris'],
                'room_type'=> 1,
                'category'=> $room_category,
                'merchandise'=> $data['select_merchandise'],
                'id_bandara'=> $data['id_bandara'],
                'refund'=> 0,
                'handling'=> 1000000,
                'akomodasi'=> 0,
                'fee'=> 0,
                'fee_input'=> 0,
                'harga_paket'=> 0,
                'create_by'=>$this->session->userdata('id_user'),
                'create_date'=>date('Y-m-d H:i:s'),
                'tipe_jamaah'=>7,
                'kode_unik'=>$data['unique_digit'],
                'status_fee'=>0,
                'status'=>0

        );       

        
        $this->db->insert($this->_registrasi_jamaah,$arr);
        $id_registrasi =  $this->db->insert_id(); 

        $this->db->update($this->_registrasi_jamaah,
                    array('id_jamaah'=> $this->generate_id_jamaah($initial.$id_registrasi)),
                    array('id_registrasi'=>$id_registrasi));

        
        
        $arr = array(
          'id_booking'=> $id_booking,
          'id_registrasi'=> $id_registrasi,
          'id_jamaah'=> $this->generate_id_jamaah($initial.$id_registrasi),
          'id_affiliate'=> $data['id_affiliate'],
          'no_pasport'=> $data['no_pasport'],
          'issue_office'=> $data['issue_office'],
          'isui_date'=> $data['isue_date'],
          'status_identitas'=> $data['status_identitas'],
          'status_kk'=> $data['status_kk'],
          'status_photo'=> $data['status_photo'],
          'status_pasport'=> $data['status_pasport'],
          'status_vaksin'=> $data['status_vaksin'],
          'hubkeluarga'=> $data['select_status_hubungan'],
          'create_by'=>$this->session->userdata('id_user'),
          'create_date'=>date('Y-m-d H:i:s'),
          'status'=> 0,
      );       
      $this->db->insert('manifest',$arr);
   

         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $id_booking,
            
            'status'=>0
        );       
        $this->db->insert('pengiriman',$arr);

       

       $arr = array(
          'id_registrasi' =>$id_registrasi,
          'id_booking'=> $id_booking,
          'status_visa' =>$data['select_status_visa'],
          'tgl_visa' =>$data['tanggal_visa'],
          'tgl_daftar' =>date('Y-m-d H:i:s'),
          'status' =>0
          );
        $this->db->insert('visa',$arr);

        $sql = "call hitung_muhrim(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();

          foreach($query as $key=>$value){
            // $id_registrasi = $value['id_registrasi'];
            $biaya_muhrim = $value['muhrim'];
          }

        $arr = array(
          'muhrim'=>$biaya_muhrim,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi)); 

        $arr = array(
          'id_jamaah_mutasi'=> $this->generate_id_jamaah($initial.$id_registrasi),
          'st'=>3,
         
        );    
          $this->db->update('reward_dp',$arr,array('id'=>$data['id_reward'])); 

          $sql = "call v_visa(?) ";
        $query = $this->db->query($sql,array('id_registrasi'=>$id_registrasi))->result_array();

          foreach($query as $key=>$value){
            $biaya_visa = $value['Total_VISA'];
          }

        $arr = array(
          'visa'=>$biaya_visa,
         
        );    
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$id_registrasi));   

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            redirect('rewardmgm_umroh/report_registrasi/'.$id_booking.'');
            return true;
        }
    }
   
   
    
      public function get_pic($id_booking,$id_affiliate=''){
      
        $sql ="SELECT * from detail_fee_posting_jamaah_admin WHERE  id_booking  = ? and id_affiliate = ?
              ";
        return $this->db->query($sql,array($id_booking,$id_affiliate))->result_array();    

    }

   

    public function get_pic_posting($id_booking,$id_affiliate=''){
 

         $sql = "SELECT * from fee_posting_jamaah_admin_new where id_booking = '$id_booking' and id_affiliate = '$id_affiliate'";
        return $this->db->query($sql)->row_array();  
    }


    public function update_pengajuan($id){

        $arr = array(
        
           'st' => '2',
 
        );       
         $this->db->update($this->_table,$arr,array('id'=>$id));

  
    }


   private function _get_datatables_query()
    {
        
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_data_fee($where= "") {
    // $data = $this->db->query('select * from detail_jamaahnoaktif where id_affiliate="'.$id_user.'" '.$where);
        $data = $this->db->query('SELECT a.*, b.nama from reward_dp as a , affiliate as b where a.id_user = b.id_user and peringkat="1" and st in("2","1")'.$where);
        return $data;
}

    function get_data_bandara($pemberangkatan='') {
      $this->db->where("pemberangkatan",$pemberangkatan);
      return $this->db->get("view_refund");
    }

    function searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan){
     
        
      
          $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'"; 
            $query = $this->db->query($sql);
    
    if (empty($query->result())){
        
       echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
           exit;
       
       }else{

        
        foreach ($query->result() as $row){    
       

            echo'<tr>
                   
                   <td><strong>'.$row->paket.'</strong></td>
                   <td><strong>'.$row->departure.'</strong></td>
                   <td><strong>'.$row->date_schedule.'</strong></td>
                   <td><strong>'.$row->time_schedule.'</strong></td>
                   <td><strong>'.$row->seats.'</strong></td>
                   <td><strong>'.$row->type.'</strong></td>
                   <td><strong>'.$row->category.'</strong></td>
                   <td><strong>'.$row->keterangan.'</strong></td>
                 <td><strong>'.$row->harga.'</strong></td>
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="'.$row->id_schedule.' "</td>
                
                </a></div>
                </td>
                </tr>';

       }

       }
   }

        public function get_data_affiliate($id){
 

         $sql = "SELECT * from reward_dp where id = '$id'";
  
        return $this->db->query($sql)->row_array();  
    }
    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        
        return $query->result();
    }

    function get_provinsi() {
    
    $query = $this->db->get('wilayah_provinsi');
    return $query->result();
    
    }
    public function get_report_registrasi($id_booking){
 
        $id_user=  $this->session->userdata('id_user');
         $sql = "SELECT * from faktur5free1 where id_booking = {$id_booking} ";
  
        return $this->db->query($sql)->row_array();  
    }


    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if($query->num_rows()>0){
                $result = $query->result();
                foreach($result as $value){
                    $out[$value->opsi_setting] = $value->key_setting;
                }
                return $out;
        } else {
            return array();
        }
    }
}
