<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departure_group extends CI_Controller{
	 var $folder = "departure_group";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(SCHEDULE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','settings');	
		$this->load->model('departuregroup_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/departure_group');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	   
	}

	private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->schedule_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="40%">'.$r->departure.'</td>';
	                $rows .='<td width="10%">'.$r->date_schedule.'</td>';
	                $rows .='<td width="10%">'.$r->time_schedule.'</td>';
	                $rows .='<td width="10%">'.$r->seats.'</td>';

	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'schedule/edit/'.$r->id_schedule.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_schedule('."'".$r->id_schedule."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               // $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'family_relation/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->schedule_model->save($data);
	     if($send)
	        redirect('schedule');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(SCHEDULE,'add'))
		    $this->general->no_access();

	    
	     $data = array('select_embarkasi'=>$this->_select_embarkasi());
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(SCHEDULE,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('schedule',array('id_schedule'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	    $data = array('select_embarkasi'=>$this->_select_embarkasi());
	    				
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->schedule_model->update($data);
	     if($send)
	        redirect('schedule');
		
	}
	
	

	
	public function ajax_delete($id_schedule)
	{
		if(!$this->general->privilege_check(SCHEDULE,'remove'))
		    $this->general->no_access();
		$send = $this->schedule_model->delete_by_id($id_schedule);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('schedule');
	}


}
