<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Departuregroup_model extends CI_Model{
    
    private $_table="schedule";
    private $_primary="id_schedule";

    public function save($data){
        
        // $cek = $this->db->select('id_user')->where('username',$data['username'])
        //                 ->or_where('nik',$data['nik'])->get('user')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'date_schedule' => $data['date_schedule'],
            'time_schedule' => $data['time_schedule'],
            'seats' => $data['seats'],
            
            'embarkasi' => $data['embarkasi'],
            'create_by' => $this->session->userdata('nama')
        );       
        
        return $this->db->insert('schedule',$arr);
    }
    public function update($data){
        
        $arr = array(
        
           
            'date_schedule' => $data['date_schedule'],
            'time_schedule' => $data['time_schedule'],
            'seats' => $data['seats'],
            
            'embarkasi' => $data['embarkasi'],
            'create_by' => $this->session->userdata('nama')
        );       
      
                
        return $this->db->update('schedule',$arr,array('id_schedule'=>$data['id_schedule']));
    }
    
    public function get_data($offset,$limit,$q=''){
    
        $sql = "SELECT a.*,b.departure FROM schedule a 
                LEFT JOIN embarkasi b ON b.id_embarkasi = a.embarkasi
                WHERE 1=1 
                ";

        if($q){
            
            $sql .=" AND a.date_schedule LIKE '%{$q}%'
                    OR b.departure LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY a.id_schedule DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    
    
    public function delete_by_id($id_schedule)
    {
        $this->db->where('id_schedule', $id_schedule);
        $this->db->delete($this->_table);
    }
    
    
}
