<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reg_jamaahrewardvoucher extends CI_Controller{
	var $folder = "reg_jamaahrewardvoucher";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REGISTRASI_JAMAAH_VOUCHER,'view'))
		    $this->general->no_access();	
		$this->session->set_userdata('menu','registration');	
		$this->load->model('regjamaahrewardvoucher_model');
		$this->load->library('terbilang');
		$this->load->helper('fungsi');
	}
	
	public function index(){
	
		$data = array(
			
			'select_product'=>$this->_select_product(),
			// 'select_rentangumur'=>$this->_select_rentangumur(),
			// 'select_affiliate'=>$this->_select_affiliate(),
			// 'select_kelamin'=>$this->_select_kelamin(),
			'select_embarkasi'=>$this->_select_embarkasi(),
			// 'query' => $this->regjamaahrewardvoucher_model->searchrefrensi(),
			'select_roomcategory'=>$this->_select_roomcategory(),
	    	// 'provinsi'=>$this->regjamaahrewardvoucher_model->get_all_provinsi(),
	    	// kabupaten'=>$this->regjamaahrewardvoucher_model->get_all_kabupaten()
	    	);
		 // $data2['query']= $this->regjamaahrewardvoucher_model->searchrefrensi();

	    $this->template->load('template/template', $this->folder.'/reg_jamaahrv',$data);	
		
	}

	public function get_registrasi(){
	    
	   $id_booking = $this->uri->segment(3);
	  $category = $this->uri->segment(4);
	  // $tempjmljamaah = $this->input->post('tempjmljamaah');
	  // redirect('registrasi_jamaah/detail/'.$id_booking.'');
				  $sql = "SELECT a.id_booking as id_booking,a.tempjmljamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking) as jumlah_jamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking and registrasi_jamaah.ket_keberangkatan='8') as jml_keluarga 
				         from booking as a where id_booking = '".$id_booking."'";
		 	$query = $this->db->query($sql)->result_array();
	     	 foreach($query as $key=>$value){
	         $jumlah_jamaah = $value['jumlah_jamaah'];
	         $jml_keluarga = $value['jml_keluarga'];
	         $booking = $value['id_booking'];
	         $tempjmljamaah = $value['tempjmljamaah'];
			if ($jumlah_jamaah < $tempjmljamaah){
				$this->session->set_flashdata('warning', "Data jamaah yang anda input = '".$jumlah_jamaah."' tidak sesuai dengan banyak jamaah yang anda input di awal sebanyak = '".$tempjmljamaah."'");
            	redirect('reg_jamaahrewardvoucher/detail/'.$id_booking.'');
       		}else{
				
					
					redirect('reg_jamaahrewardvoucher/report_registrasi/'.$id_booking.'');
       			
			}
	     }
		
	}



	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('view_refund')->like('pemberangkatan',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->pemberangkatan,
				'refund'	=>$row->refund
				

			);
		}
		
		echo json_encode($arr);
	}

	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->regjamaahrewardvoucher_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

	private function _select_affiliate_type(){
	
	    return $this->db->get('affiliate_type')->result();
	}

    private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}
	private function _select_product(){
	
	    $status = array('1');
		$this->db->where_in('status', $status);
	    return $this->db->get('product')->result();
	}
	private function _select_keluarga($id_booking=''){

		$this->db->where_in('id_booking', $id_booking);
	    return $this->db->get('registrasi_jamaah')->result();
	}
	private function _family_relation(){
		
		return $this->db->get('family_relation')->result();
	}
	private function _select_merchandise(){
		
		return $this->db->get('merchandise')->result();
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}

	private function _select_tipe_jamaah(){
		$kdstatus = array('2', '3','6');
		$this->db->where_in('id_tipe_jamaah', $kdstatus);
		return $this->db->get('tipe_jamaah')->result();

	}

	private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}
	

	private function _select_statuskawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();

	}


	private function _select_rentangumur(){
	
	    return $this->db->get('rentang_umur')->result();
	}

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_affiliate(){
		$id_affiliate_type = array('1','2','3');
		$status = array('1');
		$this->db->where_in('id_affiliate_type', $id_affiliate_type);
		$this->db->where_in('status', $status);
		return $this->db->get('affiliate')->result();
	    
	}

	private function _select_status_hubungan(){
		$kdstatus = array('8', '9');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    
	}

	private function _select_pemberangkatan(){
		return $this->db->get('view_refund')->result();
	    
	}
	
	private function _select_type(){
	    
	    return $this->db->get('room_type')->result();
	}

	function add_ajax_affiliate_type($id_affiliate_type){
		    $query = $this->db->get_where('affiliate',array('id_affiliate_type'=>$id_affiliate_type));
		    $data = "<option value=''>- Select affiliate -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->id_user."'>".$value->nama." - ".$value->id_user."</option>";
		    }
		    echo $data;
	}

	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	

	private function _select_hub_ahliwaris(){
		
		return $this->db->get('hub_ahli_waris')->result();
	}
	
	public function save(){
			// $data = $this->input->post(null,true);
	     
	  //   	 $send = $this->regjamaahrewardvoucher_model->save($data);
	   $jumlah_jamaah = $this->input->post('jumlah_jamaah');
        $seats = $this->input->post('seats');

        
        if ($seats < $jumlah_jamaah){
            $this->session->set_flashdata('Warning', "Jumlah Jamaah yang anda masukkan melebihi Seats yang Tersedia..Pilih Jadwal lagi, dengan jumlah seats yang anda ingginkan!!");
            
            	redirect('reg_jamaahrewardvoucher/index');
        }else{ 
			$data = $this->input->post(null,true);
	    	 $send = $this->regjamaahrewardvoucher_model->save($data);
	    
	    }
	    
	}

	public function save_jammaah(){


	$id_booking = $this->input->post('id_booking');
      $tempjmljamaah = $this->input->post('tempjmljamaah');
      $select_status_visa = $this->input->post('select_status_visa');
    	 $tanggal_visa = $this->input->post('tanggal_visa');
    	 // $category = $this->uri->segment(4);
    	 $category = $this->input->post('category');
	  $sql = "SELECT a.id_booking as id_booking,
		         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking) as jumlah_jamaah 
		         from booking as a where id_booking = '".$id_booking."'";
 	$query = $this->db->query($sql)->result_array();
 	 foreach($query as $key=>$value){
     $jumlah_jamaah = $value['jumlah_jamaah'];
     $booking = $value['id_booking'];
	}

	  $this->form_validation->set_rules('nama','nama','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            $no_identitas=$this->input->post('no_identitas'); // mendapatkan input dari kode
            $cek=$this->regjamaahrewardvoucher_model->cek($no_identitas); // cek kode di database
            $cek_idsahabat=$this->regjamaahrewardvoucher_model->cek_idsahabat($id_sahabat);
            if($cek->num_rows()>0){ // jika kode sudah ada, maka tampilkan pesan
                 
               // echo'<div class="alert alert-dismissable alert-danger"><small>No Identitas sudah digunakan</small></div>';
            	$this->session->set_flashdata('info', "No Identitas sudah digunakan");
            	 $id_booking = $this->input->post('id_booking');
            	redirect('reg_jamaahrewardvoucher/detail/'.$id_booking.'');
            // }elseif ($cek_idsahabat->num_rows()>0) {
            // 	 $id_booking = $this->input->post('id_booking');
            // 	redirect('reg_jamaahrewardvoucher/detail/'.$id_booking.'');
            }elseif ($jumlah_jamaah >= $tempjmljamaah) {
            	$this->session->set_flashdata('info', "Data Yang anda Input melebihi jumlah jamaah yg anda masukkan diregistrasi jamaah");
            	redirect('reg_jamaahrewardvoucher/detail/'.$id_booking.'');

            }
            else{
			 
			  $data = $this->input->post(null,true);
			  $send = $this->regjamaahrewardvoucher_model->save_registrasi($data);

			  $sql = "SELECT a.id_booking as id_booking,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking) as jumlah_jamaah,
				         (select COUNT(*) from registrasi_jamaah  where a.id_booking = registrasi_jamaah.id_booking and registrasi_jamaah.ket_keberangkatan='8') as jml_keluarga 
				         from booking as a where id_booking = '".$id_booking."'";
		 	$query = $this->db->query($sql)->result_array();
	     	 foreach($query as $key=>$value){
	         $jumlah_jamaah = $value['jumlah_jamaah'];
	         $jml_keluarga = $value['jml_keluarga'];
	         $booking = $value['id_booking'];

			if ($jumlah_jamaah == $tempjmljamaah){
            	redirect('reg_jamaahrewardvoucher/report_registrasi/'.$id_booking.'');

            	
       		}else{
				
					$this->session->set_flashdata('success', "Transaction Successfull");
					redirect('reg_jamaahrewardvoucher/detail/'.$id_booking.'');
			}
	     }


		}

	}
}

	public function add(){   
	     if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'add'))
		    $this->general->no_access();
	   		$data = array('select_product'=>$this->_select_product());
       	 $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	
	public function schedule(){
	     
	     if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('view_schedule',array('id_schedule'=>$id))->row_array();
	    if(!$get)
	        show_404();
	         $data = array(
	         	'select_affiliate'=>$this->_select_affiliate(),
	         	'select_affiliate_type'=>$this->_select_affiliate_type(),
	         	// 'select_status_fee'=>$this->_select_status_fee(),
	         	'select_tipe_jamaah'=>$this->_select_tipe_jamaah(),
	  		);
	  	// 	$opsi_val_arr = $this->regjamaahrewardvoucher_model->get_biaya_room();
				// foreach ($opsi_val_arr as $key => $value){
				// 	$out[$key] = $value;
				// }	
        $this->template->load('template/template', $this->folder.'/edit_jamaah',array_merge($data,$get));
	}



	
	 public function detail(){
	


	    if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'view'))
		    $this->general->no_access();
	    $id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->regjamaahrewardvoucher_model->get_pic_booking($id_booking,$id);
	    $pic    = array();
	    $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        
	        $pic = $this->regjamaahrewardvoucher_model->get_pic($id_booking);
	    }    

	      
	    $data = array(

	        	'family_relation'=>$this->_family_relation(),
				'select_product'=>$this->_select_product(),
				'select_merchandise'=>$this->_select_merchandise(),
				'select_rentangumur'=>$this->_select_rentangumur(),
				'select_status_hubungan'=>$this->_select_status_hubungan(),
				'select_kelamin'=>$this->_select_kelamin(),
				'select_statuskawin'=>$this->_select_statuskawin(),
		    	'provinsi'=>$this->regjamaahrewardvoucher_model->get_all_provinsi(),
		    	'kabupaten'=>$this->regjamaahrewardvoucher_model->get_all_kabupaten(),
		    	'select_embarkasi'=>$this->_select_embarkasi(),
		    	 'select_hub_ahliwaris'=> $this->_select_hub_ahliwaris(),
		    	'id_bandara' => $this->regjamaahrewardvoucher_model->get_data_bandara(),
		    	'select_affiliate'=>$this->_select_affiliate(),
	  			 'select_pemberangkatan'=>$this->_select_pemberangkatan(),
	  			 'select_type'=>$this->_select_type(),
	       		 'booking'=>$booking,'pic'=>$pic,
	       		 'provinsi_manasik'=>$this->regjamaahrewardvoucher_model->get_all_provinsi_manasik(),
	       		 'select_status_visa'=>$this->_select_status_visa(),
	       		 );
	 
	    $this->template->load('template/template', $this->folder.'/data_jamaah',($data));

	}
	

	public function data_jamaah(){
	     

	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('view_booking',array('id_booking'=>$id))->row_array();
	    if(!$get)
	        show_404();

	        	$this->data['family_relation']=$this->_family_relation();
				$this->data['select_product']=$this->_select_product();
				$this->data['select_merchandise']=$this->_select_merchandise();
				$this->data['select_rentangumur']=$this->_select_rentangumur();
				$this->data['select_status_hubungan']=$this->_select_status_hubungan();
				$this->data['select_kelamin']=$this->_select_kelamin();
				$this->data['select_statuskawin']=$this->_select_statuskawin();
		    	$this->data['provinsi']=$this->regjamaahrewardvoucher_model->get_all_provinsi();
		    	$this->data['kabupaten']=$this->regjamaahrewardvoucher_model->get_all_kabupaten();
		    	$this->data['select_embarkasi']=$this->_select_embarkasi();
		    	$this->data['select_keluarga']=$this->_select_keluarga($id);
		    	$this->data['select_affiliate']=$this->_select_affiliate();
		    	$this->data['select_pemberangkatan']=$this->_select_pemberangkatan();
	         	
				// $this->data['id_booking'] = $id_booking;

	    	$biaya_setting_ = $this->regjamaahrewardvoucher_model->get_key_val();
		  
			foreach ($biaya_setting_ as $key => $value){
				$out[$key] = $value;
			}

		
        $this->template->load('template/template', $this->folder.'/data_jamaah',array_merge($get,$out,$this->data));
	}
  


	function get_biaya_refund_(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->regjamaahrewardvoucher_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['refund'];
           
          
        }
        
	}

	function get_biaya_refund_2(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->regjamaahrewardvoucher_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['id_bandara'];
           
          
        }
        
	}

	function get_biaya_refund_3(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->regjamaahrewardvoucher_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['akomodasi'];
           
          
        }
        
	}

	function get_biaya_refund_4(){
		$pemberangkatan = $this->input->post('pemberangkatan');
		$select_pemberangkatan = $this->regjamaahrewardvoucher_model->get_data_bandara($pemberangkatan);
		 if($select_pemberangkatan->num_rows()>0){
            $select_pemberangkatan=$select_pemberangkatan->row_array();
            echo $select_pemberangkatan['handling'];
           
          
        }
        
	}

	function generate_bot_jamaah(){
		for($i=0; $i< 120; $i++){
			echo $i;
			$data = array(
				'kode_unik' => random_3_digit(),
				'create_date' => date('Y-m-d H:i:s'),
			);
			echo ' : ';
			echo '<b>'.$data['kode_unik'].'</b>';
			echo '<br>';
			$this->db->insert('booking', $data);
		}
	}
	
	
public function detail_jamaahnonaktif(){
	


	    // if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'view'))
		   //  $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->regjamaahrewardvoucher_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        $pic = $this->regjamaahrewardvoucher_model->get_pic($id_booking);

	    }    

	    $data = array(

	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahnonaktif',($data));

	}

function add_ajax_kab_manasik($id_prov){
		    $query = $this->db->get_where('view_manasik',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->kabupaten."</option>";
		    }
		    echo $data;
	}
	
	function add_ajax_kec_manasik($id_kab){
	    $query = $this->db->get_where('view_manasik',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->kecamatan."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des_manasik($id_kec){
	    $query = $this->db->get_where('view_manasik',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->kecamatan."</option>";
	    }
	    echo $data;
	}

	private function _select_status_visa(){
	    
	    return $this->db->get('status_visa')->result();
	}


	public function get_refrensi_reward_sahabat() {
        $id_sahabat = $this->input->post('userid',TRUE); //variabel kunci yang di bawa dari input text id kode
        $query = $this->regjamaahrewardvoucher_model->refrensi_reward_sahabat(); //query model
 
        $reward       =  array();
        foreach ($query as $d) {
            $reward[]     = array(
                'userid' => $d->id_sahabat, //variabel array yg dibawa ke label ketikan kunci
                'nama' => $d->nama//variabel yg dibawa ke id nama
                // 'ibukota' => $d->ibu_kota, //variabel yang dibawa ke id ibukota
                // 'keterangan' => $d->keterangan //variabel yang dibawa ke id keterangan
            );
        }
        echo json_encode($reward);      //data array yang telah kota deklarasikan dibawa menggunakan json
    }

   public function report_registrasi(){  
   		$id=  $this->session->userdata('id_user');
	    $id_booking = $this->uri->segment(3);
	    $report_registrasi = $this->regjamaahrewardvoucher_model->get_report_registrasi($id_booking,$id);
	    if(!$report_registrasi){
	        show_404();
	    }
	   
      
	    $data = array(

	       		 'report_registrasi'=>$report_registrasi,
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/report_registrasi',($data));
	    // $this->load->view('bilyet_barcode',($data));
	}
}
