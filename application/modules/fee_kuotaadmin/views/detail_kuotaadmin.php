
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        <form   class="form-horizontal" role="form" action='<?= base_url();?>fee_kuotaadmin/save' method='POST'>
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Fee Jamaah posting Sponsor </b>
                </div>
                  <div class="panel-body">                       

                   <input type="hidden" name="id_booking" value="<?php echo $kuota['id_booking_kuota']?>">
                   <input type="hidden" name="invoice" value="<?php echo $kuota['invoice']?>">
                    <input type="hidden" name="sponsor" value="<?php echo $kuota['sponsor']?>">
                    <input type="hidden" name="id_affiliate" value="<?php echo $kuota['id_affiliate']?>">
                   
                      
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Fee Posting Sponsor</b>
                </div>

                 <div class="panel-body">
                    
                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                	<!-- <th><input id="selecctall" type="checkbox">&nbsp;Check All</th> -->
                                    <th>No</th>
                                    <th>ID JAMAAH</th>
                                    <th>NAMA</th>
                                    <th>SPONSOR </th>
                                    <th>Fee </th>
                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                  	<!-- <td> <input name="textbox[]" class="checkbox1" type="hidden" id="textbox[]" value="<?php echo $pi['id_jamaah'] ?>" ></td> -->
                                      


                                    <td><?php echo $no ?>  <input name="textbox[]" class="checkbox1" type="hidden" id="textbox[]" value="<?php echo $pi['id_jamaah'] ?>" ></td> 
                                       <td><?php echo $pi['id_jamaah']?>  </td> 
                                      <td><?php echo $pi['nama']?>  </td>
                                      <td><?php echo $pi['sponsor']?> </td>
                                      <td><?php echo number_format($pi['fee_refrensi'])?>  </td>
                                      
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                
                  
                  
              </div>
          </div>
      </div>
    </div>
     
       <div class="row">
         <div class="col-lg-10">
         
          <div class="panel-body">
            <div class="form-group">
                    <label class="col-sm-2 control-label"for="inputPassword3" >Transfer Date</label>
                    <div class="col-sm-10">
                    
                         <input type="text" class="form-control" name="transfer_date" id="datepicker2"   placeholder="YYYY-MM-DD" />
                
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Dari Dana Bank </label>
                      <div class="col-lg-10">
                         <select required class="form-control" name="dana_bank">
                          <option value="">- Select Bank -</option>
                            <?php foreach($dana_bank as $sb){?>
                                <option value="<?php echo $sb->id;?>"><?php echo $sb->nama;?></option>
                            <?php } ?>
                    </select> 
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer </label>
                      <div class="col-lg-10">
                         <select required class="form-control" name="bank_transfer">
                          <option value="">- Select Bank -</option>
                            <?php foreach($bank_transfer as $sb){?>
                                <option value="<?php echo $sb->id;?>"><?php echo $sb->nama;?></option>
                            <?php } ?>
                    </select> 
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">No Rekening</label>
                      <div class="col-lg-10">
                        <input name="no_rek" class="form-control" > 
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Atas Nama Rekening</label>
                      <div class="col-lg-10">
                        <input name="nama_rek" class="form-control"  > 
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-lg-2 control-label">Nominal</label>
                      <div class="col-lg-10">
                        <input name="nominal" class="form-control" value="Rp <?php echo number_format($kuota['setelah_potong_pajak']);?>" readonly="true"  > 
                         <input type="hidden" name="fee" class="form-control" value="<?php echo $kuota['setelah_potong_pajak'];?>"  > 
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Keterangan</label>
                      <div class="col-lg-10">
                        <textarea class="form-control" name="keterangan" ></textarea>
                      </div>
                    </div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Approved</button>
        <a href="<?php echo base_url();?>fee_kuotaadmin"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
          </div>


        
     </div>
   </div>
  </form>       
                
</div>
<script type="text/javascript">
 $('#selecctall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });
</script>

              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 45});

  });
</script>

<script type="text/javascript">
	$("#clime").on('click', function (e){
    e.preventDefault();
var checkValues = $('.checkbox1:checked').map(function()
    {
        return $(this).val();
    }).get();
//console.log(checkValues); return  false;
$.ajax({
        url: '<?php echo base_url() ?>finance/fee_input/save',
        type: 'post',
        data: 'id_affiliate='+checkValues
        }).done(function(data){
            window.location.reload();
//            $("#respose-text").html(data);
        });
    });
 $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
   
</script>