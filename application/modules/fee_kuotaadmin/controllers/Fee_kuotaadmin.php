<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fee_kuotaadmin extends CI_Controller {

    var $folder = "fee_kuotaadmin";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend_affiliate/login_affiliate');
        if (!$this->general->privilege_check(KELOLAH_FEE_KUOTA_SPONSOR, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('feekuotaadmin_model');
    }

    public function index() {

        $this->template->load('template/template', $this->folder . '/fee_kuotaadmin');
    }

    public function get_data() {

        $limit = $this->config->item('limit');
        $offset = $this->uri->segment(3, 0);
        $q = isset($_POST['q']) ? $_POST['q'] : '';
        $data = $this->feekuotaadmin_model->get_data($offset, $limit, $q);
        $rows = $paging = '';
        $total = $data['total'];

        if ($data['data']) {

            $i = $offset + 1;
            $j = 1;
            foreach ($data['data'] as $r) {

                $rows .= '<tr>';

                $rows .= '<td width="5%">' . $i . '</td>';
                $rows .= '<td width="10%">' . $r->invoice . '</td>';
                $rows .= '<td width="10%">' . $r->sponsor . '</td>';
                $rows .= '<td width="10%">' . $r->affiliate . '</td>';
                $rows .= '<td width="10%">Rp ' . number_format($r->fee_refrensi) . '</td>';
                $rows .= '<td width="10%">Rp ' . number_format($r->pajak_fee) . '</td>';
                $rows .= '<td width="20%">Rp ' . number_format($r->fee_refrensi) . '</td>';
                $rows .= '<td width="20%" align="center">';
                if ($r->status_fee == 2) {
                    $rows .= '<a title="Proses Fee" class="btn btn-sm btn-primary" href="' . base_url() . 'fee_kuotaadmin/detail/' . $r->id_booking_kuota . '/' . $r->sponsor . '">
	                            <i class="fa fa-pencil"></i> Proses Fee
	                        </a> ';
                } else {
                    $rows .= '<a title="Sudah di Proses" class="btn btn-sm btn-success" href="#">
	                            </i> Sudah di Proses
	                        </a> ';
                }
                //  $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'profil/detail/'.$r->id_user.'">
                //              <i class="fa fa-pencil"></i> Detail
                //          </a> ';

                $rows .= '</td>';

                $rows .= '</tr>';

                ++$i;
                ++$j;
            }

            $paging .= '<li><span class="page-info">Displaying ' . ($j - 1) . ' Of ' . $total . ' items</span></i></li>';
            $paging .= $this->_paging($total, $limit);
        } else {

            $rows .= '<tr>';
            $rows .= '<td colspan="6">No Data</td>';
            $rows .= '</tr>';
        }

        echo json_encode(array('rows' => $rows, 'total' => $total, 'paging' => $paging));
    }

    private function _paging($total, $limit) {

        $config = array(
            'base_url' => base_url() . 'fee_postingaffiliate/get_data/',
            'total_rows' => $total,
            'per_page' => $limit,
            'uri_segment' => 3
        );
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }

    private function _select_bank() {

        return $this->db->get('bank')->result();
    }

    public function detail() {

        if (!$this->general->privilege_check(KELOLAH_FEE_KUOTA_SPONSOR, 'edit'))
            $this->general->no_access();

        $id_booking = $this->uri->segment(3);
        $sponsor = $this->uri->segment(4);
        $kuota = $this->feekuotaadmin_model->get_pic_kouta($id_booking, $sponsor);
        $pic = array();
        // $pic_booking= array();
        if (!$kuota) {
            show_404();
        } else {

            $pic = $this->feekuotaadmin_model->get_pic($id_booking, $sponsor);
        }

        $data = array(
            'dana_bank' => $this->_select_bank(),
            'bank_transfer' => $this->_select_bank(),
            'kuota' => $kuota, 'pic' => $pic
        );

        $this->template->load('template/template', $this->folder . '/detail_kuotaadmin', ($data));
    }

    function save() {
        $data = $this->input->post(null, true);
        // $data2 = $this->input->post('textbox');
        // $inputan = '';
        // foreach($data2 as $value){
        // 	$inputan .= ($inputan!=='') ? ',' : '';
        // 	$inputan .= $value;
        // }
        $simpan = $this->feekuotaadmin_model->save($data);
        if ($simpan)
            redirect('fee_kuotaadmin');
    }

}
