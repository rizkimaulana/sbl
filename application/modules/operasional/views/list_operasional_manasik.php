<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Operasional Manasik</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form name="form1" action="<?php echo base_url(); ?>operasional/operasional_manasik/add_claim_manasik" method="post">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th>NO.</th>
                                            <th>KD MANASIK</th>
                                            <th>AFFILIATE</th>
                                            <th>JML JAMAAH</th>
                                            <th>BIAYA</th>
                                            <th>KETERANGAN</th>
                                            <th>ACTION</th>
    <!--                                        <th><input type="checkbox" id="checkAll" name="checkAll"> All</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--Appended by Ajax-->
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="pull-right">
                                <ul class="pagination"></ul>    
                            </div>
                        </div>

                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->

<form action="<?php echo base_url()?>operasional/operasional_manasik/export_data_manasik" method="POST" name="form1" id="form1">
    <input type="hidden" id="kd_manasik" name="kd_manasik" value="" >
</form>
<form action="<?php echo base_url()?>operasional/operasional_manasik/detail_manasik" method="POST" name="form2" id="form2">
    <input type="hidden" id="kd_manasik2" name="kd_manasik2" value="" >
</form>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        //datatables
        table = $('#data-table').DataTable({
            "processing": true,
            "ajax": {
                "url": "<?php echo site_url('operasional/operasional_manasik/ajax_list_manasik') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                    //data.filter = $('#filter').val();
                }
            },
        });
    });

    function export_excel(kd_manasik) {
        $("#kd_manasik").val(kd_manasik);
        $("#form1").submit();
        
    }
    
    function detail_manasik(kd_manasik){
        $("#kd_manasik2").val(kd_manasik);
        $("#form2").submit();
    }


</script>