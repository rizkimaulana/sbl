<html lang="en">
    <head>
        <style>
            th, td { white-space: nowrap; }
        </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                    header("Content-type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename=list_claim_biaya_manasik_" . date('d-M-Y') . ".xls");
                    ?> 
                    <th>NO</th>
                    <th>KD MANASIK</th>
                    <th>INVOICE</th>
                    <th>ID JAMAAH</th>
                    <th>NAMA JAMAAH</th>
                    <th>AFFILIATE</th>
                    <th>BIAYA</th>
                    <th>KETERANGAN</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 0;
                foreach ($list as $pi) {
                    $no++
                    ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['kd_manasik'] ?>  </td>
                        <td><?php echo $pi['invoice'] ?>  </td>
                        <td><?php echo $pi['id_jamaah'] ?>  </td>
                        <td><?php echo $pi['nama_jamaah'] ?>  </td>
                        <td><?php echo $pi['affiliate'] ?>  </td>
                        <td><?php echo $pi['biaya'] ?>  </td>
                        <td><?php echo $pi['keterangan'] ?>  </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url(); ?>assets/css/opensans.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/raleway.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
