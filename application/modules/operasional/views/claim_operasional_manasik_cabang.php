<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Claim Operasional Manasik</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form name="form1" action="<?php echo base_url(); ?>operasional/operasional_manasik/add_claim_manasik" method="post">
                        <div class="col-lg-12">

                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="submit" class="btn btn-warning" value="Claim Operasional Manasik">
                            </div>
                            <div style="height: 400px; overflow-y: scroll;">
                                <div class="table-responsive">
                                    <table id="data-table" class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="10%">ID REG.</th>
                                                <th width="10%">ID JAMAAH</th>
                                                <th width="20%">NAMA JAMAAH</th>
                                                <th width="20%">KAB/KOTA</th>
                                                <th width="20%">PROVINSI</th>
                                                <th width="10%"><input type="checkbox" id="checkAll" name="checkAll" checked> All</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!--Appended by Ajax-->
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                                <div class="pull-right">
                                    <ul class="pagination"></ul>    
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->


<script type="text/javascript">
    var table;
    $(document).ready(function () {

        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "info": false,
            "bPaginate": false,
            "bLengthChange": false,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "<?php echo site_url('operasional/operasional_manasik/ajax_list_cabang') ?>",
                "type": "POST",
                "data": function (data) {
                    
                }
            },
        });

        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
        
        $(document).on('click', '.cb', function() {
            if ($(this).not(':checked')) {
              $("#checkAll").prop('checked', false);
            }
          });
        
    });

</script>