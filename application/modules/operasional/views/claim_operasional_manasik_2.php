<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Claim Operasional Manasik</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form name="form1" action="<?php echo base_url();?>operasional/operasional_manasik/add_claim_manasik" method="post">
                        <div class="col-lg-12">
                            
                        </div>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered table-hover" >
                                    <thead>
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="10%">ID REG.</th>
                                            <th width="10%">ID JAMAAH</th>
                                            <th width="20%">NAMA JAMAAH</th>
                                            <th width="20%">KAB/KOTA</th>
                                            <th width="20%">PROVINSI</th>
                                            <th width="20%">ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--Appended by Ajax-->
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="pull-right">
                                <ul class="pagination"></ul>    
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="submit" class="btn btn-warning" value="Claim Operasional Manasik">
                            </div>
                            <div class="table-responsive">
                            <table id="data-table2" class="table table-striped table-bordered table-hover" >
                                <thead>
                                    <tr>
                                        <th width="10%" class="text-center">ID REG.</th>
                                        <th width="15%">ID JAMAAH</th>
                                        <th width="20%">NAMA JAMAAH</th>
                                        <th width="15%">KAB/KOTA</th>
                                        <th width="20%">PROVINSI</th>
                                        <th width="10%">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--Appended by Ajax-->
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->


<script type="text/javascript">
    var table;
    $(document).ready(function () {

        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "info" : false,
            //"searching" : false,
            "pageLength": 5,
            "ajax": {
                "url": "<?php echo site_url('operasional/operasional_manasik/ajax_list') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#awal').val();
                    data.akhir = $('#akhir').val();
                    //data.filter = $('#filter').val();
                }
            },
        });

        //
//        $("#data-table tbody").on('click', 'tr', function(){
//            var id_registrasi = $("td", this).eq(1).text();
//            var id_jamaah = $("td", this).eq(2).text();
//            var nama = $("td", this).eq(3).text();
//            var kota = $("td", this).eq(4).text();
//            var provinsi = $("td", this).eq(5).text();
//            
//            
//            $("#data-table2 tr").last().after('<tr><td><input type="hidden" name="id_registrasi[]" value="'+id_registrasi+'">'+id_registrasi+'</td><td>'+id_jamaah+'</td><td>'+nama+'</td><td>'+kota+'</td><td>'+provinsi+'</td><td><input type="button" value="x" class="btn btn-danger" onclick="deleteRow(this)"></td></tr>' );
//        });

        
    });
    
    function deleteRow(r) {
        var i = r.parentNode.parentNode.rowIndex;
        document.getElementById("data-table2").deleteRow(i);
    }
    
    
    function test(id_registrasi){
        var link_rm = "<?php echo site_url('operasional/operasional_manasik/get_data') ?>";
        link_rm = link_rm + "/" + id_registrasi;
        $.get(link_rm, function (data) {
           var id_registrasi = data.id_registrasi;
           var id_jamaah = data.id_jamaah;
           var nama = data.nama;
           var kota = data.kota;
           var provinsi = data.provinsi;
           
            $("#data-table2 tr").last().after('<tr><td><input type="hidden" name="id_registrasi[]" value="'+id_registrasi+'">'+id_registrasi+'</td><td>'+id_jamaah+'</td><td>'+nama+'</td><td>'+kota+'</td><td>'+provinsi+'</td><td><input type="button" value="x" class="btn btn-danger" onclick="deleteRow(this)"></td></tr>' );
        }, "json");
        
    }
</script>