<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_manasik extends CI_Controller {

    var $folder = "operasional";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend_affiliate/login_affiliate');
        if (!$this->general->privilege_check_affiliate(CLAIM_OPERASIONAL_MANASIK, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'operasional');
        $this->load->model('Operasional_manasik_model', 'manasik_m');
    }

    public function index() {
        $id_user = $this->session->userdata('id_user');
        $this->template->load('template/template', $this->folder . '/claim_operasional_manasik');
    }
    
    public function ajax_list() {
        $userid = $this->session->userdata('id_user');
        $keyword = $this->uri->segment(4) ? $this->uri->segment(4) : '';
        $keyword = urldecode($keyword);
        
        $hasil = $this->manasik_m->get_data_manasik($keyword);
        
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $kab = $this->get_data_kabupaten($item['kabupaten_id']);
            $prov = $this->get_data_provinsi($item['provinsi_id']);
            
            $manasik = $this->get_data_manasik($item['id_registrasi']);
            $affiliate = $this->get_data_affiliate($manasik['id_affiliate']);
            $row[] = '<td width="10%" >' . $item['id_registrasi'] . '<td>';
            $row[] = '<td width="10%" >' . $item['id_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . $item['nama'] .'<td>';
            $row[] = '<td width="10%" >' . $kab['nama'] . '<td>';
            $row[] = '<td width="10%" >' . $prov['nama'] . '<td>';
            
            $add = '';
            $id_reg= $item['id_registrasi'];
            $id_jamaah = $item['id_jamaah'];
            $nama = $item['nama'];
            $kab = $kab['nama'];
            $prov = $prov['nama'];
            if($item['status_claim'] == 0){
                $add = '<center><button type="button" class="btn btn-primary" name="addRow" id="addRow" title="Tambahkan ke claim manasik" onclick ="claim('.$id_reg.')">Add</button></center>';
            }else{
                $add = 'Sudah Di Claim : '.$manasik['id_affiliate'].' ('.$affiliate['nama'].')';
            }
            $row[] = '<td width="10%" >' . $add . '</td>';
            $data[] = $row;
        }
        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }
    
    public function manasik_cabang(){
        $this->template->load('template/template', $this->folder . '/claim_operasional_manasik_cabang');
    }
    
    public function ajax_list_cabang() {
        $userid = $this->session->userdata('id_user');
        $hasil = $this->manasik_m->get_data_manasik_cabang($userid);
        
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $kab = $this->get_data_kabupaten($item['kabupaten_id']);
            $prov = $this->get_data_provinsi($item['provinsi_id']);
            
            $manasik = $this->get_data_manasik($item['id_registrasi']);
            $affiliate = $this->get_data_affiliate($manasik['id_affiliate']);
            $row[] = '<td width="10%" >' . $item['id_registrasi'] . '<td>';
            $row[] = '<td width="10%" >' . $item['id_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . $item['nama'] .'<td>';
            $row[] = '<td width="10%" >' . $kab['nama'] . '<td>';
            $row[] = '<td width="5%" >' . $prov['nama'] . '<td>';
            
            $add = '';
            $id_reg= $item['id_registrasi'];
            $id_jamaah = $item['id_jamaah'];
            $nama = $item['nama'];
            $kab = $kab['nama'];
            $prov = $prov['nama'];
            if($item['status_claim'] == 0){
                $add = '<center><input type="checkbox" name="id_registrasi[]" class = "cb" checked value="'.$id_reg.'" /></center>';
            }else{
                $add = 'Sudah Di Claim : '.$manasik['id_affiliate'].' ('.$affiliate['nama'].')';
            }
            $row[] = '<td width="10%" >' . $add . '</td>';
            $data[] = $row;
        }
        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }
    
    function get_data($id_registrasi){
        
        $row = $this->db->get_where('registrasi_jamaah', array('id_registrasi' => $id_registrasi))->row_array();
        
        $kab = $this->get_data_kabupaten($row['kabupaten_id']);
        $row['kota'] = $kab['nama'];
        
        $prov = $this->get_data_provinsi($row['provinsi_id']);
        $row['provinsi'] = $prov['nama'];
        
        $hasil = array(
           'id_registrasi'=> $row['id_registrasi'],
           'id_jamaah'=> $row['id_jamaah'], 
           'nama'=> $row['nama'],
           'kota'=> $row['kota'],
           'provinsi'=> $row['provinsi'] 
                
        );
        //$this->myDebug($hasil);
        echo json_encode($hasil);
    }
    
    function add_claim_manasik(){
        $data = $this->input->post();
        $send = $this->manasik_m->save($data);
        if($send){
            $this->session->set_flashdata('info', "Data claim biaya manasik jamaah sudah tersimpan !");
            redirect('operasional/operasional_manasik/list_operasional_manasik');
        }else{
            $this->session->set_flashdata('warning', "Data claim biaya manasik jamaah yang anda input tidak tersimpan !");
            redirect('operasional/operasional_manasik');
        }
        
    }
    
    function add_claim_manasik_cabang(){
        $data = $this->input->post();
        
        $post_data = array_unique($data['cekRow']);
        $this->myDebug($data['cekRow']);
    }
    
    function list_operasional_manasik(){
        $this->template->load('template/template', $this->folder . '/list_operasional_manasik');
    }
    
    public function ajax_list_manasik() {
        $userid = $this->session->userdata('id_user');
      
        $hasil = $this->manasik_m->get_list_manasik();

        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $row[] = '<td width="10%" >' . $item['kd_manasik'] . '<td>';
            $row[] = '<td width="10%" >' . $item['id_affiliate'] . '<td>';
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] .'<td>';
            $row[] = '<td width="10%" >' . number_format($item['biaya']) . '<td>';
            $ket = '';
            if($item['status']==0){
                $ket = 'Sudah di Claim';
            }elseif($item['status']==1){
                $ket = 'Proses Pengajuan SPB';
            }elseif($item['status']==2){
                $ket = 'Processing';
            }elseif($item['status']==3){
                $ket = 'Pengajuan';
            }elseif($item['status']==4){
                $ket = 'Approved';
            }elseif($item['status']==5){
                $ket = 'Sudah Di Transfer';
            }
            $row[] = '<td width="10%" >' .$ket . '<td>';
            $kd_manasik = $item['kd_manasik'];
            $print = '<input type="button" class="btn btn-success" name="export" value="Export Data Manasik" onclick="export_excel(\''.$kd_manasik.'\')">';
            $detail = '<input type="button" class="btn btn-warning" name="detail" value="Detail" onclick="detail_manasik(\''.$kd_manasik.'\')">';
            $row[] = '<td width="10%" >' . $print . ' ' .$detail.'<td>';
            $data[] = $row;
        }
        //$this->myDebug($data);
        $output = array(
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
    }
    
    function export_data_manasik() {
        $data = $this->input->post();
        $id_user = $this->session->userdata('id_user');
        $sql = "SELECT * FROM group_manasik WHERE kd_manasik = '".$data['kd_manasik']."'".
                "AND id_affiliate = '".$id_user."'";
       
        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            
            $jamaah = $this->get_data_jamaah($item['id_jamaah']);
            $affiliate = $this->get_data_affiliate($item['id_affiliate']);
            
            $item['nama_jamaah'] = $jamaah['nama'];
            $item['affiliate'] = $affiliate['id_affiliate'].' - '.$affiliate['nama'];
            $row[] = $no;
            $row[] = $item['kd_manasik'];
            $row[] = $item['invoice'];
            $row[] = $item['id_jamaah'];
            $row[] = $item['nama_jamaah'];
            $row[] = $item['affiliate'];
            $row[] = $item['biaya'];
            $row[] = $item['keterangan'];
            
            $data[] = $item;
        }
        $data['list'] = $data;

        $this->load->view('operasional/cetak_manasik_excel', ($data));
    }
    
    function detail_manasik(){
        $data = $this->input->post();
        $data['kd_manasik'] = $data['kd_manasik2'];
        $this->template->load('template/template', $this->folder . '/detail_operasional_manasik', $data);
    }
    
    function ajax_detail_manasik($kd_manasik){
        
        $hasil = $this->manasik_m->get_detail_manasik($kd_manasik);

        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $kab = $this->get_data_kabupaten($item['kabupaten_id']);
            $prov = $this->get_data_provinsi($item['provinsi_id']);
            
            $row[] = '<td width="10%" >' . $item['id_registrasi'] . '<td>';
            $row[] = '<td width="10%" >' . $item['id_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . $item['nama'] .'<td>';
            $row[] = '<td width="10%" >' . $kab['nama'] . '<td>';
            $row[] = '<td width="10%" >' . $prov['nama'] . '<td>';
            
            $id_registrasi = $item['id_registrasi'];
            if($item['status']==0){
                $delete = '<center><input type="button" title="hapus detail" class="btn btn-danger" name="delete" value="x" onclick="delete_detail(\''.$id_registrasi.'\')"></center>';
            }else{
                $delete = '<center><input type="button" disabled class="btn btn-danger" name="delete" value="x"></center>';
            }
            
            $row[] = '<td width="10%" >' . $delete .'<td>';
            $data[] = $row;
        }
        //$this->myDebug($data);
        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    function delete_detail($id_registrasi){
        $send = $this->manasik_m->delete_detail_manasik($id_registrasi);
    }
    
    function get_data_jamaah($id_jamaah){
        $row = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $id_jamaah))->row_array();
        $hasil = array(
           'nama'=> $row['nama']
        );
        return $hasil;
    }
    
    function get_data_affiliate($userid){
        $row = $this->db->get_where('affiliate', array('id_user' => $userid))->row_array();
        $hasil = array(
            'id_affiliate' => $row['id_affiliate'],
            'nama'=> $row['nama']
        );
        return $hasil;
    }
    
    function get_data_kabupaten($id){
        $row = $this->db->get_where('wilayah_kabupaten', array('kabupaten_id' => $id))->row_array();
        $hasil = array(
            'nama'=> $row['nama']
        );
        return $hasil;
    }
    
    function get_data_provinsi($id){
        $row = $this->db->get_where('wilayah_provinsi', array('provinsi_id' => $id))->row_array();
        $hasil = array(
            'nama'=> $row['nama']
        );
        return $hasil;
    }
    
    function get_data_manasik($id_registrasi){
        $row = $this->db->get_where('group_manasik', array('id_registrasi' => $id_registrasi))->row_array();
        $hasil = array(
            'id_affiliate'=> $row['id_affiliate']
        );
        return $hasil;
    }
    
    

}
