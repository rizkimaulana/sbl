<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional extends CI_Controller {

    var $folder = "operasional";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend_affiliate/login_affiliate');
        if (!$this->general->privilege_check_affiliate(OPERASIONAL, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'operasional');
        $this->load->model('Operasional_handling_model');
    }

    public function index() {
        $id_user = $this->session->userdata('id_user');
        //$this->myDebug($this->session->userdata('jabatan_id'));
        $this->template->load('template/template', $this->folder . '/claim_biaya_operasional');
    }

    public function operasional_handling() {
        //$this->myDebug($this->session->userdata('jabatan_id'));
        //claim operasional by invoice gm
        $this->template->load('template/template', $this->folder . '/operasional_handling');
    }

    public function ajax_list() {
        $id_user = $this->session->userdata('id_user');
        //$sql = "call List_handling_cabang(?)";
        $sql = "SELECT a.invoice, a.id_affiliate, b.nama as affiliate, COUNT(*) as jml_jamaah,
sum(if((`a`.`status_manifest` = '1'),1,0)) as status_manifest from registrasi_jamaah as a
        JOIN affiliate b ON b.id_user = a.id_affiliate
        JOIN additional_cost c ON (c.id_registrasi = a.id_registrasi and c.invoice = a.invoice AND c.jns_trans = '144' AND c.status = '1')
	where a.`status` in ('1') AND a.id_affiliate = '" . $id_user . "' GROUP BY a.id_booking";
        $hasil = $this->db->query($sql)->result_array();
        
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;


            $row[] = '<td width="20%" >' . $item['invoice'] . '<td>';
            $row[] = '<td width="20%" >' . $item['id_affiliate'] . '<br>' . $item['affiliate'] . '<td>';
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . number_format($item['jml_jamaah'] * 150000) . '<td>';
            $btnProcess = '';
            if($item['status_manifest']==$item['jml_jamaah']){
                $status = '<button class="btn btn-sm btn-success" title="Approved" >Approved</button>';
                $btnProcess = '<a class="btn btn-sm btn-warning" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
            }elseif ($item['status_manifest']<=$item['jml_jamaah']){
                $status = '<button class="btn btn-sm btn-danger"title="Nor Approve" >Not Approve: Jamaah : '.$item['jml_jamaah'].', Manifest : '.$item['status_manifest'].'</button>';
            }//else{
//                $status = '<button class="btn btn-sm btn-danger"title="Nor Approve" >Not Approve: Manifest Belum Terdata di Pusat</button>';
//            }
            
            $row[] = '<td width="20%" >' . $status . '<td>';

            
//            if ($item['status'] == '1') {
//
//                $btnProcess = '<a class="btn btn-sm btn-warning" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
//            }elseif($item['status'] == '1A'){
//                $btnProcess = '<button type="button" class="btn btn btn-danger" >Menunggu Approval GM</button>';
//            }elseif ($item['status'] == '2') {
//                $btnProcess = '<button type="button" class="btn btn btn-primary" >Menunggu Konfirmasi</button>';
//            } elseif ($item['status'] == '3') {
//                $btnProcess = '<button type="button" class="btn btn btn-info" >On Process</button>';
//            } else{
//                $btnProcess = '<button type="button" class="btn btn btn-success" >Sudah di uangkan</button>';
//            }

            $row[] = '<td width="20%" >' . $btnProcess . '</td>';

            $data[] = $row;
        }
        //$this->myDebug($data);
        $output = array(
            "data" => $data,
        );
        //output to json format

        echo json_encode($output);
    }

    public function claim_biaya_submit() {
        $invoice = $this->input->post('invoice');

        $send = $this->Operasional_handling_model->claim_update_gm($invoice);
        if ($send) {
            //redirect('operasional');
            $data['invoice'] = $invoice;
            $this->template->load('template/template', $this->folder . '/operasional_handling', $data);
        }
    }

    public function claim_biaya() {
        $invoice = $this->uri->segment(3);
        $send = $this->Operasional_handling_model->claim_update($invoice);
        
    }

    public function approve() {
        $invoice = $this->uri->segment(3);
        $send = $this->Operasional_handling_model->approve($invoice);
    }

    public function not_approve() {
        $invoice = $this->uri->segment(3);
        $send = $this->Operasional_handling_model->not_approve($invoice);
    }
    
    public function list_operasional(){
        //echo 'test';
        $this->template->load('template/template', $this->folder . '/list_operasional_handling');
    }
    
    function ajax_list_handling(){
        $id_user = $this->session->userdata('id_user');
        $data = array();
        
        $sql = "SELECT invoice, id_user, count(*) jml_jamaah, sum(jumlah) as biaya, status FROM additional_cost ".
                "WHERE id_user = '".$id_user."' AND jns_trans = '144' AND status <> '1' ".
                "GROUP BY id_booking";
        $hasil = $this->db->query($sql)->result_array();
        
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            
            //get data affiliate untuk transfer
            $transfer = $this->get_data_affiliate($item['id_user']);
            if($item['status']=='1A'){
                $status = 'Menunggu Approval Dari GM';
            }else if($item['status']=='2'){
                $status = 'Menunggu Konfirmasi';
            }else if($item['status']=='3'){
                $status = 'Dalam Proses';
            }else{
                $status = 'Sudah di Transfer ke ('.$item['id_user'].') '.$transfer['nama'];
            }
            
            //cek status di table t_handling
            $this->db->select('status');
            $handling = $this->db->get_where('t_handling', array('invoice' => $item['invoice']))->row_array();
            if(count($handling) > 0){
                if($handling['status']==1){
                    $status = ($handling['status']==1) ? 'Proses Pengajuan' : $status;
                }
            }
            //get data affiliate
            $affiliate = $this->get_data_affiliate($id_user);
            
            $row[] = '<td>' . $item['invoice'] . '<td>';
            $row[] = '<td>' . $item['id_user'] .' '.$affiliate['nama'] . '<td>';
            $row[] = '<td>' . $item['jml_jamaah'] . '<td>';
            $row[] = '<td>' . number_format($item['biaya']) . '<td>';
            $row[] = '<td>'.$status.'<td>';
            
            $data[] = $row;
        }
        
        $output = array(
            'data' => $data
        );
        //output
        echo json_encode($output);
        
    }
//    public function ajax_list_invoice($invoice) {
//        $invoice = $invoice;
//        $id_user = $this->session->userdata('id_user');
//        $data = array();
//
//        $id_user = $this->session->userdata('id_user');
//        //$this->myDebug($id_user);
//        $sql = "call List_handling_claim(?)";
//        $hasil = $this->db->query($sql, array('id_user' => $id_user))->result_array();
//
//        $no = 0;
//        foreach ($hasil as $item) {
//            $no++;
//            $row = array();
//            //$row[] = $no;
//
//            $btnProcess = '';
//            if ($item['status'] == '1') {
////$btnProcess = '<button onclick="claim_biaya(' . $item['invoice'] . ')" type="button" class="btn btn btn-warning" >Claim</button>';
//                $btnProcess = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
//            } elseif ($item['status'] == '1A') {
//                $btnProcess = '<button type="button" class="btn btn btn-danger" >Menunggu Approval GM</button>';
//            } elseif ($item['status'] == '2') {
//                $btnProcess = '<button type="button" class="btn btn btn-primary" >Menunggu Konfirmasi</button>';
//            } elseif ($item['status'] == '3') {
//                $btnProcess = '<button type="button" class="btn btn btn-info" >On Process</button>';
//            } else {
//                $btnProcess = '<button type="button" class="btn btn btn-success" >Sudah di Transfer ke </button>';
//            }
//
//            $row[] = '<td>' . $item['invoice'] . '<td>';
//            $row[] = '<td>' . $item['id_user'] . '<td>';
//            $row[] = '<td>' . $item['id_user'] . '<td>';
//            $row[] = '<td>' . $item['jml_jamaah'] . '<td>';
//            $row[] = '<td>' . number_format($item['biaya_operasional']) . '<td>';
//            $row[] = '<td>' . $item['keterangan'] . '<td>';
//            $row[] = '<td>' . $btnProcess . '<td>';
//
//            $data[] = $row;
//        }
//        $output = array(
//            'data' => $data
//        );
//        //output
//        //$this->myDebug($data);
//        echo json_encode($output);
//    }

    function search_invoice() {
        $search = $this->input->post('s');
        
        if (!empty($search)) {
            $query = $this->Operasional_handling_model->search_invoice($search);
            if (empty($query->result())) {

                echo'<tr><td colspan="10"><h2 style="color: #9F6000;">Maaf ! DATA Yang Anda Cari Kosong </h2></td></tr>';
                exit;
            } else {

                foreach ($query->result() as $row) {

                    $btnProcess = '';
    //                if ($row->status == '1') {
    //                    $btnProcess = '<div class="btn-group">
    //                        <button type="button" onclick ="claim_biaya_submit(\'' . $row->invoice . '\')"  class="btn btn-warning" ><i class="glyphicon glyphicon-pencil"></i>Claim</a>
    //                        </div>';
    //                    //$btnProcess = '<button onclick="claim_biaya(' . $item['invoice'] . ')" type="button" class="btn btn btn-warning" >Claim</button>';
    //                    //$btnProcess = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
    //                } elseif ($row->status == '1A') {
    //                    $btnProcess = '<button type="button" class="btn btn btn-danger" >Menunggu Approval GM</button>';
    //                } elseif ($row->status == '2') {
    //                    $btnProcess = '<button type="button" class="btn btn btn-primary" >Menunggu Konfirmasi</button>';
    //                } elseif ($row->status == '3') {
    //                    $btnProcess = '<button type="button" class="btn btn btn-info" >On Process</button>';
    //                } else {
    //                    $btnProcess = '<button type="button" class="btn btn btn-success" >Sudah di uangkan</button>';
    //                } 
                    $btnProcess = '';
                    if ($row->status_manifest == $row->jml_jamaah) {
                        $status = '<button class="btn btn-sm btn-success" href="" title="Approved" >Approved</button>';
                        $btnProcess = '<a class="btn btn-sm btn-warning" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $row->invoice . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
                    } else {
                        $status = '<button class="btn btn-sm btn-danger" href="" title="Not Approve" >Not Approved: Jamaah : '.$row->jml_jamaah.', Manifest : '.$row->status_manifest.'</button>';
                    }
                    
                    $cabang = $this->get_data_cabang($row->invoice);
                    if($cabang['status_additional_cost'] != '1'){
                      
                        $btnProcess = '<button class="btn btn-sm btn-primary" href="" title="Claim" >Sudah Di Claim</button>';
                    }
                    
                    echo '<tr>

                       <td><strong>' . $row->invoice . '</strong></td>
                       <td>' . $row->id_affiliate . '</td>    
                       <td>' . $row->affiliate . '</td>
                       <td>' . $row->jml_jamaah . '</td>
                       <td>' . number_format($row->jml_jamaah * 150000) . '</td>
                       <td>' . $status . '</td>

                        <td>' . $btnProcess . '</td>
                    </tr>';
                    //<a href="' . base_url() . 'operasional/claim_biaya/' . $row->invoice . '"  class="btn btn-warning" ><i class="glyphicon glyphicon-pencil"></i>Claim</a>
                }
            }
            
        } else {
            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
        }
    }

    function approval_claim_operasional() {
        $this->template->load('template/template', $this->folder . '/appr_claim_biaya_operasional');
    }

    public function ajax_list_appr() {
        $id_user = $this->session->userdata('id_user');
        //$sql = "call List_handling_cabang(?)";
        $sql = "SELECT a.invoice, a.id_affiliate, b.nama as affiliate, COUNT(*) as jml_jamaah,
                sum(if((`a`.`status_manifest` = '1'),1,0)) as status_manifest from registrasi_jamaah as a
                JOIN affiliate b ON b.id_user = a.id_affiliate
                where a.`status` in ('1') and a.id_affiliate = '".$id_user."'
                GROUP BY a.id_booking;";
        $hasil = $this->db->query($sql)->result_array();
        //$this->myDebug($hasil);
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            $row = array();
            $row[] = $no;

            $transfered = '';
            $row[] = '<td width="20%" >' . $item['invoice'] . '<td>';
            $row[] = '<td width="20%" >' . $item['id_affiliate'].'<br>'.$item['affiliate'].'<td>';
            
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . number_format($item['jml_jamaah'] * 150000) . '<td>';
            
            $btnProcess = '';
            if ($item['status_manifest'] == $item['jml_jamaah']) {
                $status = '<strong>Complete</strong>';
                $btnProcess = '<a class="btn btn-sm btn-warning" href="javascript:void()" title="CLAIM" onclick="claim_biaya(' . "'" . $item['invoice'] . "'" . ')"><i class="fa fa-pencil"></i> Claim</a>';
            } else {
                $status = 'Not Complete';
            }
            
            $row[] = '<td width="20%" >' . $status . '<td>';
            
            $btnAppr = '';
            $btnNotAppr = '';
            $ket = '';
            $cabang = $this->get_data_cabang($item['invoice']);
            if(empty($cabang['id_user']) || $cabang['id_user'] == $id_user){
                $ket = '<a class="btn btn-sm btn-warning" > Belum di claim </a>';
            }elseif(!empty($cabang['id_user']) && $cabang['id_user'] != $id_user){
                $ket = '<a class="btn btn-sm btn-primary" > Di claim oleh : ' . $cabang['id_user'] . '<br>' . $cabang['nama_cabang'] . ' </a>';
                if($cabang['status_additional_cost']==2){
                    $ket .= '<br><a class="btn btn-sm btn-info" > Menunggu konfirmasi </a>';
                }
                
            }
            
            if ($cabang['status_additional_cost'] != '1A') {
                //$btnStatus = '<a class="btn btn-sm btn-warning" > Belum di claim </a>';
                $btnAppr = '<a disabled class="btn btn-sm btn-success" href="javascript:void()" title="APPROVE" onclick="approve(' . "'" . $item['invoice'] . "'" . ')"> Approve</a>';
                $btnNotAppr = '<a disabled class="btn btn-sm btn-danger" href="javascript:void()" title="NOT APPROVE" onclick="not_approve(' . "'" . $item['invoice'] . "'" . ')"> Not Approve</a>';
            } else if ($cabang['status_additional_cost'] == '1A') {
                //$btnStatus = '<a class="btn btn-sm btn-primary" > Di claim oleh : ' . $item['id_claim'] . '<br>' . $cabang['nama_cabang'] . ' </a>';
                $btnAppr = '<a class="btn btn-sm btn-success" href="javascript:void()" title="APPROVE" onclick="approve(' . "'" . $item['invoice'] . "'" . ')"> Approve</a>';
                $btnNotAppr = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="NOT APPROVE" onclick="not_approve(' . "'" . $item['invoice'] . "'" . ')"> Not Approve</a>';
           } //else if ($cabang['status_additional_cost'] == '2') {
//                //$test = 'test';//$ket.'<br><a class="btn btn-sm btn-primary" > Menunggu konfirmasi </a>';
//                //$btnAppr = '<a disabled class="btn btn-sm btn-success" href="javascript:void()" title="APPROVE" onclick="approve(' . "'" . $item['invoice'] . "'" . ')"> Approve</a>';
//                //$btnNotAppr = '<a disabled class="btn btn-sm btn-danger" href="javascript:void()" title="NOT APPROVE" onclick="not_approve(' . "'" . $item['invoice'] . "'" . ')"> Not Approve</a>';
//            }
            $row[] = '<td width="20%" >'.$ket.'<td>';


            $row[] = '<td width="20%" >'. $btnAppr . ' ' . $btnNotAppr . '</td>';

//$row[] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'].'<br>'.$aktivasi['update_by'];
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    function get_data_cabang($invoice) {
        $this->db->select('additional_cost.id_user, affiliate.nama as nama_cabang, additional_cost.status');
        $this->db->join('affiliate', 'affiliate.id_user = additional_cost.id_user');
        $this->db->where('additional_cost.invoice', $invoice);
        $this->db->where('additional_cost.jns_trans', '144');
        $this->db->order_by('additional_cost.id_user', 'DESC');
        $hasil = $this->db->get('additional_cost')->row_array();
        $result = array(
            'id_user' => !empty($hasil['id_user']) ? $hasil['id_user'] : '',
            'nama_cabang' => !empty($hasil['nama_cabang']) ? $hasil['nama_cabang'] : '',
            'status_additional_cost' => !empty($hasil['status']) ? $hasil['status'] : ''
        );
        //$this->myDebug($result);
        return $result;
    }
    
    function get_data_affiliate($id_user){
        $row = $this->db->get_where('affiliate', array('id_user' => $id_user))->row_array();
        $hasil = array('nama' => $row['nama']);
        return $hasil;
    }

}
