<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_cost extends CI_Controller {

    var $folder = "operasional";

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(OPERASIONAL, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'finance');
        $this->load->model('Operasional_model');
    }

    public function index() {
        $this->template->load('template/template', $this->folder . '/operasional_cost');
    }
   
    public function ajax_list() {
        //$filter = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        
        $filter_query = '';
        if(!empty($awal) && !empty($akhir)){
            $filter_query = " WHERE create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
        }
        
        //$filter_query = "WHERE pindah_paket.create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' ";
        $sql = "SELECT * FROM z_view_operational_cost ".$filter_query ;
//        echo '<pre>';
//        echo $filter;
//        die();
                //"JOIN registrasi_jamaah ON registrasi_jamaah.id_sahabat = pindah_paket.id_sahabat " . $filter_query;
        $hasil = $this->db->query($sql)->result_array();
        
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            //$aktivasi = $this->get_data_aktivasi($item['id_booking']);
            //$booking = $this->get_data_booking($item['id_booking']);
            $row = array();
            $row[] = $no;
            
            //$tgl = empty($item['tgl_pindahpaket'])? "" : date('d-m-Y', strtotime($item['tgl_pindahpaket']));
            $transfered = '';
            $row[] = '<td width="20%" >' . $item['invoice'] . '<td>';
            $row[] = '<td width="20%" >' . $item['id_user'] .'<br>'.$item['nama']. '<td>';
            $row[] = '<td width="20%" >' . $item['jml_jamaah'] . '<td>';
            $row[] = '<td width="20%" >' . $item['keterangan'] . '<td>';
            $row[] = '<td width="20%" >' . number_format($item['total_biaya']) . '<td>';
            $row[] = '<td width="20%" >' . $transfered . '<td>';
            
            $btnProcess = '';
            if($item['status']==1){
                $btnProcess = 'Unprocess';
            }elseif($item['status']==2){
                $btnProcess = '<button onclick="update_data('.$item['id_booking'].','.$item['jns_trans'].')" type="button" class="btn btn btn-danger" >Process</button>';
            }elseif($item['status']==3){
                $btnProcess = '<button onclick="detail('.$item['id_booking'].','.$item['jns_trans'].')" type="button" class="btn btn btn-primary" >Transfer</button>';
            }

//            $btnEdit = '<button onclick="edit_data(\''.$item['id'].'\')" type="button" class="btn btn-primary" ><i class="fa fa-edit"></i></button>';
//            //$btnDelete = '<button type="button" id="btn-Delete" class="btn btn-danger" onclick=':.delete_paket(.$item['id_sahabat'].).'"><i class="glyphicon glyphicon-trash"></i></button>';
//            $btnDelete = '<a class="btn btn-danger" href="javascript:void()" title="Hapus" onclick="delete_paket(' . "'" . $item['id'] . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a> ';
            $row[] = '<td width="20%" >' . $btnProcess. '</td>';

            //$row[] = ($aktivasi['create_by'] === $aktivasi['update_by']) ? $aktivasi['create_by'] : $aktivasi['create_by'].'<br>'.$aktivasi['update_by'];
            $data[] = $row;
        }
        
        $output = array(
            "data" => $data,
        );
        //print_r($output);die();
        //$this->myDebug($output);
        //output to json format
        echo json_encode($output);
    }
    
    public function update_pengajuan($id_booking, $jns_trans) {
        // if(!$this->general->privilege_check(FEE_AFFILIATE,'edit'))
        //     $this->general->no_access();
        
        $send = $this->Operasional_model->update_pengajuan($id_booking, $jns_trans);
        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('operasional_cost');
    }
    
    public function detail() {
        //echo 'test'.$id_booking, $jns_trans;

//        if (!$this->general->privilege_check(OPERASIONAL, 'edit'))
//            $this->general->no_access();
//
//        $detail = $this->Operasional_model->get_detail_data($id_booking, $jns_trans);
//        
//
//        $data = array(
//            'dana_bank' => $this->_select_bank(),
//            'bank_transfer' => $this->_select_bank(),
//            'detail' => $detail
//        );
//
//        $this->template->load('template/template', $this->folder . '/detail_operasional_cost', ($data));
    }
    
    private function _select_bank() {

        return $this->db->get('bank')->result();
    }

    function export_pindah_paket() {
        //$filter = !empty($this->input->post('filter'))? $this->input->post('filter') : 0;
        $awal = !empty($this->input->post('awal')) ? $this->input->post('awal') : '';
        $akhir = !empty($this->input->post('akhir')) ? $this->input->post('akhir') : '';

        $filter_query = '';
        if(!empty($awal) && !empty($akhir)){
            $filter_query = " WHERE create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
        }
//        if($filter==0){
//            $filter_query = " WHERE create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
//        }
        $sql = "SELECT * FROM pindah_paket ".$filter_query;
                //JOIN registrasi_jamaah ON registrasi_jamaah.id_sahabat = pindah_paket.id_sahabat " . $filter_query;

        $hasil = $this->db->query($sql)->result_array();
        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;

            $row[] = $no;
            //$tgl = $item['tgl_pindahpaket']
            $row[] = $item['id_sahabat'];
            //$row[] = $item['nama'];
            $row[] = $item['tgl_pindahpaket'];
            //$row[] = '<form method="POST">'
            //        . '<button type="submit" value="edit"><input type="hidden" na value="'.$item['id_jamaah'].'"></form>';

            $data[] = $item;
        }
        $data['list'] = $data;
        //print_r($data);
        $this->load->view('paket/pindah_paket_excel', ($data));
    }

    public function ajax_delete() {
        if (!$this->general->privilege_check(PAKET, 'remove'))
            $this->general->no_access();
       
        $id  = $this->uri->segment(3);
        $send = $this->Paket_model->delete($id);

        echo json_encode(array("status" => TRUE));
        if ($send)
            redirect('paket');
    }

    

    public function save() {
        try {
            $id_sahabat = $this->input->post('id_sahabat');
            $tgl_pindah = $this->input->post('tgl_pindah');

            $temp_data = array('id_sahabat' => $id_sahabat,
                'tgl_pindahpaket' => $tgl_pindah,
                'status' => 2,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s')
            );
            return $this->db->insert('pindah_paket', $temp_data);
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            return FALSE;
        }
    }

    public function update() {
       try {
            $id = $this->input->post('id');
            $id_sahabat = $this->input->post('id_sahabat');
            $tgl_pindah = $this->input->post('tgl_pindah');

            $temp_data = array(
                'id_sahabat' => $id_sahabat,
                'tgl_pindahpaket' => $tgl_pindah
            );
            $this->db->update('pindah_paket', $temp_data, array('id'=> $id));
            //echo 'test';
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            return FALSE;
        }
    }

    /* public function get_data(){

      $limit = $this->config->item('limit');
      $offset= $this->uri->segment(3,0);
      $q     = isset($_POST['q']) ? $_POST['q'] : '';
      $data  = $this->paket_model->get_data($offset,$limit,$q);
      $rows  = $paging = '';
      $total = $data['total'];

      if($data['data']){

      $i= $offset+1;
      $j= 1;
      foreach($data['data'] as $r){

      $rows .='<tr>';

      $rows .='<td>'.$i.'</td>';
      $rows .='<td width="10%">'.$r->id_sahabat.'</td>';
      $rows .='<td width="40%">'.$r->nama.'</td>';
      // $rows .='<td width="10%">'.$r->harga.'</td>';
      // $rows .='<td width="10%">'.$r->hari.'</td>';
      $rows .='<td width="9%">'.$r->tgl_pindahpaket.'</td>';
      $rows .='<td width="9%">'.$r->status.'</td>';
      $rows .='<td width="30%" align="center">';

      $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'product/edit/'.$r->id_product.'">
      <i class="fa fa-pencil"></i> Edit
      </a> ';
      $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_product('."'".$r->id_product."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';

      $rows .='</td>';

      $rows .='</tr>';

      ++$i;
      ++$j;
      }

      $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
      $paging .= $this->_paging($total,$limit);


      }else{

      $rows .='<tr>';
      $rows .='<td colspan="6">No Data</td>';
      $rows .='</tr>';

      }

      echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
      } */


    /* public function ajax_delete($id)
      {
      if(!$this->general->privilege_check(PAKET,'remove'))
      $this->general->no_access();
      $send = $this->paket_model->delete_by_id($id);
      echo json_encode(array("status" => TRUE));
      if($send)
      redirect('paket');
      } */

    public function delete_by_id($id) {
        $this->db->where('id_sahabat', $id);
        $this->db->delete('pindah_paket');
    }

    function get_data($id) {
        $id = $this->uri->segment(3);
        $sql = "SELECT pindah_paket.*, registrasi_jamaah.nama FROM pindah_paket "
                . "LEFT JOIN registrasi_jamaah ON registrasi_jamaah.id_sahabat = pindah_paket.id_sahabat "
                . "WHERE pindah_paket.id = '".$id."'";
        //$paket = $this->db->get_where('pindah_paket', array('id_sahabat' => $id))->row_array();
        $paket = $this->db->query($sql)->row_array();
        if (!empty($paket)) {
            $hasil = array(
                'id' => $paket['id'],
                'id_sahabat' => $paket['id_sahabat'],
                'nama' => $paket['nama'],
                'tgl_pindahpaket' => $paket['tgl_pindahpaket']
                
                );
        } else {
            $hasil = array(
                'id' =>' ',
                'id_sahabat' => '',
                'nama' => '',
                'tgl_pindahpaket' => '',
           ) ;
        }
     
        echo json_encode($hasil);
    }

}
