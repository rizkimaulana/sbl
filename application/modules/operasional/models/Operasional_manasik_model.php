<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_manasik_model extends CI_Model {

    private $_table = 't_manasik';

    public function __construct() {
        parent::__construct();
    }

    public function generate_kode($idx) {
        $today = date('ymd');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return 'MNS'. $today . $idx;
    }

    function get_data_manasik($keyword) {
        /*$sql = "SELECT a.id_registrasi, a.id_jamaah, a.nama, a.kabupaten_id, a.provinsi_id FROM `registrasi_jamaah` a
                JOIN additional_cost b ON b.id_jamaah = a.id_jamaah
                WHERE a.tipe_jamaah in ('1','9','10') and a.`status` = '1'
                and b.jns_trans = '155' and b.status = '1' LIMIT 0,5";*/
        $id_user = $this->session->userdata('id_user'); 
        /*$sql= "SELECT a.id_registrasi, a.id_jamaah, a.nama, a.kabupaten_id, a.provinsi_id, " .
                "(SELECT COUNT(b.id_registrasi) as total ".
                "FROM group_manasik as b where a.id_registrasi=b.id_registrasi) as status_claim " .
               "FROM registrasi_jamaah as a ".
               "WHERE a.tipe_jamaah in ('1','6','8','9','10') and a.`status` = '1' ".
               "AND a.id_affiliate = '".$id_user."' OR a.id_affiliate LIKE '%GM%' OR a.id_affiliate LIKE '%P%'".
               "LIMIT 0,100";*/
        /*$sql= "SELECT a.id_registrasi, a.id_jamaah, a.nama, a.kabupaten_id, a.provinsi_id, " .
            "(SELECT COUNT(b.id_registrasi) as total ".
            "FROM group_manasik as b where a.id_registrasi=b.id_registrasi) as status_claim " .
            "FROM registrasi_jamaah as a ".
            "WHERE a.tipe_jamaah in ('1','6','8','9','10') and a.`status` = '1' ".
            "AND (a.id_affiliate = '".$id_user."' OR a.id_affiliate LIKE '%GM%' OR a.id_affiliate LIKE '%P%') ".
            "AND (a.nama LIKE '%".$keyword."%' OR a.id_jamaah LIKE '%".$keyword."%')".
            "LIMIT 0,100";*/
        $sql= "SELECT a.id_registrasi, a.id_jamaah, a.nama, a.kabupaten_id, a.provinsi_id, " .
            "(SELECT COUNT(b.id_registrasi) as total ".
            "FROM group_manasik as b where a.id_registrasi=b.id_registrasi) as status_claim " .
            "FROM registrasi_jamaah as a ".
            "WHERE a.tipe_jamaah in ('1','6','8','9','10') and a.`status` = '1' ".
            "AND (a.id_affiliate LIKE '%GM%' OR a.id_affiliate LIKE '%P%') ".
            "AND (a.nama LIKE '%".$keyword."%' OR a.id_jamaah LIKE '%".$keyword."%' OR a.id_affiliate LIKE '%".$keyword."%' )".
            "LIMIT 0,100";
               
        return $this->db->query($sql)->result_array();
    }
    
    function get_data_manasik_cabang($id_user) {
        $sql= "SELECT a.id_registrasi, a.id_jamaah, a.nama, a.kabupaten_id, a.provinsi_id, " .
            "(SELECT COUNT(b.id_registrasi) as total ".
            "FROM group_manasik as b where a.id_registrasi=b.id_registrasi) as status_claim " .
            "FROM registrasi_jamaah as a ".
            "WHERE a.tipe_jamaah in ('1','6','8','9','10') and a.`status` = '1' ".
            "AND a.id_registrasi not in (SELECT id_registrasi FROM group_manasik WHERE id_affiliate='".$id_user."') ".
            "AND a.id_affiliate = '".$id_user."' ";
               
        return $this->db->query($sql)->result_array();
    }

    function save($data) {
        $id_user = $this->session->userdata('id_user');
        $post_data = array_unique($data['id_registrasi']);
        $jml_jamaah = count($post_data);
        $id_registrasi = json_encode($post_data);
        $biaya = $this->get_biaya_manasik();
        $total_biaya = $biaya['biaya_manasik'] * $jml_jamaah;
        
        $arr = array(
            'id_affiliate' => $id_user,
            'jml_jamaah' => $jml_jamaah,
            'biaya' => $total_biaya,
            'status' => '0',
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
        );
        
        //$this->db->trans_begin();
        
        $this->db->insert($this->_table, $arr);
        $id = $this->db->insert_id();
        $kd_manasik = $this->generate_kode($id);
        $this->db->update($this->_table, array('kd_manasik' => $kd_manasik), array('id' => $id));
        
//        SP MANASIK
        $p_id_user_affiliate = $id_user;
        $p_kd_manasik = $kd_manasik;
        $p_id = $id;
        $p_id_registrasi = str_replace(']','',str_replace('[','',str_replace('"','',$id_registrasi)));        
        $query ="call sp_group_manasik ('" . $p_id_user_affiliate . "','" . $p_kd_manasik . "','" . $p_id . "','" . $p_id_registrasi . "',@hasil)";
        
        $sql_query = $this->db->query($query)->row_array();
        //print_r($sql_query['@result']);die();
        if($sql_query['@result']==1) {
            //$this->db->trans_complete();
            return true;
        }else{
            //$this->db->trans_rollback();
            return false;
        }
        //print_r($sql_query['@result']);die();
        //return true;
        
        //SP MANASIK
//        if ($this->db->trans_status() === false) {
//            $this->db->trans_rollback();
//            return false;
//        } else {
//            print_r($this->db->trans_status());die();
//            $this->db->trans_complete();
//            return true;
//        }
    }
    
    function get_list_manasik() {
        $id_affiliate = $this->session->userdata('id_user');
        $sql = "SELECT a.* FROM t_manasik a ".
                "JOIN group_manasik b ON b.kd_manasik = a.kd_manasik ".
                "WHERE a.id_affiliate = '".$id_affiliate."' ".
                "GROUP BY a.kd_manasik";
        return $this->db->query($sql)->result_array();
    }
    
    function get_detail_manasik($kd_manasik){
        $sql = "SELECT a.*, b.kabupaten_id, b.provinsi_id, b.nama FROM group_manasik a ".
                "JOIN registrasi_jamaah b ON b.id_registrasi = a.id_registrasi ".
                "WHERE a.kd_manasik = '".$kd_manasik."'";
        return $this->db->query($sql)->result_array();
    }

    function get_biaya_manasik() {
        $hasil = $this->db->get_where('setting', array('id_setting' => '32'))->row_array();
        $result = array('biaya_manasik' => $hasil['key_setting']);
        return $result;
    }
    
    function delete_detail_manasik($id_registrasi){
        $this->db->where('id_registrasi', $id_registrasi);
        return $this->db->delete('group_manasik');
    }
}
