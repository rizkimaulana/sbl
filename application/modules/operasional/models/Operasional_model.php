<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operasional_model extends CI_Model {

    private $db2;
    private $t_paket = "pindah_paket";
   

    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2', TRUE);
    }
    
    function save_pindah_paket($arr)
    {
        $this->db->trans_begin();
        
        $this->db->insert_batch($this->t_paket, $arr);
        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            
            redirect('paket');
            return true;
        }
    }
    
    public function delete($id){
        
        return $this->db->delete('pindah_paket', array('id' => $id));
        //return true;
    }

    function noinvoice() {

        $out = array();
        $this->db->select('id_booking,invoice');
        $this->db->from('booking');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_booking] = $value->invoice;
            }
            return $out;
        } else {
            return array();
        }
    }

    public function generate_kode($idx) {
        $today = date('ymd');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $today . $idx;
    }

    public function save($data) {

        $arr = array(
            'id_user_affiliate' => $this->session->userdata('id_user'),
            'id_product' => $data['id_product'],
            'id_schedule' => $data['id_schedule'],
            'id_productprice' => $data['id_productprice'],
            'category' => $data['category'],
            'embarkasi' => $data['embarkasi'],
            'tgl_daftar' => date('Y-m-d H:i:s'),
            'schedule' => $data['schedule'],
            'harga' => $data['harga'],
            'create_date' => date('Y-m-d H:i:s'),
            'status_claim_fee' => $data['select_status_fee'],
            'create_by' => $this->session->userdata('id_user'),
            'kode_unik' => random_3_digit(),
            //'tempjmljamaah' => $data['jumlah_jamaah'],
            'tempjmljamaah' => $this->session->userdata('jumlah_jamaah'),
            'status_fee' => $data['status_fee'],
            'tipe_jamaah' => 1,
            'status' => 0
        );

        $this->db->trans_begin();

        $this->db->insert($this->_table, $arr);
        $id_booking = $this->db->insert_id();

        $invoice = $this->db->insert_id();

        $this->db->update($this->_table, array('invoice' => $this->generate_kode($id_booking)), array('id_booking' => $id_booking));

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            
            $sess_id_booking = array('id_booking' => $id_booking );
            $this->session->set_userdata($sess_id_booking);
            
            //redirect('registrasi_affiliate/detail/' . $id_booking . '');
            redirect('registrasi_affiliate/detail');
            return true;
        }
    }

    function cek($no_identitas) {
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM registrasi_jamaah Where no_identitas ='$no_identitas' and id_booking='$id_booking'");
        return $query;
    }

    function cek_pindah_paket($id_sahabat) {
        $id_sahabat = $this->input->post('id_sahabat');
        $query = $this->db->query("SELECT * FROM pindah_paket Where id_sahabat ='$id_sahabat' and status in('0','1')");
        return $query;
    }

    function cek_room($id_booking) {
        $id_booking = $this->input->post('id_booking');
        $query = $this->db->query("SELECT * FROM room_order Where id_booking='$id_booking'");
        return $query;
    }

    public function generate_id_jamaah($idx) {
        $today = date('ym');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $idx . $today;
    }

    public function generate_angka_unik($angkax) {
        $rand = "0123456789";

        $ret = '';

        $limit = 3;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $angkax . $rand;
    }

    public function RandUnik($panjang) {
        $pstring = "0123456789";
        $plen = strlen($pstring); // plen = 10
        for ($i = 1; $i <= $panjang; $i++) {
            $start = rand(0, $plen); // random 0-10 ... semisal dapet 10
            $unik .= substr($pstring, $start, 1);
        }
        return $unik;
    }

    public function save_registrasi($data) {


        if ($this->input->post('MyCheckBox') == '1') {
            $id_affiliate_type = $this->session->userdata('id_affiliate_type');
            $sql = "SELECT fee_pindah_paket from affiliate_type where id_affiliate_type ='$id_affiliate_type'";
            $query = $this->db->query($sql)->result_array();
            foreach ($query as $key => $value) {
                $fee_pindah_paket = $value['fee_pindah_paket'];
            }
            $arr = array(
                'id_booking' => $data['id_booking'],
                'invoice' => $data['invoice'],
                'id_schedule' => $data['id_schedule'],
                'id_affiliate' => $data['id_affiliate'],
                'id_product' => $data['id_product'],
                'embarkasi' => $data['embarkasi'],
                'id_sahabat' => $data['id_sahabat'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tanggal_lahir' => $data['tanggal_lahir'],
                'status_diri' => $data['select_statuskawin'],
                'kelamin' => $data['select_kelamin'],
                'rentang_umur' => $data['rentang_umur'],
                'no_identitas' => $data['no_identitas'],
                'provinsi_id' => $data['provinsi'],
                'kabupaten_id' => $data['kabupaten'],
                'kecamatan_id' => $data['kecamatan'],
                'alamat' => $data['alamat'],
                'telp' => $data['telp'],
                'telp_2' => $data['telp_2'],
                'email' => $data['email'],
                'ket_keberangkatan' => $data['select_status_hubungan'],
                'ahli_waris' => $data['waris'],
                'hub_waris' => $data['select_hub_ahliwaris'],
                'room_type' => 1,
                'category' => $data['category'],
                'merchandise' => $data['select_merchandise'],
                'id_bandara' => $data['id_bandara'],
                'refund' => $data['refund'],
                'handling' => $data['handling'],
                'akomodasi' => $data['akomodasi'],
                'fee' => 0,
                'fee_input' => 0,
                'harga_paket' => $data['harga'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'tipe_jamaah' => 6,
                'kode_unik' => $data['unique_digit'],
                'status_fee' => $data['status_fee'],
                'dp_angsuran' => $data['dp'],
                'angsuran' => $data['jml_cicilan'],
                'jml_angsuran' => $data['cicilan_perbulan'],
            );
            $this->db->trans_begin();
            $this->db->insert($this->_registrasi_jamaah, $arr);
            $id_registrasi = $this->db->insert_id();
            $initalpp = 'PP';
            $this->db->update($this->_registrasi_jamaah, array('id_jamaah' => $this->generate_id_jamaah($initalpp . $id_registrasi)), array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_registrasi' => $id_registrasi,
                'id_jamaah' => $this->generate_id_jamaah($initalpp . $id_registrasi),
                'id_affiliate' => $data['id_affiliate'],
                'no_pasport' => $data['no_pasport'],
                'issue_office' => $data['issue_office'],
                'isui_date' => $data['isue_date'],
                'status_identitas' => $data['status_identitas'],
                'status_kk' => $data['status_kk'],
                'status_photo' => $data['status_photo'],
                'status_pasport' => $data['status_pasport'],
                'status_vaksin' => $data['status_vaksin'],
                'hubkeluarga' => $data['hubungan'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
            );
            $this->db->insert('manifest', $arr);


            $arr = array(
                'invoice' => $data['invoice'],
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'id_schedule' => $data['id_schedule'],
                'id_product' => $data['id_product'],
                'id_affiliate' => $data['id_affiliate'],
                'sponsor' => $data['id_refrensi'],
                'fee' => $fee_pindah_paket,
                'fee_sponsor' => 0,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'status_fee' => $data['status_fee'],
                'status_free' => 0
            );

            $this->db->insert('fee', $arr);

            $status_fee = $this->input->post('status_fee');
            if ($status_fee == 4) {
                $arr = array(
                    'fee' => $fee_pindah_paket,
                );
                $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));
            } else {
                $arr = array(
                    'fee' => 0,
                );
                $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));
            }

            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status' => 0
            );
            $this->db->insert('pengiriman', $arr);


            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status_visa' => $data['select_status_visa'],
                'tgl_visa' => $data['tanggal_visa'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'status' => 0
            );
            $this->db->insert('visa', $arr);


            $arr = array(
                'id_sahabat' => $data['id_sahabat'],
                'tgl_pindahpaket' => date('Y-m-d H:i:s'),
                'status' => 0
            );
            $this->db->insert('pindah_paket', $arr);

            $sql = "call hitung_muhrim(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                $biaya_muhrim = $value['muhrim'];
            }

            $arr = array(
                'muhrim' => $biaya_muhrim,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));


            $sql = "call v_visa(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {

                $biaya_visa = $value['Total_VISA'];
            }

            $arr = array(
                'visa' => $biaya_visa,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'status_cicilan' => 1,
            );
            $this->db2->update('t_member', $arr, array('userid' => $data['id_sahabat']));


            $arr = array(
                'status' => 1,
            );
            $this->db2->update('t_cicilan', $arr, array('userid' => $data['id_sahabat']));

            $arr = array(
                'create_date' => date('Y-m-d H:i:s'),
                'tgl_daftar' => date('Y-m-d H:i:s'),
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'create_date' => date('Y-m-d H:i:s'),
                'tgl_daftar' => date('Y-m-d H:i:s'),
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_booking' => $data['id_booking']));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
                return false;
            } else {

                $this->db->trans_complete();

                return true;
            }
        } else {
            $id_affiliate_type = $this->session->userdata('id_affiliate_type');

            $arr = array(
                'id_booking' => $data['id_booking'],
                'invoice' => $data['invoice'],
                'id_schedule' => $data['id_schedule'],
                'id_affiliate' => $data['id_affiliate'],
                'id_product' => $data['id_product'],
                'embarkasi' => $data['embarkasi'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tanggal_lahir' => $data['tanggal_lahir'],
                'status_diri' => $data['select_statuskawin'],
                'kelamin' => $data['select_kelamin'],
                'rentang_umur' => $data['rentang_umur'],
                'no_identitas' => $data['no_identitas'],
                'provinsi_id' => $data['provinsi'],
                'kabupaten_id' => $data['kabupaten'],
                'kecamatan_id' => $data['kecamatan'],
                'alamat' => $data['alamat'],
                'telp' => $data['telp'],
                'telp_2' => $data['telp_2'],
                'email' => $data['email'],
                'ket_keberangkatan' => $data['select_status_hubungan'],
                'ahli_waris' => $data['waris'],
                'hub_waris' => $data['select_hub_ahliwaris'],
                'room_type' => 1,
                'category' => $data['category'],
                'merchandise' => $data['select_merchandise'],
                'id_bandara' => $data['id_bandara'],
                'refund' => $data['refund'],
                'handling' => $data['handling'],
                'akomodasi' => $data['akomodasi'],
                'fee' => $data['fee_posting_jamaah_input'],
                'fee_input' => 0,
                'harga_paket' => $data['harga'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'tipe_jamaah' => 1,
                'status_fee' => $data['status_fee'],
            );
            $this->db->trans_begin();

            $this->db->insert($this->_registrasi_jamaah, $arr);
            $id_registrasi = $this->db->insert_id();

            $this->db->update($this->_registrasi_jamaah, array('id_jamaah' => $this->generate_id_jamaah($id_registrasi)),
                    // array('harga_paket'=> $this->generate_angka_unik($harga)),
                    array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_registrasi' => $id_registrasi,
                'id_jamaah' => $this->generate_id_jamaah($id_registrasi),
                'id_affiliate' => $data['id_affiliate'],
                'no_pasport' => $data['no_pasport'],
                'issue_office' => $data['issue_office'],
                'isui_date' => $data['isue_date'],
                'status_identitas' => $data['status_identitas'],
                'status_kk' => $data['status_kk'],
                'status_photo' => $data['status_photo'],
                'status_pasport' => $data['status_pasport'],
                'status_vaksin' => $data['status_vaksin'],
                'hubkeluarga' => $data['hubungan'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
            );
            $this->db->insert('manifest', $arr);


            $arr = array(
                'invoice' => $data['invoice'],
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'id_schedule' => $data['id_schedule'],
                'id_product' => $data['id_product'],
                'id_affiliate' => $data['id_affiliate'],
                'sponsor' => $data['id_refrensi'],
                'fee' => $data['fee_posting_jamaah'],
                'fee_sponsor' => $data['fee_sponsor_posting_jamaah'],
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'status_fee' => $data['status_fee'],
                'status_free' => 0
            );

            $this->db->insert('fee', $arr);



            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status' => 0
            );
            $this->db->insert('pengiriman', $arr);


            $arr = array(
                'id_registrasi' => $id_registrasi,
                'id_booking' => $data['id_booking'],
                'status_visa' => $data['select_status_visa'],
                'tgl_visa' => $data['tanggal_visa'],
                'tgl_daftar' => date('Y-m-d H:i:s'),
                'status' => 0
            );
            $this->db->insert('visa', $arr);


            $sql = "call hitung_muhrim(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                // $id_registrasi = $value['id_registrasi'];
                $biaya_muhrim = $value['muhrim'];
            }

            $arr = array(
                'muhrim' => $biaya_muhrim,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));


            $sql = "call v_visa(?) ";
            $query = $this->db->query($sql, array('id_registrasi' => $id_registrasi))->result_array();
            foreach ($query as $key => $value) {
                // $id_registrasi = $value['id_registrasi'];
                $biaya_visa = $value['Total_VISA'];
            }

            $arr = array(
                'visa' => $biaya_visa,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'create_date' => date('Y-m-d H:i:s'),
                'tgl_daftar' => date('Y-m-d H:i:s'),
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                echo'<div class="alert alert-dismissable alert-danger"><h4>Transaction Unsuccessfull</h4></div>';
                return false;
            } else {

                $this->db->trans_complete();

                return true;
            }
        }
    }

//    public function get_pic($id_booking) {
//
//        $sql = "SELECT * from registrasi_jamaah WHERE  id_booking  = ?
//              ";
//        return $this->db->query($sql, array($id_booking))->result_array();
//    }

    public function get_pic_booking($id_booking, $id) {

        $stored_procedure = "call view_booking(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    public function get_pic_room_type($id_booking) {


        $stored_procedure = "call list_order_room_view(?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking))->result_array();
    }

    public function get_pic_booking_room($id_booking, $id) {

        $stored_procedure = "call room_order_view(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    public function get_pic_order_view($id_booking, $id) {

        $stored_procedure = "call room_order_view(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    public function get_pic_room_price($category_id) {
        $category = $this->input->post('category');
        $sql = "SELECT * from room_price where category_id = ?
         ";
        return $this->db->query($sql, array($category_id))->result_array();
    }

    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->opsi_setting] = $value->key_setting;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_biaya_room() {

        $out = array();
        $this->db->select('id_room,room,harga_idr,kapasitas');
        $this->db->from('view_get_room_price');
        // $this->db->where('room_type','2');
        // $this->db->where('category','1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->room] = $value->harga_idr;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_biaya_refund() {
        $this->db->select('*');
        $this->db->from('view_refund');
        // $this->db->where('tampil', 'Y');
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function get_allrefund() {
        $this->db->from('view_refund');
        // $this->db->from($this->tabel);
        $query = $this->db->get();

        //cek apakah ada data
        if ($query->num_rows() > 0) { //jika ada maka jalankan
            return $query->result();
        }
    }

    public function get_booking($id_booking) {

        $sql = "SELECT a.*,b.nama as koordinator, c.nama as paket FROM booking a
              LEFT JOIN affiliate b ON b.id_user = a.id_user_affiliate
              LEFT JOIN product c ON c.id_product = a.id_product
              WHERE a.id_booking = ?
              ";

        return $this->db->query($sql, array($id_booking))->row_array();
    }

    public function searchrefrensi() {

        // $segment = intval($this->uri->segment(4));
        $sql = "SELECT * from view_schedule ";
        $query = $this->db->query($sql);
        return $query;

        //   $sql = 'SELECT * from view_schedule  where id_product = '.$paket.' and BulanKeberangkatan='.$datepicker_keberangkatan.' 
        // and TahunKeberangkatan='.$datepicker_tahun_keberangkatan.' and embarkasi='.$departure.'';
        // $query = $this->db->query($sql);
        // return $query;
    }

    function searchItem($paket, $departure, $datepicker_tahun_keberangkatan, $datepicker_keberangkatan, $jumlah_jamaah) {


        $jml = $jumlah_jamaah;
        //$jumlah_jamaah = $this->input->post('jumlah_jamaah');
        $sess_jumlah_jamaah = array('jumlah_jamaah' => $jml);
        $this->session->set_userdata($sess_jumlah_jamaah);
        
        $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'";
        $query = $this->db->query($sql);

        if (empty($query->result())) {

            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong ' . $jumlah_jamaah . '</h2></td></tr>';
            exit;
        } else {


            foreach ($query->result() as $row) {
                // if ($row->status_keberangkatan == 6){ 
                //    echo'<tr>
                //            <td>'.$row->paket.'</td>
                //            <td>'.$row->departure.'</td>
                //            <td>'.$row->date_schedule.'</td>
                //            <td>'.$row->time_schedule.'</td>
                //            <td>'.$row->seats.'</td>
                //            <td>'.$row->type.'</td>
                //            <td>'.$row->category.'</td>
                //            <td>'.$row->keterangan.'</td>
                //            <td>'.$row->harga.'</td>
                //            <td><div class="btn-group"><button type="submit" formaction="'.base_url().'registrasi_affiliate/schedule/'.$row->id_schedule.'"  class="btn btn-info btn-sm" title="Pilih Waktu Keberangkatan"><i class="glyphicon glyphicon-pencil"></i></a>
                //         </a></div>
                //         </td>
                //         </tr>';
                //     }else{
                //         echo'<tr>
                //                 <td>'.$row->paket.'</td>
                //                 <td>'.$row->departure.'</td>
                //                 <td>'.$row->BulanKeberangkatan.'</td>
                //                 <td>'.$row->TahunKeberangkatan.'</td>
                //                 <td>'.$row->seats.'</td>
                //                 <td>'.$row->type.'</td>
                //                 <td>'.$row->category.'</td>
                //                 <td>'.$row->keterangan.'</td>
                //                 <td>'.$row->harga.'</td>
                //                <td><div class="btn-group"><button type="submit" formaction="'.base_url().'registrasi_affiliate/schedule/'.$row->id_schedule.'"  class="btn btn-info btn-sm" title="Pilih Waktu Keberangkatan"><i class="glyphicon glyphicon-pencil"></i></a>
                //         </a></div>
                //         </td>
                //         </tr>';
                //     }

                echo'<tr>
                   
                   <td><strong>' . $row->paket . '</strong></td>
                   <td><strong>' . $row->departure . '</strong></td>
                   <td><strong>' . $row->date_schedule . '</strong></td>
                   <td><strong>' . $row->time_schedule . '</strong></td>
                   <td><strong>' . $row->seats . '</strong></td>
                   <td><strong>' . $row->type . '</strong></td>
                   <td><strong>' . $row->category . ' - ' . $row->hari . ' hari</strong></td>
                   <td><strong>' . $row->keterangan . '</strong></td>
                   <td><strong>' . number_format($row->harga) . '</strong></td>
                    <td><strong>' . $row->maskapai . '</strong></td>
                   <td><div class="btn-group">
               
                            <a title="Pilih waktu keberangkatan" class="btn btn-info btn-sm" href="javascript:formSubmit('.$row->id_schedule.');" ><i class="glyphicon glyphicon-pencil"></i></a>
                            
                </div>
                </td>
                </tr>';
                //<button type="submit" formaction="'.base_url().'registrasi_affiliate/schedule/'.$row->id_schedule.'"  class="btn btn-info btn-sm" title="Pilih Waktu Keberangkatan"><i class="glyphicon glyphicon-pencil"></i></a>            
            }
        }
    }

    function get_keluarga() {

        $query = $this->db->get('registrasi_jamaah');
        return $query->result();
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_all_kabupaten() {
        $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }

    function get_provinsi() {

        $query = $this->db->get('wilayah_provinsi');
        return $query->result();
    }

    function get_kabupaten() {

        $query = $this->db->get('wilayah_kabupaten');
        return $query->result();
    }

    function get_kecamatan() {

        $query = $this->db->get('wilayah_kecamatan');
        return $query->result();
    }

    public function get_data($offset, $limit, $q = '') {

        $id = $this->session->userdata('id_user');
        $sql = " SELECT * from data_laporan_jamah_belumaktif where id_user_affiliate = '" . $id . " '
                    ";

        if ($q) {

            $sql .= " AND affiliate LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                     OR departure LIKE '%{$q}%'
                     OR tgl_daftar LIKE '%{$q}%'
                     OR paket LIKE '%{$q}%'
                      OR user_create LIKE '%{$q}%'
            ";
        }
        $sql .= " ORDER BY tgl_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .= " LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    function get_data_jamaah_ajax($offset, $limit, $q = '', $sort, $order) {
        $sql = "SELECT * FROM view_lap_pembayaran";
        if (is_array($q)) {
            if ($q['id_booking'] != '') {
                // $q['kode_transaksi'] = str_replace('TPJ', '', $q['kode_transaksi']);
                // $q['kode_transaksi'] = $q['kode_transaksi'] * 1;
                // $sql .=" AND (id LIKE '".$q['kode_transaksi']."' OR anggota_id LIKE '".$q['kode_transaksi']."'OR nama LIKE '%".$q['kode_transaksi']."%') ";
                $sql .= " AND nama LIKE '%" . $q['id_booking'] . "%' ";
            } else {
                if ($q['tgl_dari'] != '' && $q['tgl_sampai'] != '') {
                    $sql .= " AND DATE(create_date) >= '" . $q['tgl_dari'] . "' ";
                    $sql .= " AND DATE(create_date) <= '" . $q['tgl_sampai'] . "' ";
                }
            }
        }
        $result['count'] = $this->db->query($sql)->num_rows();
        $sql .= " ORDER BY {$sort} {$order} ";
        $sql .= " LIMIT {$offset},{$limit} ";
        $result['data'] = $this->db->query($sql)->result();
        return $result;
    }

    function get_data_bandara($pemberangkatan = '') {
        $this->db->where("pemberangkatan", $pemberangkatan);
        return $this->db->get("view_refund");
    }

    function get_nama_jamaah() {
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status', '0');
        $this->db->order_by('id_registrasi', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function get_data_jamaah($id) {
        $this->db->select('*');
        $this->db->from('view_lap_pembayaran');
        $this->db->where('id_booking', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    public function delete1($id_aff) {

        $this->db->trans_begin(); //transaction initialize

        $this->db->delete($this->table, array('id_aff' => $id_aff));
        // $this->db->delete('user',array('id_user'=>$id_user));

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            return true;
        }
    }

    public function delete_by_id($id_aff) {
        $this->db->where('id_aff', $id_aff);
        $this->db->delete($this->_table);
    }

    function lap_data_jamaah($id_booking) {

        $stored_procedure = "call faktur_jamaah(?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking))->result();
    }

    function lap_data_perjamaah($id_booking) {


        $stored_procedure = "call faktur_perjamaah(?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking))->result();
    }

    public function get_report_registrasi($id_booking, $id) {



        $stored_procedure = "call data_laporan_jamah_belumaktif_all(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
                    'create_by' => $id
                ))->row_array();
    }

    function get_all_provinsi_manasik() {
        $this->db->distinct();
        $this->db->select('provinsi_id,provinsi');
        $this->db->from('view_manasik');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_pic_keluarga($id_booking, $id) {
        $stored_procedure = "call setting_keluarga(?,?)";
        return $this->db->query($stored_procedure, array('id_booking' => $id_booking, 'create_by ' => $id
                ))->result_array();
    }

    public function update_room_order($data) {
        $id_booking = $this->input->post('id_booking');
        $category = $this->input->post('category');
        $id_product = $this->input->post('id_product');
        $room_type = $this->input->post('room_type');
        $double = $this->input->post('double');
        $triple = $this->input->post('triple');
        $pre_double = $this->input->post('pre_double');
        $pre_triple = $this->input->post('pre_triple');
        $jumlah_jamaah = $this->input->post('jumlah_jamaah');

        $sql = "SELECT * from room where category = '$category' and room_type ='2'";
        $query = $this->db->query($sql)->result_array();
        foreach ($query as $key => $value) {
            $harga_double = $value['harga_idr'];
        }

        $sql2 = "SELECT * from room where category = '$category' and room_type ='3'";
        $query = $this->db->query($sql2)->result_array();
        foreach ($query as $key => $value) {
            $harga_triple = $value['harga_idr'];
        }

        $pilihan_double = $double * 2;
        $pilihan_triple = $triple * 3;


        $total_pilihan = $pilihan_double + $pilihan_triple;
        if ($total_pilihan > $jumlah_jamaah) {
            $this->session->set_flashdata('info', "Jamaah yang anda input = " . $jumlah_jamaah . " Orang,  
          <br>Jumlah Kamar yang and Input tidak Sesuai dengan jumlah jamaah yang anda input, isi jumlah kamar sesuai jumlah jamaah yang anda input!!");
            redirect('registrasi_affiliate/room_question/' . $id_booking . '/' . $category . '');
        } else {



            $total_harga_double = $harga_double * $double;
            $total_harga_triple = $harga_triple * $triple;
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_product' => $data['id_product'],
                'category' => $data['category'],
                'room_type' => 2,
                'jumlah' => $data['double'],
                'harga' => $total_harga_double,
                'status' => 0,
                'status_room_order' => 0,
                'create_date' => date('Y-m-d H:i:s'),
                'create_by' => $this->session->userdata('id_user'),
            );

            $this->db->insert('room_order', $arr);

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_product' => $data['id_product'],
                'category' => $data['category'],
                'room_type' => 3,
                'jumlah' => $data['triple'],
                'harga' => $total_harga_triple,
                'status' => 0,
                'status_room_order' => 0,
                'create_date' => date('Y-m-d H:i:s'),
                'create_by' => $this->session->userdata('id_user'),
            );

            $this->db->insert('room_order', $arr);

            for ($i = 0; $i < $pilihan_double; $i++) {
                $arr = array(
                    'id_booking' => $data['id_booking'],
                    'category' => $data['category'],
                    'room_type' => 2,
                    'harga' => $harga_double,
                    'status' => 0,
                    'create_date' => date('Y-m-d H:i:s'),
                    'create_by' => $this->session->userdata('id_user'),
                );
                $this->db->insert('list_order_room', $arr);
            }
            for ($i = 0; $i < $pilihan_triple; $i++) {
                $arr = array(
                    'id_booking' => $data['id_booking'],
                    'category' => $data['category'],
                    'room_type' => 3,
                    'harga' => $harga_triple,
                    'status' => 0,
                    'create_date' => date('Y-m-d H:i:s'),
                    'create_by' => $this->session->userdata('id_user'),
                );
                $this->db->insert('list_order_room', $arr);
            }

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                return true;
            }
        }
    }

    public function update_room_setting($data, $data2 = '', $data3 = '') {
        $id_jamaah = $this->input->post('select_jamaah');
        $id_booking = $this->input->post('id_booking');

        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_jamaah = $pi['select_jamaah'];
                $room_order = $pi['room_order'];
                $room_type = $pi['room_type'];
                $tmp = array(
                    'id_jamaah' => $id_jamaah,
                    'room_order' => $room_order,
                        // 'room_type' => $room_type,
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('registrasi_jamaah', $pic, 'id_jamaah');
        }

        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_listorderroom = $pi['id_listorderroom'];
                $id_jamaah = $pi['select_jamaah'];

                $tmp = array(
                    'id_listorderroom' => $id_listorderroom,
                    'id_jamaah' => $id_jamaah,
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('list_order_room', $pic, 'id_listorderroom');
        }

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            return true;
        }
    }

    public function update_setting_keluarga($data, $data2 = '') {
        $id_jamaah = $this->input->post('id_jamaah');
        if (isset($data['data2'])) {

            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_jamaah = $pi['id_jamaah'];
                $tmp = array(
                    'id_jamaah' => $pi['id_jamaah'],
                    'muhrim' => $pi['muhrim'],
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('registrasi_jamaah', $pic, 'id_jamaah');
            // print_r($pic);


            $pic = array();
            foreach ($data['data2'] as $pi) {
                $id_jamaah = $pi['id_jamaah'];
                $tmp = array(
                    'id_jamaah' => $pi['id_jamaah'],
                    'hubkeluarga' => $pi['hubungan'],
                    'keluarga' => $pi['select_keluarga'],
                );

                $pic[] = $tmp;
            }

            $this->db->update_batch('manifest', $pic, 'id_jamaah');
        }


        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            return true;
        }
    }

    function get_id_group_concat_keluarga() {

        $out = array();
        $this->db->select('id_con,id_relation');
        $this->db->from('group_concat_keluarga');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_con] = $value->id_relation;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_ket_group_concat_keluarga() {

        $out = array();
        $this->db->select('ket, keterangan');
        $this->db->from('group_concat_keluarga');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->ket] = $value->keterangan;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_group_concat_id_jamaah() {

        $out = array();
        $this->db->select('id_booking, id_jamaah');
        $this->db->from('group_concat_jamaah');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_booking] = $value->id_jamaah;
            }
            return $out;
        } else {
            return array();
        }
    }

    function get_group_concat_jamaah() {

        $out = array();
        $this->db->select('id_booking, jamaah');
        $this->db->from('group_concat_jamaah');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->id_booking] = $value->jamaah;
            }
            return $out;
        } else {
            return array();
        }
    }

    function searchItem_pindahpaket($id_sahabat) {


        $id_sponsor = $this->session->userdata('id_affiliate');

        // $sql = "SELECT a.userid,a.id_titipan,b.cicilan,concat(b.dp,'000000') as dp
        //       ,if((select SUM(c.cicilan) from t_cicilan as c  where c.userid=a.userid)IS NULL,'0',(select SUM(c.cicilan) from t_cicilan as c  where c.userid=a.userid)) as jml_cicilan,
        //       b.harga as cicilan_perbulan,a.nama,a.sponsor
        //       from t_member as a , t_titipan as b
        //        WHERE   a.id_titipan = b.id_titipan and a.status_cicilan IS NULL and a.userid='$id_sahabat'
        //        GROUP BY a.userid"; 
        //   $query = $this->db2->query($sql);
        $stored_procedure = "call pindah_paket(?) ";
        $query = $this->db->query($stored_procedure, array('userid' => $id_sahabat,
        ));
        if (empty($query->result())) {

            echo'<tr><td colspan="4"><h2 style="color: #9F6000;">Maaf ! ID SAHABAT TIDAK ADA </h2></td></tr>
                <tr>
                   
                   <td><input name="nama_dp" type="text"  id="nama_dp" maxlength="0" class="form-control" required/></td>
                   <td><input name="dp" type="text"  id="dp" class="form-control" maxlength="0" required/></td>
                   <td><input name="jml_cicilan" type="text"  class="form-control" maxlength="0"  id="jml_cicilan"   required/></td>
                   <td><input name="cicilan_perbulan" type="text" class="form-control"  maxlength="0" id="cicilan_perbulan"   required /></td>
                    
                </tr>
       ';
            // echo'<tr><td colspan="4"><h2 style="color: #9F6000;">Maaf ! ID SAHABAT TIDAK ADA</h2></td></tr>';
            //       exit;
        } else {


            foreach ($query->result() as $row) {

                // echo'<tr>
                //        <td><strong>'.$row->nama.'</strong></td>
                //        <td><strong>'.$row->dp.'</strong></td>
                //        <td><strong>'.$row->jml_cicilan.'</strong></td>
                //        <td><strong>'.$row->cicilan_perbulan.'</strong></td>
                //     </tr>';
                echo'<tr>
                   
                   <td>' . $row->nama . '</strong></td>
                   <td width="15%"><input name="dp" type="text"  id="dp" class="form-control"  value=' . $row->dp . '  readonly=true/></strong></td>
                   <td><input name="jml_cicilan" type="text"  class="form-control"  id="jml_cicilan"  value=' . $row->jml_cicilan . ' readonly=true/></strong></td>
                   <td><input name="cicilan_perbulan" type="text" class="form-control" id="cicilan_perbulan"  value=' . $row->cicilan_perbulan . '  readonly=true/></strong></td>
                    
                </tr>';
            }
        }
    }
    
    /*public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT pindah_paket.*, registrasi_jamaah.nama FROM pindah_paket " .
                "JOIN registrasi_jamaah ON registrasi_jamaah.id_sahabat = pindah_paket.id_sahabat
                    ";
        
        if($q){
            
            $sql .=" AND a.kode LIKE '%{$q}%' 
            		OR a.nama LIKE '%{$q}%'
            		OR b.keterangan LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_product DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }*/
    
    public function update_pengajuan($id_booking, $jns_trans) {

        $arr = array(
            'status' => '3',
            'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user'),
        );
        $this->db->update('additional_cost', $arr, array('id_booking' => $id_booking, 'jns_trans'=> $jns_trans));
    }
    
    public function get_detail_data($id_booking, $jns_trans) {

        // $sql ="SELECT * from detail_fee_posting_sponsor WHERE  id_booking  = ? and sponsor = ?
        //       ";
        // return $this->db->query($sql,array($id_booking,$sponsor))->result_array();    
//        $stored_procedure = "call detail_fee_posting_sponsor(?,?)";
//        return $this->db->query($stored_procedure, array('id_booking' => $id_booking,
//                    'sponsor' => $sponsor
//                        )
//                )->result_array();
        
        return $this->db->query('SELECT * FROM z_view_operasional_cost_detail', array('id_booking' => $id_booking,
                    'jns_trans' => $jns_trans
                        )
                )->result_array();
    }
    
}
