<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak_jamaahkuota extends CI_Controller {
	var $folder = "kuota_admin";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(JAMAAH_KUOTA_ADMIN,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('kuotaadmin_model');
		$this->load->helper('fungsi');

		$this->load->library('terbilang');
	}


	

	function cetak($id_booking_kuota) {
		$registrasi = $this->kuotaadmin_model->lap_data_jamaah($id_booking_kuota);

		$opsi_val_arr = $this->kuotaadmin_model->get_key_val();
		foreach ($opsi_val_arr as $key => $value){
			$out[$key] = $value;
		}

		$this->load->library('Struk');
		$pdf = new Struk('P', 'mm', 'A5', true, 'UTF-8', false);
		$pdf->set_nsi_header(false);
		// $resolution = array(280, 110);
		$pdf->AddPage('P');
		$html = '
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 7pt; text-align: center;}
		</style>';
		$html .= ''.$pdf->nsi_box($text =' <table width="100%">
			<tr>
				<td colspan="2" class="h_kanan"><strong>PT SOLUSI BALAD LUMAMPAH</strong></td>
			</tr>
			<tr>
				<td width="20%"><strong>STRUK</strong>
					<hr width="500%">
				</td>
				<td class="h_kanan" width="80%">WISMA BUMI PUTRA- 5&7th Floor Jl. Asia Afrika No 141-149 Bandung</td>
			</tr>

		</table>', $width = '100%', $spacing = '0', $padding = '1', $border = '0', $align = 'left').'';
		$no =1;
		foreach ($registrasi as $row) {
			
			$html .='<table width="100%">
			<tr>
				<td width="25%"> No INVOICE </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri">#'.$row->invoice.'</td>

				<td width="30%" class="h_kiri">BANK Transfer A/N SBL</td>
			</tr>
		
			<tr>
				<td width="25%"> Tanggal Transaksi </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri">'.$row->tgl_pembelian.'</td>

				<td> BANK MANDIRI </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['MANDIRI'].'</td>
			</tr>
		
			<tr>
				<td> ID Affiliate </td>
				<td>:</td>
				<td>'.$row->id_affiliate.'</td>
				
				
				
				<td> BANK BNI </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['BNI'].'</td>
			</tr>
			<tr>
				<td> Nama </td>
				<td>:</td>
				<td>'.$row->affiliate.'</td>

				<td> BANK BCA </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['BCA'].'</td>
			</tr>
			
			<tr>
			
				<td> Jumlah Kuota </td>
				<td>:</td>
				<td>'.$row->jml_kuota.'</td>

				<td> BANK MANDIRI Syariah </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['MANDIRI_Syariah'].'</td>
			</tr>
			<tr>
				<td> Total Pembayaran </td>
				<td>:</td>
				<td>Rp. '.number_format($row->total_harga).'</td>

				<td> BANK BRI </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['BRI'].'</td>

			</tr>
			<tr>
				<td>  </td>
				<td></td>
				<td></td>

				
				<td> BANK MUAMALAT </td>
				<td width="1%">:</td>
				<td class="h_kiri">'.$out['MUAMALAT'].'</td>
			</tr>
			
			<tr>
				<td>  </td>
				<td></td>
				<td></td>
			</tr>

			
			<tr>
				<td> Terbilang </td> 
				<td>:</td>
				<td colspan="2"><strong>'.$this->terbilang->eja($row->total_harga).' RUPIAH </strong></td>


				
			</tr>
			<tr>
				
				<td> Ketentuan dan Syarat< </td> 
				<td>:</td>
				<td colspan="5"> Untuk Waktu Pembayaran Dibatasi 4jam, Apabila Pembayaran lebih dari 4jam data yang anda Registrasikan akan Terhapus Otomatis dan pembayaran lebih 4jam harus Registrasi ulang.</td>
				
				
			</tr>';
		}
		$html .= '</table> 
		';
		$pdf->nsi_html($html);
		$pdf->Output(date('Ymd_His') . '.pdf', 'I');
	} 

}

// $html .= '</table> 
// 		<p class="txt_content"></p>

// 		<p class="txt_content">Ref. '.date('Ymd').'<br> 
// 			Informasi Hubungi Call Center : 23423423
// 			<br>
// 			atau dapat diakses melalui : web
// 		</p>';
// 		$pdf->nsi_html($html);
// 		$pdf->Output(date('Ymd_His') . '.pdf', 'I');