<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rewardsponsorcash_model extends CI_Model {

    var $table = 'fee_posting_jamaah_admin_new';
    var $column_order = array(null, 'invoice', 'id_affiliate', 'affiliate', 'create_date'); //set column field database for datatable orderable
    var $column_search = array('invoice', 'id_affiliate', 'affiliate', 'create_date'); //set column field database for datatable searchable 
    var $order = array('invoice' => 'asc'); // default order 
    private $_table = "reward_dp";
    private $_primary = "id";
    private $_kuota_booking = "kuota_booking";
    private $_id_booking = "id_booking";
    private $_claim_rewardmgm = "claim_rewardmgm";
    private $_id_claim_feefree = "id_claim_feefree";

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function save($data) {

        $arr = array(
            'id_reward' => $data['id'],
            'transfer_date' => $data['transfer_date'],
            'dana_bank' => $data['dana_bank'],
            'bank_transfer' => $data['bank_transfer'],
            'nama_rek' => $data['nama_rek'],
            'no_rek' => $data['no_rek'],
            'nominal' => $data['fee'],
            'keterangan' => $data['keterangan'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user'),
            'status' => 1,
            'status_reward' => 'FEE SPONSOR JAMAAH DP',
        );

        $this->db->trans_begin(); //transaction initialize

        $this->db->insert($this->_claim_rewardmgm, $arr);
        $id_claim_reward = $this->db->insert_id();



        $id_reward = $this->input->post('id');

        $arr = array(
            'st_sponsor' => '3',
        );
        $this->db->update($this->_table, $arr, array('id' => $id_reward));

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();

            return false;
        } else {

            $this->db->trans_complete();

            return true;
        }
    }

    public function get_pic_posting($id_reward) {


        $sql = "SELECT * from reward_sponsormgm where id = '" . $id_reward . "'";
        return $this->db->query($sql)->row_array();
    }

//    public function update_pengajuan($id) {
//
//        $arr = array(
//            'st_sponsor' => '2',
//        );
//        $this->db->update($this->_table, $arr, array('id' => $id));
//    }
    
    public function update_pengajuan($id) {

        $arr = array(
            'status' => '2',
        );
        $this->db->update('reward_konven', $arr, array('id' => $id));
    }

    private function _get_datatables_query() {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_data_fee($where = "") {
        // $data = $this->db->query('select * from detail_jamaahnoaktif where id_affiliate="'.$id_user.'" '.$where);
        $data = $this->db->query('SELECT * from reward_sponsormgm ' . $where);
        return $data;
    }
    
    function get_data_reward_sponsor(){
        //$param = array('2','3');
        //$this->db->where_in('status ',$param);
        return $this->db->get_where('reward_konven', array('tipe_reward' => '255' ))->result_array();
    }

}
