<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=laporan_harian".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>NO INVOICE</th>
                    <th>TGL DAFTAR</th>
                    <th>TGL BAYAR</th>
                    <th>TGL AKTIVASI</th>
                    <th>ID AFFILIATE</th>
                    <th>NAMA JAMAAH</th>
                    <th>JML JAMAAH</th>
                    <th>TIPE JAMAAH</th>
                    <th>PAKET</th>
                    <th>TGL BERANGKAT</th>
                    <th>QUARD</th>
                    <th>TRIPLE</th>
                    <th>DOUBLE</th>
                    <th>REFUND</th>
                    <th>FEE</th>
                    <th>FEE INPUT</th>
                    <th>MUHRIM</th>
                    <th>AKOMODASI</th>
                    <th>HANDLING</th>
                    <th>VISA</th>
                    <th>HARGA</th>
                    <th>ROOM PRICE</th>
                    <th>DP BOOKING SEAT</th>
                    <th>JML ANGSURAN</th>
                    <th>ANGSURAN</th>
                    <th>DP ANGSURAN</th>
                    <th>TOTAL</th>
                    <th>KODE UNIK</th>
                    <th>CARA BAYAR</th>
                    <th>JML BAYAR</th>
                    <th>KETERANGAN</th>
                    <th>USER</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['invoice'] ?>  </td>
                        <td><?php echo $pi['tgl_daftar'] ?>  </td>
                        <td><?php echo $pi['tgl_bayar'] ?>  </td>
                        <td><?php echo $pi['tgl_aktivasi'] ?>  </td>
                        <td><?php echo $pi['id_affiliate'] ?>  </td>
                        <td><?php echo $pi['nama_affiliate'] ?>  </td>
                        <td><?php echo $pi['jumlah_jamaah'] ?>  </td>
                        <td><?php echo $pi['tipe_jamaah'] ?>  </td>
                        <td><?php echo $pi['paket'] ?>  </td>
                        <td><?php echo $pi['tgl_keberangkatan'] ?>  </td>
                        <td><?php echo $pi['quard_2'] ?>  </td>
                        <td><?php echo $pi['triple_2'] ?>  </td>
                        <td><?php echo $pi['double_2'] ?>  </td>
                        <td><?php echo $pi['refund'] ?>  </td>
                        <td><?php echo $pi['fee'] ?>  </td>
                        <td><?php echo $pi['fee_input'] ?>  </td>
                        <td><?php echo $pi['muhrim'] ?>  </td>
                        <td><?php echo $pi['akomodasi'] ?>  </td>
                        <td><?php echo $pi['handling'] ?>  </td>
                        <td><?php echo $pi['visa'] ?>  </td>
                        <td><?php echo $pi['harga'] ?>  </td>
                        <td><?php echo $pi['Room_Price_2'] ?>  </td>
                        <td><?php echo $pi['dpbooking_seats'] ?>  </td>
                        <td><?php echo $pi['jml_angsuran'] ?>  </td>
                        <td><?php echo $pi['angsuran'] ?>  </td>
                        <td><?php echo $pi['dp_angsuran'] ?>  </td>
                        <td><?php echo $pi['total'] ?>  </td>
                        <td><?php echo $pi['kode_unik'] ?>  </td>
                        <td><?php echo $pi['cara_bayar'] ?>  </td>
                        <td><?php echo $pi['nominal'] ?>  </td>
                        <td><?php echo $pi['keterangan'] ?>  </td>
                        <td><?php echo $pi['user'] ?>  </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url();?>assets/css/opensans.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/raleway.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
