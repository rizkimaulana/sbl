
<div class="page-head">
    <!-- <h2>Affiliate </h2> -->
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">BIAYA LAIN-LAIN</a></li>
        <li class="active">BIAYA AKOMODASI</li>
    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>LIST BIAYA AKOMODASI</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <select name="filter_by" id="filter_by" class="form-control">
                                <option value="payment_date">Payment Date</option>
                                <option value="invoice">ID Jamaah / Invoice</option>
                            </select>
                            <input type="hidden" id="filter" name="filter" value="">
                        </div>
                        <div id="filter_by_date">
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                        </div>
                        <div class="col-sm-4" id="filter_by_inv" hidden="hide">
                            <input type="text" class="form-control" id="search" name="search" onkeyup="search()" placeholder="Masukan Id. Jamaah / No. Invoice">
                        </div>
                        <div class="col-sm-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                        </div>

                    </div>

                    <form name="form1" id="form1" action="<?php echo base_url(); ?>laporan/export_lap_manasik" role="form" method="POST" class="form-horizontal">
                        <input type="hidden" class="form-control" name="filter_value" id="filter_value" value="" />
                        <input type="hidden" class="form-control" name="awal" id="awal" value="" />
                        <input type="hidden" class="form-control" name="akhir" id="akhir" value="" />
                        <input type="hidden" class="form-control" name="cari" id="cari" value="" />
                    </form>

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover " >
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>KD Manasik</th>
                                    <th>Invoice</th>
                                    <th>Data Affiliate</th>
                                    <th>Data Jamaah</th>
                                    <th>Bayar</th>
                                    <th>Keterangan</th>
<!--                                    <th>Action</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <!--                    <div class="pull-right">
                                            <ul class="pagination"></ul>    
                                        </div>                     -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<form id="form2" name="form2" action="<?php echo base_url(); ?>laporan/detail_lap_manasik" method="post" target="_blank" >
    <input type="hidden" name="kd_manasik" id="kd_manasik" value="">
</form>
<script>
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        //datatables
        table = $('#data-table').DataTable({
            "info": false,
            "searching": false,
            "pageLength": 10,
            "bLengthChange": false,
        });

    });

    $('#btn-filter').click(function () { //button filter event click
        var filter = document.getElementById("filter_by").value;
        var awal = document.getElementById("datepicker1").value;
        var akhir = document.getElementById("datepicker2").value;
        table.ajax.url("<?php echo site_url('laporan/ajax_lap_manasik'); ?>/" + awal + "/" + akhir);
        table.ajax.reload();  //just reload table
        $('#filter_value').val(filter);
        $('#awal').val(awal);
        $('#akhir').val(akhir);
    });

    $('#filter_by').change(function () {
        var filter = $('#filter_by').val();
        if (filter === 'payment_date') {
            $('#filter_by_date').show();
            $('#filter_by_inv').hide();
            $('#btn-filter').attr('disabled', false);
        } else {
            $('#filter_by_inv').show();
            $('#filter_by_date').hide();
            $('#btn-filter').attr('disabled', true);
            $('#filter_value').val('');
            $('#awal').val('');
            $('#akhir').val('');
        }
    });

    function search() {
        keyword = $("#search").val();
        $('#cari').val(keyword);
        if (keyword) {
            table.ajax.url("<?php echo site_url('laporan/ajax_lap_manasik_by_search') ?>/" + keyword);
            table.ajax.reload();
        }
    }

    $('#btnExcel').click(function () {
        //alert();
        $('#form1').submit();
    });

    function detail(kd_manasik) {
        $('#kd_manasik').val(kd_manasik);
        $('#form2').submit();
    }
</script>







