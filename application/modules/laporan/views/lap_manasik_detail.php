<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Biaya Manasik</b>
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kd Manasik</th>
                                    <th>Invoice</th>
                                    <th>Id Jamaah</th>
                                    <th>Nama Jamaah</th>
                                    <th>Id Affiliate</th>
                                    <th>Nama Affiliate</th>
                                    <th>Biaya</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!--    <div class="pull-right">
                         <ul class="pagination"></ul>    
                      </div>   -->
                </div>
            </div>
        </div>
    </div>    
</div>

<input type="hidden" name="kd_manasik" id="kd_manasik" value="<?php echo isset($kd_manasik)? $kd_manasik : ''; ?>">

<script type="text/javascript">
    var table;

    $(document).ready(function () {
        var kd_manasik;
        kd_manasik = $('#kd_manasik').val();

        //datatables
        table = $('#data-table').DataTable({
            //"processing": true, //Feature control the processing indicator.
            //"serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/ajax_lap_detail_manasik') ?>/" + kd_manasik,
                "type": "POST"
            },

            //Set column definition initialisation properties.
//            "columnDefs": [
//                {
//                    "targets": [0], //first column / numbering column
//                    "orderable": false, //set not orderable
//                },
//            ],

        });
    });
</script>
