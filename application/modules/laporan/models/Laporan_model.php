<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Laporan_model extends CI_Model{
    
    private function _get_datatables_query() {
        $status = array('1');
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    
    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_lap_manasik($filter){
//        $this->db->select('t_manasik.kd_manasik, group_manasik.invoice, t_manasik.id_affiliate,'.
//                          't_manasik.jml_jamaah, t_manasik.biaya as total_biaya, t_manasik.status');
//        $this->db->join('group_manasik', 'group_manasik.kd_manasik = t_manasik.kd_manasik '.
//                        'AND group_manasik.id_affiliate = t_manasik.id_affiliate');
        if(!empty($filter)){
            $this->db->where($filter);
        }
        //$this->db->group_by('t_manasik.kd_manasik');
        return $this->db->get('group_manasik')->result_array();
    }
    
    function get_lap_manasik_by_search($filter){
//        $this->db->select('t_manasik.kd_manasik, group_manasik.invoice, t_manasik.id_affiliate,'.
//                          't_manasik.jml_jamaah, t_manasik.biaya as total_biaya, t_manasik.status');
//        $this->db->join('group_manasik', 'group_manasik.kd_manasik = t_manasik.kd_manasik '.
//                        'AND group_manasik.id_affiliate = t_manasik.id_affiliate');
        if(!empty($filter)){
            $this->db->where($filter);
        }
        //$this->db->group_by('t_manasik.kd_manasik');
        $this->db->limit('200');
        return $this->db->get('group_manasik')->result_array();
    }
    
    
    function get_lap_detail_manasik($filter){
        if(!empty($filter)){
            $this->db->where($filter);
        }
        return $this->db->get('group_manasik')->result_array();
    }
}
