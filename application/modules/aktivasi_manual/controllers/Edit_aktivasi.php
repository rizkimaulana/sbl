<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_aktivasi extends CI_Controller{
	var $folder = "aktivasi_manual";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(EDIT_AKTIVASI_BY_INVOICE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('editaktivasi_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/edit_aktivasi');
		
	}
	

	

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->editaktivasi_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	               
	               
	                $rows .='<td width="5%">'.$r->invoice.'</td>';
	                $rows .='<td width="10%">'.$r->id_user_affiliate.'</td>';
	                $rows .='<td width="8%">'.$r->paket.'</td>';
	                $rows .='<td width="30%">'.$r->keterangan.'</td>';
	                $rows .='<td width="10%">'.$r->payment_date.'</td>';
	                $rows .='<td width="10%">'.$r->ket_pembayaran.'</td>';
	                $rows .='<td width="10%">'.$r->username.'</td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="EDIT AKTIVASI" class="btn btn-sm btn-primary" href="'.base_url().'aktivasi_manual/edit_aktivasi/detail/'.$r->id_booking.'">
	                            <i class="fa fa-pencil"></i> EDIT AKTIVASI
	                        </a> ';

	                // $rows .='<a title="Detail" class="btn btn-sm btn-primary" href="'.base_url().'affiliate/detail/'.$r->id_aff.'">
	                //             <i class="fa fa-pencil"></i> Detail
	                //         </a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_affiliate('."'".$r->id_aff."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	                // $rows .='<a class="btn btn-sm btn-danger" href=""'.base_url().'affiliate/ajax_delete/'.$r->id_aff.'"" title="Hapus" ><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	                              
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'aktivasi_manual/edit_aktivasi/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	

	private function _select_ket_fee(){
		$kd = array('5','2','1','0');
		$this->db->where_in('id_aktivasi_manual', $kd);
		return $this->db->get('status_aktivasi_manual')->result();
	}

	private function _select_ket_fee2(){
		return $this->db->get('status_aktivasi_manual')->result();
	}

	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	
	
	

	public function detail(){
	


	    if(!$this->general->privilege_check(EDIT_AKTIVASI_BY_INVOICE,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	    $get = $this->db->get_where('view_aktivasi_invoice',array('id_booking'=>$id_booking))->row_array();
	    $booking    = array();
	 
	        $booking = $this->editaktivasi_model->get_pic_aktivasi($id_booking);

	 
	    $detail = $this->editaktivasi_model->get_pic_edit_aktivasi($id_booking);
	    $data = array(
	    		'select_ket_fee'=>$this->_select_ket_fee(),
	    		'select_bank'=>$this->_select_bank(),
	       		 'booking'=>$booking,
	       		 'detail'=>$detail,
	       		 // 'pic_edit'=>$pic_edit,
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/update_aktivasi',array_merge($get,$data));
	    
	}


	


	public function update(){
	     

		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/bukti_pembayaran/',
                'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }
		    else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		    }
	    }
	     
		  //print_r($data);exit;
		    $send = $this->editaktivasi_model->update($data);
	                $this->session->set_flashdata('info', "User berhasil diubah.");
		 	       redirect('aktivasi_manual/edit_aktivasi');
		
	}
	

public function unlink(){
	
	    $data = $this->input->post(null,true);
	    
	    if(unlink('./assets/images/affiliate/'.$data['img'])){
	        
	        $column = substr($data['img'],0,4); //pic1,pic2 etc...
	        $this->db->update('affiliate',array($column=>''),array('id_aff'=>$data['id_aff']));
	        
	       echo json_encode(array('status'=>true)); 
	    }
	    
	}


	
	

public function _resize_affiliate($fulpat) {
        $config['source_image'] = './assets/images/affiliate/' . $fulpat;
        $config['new_image'] = './assets/images/affiliate/' . $fulpat;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 900;
        $config['height'] = 600;
        $this->image_lib->initialize($config);

        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }
	
	public function _create_thumb_affiliate($fulpet) {
        $config['source_image'] = './assets/images/affiliate/' . $fulpet;
        $config['new_image'] = './upload/produk/thumb/' . $fulpet;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 200;
        //$config['height'] = 200;
        $this->image_lib->initialize($config);

        if (!$this->image_lib->resize()) {
            echo "tum" . $this->image_lib->display_errors();
        }
    }

    function get_jabatan_id(){
		$affiliate_type = $this->input->post('affiliate_type');
		$affiliate_type = $this->affiliate_model->get_data_jabatan_id($affiliate_type);
		 if($affiliate_type->num_rows()>0){
            $affiliate_type=$affiliate_type->row_array();
            echo $affiliate_type['jabatan_id'];
           
          
        }
        
	}
}
