<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktivasi_manual extends CI_Controller{
	var $folder = "aktivasi_manual";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(AKTIVASI_MANUAL,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('aktivasimanual_model');
		$this->load->model('aktivasimanual_model','r');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/aktivasi_manual');
		
	}

	
	
public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
	               	$row[] ='<td width="20%" >'.$r->invoice.'</strong></td>';
	               	$row[] ='<td width="20%" >'.$r->id_affiliate.' </strong> </td>';

	                $row[] ='<td width="20%" >'.$r->jumlah_jamaah.' </strong> </td>';			
	               
	                $row[] .='<a title="Aktivasi by Jamaah " class="btn btn-sm btn-primary"  href="'.base_url().'aktivasi_manual/detail/'.$r->id_booking.'">
	                            </i> Aktivasi by Jamaah
	                        </a> 
	                        <a title="Aktivasi " class="btn btn-sm btn-success"  href="'.base_url().'aktivasi_manual/aktivasi/'.$r->id_booking.'">
	                            </i> Aktivasi
	                        </a> ';
	              
	              

			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	

    public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->aktivasimanual_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="10%">'.$r->invoice.'</td>';
	                $rows .='<td width="10%">'.$r->affiliate.'</td>';
	                $rows .='<td width="10%">'.$r->paket.'
	                Emabarkasi'.$r->departure.'</td>';
	                // $rows .='<td width="10%">'.$r->departure.'</td>';
	                $rows .='<td width="12%">Tgl Daftar = <br>'.$r->tgl_daftar.'
	                tgl berangkat = '.$r->tgl_keberangkatan.'</td>';
	                // $rows .='<td width="10%">'.$r->tgl_keberangkatan.'</td>';
	                $rows .='<td width="5%">'.$r->jumlah_jamaah.'</td>';
	                $rows .='<td width="10%">'.$r->user_create.'</td>';
	                $rows .='<td width="12%">Status FEE = <strong>'.$r->ket_fee.'</strong><br>
	                Status Cashback = <strong>'.$r->ket_cashback.'</strong>
	                </td>';
	                $rows .='<td width="20%" align="center">';
	                
	                $rows .='<a title="Aktivasi Individu" class="btn-sm btn-primary" href="'.base_url().'aktivasi_manual/detail/'.$r->id_booking.'">
	                             Aktivasi by Jamaah
	                        </a> ';
	                        $rows .='</br>';
	                        $rows .='</br>';
	                  $rows .='<a class="btn-sm btn-success"  title="Aktivasi"  href="'.base_url().'aktivasi_manual/aktivasi/'.$r->id_booking.'">Aktivasi</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}

	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'aktivasi_manual/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}


   
	
	private function _select_ket_fee(){
		$kd = array('5','2','1','0');
		$this->db->where_in('id_aktivasi_manual', $kd);
		return $this->db->get('status_aktivasi_manual')->result();
	}

	private function _select_ket_fee2(){
		return $this->db->get('status_aktivasi_manual')->result();
	}

	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	
	 public function detail(){
	


	    if(!$this->general->privilege_check(AKTIVASI_MANUAL,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->aktivasimanual_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	        $pic = $this->aktivasimanual_model->get_pic($id_booking);
	        // $row_datajamaah = $this->datajamaah_model->get_pic_booking ($id_booking);
		
	    }    

	    $data = array(
	    		
	        	// 'row_datajamaah' => $this->datajamaah_model->get_pic_booking ($id_booking),
	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_aktivasimanual',($data));

	}
	
	private function _select_pay_method(){
		
		return $this->db->get('pay_method')->result();
	}

	public function aktivasi(){
	


	    if(!$this->general->privilege_check(AKTIVASI_MANUAL,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(3);
	    $booking = $this->aktivasimanual_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{

	        $pic = $this->aktivasimanual_model->get_pic($id_booking);
	        // $pic_harga = $this->aktivasimanual_model->get_pic_harga($id_booking);
	        // $pic_harga_kamar = $this->aktivasimanual_model->get_pic_harga_kamar($id_booking);
	    }    

	    $data = array(
	    		'select_ket_fee'=>$this->_select_ket_fee(),
	    		'select_bank'=>$this->_select_bank(),
	    		'select_pay_method'=>$this->_select_pay_method(),
	       		 'booking'=>$booking,'pic'=>$pic,
	       		 // 'pic_harga'=>$pic_harga,
	       		 // 'pic_harga_kamar'=>$pic_harga_kamar

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/aktivasi',($data));

	}

	
	public function aktivasi_byjamaah($id_booking){
	


	    if(!$this->general->privilege_check(AKTIVASI_MANUAL,'view'))
		    $this->general->no_access();
	    
	    $id_jamaah = $this->uri->segment(3);
	    $id_booking = $this->uri->segment(4);
	    // $id_booking= $this->input->post('id_booking');
	    $booking = $this->aktivasimanual_model->get_pic_booking_jamaah($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{

	        $jamaah = $this->aktivasimanual_model->get_pic_jamaah($id_jamaah);
	    }    

	    $data = array(
	    		'select_ket_fee'=>$this->_select_ket_fee2(),
	    		'select_pay_method'=>$this->_select_pay_method(),
	    		'select_bank'=>$this->_select_bank(),
	       		 'jamaah'=>$jamaah,'booking'=>$booking

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_aktivasiperjamaah',($data));

	}


	public function save(){
	 	// $this->form_validation->set_rules('nama','nama','required|trim');
	   //jika validasi dijalankan dan benar
          
		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/bukti_pembayaran/',
                'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		   		}
	    	}
	     
	  //print_r($data);exit;
	    $send = $this->aktivasimanual_model->save($data);
                $this->session->set_flashdata('info', "Aktivasi berhasil.");
	 	       	redirect('aktivasi_manual');
		}
	

	public function save_jamaah(){
	 	// $this->form_validation->set_rules('nama','nama','required|trim');
	   //jika validasi dijalankan dan benar
          
		$data = $this->input->post(null,true);
	    
      
        $flag=0;
        $rename_file = array();
        for($i=0;$i<count($_FILES['pic']['name']);$i++){
           
            if($_FILES['pic']['name'][$i]){
               
               $rename_file[$i] = 'pic'.($i+1).'_'.$_FILES['pic']['name'][$i];
               $flag++;
            }else{
                
                $rename_file[$i] = '';
            }
        }
        

        //if files are selected
        if($flag > 0){
            
           
            $this->load->library('upload');
            $this->upload->initialize(array(
                "file_name"     => $rename_file,
                'upload_path'   => './assets/images/bukti_pembayaran/',
                'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                'max_size'      => '2000' //Max 2MB
            ));
            
            
		    if ($this->upload->do_multi_upload("pic")){
					
			    $info = $this->upload->get_multi_upload_data();
			    
			    foreach($info as $in){			
			       
			       $picx = substr($in['file_name'],0,4);
	               $data[$picx] = $in['file_name'];
	               
	            }
		    }else{
		
			    
			    $error = array('error' => $this->upload->display_errors());
                echo "Errors Occured : "; //sini aja lah
                print_r($error);
			
		   		}
	    	}
	     
	  //print_r($data);exit;
	    $send = $this->aktivasimanual_model->save_perjamaah($data);
                $this->session->set_flashdata('info', "Aktivasi berhasil.");
	 	       	redirect('aktivasi_manual');
		}



}
