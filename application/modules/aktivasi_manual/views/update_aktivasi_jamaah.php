 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});



</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_manual/edit_aktivasijamaah/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah Belum Aktif</b>
                </div>
                  <div class="panel-body">                       

                  
                    <input type="hidden" name="id_schedule" value="<?php echo $jamaah['id_schedule']?>">
                   <input type="hidden" name="id_affiliate" value="<?php echo $jamaah['id_affiliate']?>">
                   <input type="hidden" name="id_product" value="<?php echo $jamaah['id_product']?>">
                   <input type="hidden" name="id_registrasi" value="<?php echo $jamaah['id_registrasi']?>">
                   <input type="hidden" name="id_booking" value="<?php echo $jamaah['id_booking']?>">
                   
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="#<?php echo $jamaah['invoice']?>" readonly="readonly">
  
                      </div>

                       <div class="col-sm-4">
                        <label>Paket</label> 
                        <input type="text" name="paket" class="form-control" value="<?php echo $jamaah['paket']?> dengan Waktu tunggu <?php echo $jamaah['bulan_menunggu']?> Bulan " readonly="readonly" >
                            
                      </div>
                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_affiliate" class="form-control" value="<?php echo $jamaah['id_affiliate']?>" readonly="readonly" >
                           
                      </div>
                      <div class="col-sm-4">
                        <label>Nama Affiliate</label> 
                        <input type="text" name="nama_affiliate" class="form-control" value="<?php echo $jamaah['nama_affiliate']?>" readonly="readonly" >
                            
                      </div>

                       <div class="col-sm-4">
                        <label>Nama Jamaah</label> 
                        <input type="text" name="nama" class="form-control" value="<?php echo $jamaah['nama']?>" readonly="readonly" >
                            
                      </div>
                         <div class="col-sm-4">
                        <label>ID Jamaah</label> 
                        <input type="text" name="id_jamaah" class="form-control" value="<?php echo $jamaah['id_jamaah']?>" readonly="readonly" >
                            
                      </div>
                   
                    
                      <div class="col-sm-4">
                        <label>Muhrim</label> 
                        
                        <input name="muhrim" class="form-control" value="<?php echo number_format($jamaah['muhrim'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Cash Back</label> 
                        
                        <input name="refund" class="form-control" value="<?php echo number_format($jamaah['refund'])?>" readonly="readonly">
                      </div>
                      
                       <div class="col-sm-4">
                        <label>Total Bayar</label> 
                        
                        <input name="Total_Bayar" class="form-control" value="<?php echo number_format($jamaah['Total_Bayar'])?>" readonly="readonly">
                      </div>
                     
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
                       <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                     <div class="form-group">
                      <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
                      <div class="col-lg-5">
                        <textarea class="form-control" rows="5" name="ket_pembayaran" required><?php echo $jamaah['ket_pembayaran']?></textarea>
                      </div>
                    </div>


                    
                     <div class="form-group">
                      <label class="col-lg-2 control-label">Keterangan Fee</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_ket_fee">
                      <!-- <option =" ">- PILIH Keterangan Fee -</option> -->
                            <?php foreach($select_ket_fee as $sb){ 
                        
                            $selected = ($keterangan_fee == $sb->id_aktivasi_manual)  ? 'selected' :'';
                        ?>
                            
                            <option value="<?php echo $sb->id_aktivasi_manual;?>" <?php echo $selected;?>><?php echo $sb->keterangan;?></option>
                        <?php } ?>
                    </select>
                   
                  </div>
                  </div>



                <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pembayaran</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                     <?php if($detail['pic1']){?>
                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$detail['pic1']?>" target="_blank">
                              <i class="fa fa-lightbulb-o"></i> View
                          </a>
                      
                        <?php } ?>
                </div>
            </div>
             <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Mutasi Rekening</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                     <?php if($detail['pic2']){?>
                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$detail['pic2']?>" target="_blank">
                              <i class="fa fa-lightbulb-o"></i> View
                          </a>
                      
                        <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pindah Paket</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                     <?php if($detail['pic3']){?>
                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$detail['pic3']?>" target="_blank">
                              <i class="fa fa-lightbulb-o"></i> View
                          </a>
                      
                        <?php } ?>
                </div>
            </div>

             <div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_bank">
                      <!-- <option ="">- PILIH BANK TRANSFER -</option> -->
                        
                         <?php foreach($select_bank as $sb){ 
                        
                            $selected = ($bank == $sb->id)  ? 'selected' :'';
                        ?>
                            
                            <option value="<?php echo $sb->id;?>" <?php echo $selected;?>><?php echo $sb->nama;?></option>
                        <?php } ?>
                    </select>
                  </div>
                  </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">DP Angsuran </label>
                <div class="col-lg-5">
                    <input type="text" name="dp_angsuran"  class="form-control" value="<?php echo $jamaah['dp_angsuran']?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Angsuran Ke</label>
                <div class="col-lg-5">
                    <input type="text" name="angsuran"  class="form-control" value="<?php echo $jamaah['angsuran']?>" required>
                </div>
            </div>
            

            <div class="form-group">
                <label class="col-lg-2 control-label">Angsuran Perbulan</label>
                <div class="col-lg-5">
                    <input type="text" name="angsuran_bulanan"  class="form-control" value="<?php echo $jamaah['jml_angsuran']?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Nominal Pembayaran</label>
                <div class="col-lg-5">
                    <input type="text" name="nominal_pembayaran"  class="form-control" value="<?php echo $jamaah['nominal_pembayaran']?>" required>
                </div>
            </div>

            <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  value="<?php echo $jamaah['payment_date']?>"  required/>
                      </div>
                    </div>

                   <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aktivasi</button>
                 <a href="<?php echo base_url();?>aktivasi_manual/edit_aktivasijamaah" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   </div>
      
  </form>       
                
</div>
   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>
