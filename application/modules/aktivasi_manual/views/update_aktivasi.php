 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});


</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_manual/edit_aktivasi/update" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah Belum Aktif</b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                   <input type="hidden" name="id_user_affiliate" value="<?php echo $booking['id_user_affiliate']?>">
                   <input type="hidden" name="id_product" value="<?php echo $booking['id_product']?>">
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="#<?php echo $booking['invoice']?>" readonly="readonly">
  
                      </div>

                       <div class="col-sm-4">
                        <label>Paket</label> 
                        <input type="text" name="paket" class="form-control" value="<?php echo $booking['paket']?> dengan Waktu tunggu <?php echo $booking['bulan_menunggu']?> Bulan " readonly="readonly" >
                            
                      </div>

                      <div class="col-sm-4">
                        <label>Nama Affiliate</label> 
                        <input type="text" name="affiliate" class="form-control" value="<?php echo $booking['affiliate']?>" readonly="readonly" >
                            
                      </div>

                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_user_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?>" readonly="readonly" >
                           
                      </div>

                       <div class="col-sm-4">
                        <label>Emabarkasi</label> 
                        
                        <input name="departure" class="form-control" value="<?php echo $booking['departure']?>" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Jml Jamaah</label> 
                        
                        <input name="jumlah_jamaah" class="form-control" value="<?php echo $booking['jumlah_jamaah']?> Orang" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Quard</label> 
                        
                        <input name="quard" class="form-control" value="<?php echo $booking['quard_2']?> Kamar, Harga = <?php echo number_format($booking['harga_quard_2'])?>" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label>Double</label> 
                        
                        <input name="double" class="form-control" value="<?php echo $booking['double_2']?> Kamar, Harga = <?php echo number_format ($booking['harga_double_2'])?>" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label>Triple</label> 
                        
                        <input name="triple" class="form-control" value="<?php echo $booking['triple_2']?> Kamar, Harga = <?php echo number_format ($booking['harga_Triple_2'])?>" readonly="readonly">
                      </div>
                    
                      <div class="col-sm-4">
                        <label>Muhrim</label> 
                        
                        <input name="muhrim" class="form-control" value="<?php echo number_format($booking['muhrim'])?>" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label>Harga Paket</label> 
                        
                        <input name="harga" class="form-control" value="<?php echo number_format($booking['harga'])?>" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Total Bayar</label> 
                        
                        <input name="Total_Bayar" class="form-control" value="<?php echo number_format($booking['Total_Bayar'])?>" readonly="readonly">
                      </div>

            
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List AKTIVASI</b>
                </div>
                	     <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                     <div class="form-group">
	                    <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" rows="5" name="ket_pembayaran" ><?php echo $keterangan;?></textarea>
	                    </div>
	                  </div>


	                  
	                   <div class="form-group">
	                   	<label class="col-lg-2 control-label">Keterangan Fee</label>
	                   	<div class="col-lg-5">
		                <select required class="form-control" name="select_ket_fee">
		              
                          <?php foreach($select_ket_fee as $sb){ 
		                    
		                        $selected = ($keterangan_fee == $sb->id_aktivasi_manual)  ? 'selected' :'';
		                    ?>
		                        
		                        <option value="<?php echo $sb->id_aktivasi_manual;?>" <?php echo $selected;?>><?php echo $sb->keterangan;?></option>
		                    <?php } ?>

		                </select>
		               
		            	</div>
		            	</div>

		            <div class="form-group">
				        <label class="col-lg-2 control-label">Bukti Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="file" name="pic[]"  class="form-control" >
                          <?php if($detail['pic1']){?>
                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$detail['pic1']?>" target="_blank">
                              <i class="fa fa-lightbulb-o"></i> View
                          </a>
                      
                        <?php } ?>
				        </div>
				    </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Mutasi Rekening</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                       <?php if($detail['pic2']){?>
                        <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$detail['pic2']?>" target="_blank">
                            <i class="fa fa-lightbulb-o"></i> View
                        </a>
                       
                      <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pindah Paket</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                     <?php if($detail['pic3']){?>
                        <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$detail['pic3']?>" target="_blank">
                            <i class="fa fa-lightbulb-o"></i> View
                        </a>
                       
                      <?php } ?>
                </div>
            </div>


            <div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                      <div class="col-lg-5">
                   <select class="form-control" name="select_bank">
	                      	<!-- <option value=''>- Select Bank -</option> -->
		                     <?php foreach($select_bank as $sb){ 
		                    
		                        $selected = ($bank == $sb->id)  ? 'selected' :'';
		                    ?>
		                        
		                        <option value="<?php echo $sb->id;?>" <?php echo $selected;?>><?php echo $sb->nama;?></option>
		                    <?php } ?>
		                </select> 
                  </div>
                  </div>

				    <div class="form-group">
				        <label class="col-lg-2 control-label">Nominal Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="text" name="nominal_pembayaran"  class="form-control" value="<?php echo $nominal_pembayaran;?>" required>
				        </div>
				    </div>

            <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  value="<?php echo $payment_date;?>" required/>
                      </div>
                    </div>

	                 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Update Aktivasi</button>
           			 <a href="<?php echo base_url();?>aktivasi_manual/edit_aktivasi" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>

                    
          </div>
      </div>
   
      
  </form>       
                
</div>
   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>
