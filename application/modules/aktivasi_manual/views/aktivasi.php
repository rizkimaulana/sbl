 <script>
$(function(){

    $(".delete-gambar").click(function(){
    
        var img = $(this).attr("img");
        var id_registrasi  = $(this).attr("id-registrasi");
        $.ajax({
            
            url : base_url+'manifest/unlink',type:'post',dataType:'json',
            data:{img:img,id_registrasi:id_registrasi},
            success:function(res){
                
                if(res.status)
                    window.location.reload();
            },
            error:function(x,h,r){
                
                alert(r)
            }
        
        });
        
    });
});


      
</script>
 <div id="page-wrapper">
    
     
         <form action="<?php echo base_url();?>aktivasi_manual/save" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah Belum Aktif</b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                   <input type="hidden" name="id_user_affiliate" value="<?php echo $booking['id_user_affiliate']?>">
                   <input type="hidden" name="id_product" value="<?php echo $booking['id_product']?>">
                   <input type="hidden"  name="id_mgm" value="<?php echo $booking['id_mgm']?>" >
                    <input type="hidden"  name="tipe_jamaah" value="<?php echo $booking['tipe_jamaah']?>" >
                    <input type="hidden"  name="bulan_berangkat" value="<?php echo $booking['bulan_berangkat']?>" >
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="<?php echo $booking['invoice']?>" readonly="readonly">
  
                      </div>

                       <div class="col-sm-4">
                        <label>Paket</label> 
                        <input type="text" name="paket" class="form-control" value="<?php echo $booking['paket']?> dengan Waktu tunggu <?php echo $booking['bulan_menunggu']?> Bulan " readonly="readonly" >
                            
                      </div>

                      <div class="col-sm-4">
                        <label>Nama Affiliate</label> 
                        <input type="text" name="affiliate" class="form-control" value="<?php echo $booking['affiliate']?>" readonly="readonly" >
                            
                      </div>

                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_user_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?>" readonly="readonly" >
                           
                      </div>

                       <div class="col-sm-4">
                        <label>Emabarkasi</label> 
                        
                        <input name="departure" class="form-control" value="<?php echo $booking['departure']?>" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Jml Jamaah</label> 
                        
                        <input name="jumlah_jamaah" class="form-control" value="<?php echo $booking['jumlah_jamaah']?> Orang" readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                        <label>ROOM LIST</label> 
                        
                        <input name="triple" class="form-control" value="<?php echo $booking['triple_2']?> Kamar, Harga = <?php echo number_format ($booking['Room_Price_2'])?>" readonly="readonly">
                      </div>
                    
                      <div class="col-sm-4">
                        <label>Muhrim</label> 
                        
                        <input name="muhrim" id='muhrim' class="form-control" value="<?php echo number_format($booking['muhrim'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Akomodasi</label> 
                        
                        <input name="akomodasi" id='akomodasi' class="form-control" value="<?php echo number_format($booking['akomodasi'])?>" readonly="readonly">
                      </div>

                        <div class="col-sm-4">
                        <label>Handling</label> 
                        
                        <input name="handling" id='handling' class="form-control" value="<?php echo number_format($booking['handling'])?>" readonly="readonly">
                      </div>

                        <div class="col-sm-4">
                        <label>Visa</label> 
                        
                        <input name="visa" id='visa' class="form-control" value="<?php echo number_format($booking['visa'])?>" readonly="readonly">
                      </div>

                      <!--  <div class="col-sm-4">
                        <label>Room Order</label> 
                        
                        <input name="room_order" id='room_order' class="form-control" value="<?php echo number_format($booking['Room_Price_2'])?>" readonly="readonly">
                      </div> -->

                       <div class="col-sm-4">
                        <label>Angsuran Pindah Paket</label> 
                        
                        <input name="angsuran" id='angsuran' class="form-control" value="<?php echo number_format($booking['angsuran'])?>" readonly="readonly">
                      </div>


                       <div class="col-sm-4">
                        <label>DP angsuran Pindah Paket</label> 
                        
                        <input name="dp_angsuran" id='dp_angsuran' class="form-control" value="<?php echo number_format($booking['dp_angsuran'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>JML angsuran Pindah Paket</label> 
                        
                        <input name="jml_angsuran" id='jml_angsuran' class="form-control" value="<?php echo number_format($booking['jml_angsuran'])?>" readonly="readonly">
                      </div>

                        <div class="col-sm-4">
                        <label>DP BOOKING SEATS</label> 
                        
                        <input name="dpbooking_seats" id='dpbooking_seats' class="form-control" value="<?php echo number_format($booking['dpbooking_seats'])?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Fee</label> 
                        
                        <input name="fee" id='fee' class="form-control" value="<?php echo number_format($booking['fee'])?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                        <label>Harga Paket</label> 
                        
                        <input name="harga" class="form-control" value="<?php echo number_format($booking['harga'])?>" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Total Bayar</label> 
                        
                        <input name="Total_Bayar" class="form-control" value="<?php echo number_format($booking['Total_Bayar'])?>" readonly="readonly">
                      </div>
                        <div class="col-sm-4">
                        <label>status fee</label> 
                        
                       
                        <?php if($booking['status_fee']==4){?>
                          <input type='hidden' name="status_fee" class="form-control"  value="<?php echo $booking['status_fee']?>" readonly="readonly">

                           <input type='text' name="ket_fee" class="form-control"  value="SUDAH POTONG FEE" readonly="readonly">
                       <?php }else{  ?>
                         <input type='hidden' name="status_fee" class="form-control"  value="<?php echo $booking['status_fee'] + 1?>" readonly="readonly">
                         <input type='text' name="ket_fee" class="form-control"  value="BELUM POTONG FEE" readonly="readonly">
                      <?php } ?>
                      </div>
                     
                  
                         </div>
                        
            
              </div>
            </div>
            </div>
         
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>
                	     <?php if($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">  
                                    <a class="close" data-dismiss="alert">x</a>  
                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                            </div>
                        <?php } ?>
                 <div class="panel-body">
                     <div class="form-group">
	                    <label class="col-lg-2 control-label">Keterangan Pembayaran</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="ket_pembayaran" required></textarea>
	                    </div>
	                  </div>


<!-- 	                  
	                   <div class="form-group">
	                   	<label class="col-lg-2 control-label">Keterangan Fee</label>
	                   	<div class="col-lg-5">
		                <select required class="form-control" name="select_ket_fee">
		                	<option ="">- PILIH Keterangan Fee -</option>
                         <?php foreach($select_ket_fee as $sj){ 
                               $selected = ($status_claim_fee == $sj->id_aktivasi_manual)  ? 'selected' :'';?>
                              <option value="<?php echo $sj->id_aktivasi_manual;?>" <?php echo $selected;?>><?php echo $sj->keterangan;?> </option>
                          <?php } ?>

		                </select>
		               
		            	</div>
		            	</div>
 -->

             <div class="form-group">
                      <label class="col-lg-2 control-label">Metode Pembayaran</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_pay_method" id='select_pay_method'>
                      <option value="">- PILIH Metode Pembayaran -</option>
                    <?php foreach($select_pay_method as $sk){?>
                            <option value="<?php echo $sk->pay_method;?>"><?php echo $sk->pay_method;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                 </div>

        <!-- <div id="hidden_div2" style="display: none;"> -->
              <?php if ($booking['akomodasi'] > 0) { ?>      
                    <div class="form-group">
                      <label class="col-lg-2 control-label">STATUS AKOMODASI</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="status_akomodasi" id="status_akomodasi">
                      <option value="">- PILIH STATUS PEMBAYARAN AKOMODASI -</option>
                    
                            <option value="1">TIDAK LANGSUNG BAYAR</option>
                            <option value="3">LANGSUNG BAYAR</option>

                    </select>
                  </div>
                  </div>
                  <?php }else{ ?>
                    <input type='hidden'name="status_akomodasi" class="form-control"   readonly="readonly">
                  <?php } ?> 

                 <?php if ($booking['muhrim'] > 0) { ?>      
                    <div class="form-group">
                      <label class="col-lg-2 control-label">STATUS MUHRIM</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="status_muhrim" id="status_muhrim">
                      <option value="">- PILIH STATUS PEMBAYARAN MUHRIM -</option>
                    
                            <option value="1">TIDAK LANGSUNG BAYAR</option>
                            <option value="3">LANGSUNG BAYAR</option>

                    </select>
                  </div>
                  </div>
                  <?php }else{ ?>
                    <input type='hidden'name="status_muhrim" class="form-control"   readonly="readonly">
                  <?php } ?> 

                   <?php if ($booking['Room_Price_2'] > 0) { ?>      
                    <div class="form-group">
                      <label class="col-lg-2 control-label">TAMBAH ROOM</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="status_room" id="status_room">
                      <option value="">- PILIH STATUS TAMBAH ROOM -</option>
                    
                            <option value="1">TIDAK LANGSUNG BAYAR</option>
                            <option value="3">LANGSUNG BAYAR</option>

                    </select>
                  </div>
                  </div>
                  <?php }else{ ?>
                    <input type='hidden'name="status_room" class="form-control"   readonly="readonly">
                  <?php } ?> 

                    <?php if ($booking['handling'] > 0) { ?>      
                    <div class="form-group">
                      <label class="col-lg-2 control-label">STATUS HANDLING</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="status_handling" id="status_handling">
                      <option value="">- PILIH STATUS HANDLING -</option>
                    
                            <option value="1">TIDAK LANGSUNG BAYAR</option>
                            <option value="3">LANGSUNG BAYAR</option>

                    </select>
                  </div>
                  </div>
                  <?php }else{ ?>
                    <input type='hidden' name="status_handling" class="form-control"   readonly="readonly">
                  <?php } ?> 
<!-- </div> -->
<!-- 				    <div class="form-group">
				        <label class="col-lg-2 control-label">Nominal Pembayaran</label>
				        <div class="col-lg-5">
				            <input type="text" name="nominal_pembayaran" value="<?php echo $booking['Total_Bayar'] - $booking['kode_unik']?>" class="form-control" required>
				        </div>
				    </div> -->
              <div id="hidden_div" style="display: none;">
              <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pembayaran</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  id="pic1" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Mutasi Rekening</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Bukti Pindah Paket</label>
                <div class="col-lg-5">
                    <input type="file" name="pic[]"  class="form-control" >
                </div>
            </div>


            <div class="form-group">
                      <label class="col-lg-2 control-label">Bank Transfer</label>
                      <div class="col-lg-5">
                    <select required class="form-control" name="select_bank" id="select_bank">
                      <option value="">- PILIH BANK TRANSFER -</option>
                    <?php foreach($select_bank as $sk){?>
                            <option value="<?php echo $sk->nama;?>"><?php echo $sk->nama;?></option>
                        <?php } ?> 
                        

                    </select>
                  </div>
                  </div>
        </div>
            <div class="form-group">
                      <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div>

	                 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aktivasi</button>
           			 <a href="<?php echo base_url();?>aktivasi_manual" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                     
              </div>
          </div>
      </div>
   
      </div>

  </form>       
  </div>             

   
<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
    </script>

<script type="text/javascript">
      document.getElementById('select_pay_method').addEventListener('change', function () {
    var style = this.value == 'TRANSFER' ? 'block' : 'none';
    document.getElementById('hidden_div').style.display = style;
    // document.getElementById('hidden_div2').style.display = style;
    // $("#datepicker").prop('required',true);
     select_pay_method = $(this).val();

      console.log(select_pay_method);

      if(select_pay_method=='TRANSFER' ){
         $("#select_bank").prop('required',true);
        $("#pic1").prop('required',true);
       
      }else{
        $("#select_bank").prop('required',false);
         $("#pic1").prop('required',false);
         
      }
    });


  </script>
