
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        <form   class="form-horizontal" role="form"  >
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah </b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                   <input type="hidden" name="id_user_affiliate" value="<?php echo $booking['id_user_affiliate']?>">
                   <input type="hidden" name="id_product" value="<?php echo $booking['id_product']?>">
                   <input type="hidden" name="id_mgm" value="<?php echo $booking['id_mgm']?>">
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="#<?php echo $booking['invoice']?>" readonly="readonly">
  
                      </div>

                       <div class="col-sm-4">
                        <label>Paket</label> 
                        <input type="text" name="paket" class="form-control" value="<?php echo $booking['paket']?> dengan Waktu tunggu <?php echo $booking['bulan_menunggu']?> Bulan " readonly="readonly" >
                            
                      </div>

                      <div class="col-sm-4">
                        <label>Nama Affiliate</label> 
                        <input type="text" name="affiliate" class="form-control" value="<?php echo $booking['affiliate']?>" readonly="readonly" >
                            
                      </div>

                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_user_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?>" readonly="readonly" >
                           
                      </div>

                       <div class="col-sm-4">
                        <label>Emabarkasi</label> 
                        
                        <input name="departure" class="form-control" value="<?php echo $booking['departure']?>" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Jml Jamaah</label> 
                        
                        <input name="jumlah_jamaah" class="form-control" value="<?php echo $booking['jumlah_jamaah']?> Orang" readonly="readonly">
                      </div>
                       <!-- <div class="col-sm-4">
                        <label>Quard</label> 
                        
                        <input name="quard" class="form-control" value="<?php echo $booking['quard_2']?> Kamar, Harga = <?php echo number_format($booking['harga_quard_2'])?>" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label>Double</label> 
                        
                        <input name="double" class="form-control" value="<?php echo $booking['double_2']?> Kamar, Harga = <?php echo number_format ($booking['harga_double_2'])?>" readonly="readonly">
                      </div> -->
                      <div class="col-sm-4">
                        <label>Triple</label> 
                        
                        <input name="triple" class="form-control" value="<?php echo $booking['triple_2']?> Kamar, Harga = <?php echo number_format ($booking['Room_Price_2'])?>" readonly="readonly">
                      </div>
                    
                      <div class="col-sm-4">
                        <label>Muhrim</label> 
                        
                        <input name="muhrim" class="form-control" value="<?php echo number_format($booking['muhrim'])?>" readonly="readonly">
                      </div>
                      <div class="col-sm-4">
                        <label>Harga Paket</label> 
                        
                        <input name="harga" class="form-control" value="<?php echo number_format($booking['harga'])?>" readonly="readonly">
                      </div>
                       <div class="col-sm-4">
                        <label>Total Bayar</label> 
                        
                        <input name="Total_Bayar" class="form-control" value="<?php echo number_format($booking['Total_Bayar'])?>" readonly="readonly">
                      </div>
                    
            
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>

                 <div class="panel-body">
                    
                           <span class="input-group-btn">
             
                      <?php
                          echo '<a href="'.site_url().'aktivasi_manual" class="btn btn-sm btn-danger" title="Kembali"> <i class="glyphicon glyphicon-circle-arrow-left"></i> Kembali </a>
                          
                          <a href="'.site_url('data_jamaah/cetak_datajamaah').'/cetak/' . $booking['id_booking'] . '"  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank"> <i class="glyphicon glyphicon-print"></i> Cetak Detail</a>';
                        ?>
                          </span>
                         

                  
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>id jamaah</th>
                                    <th>Nama </th>
                                    <th>No Identitas </th>
                                   <th>Telp </th>
                                   <th>Status </th>
                                   <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      
                                      <td><?php echo $pi['id_jamaah']?>  </td>
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['no_identitas']?>  </td>
                                      <td><?php echo $pi['telp']?>  </td>
                                      <td><?php echo $pi['status']?>  </td>
                                        <td>
                                        <div class="btn-group">
                                            <a class="btn-sm btn-primary" title='Edit' href="<?php echo base_url();?>aktivasi_manual/aktivasi_byjamaah/<?php echo $pi['id_jamaah']; ?>/<?php echo $pi['id_booking']; ?>">Aktivasi</a>
                                   
                                         </div>
                                                    
                                  </td>
                                  </tr>

                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
