<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Editaktivasi_model extends CI_Model{
   
   
    private $_table="aktivasi";
    private $_primary="id_aktivasi";

    private $_claim_feefree="claim_feefree";
    private $_id_claim_feefree="id_claim_feefree";
    
public function update($data){
   // status_potongan_fee = Fee dan CastBack Belum diPotong    
        $status_potongan_fee= $this->input->post('select_ket_fee');
        $id_booking= $this->input->post('id_booking');
        if ($status_potongan_fee==1){
              $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi',$arr,array('id_booking'=>$data['id_booking']));  


          $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'payment_date' => $data['payment_date'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
          $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 

         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));  
    
        $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> 1,
            'status_free'=> 1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'id_claim'=>0
        );  
             
        
          $this->db->update('fee',$arr,array('id_booking'=>$data['id_booking']));  
    
       $this->db->delete('claim_feefree',array('id_booking'=>$id_booking));

        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
             'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
        
          $this->db->update('fee_input',$arr,array('id_booking'=>$data['id_booking']));  


        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
             'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_booking'=>$data['id_booking']));
    
         $id_booking = $this->input->post('$id_booking');
         $this->db->delete('transaksi',array('id_booking'=>$id_booking));
      
        $arr = array(

            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1
        );       
         $this->db->update('pengiriman',$arr,array('id_booking'=>$data['id_booking'])); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
       }elseif ($status_potongan_fee==2){


        $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
          
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
         if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi',$arr,array('id_booking'=>$data['id_booking']));  
        // $id_aktivasi =  $this->db->insert_id(); //get last insert ID
        
        $id_product = $this->input->post('id_product');

        if ($id_product==4){


        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_data
        FROM claim_feefree 
        WHERE id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data > 0){  
          $arr = array(
            // 'id_user' => $data['id_user'],
            
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 


        $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data; 
       }else{
        
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         $id_booking= $this->input->post('id_booking');
        $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data;
      
       }

     }
  }else{ 

        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_data
        FROM claim_feefree 
        WHERE id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data > 0){  
           $arr = array(
           
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => $data['payment_date'],
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 



        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee = '3',payment_date='".$data['payment_date']."',claim_date='".$data['payment_date']."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data;
       }else{
        $id_booking= $this->input->post('id_booking');
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         
        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data;
      
       }
     }
    }

          $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'payment_date' => $data['payment_date'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
          $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 

         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
             'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));  
    
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('fee_input',$arr,array('id_booking'=>$data['id_booking']));  

         $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_booking'=>$data['id_booking']));  
    
         $id_booking = $this->input->post('$id_booking');
         $this->db->delete('transaksi',array('id_booking'=>$id_booking));
      
        $arr = array(

            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1
        );       
         $this->db->update('pengiriman',$arr,array('id_booking'=>$data['id_booking']));  

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }


    }elseif ($status_potongan_fee==5) {
         $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
          
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
         if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi',$arr,array('id_booking'=>$data['id_booking']));  
        // $id_aktivasi =  $this->db->insert_id(); //get last insert ID
        
        $id_product = $this->input->post('id_product');

        if ($id_product==4){


        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_data
        FROM claim_feefree 
        WHERE id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data > 0){  
          $arr = array(
            // 'id_user' => $data['id_user'],
            
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 

        $id_booking= $this->input->post('id_booking');
        $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data; 
       }else{
        
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         $id_booking= $this->input->post('id_booking');
        $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data;
      
       }

     }
  }else{ 

        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_data
        FROM claim_feefree 
        WHERE id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data > 0){  
           $arr = array(
           
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => $data['payment_date'],

            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 



        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee = '3',payment_date='".$data['payment_date']."',claim_date='".$data['payment_date']."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data;
       }else{
        $id_booking= $this->input->post('id_booking');
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         
        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  WHERE id_booking  ='".$id_booking."' ";
        $update_data = $this->db->query($sql);
         $update_data;
      
       }
     }
    }

          $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'payment_date' => $data['payment_date'],
              'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
          $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 

         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));  
    
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
          $this->db->update('fee_input',$arr,array('id_booking'=>$data['id_booking']));  

         $arr = array(
            'payment_date' => $data['payment_date'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
          $this->db->update('list_gaji',$arr,array('id_booking'=>$data['id_booking']));  
    
         $id_booking = $this->input->post('$id_booking');
         $this->db->delete('transaksi',array('id_booking'=>$id_booking));
      
        $arr = array(


            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1
        );       
         $this->db->update('pengiriman',$arr,array('id_booking'=>$data['id_booking']));  

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }
    else{ 
        
            
        $arr = array(
              'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
          
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi',$arr,array('id_booking'=>$data['id_booking'])); 
        


          $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'payment_date' => $data['payment_date'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
          $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 

         $arr = array(
              'status_claim_fee'=>$data['select_ket_fee'],
             'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));  
    
        $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> 1,
            'status_free'=> 1,
            'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
           'id_claim'=>0
        );  
             
        
          $this->db->update('fee',$arr,array('id_booking'=>$data['id_booking']));  
    
       $this->db->delete('claim_feefree',array('id_booking'=>$id_booking));
       
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
           'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
        );  
             
        
          $this->db->update('fee_input',$arr,array('id_booking'=>$data['id_booking']));  


        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
           'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_booking'=>$data['id_booking']));
    
         $id_booking = $this->input->post('$id_booking');
         $this->db->delete('transaksi',array('id_booking'=>$id_booking));
      
        $arr = array(

            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
         $this->db->update('pengiriman',$arr,array('id_booking'=>$data['id_booking'])); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
        
       }
    }

   


   
    
    public function get_data($offset,$limit,$q=''){

          $sql = " SELECT * FROM view_aktivasi_invoice WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND id_user_affiliate LIKE '%{$q}%' 
            		OR nama LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                    OR ket_pembayaran LIKE '%{$q}%'
                    OR paket LIKE '%{$q}%'
                    OR payment_date LIKE '%{$q}%'
                    OR username LIKE '%{$q}%'
            		OR ket_pembayaran LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY create_date DESC";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    public function get_pic_aktivasi($id_booking){
 

         $sql = "SELECT * from view_booking_aktif where id_booking = {$id_booking}
                ";
        return $this->db->query($sql)->row_array();  
    }

   public function get_pic($id_booking){
    
        $sql ="SELECT * from detail_jamaahaktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    
 }

  public function get_pic_edit_aktivasi($id_booking){
    
        $sql ="SELECT * from view_aktivasi_invoice WHERE  id_booking  = {$id_booking}
              ";
        return $this->db->query($sql)->row_array();   
 }
    
    public function get_pic_aktivasi_perjamaah($id_jamaah){
 

         $sql = "SELECT * from view_aktivasi_perjamaah where id_jamaah = {$id_jamaah}
                ";
        return $this->db->query($sql)->row_array();  
    }
    
}
