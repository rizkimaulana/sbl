<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Editaktivasijamaah_model extends CI_Model{
   
   
    private $_table="aktivasi_jamaah";
    private $_primary="id_aktivasi";

    private $_claim_feefree="claim_feefree";
    private $_id_claim_feefree="id_claim_feefree";
    
    public function save_perjamaah($data){
    $status_potongan_fee= $this->input->post('select_ket_fee');
     $id_jamaah= $this->input->post('id_jamaah');

    if ($status_potongan_fee==1) {
         $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'id_jamaah'=> $data['id_jamaah'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));

      $arr = array(
          'status_claim_fee'=>$data['select_ket_fee'],
          'dp_angsuran' => 0,
          'angsuran' => 0,
          'jml_angsuran' => 0,
          'status'=>1,
          'cashback' => 2,
          'payment_date' => $data['payment_date'],
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  
      $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> 1,
            'status_free'=> 1,
            'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
           'id_claim'=>0
        );  
             
          $this->db->update('fee',$arr,array('id_registrasi'=>$data['id_registrasi'])); 

         $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> $data['status_fee'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            
        );      
       $this->db->update('fee_paketpromo',$arr,array('id_registrasi'=>$data['id_registrasi'])); 

      $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> $data['status_fee'],
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            
        );      
       $this->db->update('fee_pindahpaket',$arr,array('id_registrasi'=>$data['id_registrasi'])); 


      $id_registrasi = $this->input->post('id_registrasi');
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
              'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
          $this->db->update('fee_input',$arr,array('id_registrasi'=>$data['id_registrasi']));  

         $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));  
            $id_registrasi = $this->input->post('$id_registrasi');
         $arr = array(
            'id_registrasi' =>$id_registrasi,                   
            'id_booking' => $data['id_booking'],
            'status'=>1,
             'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi'])); 
         
       $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }
         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      
         if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }

    }elseif ($status_potongan_fee==2) {
       $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'id_jamaah'=> $data['id_jamaah'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));
        
        $id_product = $this->input->post('id_product');
        if ($id_product==4){
            $booking= $this->input->post('id_jamaah');
            $sql = "SELECT COUNT(*) as jumlah_data
                         FROM claim_feefree 
                           WHERE id_jamaah = '".$id_jamaah."'";
                    $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data == 0){  


           $arr = array(
              'id_booking'=> $data['id_booking'],
                'id_schedule'=> $data['id_schedule'],
                'id_affiliate'=> $data['id_user_affiliate'],
                'id_product'=> $data['id_product'],
                'id_jamaah'=> $data['id_jamaah'],
                'claim_date' => $data['payment_date'],
                'create_date' => date('Y-m-d H:i:s'),
                'create_by'=>$this->session->userdata('id_user'),
                'status' => 1,
                'status_feefree' => 2,
                'update_by'=>$this->session->userdata('id_user'),
                'update_date' => date('Y-m-d H:i:s'),
            );       
            
             $this->db->trans_begin(); //transaction initialize
            
            $this->db->insert($this->_claim_feefree,$arr);
            $id_claim_feefree =  $this->db->insert_id();
             $id_jamaah= $this->input->post('id_jamaah');

            $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  
                    WHERE id_registrasi ='".$id_registrasi."' ";
             $update_data = $this->db->query($sql);
             $update_data;

             
           }else{
  
           $arr = array(
                'id_booking'=> $data['id_booking'],
                'id_schedule'=> $data['id_schedule'],
                'id_affiliate'=> $data['id_user_affiliate'],
                'id_product'=> $data['id_product'],
                'id_jamaah'=> $data['id_jamaah'],
                'claim_date' => $data['payment_date'],
                'create_date' => date('Y-m-d H:i:s'),
                'create_by'=>$this->session->userdata('id_user'),
                'status' => 1,
                'status_feefree' => 2,
                'update_by'=>$this->session->userdata('id_user'),
                'update_date' => date('Y-m-d H:i:s'),
            );       
            $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 


            $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."'  
                     WHERE id_registrasi  ='".$id_registrasi."' ";
             $update_data = $this->db->query($sql);
             $update_data; 
          
           }
     }
  }else{ 

        $id_jamaah= $this->input->post('id_jamaah');
        $sql = "SELECT COUNT(*) as jumlah_data
                  FROM claim_feefree 
                  WHERE id_jamaah = '".$id_jamaah."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data == 0){  
         $id_booking= $this->input->post('id_booking');
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         
        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  
                WHERE id_registrasi  ='".$id_registrasi."' ";
        $update_data = $this->db->query($sql);
         $update_data;
       }else{

         $arr = array(
           
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'claim_date' => $data['payment_date'],
            'create_date' => $data['payment_date'],
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 

        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee = '3',payment_date='".$data['payment_date']."',claim_date='".$data['payment_date']."'  
                WHERE id_registrasi  ='".$id_registrasi."' ";
        $update_data = $this->db->query($sql);
         $update_data;

        
      
       }
     }
    }
        
          

         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'dp_angsuran' => 0,
           'angsuran' => 0,
            'jml_angsuran' => 0,
             'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_registrasi'=>$data['id_registrasi']));  
    
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('fee_input',$arr,array('id_registrasi'=>$data['id_registrasi']));  

         $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));  
    

     $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }

         $id_booking = $this->input->post('$id_booking');
         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      
        $arr = array(
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1
        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi']));  
      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }elseif ($status_potongan_fee==3) {
       $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'id_jamaah'=> $data['id_jamaah'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));
        
       
        
        
 $arr = array(
         'status_claim_fee'=>$data['select_ket_fee'],
         'dp_angsuran' => $data['dp_angsuran'],
          'angsuran' => $data['angsuran'],
          'jml_angsuran' => $data['angsuran_bulanan'],
          'status'=>1,
          'cashback'=>1,
          'payment_date' => $data['payment_date'],
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  

       
    
        // $this->db->delete('fee',array('id_registrasi'=>$id_registrasi));
        // $this->db->delete('fee_input',array('id_registrasi'=>$id_registrasi));
        // $this->db->delete('list_gaji',array('id_registrasi'=>$id_registrasi));
      $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 4,
            'status_fee'=> 4,
            'status_free'=> 4,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  

          $this->db->update('fee',$arr,array('id_registrasi'=>$data['id_registrasi']));  
    
       
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>4,
             'update_by'=>$this->session->userdata('id_user'),
             'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
        
          $this->db->update('fee_input',$arr,array('id_registrasi'=>$data['id_registrasi']));  


        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>4,
           'update_by'=>$this->session->userdata('id_user'),
          'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));
        
        $arr = array(
               
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1

        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi'])); 
       
        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }

         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      
      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }elseif ($status_potongan_fee==4) {
      $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'id_jamaah'=> $data['id_jamaah'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_booking'=>$data['id_booking']));
        

       $arr = array(
         'status_claim_fee'=>$data['select_ket_fee'],
         'dp_angsuran' => $data['dp_angsuran'],
          'angsuran' => $data['angsuran'],
          'jml_angsuran' => $data['angsuran_bulanan'],
          'status'=>1,
          'cashback'=>2,

          'payment_date' => $data['payment_date'],
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  

        // $this->db->delete('fee',array('id_registrasi'=>$id_registrasi));
        // $this->db->delete('fee_input',array('id_registrasi'=>$id_registrasi));
        // $this->db->delete('list_gaji',array('id_registrasi'=>$id_registrasi));
      $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 4,
            'status_fee'=> 4,
            'status_free'=> 4,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  

          $this->db->update('fee',$arr,array('id_registrasi'=>$data['id_registrasi']));  
    
       
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>4,
             'update_by'=>$this->session->userdata('id_user'),
             'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
        
          $this->db->update('fee_input',$arr,array('id_registrasi'=>$data['id_registrasi']));  


        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>4,
           'update_by'=>$this->session->userdata('id_user'),
          'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));
        
        $arr = array(                   
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1

        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi'])); 
       


  $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }


         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }elseif ($status_potongan_fee==5) {
       $id_jamaah= $this->input->post('id_jamaah');
   $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            'id_jamaah'=> $data['id_jamaah'],
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
         if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  
        // $id_aktivasi =  $this->db->insert_id(); //get last insert ID
        
        $id_product = $this->input->post('id_product');

        if ($id_product==4){


        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_data
        FROM claim_feefree 
        WHERE id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data > 0){  
          $arr = array(
            // 'id_user' => $data['id_user'],
            
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 

        $id_booking= $this->input->post('id_booking');
        $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."' 
                 WHERE id_registrasi  ='".$id_registrasi."' ";
        $update_data = $this->db->query($sql);
         $update_data; 
       }else{
        
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         $id_booking= $this->input->post('id_booking');
        $sql = "UPDATE fee SET status='1',status_fee = '3',status_free='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."'  
                WHERE id_registrasi ='".$id_registrasi."' ";
        $update_data = $this->db->query($sql);
         $update_data;
      
       }

     }
  }else{ 

        $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_data
        FROM claim_feefree 
        WHERE id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_data = $value['jumlah_data'];
         if ($jumlah_data > 0){  
           $arr = array(
           
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'claim_date' => $data['payment_date'],
            'create_date' => $data['payment_date'],

            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
        $this->db->update('claim_feefree',$arr,array('id_booking'=>$data['id_booking'])); 



        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee = '3',payment_date='".$data['payment_date']."',claim_date='".$data['payment_date']."' 
                 WHERE id_registrasi  ='".$id_registrasi."' ";
        $update_data = $this->db->query($sql);
         $update_data;
       }else{
        $id_booking= $this->input->post('id_booking');
        $arr = array(
          'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_affiliate'=> $data['id_user_affiliate'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'claim_date' => $data['payment_date'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by'=>$this->session->userdata('id_user'),
            'status' => 1,
            'status_feefree' => 2,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_claim_feefree,$arr);
        $id_claim_feefree =  $this->db->insert_id();
         
        $sql = "UPDATE fee SET status='1',status_free = '1',status_fee='3',claim_date='".$data['payment_date']."',payment_date='".$data['payment_date']."',id_claim='".$id_claim_feefree."' 
                 WHERE id_registrasi  ='".$id_registrasi."' ";
        $update_data = $this->db->query($sql);
         $update_data;
      
       }
     }
    }


    $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'dp_angsuran' => 0,
           'angsuran' => 0,
            'jml_angsuran' => 0,
            'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  

 

     
    
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
          $this->db->update('fee_input',$arr,array('id_registrasi'=>$data['id_registrasi']));  

         $arr = array(
            'payment_date' => $data['payment_date'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
           
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));  
    
       $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }

         $id_booking = $this->input->post('$id_booking');
         $this->db->delete('transaksi',array('id_booking'=>$id_booking));
      
        $arr = array(


            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1
        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi']));  

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }elseif ($status_potongan_fee==6) {
      $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'id_jamaah'=> $data['id_jamaah'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
            
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));
        

       $arr = array(
       'dp_angsuran' => 0,
           'angsuran' => 0,
            'jml_angsuran' => 0,
          'status'=>1,
          'cashback'=>3,
          'payment_date' => $data['payment_date'],
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  

        // $this->db->delete('fee',array('id_registrasi'=>$id_registrasi));
        // $this->db->delete('fee_input',array('id_registrasi'=>$id_registrasi));
        // $this->db->delete('list_gaji',array('id_registrasi'=>$id_registrasi));
     
        $arr = array(                   
           'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'status'=>1

        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi'])); 
       


  $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }



         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }else{
       $arr = array(
            'id_booking'=> $data['id_booking'],
            'id_schedule'=> $data['id_schedule'],
            'id_product'=> $data['id_product'],
            'id_jamaah'=> $data['id_jamaah'],
            'id_user_affiliate'=> $data['id_user_affiliate'],
            'keterangan_fee' => $data['select_ket_fee'],
            'keterangan' => $data['ket_pembayaran'],
            'bank' => $data['select_bank'],
          
            'nominal_pembayaran' => $data['nominal_pembayaran'],
            'payment_date' => $data['payment_date'],
            'update_date' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user')
        );       
        
        if(isset($data['pic1'])){
            
                $arr['pic1'] = $data['pic1'];
            }
            if(isset($data['pic2'])){
                
                $arr['pic2'] = $data['pic2'];
            }
            if(isset($data['pic3'])){
                
                $arr['pic3'] = $data['pic3'];
            }
        $this->db->update('aktivasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah'])); 
        


         $arr = array(
              'status_claim_fee'=>$data['select_ket_fee'],
             'status'=>1,
            'payment_date' => $data['payment_date'],
            'cashback' => 1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  

        
          $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));  
    
        $arr = array(
            'payment_date' => $data['payment_date'],
            'status' => 1,
            'status_fee'=> 1,
            'status_free'=> 1,
            'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
           'id_claim'=>0
        );  
             
        
          $this->db->update('fee',$arr,array('id_registrasi'=>$data['id_registrasi']));  
    
       $this->db->delete('claim_feefree',array('id_booking'=>$id_booking));
       
        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
           'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
        );  
             
        
          $this->db->update('fee_input',$arr,array('id_registrasi'=>$data['id_registrasi']));  


        $arr = array(
            'payment_date' => $data['payment_date'],
             'status'=>1,
           'update_by'=>$this->session->userdata('id_user'),
           'update_date' => date('Y-m-d H:i:s'),
        );  
             
          $this->db->update('list_gaji',$arr,array('id_registrasi'=>$data['id_registrasi']));
    
         $id_booking = $this->input->post('$id_booking');


 $id_booking= $this->input->post('id_booking');
        $sql = "SELECT COUNT(*) as jumlah_jamaah 
        FROM registrasi_jamaah 
        WHERE status='0' and id_booking = '".$id_booking."'";
        $query = $this->db->query($sql)->result_array();
      foreach($query as $key=>$value){
         $jumlah_jamaah = $value['jumlah_jamaah'];

         if ($jumlah_jamaah == 0){  
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );    
         $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking'])); 
       }else{
         $arr = array(
            'status_claim_fee'=>$data['select_ket_fee'],
            'status'=>0,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );  
        $this->db->update('booking',$arr,array('id_booking'=>$data['id_booking']));   
       }
     }

         $this->db->delete('transaksi_jamaah',array('id_registrasi'=>$id_registrasi));
      
        $arr = array(

            'status'=>1,
            'update_by'=>$this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
        );       
         $this->db->update('pengiriman',$arr,array('id_registrasi'=>$data['id_registrasi'])); 

        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
           
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            
            return true;
        }
    }
  }
   


   
    
    public function get_data($offset,$limit,$q=''){

          $sql = " SELECT * FROM view_aktivasi_perjamaah WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND username LIKE '%{$q}%' 
            		OR nama LIKE '%{$q}%'
                    OR invoice LIKE '%{$q}%'
                    OR ket_pembayaran LIKE '%{$q}%'
                    OR paket LIKE '%{$q}%'
                    OR payment_date LIKE '%{$q}%'
                    OR id_jamaah LIKE '%{$q}%'
            		";
        }
        $sql .=" ORDER BY update_date DESC";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

    public function get_pic_aktivasi($id_jamaah){
 

         $sql = "SELECT * from detail_jamaahaktif where id_jamaah = {$id_jamaah}
                ";
        return $this->db->query($sql)->row_array();  
    }

   public function get_pic($id_jamaah){
    
        $sql ="SELECT * from detail_jamaahaktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql,array($id_booking))->result_array();    
 }

 //  public function get_pic_edit_aktivasi($id_jamaah){
    
 //        $sql ="SELECT * from view_aktivasi_invoice WHERE  id_booking  = {$id_jamaah}
 //              ";
 //        return $this->db->query($sql)->row_array();   
 // }
  
    public function get_pic_aktivasi_perjamaah($id_jamaah){
 

         $sql = "SELECT * from view_aktivasi_perjamaah where id_jamaah = {$id_jamaah}
                ";
        return $this->db->query($sql)->row_array();  
    }
    
}
