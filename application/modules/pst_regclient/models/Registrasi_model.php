<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi_model extends CI_Model {
    var $_table_anggota = 'pst_anggota';
    var $_table_aktivasi = 'pst_aktivasi';
    var $_table_affiliate = 'affiliate';
    var $_table_referensi_member = 't_refrensi_member';
    
    public function generate_kode($idx) {
        $today = date('md');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return $idx . $today;
    }
    
    function simpan($data, $affiliate) {
        if (!empty($data['id'])) {
            $hasil = $this->db->update($this->_table_anggota, $data, array('id' => $data['id']));
        } else {
            $this->db->trans_begin();
            // 1. Insert ke tabel PST_ANGGOTA
            $hasil = $this->db->insert($this->_table_anggota, $data);
            $id = $this->db->insert_id();
            $initial = date('ym');
            
            // 2. Insert ke tabel PST_AKTIVASI (jika kelompok Jamaah)
            if ($data['kelompok'] == 'JAMAAH') {
                $kode_unik = random_3_aktivasi_jamaah();
                $aktivasi = array(
                    'pst_anggota_id' => $id,
                    'kode_unik' => $kode_unik,
                    'nominal' => 1000000,
                    'status' => 'DAFTAR'
                );
                $this->db->insert($this->_table_aktivasi, $aktivasi);
                $id_aktivasi = $this->db->insert_id();
                
                $initial = 'JAM';
                $this->db->update($this->_table_anggota, array('userid' => $id_user), array('id' => $id));
            }

            // 3. Insert ke tabel AFFILIATE di DATABASE KONVEN
            if (!empty($affiliate)) {
                $get_initial = $this->db->select('affiliate_code')
                    ->where('id_affiliate_type', $affiliate['id_affiliate_type'])->get('affiliate_type')->row();

                $initial = ($get_initial) ? $get_initial->affiliate_code : '';
                $this->db->insert($this->_table_affiliate, $affiliate);
                $id_aff = $this->db->insert_id();
                
                // Update tabel AFFILIATE untuk generate ID_User dan Username sesuai dengan
                $id_user = $this->generate_kode($initial . $id_aff);
                $this->db->update($this->_table_affiliate, 
                    array('id_user' => $id_user, 'username' => $id_user),
                    array('id_aff' => $id_aff));

                // Update table PST_ANGGOTA relasi ke tabel AFFILIATE
                $this->db->update($this->_table_anggota, 
                    array('userid' => $id_user, 'affiliate_id_aff' => $id_aff), 
                    array('id' => $id));
                
                //4. INSERT ke table t_referensi_member
                $arr_ref = array(
                    'id_user' => $id_user,
                    'sponsor' => $affiliate['sponsor'],
                    'id_affiliate_type' => $affiliate['id_affiliate_type'],
                    'nama' => $affiliate['nama'],
                    'telp' => $affiliate['telp'],
                    'alamat' => $affiliate['alamat'],
                    'provinsi' => $data['provinsi'],
                    'kabupaten' => $data['kabupaten'],
                    'kecamatan' => ''
                );
                $this->db->insert($this->_table_referensi_member, $arr_ref);
            }

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_complete();
                
                // 4. Send SMS ke user terkait login dan password (jika bukan kelompok Jamaah)
//                if ($data['kelompok'] != 'JAMAAH') {
//                    $sql = "SELECT * from affiliate WHERE  id_user = '" . $id_user . "'";
//                    $query = $this->db->query($sql)->result_array();
//                    foreach ($query as $key => $value) {
//                        $notlp = str_replace(' ', '', $value['telp']);
//                        $nama = $value['nama'];
//                        $username = $value['id_user'];
//                        $pass = $value['ket_upgrade'];
//                    }
//                    $return = '0';
//                    $api_element = '/api/web/send/';
//                    $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=" . $notlp . "&message=" . $data['kelompok'] . "+" . str_replace(' ', '+', $nama) . "+telah+telah+terdaftar+di+program+pesantren+sbl+username+" . $username . "+password+" . $pass . "+login+di+office.sbl.co.id";
//                    $smsgatewaydata = $api_params;
//                    $url = $smsgatewaydata;
//                    $ch = curl_init();
//                    curl_setopt($ch, CURLOPT_POST, false);
//                    curl_setopt($ch, CURLOPT_URL, $url);
//                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                    $output = curl_exec($ch);
//                    curl_close($ch);
//                    if (!$output) {
//                        $output = file_get_contents($smsgatewaydata);
//                    }
//                    if ($return == '1') {
//                        return $output;
//                    }
//                }
                return true;
            }
        }
        return $hasil;
    }
    
    function get_anggota($userid){
        return $this->db->get_where($this->_table_anggota, array('userid' => $userid))->row_array();
    }
    
    function get_id_anggota($userid){
        return $this->db->get_where($this->_table_anggota, array('userid' => $userid))->row_array();
    }
    
    function get_data($id){
        $this->db->select('*');
        $this->db->where('parent_userid', $id);
        return $this->db->get($this->_table_anggota)->result_array(); 
    }
    
    function get_data_parent($id){
        return $this->db->get_where($this->_table_anggota, array('userid' => $id))->row_array();
    }
            
    function get_data_edit($id){
        if(!empty($id)){
            $this->db->where('id', $id);
        }
        return $this->db->get($this->_table_anggota)->row_array();
    }
    
    function search_kyai($keyword){
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%'.$keyword.'%');
        $this->db->where('kelompok', 'KYAI');
        return $this->db->get($this->_table_anggota)->result_array();
    }
    
    function search_ustadz($keyword){
        $this->db->select('CONCAT(userid," - ", nama_lengkap) as nama, userid, id');
        $this->db->where('CONCAT(userid, nama_lengkap) LIKE ', '%'.$keyword.'%');
        $this->db->where('kelompok', 'USTADZ');
        return $this->db->get($this->_table_anggota)->result_array();
    }
    
    function delete($id){
        $data = array('status' => 'HAPUS');
        return $this->db->update($this->_table_anggota, $data, array('id' => $id));
    }
    
    function get_data_prov($id){
        if(!empty($id)){
            $this->db->where('provinsi_id', $id);
        }
        $hasil = $this->db->get('wilayah_provinsi')->row_array();
        $result = array(
            'provinsi' => !empty($hasil['nama']) ? $hasil['nama'] : ''
        );
        
        return $result;
        
    }
}
?>