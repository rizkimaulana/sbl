<style>
    .box {border:2px solid #0094ff;margin:auto;font-size:12px;}
    .box h3 {background:#0094ff;color:white;padding:10px;}
    .box p {color:#333;padding:5px;}
    .box {
        -moz-border-radius-topright:5px;
        -moz-border-radius-topleft:5px;
        -webkit-border-top-right-radius:5px;
        -webkit-border-top-left-radius:5px;
    }
    .table {color:#424242;font-size:12px;}
</style>
<body style="font-family: sans-serif;"  >

    <div class="" align="center">
        <br><br>
        <div style="border:1px solid #000; width: 50%">
            <br />
            <table class="table" width="640" cellspacing="0" cellpadding="4" >
                <tr>
                    <td colspan="2" align="center"><h3>INVOICE</h3></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><hr></td>
                </tr>
                <tr>
                    <td width="50%"><p>KODE TRANSAKSI : <?php echo $kd_transaksi;?> </p></td>


                    <td align="right" width="50%"><p><b>Date </b> : <?php echo date('d-m-Y'); ?></p></td>

                </tr>

                <tr>
                    <td width="50%">
                        <p>
                            <?php 
                                echo '<b>'.$pst_anggota_userid.'</b><br>';
                                echo $nama.'<br><br>'; 
                                echo $alamat.'<br>'; 
                            ?>
                        </p>
                    </td>
                    <td align="right" width="50%">
                        <p>

                            <?php 
                                $bank_transfer = !empty($bank_transfer) ? $bank_transfer : ' - ';
                                echo 'Invoice# : <b>'.$id.'</b><br>';
                                echo 'Payment Method: '.$pay_method.'<br><br>'; 
                                echo 'Bank Transfer: '.$bank_transfer.'<br>'; 
                            ?>
                        </p>
                    </td>

                </tr>
                <tr height="35" style="background-color: #CFD8DC;">
                    <td width="50%" align="center" ><b>Keterangan</b></td>
                    <td width="50%" align="right" ><b>Jumlah Biaya</b></td>
                </tr>
                <tr>
                    <td width="50%">
                        <p>
                            <?php 
                                echo 'BIAYA REGISTRASI JAMAAH'.'<br>';

                            ?>
                        </p>
                    </td>
                    <td align="right" width="50%">
                        <p>

                            <?php 

                                echo 'Rp. '.number_format($nominal).'<br>'; 

                            ?>
                        </p>
                    </td>

                </tr
                <tr>
                    <td width="50%">
                        <p>

                        </p>
                    </td>
                    <td align="right" width="50%">
                        <p>

                            <?php 

                                echo '<b>Total Bayar : Rp. '.number_format($nominal).'</b><br>'; 

                            ?>
                        </p>
                    </td>

                </tr>
            </table>
            <br />
        </div>
        <br />
        <div style="width: 50%" align='right'>
            <a href="<?php echo base_url();?>pst_regclient/registrasi/register_jamaah">Back</a>
        </div>
    </div>
    
</body>

