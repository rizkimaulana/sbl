<div class="page-head">
    <h2>Registrasi Ustadz </h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Pesantren</a></li>
        <li class="active">Regstrasi Ustadz</li>
    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Register Ustadz</h3>
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert alert-danger">  
                        <a class="close" data-dismiss="alert">x</a>  
                        <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                    </div>
                <?php } ?>
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-body">  
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nama Lengkap</label>
                                <div class="col-md-5">
                                    <input type="hidden" name="id_affiliate_type" id="id_affiliate_type" value="8">
                                    <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" onblur="cek_nama()" required="">
                                </div>
                                <div class="col-md-5">
                                    <label style="color:orange" id="hasil_cek"></label>
                                </div>
                            </div>                            
                            <!--<div class="form-group">
                                <label class="col-lg-2 control-label">Kyai</label>
                                <div class="col-md-5">
                                    <input type="search" name="autocomplete" id="autocomplete1" class="form-control autocomplete" tabindex="1" required>
                                    <input type="hidden" name="kyai_id" id="kyai_id" class="form-control" required="">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tempat Lahir</label>
                                <div class="col-md-5">
                                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Tanggal Lahir</label>
                                <div class="col-lg-5">
                                    <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control" placeholder="yyyy-mm-dd" data-mask='date'>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Jenis Kelamin</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                        <option value="">- Pilih Jenis Kelamin -</option>
                                    </select>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Status Pernikahan</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="status_nikah" name="status_nikah">
                                        <option value="">- Pilih Status Nikah -</option>
                                        <option value="BELUM MENIKAH">Belum Menikah</option>
                                        <option value="SUDAH MENIKAH">Sudah Menikah</option>
                                        <option value="DUDA/JANDA">Duda/Janda</option>
                                    </select>   
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="col-lg-2 control-label" >Hubungan Keluarga</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="hubwaris" name="hubwaris">
                                        <option value="">-Pilih Hubungan Keluarga -</option>

                                    </select>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Ahli Waris</label>
                                <div class="col-lg-5">
                                    <input type="text" name="waris" id="waris" class="form-control" >
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Alamat</label>
                                <div class="col-lg-5">
                                    <textarea name="alamat" id="alamat" rows="3"  class="form-control" ></textarea>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="col-lg-2 control-label" >Alamat Kantor</label>
                                <div class="col-lg-5">
                                    <textarea name="alamat_kantor" id="alamat_kantor" rows="3"  class="form-control" ></textarea>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Kode Pos</label>
                                <div class="col-lg-5">
                                    <input type="text" name="kode_pos" id="kode_pos" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Provinsi</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="provinsi" name="provinsi">
                                        <option value="">- Select Provinsi -</option>
                                    </select>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Kabupaten/Kota</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="kabupaten" name="kabupaten">
                                        <option value="">- Select Kabupaten/Kota -</option>
                                    </select>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Kecamatan</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="kecamatan" name="kecamatan">
                                        <option value="">- Select Kecamatan -</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Nomor Telepon</label>
                                <div class="col-lg-5">
                                    <input type="text" name="no_telp" id="no_telp" class="form-control" data-mask="phone-int">
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="col-lg-2 control-label" >Alamat Email</label>
                                <div class="col-lg-5">
                                    <input type="email" name="email" id="email" class="form-control" >
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >No. Identitas</label>
                                <div class="col-lg-5">
                                    <input type="text" name="no_identitas" id="no_identitas" class="form-control" >
                                </div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >NPWP</label>
                                <div class="col-lg-5">
                                    <input type="text" name="npwp" id="npwp" class="form-control" >
                                </div>
                            </div>
                            -->
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >BANK</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="bank" name="bank">
                                        <option value="">Bank</option>
                                    </select>   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >No. Rekening</label>
                                <div class="col-lg-5">
                                    <input type="text" name="no_rek" id="no_rek" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Nama Rekening</label>
                                <div class="col-lg-5">
                                    <input type="text" name="nama_rek" id="nama_rek" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Password</label>
                                <div class="col-lg-5">
                                    <input type="password" name="password" id="password" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" >Foto</label>
                                <div class="col-lg-5">
                                    <input type="file" name="pic[]"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-5">
                                    <label class="col-lg-5 control-label" ></label>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                                    <a href="<?php echo base_url(); ?>pst_registrasi/registrasi/register_kyai" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>  
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>

<script type='text/javascript'>
    var site = "<?php echo base_url(); ?>";
    $(function () {
        $('#provinsi').change(function () {
            var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_kab') ?>/" + $(this).val();
            $('#kabupaten').load(url);
        });

        $('#kabupaten').change(function () {
            var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_kec') ?>/" + $(this).val();
            $('#kecamatan').load(url);
        });
    });

    $(document).ready(function () {
        var url = "<?php echo base_url('pst_regclient/registrasi/ajax_jenis_kelamin') ?>";
        $("#jenis_kelamin").load(url);        
        var url = "<?php echo base_url('pst_regclient/registrasi/ajax_family_relation') ?>";
        $("#hubwaris").load(url);    
        var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_prov') ?>";
        $("#provinsi").load(url);
        var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_bank') ?>";
        $("#bank").load(url);
    });

</script>

</script> <script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
        //initialize the javascript
//        $("[data-mask='date']").mask("9999-99-99");
//        $("[data-mask='phone']").mask("(999) 999-9999");
//        $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
//        $("[data-mask='phone-int']").mask("9999 9999 9999");
//        $("[data-mask='taxid']").mask("99-9999999");
//        $("[data-mask='ssn']").mask("999-99-9999");
//        $("[data-mask='product-key']").mask("a*-999-a999");
//        $("[data-mask='percent']").mask("99%");
//        $("[data-mask='currency']").mask("$999,999,999.99");
});
</script>

<script src="<?php echo base_url(); ?>'/assets/js/jquery.min.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.autocomplete.js'></script>
<link href='<?php echo base_url(); ?>assets/css/jquery.autocomplete.css' rel='stylesheet' />

<script type='text/javascript'>
    var site = "<?php echo base_url(); ?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + 'pst_regclient/registrasi/search_kyai',
            onSelect: function (suggestion) {
                $('#kyai_id').val('' + suggestion.anggota_id);
            }
        });
    });
    function cek_nama(){
        let nama = $('#nama_lengkap').val();
        var link_rm = "<?php echo site_url('pst_regclient/registrasi/cek_nama') ?>";
                link_rm = link_rm + "/" + nama;
        $.get(link_rm, function(data){
            document.getElementById('hasil_cek').innerText = data.hasil;
        },"json");
    }
</script>

