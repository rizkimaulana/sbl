<div class="page-head">
    <h2>Data Anggota </h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">PESANTREN</a></li>
        <li class="active">Data Anggota</li>
    </ol>
</div>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Anggota</b>
                </div>
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID User</th>
                                    <th>Nama</th>
                                    <th>Kabupaten</th>
                                    <th>Tgl. Daftar</th>
                                    <th>Telp</th>
                                    <!-- <th>Email</th> -->
                                    <th>Status</th>
                                    <th>Kelompok</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="md-custom" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="POST" id="form_aktivasi" name="form_detail" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">Data Anggota</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>Nama lengkap</b></label>
                                <input type="hidden" name="id" id="id">
                                <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>Jenis Kelamin</b></label>
                                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                    <option value="">- Pilih Jenis Kelamin -</option>
                                    <option value="2">Laki-laki</option>
                                    <option value="3">Perempuan</option>
                                </select>
                                <!--<input type="text" name="jenis_kelamin" id="jenis_kelamin" class="form-control" placeholder="Jenis Kelamin">-->
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="color: red"><b>Alamat</b></label>
                                <input type="text" name="alamat" id="alamat" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Tempat Lahir</b></label>
                                <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Tanggal Lahir</b></label>
                                <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Provinsi</b></label>
                                <select class="form-control" id="provinsi" name="provinsi">
                                    <option>Provinsi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Kota</b></label>
     <!--<input type="text" name="kota" id="kota" class="form-control" placeholder="Tgl lahir">-->
                                <select class="form-control" id="kabupaten" name="kabupaten">
                                    <option>Kota</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Kode Pos</b></label>
                                <input type="text" name="kode_pos" id="kode_pos" class="form-control" placeholder="Tgl lahir">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Telp</b></label>
                                <input type="text" name="telp" id="telp" class="form-control" data-mask="phone-int" placeholder="9999 9999 9999">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Email</b></label>
                                <input type="text" name="email" id="email" class="form-control" placeholder="Tgl lahir">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>No. Identitas</b></label>
                                <input type="text" name="no_identitas" id="no_identitas" class="form-control" placeholder="Status Kawin">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Status Kawin</b></label>
                                <select class="form-control" id="status_nikah" name="status_nikah">
                                    <option value="">Status Nikah</option>
                                    <option value="BELUM MENIKAH">Belum Menikah</option>
                                    <option value="SUDAH MENIKAH">Sudah Menikah</option>
                                    <option value="DUDA/JANDA">Duda/Janda</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Tanggal Daftar</b></label>
                                <input type="text" name="tgl_daftar" id="tgl_daftar" class="form-control" readonly="readonly">
                            </div>
                        </div>  
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="color: red"><b>Kelompok</b></label>
                                <input type="text" name="kelompok" id="kelompok" class="form-control" readonly="readonly">
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnsave" type="submit" class="btn btn-primary" onclick="simpan_aktivasi()">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "ajax": {
                "url": "<?php echo site_url('pst_regclient/registrasi/ajax_data_anggota') ?>",
            },
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
        });
    });

    function detail(id_anggota) {
        var link_rm = "<?php echo site_url('pst_regclient/registrasi/detail') ?>";
        link_rm = link_rm + "/" + id_anggota;
        $.get(link_rm, function (data) {
            $('#id').val(data.id);
            $('#nama_lengkap').val(data.nama_lengkap);
            $('#tempat_lahir').val(data.tempat_lahir);
            $('#tgl_lahir').val(data.tgl_lahir);
            $('#alamat').val(data.alamat);
            $('#tgl_daftar').val(data.tanggal);
            $('#kabupaten').val(data.kabupaten_id);
            $('#provinsi').val(data.provinsi_id);
            $('#kode_pos').val(data.kode_pos);
            $('#telp').val(data.telp);
            $('#email').val(data.email);
            $('#jenis_kelamin').val(data.jenis_kelamin);
            $('#no_identitas').val(data.no_identitas);
            $('#status_nikah').val(data.status_nikah);
            $('#kelompok').val(data.kelompok);
            $('#btnsave').prop('disabled', true);
        }, "json");

        $('#md-custom').modal('show');

    };

    function edit_data(id_anggota) {
        var link_rm = "<?php echo site_url('pst_regclient/registrasi/detail') ?>";
        link_rm = link_rm + "/" + id_anggota;
        $.get(link_rm, function (data) {
            $('#id').val(data.id);
            $('#nama_lengkap').val(data.nama_lengkap);
            $('#tempat_lahir').val(data.tempat_lahir);
            $('#tgl_lahir').val(data.tgl_lahir);
            $('#alamat').val(data.alamat);
            $('#tgl_daftar').val(data.tanggal);
            $('#kabupaten').val(data.kabupaten_id);
            $('#provinsi').val(data.provinsi_id);
            $('#kode_pos').val(data.kode_pos);
            $('#telp').val(data.telp);
            $('#email').val(data.email);
            $('#jenis_kelamin').val(data.jenis_kelamin);
            $('#no_identitas').val(data.no_identitas);
            $('#status_nikah').val(data.status_nikah);
            $('#kelompok').val(data.kelompok);

            $('#btnsave').prop('disabled', false);

        }, "json");

        $('#md-custom').modal('show');

    }
    ;

    function delete_data(id)
    {
        if (confirm('Apakah Anda Akan Menghapus Data Ini ?'))
        {
            $.ajax({
                url: "<?php echo site_url('pst_regclient/registrasi/delete_data') ?>/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    table.ajax.reload();
                }
            });
        }
    }

    document.getElementById('select_pay_method').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        // document.getElementById('hidden_div2').style.display = style;
        // $("#datepicker").prop('required',true);
        select_pay_method = $(this).val();
        console.log(select_pay_method);
        if (select_pay_method == 'TRANSFER') {
            $("#select_bank").prop('required', true);
            $("#pic1").prop('required', true);
        } else {
            $("#select_bank").prop('required', false);
            $("#pic1").prop('required', false);
        }
    });

    function simpan_aktivasi() {
        $('#form_aktivasi').submit();
    }
    ;
</script>

<script type='text/javascript' src='<?php echo base_url(); ?>assets/jquery.autocomplete.js'></script>
<link href='<?php echo base_url(); ?>assets/jquery.autocomplete.css' rel='stylesheet' />

<script type='text/javascript'>
    var site = "<?php echo base_url(); ?>";
    $(function () {
        $('.autocomplete').autocomplete({
            serviceUrl: site + 'pst_regclient/registrasi/search_ustadz',
            onSelect: function (suggestion) {
                $('#ustadz_id').val('' + suggestion.anggota_id);
            }
        });

        $('#provinsi').change(function () {
            var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_kab') ?>/" + $(this).val();
            $('#kabupaten').load(url);
            //return false;
        });
    });

    $(document).ready(function () {
        var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_prov') ?>";
        $("#provinsi").load(url);
        var url = "<?php echo base_url('pst_regclient/registrasi/add_ajax_kab_detail') ?>";
        $("#kabupaten").load(url);
    });



</script>

</script> <script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //initialize the javascript
        $("[data-mask='date']").mask("9999-99-99");
        $("[data-mask='phone']").mask("(999) 999-9999");
        $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
        $("[data-mask='phone-int']").mask("9999 9999 9999");
        $("[data-mask='taxid']").mask("99-9999999");
        $("[data-mask='ssn']").mask("999-99-9999");
        $("[data-mask='product-key']").mask("a*-999-a999");
        $("[data-mask='percent']").mask("99%");
        $("[data-mask='currency']").mask("$999,999,999.99");
    });
</script>