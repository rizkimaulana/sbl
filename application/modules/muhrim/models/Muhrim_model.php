<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Muhrim_model extends CI_Model{
   
   
    private $_table="muhrim";
    private $_primary="id_muhrim";

    public function save($data){
   
        // $cek = $this->db->select('kode')->where('nama',$data['nama'])->get('product')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        	'id_jenis_kelamin ' => $data['select_kelamin'],
            'id_status ' => $data['select_statuskawin'],
            'id_rentang_umur ' => $data['rentang_umur'],
            'id_hubungan ' => $data['select_hubungan_muhrim'],
            'biaya_muhrim' => $data['biaya_muhrim'],
            'create_date' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('id_user')
           
        );       
        
         return $this->db->insert('muhrim',$arr);

       
    }


    public function update($data){
        
        $arr = array(
        'id_jenis_kelamin ' => $data['select_kelamin'],
            'id_status ' => $data['select_statuskawin'],
            'id_rentang_umur ' => $data['rentang_umur'],
            'id_hubungan ' => $data['select_hubungan_muhrim'],
            'biaya_muhrim' => $data['biaya_muhrim'],
             'update_date' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('id_user')
        );       
              
        return $this->db->update($this->_table,$arr,array('id_muhrim'=>$data['id_muhrim']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT a.id_muhrim,b.keterangan as jenis_kelamin, c.keterangan as statuskawin, d.keterangan as rentang_umur, e.ket_hubungan as hubungan, a.biaya_muhrim, f.nama as create_by FROM muhrim a 
                    LEFT JOIN status b ON b.kdstatus = a.id_jenis_kelamin
                    LEFT JOIN status c ON c.kdstatus = a.id_status
                    LEFT JOIN rentang_umur d ON d.id_rentang_umur = a.id_rentang_umur
                    LEFT JOIN hubungan_muhrim e ON e.id_hub_muhrim = a.id_hubungan
                    LEFT JOIN user f ON f.id_user = a.create_by
                    WHERE 1=1";
        
        if($q){
            
            $sql .=" AND b.keterangan LIKE '%{$q}%' 
           			 OR c.keterangan LIKE '%{$q}%'
            		OR rentang_umur LIKE '%{$q}%'
            		OR hubungan LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY a.id_muhrim DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     public function delete($id_muhrim){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_muhrim'=>$id_muhrim));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
    
    public function delete_by_id($id_muhrim)
    {
        $this->db->where('id_muhrim', $id_muhrim);
        $this->db->delete($this->_table);
    }
    
    
}
