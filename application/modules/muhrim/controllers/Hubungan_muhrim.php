<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hubungan_muhrim extends CI_Controller{
	var $folder = "muhrim";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REFERENSI_MUHRIM,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('hubunganmuhrim_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/hubungan_muhrim');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->hubunganmuhrim_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                // $rows .='<td width="10%">'.$r->kode.'</td>';
	                // $rows .='<td width="40%">'.$r->nama.'</td>';
	                $rows .='<td width="70%">'.$r->ket_hubungan.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'muhrim/hubungan_muhrim/edit/'.$r->id_hub_muhrim.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_hubungan('."'".$r->id_hub_muhrim."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'muhrim/hubungan_muhrim/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->hubunganmuhrim_model->save($data);
	     if($send)
	        redirect('muhrim/hubungan_muhrim');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(REFERENSI_MUHRIM,'add'))
		    $this->general->no_access();

	    
	    // $data = array('select_status'=>$this->_select_status());
        $this->template->load('template/template', $this->folder.'/add_hubungan');	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(REFERENSI_MUHRIM,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(4);
	    $get = $this->db->get_where('hubungan_muhrim',array('id_hub_muhrim'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   
        $this->template->load('template/template', $this->folder.'/edit_hubungan',array_merge($get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->hubunganmuhrim_model->update($data);
	     if($send)
	        redirect('muhrim/hubungan_muhrim');
		
	}
	
	
	public function ajax_delete($id_hub_muhrim)
	{
		if(!$this->general->privilege_check(REFERENSI_MUHRIM,'remove'))
		    $this->general->no_access();
		$send = $this->hubunganmuhrim_model->delete_by_id($id_hub_muhrim);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('muhrim/hubungan_muhrim');
	}


}
