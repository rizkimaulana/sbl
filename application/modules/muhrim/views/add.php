 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-12">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Muhrim</h3>
       
        <form class="form-horizontal"  role="form" method="post" action="<?php echo base_url();?>muhrim/save">
           <div class="panel panel-default">
              <div class="panel-body">  
		            <div class="form-group">
			             <label class=" col-lg-2 control-label" >Jenis Kelamin :</label>
			             <div class="col-lg-5">
			            <select class="form-control" name="select_kelamin">
			              <option value=''>- PILIH JENIS KELAMIN -</option>
			                <?php foreach($select_kelamin as $sk){?>
			                    <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
			                <?php } ?>
				            </select>
				          </div>
			          </div>
          
        
			            <div class="form-group">
			                 <label class="col-lg-2 control-label" >Status :</label>
			                 <div class="col-lg-5">
			                <select class="form-control" name="select_statuskawin">
			                  <option value="">- PILIH STATUS -</option>
			                    <?php foreach($select_statuskawin as $sk){?>
			                        <option value="<?php echo $sk->kdstatus;?>"><?php echo $sk->keterangan;?></option>
			                    <?php } ?>
			                </select>
			              </div>
			            </div>
	            <div class="form-group">
	                 <label class="col-lg-2 control-label" >Rentang Umur :</label>
	                 <div class="col-lg-5">
	                <select class=" form-control" name="rentang_umur">
	                  <option value=""></option>
	                    <?php foreach($select_rentangumur as $sk){?>
	                        <option value="<?php echo $sk->id_rentang_umur;?>"><?php echo $sk->keterangan;?></option>
	                    <?php } ?>
	                </select>
	              </div>
			      </div>
	            <div class="form-group">
	              <label class="col-lg-2 control-label" >Hubungan Keluarga</label>
	               <div class="col-lg-5">
	                <select class="form-control" name="select_hubungan_muhrim">
	                    <option value="">- PERGI DENGAN -</option>
	                  <?php foreach($select_hubungan_muhrim as $fr){?>
	                      <option value="<?php echo $fr->id_hub_muhrim;?>"><?php echo $fr->ket_hubungan;?></option>
	                  <?php } ?>
	              </select>
	              </div>
	              </div>


	                <div class="form-group">
	                    <label class="col-lg-2 control-label" >Biaya Muhrim</label>
	                    <div class="col-lg-5">
	                      <input name="biaya_muhrim" class="form-control" required>
	                    </div>
	                </div>

	        </div>
	    </div>
     <!--        <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="select_status">
                    <?php foreach($select_status as $st){?>
                        <option value="<?php echo $st->kdstatus;?>"><?php echo $st->keterangan;?></option>
                    <?php } ?>
                </select>
            </div> -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>muhrim" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
