<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class List_jamaah extends CI_Controller {
	var $folder = "registrasi_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(REGISTRASI_JAMAAH,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registrastion');	
		$this->load->model('registrasijamaah_model');
		
	}


	public function index() {
		// $this->data['judul_browser'] = 'Pinjaman';
		// $this->data['judul_utama'] = 'Transaksi';
		// $this->data['judul_sub'] = 'Pembayaran Angsuran';

		// $this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		// $this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		// $this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';
		// //$this->data['js_files'][] = base_url() . 'assets/easyui/datagrid-detailview.js';

		// #include tanggal
		// $this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		// $this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		// $this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';
		// #include seach
		// $this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		// $this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';

		$this->template->load('template/template', $this->folder.'/list_jamaah');
		// $this->data['isi'] = $this->load->view('list_jamaah', $this->data, TRUE);
		// $this->load->view('template/template', $this->data);
	}


	function ajax_list() {
		/*Default request pager params dari jeasyUI*/
		$offset = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$limit  = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		$sort  = isset($_POST['sort']) ? $_POST['sort'] : 'create_date';
		$order  = isset($_POST['order']) ? $_POST['order'] : 'desc';
		$id_booking = isset($_POST['id_booking']) ? $_POST['id_booking'] : '';
		$tgl_dari = isset($_POST['tgl_dari']) ? $_POST['tgl_dari'] : '';
		$tgl_sampai = isset($_POST['tgl_sampai']) ? $_POST['tgl_sampai'] : '';
		$search = array('id_booking' => $id_booking, 'tgl_dari' => $tgl_dari, 'tgl_sampai' => $tgl_sampai);
		$offset = ($offset-1)*$limit;
		$data   = $this->registrasijamaah_model->get_data_jamaah_ajax($offset,$limit,$search,$sort,$order);
		$i	= 0;
		$rows   = array(); 

		foreach ($data['data'] as $r) {
			// $tgl_bayar = explode(' ', $r->tgl_pinjam);
			// $txt_tanggal = jin_date_ina($tgl_bayar[0],'p');		

			//array keys ini = attribute 'field' di view nya
			// $anggota = $this->general_m->get_data_anggota($r->anggota_id);   

			$rows[$i]['id_booking'] = $r->id_booking;
			$rows[$i]['invoice'] ='#'.$r->invoice;
			$rows[$i]['paket_txt'] = $r->paket;
			$rows[$i]['departure'] = $r->departure;
			$rows[$i]['create_date'] = $r->create_date;
			$rows[$i]['tgl_keberangkatan'] = $r->tgl_keberangkatan;
			$rows[$i]['jumlah_jamaah'] = $r->jumlah_jamaah.' Orang';
			// $rows[$i]['bayar'] = '<p></p><p>
			// <a href="'.site_url('angsuran').'/index/' . $r->id . '" title="Bayar Angsuran"> <i class="fa fa-money"></i> Bayar </a></p>';
			$i++;
		}
		//keys total & rows wajib bagi jEasyUI
		$result = array('total'=>$data['count'],'rows'=>$rows);
		echo json_encode($result); //return nya json
	}

	

}