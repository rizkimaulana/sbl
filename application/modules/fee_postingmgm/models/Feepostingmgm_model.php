<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feepostingmgm_model extends CI_Model{
      var $table = 'reward_dp';
    var $column_order = array(null, 'id','reward_sponsor','id_user','sponsor','peringkat','kelas','reward','cash','datewin','datewin_sponsor','keterangan','st','st_sponsor','tipe','group_id','status','id_jamaah_mutasi'); //set column field database for datatable orderable
    var $column_search = array('id','reward_sponsor','id_user','sponsor','peringkat','kelas','reward','cash','datewin','datewin_sponsor','keterangan','st','st_sponsor','tipe','group_id','status','id_jamaah_mutasi'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); // default order 
    private $_table="reward_dp";
    private $_primary="id";


    public function get_data($offset,$limit,$q=''){
    
     $id_user=  $this->session->userdata('id_user');
          $sql = " SELECT * FROM reward_dp where id_user='".$id_user." ' 
          ";
        
        if($q){
            
            $sql .=" AND reward LIKE '%{$q}%' 
            	    
                    
                    ";
        }
        $sql .=" ORDER BY id DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

   
     public function update_status_fee_posting_jamaah($id){
    
      
        $arr = array(
        
           'st' => 1,
            'datewin' => date('Y-m-d H:i:s'),
            
            
           
        );       
              
         $this->db->update($this->_table,$arr,array('id'=>$id));

  
    }
    private function _get_datatables_query()
    {
        $id_user=  $this->session->userdata('id_user');
        $this->db->where('id_user', $id_user);
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                   
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
         
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
