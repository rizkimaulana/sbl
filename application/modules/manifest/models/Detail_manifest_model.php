<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Detail_manifest_model extends CI_Model {
    /* var $table = 'jamaah_aktif';
      var $column_order = array('id_registrasi'); //set column field database for datatable orderable
      var $column_search = array('id_registrasi', 'id_sahabat', 'id_booking', 'id_affiliate', 'invoice', 'id_jamaah', 'nama', 'telp', 'no_identitas', 'tgl_daftar', 'paket', 'category', 'date_schedule', 'bulan_menunggu', 'bulan_keberangkatan', 'departure', 'id_user', 'tipe_jamaah', 'status', 'kd_tipe', 'tgl_aktivasi', 'update_date'); //set column field database for datatable searchable
      var $order = array('update_date' => 'asc'); // default order
      private $_table = "registrasi_jamaah";
      private $_booking = "booking";
      private $_primary = "id_jamaah";
      private $_bilyet = "bilyet";
      private $_aktivasi_reschedule = "aktivasi_reschedule"; */

    var $table = 'detail_data_manifest';//'export_detail_manifest';
    var $column_order = array(null, 'id_registrasi','id_jamaah', 'id_booking', 'nama', 'telp'); //set column field database for datatable orderable
    var $column_search = array('id_registrasi','id_jamaah', 'id_booking', 'nama', 'telp'); //set column field database for datatable searchable 
    var $order = array('id_jamaah' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {
        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if (!empty($_POST['search']['value'])) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($tahun, $bulan) {
        $this->_get_datatables_query();
        if (!empty($tahun) && !empty($bulan)) {
            $this->db->where("DATE_FORMAT(date_schedule,'%Y')", $tahun);
            $this->db->where("DATE_FORMAT(date_schedule,'%M')", $bulan);
        }
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_filtered($tahun, $bulan) {
        $this->_get_datatables_query();
        if (!empty($tahun) && !empty($bulan)) {
            $this->db->where("DATE_FORMAT(date_schedule,'%Y')", $tahun);
            $this->db->where("DATE_FORMAT(date_schedule,'%M')", $bulan);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($tahun, $bulan) {
        $this->db->from($this->table);
        if (!empty($tahun) && !empty($bulan)) {
            $this->db->where("DATE_FORMAT(date_schedule,'%Y')", $tahun);
            $this->db->where("DATE_FORMAT(date_schedule,'%M')", $bulan);
        }
        return $this->db->count_all_results();
    }
    
    public function get_detail_data_manifest($tahun, $bulan){
        $this->db->from($this->table);
        if (!empty($tahun) && !empty($bulan)) {
            $this->db->where("DATE_FORMAT(date_schedule,'%Y')", $tahun);
            $this->db->where("DATE_FORMAT(date_schedule,'%M')", $bulan);
        }
        return $this->db->get()->result_array();
    }
    
}
