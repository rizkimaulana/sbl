
<div id="page-wrapper">

    <form   class="form-horizontal" role="form"  >
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>List Jamaah Manifest</b>
                    </div>
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-danger">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>DATA GM/CABANG</th>
                                    <th>JML JAMAAH</th>
                                    <th>BELUM CLUSTER</th>
                                    <th>SUDAH CLUSTER</th>
                                    <th>APPROVED </th>
                                    <th>ACTION </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>  
                </div>
            </div>
        </div>
    </form> 
</div>





<script type="text/javascript">

    var table;

    $(document).ready(function () {

        //datatables
        table = $('#data-table').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('manifest/manifest_perwakilan/ajax_list') ?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

    });

</script>