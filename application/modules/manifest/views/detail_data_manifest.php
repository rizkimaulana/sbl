<!--<script src="<?php //echo base_url() ?>assets/js/jquery.table2excel.js"></script>-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>DATA MANIFEST</b>
                </div>
                <div class="panel-body">
                    <!--                    <form action="#" role="form" method="POST" class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <select name="tipe_tanggal" id="tipe_tanggal" class="form-control">
                                                        <option value="payment_date" selected="selected">Payment Date</option>
                                                        <option value="tgl_daftar">Tgl Daftar</option>
                                                        <option value="update_date">Tgl Update</option>
                                                    </select>
                                                    <input type="hidden" id="filter" name="filter" value="">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                                                </div>
                                                <div class="col-sm-4">
                                                    <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                                    <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                                                </div>
                                            </div>
                                        </form>-->      
                    <button>Export</button>
                    <div class="table-responsive">
                        <table id="example2" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>AFFILIATE</th>
                                    <th>JAMAAH</th>
                                    <th>PAKET</th>
                                    <th>KAMAR</th>
                                    <th>EMBARKASI</th>
                                    <th>TELP</th>
                                    <th>SEX</th>
                                    <th>LAHIR</th>
                                    <th>PASSPORT</th>
                                    <th>ISSUE PASSPORT</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>    
</div>
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan; ?>">



<script type="text/javascript">
    var table;
    var tahun = $('#tahun').val();
    var bulan = $('#bulan').val();

    $(document).ready(function () {
        //datatables
        table = $('#example2').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.

            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('manifest/detail_manifest/ajax_list'); ?>/" + tahun + "/" + bulan,
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

    });
</script>

<script type="text/javascript">
    $('button').click(function () {
        $("#example2").table2excel({
            exclude: ".noExl",
            name: "Laporan Data Manifest",
            filename: "lap_data_manifest",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        })
    });
</script>