<div id="page-wrapper">


    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Edit Affiliate</h3>

                <form action="#" id="form" class="form-horizontal" role="form" method="post"  onsubmit="return checkForm(this);">

           <!--   <form action="<?php echo base_url(); ?>manifest_affiliate/save" id="form" class="form-horizontal"  enctype="multipart/form-data"> -->
                    <div class="modal-body form">

                        <input type="hidden" value="" name="id"/> 
                        <input type="hidden" value="" name="id_registrasi"/> 
                        <input type="hidden" value="" name="id_booking"/> 
                        <input type="hidden" value="" name="id_jamaah"/> 
                        <input type="hidden" value="" name="id_affiliate"/> 
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama di Passport</label>
                                <div class="col-sm-9">
                                    <input name="nama_passport" type="text" id="nama_passport" class=" form-control" placeholder="Nama Passport " required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">No Passport</label>
                                <div class="col-sm-9">
                                    <input name="no_pasport" type="text" id="no_pasport" class=" form-control" placeholder="No Passport Office" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">issue office</label>
                                <div class="col-sm-9">
                                    <input name="issue_office" type="text" id="issue_office" class="input_keluarga form-control" placeholder="Input Issue Office" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3" >Isue Date</label>
                                <div class="col-sm-9">

                                    <input type="text" class="form-control" data-mask="date" name="isui_date" id="isui_date"   placeholder="YYYY-MM-DD" required/>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3" >TELP</label>
                                <div class="col-sm-9">

                                    <input type="text" class="form-control" data-mask="date" name="telp" id="telp"   placeholder="NO TELP" required/>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3" >TELP 2</label>
                                <div class="col-sm-9">

                                    <input type="text" class="form-control" data-mask="date" name="telp_2" id="telp_2"   placeholder="NO TELP KELUARGA SERUMAH ATAU TIDAK SERUMAH" required/>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3" >Tanggal Lahir</label>
                                <div class="col-sm-9">

                                    <input type="text" class="form-control" data-mask="date" name="tanggal_lahir" id="tanggal_lahir"   placeholder="YYYY-MM-DD" required/>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3" >Tempat Lahir</label>
                                <div class="col-sm-9">

                                    <input type="text" class="form-control" data-mask="date" name="tempat_lahir" id="tempat_lahir"   placeholder="Alamat Lahir" required/>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Jenis Kelamin</label>
                                <div class="col-md-9">
                                    <select required name="kelamin" class="form-control"  >
                                        <option value="">--PILIH JENIS KELAMIN--</option>
                                        <option value="2">PRIA</option>
                                        <option value="3">WANITA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">PERGI DENGAN</label>
                                <div class="col-md-9">
                                    <select required name="ket_keberangkatan" class="form-control"  >
                                        <option value="">--PILIH BERANGKAT DENGAN--</option>
                                        <option value="8">KELUARGA</option>
                                        <option value="9">SENDIRI/GROUP</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">HUBANGAN KELUARGA</label>
                                <div class="col-md-9">
                                    <select name="hubkeluarga" class="form-control"  >
                                        <option value="">--PILIH BERANGKAT DENGAN--</option>
                                        <?php foreach ($family_relation as $fr) { ?>
                                            <option value='<?php echo $fr->id_relation; ?>'><?php echo $fr->keterangan; ?></option>
                                        <?php } ?> 

                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>



                            <!--   <div class="form-group">
                             <label class="control-label col-md-3">INPUT KELUARGA</label>
                             <div class="col-md-9">
                               <select class="form-control chosen-select"  name="keluarga" id="keluarga" tabindex="9" >
                                <option value=''>- PILIH KELUARGA -</option>
                            <?php
                            foreach ($get_keluarga as $kl) {
                                echo '<option value="' . $kl->id_jamaah . '">' . $kl->nama . '-' . $kl->id_jamaah . '</option>';
                            }
                            ?>
                              </select>
                                        <span class="help-block"></span>
                                </div>
                              </div> -->
                            <!--  <label class="col-sm-4 control-label" for="inputPassword3" >ID KELUARGA</label>
                                                         <div class="col-sm-5">
                                                            <input type="text" class="form-control"  name="keluarga" id="keluarga"   />
                                                         </div> -->
                            <div class="form-group">

                                <label class="control-label col-md-3">INPUT KELUARGA</label>
                                <div class="col-md-9">

                                    <input type="search" name="autocomplete" id="autocomplete1" class="form-control autocomplete id_jamaah" placeholder="INPUT ID ATAU NAMA KELUARGA"  required>
                                </div> 

                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputPassword3" >KETERANGAN KELUARGA</label>
                                <div class="col-sm-9">

                                    <input type="text" class="form-control" data-mask="date" name="ket_keluarga" id="ket_keluarga"   placeholder="KETERANGAN KELUARGA" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy Pasport </label>
                                <div class="col-md-9">
                                    <select name="status_pasport" class="form-control">
                                        <option value="">--PILIH STATUS PASSPORT--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima Passport</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_passport" id="tgl_passport"   placeholder="YYYY-MM-DD" required/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy Kartu Identitas/KTP</label>
                                <div class="col-md-9">
                                    <select name="status_identitas" class="form-control">
                                        <option value="">--PILIH STATUS IDENTITAS/KTP--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima KTP</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_ktp" id="tgl_ktp"   placeholder="YYYY-MM-DD" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy Kartu Keluarga</label>
                                <div class="col-md-9">
                                    <select name="status_kk" class="form-control">
                                        <option value="">--PILIH STATUS KARTU KELUARGA--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima KK</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_kk" id="tgl_kk"   placeholder="YYYY-MM-DD" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy Pas Photo</label>
                                <div class="col-md-9">
                                    <select name="status_photo" class="form-control">
                                        <option value="">--PILIH STATUS PHOTO--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima Pas PHOTO</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_photo" id="tgl_photo"   placeholder="YYYY-MM-DD" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy Terima Vaksin</label>
                                <div class="col-md-9">
                                    <select name="status_vaksin" class="form-control">
                                        <option value="">--PILIH STATUS BUKU NIKAH--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima Vaksin</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_vaksin" id="tgl_vaksin"   placeholder="YYYY-MM-DD" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy Buku Nikah</label>
                                <div class="col-md-9">
                                    <select name="status_buku_nikah" class="form-control">
                                        <option value="">--PILIH STATUS BUKU NIKAH--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima BN   </label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_bn" id="tgl_bn"   placeholder="YYYY-MM-DD" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">FotoCopy AKTE LAHIR</label>
                                <div class="col-md-9">
                                    <select name="status_akte" class="form-control">
                                        <option value="">--PILIH STATUS AKTE LAHIR--</option>
                                        <option value="0">BELUM ADA</option>
                                        <option value="1">SUDAH ADA</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <label class="col-sm-7 control-label" for="inputPassword3" >Tgl Terima Akte</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" data-mask="date" name="tgl_akte" id="tgl_akte"   placeholder="YYYY-MM-DD" />
                                </div>
                            </div>




                        </div>

                    </div>
                    <div class="modal-footer">

                        <button type="submit" name="myButton" id="myButton" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>