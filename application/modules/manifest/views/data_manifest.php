<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>DATA MANIFEST</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>AFFILIATE</th>
                                    <th>JAMAAH</th>
                                    <th>PAKET</th>
                                    <th>KAMAR</th>
                                    <th>EMBARKASI</th>
                                    <th>TELP</th>
                                    <th>SEX</th>
                                    <th>LAHIR</th>
                                    <th>PASSPORT</th>
                                    <th>ISSUE PASSPORT</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        //datatables
        table = $('#data-table').DataTable({
//            "processing": true, //Feature control the processing indicator.
//            "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
            "ajax": {
                "url": "<?php echo site_url('manifest/json_data_manifest/'.$tahun.'/'.$bulan); ?>"
                
            },
            //Set column definition initialisation properties.
//            "columnDefs": [
//                {
//                    "targets": [0], //first column / numbering column
//                    "orderable": false, //set not orderable
//                },
//            ],
        });
    });
</script>