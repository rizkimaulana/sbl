<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=laporan_manifest_".date('d-M-Y').".xls");
                    ?> 
                    <th>NO</th>
                    <th>USER ID</th>
                    <th>CABANG</th>
                    <th>PAKET</th>
                    <th>ROOM</th>
                    <th>TELEPON</th>
                    <th>NAME</th>
                    <th>SEX</th>
                    <th>PLACE OF BIRTH</th>
                    <th>DATE OF BIRTH</th>
                    <th>PASSPORT</th>
                    <th>ISSUE OFFICE</th>
                    <th>ISSUE DATE</th>
                    <th>EXPIRED</th>
                    <th>HUBUNGAN KELUARGA</th>
                    <th>ID AFFILIATE</th>
                    <th>NAMA AFFILIATE</th>
                    <th>EMBARKASI</th>
                    <th>TGL KEBERANGKATAN</th>
                </tr>
            </thead>
            <tbody>
            <?php $no = 0;
                foreach ($data_manifest as $pi) {
                $no++ ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $pi['id_jamaah']; ?>  </td>
                        <td><?php echo $pi['nama_affiliate']; ?>  </td>
                        <td><?php echo $pi['nama_produk']; ?>  </td>
                        <td><?php echo $pi['nama_room_category']; ?>  </td>
                        <?php $telp = !empty($pi['telp_2']) ? $pi['telp'].' / '.$pi['telp_2'] : $pi['telp']; ?>
                        <td><?php echo $telp; ?>  </td>
                        <td><?php echo !empty($pi['nama_passport']) ? $pi['nama_passport'] : $pi['nama']; ?>  </td>
                        <td><?php echo $pi['jenis_kelamin'] ?>  </td>
                        <td><?php echo $pi['tempat_lahir'] ?>  </td>
                        <td><?php echo !empty($pi['tanggal_lahir']) ? date('d-M-Y', strtotime($pi['tanggal_lahir'])) : ''; ?>  </td>
                        <td><?php echo $pi['no_passport'] ?>  </td>
                        <td><?php echo $pi['issue_office'] ?>  </td>
                        <?php $issue_date = !empty($pi['issue_date']) && ($pi['issue_date'] !=='0000-00-00 00:00:00') ? date('Y-m-d', strtotime($pi['issue_date'])) : ''; ?>
                        <td><?php echo $issue_date; ?>  </td>
                        <?php 
                            $current_date = $issue_date;
                            $add_month = strtotime('+5 year', strtotime($current_date));
                            $add_month = date('Y-m-d', $add_month);
                        ?>
                        <td><?php echo !empty($issue_date) ? $add_month : ''; ?>  </td>
                        <td><?php echo $pi['hub_relasi'].' '.$pi['keluarga']; ?>  </td>
                        <td><?php echo $pi['id_affiliate'] ?>  </td>
                        <td><?php echo $pi['nama_affiliate'] ?>  </td>
                        <td><?php echo $pi['nama_embarkasi'] ?>  </td>
                        <td><?php echo date('d/m/Y', strtotime($pi['tgl_berangkat'])) ?>  </td>
                    </tr>
            <?php } ?>  
            </tbody>
        </table>
    </div>
</head>
</html>

<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
