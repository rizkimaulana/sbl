<link href="<?php echo base_url(); ?>assets/css/buttons.bootstrap.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.colVis.min.js"></script>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>DATA MANIFEST</b>
                </div>
                <div class="panel-body">
                    <div style="width: 920px; overflow-x: scroll;">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>USER ID</th>
                                    <th>CABANG</th>
                                    <th>PAKET</th>
                                    <th>ROOM</th>
                                    <th>ROOM TYPE</th>
                                    <th>TELEPON</th>
                                    <th>NAME</th>                                    
                                    <th>SEX</th>
                                    <th>PLACE OF BIRTH</th>
                                    <th>DATE OF BIRTH</th>
                                    <th>PASSPORT</th>
                                    <th>ISSUE OFFICE</th>
                                    <th>ISSUE DATE</th>
                                    <th>EXPIRED</th>
                                    <th>HUB KELUARGA</th>
                                    <th>ID AFFILIATE</th>
                                    <th>NAMA AFFILIATE</th>
                                    <th>EMBARKASI</th>
                                    <th>TGL KEBERANGKATAN</th>

                                </tr>
                            </thead>
                            <tfoot></tfoot>
                            <tbody>
                                <?php
//                                $no = 0;
//                                foreach ($data_manifest as $rows) {
//                                    $no++;
//                                    echo "<tr>";
//                                    echo "<td>" . $no . "</td>";
//                                    echo "<td>" . $rows['id_jamaah'] . "</td>";
//                                    echo "<td>" . $rows['nama_affiliate'] . "</td>";
//                                    echo "<td hidden='hide'>" . $rows['paket'] . "</td>";
//                                    echo "<td hidden='hide'>" . $rows['room'] . "</td>";
//                                    echo "<td hidden='hide'>" . $rows['telp'] . "</td>";
//                                    echo "<td>" . $rows['nama'] . "</td>";
                                    
//                                    echo "<td hidden='hide'>" . $rows['sex'] . "</td>";
//                                    echo "<td hidden='hide'>" . $rows['tempat_lahir'] . "</td>";
//                                    echo "<td hidden='hide'>" . $rows['tanggal_lahir'] . "</td>";
//                                    echo "<td>" . $rows['no_passport'] . "</td>";
//                                    echo "<td>" . $rows['issue_office'] . "</td>";
//                                    echo "<td>" . $rows['issue_date'] . "</td>";
//                                    echo "<td>" . $rows['expired'] . "</td>";
//                                    echo "<td>" . $rows['keluarga'] . "</td>";
//                                    echo "<td>" . $rows['id_affiliate'] . "</td>";
//                                    echo "<td hidden='hide'>" . $rows['nama_affiliate'] . "</td>";
//                                    echo "<td>" . $rows['embarkasi'] . "</td>";
//                                    echo "<td>" . date('d-m-Y',strtotime($rows['date_schedule'])) . "</td>";
//                                    echo "</tr>";                                    
//                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<input type="hidden" name="tahun" id="tahun" value="<?php //echo $tahun;    ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php //echo $bulan;    ?>">-->
<input type="hidden" name="tahun" id="tahun" value="<?php echo $tahun; ?>">
<input type="hidden" name="bulan" id="bulan" value="<?php echo $bulan; ?>">

<script type="text/javascript">
    var table;
    var tahun = $('#tahun').val();
    var bulan = $('#bulan').val();

    $(document).ready(function () {
        table = $('#example').DataTable({
            dom: 'Bfrtip',
            lengthChange: false,
            buttons: ['excel', 'pdf', 'colvis'],
            "ajax": {
                "url": "<?php echo site_url('manifest/detail_manifest/ajax_export'); ?>/" + tahun + "/" + bulan,
                "type": "POST"
            },
        });

        table.buttons().container()
            .appendTo('#example_wrapper .col-sm-6:eq(0)');
    });
</script>