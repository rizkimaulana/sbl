<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manifest_cabang extends CI_Controller {

    var $folder = "manifest";

    public function __construct() {

        parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(MANIFEST_CABANG, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'manifest');
        $this->load->model('manifestcabang_model');
        $this->load->model('manifestcabang_model', 'r');
    }

    public function index() {
        $data = array(
            'pic' => $this->manifestcabang_model->get_cabang('order by id_affiliate desc')->result_array(),
        );
        $this->template->load('template/template', $this->folder . '/manifest_cabang', ($data));
    }

    public function ajax_list() {
        $list = $this->r->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row[] = '<tr>';
            $row = array();


            $row[] = $no;

            $row[] = '<td width="20%" >' . $r->cluster . '</strong><br>
	             			 <strong>NAMA : (' . $r->namakoordinator . ')<br>
	             			
	             			 Telp : ' . $r->telp . '<br>
	             			 Provinsi : ' . $r->provinsi . '<br>
	             			 Kota/kab : ' . $r->kabupaten . '
	               			 </td>';
            $row[] = '<td width="10%" >  <strong>' . $r->jml_jamaah . '<strong> </td>';
            $row[] = '<td width="10%" >  <strong>' . $r->jml_approvel . '<strong> </td>';

            $row[] = '<td><div class="btn-group"><button type="submit" formaction="' . base_url() . 'manifest/detail_manifestcabang/data_manifest/' . $r->cluster . '"  class="btn btn-info btn-sm" title="INPUT MANIFEST"><i class="glyphicon glyphicon-pencil"></i>KELOLAH MANIFEST</a></a></div></td>';
            $row[] = '</tr>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->r->count_all(),
            "recordsFiltered" => $this->r->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function a() {
        $id_user = $this->session->userdata('id_user');
        $sql = "call group_admin_cabang(?) ";
        $query = $this->db->query($sql, array('admin' => $id_user))->result_array();
        foreach ($query as $key => $value) {
            $cabang = $value['id_user'];
        }
        print_r($cabang);
    }

}
