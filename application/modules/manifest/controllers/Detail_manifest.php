<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Detail_manifest extends CI_Controller {

    var $folder = "manifest";

    public function __construct() {

    parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(MANIFEST, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'manifest');
        $this->load->model('Detail_manifest_model', 'mod');
    }
    
    public function detail() {
        $tahun =  $this->uri->segment(4);
        $bulan =  $this->uri->segment(5);
        $data = array('tahun' => $tahun, 'bulan' => $bulan);
        $this->template->load('template/template', $this->folder . '/detail_data_manifest', $data);
    }

    public function ajax_list() {
        $tahun = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        $bulan = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : '';
                
        $list = $this->mod->get_datatables($tahun, $bulan);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $this->db->select('nama');
            $affiliate = $this->db->get_where('affiliate', array('id_user'=>$item['id_affiliate']))->row_array();
            $row[] = $no;
            $row[] = $item['id_affiliate'].' '.$affiliate['nama'];
            $row[] = $item['id_jamaah'] . '<br>' . $item['nama'];
            $row[] = $item['paket'];
            $row[] = !empty($item['room_category']) ? $item['room_category'] : '';
            $row[] = !empty($item['departure']) ? $item['departure'] : '';
            $row[] = $item['telp'];
            $row[] = $item['sex'];
            $row[] = $item['tempat_lahir'] . '<br>' . $item['tanggal_lahir'];
            $row[] = $item['no_passport'] . '<br>' . $item['nama_passport'];
            $row[] = $item['issue_date'] . '<br>' . $item['issue_office'];
            
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod->count_all($tahun, $bulan),
            "recordsFiltered" => $this->mod->count_filtered($tahun, $bulan),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    function export_to_excell() {
        if (!$this->general->privilege_check(DATA_MANIFEST, 'view'))
            $this->general->no_access();

        $tahun = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        $bulan = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : '';
        
        $data_manifest = $this->mod->get_detail_data_manifest($tahun, $bulan);
       
        $list_data = array();
        foreach ($data_manifest as $item) {
            $this->db->select('nama');
            $affiliate = $this->db->get_where('affiliate', array('id_user' => $item['id_affiliate']))->row_array();
            $this->db->select('nama');
            $keluarga = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $item['keluarga']))->row_array();
            $hub_keluarga = $this->db->get_where('family_relation', array('id_relation' => $item['hubkeluarga']))->row_array();
            
            $item['nama_embarkasi'] = $item['departure'];
            $item['nama_produk'] = $item['paket'];
            $item['nama_room_category'] = $item['room_category'];
            $item['jenis_kelamin'] = $item['sex'];
            
            $item['hub_relasi'] = !empty($hub_keluarga['keterangan']) ? $hub_keluarga['keterangan'] : '';
            
            $item['nama_passport'] = !empty($item['nama_passport']);
            $item['no_passport'] = !empty($item['no_pasport']);
            $item['issue_date'] = !empty($item['isui_date']);
            $item['issue_office'] = !empty($item['issue_office']);
            
            $item['status_identitas'] = !empty($item['status_identitas']) ? 'Ya' : 'Tidak';
            $item['status_kk'] = !empty($item['status_kk']) ? 'Ya' : 'Tidak';
            $item['status_foto'] = !empty($item['status_photo']) ? 'Ya' : 'Tidak';
            $item['status_passport'] = !empty($item['status_pasport']) ? 'Ya' : 'Tidak';
            $item['status_vaksin'] = !empty($item['status_vaksi']) ? 'Ya' : 'Tidak';
            $item['status_buku_nikah'] = !empty($item['status_buku_nikah']) ? 'Ya' : 'Tidak';
            $item['status_akte'] = !empty($item['status_akte']) ? 'Ya' : 'Tidak';
           
            $item['nama_affiliate'] = !empty($affiliate['nama']) ? $affiliate['nama'] : '';
            
            $item['tipe_kamar'] = $item['type'];
            $item['tgl_berangkat'] = $item['date_schedule'];
            $item['keluarga'] = !empty($keluarga['nama']) ? $keluarga['nama'] : '';
            $list_data[] = $item;
        }

        $data = array(
            'data_manifest' => $list_data
        );
        //$this->myDebug($list_data);
        $this->load->view('manifest/view_laporan', ($data));
    }
    
    public function muhrim() {
        $this->template->load('template/template', $this->folder . '/muhrim');
    }

    public function ajax_muhrim_list() {
        $keyword = $this->uri->segment(3) ? $this->uri->segment(3) : '';
        $keyword = urldecode($keyword);
        
        $list = $this->biaya_model->get_data_muhrim($keyword);
        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : '';
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah' => $r->tipe_jamaah))->row_array();
            $row[] = '<td width="20%" >Invoice : ' . $r->invoice . '</strong><br>
	             			 <strong>(' . $r->id_jamaah . ' - ' . $r->nama . ')<br>
	             			 Telp : ' . $r->telp . '<br>
	             			 ' . $tipe['nama'] . '
	               			 </td>';
            $product = $this->db->get_where('product', array('id_product' => $r->id_product))->row_array();
            $category = $this->db->get_where('category', array('id_room_category' => $r->category))->row_array();
            $row[] = '<td width="18%" >' . $product['nama'] . ' - ' . $category['category'] . '<br>
	             			 </td>';
            $row[] = '<td width="18%" >Tgl Daftar : ' . date('d-m-Y', strtotime($r->tgl_daftar)) . '<br>
	             			 Tgl Aktivasi : ' . date('d-m-Y', strtotime($r->update_date)) . '<br>
                      </td>';
            $biaya = $this->db->get_where('setting', array('id_setting' => '9'))->row_array();
            $row[] = number_format($biaya['key_setting']);

            $aktivasi_biaya_lain = $this->db->get_where('aktivasi_biaya_lain', array('registrasi_jamaah_id_jamaah' => $r->id_jamaah, 'tipe_biaya' => '144'))->row_array();

            if (count($aktivasi_biaya_lain['registrasi_jamaah_id_jamaah']) > 0) {
                $row[] = '<font style="color:green;"><strong>Sudah di bayar</strong></font>';
            } else {
                $row[] = '<a title="Mutasi" class="btn btn-sm btn-primary" href="' . base_url() . 'biaya_lain/proses_muhrim/' . $r->id_jamaah . '">
	                            <i class="fa fa-pencil"></i>Proses
	                        </a> ';
            }

            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function proses_muhrim($id_jamaah) {

        $registrasi = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $id_jamaah))->row_array();
        $biaya = $this->db->get_where('setting', array('id_setting' => '9'))->row_array();
        $data = array(
            'id_jamaah' => $registrasi['id_jamaah'],
            'nama' => $registrasi['nama'],
            'id_booking' => $registrasi['id_booking'],
            'id_affiliate' => $registrasi['id_affiliate'],
            'biaya' => $biaya['key_setting'],
            'select_bank' => $this->_select_bank(),
            'select_pay_method' => $this->_select_pay_method(),
        );

        $this->template->load('template/template', $this->folder . '/proses_muhrim', $data);
    }

    function update_muhrim() {
        $data = $this->input->post(null, true);
        //$this->myDebug($data);
        try {
            //SAVE FILE IMAGE
            $flag = 0;
            $rename_file = array();
            for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                if ($_FILES['pic']['name'][$i]) {
                    $rename_file[$i] = 'pic' . ($i + 1) . '_mutasi_nama_' . $_FILES['pic']['name'][$i];
                    $flag++;
                } else {
                    $rename_file[$i] = '';
                }
            }
            if ($flag > 0) {
                $this->load->library('upload');
                $this->upload->initialize(array(
                    "file_name" => $rename_file,
                    'upload_path' => './assets/images/bukti_pembayaran/',
                    'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                    'max_size' => '2000' //Max 2MB
                ));

                if ($this->upload->do_multi_upload("pic")) {
                    $info = $this->upload->get_multi_upload_data();
                    foreach ($info as $in) {
                        $picx = substr($in['file_name'], 0, 4);
                        $data[$picx] = $in['file_name'];
                    }
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    echo "Errors Occured : "; //sini aja lah
                    print_r($error);
                }
            }
            //END SAVE FILE IMAGE
            if (!$this->biaya_model->proses_muhrim($data)) {
                throw new Exception('Ada kesalahan ketika simpan data mutasi nama!');
            }
            $msg = 'Biaya Akomodasi Jamaah ' . $data['nama'] . ' berhasil diproses!';
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
        }

        $this->session->set_flashdata('info', $msg);
        redirect('biaya_lain/cetak_invoice_muhrim/'.$data['id_jamaah']);
    }
    
    function cetak_invoice_muhrim($id_jamaah){
        $getdata = $this->biaya_model->get_data_aktivasi($id_jamaah,'144')->result();
        $data = array();
        foreach($getdata as $row){
            $data = array(
                        'id' => $row->id,
                        'id_booking' => $row->id_booking,
                        'id_jamaah' => $row->registrasi_jamaah_id_jamaah,
                        'nama' => $row->nama,
                        'alamat' => $row->alamat,
                        'telp' => $row->telp,
                        'biaya_akomodasi' => $row->nominal,
                        'keterangan' => $row->keterangan,
                        'pay_method' => $row->payment_method,
                        'payment_date' => $row->payment_date,
                        'bank_transfer' => $row->bank,
                        'kd_trans' => $row->kd_trans
                    );
           
        }
        
        $this->load->view('inv_muhrim', $data);
    }

    private function _select_pay_method() {
        return $this->db->get('pay_method')->result();
    }

    private function _select_bank() {
        return $this->db->get('bank')->result();
    }
    
    function export(){
        if (!$this->general->privilege_check(DATA_MANIFEST, 'view'))
            $this->general->no_access();
        $tahun =  $this->uri->segment(4);
        $bulan =  $this->uri->segment(5);
        $data = array('tahun' => $tahun, 'bulan' => $bulan);
        $this->template->load('template/template', $this->folder . '/export', $data);
    }
    
    function ajax_export(){
        $tahun = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        $bulan = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : '';
        
        $data_manifest = $this->mod->get_detail_data_manifest($tahun, $bulan);
       
        $list = array();
        $no = 0;
        foreach ($data_manifest as $item) {
            $no++;
            
            $row = array();
            $this->db->select('nama, nama_affiliate');
            $affiliate = $this->db->get_where('affiliate', array('id_user'=>$item['id_affiliate']))->row_array();
            $keluarga = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $item['keluarga']))->row_array();
            $hub_keluarga = $this->db->get_where('family_relation', array('id_relation' => $item['hubkeluarga']))->row_array();
            
            $row[] = $no;
            $row[] = $item['id_jamaah'];
            $row[] = $affiliate['nama_affiliate'];
            $row[] = $item['paket'];
            $row[] = !empty($item['room_category']) ? $item['room_category'] : '';
            $row[] = $item['type'];
            $row[] = $item['telp'];
            $row[] = $item['nama_passport'];
            $row[] = $item['sex'];
            $row[] = $item['tempat_lahir'];
            $row[] = $item['tanggal_lahir'];
            $row[] = $item['no_passport'];
            $row[] = $item['issue_office'];
            $row[] = $item['issue_date'];
            $row[] = $item['expired'];
            $row[] = !empty($hub_keluarga['keterangan'])? $hub_keluarga['keterangan'].' '.$keluarga['nama'] : '';
            $row[] = $item['id_affiliate'];
            $row[] = $affiliate['nama'];
            $row[] = $item['departure'];
            $row[] = $item['date_schedule'];
            
            $list[] = $row;
        }

        $output = array(
            'data' => $list
        );
        //$this->myDebug($output);
        echo json_encode($output);
    }
}
