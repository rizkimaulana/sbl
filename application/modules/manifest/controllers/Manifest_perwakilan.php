<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manifest_perwakilan extends CI_Controller{ 
	var $folder = "manifest";
  public function __construct(){
    
  parent::__construct();
    if(!$this->session->userdata('is_login'))redirect('frontend/login');
    if(!$this->general->privilege_check(MANIFEST_GM,'view'))
        $this->general->no_access();
    $this->session->set_userdata('menu','manifest');  
    $this->load->model('manifestperwakilan_model');
    $this->load->model('manifestperwakilan_model','r');


  }



	
	
	public function index(){

	   $this->template->load('template/template', $this->folder.'/manifest_perwakilan');
	}


   

		public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
      $row[] ='<tr>'; 
			$row = array();
            

			$row[] = $no;

                    $row[] ='<td width="20%" >'.$r->id_perwakilan.'</strong><br>
	             			 <strong>NAMA : ('.$r->namakoordinator.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 Provinsi : '.$r->provinsi.'<br>
	             			 Kota/kab : '.$r->kabupaten.'
	               			 </td>';
	                 $row[] ='<td width="10%" >  <strong>'.$r->jml_jamaah.'<strong> </td>';
	                 if ($r->belum_cluster > 0){
	                 $row[] ='<td width="10%">  <h5> <span class="label label-danger"><strong>'.$r->belum_cluster.'<strong> </spam></h5> </td>';
	            	 }else{
	            	 $row[] ='<td width="10%"> <strong>'.$r->belum_cluster.'<strong> </td>';	
	            	 }
	                 $row[] ='<td width="10%" >  <strong>'.$r->sudah_cluster.'<strong> </td>';
                   $row[] ='<td width="10%" >  <strong>'.$r->jml_approvel.'<strong> </td>';
                 
                     $row[] = '<td><div class="btn-group"><button type="submit" formaction="'.base_url().'manifest/detail_manifestperwakilan/data_manifest/'.$r->id_perwakilan.'"  class="btn btn-info btn-sm" title="INPUT MANIFEST"><i class="glyphicon glyphicon-pencil"></i>KELOLAH MANIFEST</a></a></div></td>';
              $row[] ='</tr>'; 
			$data[] = $row;
     
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	
}
