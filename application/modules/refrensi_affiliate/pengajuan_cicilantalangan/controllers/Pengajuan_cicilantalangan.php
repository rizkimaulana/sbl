<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan_cicilantalangan extends CI_Controller{
	var $folder = "pengajuan_cicilantalangan";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(INPUT_CICILAN_TALANGAN,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','registration');	
		$this->load->model('cicilantalangan_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/pengajuan_cicilan');
		
	}
	

	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	private function _select_kelamin(){
		$kdstatus = array('2', '3');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	private function _select_kawin(){
		$kdstatus = array('4', '5');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	  
	}
	private function _select_bank(){
		
		return $this->db->get('bank')->result();
	}
	private function _select_pendidikan(){
		
		return $this->db->get('pendidikan')->result();
	}
	private function _select_jenis_pekerjaan(){
		
		return $this->db->get('jenis_pekerjaan')->result();
	}
	private function _select_status_karyawan(){
		
		return $this->db->get('status_karyawan')->result();
	}
	function add_ajax_kab($id_prov){
		    $query = $this->db->get_where('wilayah_kabupaten',array('provinsi_id'=>$id_prov));
		    $data = "<option value=''>- Select Kabupaten -</option>";
		    foreach ($query->result() as $value) {
		        $data .= "<option value='".$value->kabupaten_id."'>".$value->nama."</option>";
		    }
		    echo $data;
	}
		
	function add_ajax_kec($id_kab){
	    $query = $this->db->get_where('wilayah_kecamatan',array('kabupaten_id'=>$id_kab));
	    $data = "<option value=''> - Pilih Kecamatan - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
		
	function add_ajax_des($id_kec){
	    $query = $this->db->get_where('wilayah_desa',array('kecamatan_id'=>$id_kec));
	    $data = "<option value=''> - Pilih Desa - </option>";
	    foreach ($query->result() as $value) {
	        $data .= "<option value='".$value->kecamatan_id."'>".$value->nama."</option>";
	    }
	    echo $data;
	}
	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->cicilantalangan_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	               
	               
	                
	                // $rows .='<td width="10%" style="vertical-align:middle"><img src="'.base_url('data_jamaah/jamaah_aktif/gambar/'.$r->id_jamaah.'?height=80&width=1').'"></td>';
	                $rows .='<td width="20%" >'.$r->id_pengajuan.'</strong><br>
	             			 <strong>('.$r->nama.')<br>
	             			 Identitas : '.$r->identitas.'<br>
	             			 Jenis Kelamin : '.$r->sex.'<br>
	             			 tgl lahir : '.$r->tgl_lahir.'<br>
	             			 tempat lahir : '.$r->tempat_lahir.'<br>
	             			 No Telp : '.$r->telp.'<br>
	             			 Email : '.$r->email.'<br>
	             			 Status menikah : '.$r->status_nikah.'<br>
	             			 ibu kandung : '.$r->ibu_kandung.'<br>
	             			 Pendidikan : '.$r->pendidikan_terakhir.'
	               			 </td>';
	               	 $rows .='<td width="20%" >Alamat : '.$r->alamat.'<br>
	               	 		 Provinsi : '.$r->provinsi.'<br>
	             			 kabupaten: '.$r->kabupaten.'<br>
	             			 Kecamatan: '.$r->kecamatan.'<br>
	             			 ALAMAT DOMISILI: '.$r->alamat_domisili.'
	               			 </td>';
	               	 $rows .='<td>Perusahaan : '.$r->nama_perusahaan.'<br>
	             			 Jns Pekerjaan : '.$r->jenis_pekerjaan.'<br>
	             			 Status Pekerjaan : '.$r->status_karyawan.' <br>
	             			 Jabatan : '.$r->jabatan.' <br>
	             			 No Karyawan : '.$r->no_pegawai.' <br>
	             			 No SK : '.$r->no_sk.' <br>
	             			 Penghasilan Perbulan :  '.number_format ($r->gaji).' <br>
	             			 Cicilan Via Bank : '.$r->via_bank.' 
	               			 </td>';
	                 $rows .='<td>Paket : '.$r->product.' '.$r->room_category.'<br>
	             			 Bulan Berangkat : '.$r->bln_berangkat.'<br>
	             			 Waktu Tunggu : '.$r->bulan_menunggu.' Bulan <br>
	             			 Harga Paket :  '.number_format ($r->harga_paket).' <br>
	             			 
	               			 </td>';		

	                $rows .='<td width="18%" align="center">';
	                
	
	                 $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="TIDAK ACC"  onclick="update_status_tolak('."'".$r->id_pengajuan."'".')"><i class="glyphicon glyphicon-trash"></i> TIDAK ACC</a> <br><br>';
	          
	                $rows .='<a class="btn btn-sm btn-primary" href="'.base_url().'pengajuan_cicilantalangan/aktivasi/'.$r->id_pengajuan.'"" title="ACC PENGAJUAN" ><i class="fa fa-pencil"></i> ACC PENGAJUAN</a> ';
	                              
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'pe/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	


	
	

	public function add(){
	     
	     if(!$this->general->privilege_check(INPUT_CICILAN_TALANGAN,'add'))
		    $this->general->no_access();
		
	    $data = array(
	    			
	    			'provinsi'=>$this->cicilantalangan_model->get_all_provinsi(),
	    			'kabupaten'=>$this->cicilantalangan_model->get_all_kabupaten(),
	    			'select_status'=>$this->_select_status(),
	    			'select_bank'=>$this->_select_bank(),
	    			'select_kelamin'=>$this->_select_kelamin(),
	    			'select_kawin'=>$this->_select_kawin(),
	    			'select_pendidikan'=>$this->_select_pendidikan(),
	    			'select_jenis_pekerjaan'=>$this->_select_jenis_pekerjaan(),
	    			'select_status_karyawan'=>$this->_select_status_karyawan(),
	    			'select_embarkasi'=>$this->_select_embarkasi(),
		    	    'select_roomcategory'=>$this->_select_roomcategory(),
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);	
	}
	

	
	public function aktivasi($id_pengajuan){
	


	    if(!$this->general->privilege_check(INPUT_CICILAN_TALANGAN,'view'))
		    $this->general->no_access();
	    
	    $id_pengajuan = $this->uri->segment(3);
	    $aktivasi = $this->cicilantalangan_model->get_pic_aktivasi($id_pengajuan);
	    if(!$aktivasi){
	        show_404();
	    }
	   
	    $data = array(
	    		
	    		'select_bank_transfer'=>$this->_select_bank_transfer(),
	       		 'aktivasi'=>$aktivasi,
	       		 'select_pay_method'=>$this->_select_pay_method(),
	       		 );
	
	    $this->template->load('template/template', $this->folder.'/aktivasi_acc_pengajuan',($data));

	}


	public function save(){


	   $this->form_validation->set_rules('radio','radio','required|trim');
	   if($this->form_validation->run()==true){//jika validasi dijalankan dan benar
            
			 
		$data = $this->input->post(null,true);
	    $schedule = $this->input->post('radio');
	    $inputan = '';
	    foreach($schedule as $value){
	    	$inputan .= ($inputan!=='') ? ',' : '';
	    	$inputan .= $value;
	    }
	   
	  $send = $this->cicilantalangan_model->save($data,$inputan);

	          $this->session->set_flashdata('info', "Pengajuan CICILAN berhasil ditambahkan.");
	          redirect('pengajuan_cicilantalangan');
		

	}else{
		
	
	    redirect('pengajuan_cicilantalangan');
	    $this->session->set_flashdata('info', "ERROR INPUT");
	}
	}

	

	function searchItem(){
            
			 
             $paket = $this->input->post('q');
             $departure = $this->input->post('l');
             $datepicker_tahun_keberangkatan = $this->input->post('s');
             $datepicker_keberangkatan = $this->input->post('t');
             
             if(!empty($paket)){
                 $this->cicilantalangan_model->searchItem($paket,$departure,$datepicker_tahun_keberangkatan,$datepicker_keberangkatan);
             }
             else{
                 echo'<tr><td colspan="12"><h2 style="color: #9F6000;">Sorry ! no search result found</h2></td></tr>';
             }
    }

    private function _select_roomcategory(){
	
	    return $this->db->get('room_category')->result();
	}

	private function _select_embarkasi(){
	
	    return $this->db->get('embarkasi')->result();
	}


	public function update_status_tolak($id_pengajuan)
	{
		// if(!$this->general->privilege_check(FEE_INPUT_CICILAN_TALANGAN,'edit'))
		//     $this->general->no_access();
		$send = $this->cicilantalangan_model->update_status_tolak($id_pengajuan);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('pengajuan_cicilantalangan');

	}

	private function _select_bank_transfer(){
		
		return $this->db->get('bank')->result();
	}

	private function _select_pay_method(){
		
		return $this->db->get('pay_method')->result();
	}
	
}
