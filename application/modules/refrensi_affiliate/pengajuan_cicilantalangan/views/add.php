
 <div id="page-wrapper">
    
    
       <div class="row">
       	<!-- <div class="col-lg-7"> -->
       		<div class="panel panel-default">
       			<div class="panel-body">
			        <h3 class="page-header">Add Pengajuan Cicilan</h3>
			       
			         <?php if($this->session->flashdata('info')) { ?>
			                            <div class="alert alert-danger">  
			                                    <a class="close" data-dismiss="alert">x</a>  
			                                    <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
			                            </div>
			                        <?php } ?>
       
        <form action="<?php echo base_url();?>pengajuan_cicilantalangan/save" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
             <div class="panel panel-default">
              <div class="col-sm-6 col-md-12">
                <div class="block-flat">
                  <div class="header">              
                    <h4><strong>UBAH JADWAL KEBERANGKATAN</strong></h4>
                  </div>
                  <div class="content">
                  <div class="form-group">
                    <label class="col-lg-2 control-label">Category</label>
                    <div class="col-lg-5">
                      
                     <select required class="form-control" name="paket" id="paket">
                        <option value="">Pilih Category</option>
                       <!--  <?php foreach($select_product as $sp){?>
                            <option required value="<?php echo $sp->id_product;?>"><?php echo $sp->nama;?> </option>
                        <?php } ?> -->
                      <?php foreach($select_roomcategory as $se){?>
                        <option value="<?php echo $se->id_room_category;?>"><?php echo $se->category;?></option>
                    <?php } ?>
                    </select>
                    </div>
                  </div>

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Embarkasi</label>
                      <div class="col-lg-5">
                      <select required class="form-control" name="departure" id="departure" >
                          <option value="">Pilih Embarkasi</option>
                          <?php foreach($select_embarkasi as $se){?>
                              <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                 <!--   <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div> -->
                <div class="form-group">
                          <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' id='datepicker_tahun_keberangkatan1'> -->
                                <input type='text' name="datepicker_tahun_keberangkatan" id="datepicker_tahun_keberangkatan1" class="form-control" placeholder="YYYY/2016" required/>
                             <!--    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                             
                          </div>
                    </div>

                     <div class="form-group">
                          <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                <input type='text' name="datepicker_keberangkatan" id="datepicker_keberangkatan1" class="form-control" placeholder="MM/January" required/>
                               <!--  <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                          </div>

                    </div>
                    
                
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-10">
                        <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check Waktu Keberangkatan</button>
                         
                    </div>
                  </div>
                
                  <div class="table-responsive">
                   <table id="data-table" style="width:100%" align="center" class="table table-striped table-bordered table-hover" >
                    <caption>Pilih Jadwal</caption>
                    <thead>
                        <tr>
                            <!-- <th width="2%">Check </th> -->
                            <th width="8%"><strong>Paket</strong></th>
                            <th width="10%"><strong>Embarkasi</strong></th>
                            <th width="10%"><strong>Tgl/Bln </strong></th>
                            <th width="8%"><strong>Waktu </strong></th>
                            <th width="5%"><strong>Jml Kursi</strong></th>
                            <th width="5%"><strong>Tipe Kamar</strong></th>
                            <th width="5%"><strong>Category</strong></th>
                            <th width="5%"><strong>Keterangan</strong></th>
                            <th width="5%"><strong>Harga</strong></th>
                            <th width="5%"><strong>Status Jadwal</strong></th>
                            <th width="5%"><strong>maskapai</strong></th>
                            <th width="5%"><strong>PILIH</strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <tr><td colspan="12"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>

                    </tbody>
                </table>
                </div>

                  </div>

 			 </div>

 			  </div>
                <div class="panel-body">  

	                <div class="form-group">
	                    <label class="col-lg-2 control-label" >Nama lengkap</label>
	                    <div class="col-lg-5">
	                      <input name="nama" class="form-control" >
	                    </div>
	                </div>
	                 <div class="form-group">
	                    <label class="col-lg-2 control-label">No Identitas</label>
	                    <div class="col-lg-5">
	                      <input name="identitas" class="form-control" >
	                    </div>
	                  </div>
	                  <div class="form-group">
	                   <label class="col-lg-2 control-label">Jenis Kelamin</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_kelamin">
		                	<option value=''>- PILIH JENIS KELAMIN -</option>
		                    <?php foreach($select_kelamin as $sk){?>
		                        <option value="<?php echo $sk->keterangan;?>"><?php echo $sk->keterangan;?></option>
		                    <?php } ?>
		                </select>
		           </div>
		           </div>

		            <div class="form-group">
	                    <label class="col-lg-2 control-label">Tempat tanggal Lahir</label>
	                    <div class="col-lg-5">
	                      <input name="tempat_lahir" class="form-control"> 
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">Tanggal Lahir</label>
	                    <div class="col-lg-5">
	                     <div class='input-group date' id='datepicker'>
	                          <input type='text' name="tgl_lahir" id="datepicker" class="form-control" placeholder="dd/mm/yyyy "/>
	                          <span class="input-group-addon">
	                            <span class="glyphicon glyphicon-calendar"></span>
	                          </span>
                        </div>
	                    </div>
	                  </div>

	                 
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">No Tlp</label>
	                    <div class="col-lg-5">
	                      <input name="telp" class="form-control" >
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">E-Mail</label>
	                    <div class="col-lg-5">
	                      <input name="email" class="form-control" >
	                    </div>
	                  </div>

	                  	<div class="form-group">
	                    <label class="col-lg-2 control-label">Provinsi</label>
	                    <div class="col-lg-5">
	                      
                            <select name="provinsi_id" class="form-control chosen-select" id="provinsi" >
                                <option value=''>- Select Provinsi -</option>
                                <?php foreach($provinsi as $Country){
                                    echo '<option value="'.$Country->provinsi_id.'">'.$Country->nama.'</option>';
                                } ?>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kabupaten</label>
	                    <div class="col-lg-5">
	                      
                            <select name="kabupaten_id" class="form-control" id="kabupaten">
                                <option value=''>Select Kabupaten</option>
                            </select>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Kecamatan</label>
	                    <div class="col-lg-5">
	                      <select name="kecamatan_id" class="form-control" id="kecamatan">
                                <option value=''>Select Kecamatan</option>
                            </select>
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="alamat" ></textarea>
	                    </div>
	                  </div>

	                  	<div class="form-group">
	                    <label class="col-lg-2 control-label">Alamat Domisili</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="alamat_domisili" ></textarea>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                   <label class="col-lg-2 control-label">Status Nikah</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_kawin">
		                	<option value=''>- PILIH STATUS NIKAH -</option>
		                    <?php foreach($select_kawin as $sk){?>
		                        <option value="<?php echo $sk->keterangan;?>"><?php echo $sk->keterangan;?></option>
		                    <?php } ?>
		                </select>
		           </div>
		           </div>
		           <div class="form-group">
	                   <label class="col-lg-2 control-label">Pendidikan Terakhir</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_pendidikan">
		                	<option value=''>- PILIH Pendidikan Terakhir -</option>
		                    <?php foreach($select_pendidikan as $sk){?>
		                        <option value="<?php echo $sk->pendidikan;?>"><?php echo $sk->pendidikan;?></option>
		                    <?php } ?>
		                </select>
		           </div>
		           </div>
		            <div class="form-group">
	                    <label class="col-lg-2 control-label">Nama Ibu Kandung</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="ibu_kandung" ></textarea>
	                    </div>
	                  </div>
	                   <div class="form-group">
	                    <label class="col-lg-2 control-label">Nama Prusahaan</label>
	                    <div class="col-lg-5">
	                      <textarea class="form-control" name="nama_perusahaan" ></textarea>
	                    </div>
	                  </div>

	                   <div class="form-group">
	                   <label class="col-lg-2 control-label">Jenis Pekerjaan</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_jenis_pekerjaan">
		                	<option value=''>- PILIH Jenis Pekerjaan -</option>
		                    <?php foreach($select_jenis_pekerjaan as $sk){?>
		                        <option value="<?php echo $sk->jenis_pekerjaan;?>"><?php echo $sk->jenis_pekerjaan;?></option>
		                    <?php } ?>
		                </select>
		           </div>
		           </div>
		           <div class="form-group">
	                   <label class="col-lg-2 control-label">Status Pekerjaan</label>
	                   <div class="col-lg-5">
		                <select class="form-control" name="select_status_karyawan">
		                	<option value=''>- PILIH Status Pekerjaan -</option>
		                    <?php foreach($select_status_karyawan as $sk){?>
		                        <option value="<?php echo $sk->status_karyawan;?>"><?php echo $sk->status_karyawan;?></option>
		                    <?php } ?>
		                </select>
		           </div>
		           </div>
	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Jabatan</label>
	                    <div class="col-lg-5">
	                      <input name="jabatan" class="form-control" >
	                    </div>
	                  </div>
	                 
	                  	<div class="form-group">
	                    <label class="col-lg-2 control-label">ID Pegawai</label>
	                    <div class="col-lg-5">
	                      <input name="no_pegawai" class="form-control" >
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Penghasilan Perbulan</label>
	                    <div class="col-lg-5">
	                      <input name="gaji" class="form-control" >
	                    </div>
	                  </div>

	                  <div class="form-group">
	                    <label class="col-lg-2 control-label">Cicilan Via Bank</label>
	                    <div class="col-lg-5">
	                       <select class="form-control" name="select_bank">
	                       	<option>- Select Bank -</option>
		                    <?php foreach($select_bank as $sb){?>
		                        <option value="<?php echo $sb->nama;?>"><?php echo $sb->nama;?></option>
		                    <?php } ?>
		                </select> 
	                    </div>
	                  </div>
	                 <div class="form-group">
	                    <label class="col-lg-2 control-label">No SK</label>
	                    <div class="col-lg-5">
	                      <input name="no_sk" class="form-control" >
	                       
	                    </div>
	                  </div>

	                  
	                  
	            <!--           <div class="form-group">
					        <label class="col-lg-2 control-label">FOTO</label>
					        <div class="col-lg-5">
					            <input type="file" name="pic[]"  class="form-control">
					        </div>
					    </div> -->

	              </div>
              <!-- </div> -->

            <!-- </div> -->
               </div>
            <!-- </div>  -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>pengajuan_cicilantalangan" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       <!-- </div> -->
    </div>
</div>
<!-- /#page-wrapper -->

<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker_keberangkatan1').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
  </script>
<script>
        $(document).ready(function(){
            $("#provinsi").change(function (){
                var url = "<?php echo site_url('pengajuan_cicilantalangan/add_ajax_kab');?>/"+$(this).val();
                $('#kabupaten').load(url);
                return false;
            })
            
            $("#kabupaten").change(function (){
                var url = "<?php echo site_url('pengajuan_cicilantalangan/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan').load(url);
                return false;
            })
            
            $("#kecamatan").change(function (){
                var url = "<?php echo site_url('pengajuan_cicilantalangan/add_ajax_des');?>/"+$(this).val();
                $('#desa').load(url);
                return false;
            })
        });


</script>
     
 <script type="text/javascript">
 	jQuery(document).ready(function(){
	$(".chosen-select").chosen({width: "95%"}); 
});


 </script>

 <script>
    $("#check").on('click', function (e){
    e.preventDefault();
    var paket = $("#paket").val();
    var departure = $("#departure").val();
    var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
    var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
    console.log(paket);
    console.log(departure);
    console.log(datepicker_tahun_keberangkatan);
    console.log(datepicker_keberangkatan);
    
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>pengajuan_cicilantalangan/searchItem',
             // data:'from='+from+'&to='+to
            data:'q='+paket+'&l='+departure+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
    
});
</script>