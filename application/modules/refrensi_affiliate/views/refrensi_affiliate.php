<div class="page-head">
    <h2>Downline</h2>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">LIST Downline</li>
    </ol>
</div>  

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Downline</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                   
                <!--<form action="<?php echo base_url(); ?>laporan/export_pindah_paket" role="form" method="POST"  class="form-horizontal">
                    <div class="form-group">
                        
                        <div class="col-sm-2">
                            
                            <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                        </div>
                        <div class="col-sm-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                        </div>
                    </div>
                </form>-->
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Downline</th>
                                    <th>ID Downline</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script type="text/javascript">
    var table;
    $(document).ready(function () {

        //datatables

        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('refrensi_affiliate/ajax_list') ?>",
                "type": "POST",
                "data": function (data) {

                }
            },
        });



    });
</script>