<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refrensiaffiliate_model extends CI_Model {

    public function get_data($offset, $limit, $q = '') {

        $id_user = $this->session->userdata('id_user');
        $id = $this->session->userdata('id');
        $sql = " SELECT * FROM view_refrensi where IDsponsor='" . $id_user . " ' 
          ";

        if ($q) {

            $sql .= " AND Upline LIKE '%{$q}%' 
            		OR IDUpline LIKE '%{$q}%'";
        }
        $sql .= " ORDER BY IDUpline DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .= " LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    public function get_ajax_list() {
        $id_user = $this->session->userdata('id_user');
        $this->db->select('*');
        $this->db->where('IDsponsor',$id_user);
//        if (!empty($search)) {
//            $this->db->like('Upline', $search);
//            $this->db->or_like('IDUpline', $search);
//        }
        $this->db->order_by('IDUpline', 'DESC');
        return $this->db->get('view_refrensi');
    }

}
