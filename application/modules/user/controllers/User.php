<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	var $folder =   "user";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		$this->load->model('user_model');
		$this->session->set_userdata('menu','user_management');
		if(!$this->general->privilege_check(USER,'view'))
		    $this->general->no_access();
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/user');
	 
	}

	private function _render($view,$data = array()){
	    
	    // $this->load->view('header',$data);
	    // $this->load->view('sidebar');
	    // $this->load->view($view,$data);
	    $this->template->load('template/template', $this->folder.'/user',$data);
	    // $this->load->view('footer');

	}
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->user_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                $rows .='<td width="15%">'.$r->id_user.'</td>';
	                $rows .='<td width="20%">'.$r->nama.'</td>';
	                $rows .='<td width="15%">'.$r->username.'</td>';
	                $rows .='<td width="20%">'.$r->jabatan.'</td>';
	                $rows .='<td width="9%">'.$r->keterangan.'</td>';
	                $rows .='<td width="40%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'user/edit/'.$r->id.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';

	                  $rows .=' <a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_user('."'".$r->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';

	                  
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="7">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'user/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}

	

	public function add(){
	    
	    if(!$this->general->privilege_check(USER,'add'))
		    $this->general->no_access();

	    
	    $data = array('select_jabatan'=>$this->_select_jabatan(),
	    				'select_status'=>$this->_select_status(),
	    				'message'=>''
	    	);
        $this->template->load('template/template', $this->folder.'/add',$data);		
	    	   
	}

	public function edit(){
	    
	    if(!$this->general->privilege_check(USER,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('user',array('id'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	    $data = array('select_jabatan'=>$this->_select_jabatan(),
	    				'select_status'=>$this->_select_status()
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($data,$get));		
	    	   
	}

	private function _select_jabatan(){
	
	    return $this->db->get('privilege')->result();
	}

	private function _select_status(){

		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
		// $query = $this->db->query("SELECT keterangan FROM status where kdstatus in('1','0') ")->result();
		// return $query;
		  // return $this->db->get_where('status',array('kdstatus'=>0))->result();
	      // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}
	// private function _status(){
	    
	//     $status = array('1'=>'Active','0'=>'Non Active');
	    
	//     return $status;
	// }

	public function save(){
	 
		// $data['message']="<div class='alert alert-danger'>Username sudah digunakan</div>";
	 //     $data = $this->input->post(null,true);
	     
	 //     $send = $this->user_model->save($data);
	 //     if($send){ 
	        
	 //        $this->session->set_flashdata('info', "User berhasil ditambahkan.");
	 //        redirect('user');
		// }else{
		// 	 $this->session->set_flashdata('info', "Input Data error");
		// }


		$this->form_validation->set_rules('id_user','id_user','required|trim');

		if($this->form_validation->run()==true){
            $id_user=$this->input->post('id_user');
            $cek=$this->user_model->cek($id_user);
            if($cek->num_rows()>0){
                 //$data['message']="<div class='alert alert-danger'>Username sudah digunakan</div>";
                $this->session->set_flashdata('info', "Username sudah digunakan");
                // $this->template->load('template/template', $this->folder.'/add',$data);
                 redirect('user/add');
            }else{
            	$data = $this->input->post(null,true);
               $send = $this->user_model->save($data);
                $this->session->set_flashdata('info', "User berhasil ditambahkan.");
	 	       redirect('user');
            }
        }else{
            $data['message']="";
            $this->template->load('template/template', $this->folder.'/user',$data);
        }

	}
	public function update(){
	    
	     $data = $this->input->post(null,true);
	     $send = $this->user_model->update($data);
	     if($send)
	        redirect('user');
	}

	

	function delete(){
		 if(!$this->general->privilege_check(USER,'remove'))
		    $this->general->no_access();
		    
	     $id  = $this->uri->segment(4);
	     $send = $this->user_model->delete($id);
	     if($send)
	        redirect('user');
		
    }

    public function ajax_delete($id_user)
	{
		if(!$this->general->privilege_check(USER,'remove'))
		    $this->general->no_access();
		$send = $this->user_model->delete_by_id($id_user);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('user');
	}
        
        
    function ajax_data_user(){
        $list_data = $this->user_model->get_data_user();
        $data = array();
        $no = 1;
        foreach ($list_data as $item) {
            
            
            
            $row = array();
            $row[] = $no;
            $row[] = $item['id_user'];
            $row[] = $item['nama'];
            $row[] = $item['username'];
            $row[] = $item['jabatan'];
            $row[] = $item['keterangan'];
            
            $btnEdit = '<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'user/edit/'.$item['id'].'">
	                   <i class="fa fa-pencil"></i> Edit</a>';
            
            $btnDelete = '<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_user('."'".$item['id']."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
            
            $row[] = $btnEdit.' '.$btnDelete;
            $data[] = $row;
            $no++;
        }
        $output = array(
            "data" => $data,
        );
       
        echo json_encode($output);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */