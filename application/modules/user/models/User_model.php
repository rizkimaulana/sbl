<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model{
    var $table = 'user';
    var $order = array('id_user' => 'desc');
   
    private $_table="user";
    private $_primary="id_user";

    public function save($data){
        
        // $cek = $this->db->select('id_user')->where('username',$data['username'])
        //                 ->or_where('id_user',$data['nik'])->get('user')->num_rows();
        // if($cek)
        //      $this->session->set_flashdata('info', "Username sudah digunakan.");
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'id_user' => $data['id_user'],
            'nama' => $data['nama'],
            'username' => $data['username'],
            'password' => md5($data['password']),
            'jabatan_id' => $data['jabatan_id'],
            'status' => $data['status'],
        );       
        
        return $this->db->insert('user',$arr);
    }

    function cek($id_user){
        $query = $this->db->query("SELECT * FROM user Where id_user ='$id_user'");
        return $query;
    }

    public function update($data){
        
        $arr = array(
        
            'id_user' => $data['nik'],
            'nama' => $data['nama'],
            'jabatan_id' => $data['jabatan_id'],
            'status' => $data['status'],
        );       
        
        if($data['password']!=''){
            
            $arr['password'] = md5($data['password']);
        }
                
        return $this->db->update('user',$arr,array('id'=>$data['id_user']));
    }
    
    public function get_data($offset,$limit,$q=''){
    
        $sql = "SELECT a.*,b.jabatan,c.keterangan FROM user a 
                LEFT JOIN privilege b ON b.id = a.jabatan_id
				LEFT JOIN status c ON c.kdstatus = a.status
                WHERE 1=1 
                ";
        
        if($q){
            
            $sql .=" AND (a.nama LIKE '%{$q}%' 
                OR a.id_user LIKE '%{$q}%' 
                OR a.username LIKE '%{$q}%'
                OR b.jabatan LIKE '%{$q}%' 
                OR c.keterangan LIKE '%{$q}%')";
        }
        $sql .=" ORDER BY a.id DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     public function delete($id_user){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_user'=>$id_user));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
    
    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
    
    
    function get_data_user(){
         $sql = "SELECT a.*,b.jabatan,c.keterangan FROM user a 
                LEFT JOIN privilege b ON b.id = a.jabatan_id
				LEFT JOIN status c ON c.kdstatus = a.status
                WHERE 1=1 ORDER BY a.id DESC";
        return $this->db->query($sql)->result_array();
    }
    
}
