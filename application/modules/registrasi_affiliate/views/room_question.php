<div class="block-flat">
    <div class="header">	
        <?php if ($this->session->flashdata('info')) { ?>
            <div class="alert alert-danger">  
                <a class="close" data-dismiss="alert">x</a>  
                <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
            </div>
        <?php } ?>						
        <h3>Room Question</h3>
    </div>
    <div class="content">

        <h4>Apakah Jamaah Yang Anda Input, Ada Yang Mau Ganti Kamar Ke <strong>DOUBLE</strong> Atau <strong>TRIPLE</strong>?<br>
            Apabila iya Klik Tombol <strong>YES</strong> Apabila Tidak Klik Tombol <strong>NO</strong></h4>

        <h4>KETERANGAN</h4>
        <p>Default Kamar JAMAAH adalah QUARD,
            Apabilah Jamaah Mau Ganti Kamar Dari QUARD ke DOUBLE Atau TRIPLE dikenakan BIAYA
            Yang Tertera Dibawah Ini.</p>

        <div class="table-responsive">
            <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Room Type </th>
                        <th>Category </th>
                        <th>Harga </th>
                        <th>Kapasitas </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0;
                    foreach ($pic as $pi) {
                        $no++ ?>
                        <tr>

                            <td><?php echo $no ?> </td>
                            <td><?php echo $pi['type'] ?> </td>
                            <td><?php echo $pi['category'] ?> </td>
                            <td><?php echo number_format($pi['harga']) ?> </td>
                            <td><?php echo $pi['kapasitas'] ?> Orang</td>
                        </tr>

<?php } ?> 


                </tbody>
            </table>
        </div>

        <div class="spacer spacer-bottom">
            <button type="button" class="btn btn-block btn-primary btn-rad button" onclick="toggle_visibility('hideMe')"><strong>YES</strong></button>
            <form   class="form-horizontal" role="form" action='<?= base_url(); ?>registrasi_affiliate/update_room' method='POST'> 
                <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking'] ?>">
                <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule'] ?>">
                <input type="hidden" name="id_user_affiliate" value="<?php echo $booking['id_user_affiliate'] ?>">
                <input type="hidden" name="id_product" value="<?php echo $booking['id_product'] ?>">
                <input type="hidden" name="id_product" value="<?php echo $booking['id_product'] ?>">
                <input type="hidden" name="category" value="<?php echo $booking['category_id'] ?>">
                <input type="hidden" name="jumlah_jamaah" value="<?php echo $booking['jumlah_jamaah'] ?>">

                <input type="hidden" name="pre_double" value="<?php echo $booking['pre_double'] ?>">
                <input type="hidden" name="pre_triple" value="<?php echo $booking['pre_triple'] ?>">

                <div id="hideMe">


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>KETERANGAN</h4>
                                    <p>Prediksi Jumlah Kamar DOUBLE Atau TRIPLE Yang Bisa Anda ORDER.</p>
                                    <h4>ILUSTRASI</h4>
                                    <p>Apabila Anda MeRegistrasi Jamaah Sebanyak 5 orang, Berarti Jumlah Kamar Yang dapat anda Order <br>
                                        2 kamar untuk DOUBLE dan 1 kamar untuk TRIPLE, sisa Jamaah di QUARD.</p>
                                </div>

                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">DOUBLE </label>
                                        <div class="col-lg-10">
                                            <select required name="double"  class="form-control ">
                                                <?php
                                                for ($i = 0; $i <= $booking['pre_double']; $i++) {
                                                    ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div> 
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">TRIPLE </label>
                                        <div class="col-lg-10">
                                            <select required name="triple"  class="form-control ">
                                                <?php
                                                for ($i = 0; $i <= $booking['pre_triple']; $i++) {
                                                    ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> LANJUTKAN</button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- <button type="button" class="btn btn-block btn-danger btn-rad"><strong>NO</strong></button> -->
            <span>
                <?php
                echo '
                                
                                <a href="' . site_url('registrasi_affiliate') . '/report_registrasi/' . $booking['id_booking'] . '"  title="NO" class="btn btn-block btn-danger btn-rad" >  <strong>NO</strong></a>';
                ?>
            </span>
        </div>

    </div>
</div>

<script type="text/javascript">
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        if (e.style.display == 'block')
            e.style.display = 'none';
        else
            e.style.display = 'block';
    }

</script>

<style type="text/css">
    .button {
        display: block;


    }

    #hideMe {
        display: none;

    }

</style>

<script type="text/javascript">
    (function (global) {

        if (typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };
        }

    })(window);
</script>