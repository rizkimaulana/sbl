<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 7pt; text-align: center;}
		</style>';
 <table width="100%">
			<tr>
				<td colspan="2" class="h_kanan"><strong>PT SOLUSI BALAD LUMAMPAH</strong></td>
			</tr>
			<tr>
				<td width="20%"><strong>STRUK</strong>
					<hr width="500%">
				</td>
				<td class="h_kanan" width="80%">WISMA BUMI PUTRA- 5&7th Floor Jl. Asia Afrika No 141-149 Bandung</td>
			</tr>

</table>
<table width="100%">
			 <?php 
              $biaya_setting_ = $this->rewardsahabat_model->get_key_val();
                foreach ($biaya_setting_ as $key => $value){
                  $out[$key] = $value;
                }
          ?>
			<tr>
				<td width="25%"> No INVOICE </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri">#<?php echo $report_registrasi['invoice']?></td>

				<td colspan="3" class="h_kiri"><strong>BANK Transfer A/N SBL</strong></td>
			</tr>
		
			<tr>
				<td width="25%"> Tanggal Transaksi </td>
				<td width="2%">:</td>
				<td width="20%" class="h_kiri"><?php echo $report_registrasi['create_date']?></td>

				<td width="14%"> BANK MANDIRI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><?php echo $out['MANDIRI'];?></td>
			</tr>
		
			<tr>
				<td> ID Affiliate </td>
				<td>:</td>
				<td><?php echo $report_registrasi['id_user_affiliate']?></td>
				

				<td> BANK BNI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><?php echo $out['BNI'];?></td>
			</tr>
			<tr>
				<td> Nama </td>
				<td>:</td>
				<td><?php echo $report_registrasi['affiliate']?></td>

				<td> BANK BCA </td>
				<td width="1%">:</td>
				<td class="h_kiri"><?php echo $out['BCA'];?></td>
			</tr>
			<tr>
				<td> Paket </td>
				<td>:</td>
				<td><?php echo $report_registrasi['paket']?></td>

				
				<td> BANK MANDIRI Syariah </td>
				<td width="1%">:</td>
				<td class="h_kiri"><?php echo $out['MANDIRI_Syariah'];?></td>
			</tr>
			<tr>
				<td> TGL Keberangkatan </td>
				<td>:</td>
				<td><?php echo $report_registrasi['tgl_keberangkatan']?></td>


				<td> BANK BRI </td>
				<td width="1%">:</td>
				<td class="h_kiri"><?php echo $out['BRI'];?></td>
			</tr>
			<tr>
				<td> Jama Keberangkatan </td>
				<td>:</td>
				<td><?php echo $report_registrasi['jam_keberangkatan']?></td>

				<td> BANK MUAMALAT </td>
				<td width="1%">:</td>
				<td class="h_kiri"><?php echo $out['MUAMALAT'];?></td>
			</tr>
			<tr>
				<td> Lama Menunggu </td>
				<td>:</td>
				<td><?php echo $report_registrasi['bulan_menunggu']?> Bulan</td>
				
				<td> Ketentuan dan Syarat </td> 
				<td width="1%">:</td>
				<td class="h_kiri"> <strong> Untuk Waktu Pembayaran Dibatasi 6jam, <br>Apabila Pembayaran lebih dari 6jam data yang anda Registrasikan akan Terhapus Otomatis dan pembayaran lebih 6jam harus Registrasi ulang.</strong> </td>
				
			</tr>
		
			<tr>
				<td> Jumlah Jamaah </td>
				<td>:</td>
				<td><?php echo $report_registrasi['jumlah_jamaah']?> Orang</td>
				
				
			</tr>
			<tr>
				<td> Quard </td>
				<td>:</td>
				<td><?php echo $report_registrasi['quard_2']?> Kamar</td>
				

			</tr>
			<tr>
				<td> Double </td>
				<td>:</td>
				<td><?php echo $report_registrasi['double_2']?> Kamar</td>
				
			</tr>
			<tr>
				<td> Tripel </td>
				<td>:</td>
				<td><?php echo $report_registrasi['triple_2']?> Kamar</td>
				
			</tr>
			<tr>
				<td> Harga Ganti Kamar </td>
				<td>:</td>
				<td>Rp.  <?php echo number_format($report_registrasi['Room_Price_2'])?></td>

					

			</tr>
			<tr>
				<td> Biaya Visa </td>
				<td>:</td>
				<td>Rp. <?php echo number_format($report_registrasi['visa'])?></td>

			</tr>
			<tr>
				<td> Biaya Muhrim </td>
				<td>:</td>
				<td>Rp. <?php echo number_format($report_registrasi['muhrim'])?></td>

			</tr>
			<tr>
				<td> Refund </td>
				<td>:</td>
				<td>Rp.  <?php echo number_format($report_registrasi['refund'])?> </td>

			</tr>
			<tr>
				<td> Handling </td>
				<td>:</td>
				<td>Rp.  <?php echo number_format($report_registrasi['handling'])?> </td>

			</tr>
				<tr>
				<td> Akomodasi </td>
				<td>:</td>
				<td>Rp.  <?php echo number_format($report_registrasi['akomodasi'])?> </td>

			</tr>
			<tr>
				<td> Harga Paket </td>
				<td>:</td>
				<td>Rp.  <?php echo number_format($report_registrasi['harga'])?> </td>

			</tr>
			<tr>
				<td> Total Harga Wajib dibayar </td>
				<td>:</td>
				<td>Rp.  <?php echo number_format($report_registrasi['Total_Bayar'])?> </td>

			</tr>
			<tr>
				
				<td colspan="3"><strong> <?php echo $this->terbilang->eja($report_registrasi['Total_Bayar'])?>  RUPIAH </strong></td>


				
			</tr>


			
</table>

<span class="input-group-btn">
  	 <?php
      echo '<a href="'.site_url('registrasi_jamaah/cetak_registrasi').'/cetak/' . $report_registrasi['id_booking'] . '"  title="Cetak Detail" class="btn btn-sm btn-success" target="_blank"> <i class="glyphicon glyphicon-print"></i> CETAK By Invoice</a>';

     if (($report_registrasi['jumlah_jamaah']) > 1){ 
      echo '<a href="'.site_url('registrasi_jamaah/cetak_registrasijamaah').'/cetak/' . $report_registrasi['id_booking'] . '"  title="Cetak Detail" class="btn btn-sm btn-danger" target="_blank"> <i class="glyphicon glyphicon-print"></i> CETAK By Jamaah</a>';
      }
    ?>
    
</span>

<script type="text/javascript">
    (function (global) { 

    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };          
    }

})(window);
</script>