
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        <form   class="form-horizontal" role="form"  >
        
      <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data AffIliate</b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $detail['id_booking']?>">
                      <div class="col-sm-4">
                        <label>Nama Affiliate</label> 
                        <input type="text" name="affiliate" class="form-control" value="<?php echo $detail['affiliate']?>" readonly="readonly" >
                          
                      </div>

                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_affiliate" class="form-control" value="<?php echo $detail['id_affiliate']?>" readonly="readonly" >
                           
                      </div>
                   
                  
              </div>
            </div>
            </div>
          </div> 
     <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>data Jamaah</b>
                </div>

                 <div class="panel-body">
                    
                          <div class="table-responsive">
                              <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                  <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TANGGAL DFTR/AKT </th>
                                    <th>Alamat </th>
                                   <th>Detail Pembayaran </th>
                                  <!-- <th>User </th> -->
                                </tr>
                            </thead>
                            <tbody>
                               <?php  foreach($get_pic_jamaah as $pi){  ?>
                                  <tr>
                                      
                                      <td width="20%" ><?php echo $pi['invoice']?></strong><br>
                                         <strong><?php echo $pi['id_jamaah']?> - <?php echo $pi['nama']?></strong><br>
                                         Telp : <?php echo $pi['telp']?>'
                                         KTP/K. IDENTITAS : <?php echo $pi['no_identitas']?>
                                         EMBARKASI : <?php echo $pi['departure']?>
                                      </td>
                                      <td width="18%" > <strong><?php echo $pi['paket']?> <?php echo $pi['category']?> </strong><br>
                                         Tgl Berangkat : <?php echo $pi['bulan_keberangkatan']?><br>
                                         Waktu Tunggu : <strong><?php echo $pi['bulan_menunggu']?> Bulan </strong>
                                         Tipe Jamaah : <?php echo $pi['tipe_jamaah']?>
                                         <!-- Logistik : <?php echo $pi['logistik']?> -->
                                      </td>
                                      <td width="18%" >Tgl Daftar : <?php echo $pi['tgl_daftar']?><br>
                                         <!-- Tgl Aktivasi : <?php echo $pi['tgl_aktivasi']?><br> -->
                                         Tgl Bayar : <?php echo $pi['tgl_bayar']?>
                                      </td>
                                         <td width="20%" >Provinsi : <?php echo $pi['provinsi']?><br>
                                         Kabupaten : <?php echo $pi['kabupaten']?><br>
                                         kecamatan : <?php echo $pi['kecamatan']?><br>
                                         Alamat : <br>
                                         <?php echo $pi['alamat']?><br>
                                      </td>
                                       </td>
                                         <td width="20%" >
                                         Harga Paket : <?php echo $pi['harga_paket']?><br>
                                         Cashback : <?php echo $pi['refund']?><br>
                                         Handling : <?php echo $pi['handling']?><br>
                                         Akomodasi : <?php echo $pi['akomodasi']?><br>
                                         Muhrim : <?php echo $pi['muhrim']?><br>
                                         <!-- Total_VISA : <?php echo $pi['Total_VISA']?><br> -->
                                         DP Angsuran : <?php echo $pi['dp_angsuran']?><br>
                                         Jml Angsuran : <?php echo $pi['angsuran']?><br>
                                         Nominal Cicilan : <?php echo $pi['jml_angsuran']?><br>
                                      </td>
                                       </td>
                                       <!--   <td width="10%" >
                                         <?php echo $pi['create_by']?><br>
                                        
                                      </td> -->
                                  <!--           
                                     <td width="18%">'.$r->create_by.'<br>
                                         User ID : '.$r->id_user.'<br>

                                          </td>'; -->
                                  
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>

              </div>
          </div>
      </div>
    </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Historis Aktivasi By Invoice</b>
                </div>

                 <div class="panel-body">
                    
                          <div class="table-responsive">
                              <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                  
                                    <th>invoice</th>
                                    <th>Status Pembayaran </th>
                                    <th>Ket Pembayaran </th>
                                    <th>Bukti Pembayaran </th>
                                   <th>Info </th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                               <?php foreach($pic as $pi){ ?>
                                  <tr>
                                     
                                      
                                      <td width="5%"><?php echo $pi['invoice']?>  </td>
                                      <td width="15%"><?php echo $pi['pay_method']?> </td>
                                      <td width="40%"><?php echo $pi['keterangan']?>  </td>
                                      <td width="25%"> Bukti Pembayaran :
                                      
                                          <!-- <img src="<?php echo base_url();?>manifest/gambar/<?php echo $pi['id_jamaah']?>"> </td> -->
                                         <?php if($pi['pic1']){?>
                                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$pi['pic1']?>" target="_blank">
                                            <i class="fa fa-lightbulb-o"></i> View
                                        </a>
                                         
                                        <?php } ?></br>
                                        Bukti Mutasi Rekening :
                                          <?php if($pi['pic2']){?>
                                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$pi['pic2']?>" target="_blank">
                                            <i class="fa fa-lightbulb-o"></i> View
                                        </a>
                                         
                                        <?php } ?></br>
                                        Bukti Pindah Paket :

                                         <?php if($pi['pic3']){?>
                                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$pi['pic3']?>" target="_blank">
                                            <i class="fa fa-lightbulb-o"></i> View
                                        </a>
                                        <?php } ?>
                                      </td>
                                      <td width="35%">Bank Transfer :<strong> <?php echo $pi['bank']?> </strong><br>
                                          User Aktivasi : <strong><?php echo $pi['user']?> </strong>
                                       </td>
                                      
                                  
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Historis Aktivasi By Jamaah</b>
                </div>

                 <div class="panel-body">
                    
                          <div class="table-responsive">
                              <table id="data-table" class="table table-striped table-bordered table-hover" >
                               <thead>
                                <tr>
                                  
                                    <th>Id Jamaah</th>
                                    <th>Status Pembayaran </th>
                                    <th>Ket Pembayaran </th>
                                    <th>Bukti Pembayaran </th>
                                   <th>Info </th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                               <?php foreach($pic_by_jamaah as $pi){ ?>
                                  <tr>
                                     
                                      
                                      <td width="5%"><?php echo $pi['id_jamaah']?>  </td>
                                      <td width="15%"><?php echo $pi['pay_method']?> </td>
                                      <td width="40%"><?php echo $pi['keterangan']?>  </td>
                                      <td width="25%"> Bukti Pembayaran :
                                      
                                          <!-- <img src="<?php echo base_url();?>manifest/gambar/<?php echo $pi['id_jamaah']?>"> </td> -->
                                         <?php if($pi['pic1']){?>
                                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$pi['pic1']?>" target="_blank">
                                            <i class="fa fa-lightbulb-o"></i> View
                                        </a>
                                         
                                        <?php } ?></br>
                                        Bukti Mutasi Rekening :
                                          <?php if($pi['pic2']){?>
                                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$pi['pic2']?>" target="_blank">
                                            <i class="fa fa-lightbulb-o"></i> View
                                        </a>
                                         
                                        <?php } ?></br>
                                        Bukti Pindah Paket :

                                         <?php if($pi['pic3']){?>
                                          <a href="<?php echo base_url().'assets/images/bukti_pembayaran/'.$pi['pic3']?>" target="_blank">
                                            <i class="fa fa-lightbulb-o"></i> View
                                        </a>
                                        <?php } ?>
                                      </td>
                                      <td width="35%">Bank Transfer :<strong> <?php echo $pi['bank']?> </strong><br>
                                          User Aktivasi : <strong><?php echo $pi['user']?> </strong>
                                       </td>
                                      
                                  
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>
         <span class="input-group-btn">
            <?php
                echo '<a href="'.site_url().'data_jamaah/jamaah_aktif/data_jamaahaktif" class="btn btn-sm btn-danger" title="Kembali"> <i class="glyphicon glyphicon-circle-arrow-left"></i> Kembali </a>';
              ?> 
        </span>
  </form>       
                
</div>


              
  <script>
 
</script>
