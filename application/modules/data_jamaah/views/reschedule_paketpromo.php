
<div id="page-wrapper">
  <div class="row">
   <div class="col-lg-12">
        
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>RESCHEDULE PAKET PROMO</b>
        </div>
        <div class="panel-body">
        <form  role="form" method="POST"  class="form-horizontal" action="<?php echo base_url();?>data_jamaah/jamaah_aktif/update_schedule_paketpromo">
        <!-- <form  role="form" method="POST"  class="form-horizontal" > -->
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>RESCHEDULE JAMAAH PAKET PROMO</b>
                </div>
                 <?php if($this->session->flashdata('Warning')) { ?>
                                  <div class="alert alert-danger">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Warning! </strong><?php echo $this->session->flashdata('Warning'); ?>  
                                  </div>
                              <?php } ?>
                   <?php if($this->session->flashdata('success')) { ?>
                                  <div class="alert alert-success">  
                                          <a class="close" data-dismiss="alert">x</a>  
                                          <strong>Info! </strong><?php echo $this->session->flashdata('success'); ?>  
                                  </div>
                  <?php } ?>
                <!-- /.panel-heading -->
           <div class="panel-body">

               <div class="col-sm-6 col-md-12">
                <div class="block-flat">
                  <div class="header">              
                    <h4><strong>JADWAL KEBERANGKATAN</strong></h4>
                  </div>
                   <div class="content">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">ID JAMAAH </label>
                               <div class="col-sm-10">
                                    <input name="id_jamaah" type="text" class="form-control" id="id_jamaah" value="<?php echo $detail['id_jamaah']?>"  readonly='true' required/>
                                </div>
                    </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Nama  </label>
                               <div class="col-sm-10">
                                    <input name="nama" type="text" class="form-control" id="nama" value="<?php echo $detail['nama']?>"  readonly='true' required/>
                                </div>
                    </div>
	               <div class="form-group">
	                          <label for="inputEmail3" class="col-sm-2 control-label">TGL KEBERANGKATAN  </label>
	                             <div class="col-sm-10">
	                                  <input name="bulan_keberangkatan" type="text" class="form-control" id="bulan_keberangkatan" value="<?php echo $detail['bulan_keberangkatan']?>"  readonly='true' required/>
	                              </div>
	                  </div>
	                  <div class="form-group">
	                          <label for="inputEmail3" class="col-sm-2 control-label">Paket  </label>
	                             <div class="col-sm-10">
	                                  <input name="category" type="text" class="form-control" id="category" value="<?php echo $detail['paket']?> <?php echo $detail['category']?>"  readonly='true' required/>
	                              </div>
	                  </div>
                    <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Menunggu  </label>
                               <div class="col-sm-10">
                                    <input name="menunggu" type="text" class="form-control" id="menunggu" value="<?php echo $detail['bulan_menunggu']?> Bulan "  readonly='true' required/>
                                    <input name="waktutunggu_keberangkatan" type="hidden" class="form-control" id="waktutunggu_keberangkatan" value="<?php echo $detail['bulan_menunggu']?>"  readonly='true' required/>
                                </div>
                    </div>
                      <div class="form-group">
	                          <label for="inputEmail3" class="col-sm-2 control-label">Embarkasi </label>
	                             <div class="col-sm-10">
	                                  <input name="embarkasi" type="text" class="form-control" id="embarkasi" value="<?php echo $detail['departure']?>"  readonly='true' required/>
	                              </div>
	                  </div>
                     <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Tipe Jamaah  </label>
                               <div class="col-sm-10">
                                    <input name="tipe_jamaah" type="text" class="form-control" id="tipe_jamaah" value="<?php echo $detail['tipe_jamaah']?>"  readonly='true' required/>
                                    <input name="kd_tipe" type="hidden" class="form-control" id="kd_tipe" value="<?php echo $detail['kd_tipe']?>"  readonly='true' required/>
                                  
                                </div>
                    </div>
                      <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Harga  </label>
                               <div class="col-sm-10">
                                    <input name="harga_paket" type="text" class="form-control" id="harga_paket" value="<?php echo $detail['harga_paket']?>"  readonly='true' required/>
                                    
                                </div>
                    </div>
                   </div>
                    <input type="hidden" name="id_booking" value="<?php echo $detail['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $detail['id_schedule']?>">
                    <input type="hidden" name="id_jamaah" value="<?php echo $detail['id_jamaah']?>">
                    <input type="hidden" name="id_registrasi" value="<?php echo $detail['id_registrasi']?>">
                    <input type="hidden" name="seats_lama" value="<?php echo $detail['seats']?>">
                 </div> 
                 </div>        

                <div class="col-sm-6 col-md-12">
                <div class="block-flat">
                  <div class="header">              
                    <h4><strong>UBAH JADWAL KEBERANGKATAN</strong></h4>
                  </div>
                  <div class="content">
                 

                  <div class="form-group">
                      <label class="col-lg-2 control-label">Embarkasi</label>
                      <div class="col-lg-5">
                      <select required class="form-control" name="departure" id="departure" >
                          <option value="">Pilih Embarkasi</option>
                          <?php foreach($select_embarkasi as $se){?>
                              <option value="<?php echo $se->id_embarkasi;?>"><?php echo $se->departure;?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </div>
                 <!--   <label class="col-lg-2 control-label">Payment Date</label>
                      <div class="col-lg-5">
                             <input type="text" class="form-control " name="payment_date" id="datepicker"  placeholder="yyyy-mm-dd " required/>
                      </div>
                    </div> -->
                <div class="form-group">
                          <label class="col-lg-2 control-label">Tahun Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' id='datepicker_tahun_keberangkatan1'> -->
                                <input type='text' name="datepicker_tahun_keberangkatan" id="datepicker_tahun_keberangkatan1" class="form-control" placeholder="YYYY/2016" required/>
                             <!--    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                             
                          </div>
                    </div>

                     <div class="form-group">
                          <label class="col-lg-2 control-label">Bulan Pemberangkatan : </label>
                          <div class="col-lg-5">
                           <!-- <div class='input-group date' name="datepicker_keberangkatan1" id='datepicker_keberangkatan1'> -->
                                <input type='text' name="datepicker_keberangkatan" id="datepicker_keberangkatan1" class="form-control" placeholder="MM/January" required/>
                               <!--  <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            <!-- </div> -->
                          </div>

                    </div>
                    
                
                    <div class="form-group">
                      <label class="col-lg-2 control-label"> </label>
                        <div class="col-lg-10">
                        <button type="button" id="check" class="btn btn-success" value="submit"><span class="icon-search"></span> Check Waktu Keberangkatan</button>
                         
                    </div>
                  </div>
                
                  <div class="table-responsive">
                   <table id="data-table" style="width:100%" align="center" class="table table-striped table-bordered table-hover" >
                    <caption>Pilih Jadwal</caption>
                    <thead>
                        <tr>
                            <!-- <th width="2%">Check </th> -->
                            <th width="8%"><strong>Paket</strong></th>
                            <th width="10%"><strong>Embarkasi</strong></th>
                            <th width="10%"><strong>Tgl/Bln </strong></th>

                            <th width="5%"><strong>Jml Kursi</strong></th>
                            <th width="5%"><strong>Tipe Kamar</strong></th>
                            <th width="5%"><strong>Jml HAri</strong></th>
                            
                            <th width="5%"><strong>PILIH</strong></th>
                        </tr>
                        
                    </thead>

               
                <tbody id='response-table'>
                        <tr><td colspan="12"><h2 style="color: #f5b149">Search your specific transaction here</h2></td></tr>

                    </tbody>
                </table>
                </div>

                  </div>
              
 			 </div>
 			  </div>
 			   </div>
                           
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
           
            <a href="<?php echo base_url();?>data_jamaah/jamaah_aktif/data_jamaahaktif"class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>

                   </div>
                </div>

              </div>
                    </form>
            </div><!--end col-->




  
           
           </div>

     </div>
     </div>
     </div>




<script type="text/javascript">
      $(function () {
                $('#datepicker').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker2').datetimepicker({
          format: 'YYYY-MM-DD',
        });
            });
       $(function () {
                $('#datepicker_keberangkatan1').datetimepicker({
          format: 'MMMM',
        });
            });

       $(function () {
                $('#datepicker_tahun_keberangkatan1').datetimepicker({
          format: 'YYYY',
        });
            });
    </script>

    

<script>
       

   $("#check").on('click', function (e){
    e.preventDefault();
    var departure = $("#departure").val();
    var datepicker_tahun_keberangkatan = $("#datepicker_tahun_keberangkatan1").val();
    var datepicker_keberangkatan = $("#datepicker_keberangkatan1").val();
 
    console.log(departure);
    console.log(datepicker_tahun_keberangkatan);
    console.log(datepicker_keberangkatan);
    // if(paket=='4' ){ 
        $.ajax({
            type:'POST',
            url:'<?php echo base_url();?>data_jamaah/jamaah_aktif/searchItem_paket_promo',
             // data:'from='+from+'&to='+to
            data:'s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan+'&l='+departure
           // data:'q='+paket+'&s='+datepicker_tahun_keberangkatan+'&t='+datepicker_keberangkatan
        }).done(function (data){
            $("#response-table").html(data);
        });
   
});


</script>

