
<style type="text/css">
body {
  font-family: verdana,arial,sans-serif;
  margin: 0px;
  padding: 0px;
}

.wrap { 
  width:50%; 
  background:#F0F0F0; 
  margin:auto;
  padding: 25px;
  overflow: hidden;
}

h1 {
  text-align: center;
}

input.pemberangkatan {
  font-size:28px; 
  width:380px;
}

input, textarea {
    border: 1px solid #CCC;
}
</style>
<div id="page-wrapper">
     
        <form   class="form-horizontal" role="form"  >
        
        <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Jamaah Alumni</b>
                </div>
                  <div class="panel-body">                       

                    <input type="hidden" name="id_booking" value="<?php echo $booking['id_booking']?>">
                    <input type="hidden" name="id_schedule" value="<?php echo $booking['id_schedule']?>">
                   
                      <div class="col-sm-4">
                        <label >Invoice</label>
                        <input name="invoice" class="form-control" value="#<?php echo $booking['invoice']?>" readonly="readonly">
  
                      </div>

                       <div class="col-sm-4">
                        <label>Paket</label> 
                        <input type="text" name="paket" class="form-control" value="<?php echo $booking['paket']?> dengan Waktu tunggu <?php echo $booking['bulan_menunggu']?> Bulan " readonly="readonly" >
                            <input type="hidden" name="id_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?> " readonly="readonly" >
                      </div>

                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="affiliate" class="form-control" value="<?php echo $booking['affiliate']?>" readonly="readonly" >
                            <input type="hidden" name="id_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?>" readonly="readonly" >
                      </div>

                      <div class="col-sm-4">
                        <label>ID Affiliate</label> 
                        <input type="text" name="id_user_affiliate" class="form-control" value="<?php echo $booking['id_user_affiliate']?>" readonly="readonly" >
                           
                      </div>

                       <div class="col-sm-4">
                        <label>Emabarkasi</label> 
                        
                        <input name="seats" class="form-control" value="<?php echo $booking['departure']?>" readonly="readonly">
                      </div>

                       <div class="col-sm-4">
                        <label>Jumlah Jamaah</label> 
                        
                        <input name="seats" class="form-control" value="<?php echo $booking['jumlah_jamaah']?>" readonly="readonly">
                      </div>
                  
                    <?php if ($booking['status_keberangkatan'] == 6){ ?>

                      <div class="col-sm-4">
                        <label>Tanggal </label>

                        <input name="tgl_Keberangkatan" class="form-control" value="<?php echo $booking['tgl_keberangkatan']?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4">
                      <label>Jam </label>
                        <input name="jam_Keberangkatan" class="form-control" value="<?php echo $booking['jam_keberangkatan'];?>" readonly="readonly">
                      </div>
                  <?php }else{ ?>
                    <div class="col-sm-4" >  <!-- style="visibility:hidden"> -->
                        <label >Bulan </label>

                        <input  type="hidden" name="tgl_Keberangkatan" class="form-control" value="<?php echo $booking['tgl_keberangkatan'];?>" readonly="readonly">
                         <input  name="BulanKeberangkatan" class="form-control" value="<?php echo $booking['BulanKeberangkatan'];?>" readonly="readonly">
                      </div>

                      <div class="col-sm-4" ><!-- style="visibility:hidden"> -->
                      <label>Tahun </label>
                        <input type="hidden" name="jam_Keberangkatan" class="form-control" value="<?php echo $booking['jam_keberangkatan'];?>" readonly="readonly">
                        <input  name="TahunKeberangkatan" class="form-control" value="<?php echo  $booking['TahunKeberangkatan'];?>" readonly="readonly">

                      </div>
                <?php } ?> 
                     <!--       <div class="col-sm-4">
                        <label>Harga</label> 
                    
                          <div class="input-group">
                              <div class="input-group-addon">Rp</div>
                              <input type="text" class="form-control"  name="harga" id="harga" value="<?php echo number_format($booking['harga']);?>"  readonly="readonly">
                              <div class="input-group-addon">.00</div>
                            </div>
                        </div> -->
                        <!-- <input type="text" class="form-control"  name="harga" id="harga" value="<?php echo number_format($harga + $tiga_digit);?>"  readonly="readonly"> -->

               
              </div>
            </div>
            </div>
          </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>List Jamaah</b>
                </div>

                 <div class="panel-body">
                    
                          <div class="table-responsive">
                              <table  class="table table-striped table-bordered table-hover" id="dataTables-example">
                               <thead>
                                <tr>
                                    <th>No</th>
                                    <th>id jamaah</th>
                                    <th>Nama </th>
                                    <th>No Identitas </th>
                                   <th>Telp </th>
                                   <th>Status </th>
                                   <th>user </th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $no=0; foreach($pic as $pi){ $no++ ?>
                                  <tr>
                                      <td><?php echo $no; ?></td>
                                      
                                      <td><?php echo $pi['id_jamaah']?>  </td>
                                      <td><?php echo $pi['nama']?> </td>
                                      <td><?php echo $pi['no_identitas']?>  </td>
                                      <td><?php echo $pi['telp']?>  </td>
                                      <td><?php echo $pi['status']?>  </td>
                                      <td><?php echo $pi['create_by']?>  </td>
                                  </tr>
                                  <?php } ?> 
                            </tbody>
                     </table>
                   </div>
                  
                    <!--  <div class="pull-right">
                        <ul class="pagination"></ul>    
                     </div>  -->
              </div>
          </div>
      </div>
    </div>

  </form>       
                
</div>


              
  <script>
 $(document).ready(function () {
    $('#dataTables-example').dataTable({
      "pagingType": "full_numbers",
      "iDisplayLength": 15});

  });
</script>
