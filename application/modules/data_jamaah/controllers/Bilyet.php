<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bilyet extends CI_Controller {
	var $folder = "data_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DATA_JAMAAH_AKTIF,'cetak'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','data_jamaah');	
		$this->load->model('jamaahaktif_model');
		$this->load->helper('fungsi');
		// $this->load->model('registrasijamaah_model');
		// $this->load->model('settingdata_model');
		//$this->load->model('setting_m');
		$this->load->library('terbilang');
	}

	 function gambar($kode)
	{
				$height = isset($_GET['height']) ? mysql_real_escape_string($_GET['height']) : '50';	
				$width = isset($_GET['width']) ? mysql_real_escape_string($_GET['width']) : '1'; //1,2,3,dst
				$this->load->library('zend');
		        $this->zend->load('Zend/Barcode');
		 		$barcodeOPT = array(
				    'text' => $kode, 
				    'barHeight'=> $height, 
				    'factor'=>$width,
				);
						
					$renderOPT = array();
			$render = Zend_Barcode::factory(
		'code39', 'image', $barcodeOPT, $renderOPT
		)->render();
	}
	

	function cetak($id_jamaah) {
		$registrasi = $this->jamaahaktif_model->data_jamaahaktif($id_jamaah);

		// $opsi_val_arr = $this->registrasijamaah_model->get_key_val();
		// foreach ($opsi_val_arr as $key => $value){
		// 	$out[$key] = $value;
		// }

		$this->load->library('Struk');
		$pdf = new Struk('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->set_nsi_header(false);
		// $resolution = array(280, 100);
		$pdf->AddPage('P');
		$html = '
		<style>
			.h_tengah {text-align: center;}
			.h_kiri {text-align: left;}
			.h_kanan {text-align: right;}
			.txt_judul {font-size: 12pt; font-weight: bold; padding-bottom: 12px;}
			.header_kolom {background-color: #cccccc; text-align: center; font-weight: bold;}
			.txt_content {font-size: 12pt; text-align: center;}
			.txt_content2 {font-size: 12pt; text-align: center;}
		</style>';
		
		$no =1;
		foreach ($registrasi as $row) {
			
			$html .='<table width="350px"  border = "0">
			
		<p class="txt_content"></p>
		<p class="txt_content"></p>
		<p class="txt_content"></p>
		<p class="txt_content"></p>
		
		
			<tr >
				
				<td >  </td>
				<td > Terdaftar Sejak :</td>
				
				<td >'.$row->payment_date.'</td>
			</tr>
			<tr>
			<td >  </td>
				<td> Nama :</td>
				
				<td>'.$row->nama.'</td>

			
			</tr>
			<tr>
			<td >  </td>
				<td> ID Jamaah :</td>



				<td>'.$row->id_jamaah.'</td>

			
			</tr>
			<tr>
			<td >  </td>
				<td> Alamat :</td>
				
				<td>'.$row->alamat.'</td>

				

			</tr>
			<tr>
			<td >  </td>
				<td> No Telp :</td>
				
				<td>'.$row->telp.'</td>


				
			</tr>
			<tr>
			<td >  </td>
				<td> Paket :</td>
			
				<td>'.$row->paket.' '.$row->category.'</td>

			</tr>
			<tr>
			<td >  </td>
				<td> Pemberangkatan :</td>
				
				<td>'.$row->date_schedule.' .Bulan</td>
				
			</tr>
			<tr>
			<td >  </td>
				<td> Harga :</td>
				
				<td>Rp. '.number_format($row->harga_paket).'</td>
				
			</tr>	

	
			';
		}
		$html .= '</table> 
		<p class="txt_content"></p>
		<p class="txt_content"><strong>PT. Solusi Balad Lumampah</strong><br> </p>
		<p class="txt_content"></p>
		</br>
		</br>
		<p class="txt_content">H Aom Juang Wibowo Sastra Ningrat<br> 
		</p>

		';

		$pdf->nsi_html($html);
		$pdf->Output(date('Ymd_His') . '.pdf', 'I');
	} 

}