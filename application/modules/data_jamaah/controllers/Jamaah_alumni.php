<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jamaah_alumni extends CI_Controller{
	var $folder = "data_jamaah";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','jamaah_alumni');	
		$this->load->model('jamaahalumni_model');
		$this->load->model('jamaahalumni_model','r');
		//$this->load->helper('fungsi');
	}
	
	public function index(){
	
		
	   $this->template->load('template/template', $this->folder.'/jamaah_alumni');
		
	}

	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	               	 $row[] ='<td width="20%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 ID SAHABAT :  '.$r->id_sahabat.'<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	 $row[] ='<td width="18%" >'.$r->paket.' '.$r->category.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	 $row[] ='<td width="18%" >Tgl Daftar : '.$r->tgl_daftar.'<br>
	             			 Tgl Aktivasi : '.$r->tgl_aktivasi.'<br>
	             			 
	               			 </td>';
	              
	              	 $row[] ='
	                <a title="Detail" class=" btn-sm btn-success" href="'.base_url().'data_jamaah/jamaah_aktif/detail_data_jamaah_aktif/'.$r->id_booking.'/'.$r->id_jamaah.'">
	                            Detail
	                        </a> ';

			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


    



	
	 public function detail(){
	


	    if(!$this->general->privilege_check(DATA_JAMAAH,'view'))
		    $this->general->no_access();
	    
	    $id_booking = $this->uri->segment(4);
	    $booking = $this->jamaahalumni_model->get_pic_booking($id_booking);
	    $pic    = array();
	    // $pic_booking= array();
	    if(!$booking){
	        show_404();
	    }
	    else{
	        
	        // $pic_booking = $this->registrasijamaah_model->get_pic_booking($id_booking);
	        $pic = $this->jamaahalumni_model->get_pic($id_booking);
	    }    

	      
	    $data = array(

	       		 // 'pic_booking'=>$pic_booking,
	       		 'booking'=>$booking,'pic'=>$pic

	       		 );
	
	    $this->template->load('template/template', $this->folder.'/detail_jamaahalumni',($data));

	}
	

}
