<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jamaahaktif_model extends CI_Model {

    var $table = 'jamaah_aktif';
    var $column_order = array('id_registrasi'); //set column field database for datatable orderable
    var $column_search = array('id_registrasi', 'id_sahabat', 'id_booking', 'id_affiliate', 'invoice', 'id_jamaah', 'nama', 'telp', 'no_identitas', 'tgl_daftar', 'paket', 'category', 'date_schedule', 'bulan_menunggu', 'bulan_keberangkatan', 'departure', 'id_user', 'tipe_jamaah', 'status', 'kd_tipe', 'tgl_aktivasi', 'update_date'); //set column field database for datatable searchable 
    var $order = array('update_date' => 'asc'); // default order 
    private $_table = "registrasi_jamaah";
    private $_booking = "booking";
    private $_primary = "id_jamaah";
    private $_bilyet = "bilyet";
    private $_aktivasi_reschedule = "aktivasi_reschedule";

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function update($data) {
        $arr = array(
            'id_booking' => $data['id_booking'],
            'nama' => $data['nama'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'status_diri' => $data['select_statuskawin'],
            'kelamin' => $data['select_kelamin'],
            'rentang_umur' => $data['select_rentangumur'],
            'no_identitas' => $data['no_identitas'],
            'alamat' => $data['alamat'],
            'telp' => $data['telp'],
            'email' => $data['email'],
            // 'ket_keberangkatan'=>$data['select_status_hubungan'],
            'ahli_waris' => $data['waris'],
            'hub_waris' => $data['select_hub_ahliwaris'],
            'merchandise' => $data['select_merchandise'],
            'refund' => $data['refund'],
            'handling' => $data['handling'],
            'akomodasi' => $data['akomodasi'],
            'category' => $data['category'],
            'muhrim' => $data['muhrim'],
            'dp_angsuran' => $data['dp_angsuran'],
            'angsuran' => $data['angsuran'],
            'jml_angsuran' => $data['jml_angsuran'],
            'harga_paket' => $data['harga_paket'],
            'update_by' => $this->session->userdata('id_user'),
            'update_date' => date('Y-m-d H:i:s'),
            'ket_edit' => $data['ket_edit'],
        );

        if ($data['provinsi_id'] != '' && $data['kecamatan_id'] != '' && $data['kabupaten_id'] != '') {

            // $arr['password'] = ($data['password']);
            $arr['provinsi_id'] = ($data['provinsi_id']);
            $arr['kecamatan_id'] = ($data['kecamatan_id']);
            $arr['kabupaten_id'] = ($data['kabupaten_id']);
        }
        $this->db->update($this->_table, $arr, array('id_jamaah' => $data['id_jamaah']));
        // $this->db->update('registrasi_jamaah',$arr,array('id_jamaah'=>$data['id_jamaah']));    


        $arr = array(
            // 'tgl_daftar' => $data['tgl_daftar'],
            // 'embarkasi' => $data['embarkasi'],
            // 'id_product' => $data['product'],
            'harga' => $data['harga_paket'],
                // 'status_claim_fee'=>$data['status_claim_fee'],
        );
        $this->db->update($this->_booking, $arr, array('id_booking' => $data['id_booking']));

        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();

            return true;
        }
    }

    public function get_pic($id_booking) {

        $sql = "SELECT * from detail_jamaahaktif WHERE  id_booking  = ?
              ";
        return $this->db->query($sql, array($id_booking))->result_array();
    }

    public function get_pic_booking($id_booking) {


        $sql = "SELECT * from view_booking_aktif where id_booking = {$id_booking}";

        return $this->db->query($sql)->row_array();
    }

    public function get_pic_booking_schedule($id_booking) {


        $sql = "SELECT * from registrasi_jamaah where id_booking = {$id_booking}";

        return $this->db->query($sql)->row_array();
    }

    public function get_pic_detail_jamaah($id_jamaah) {

        $sql = "SELECT * from data_jamaah_aktif where id_jamaah = '$id_jamaah'";

        return $this->db->query($sql)->row_array();
    }

    public function get_pic_aktif($where = "") {
        $data = $this->db->query('select * from detail_jamaahaktif ' . $where);
        return $data;
    }

    public function get_pic_aktivasi_invoice($id_booking) {

        $sql = "SELECT * from historis_pembayaran_invoice WHERE  id_booking  = ?
              ";
        return $this->db->query($sql, array($id_booking))->result_array();
    }

    public function get_pic_aktivasi_by_jamaah($id_jamaah) {

        $sql = "SELECT * from historis_pembayaran_id_jamaah WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql, array($id_jamaah))->result_array();
    }

    public function get_pic_jamaah($id_jamaah) {

        $sql = "SELECT * from data_jamaah_aktif WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql, array($id_jamaah))->result_array();
    }

    function get_biaya_refund() {
        $this->db->select('*');
        $this->db->from('view_refund');
        // $this->db->where('tampil', 'Y');
        // $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    public function get_data($offset, $limit, $q = '') {
        $offset = $this->uri->segment(3, 0);
        $id = $this->session->userdata('id_user');
        $sql = " SELECT * from view_booking_aktif 
                   where (1=1) ";

        if ($q) {

            $sql .= " AND affiliate LIKE '%{$q}%'
                    OR departure LIKE '%{$q}%'
                     OR invoice LIKE '%{$q}%'
                     OR paket LIKE '%{$q}%'   
                     OR tgl_daftar LIKE '%{$q}%' 
                     OR tgl_keberangkatan LIKE '%{$q}%'
                     OR jumlah_jamaah LIKE '%{$q}%'   
                     OR user_create LIKE '%{$q}%' 
                ";
        }
        $sql .= " ORDER BY payment_date ASC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .= " LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    public function get_pic_bilyet($id_jamaah) {

        $sql = "SELECT * from data_aktivasi_jamaah WHERE  id_jamaah  = ?
              ";
        return $this->db->query($sql, array($id_jamaah))->result_array();
    }

    public function save_cetak_bilyet($data) {
        $total_cetak_bilyet = $this->input->post('total_cetak_bilyet');
        $id_jamaah = $this->input->post('id_jamaah');
        $tc_bilyet = $this->input->post('tc_bilyet');


        if ($total_cetak_bilyet <= 0) {
            $arr = array(
                'id_jamaah' => $data['id_jamaah'],
                'keterangan' => $data['keterangan'],
                'id_user' => $data['select_user'],
                'create_date' => date('Y-m-d H:i:s'),
                'create_by' => $this->session->userdata('id_user'),
            );

            $this->db->insert('bilyet', $arr);
            $this->db->trans_begin();

            $this->db->insert($this->_bilyet, $arr);
            // $id_jamaah =  $this->db->insert_id(); 
            $arr = array(
                'tc_bilyet' => ($tc_bilyet + 1),
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();


                return true;
            }
        } else {
            $arr = array(
                'id_jamaah' => $data['id_jamaah'],
                'keterangan' => $data['keterangan'],
                'id_user' => $data['select_user'],
                'update_date' => date('Y-m-d H:i:s'),
                'update_by' => $this->session->userdata('id_user'),
            );

            $this->db->update($this->_bilyet, $arr, array('id_jamaah' => $data['id_jamaah']));
            // $id_jamaah =  $this->db->insert_id(); 
            $arr = array(
                'tc_bilyet' => ($tc_bilyet + 1),
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();


                return true;
            }
        }
    }

    public function get_jamaah_aktif($offset, $limit, $q = '') {

        $id = $this->session->userdata('id_user');
        $sql = "SELECT * FROM jamaah_aktif
                   where (1=1) ";

        if ($q) {

            $sql .= " AND invoice LIKE '%{$q}%'
                    OR id_jamaah LIKE '%{$q}%'
                     OR nama LIKE '%{$q}%'
                     OR id_affiliate LIKE '%{$q}%'
                     OR telp LIKE '%{$q}%'   
                     OR tgl_daftar LIKE '%{$q}%' 
                     OR paket LIKE '%{$q}%'
                     OR bulan_menunggu LIKE '%{$q}%'
                     OR affiliate LIKE '%{$q}%'  
                     OR provinsi LIKE '%{$q}%' 
                     OR kota LIKE '%{$q}%' 
                     OR create_by LIKE '%{$q}%' 
                ";
        }
        // $sql .=" ORDER BY tgl_daftar DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();

        $sql .= " LIMIT {$offset},{$limit} ";

        $ret['data'] = $this->db->query($sql)->result();

        return $ret;
    }

    function get_nama_jamaah() {
        $this->db->select('*');
        $this->db->from('registrasi_jamaah');
        $this->db->where('status', '0');
        $this->db->order_by('id_registrasi', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return array();
        }
    }

    function lap_data_jamaah($id_booking) {
        $this->db->select('*');
        $this->db->from('view_booking_aktif');
        $this->db->where('id_booking', $id_booking);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_booking($id_booking) {
        $this->db->select('*');
        $this->db->from('view_booking_aktif');
        $this->db->where('id_booking', $id_booking);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_jamaahaktif($id_booking) {
        $this->db->select('*');
        $this->db->from('detail_jamaahaktif');
        $this->db->where('id_booking', $id_booking);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function data_jamaahaktif($id_jamaah) {
        $this->db->select('*');
        $this->db->from('detail_jamaahaktif');
        $this->db->where('id_jamaah', $id_jamaah);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $out = $query->result();
            return $out;
        } else {
            return FALSE;
        }
    }

    function get_data_bandara($pemberangkatan = '') {
        $this->db->where("pemberangkatan", $pemberangkatan);
        return $this->db->get("view_refund");
    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();

        return $query->result();
    }

    function get_all_kabupaten() {
        $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");

        return $query->result();
    }

    function get_provinsi() {

        $query = $this->db->get('wilayah_provinsi');
        return $query->result();
    }

    function get_kabupaten() {

        $query = $this->db->get('wilayah_kabupaten');
        return $query->result();
    }

    function get_kecamatan() {

        $query = $this->db->get('wilayah_kecamatan');
        return $query->result();
    }

    function searchItem($paket, $departure, $datepicker_tahun_keberangkatan, $datepicker_keberangkatan) {
        $sql = "SELECT * from view_schedule where room_category = '$paket' and BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'";
        $query = $this->db->query($sql);

        if (empty($query->result())) {

            echo'<tr><td colspan="12"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
            exit;
        } else {


            foreach ($query->result() as $row) {


                echo'<tr>
                   
                   <td><strong>' . $row->paket . '</strong></td>
                   <td><strong>' . $row->departure . '</strong></td>
                   <td><strong>' . $row->date_schedule . ', wkt tunggu ' . $row->waktutunggu . ' Bulan</strong></td>
                   <td><strong>' . $row->time_schedule . '</strong></td>
                   <td><strong>' . $row->seats . '</strong></td>
                   <td><strong>' . $row->type . '</strong></td>
                   <td><strong>' . $row->category . ' - ' . $row->hari . ' hari</strong></td>
                   <td><strong>' . $row->keterangan . '</strong></td>
                   <td><strong>' . number_format($row->harga) . '</strong></td>
                   <td>' . $row->status_jadwal . ', <strong> JAMAAH (' . $row->tipe_jamaah . ')</strong></td>
                   <td><strong>' . $row->maskapai . '</strong></td>
                 
                   <td> <input name="radio" class="radio1" type="radio" id="radio1" value="' . $row->id_schedule . ' "</td>
                
                </a></div>
                </td>
                </tr>';
            }
        }
    }

    public function update_schedule($data) {
        $schedule = $this->input->post('radio');
        $id_booking = $this->input->post('id_booking');
        $id_jamaah = $this->input->post('id_jamaah');
        $id_registrasi = $this->input->post('id_registrasi');
        $waktutunggu_keberangkatan = $this->input->post('waktutunggu_keberangkatan');
        $harga_paket = $this->input->post('harga_paket');
        $tipe_jamaah_ori = $this->input->post('kd_tipe');
        $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $key => $value) {
            $id_schedule = $value['id_schedule'];
            $id_product = $value['id_product'];
            $id_room_type = $value['id_room_type'];
            $room_category = $value['room_category'];
            $embarkasi = $value['embarkasi'];
            $harga = $value['harga'];
            $id_productprice = $value['id_productprice'];
            $seats = $value['seats'];
            $pindah_bulan_menunggu = $value['waktutunggu'];
            $id_tipe_jamaah = $value['id_tipe_jamaah'];
        }
        if ($id_product == 4) {
            $harga_bayar = ($harga - $harga_paket);
            // $waktu_tunggu =($pindah_bulan_menunggu - );
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => $harga_bayar,
                'status' => 0,
                'status_reschedule' => 2,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );

            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');

                return true;
            }
        } elseif ($id_product == 4 && $pindah_bulan_menunggu > $waktutunggu_keberangkatan) {
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'tipe_jamaah' => $id_tipe_jamaah,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 2,
                'status_reschedule' => 3,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();

            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('aktivasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));



            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('list_gaji', $arr, array('id_registrasi' => $id_registrasi));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');

                return true;
            }
        } elseif ($pindah_bulan_menunggu >= $waktutunggu_keberangkatan) {

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'tipe_jamaah' => $id_tipe_jamaah,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 2,
                'status_reschedule' => 3,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();

            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('aktivasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));



            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('list_gaji', $arr, array('id_registrasi' => $id_registrasi));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');

                return true;
            }
        } elseif ($waktutunggu_keberangkatan > $pindah_bulan_menunggu) {

            $biaya_setting_ = $this->get_key_val();
            foreach ($biaya_setting_ as $key => $value) {
                $out[$key] = $value;
            }
            $waktu_tunggu = ($waktutunggu_keberangkatan - $pindah_bulan_menunggu);
            $harga_pindah = ($out['PINDAH_JADWAL'] * $waktu_tunggu);
            // print_r($waktu_tunggu);
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => $harga_pindah,
                'status' => 0,
                'status_reschedule' => 1,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();
            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        }
    }

    function get_key_val() {

        $out = array();
        $this->db->select('id_setting,opsi_setting,key_setting');
        $this->db->from('setting');
        // $this->db->where('status_setting', $room_category);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $value) {
                $out[$value->opsi_setting] = $value->key_setting;
            }
            return $out;
        } else {
            return array();
        }
    }

    public function get_report_reschedule_reguler($id_reschedule) {


        $sql = "SELECT * from aktivasi_reschedule_reguler where id_reschedule = {$id_reschedule} ";

        return $this->db->query($sql)->row_array();
    }

    public function update_schedule_reward_sahabat($data) {
        $schedule = $this->input->post('radio');
        $id_booking = $this->input->post('id_booking');
        $id_jamaah = $this->input->post('id_jamaah');
        $id_registrasi = $this->input->post('id_registrasi');
        $waktutunggu_keberangkatan = $this->input->post('waktutunggu_keberangkatan');
        $harga_paket = $this->input->post('harga_paket');
        $tipe_jamaah_ori = $this->input->post('kd_tipe');
        $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $key => $value) {
            $id_schedule = $value['id_schedule'];
            $id_product = $value['id_product'];
            $id_room_type = $value['id_room_type'];
            $room_category = $value['room_category'];
            $embarkasi = $value['embarkasi'];
            $harga = $value['harga'];
            $id_productprice = $value['id_productprice'];
            $seats = $value['seats'];
            $pindah_bulan_menunggu = $value['waktutunggu'];
            $id_tipe_jamaah = $value['id_tipe_jamaah'];
        }
        if ($id_tipe_jamaah == 2) {

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 1,
                'status_reschedule' => 5,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();


            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('aktivasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));
            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        } elseif ($pindah_bulan_menunggu >= $waktutunggu_keberangkatan) {

            //     $biaya_setting_ = $this->get_key_val();
            //           foreach ($biaya_setting_ as $key => $value){
            //             $out[$key] = $value;
            //      }
            //      $arr = array(
            //         'id_booking'=> $data['id_booking'],
            //         'id_jamaah'=>$id_jamaah,
            //         'id_product'=>$id_product,
            //         'category'=> $room_category,
            //         'room_type'=> $id_room_type,
            //         'tipe_jamaah'=>$tipe_jamaah_ori,
            //         'ori_schedule'=> $this->input->post('id_schedule'),
            //         'reschedule'=>$id_schedule,
            //         'biaya'=> $out['JAMAAH_REWARD_PINDAH_KE_REGULER'],
            //         'status'=> 0,
            //         'status_reschedule'=>4,
            //         'create_by'=>$this->session->userdata('id_user'),
            //         'create_date'=>date('Y-m-d H:i:s'),
            //     ); 
            //       $this->db->trans_begin(); 
            // $this->db->insert($this->_aktivasi_reschedule,$arr);
            // $id_reschedule =  $this->db->insert_id(); 

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 1,
                'status_reschedule' => 5,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();


            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        } elseif ($waktutunggu_keberangkatan > $pindah_bulan_menunggu && $id_tipe_jamaah == 1) {

            $biaya_setting_ = $this->get_key_val();
            foreach ($biaya_setting_ as $key => $value) {
                $out[$key] = $value;
            }
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'tipe_jamaah' => $tipe_jamaah_ori,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => $out['JAMAAH_REWARD_PINDAH_KE_REGULER'],
                'status' => 0,
                'status_reschedule' => 4,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );

            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();
            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        }
    }

    public function update_schedule_reward_prestasi($data) {
        $schedule = $this->input->post('radio');
        $id_booking = $this->input->post('id_booking');
        $id_jamaah = $this->input->post('id_jamaah');
        $id_registrasi = $this->input->post('id_registrasi');
        $waktutunggu_keberangkatan = $this->input->post('waktutunggu_keberangkatan');
        $harga_paket = $this->input->post('harga_paket');
        $tipe_jamaah_ori = $this->input->post('kd_tipe');
        $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $key => $value) {
            $id_schedule = $value['id_schedule'];
            $id_product = $value['id_product'];
            $id_room_type = $value['id_room_type'];
            $room_category = $value['room_category'];
            $embarkasi = $value['embarkasi'];
            $harga = $value['harga'];
            $id_productprice = $value['id_productprice'];
            $seats = $value['seats'];
            $pindah_bulan_menunggu = $value['waktutunggu'];
            $id_tipe_jamaah = $value['id_tipe_jamaah'];
        }
        if ($room_category == 2) {
            $harga_bayar = ($harga - $harga_paket);
            // $waktu_tunggu =($pindah_bulan_menunggu - );
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => $harga_bayar,
                'status' => 0,
                'status_reschedule' => 6,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        } elseif ($pindah_bulan_menunggu >= $waktutunggu_keberangkatan) {

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 2,
                'status_reschedule' => 7,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();

            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('aktivasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        } elseif ($waktutunggu_keberangkatan > $pindah_bulan_menunggu) {

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 2,
                'status_reschedule' => 7,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();


            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('aktivasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));
            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        }
    }

    function get_data_faktur($id_booking) {
        $this->db->select('*');
        $this->db->from('aktivasi_reschedule_reguler');
        $this->db->where('id_reschedule', $id_reschedule);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $out = $query->row();
            return $out;
        } else {
            return FALSE;
        }
    }

    public function update_schedule_voucher($data) {
        $schedule = $this->input->post('radio');
        $id_affiliate = $this->input->post('id_affiliate');
        $id_booking = $this->input->post('id_booking');
        $id_jamaah = $this->input->post('id_jamaah');
        $id_registrasi = $this->input->post('id_registrasi');
        $waktutunggu_keberangkatan = $this->input->post('waktutunggu_keberangkatan');
        $harga_paket = $this->input->post('harga_paket');
        $tipe_jamaah_ori = $this->input->post('kd_tipe');
        $sql = "SELECT *
         from view_schedule where id_schedule in ('$schedule')";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $key => $value) {
            $id_schedule = $value['id_schedule'];
            $id_product = $value['id_product'];
            $id_room_type = $value['id_room_type'];
            $room_category = $value['room_category'];
            $embarkasi = $value['embarkasi'];
            $harga = $value['harga'];
            $id_productprice = $value['id_productprice'];
            $seats = $value['seats'];
            $pindah_bulan_menunggu = $value['waktutunggu'];
            $id_tipe_jamaah = $value['id_tipe_jamaah'];
        }
        if ($id_product == 4) {
            $this->session->set_flashdata('Warning', "JAMAAH VOUCHER TIDAK BISA RESCHEDULE KE PAKET TUNAI");
            $id_jamaah = $this->input->post('id_jamaah');
            // data_jamaah/jamaah_aktif/reschedule_jamaah_voucher/470/5561611/SBL01
            redirect('data_jamaah/jamaah_aktif/reschedule_jamaah_voucher/' . $id_booking . '/' . $id_jamaah . '/' . $id_affiliate . '');
        } elseif ($pindah_bulan_menunggu >= $waktutunggu_keberangkatan) {

            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'tipe_jamaah' => $id_tipe_jamaah,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => 0,
                'status' => 2,
                'status_reschedule' => 3,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();

            $arr = array(
                'id_product' => $id_product,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('booking', $arr, array('id_booking' => $data['id_booking']));

            $arr = array(
                'category' => $room_category,
                'room_type' => $id_room_type,
                'id_schedule' => $id_schedule,
            );
            $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));

            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('fee_input', $arr, array('id_registrasi' => $id_registrasi));


            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('aktivasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));



            $arr = array(
                'id_schedule' => $id_schedule,
                'id_product' => $id_product,
            );
            $this->db->update('list_gaji', $arr, array('id_registrasi' => $id_registrasi));

            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');

                return true;
            }
        } elseif ($waktutunggu_keberangkatan > $pindah_bulan_menunggu) {

            $biaya_setting_ = $this->get_key_val();
            foreach ($biaya_setting_ as $key => $value) {
                $out[$key] = $value;
            }
            $waktu_tunggu = ($waktutunggu_keberangkatan - $pindah_bulan_menunggu);
            $harga_pindah = ($out['PINDAH_JADWAL'] * $waktu_tunggu);
            // print_r($waktu_tunggu);
            $arr = array(
                'id_booking' => $data['id_booking'],
                'id_jamaah' => $id_jamaah,
                'tipe_jamaah' => $id_tipe_jamaah,
                'id_product' => $id_product,
                'category' => $room_category,
                'room_type' => $id_room_type,
                'ori_schedule' => $this->input->post('id_schedule'),
                'reschedule' => $id_schedule,
                'biaya' => $harga_pindah,
                'status' => 0,
                'status_reschedule' => 1,
                'create_by' => $this->session->userdata('id_user'),
                'create_date' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->db->insert($this->_aktivasi_reschedule, $arr);
            $id_reschedule = $this->db->insert_id();
            if ($this->db->trans_status() === false) {

                $this->db->trans_rollback();
                return false;
            } else {

                $this->db->trans_complete();
                redirect('data_jamaah/jamaah_aktif/report_reschedule/' . $id_reschedule . '');
                return true;
            }
        }
    }

    private function _get_datatables_query() {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function searchItempaketpromo($datepicker_tahun_keberangkatan, $datepicker_keberangkatan, $departure) {



        $sql = "SELECT * from view_schedule_promo where  BulanKeberangkatan='$datepicker_keberangkatan' 
          and TahunKeberangkatan='$datepicker_tahun_keberangkatan' and embarkasi='$departure'";
        $query = $this->db->query($sql);

        if (empty($query->result())) {

            echo'<tr><td colspan="9"><h2 style="color: #9F6000;">Maaf ! Jadwal Yang Anda Cari Kosong </h2></td></tr>';
            exit;
        } else {


            foreach ($query->result() as $row) {


                echo'<tr>
                   
                   <td><strong>' . $row->paket . '</strong></td>
                   <td><strong>' . $row->departure . '</strong></td>
                   <td><strong>' . $row->date_schedule . '</strong></td>
                  
                   <td><strong>' . $row->seats . '</strong></td>
                   <td><strong>' . $row->type . '</strong></td>
                   <td><strong>' . $row->category . ' - ' . $row->hari . ' hari</strong></td>
                    <td> <input name="radio" class="radio1" type="radio" id="radio1" value="' . $row->id_schedule . ' "</td>
                  
              
                  
                </tr>';
            }
        }
    }

    public function update_schedule_paketpromo($data) {
        $schedule = $this->input->post('radio');
        $id_booking = $this->input->post('id_booking');
        $id_jamaah = $this->input->post('id_jamaah');
        $id_registrasi = $this->input->post('id_registrasi');

        $harga_paket = $this->input->post('harga_paket');
        $tipe_jamaah_ori = $this->input->post('kd_tipe');
        $jadwal_sebelumnya = $this->input->post('id_schedule');
        $seats_lama = $this->input->post('seats_lama');
        $sql = "SELECT *
         from view_schedule_promo where id_schedule = '$schedule'";
        $query = $this->db->query($sql)->result_array();

        foreach ($query as $key => $value) {
            $id_schedule = $value['id_schedule'];
            $id_product = $value['id_product'];
            $id_room_type = $value['id_room_type'];
            $room_category = $value['room_category'];
            $embarkasi = $value['embarkasi'];
            $harga = $value['harga'];
            $id_productprice = $value['id_productprice'];
            $seats_baru = $value['seats'];
        }
// print_r($schedule);
        $arr = array(
            'seats' => $seats_baru - 1,
        );
        $this->db->update('schedule', $arr, array('id_schedule' => $schedule));


        //  $sql = "SELECT *
        //    from schedule where id_schedule = '$jadwal_sebelumnya'";
        //   $query = $this->db->query($sql)->result_array();
        // foreach($query as $key=>$value){
        //   $seat_sebelumnya = $value['seats'];
        // }
        $arr = array(
            'seats' => $seats_lama - 1,
        );
        $this->db->update('schedule', $arr, array('id_schedule' => $jadwal_sebelumnya));



        $arr = array(
            'embarkasi' => $embarkasi,
            'id_schedule' => $id_schedule,
        );
        $this->db->update('registrasi_jamaah', $arr, array('id_jamaah' => $id_jamaah));


        $arr = array(
            'id_schedule' => $id_schedule,
        );
        $this->db->update('fee', $arr, array('id_registrasi' => $id_registrasi));



        if ($this->db->trans_status() === false) {

            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->trans_complete();
            redirect('data_jamaah/jamaah_aktif/reschedule_jamaah_paketpromo/' . $id_booking . '/' . $id_jamaah . '');
            return true;
        }
    }

}
