<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengiriman_barang extends CI_Controller{
	var $folder = "pengiriman_barang";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(PENGIRIMAN_BARANG,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','logistik');	
		$this->load->model('pengirimanbarang_model');
		$this->load->model('pengirimanbarang_model','r');
	}
	
public function index(){
	
		
	   
	   $this->template->load('template/template', $this->folder.'/pengiriman_barang');
		
	}


	private function _select_pengiriman(){
		
		return $this->db->get('jasa_pengiriman')->result();
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(PENGIRIMAN_BARANG,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $stored_procedure = "call pengiriman_barang(?)";
          $get = $this->db->query($stored_procedure,array('id_jamaah'=>$id))->row_array(); 
	     
	    if(!$get)
	        show_404();
	        
	   			 $data = array('select_pengiriman'=>$this->_select_pengiriman(),
	    
	    				
	    	);
        $this->template->load('template/template', $this->folder.'/edit',array_merge($get,$data));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->pengirimanbarang_model->update($data);
	     if($send)
	        redirect('pengiriman_barang');
		
	}
	
	public function ajax_list()
	{
		$list = $this->r->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
;
	                $row[] ='<td width="15%" >'.$r->invoice.'</strong><br>
	             			 <strong>('.$r->id_jamaah.' - '.$r->nama.')<br>
	             			 Telp : '.$r->telp.'<br>
	             			 '.$r->tipe_jamaah.'
	               			 </td>';
	               	$row[] ='<td width="15%" >'.$r->paket.'<br>
	             			 Tgl Berangkat : '.$r->bulan_keberangkatan.'<br>
	             			 Waktu Tunggu : <strong>'.$r->bulan_menunggu.' Bulan </strong>
	               			 </td>';
	               	$row[]  ='<td width="10%" >ID Affiliate : '.$r->id_affiliate.'</td>';
	                			
	                $row[] ='<td width="15%">'.$r->alamat.'<br>
	             			

	                		</td>';

	                		if ($r->pengiriman == 2)	{ 
	                $row[] ='<a title="sudah di Kirim" class="btn btn-sm btn-primary" href="'.base_url().'pengiriman_barang/edit/'.$r->id_registrasi.'">
	                            sudah diKirim
	                        </a> ';
	              		}else{
	              	 $row[] ='<a title="Kirim" class="btn btn-sm btn-danger" href="'.base_url().'pengiriman_barang/edit/'.$r->id_registrasi.'">
	                            Kirim
	                        </a> ';
	              		}
			$data[] = $row;
		}
			$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->r->count_all(),
						"recordsFiltered" => $this->r->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	


}
