<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Affiliate_model extends CI_Model{
   
   
    private $_table="affiliate";
    private $_primary="id_aff";


     public function generate_kode($idx){
        $today=date('ymd');
        
        $ret = '';
        
        $limit = 8;
                
        for($x=0;$x<($limit- strlen($idx));$x++){
        
            $ret .='0';
        }
        
        return $idx.$today; 
    }

    public function save($data){
        
        $get_initial = $this->db->select('affiliate_code')
                                ->where('id_affiliate_type',$data['affiliate_type'])->get('affiliate_type')->row();
        
        $initial = '';
        if($get_initial)
            $initial = $get_initial->affiliate_code;
        
            
        $arr = array(
            // 'id_user' => $data['id_user'],
            'id_affiliate_type' => $data['affiliate_type'],
            'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
            'username' => $data['username'],
            'password' => md5($data['password']),
            'telp' => $data['telp'],
            'email' => $data['email'],

            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'hubwaris' => $data['hubwaris'],
            'waris' => $data['waris'],

            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            // 'provinsi_id' => $data['provinsi_id'],

            'kabupaten_id' => $data['kabupaten_id'],
            'kecamatan_id' => $data['kecamatan_id'],
            'provinsi_id' => $data['provinsi_id'],

            'alamat' => $data['alamat'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
             'foto' => $data['gambar'],
            'bank' => $data['select_bank'],
            'status' => $data['select_status'],
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('nama'),
            'create_by'=>$this->session->userdata('id_user')
        );       
        
         $this->db->trans_begin(); //transaction initialize
        
        $this->db->insert($this->_table,$arr);
        $id_aff =  $this->db->insert_id(); //get last insert ID
        
        //krena kode_client hrus digenerate, do it
        $this->db->update($this->_table,
                    array('id_user'=> $this->generate_kode($initial.$id_aff)),
                    array('id_aff'=>$id_aff));

        // return $this->db->insert('affiliate',$arr);
        
       
    }

    function cek($id_affiliate){
        $query = $this->db->query("SELECT * FROM affiliate Where id_affiliate ='$id_affiliate'");
        return $query;
    }
    

    public function update($data){
        
        $arr = array(
        
            // 'id_affiliate_type' => $data['affiliate_type'],
            // 'id_affiliate' => $data['id_affiliate'],
            'nama' => $data['nama'],
            'username' => $data['username'],
            // 'password' => $data['password'],
            'telp' => $data['telp'],
            'email' => $data['email'],

            'ktp' => $data['ktp'],
            'kelamin' => $data['select_kelamin'],
            'hubwaris' => $data['hubwaris'],
            'waris' => $data['waris'],
            'kelamin' => $data['select_kelamin'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            // 'provinsi_id' => $data['provinsi_id'],

            // 'kabupaten_id' => $data['kabupaten'],
            // 'kecamatan_id' => $data['kecamatan'],
            // 'provinsi_id' => $data['provinsi'],

            'alamat' => $data['alamat'],
            'namarek' => $data['namarek'],
            'norek' => $data['norek'],
             'foto' => $data['gambar'],
            'bank' => $data['select_bank'],
            'status' => $data['select_status'],
            'tanggal_daftar' => date('Y-m-d H:i:s'),
            'update_by'=>$this->session->userdata('id_user'),
            // 'create_by'=>$this->session->userdata('nama')
        );       
        
        // if($data['password']!='') || ($data['provinsi']!='') || ($data['kecamatan']!='') || ($data['kabupaten']!=''){
         if($data['password']!='') {
          
            $arr['password'] = md5($data['password']);
          
        }
         if($data['provinsi']!='' && $data['kecamatan']!='' && $data['kabupaten']!='') {
          
            // $arr['password'] = ($data['password']);
            $arr['provinsi_id'] = ($data['provinsi']);
            $arr['kecamatan_id'] = ($data['kecamatan']);
            $arr['kabupaten_id'] = ($data['kabupaten']);

        }

                
        // return $this->db->update('user',$arr,array('id_user'=>$data['id_user']));
        return $this->db->update('affiliate',$arr,array('id_aff'=>$data['id_aff']));

    }

    function get_all_provinsi() {
        $this->db->select('*');
        $this->db->from('wilayah_provinsi');
        $query = $this->db->get();
        
        return $query->result();
    }
     function get_all_kabupaten() {
          $query = $this->db->query("SELECT nama FROM wilayah_kabupaten");
        //  $this->db->select('nama');
        // $this->db->from('wilayah_kabupaten');
        // $query = $this->db->get();
        return $query->result();
    }
    
    public function get_data($offset,$limit,$q=''){
    
    //     $sql = "SELECT a.id_product, a.kode, a.nama ,b.keterangan from product as a , status_aktivasi as b
				// where a.status = b.kdstatus and 1=1  ";

          $sql = " SELECT a.*,b.keterangan,c.nama as provinsi, d.nama as kabupaten, e.nama as kecamatan, f.keterangan as hubungan, g.affiliate_type FROM affiliate a 
                    LEFT JOIN wilayah_provinsi c ON c.provinsi_id = a.provinsi_id
                    LEFT JOIN wilayah_kabupaten d ON d.kabupaten_id = a.kabupaten_id
                    LEFT JOIN wilayah_kecamatan e ON e.kecamatan_id = a.kecamatan_id
                    LEFT JOIN family_relation f ON f.id_relation = a.hubwaris
                    LEFT JOIN affiliate_type g ON g.id_affiliate_type = a.id_affiliate_type
                    LEFT JOIN status b ON b.kdstatus = a.status
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND a.id_user LIKE '%{$q}%' 
            		OR a.nama LIKE '%{$q}%'
                    OR a.telp LIKE '%{$q}%'
            		OR c.nama LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY a.id_affiliate DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

     public function delete($id_aff){
    
        $this->db->trans_begin(); //transaction initialize
        
            $this->db->delete($this->table,array('id_aff'=>$id_aff));
            // $this->db->delete('user',array('id_user'=>$id_user));
        
        if($this->db->trans_status()===false){
            
            $this->db->trans_rollback();
            return false;    
            
        }else{
            
            $this->db->trans_complete();
            return true;
        }
  
    }
    
    public function delete_by_id($id_aff)
    {
        $this->db->where('id_aff', $id_aff);
        $this->db->delete($this->_table);
    }
    
    
}
