 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit Room Type</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>room/room_type/update">
             <div class="form-group">
                <label>Room Type</label>
                <input type="hidden" name="id_room_type" value="<?php echo $id_room_type;?>">
                <input class="form-control" name="room_type" value="<?php echo $type;?>" required autofocus>
            </div>
           
            
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>room/room_type" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->
