 <div id="page-wrapper">
    
    
       <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        <h3 class="page-header">Add Room Category</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>room/room_category/save">
            
            <div class="form-group">
                <label>Room Category </label>
                <input class="form-control" name="category" required>
            </div>
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>room/room_category" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
    </div>
    </div>
       </div>
    </div>
</div>
<!-- /#page-wrapper -->
