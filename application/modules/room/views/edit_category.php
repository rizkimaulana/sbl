 <div id="page-wrapper">
    
    <div class="row">
       	<div class="col-lg-7">
       		<div class="panel panel-default">
       			<div class="panel-body">
        
        <h3 class="page-header">Edit Room Category</h3>
       
        <form role="form" method="post" action="<?php echo base_url();?>room/room_category/update">
             <div class="form-group">
                <label>Room Category</label>
                <input type="hidden" name="id_room_category" value="<?php echo $id_room_category;?>">
                <input class="form-control" name="category" value="<?php echo $category;?>" required autofocus>
            </div>
           
            
            
            
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a href="<?php echo base_url();?>room/room_category" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
        </form>
       
    </div>
</div>
</div>
</div>
</div>
<!-- /#page-wrapper -->
