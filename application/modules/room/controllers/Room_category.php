<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_category extends CI_Controller{
	var $folder = "room";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(ROOM_CATEGORY,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('roomcategory_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/room_category');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->roomcategory_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                // $rows .='<td width="10%">'.$r->kode.'</td>';
	                // $rows .='<td width="40%">'.$r->nama.'</td>';
	                $rows .='<td width="70%">'.$r->category.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'room/room_category/edit/'.$r->id_room_category.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room_category('."'".$r->id_room_category."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'room/room_category/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->roomcategory_model->save($data);
	     if($send)
	        redirect('room/room_category');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(ROOM_CATEGORY,'add'))
		    $this->general->no_access();

	    
	    // $data = array('select_status'=>$this->_select_status());
        $this->template->load('template/template', $this->folder.'/add_roomcategory');	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(ROOM_CATEGORY,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(4);
	    $get = $this->db->get_where('room_category',array('id_room_category'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   
        $this->template->load('template/template', $this->folder.'/edit_category',array_merge($get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->roomcategory_model->update($data);
	     if($send)
	        redirect('room/room_category');
		
	}
	
	
	public function ajax_delete($id_room_category)
	{
		if(!$this->general->privilege_check(ROOM_CATEGORY,'remove'))
		    $this->general->no_access();
		$send = $this->roomcategory_model->delete_by_id($id_room_category);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('room/room_category');
	}


}
