<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room_type extends CI_Controller{
	var $folder = "room";
	public function __construct(){
		
	parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(ROOM_TYPE,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('roomtype_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/room_type');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(4,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->roomtype_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                // $rows .='<td width="10%">'.$r->kode.'</td>';
	                // $rows .='<td width="40%">'.$r->nama.'</td>';
	                $rows .='<td width="70%">'.$r->type.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'room/room_type/edit/'.$r->id_room_type.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_room_type('."'".$r->id_room_type."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                					
            'base_url'  => base_url().'room/room_type/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 4
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->roomtype_model->save($data);
	     if($send)
	        redirect('room/room_type');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(ROOM_TYPE,'add'))
		    $this->general->no_access();

	    
	    // $data = array('select_status'=>$this->_select_status());
        $this->template->load('template/template', $this->folder.'/add_roomtype');	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(ROOM_TYPE,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(4);
	    $get = $this->db->get_where('room_type',array('id_room_type'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   
        $this->template->load('template/template', $this->folder.'/edit_type',array_merge($get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->roomtype_model->update($data);
	     if($send)
	        redirect('room/room_type');
		
	}
	
	
	public function ajax_delete($id_room_type)
	{
		if(!$this->general->privilege_check(ROOM_TYPE,'remove'))
		    $this->general->no_access();
		$send = $this->roomtype_model->delete_by_id($id_room_type);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('room/room_type');
	}


}
