<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roomtype_model extends CI_Model{
   
   
    private $_table="room_type";
    private $_primary="id_room_type";

    public function save($data){
    
        // $cek = $this->db->select('ket_hubungan')->where('ket_hubungan',$data['ket_hubungan'])->get('hubungan_muhrim')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'type' => $data['room_type'],
        );       
        
         return $this->db->insert('room_type',$arr);
    }


    public function update($data){
        
        $arr = array(
        
           'type' => $data['room_type'],
            
            // 'status' =>$data['status'],
        );       
              
        return $this->db->update($this->_table,$arr,array('id_room_type'=>$data['id_room_type']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
 
          $sql = " SELECT * FROM room_type  
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND type LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_room_type DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_room_type)
    {
        $this->db->where('id_room_type', $id_room_type);
        $this->db->delete($this->_table);
    }
    
    
}
