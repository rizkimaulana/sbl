<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roomcategory_model extends CI_Model{
   
   
    private $_table="room_category";
    private $_primary="id_room_category";

    public function save($data){
    
        // $cek = $this->db->select('ket_hubungan')->where('ket_hubungan',$data['ket_hubungan'])->get('hubungan_muhrim')->num_rows();
        // if($cek)
        //     return true; //smntara ret true ajalah
            
            
        $arr = array(
        
            'category' => $data['category'],
        );       
        
         return $this->db->insert('room_category',$arr);
    }


    public function update($data){
        
        $arr = array(
        
           'category' => $data['category'],
            
            // 'status' =>$data['status'],
        );       
              
        return $this->db->update($this->_table,$arr,array('id_room_category'=>$data['id_room_category']));
    }

    
    
    public function get_data($offset,$limit,$q=''){
    
 
          $sql = " SELECT * FROM room_category  
                    WHERE 1=1
                    ";
        
        if($q){
            
            $sql .=" AND category LIKE '%{$q}%'";
        }
        $sql .=" ORDER BY id_room_category DESC ";
        $ret['total'] = $this->db->query($sql)->num_rows();
        
            $sql .=" LIMIT {$offset},{$limit} ";
        
        $ret['data']  = $this->db->query($sql)->result();
       
        return $ret;
        
    }

 
    
    public function delete_by_id($id_room_category)
    {
        $this->db->where('id_room_category', $id_room_category);
        $this->db->delete($this->_table);
    }
    
    
}
