<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biaya_model extends CI_Model {

    var $table = 'registrasi_jamaah';//'jamaah_aktif';
    //var $column_order = array(null, 'id_registrasi', 'id_booking', 'id_affiliate', 'invoice', 'id_sahabat', 'id_jamaah', 'nama', 'telp', 'no_identitas', 'tgl_daftar', 'category','departure', 'id_user', 'tipe_jamaah', 'status', 'kd_tipe', 'tgl_aktivasi', 'update_date'); //set column field database for datatable orderable
    //var $column_search = array('id_registrasi', 'id_booking', 'id_affiliate', 'invoice', 'id_sahabat', 'id_jamaah', 'nama', 'telp', 'no_identitas', 'tgl_daftar', 'category','departure', 'id_user', 'tipe_jamaah', 'status', 'kd_tipe', 'tgl_aktivasi', 'update_date'); //set column field database for datatable searchable 

    var $column_order = array(null, 'id_jamaah', 'id_booking', 'invoice',  'nama', 'telp',  'tgl_daftar', 'category','tipe_jamaah', 'status', 'kd_tipe', 'tgl_aktivasi', 'update_date'); //set column field database for datatable orderable
    var $column_search = array('id_jamaah', 'id_booking', 'invoice',  'nama', 'telp',  'tgl_daftar', 'category', 'tipe_jamaah', 'status', 'kd_tipe', 'tgl_aktivasi', 'update_date'); //set column field database for datatable searchable 
    var $order = array('id_jamaah' => 'asc'); // default order 
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function generate_kode_akomodasi($idx) {
        $today = date('ymd');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return 'AKM'. $today . $idx;
    }
    
    public function generate_kode_muhrim($idx) {
        $today = date('ymd');

        $ret = '';

        $limit = 8;

        for ($x = 0; $x < ($limit - strlen($idx)); $x++) {

            $ret .= '0';
        }

        return 'MHR'. $today . $idx;
    }
    
    public function proses_akomodasi($data){
        $this->db->trans_begin();
        //1. Insert ke tabel aktivasi_biaya_lain
        $arr = array(
            'id_booking' => $data['id_booking'],
            'registrasi_jamaah_id_affiliate' => $data['id_affiliate'],
            'registrasi_jamaah_id_jamaah' => $data['id_jamaah'],
            'tipe_biaya' => '133', //biaya akomodasi
            'payment_method' => $data['select_pay_method'],
            'payment_date' => $data['payment_date'],
            'keterangan' => $data['keterangan'],
            'nominal' => $data['akomodasi'],
            'bank' => $data['select_bank'],
            'status' => 1,
            'create_by' => $this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s'),
            'trailer' => ''
        );
        
        if (isset($data['pic1'])) {
            $arr['file_image'] = $data['pic1'];
        }
        
        $this->db->insert('aktivasi_biaya_lain', $arr);
        $id = $this->db->insert_id();
        
        //2. update kd_trasn di aktivasi_biaya_lain
        $kd_trans = $this->generate_kode_akomodasi($id);
        $this->db->update('aktivasi_biaya_lain', array('kd_trans' => $kd_trans), array('id' => $id));
        
        //3. Update akomodasi di tabel registrasi_jamaah
        $update_registrasi = array(
            'akomodasi' => $data['akomodasi']
        );
        $this->db->update('registrasi_jamaah', $update_registrasi, array('id_jamaah' => $data['id_jamaah']));
        
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_complete();
            return true;
        }
    }
    
//    private function _get_datatables_query() {
//        $this->db->where('status', 1);
//        $this->db->from($this->table);
//
//        $i = 0;
//        
//        foreach ($this->column_search as $item) { // loop column 
//            if (!empty($_POST['search']['value'])) { // if datatable send POST for search
//
//                if ($i === 0) { // first loop
//                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
//                    $this->db->like($item, $_POST['search']['value']);
//                } else {
//                    $this->db->or_like($item, $_POST['search']['value']);
//                }
//
//                if (count($this->column_search) - 1 == $i) //last loop
//                    $this->db->group_end(); //close bracket
//            }
//            $i++;
//        }
//
//        if (isset($_POST['order'])) { // here order processing
//            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
//        } else if (isset($this->order)) {
//            $order = $this->order;
//            $this->db->order_by(key($order), $order[key($order)]);
//        }
//    }
//
//    function get_datatables() {
//        $this->_get_datatables_query();
//        //print_r(!empty($_POST['length'])? $_POST['length'] : 'test');die();
//        if ($_POST['length'] != -1)
//            $this->db->limit($_POST['length'], $_POST['start']);
//        $this->db->limit(5);
//        $query = $this->db->get();
//        return $query->result();
//    }
    
    private function _get_datatables_query($param) {
        $this->db->where('status', 1);
        $this->db->like('id_jamaah',$param);
        $this->db->or_like('nama',$param);
        //$this->db->where('id_bandara', 2);
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if (isset($_POST['search']['value'])) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($param) {
        $this->_get_datatables_query($param);
        if (isset($_POST['length']) != -1)
            $this->db->limit(isset($_POST['length']), isset($_POST['start']));
        $this->db->limit(100);
        $query = $this->db->get();
        return $query->result();
    }
    
    private function _get_data_muhrim_query($param) {
        $this->db->where('status', 1);
        $this->db->like('id_jamaah',$param);
        $this->db->or_like('nama',$param);
        
//        $this->db->where('muhrim <>', '0');
//        $this->db->where('muhrim <>', '-');
//        $this->db->where('muhrim <> ', '');
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if (isset($_POST['search']['value'])) { // if datatable send POST for search

                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        
    }
    
    function get_data_muhrim($param) {
        $this->_get_data_muhrim_query($param);
        if (isset($_POST['length']) != -1)
            $this->db->limit(isset($_POST['length']), isset($_POST['start']));
        $this->db->limit('100');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function proses_muhrim($data){
        $this->db->trans_begin();
        //1. Insert ke tabel aktivasi_biaya_lain
        $arr = array(
            'id_booking' => $data['id_booking'],
            'registrasi_jamaah_id_affiliate' => $data['id_affiliate'],
            'registrasi_jamaah_id_jamaah' => $data['id_jamaah'],
            'tipe_biaya' => '144', //biaya muhrim
            'payment_method' => $data['select_pay_method'],
            'payment_date' => $data['payment_date'],
            'keterangan' => $data['keterangan'],
            'nominal' => $data['muhrim'],
            'bank' => $data['select_bank'],
            'status' => 1,
            'create_by' => $this->session->userdata('id_user'),
            'create_date' => date('Y-m-d H:i:s'),
            'trailer' => ''
        );
        
        if (isset($data['pic1'])) {
            $arr['file_image'] = $data['pic1'];
        }
        
        $this->db->insert('aktivasi_biaya_lain', $arr);
        $id = $this->db->insert_id();
        
        //2. update kd_trasn di aktivasi_biaya_lain
        $kd_trans = $this->generate_kode_muhrim($id);
        $this->db->update('aktivasi_biaya_lain', array('kd_trans' => $kd_trans), array('id' => $id));
        
        //3. Update akomodasi di tabel registrasi_jamaah
        $update_registrasi = array(
            'muhrim' => $data['muhrim']
        );
        $this->db->update('registrasi_jamaah', $update_registrasi, array('id_jamaah' => $data['id_jamaah']));
        
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_complete();
            return true;
        }
    }
    
    function get_data_aktivasi_lain($param){
        return $this->db->query("SELECT * FROM aktivasi_biaya_lain WHERE tipe_biaya = '133'".$param)->result();
    }
    
    function get_data_aktivasi($id_jamaah, $tipe){
        $sql = "SELECT a.*, b.nama, b.alamat, b.telp FROM aktivasi_biaya_lain a ".
                "JOIN registrasi_jamaah b ON b.id_jamaah = a.registrasi_jamaah_id_jamaah ".
                "WHERE a.registrasi_jamaah_id_jamaah = '".$id_jamaah."' AND a.tipe_biaya = '".$tipe."'";
        return $this->db->query($sql);
    }
    
    
}
