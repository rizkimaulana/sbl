<html lang="en">
    <head>
    <style>
        th, td { white-space: nowrap; }
    </style>
    <div class="table-responsive">
        <table border=1 bordercolor="#000000">
            <thead>                      
                <tr>
                    <?php
                        header("Content-type: application/vnd.ms-excel");
                        header("Content-Disposition: attachment; filename=lap_biaya_akomodasi".date('d-M-Y').".xls");
                    ?> 
                    <th>No</th>
                    <th>Kode Trans</th>
                    <th>Invoice</th>
                    <th>Id Jamaah</th>
                    <th>Nama Jamaah</th>
                    <th>Telp</th>
                    <th>Tipe</th>
                    <th>Paket</th>
                    <th>Tgl Daftar</th>
                    <th>Tgl Aktivasi</th>
                    <th>Biaya Akomodasi</th>
                    <th>Cara Bayar</th>
                    <th>Tgl Bayar</th>
                    <th>Nama Bank</th>
                    <th>Keterangan</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php $no = 0;
                    foreach ($list as $pi) {
                    $no++ ?>
                    <tr>
                        <td><?php echo $no; ?>
                        <td><?php echo $pi['kd_trans'] ?>  </td>
                        <td><?php echo $pi['invoice'] ?>  </td>
                        <td><?php echo $pi['id_jamaah'] ?>  </td>
                        <td><?php echo $pi['nama'] ?>  </td>
                        <td><?php echo $pi['telp'] ?>  </td>
                        <td><?php echo $pi['tipe'] ?>  </td>
                        <td><?php echo $pi['paket'] ?>  </td>
                        <td><?php echo $pi['tgl_daftar'] ?>  </td>
                        <td><?php echo $pi['tgl_aktivasi'] ?>  </td>
                        <td><?php echo $pi['nominal'] ?>  </td>
                        <td><?php echo $pi['payment_method'] ?>  </td>
                        <td><?php echo $pi['payment_date'] ?>  </td>
                        <td><?php echo $pi['bank'] ?>  </td
                        <td><?php echo $pi['keterangan'] ?>  </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</head>
</html>
<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<link href="<?php echo base_url();?>assets/css/opensans.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/raleway.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
