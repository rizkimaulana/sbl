
<div class="page-head">
    <!-- <h2>Affiliate </h2> -->
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">BIAYA LAIN-LAIN</a></li>
        <li class="active">BIAYA MUHRIM</li>
    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>LIST BIAYA MUHRIM</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="col-lg-12">Cari data jamaah berdasarkan ID Jamaah / Nama Jamaah </label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="search" name="search" onkeyup="search()" placeholder="Search by Id Jamaah / Nama Jamaah">
                                </div>
                            </div>
                        </div>
                    </div>      

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover " >
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TANGGAL DFTR/AKT </th>
                                    <th>BIAYA</th>
                                    <th>ACTION </th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>                     

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script>



//    var table;
//
//    $(document).ready(function () {
//
//        //datatables
//        table = $('#data-table').DataTable({
//
////        "processing": true, //Feature control the processing indicator.
////        "serverSide": true, //Feature control DataTables' server-side processing mode.
//            "order": [], //Initial no order.
//
//            // Load data for the table's content from an Ajax source
//            "ajax": {
//                "url": "<?php echo site_url('biaya_lain/ajax_muhrim_list') ?>",
//                "type": "POST"
//            },
//
//            //Set column definition initialisation properties.
//            "columnDefs": [
//                {
//                    "targets": [0], //first column / numbering column
//                    "orderable": false, //set not orderable
//                },
//            ],
//
//        });
//
//    });

    var table;
    $(document).ready(function () {

        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            // "serverSide": true,
            "info": false,
            "searching": false,
            "pageLength": 5,
            "bLengthChange": false,

        });

    });
    
    function search() {
        keyword = $("#search").val();
        if(keyword){
            table.ajax.url("<?php echo site_url('biaya_lain/ajax_muhrim_list') ?>/" + keyword);
            table.ajax.reload();
        }
    }
</script>







