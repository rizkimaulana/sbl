<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Detail Biaya Lain-lain</b>
                </div>
                <div class="panel-body">
                    
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
<!--                                    <th>NO</th>
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TGL DAFTAR/AKT</th>
                                    <th>BIAYA</th>
                                    <th>PAYMENT</th>
                                    <th>KETERANGAN</th>-->
                                    <th>No.</th>
                                    <th>Tgl Bayar</th>
                                    <th>Invoice</th>
                                    <th>Id Affiliate</th>
                                    <th>Data Jamaah</th>
                                    <th>Total</th>
                                    <th>Cara Bayar</th>
<!--                                    <th>Jml Bayar</th>-->
                                    <th>Keterangan</th>
<!--                                    <th>User</th>
                                    <th>Detail</th>-->
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!--    <div class="pull-right">
                         <ul class="pagination"></ul>    
                      </div>   -->
                </div>
            </div>
        </div>
    </div>    
</div>

<input type="hidden" name="pay_date" id="pay_date" value="<?php echo $pay_date; ?>">
<input type="hidden" name="tipe" id="tipe" value="<?php echo $tipe; ?>">
<input type="hidden" name="pay_method" id="pay_method" value="<?php echo $pay_method; ?>">

<script type="text/javascript">
    var table;
    
    $(document).ready(function () {
        var pay_date, tipe, pay_method;
        pay_date = $('#pay_date').val();
        tipe = $('#tipe').val();
        pay_method = $('#pay_method').val();
        
        //datatables
        table = $('#data-table').DataTable({
            //"processing": true, //Feature control the processing indicator.
            //"serverSide": true, //Feature control DataTables' server-side processing mode.
            //"order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('biaya_lain/laporan/ajax_list_detail') ?>/"+pay_date+'/'+tipe+'/'+pay_method,
                "type": "POST"
            },

            //Set column definition initialisation properties.
//            "columnDefs": [
//                {
//                    "targets": [0], //first column / numbering column
//                    "orderable": false, //set not orderable
//                },
//            ],

        });
        
        $('#tipe_tanggal').click(function () { //button filter event click
            var tipe_tanggal = document.getElementById("tipe_tanggal").value;
            $('#filter').val(tipe_tanggal);
        });
        
    });
</script>
