<div id="page-wrapper">
    <div class="row">
        <!-- <div class="col-lg-7"> -->
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="page-header">Proses biaya muhrim jamaah</h3>
                <form action="<?php echo base_url(); ?>biaya_lain/update_muhrim" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Biaya Muhrim Jamaah</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">Rp</div>
                                <input type="text" class="form-control"  name="biaya_akomodasi" id="biaya_akomodasi" value="<?php echo number_format($biaya); ?>"  readonly="readonly">
                                <div class="input-group-addon">.00</div>
                                <input type="hidden" class="form-control"  name="id_jamaah" id="id_jamaah" value="<?php echo $id_jamaah; ?>">
                                <input type="hidden" class="form-control"  name="nama" id="nama" value="<?php echo $nama; ?>">
                                <input type="hidden" class="form-control"  name="id_booking" id="akomodasi" value="<?php echo $id_booking; ?>">
                                <input type="hidden" class="form-control"  name="id_affiliate" id="akomodasi" value="<?php echo $id_affiliate; ?>">
                                <input type="hidden" class="form-control"  name="muhrim" id="muhrim" value="<?php echo $biaya; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="keterangan" >Keterangan Pembayaran</label>
                        <div class="col-sm-10">
                            <textarea rows="2" class="form-control" name="keterangan" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Metode Pembayaran</label>
                        <div class="col-lg-5">
                            <select required class="form-control" name="select_pay_method" id='select_pay_method'>
                                <?php foreach ($select_pay_method as $sk) { ?>
                                    <option value="<?php echo $sk->pay_method; ?>"><?php echo $sk->pay_method; ?></option>
                                <?php } ?> 
                            </select>
                        </div>
                    </div>

                    <div id="hidden_div" style="display: none;">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Bukti Pembayaran</label>
                            <div class="col-lg-5">
                                <input type="file" name="pic[]"  id="pic1" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Bank Transfer</label>
                            <div class="col-lg-5">
                                <select class="form-control" name="select_bank" id="select_bank">
                                    <option value="">- PILIH BANK TRANSFER -</option>
                                    <?php foreach ($select_bank as $sk) { ?>
                                        <option value="<?php echo $sk->nama; ?>"><?php echo $sk->nama; ?></option>
                                    <?php } ?> 
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Payment Date</label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control " name="payment_date" id="datepicker2"  placeholder="yyyy-mm-dd " required/>
                        </div>
                    </div>
                  

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>

                    <a href="<?php echo base_url(); ?>biaya_lain/muhrim" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                    
                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    });
</script>
<script type="text/javascript">
    document.getElementById('select_pay_method').addEventListener('change', function () {
        var style = this.value == 'TRANSFER' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
        // document.getElementById('hidden_div2').style.display = style;
        // $("#datepicker").prop('required',true);
        select_pay_method = $(this).val();

        console.log(select_pay_method);

        if (select_pay_method == 'TRANSFER') {
            $("#select_bank").prop('required', true);
            $("#pic1").prop('required', true);

        } else {
            $("#select_bank").prop('required', false);
            $("#pic1").prop('required', false);

        }
    });
</script>
