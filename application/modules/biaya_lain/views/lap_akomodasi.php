
<div class="page-head">
    <!-- <h2>Affiliate </h2> -->
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">BIAYA LAIN-LAIN</a></li>
        <li class="active">LAPORAN BIAYA AKOMODASI</li>
    </ol>
</div>      
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>LAPORAN BIAYA AKOMODASI</b>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">  
                            <a class="close" data-dismiss="alert">x</a>  
                            <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>  
                        </div>
                    <?php } ?>
                    <form role="form" action="<?php echo base_url(); ?>/biaya_lain/export_lap_akomodasi" method="post">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <label>Filter : </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" name="btn" value="filter" id="btn-filter" class="btn btn-primary">Filter</button>
                                    <button name="btn" value="export" id="btnExcel" class="btn btn-success">Export Excel</button>
                                </div>

                            </div>
                        </div>
                    </form>           

                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover " >
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TGL DAFTAR/AKT</th>
                                    <th>BIAYA</th>
                                    <th>PAYMENT</th>
                                    <th>KETERANGAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Appended by Ajax-->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    <div class="pull-right">
                        <ul class="pagination"></ul>    
                    </div>                     

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div><!-- /#page-wrapper -->
<script>



    var table;

    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });

        //datatables
        table = $('#data-table').DataTable({

//        "processing": true, //Feature control the processing indicator.
//        "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('biaya_lain/ajax_lap_akomodasi') ?>",
                "type": "POST",
                "data": function (data) {
                    data.awal = $('#datepicker1').val();
                    data.akhir = $('#datepicker2').val();
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

        $('#btn-filter').click(function () { //button filter event click
            //var tipe_tanggal = document.getElementById("tipe_tanggal").value;

            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            //var filter = document.getElementById("filter").value;
            table.ajax.url("<?php echo site_url('biaya_lain/ajax_lap_akomodasi'); ?>/" + awal + "/" + akhir);
            table.ajax.reload();  //just reload table

        });

    });
</script>







