<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Laporan Biaya Lain-lain</b>
                </div>
                <div class="panel-body">
                    <form action="<?php echo base_url(); ?>biaya_lain/laporan/export_laporan" role="form" method="POST" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-2">
                                <select name="tipe_tanggal" id="tipe_tanggal" class="form-control">
                                    <option value="payment_date" selected="selected">Payment Date</option>
                                    <!--<option value="tgl_daftar">Tgl Daftar</option>
                                    <option value="update_date">Tgl Update</option>-->
                                </select>
                                <input type="hidden" id="filter" name="filter" value="">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="awal" id="datepicker1" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control " name="akhir" id="datepicker2" value="<?php echo date('Y-m-d'); ?>"  placeholder="yyyy-mm-dd " required/>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                <button type="submit" id="btnExcel" class="btn btn-success">Export Excel</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
<!--                                    <th>NO</th>
                                    <th>DATA JAMAAH</th>
                                    <th>PAKET </th>
                                    <th>TGL DAFTAR/AKT</th>
                                    <th>BIAYA</th>
                                    <th>PAYMENT</th>
                                    <th>KETERANGAN</th>-->
                                    <th>No.</th>
                                    <th>Tgl Bayar</th>
                                    <th>Invoice</th>
                                    <th>Id Affiliate</th>
                                    <th>Jml Jamaah</th>
                                    <th>Total</th>
                                    <th>Cara Bayar</th>
                                    <th>Jml Bayar</th>
                                    <th>Keterangan</th>
                                    <th>User</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <!--    <div class="pull-right">
                         <ul class="pagination"></ul>    
                      </div>   -->
                </div>
            </div>
        </div>
    </div>    
</div>

<form id="form1" name="form1" action="<?php echo base_url();?>biaya_lain/laporan/detail" method="post" target="_blank" >
    <input type="hidden" name="pay_date" id="pay_date" value="">
    <input type="hidden" name="tipe" id="tipe" value="">
    <input type="hidden" name="pay_method" id="pay_method" value="">
</form>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#datepicker1').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('#datepicker2').datetimepicker({
            format: 'YYYY-MM-DD',
        });

        //datatables
        table = $('#data-table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('biaya_lain/laporan/ajax_list') ?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });
        
        $('#tipe_tanggal').click(function () { //button filter event click
            var tipe_tanggal = document.getElementById("tipe_tanggal").value;
            $('#filter').val(tipe_tanggal);
        });
        
        $('#btn-filter').click(function () { //button filter event click
            var tipe_tanggal = document.getElementById("tipe_tanggal").value;
            var awal = document.getElementById("datepicker1").value;
            var akhir = document.getElementById("datepicker2").value;
            table.ajax.url("<?php echo site_url('biaya_lain/laporan/ajax_list'); ?>/" + awal + "/" + akhir + "/" + tipe_tanggal);
            table.ajax.reload();  //just reload table
        });
    });
    
    function detail(pay_date, tipe, pay_method){
       //alert(pay_date+' '+tipe+' '+pay_method);
        
        $('#pay_date').val(pay_date);
        $('#tipe').val(tipe);
        $('#pay_method').val(pay_method);
        $('#form1').submit();
    }

</script>
