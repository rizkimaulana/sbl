<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan extends CI_Controller {

    var $folder = "biaya_lain";

    public function __construct() {

    parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(LAPORAN_BIAYA, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'biaya_lain');
        $this->load->model('laporan_model');
    }

    public function index() {
        $this->template->load('template/template', $this->folder . '/lap_biaya_lain');
    }

    public function ajax_list() {
        $awal = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : date('Y-m-d H:i:s');
        $akhir = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : date('Y-m-d H:i:s');
        $filter_by = !empty($this->uri->segment(6)) ? $this->uri->segment(6) : 'payment_date';
        
        $filter_query = '';
        if($filter_by == 'payment_date'){
            $filter_by = 'payment_date';
        }/*elseif($filter_by == "tgl_daftar"){
            $filter_by = 'tgl_daftar';
        }else{
            $filter_by = 'update_date';
        }*/
        if(!empty($awal) && !empty($akhir)){
            $filter_query = $filter_by." BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
        }
        
        $list = $this->laporan_model->get_datatables($filter_query);
        //$this->myDebug($this->db->last_query());
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d/m/Y',strtotime($item['payment_date']));
            $row[] = $item['invoice'];
            $row[] = $item['id_affiliate'];
            $row[] = '<div align="Right">'.$item['jml_jamaah'].'</div>';
            $row[] = number_format($item['total']);
            $row[] = $item['payment_method'];
            $row[] = number_format($item['nominal']);
            $row[] = $item['tipe_biaya'];
            $user = $this->db->get_where('user', array('id_user' => $item['user']))->row_array();
            $row[] = $user['nama'];
            
            $pay_date = date('Y-m-d',strtotime($item['payment_date']));
            $tipe_biaya = $item['tipe'];
            $pay_method = $item['payment_method'];
            $row[] = '<a href="javascript:void()" class="btn-sm btn-warning" title="Detail" onclick="detail(\''.$pay_date.'\',\''.$tipe_biaya.'\',\''.$pay_method.'\')">Detail</a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->laporan_model->count_all($filter_query),
            "recordsFiltered" => $this->laporan_model->count_filtered($filter_query),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    function detail(){
        $data = $this->input->post();
        $this->template->load('template/template', $this->folder . '/lap_biaya_lain_detail', $data);
    }
    
    function ajax_list_detail(){
        $pay_date = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : date('Y-m-d H:i:s');
        $tipe = !empty($this->uri->segment(5)) ? $this->uri->segment(5) : '';
        $pay_method = !empty($this->uri->segment(6)) ? $this->uri->segment(6) : '';
        
        $filter_query = '';

        if(!empty($pay_date) && !empty($tipe) && !empty($pay_method)){
            $filter_query = " DATE_FORMAT(payment_date,'%Y-%m-%d') =  '" . $pay_date . "' ".
                    "AND tipe = '".$tipe."' AND payment_method = '".$pay_method."' ";
        }
        
        $list = $this->laporan_model->get_detail($filter_query);
        $data = array();
        $no = 0;//$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = date('d/m/Y',strtotime($item['payment_date']));
            $row[] = $item['invoice'];
            $row[] = $item['id_affiliate'];
            $row[] = $item['id_jamaah'].' '.$item['nama'];
            $row[] = number_format($item['total']);
            $row[] = $item['payment_method'].' '.$item['bank'];
            //$row[] = number_format($item['nominal']);
            $row[] = $item['keterangan'];
            //$row[] = $item['user'];
            
            $data[] = $row;
        }

        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->laporan_model->count_all($filter_query),
//            "recordsFiltered" => $this->laporan_model->count_filtered($filter_query),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
            
    function export_laporan() {
        $awal = !empty($this->input->post('awal')) ? $this->input->post('awal') : date('Y-m-d');
        $akhir = !empty($this->input->post('akhir')) ? $this->input->post('akhir') : date('Y-m-d');
        $filter_by = !empty($this->input->post('filter')) ? $this->input->post('filter') : 'payment_date';
        
        $filter_query = '';
        if($filter_by == 'payment_date'){
            $filter_by = 'payment_date';
        }
        if(!empty($awal) && !empty($akhir)){
            $filter_query = $filter_by." BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
        }
        
        $hasil = $this->laporan_model->get_data_report($filter_query);
        
        $data = array();
        $no = 0;
        foreach ($hasil as $row) {
            $no++;
            $item = array();
            $item['kd_trans'] = $row->kd_trans;
            $item['invoice'] = $row->invoice;
            $item['id_jamaah'] = $row->id_jamaah;
            $item['nama'] = $row->nama;
            $item['telp'] = $row->telp;
            $item['tipe_jamaah'] = $row->tipe_jamaah;
            $item['paket'] = $row->product.' - '.$row->category; //paket
            $item['tgl_daftar'] = date('d-m-Y', strtotime($row->tgl_daftar));
            $item['tgl_aktivasi'] = date('d-m-Y', strtotime($row->update_date));            
            $item['tipe_biaya'] = $row->tipe_biaya;
            $item['nominal'] = $row->nominal;
            $item['payment_method'] = $row->payment_method;
            $item['payment_date'] = date('d-m-Y', strtotime($row->payment_date));
            $item['bank'] = $row->bank;
            $item['keterangan'] = $row->tipe_biaya;
            
            $data[] = $item;
        }
        $data['list'] = $data;
        //$this->myDebug($data['list']);
        $this->load->view('biaya_lain/laporan_excel', ($data));
    }
    
    public function muhrim() {
        $this->template->load('template/template', $this->folder . '/muhrim');
    }

    public function ajax_muhrim_list() {
        $keyword = $this->uri->segment(3) ? $this->uri->segment(3) : '';
        $keyword = urldecode($keyword);
        
        $list = $this->biaya_model->get_data_muhrim($keyword);
        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : '';
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah' => $r->tipe_jamaah))->row_array();
            $row[] = '<td width="20%" >Invoice : ' . $r->invoice . '</strong><br>
	             			 <strong>(' . $r->id_jamaah . ' - ' . $r->nama . ')<br>
	             			 Telp : ' . $r->telp . '<br>
	             			 ' . $tipe['nama'] . '
	               			 </td>';
            $product = $this->db->get_where('product', array('id_product' => $r->id_product))->row_array();
            $category = $this->db->get_where('category', array('id_room_category' => $r->category))->row_array();
            $row[] = '<td width="18%" >' . $product['nama'] . ' - ' . $category['category'] . '<br>
	             			 </td>';
            $row[] = '<td width="18%" >Tgl Daftar : ' . date('d-m-Y', strtotime($r->tgl_daftar)) . '<br>
	             			 Tgl Aktivasi : ' . date('d-m-Y', strtotime($r->update_date)) . '<br>
                      </td>';
            $biaya = $this->db->get_where('setting', array('id_setting' => '9'))->row_array();
            $row[] = number_format($biaya['key_setting']);

            $aktivasi_biaya_lain = $this->db->get_where('aktivasi_biaya_lain', array('registrasi_jamaah_id_jamaah' => $r->id_jamaah, 'tipe_biaya' => '144'))->row_array();

            if (count($aktivasi_biaya_lain['registrasi_jamaah_id_jamaah']) > 0) {
                $row[] = '<font style="color:green;"><strong>Sudah di bayar</strong></font>';
            } else {
                $row[] = '<a title="Mutasi" class="btn btn-sm btn-primary" href="' . base_url() . 'biaya_lain/proses_muhrim/' . $r->id_jamaah . '">
	                            <i class="fa fa-pencil"></i>Proses
	                        </a> ';
            }

            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function proses_muhrim($id_jamaah) {

        $registrasi = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $id_jamaah))->row_array();
        $biaya = $this->db->get_where('setting', array('id_setting' => '9'))->row_array();
        $data = array(
            'id_jamaah' => $registrasi['id_jamaah'],
            'nama' => $registrasi['nama'],
            'id_booking' => $registrasi['id_booking'],
            'id_affiliate' => $registrasi['id_affiliate'],
            'biaya' => $biaya['key_setting'],
            'select_bank' => $this->_select_bank(),
            'select_pay_method' => $this->_select_pay_method(),
        );

        $this->template->load('template/template', $this->folder . '/proses_muhrim', $data);
    }

    function update_muhrim() {
        $data = $this->input->post(null, true);
        //$this->myDebug($data);
        try {
            //SAVE FILE IMAGE
            $flag = 0;
            $rename_file = array();
            for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                if ($_FILES['pic']['name'][$i]) {
                    $rename_file[$i] = 'pic' . ($i + 1) . '_mutasi_nama_' . $_FILES['pic']['name'][$i];
                    $flag++;
                } else {
                    $rename_file[$i] = '';
                }
            }
            if ($flag > 0) {
                $this->load->library('upload');
                $this->upload->initialize(array(
                    "file_name" => $rename_file,
                    'upload_path' => './assets/images/bukti_pembayaran/',
                    'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                    'max_size' => '2000' //Max 2MB
                ));

                if ($this->upload->do_multi_upload("pic")) {
                    $info = $this->upload->get_multi_upload_data();
                    foreach ($info as $in) {
                        $picx = substr($in['file_name'], 0, 4);
                        $data[$picx] = $in['file_name'];
                    }
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    echo "Errors Occured : "; //sini aja lah
                    print_r($error);
                }
            }
            //END SAVE FILE IMAGE
            if (!$this->biaya_model->proses_muhrim($data)) {
                throw new Exception('Ada kesalahan ketika simpan data mutasi nama!');
            }
            $msg = 'Biaya Akomodasi Jamaah ' . $data['nama'] . ' berhasil diproses!';
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
        }

        $this->session->set_flashdata('info', $msg);
        redirect('biaya_lain/cetak_invoice_muhrim/'.$data['id_jamaah']);
    }
    
    function cetak_invoice_muhrim($id_jamaah){
        $getdata = $this->biaya_model->get_data_aktivasi($id_jamaah,'144')->result();
        $data = array();
        foreach($getdata as $row){
            $data = array(
                        'id' => $row->id,
                        'id_booking' => $row->id_booking,
                        'id_jamaah' => $row->registrasi_jamaah_id_jamaah,
                        'nama' => $row->nama,
                        'alamat' => $row->alamat,
                        'telp' => $row->telp,
                        'biaya_akomodasi' => $row->nominal,
                        'keterangan' => $row->keterangan,
                        'pay_method' => $row->payment_method,
                        'payment_date' => $row->payment_date,
                        'bank_transfer' => $row->bank,
                        'kd_trans' => $row->kd_trans
                    );
           
        }
        
        $this->load->view('inv_muhrim', $data);
    }

    private function _select_pay_method() {
        return $this->db->get('pay_method')->result();
    }

    private function _select_bank() {
        return $this->db->get('bank')->result();
    }

}
