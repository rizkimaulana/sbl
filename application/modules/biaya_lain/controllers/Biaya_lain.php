<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biaya_lain extends CI_Controller {

    var $folder = "biaya_lain";

    public function __construct() {

    parent::__construct();
        if (!$this->session->userdata('is_login'))
            redirect('frontend/login');
        if (!$this->general->privilege_check(BIAYA_LAIN_LAIN, 'view'))
            $this->general->no_access();
        $this->session->set_userdata('menu', 'biaya_lain');
        $this->load->model('biaya_model');
    }

    public function akomodasi() {
        $this->template->load('template/template', $this->folder . '/akomodasi');
    }
    
//    public function ajax_akomodasi_list1() {
//        
//        $list = $this->biaya_model->get_datatables();
//        //$this->myDebug($list);
//        $data = array();
//        $no = $_POST['start'];//isset($_POST['start']) ? $_POST['start'] : '';
//        foreach ($list as $r) {
//            $no++;
//            $row = array();
//            $row[] = $no;
//            
//            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah' => $r->tipe_jamaah))->row_array();
//            $row[] = '<td width="20%" >Invoice : ' . $r->invoice . '</strong><br>
//	             			 <strong>(' . $r->id_jamaah . ' - ' . $r->nama . ')<br>
//	             			 Telp : ' . $r->telp . '<br>
//	             			 ' . $tipe['nama'] . '
//	               			 </td>';
//            $product = $this->db->get_where('product', array('id_product' => $r->id_product))->row_array();
//            $category = $this->db->get_where('category', array('id_room_category' => $r->category))->row_array();
//            $row[] = '<td width="18%" >' . $product['nama'] . ' - ' . $category['category'] . '<br>
//	             			 </td>';
//            $row[] = '<td width="18%" >Tgl Daftar : ' . date('d-m-Y', strtotime($r->tgl_daftar)) . '<br>
//	             			 Tgl Aktivasi : ' . date('d-m-Y', strtotime($r->update_date)) . '<br>
//                      </td>';
//            $biaya = $this->db->get_where('bandara', array('id_bandara' => $r->id_bandara))->row_array();
//            $row[] = number_format($r->akomodasi);
//
//            $aktivasi_biaya_lain = $this->db->get_where('aktivasi_biaya_lain', array('registrasi_jamaah_id_jamaah' => $r->id_jamaah, 'tipe_biaya' => '133'))->row_array();
//
//            if (count($aktivasi_biaya_lain['registrasi_jamaah_id_jamaah']) > 0) {
//                $row[] = '<font style="color:green;"><strong>Sudah di bayar</strong></font>';
//            } else {
//                $row[] = '<a title="Mutasi" class="btn btn-sm btn-primary" href="' . base_url() . 'biaya_lain/proses_akomodasi/' . $r->id_jamaah . '">
//	                            <i class="fa fa-pencil"></i>Proses
//	                        </a> ';
//            }
//            
//            $data[] = $row;
//        }
//        //$this->myDebug($this->r->count_all());
//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->r->count_all(),
//            "recordsFiltered" => $this->r->count_filtered(),
//            "data" => $data,
//        );
//        //output to json format
//        echo json_encode($output);
//    }
    
    public function ajax_akomodasi_list() {
        $keyword = $this->uri->segment(3) ? $this->uri->segment(3) : '';
        $keyword = urldecode($keyword);
        
        $list = $this->biaya_model->get_datatables($keyword);
        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : '';
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah' => $r->tipe_jamaah))->row_array();
            $row[] = '<td width="20%" >Invoice : ' . $r->invoice . '</strong><br>
	             			 <strong>(' . $r->id_jamaah . ' - ' . $r->nama . ')<br>
	             			 Telp : ' . $r->telp . '<br>
	             			 ' . $tipe['nama'] . '
	               			 </td>';
            $product = $this->db->get_where('product', array('id_product' => $r->id_product))->row_array();
            $category = $this->db->get_where('category', array('id_room_category' => $r->category))->row_array();
            $row[] = '<td width="18%" >' . $product['nama'] . ' - ' . $category['category'] . '<br>
	             			 </td>';
            $row[] = '<td width="18%" >Tgl Daftar : ' . date('d-m-Y', strtotime($r->tgl_daftar)) . '<br>
	             			 Tgl Aktivasi : ' . date('d-m-Y', strtotime($r->update_date)) . '<br>
                      </td>';
            $biaya = $this->db->get_where('bandara', array('id_bandara' => '2'))->row_array();
            $row[] = number_format($biaya['akomodasi']);

            $aktivasi_biaya_lain = $this->db->get_where('aktivasi_biaya_lain', array('registrasi_jamaah_id_jamaah' => $r->id_jamaah, 'tipe_biaya' => '133'))->row_array();

            if (count($aktivasi_biaya_lain['registrasi_jamaah_id_jamaah']) > 0) {
                $row[] = '<font style="color:green;"><strong>Sudah di bayar</strong></font>';
            } else {
                $row[] = '<a title="Proses" class="btn btn-sm btn-primary" href="' . base_url() . 'biaya_lain/proses_akomodasi/' . $r->id_jamaah . '">
	                            <i class="fa fa-pencil"></i>Proses
	                        </a> ';
            }
            
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //$this->myDebug($output);
        //output to json format
        echo json_encode($output);
    }

    function proses_akomodasi($id_jamaah) {

        $registrasi = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $id_jamaah))->row_array();
        $biaya = $this->db->get_where('bandara', array('id_bandara' => '2'))->row_array();
        $data = array(
            'id_jamaah' => $registrasi['id_jamaah'],
            'nama' => $registrasi['nama'],
            'id_booking' => $registrasi['id_booking'],
            'id_affiliate' => $registrasi['id_affiliate'],
            'biaya' => $biaya['akomodasi'],
            'select_bank' => $this->_select_bank(),
            'select_pay_method' => $this->_select_pay_method(),
        );

        $this->template->load('template/template', $this->folder . '/proses_akomodasi', $data);
    }

    function update_akomodasi() {
        $data = $this->input->post(null, true);
        try {
            //SAVE FILE IMAGE
            $flag = 0;
            $rename_file = array();
            for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                if ($_FILES['pic']['name'][$i]) {
                    $rename_file[$i] = 'pic' . ($i + 1) . '_mutasi_nama_' . $_FILES['pic']['name'][$i];
                    $flag++;
                } else {
                    $rename_file[$i] = '';
                }
            }
            if ($flag > 0) {
                $this->load->library('upload');
                $this->upload->initialize(array(
                    "file_name" => $rename_file,
                    'upload_path' => './assets/images/bukti_pembayaran/',
                    'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                    'max_size' => '2000' //Max 2MB
                ));

                if ($this->upload->do_multi_upload("pic")) {
                    $info = $this->upload->get_multi_upload_data();
                    foreach ($info as $in) {
                        $picx = substr($in['file_name'], 0, 4);
                        $data[$picx] = $in['file_name'];
                    }
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    echo "Errors Occured : "; //sini aja lah
                    print_r($error);
                }
            }
            //END SAVE FILE IMAGE
            if (!$this->biaya_model->proses_akomodasi($data)) {
                throw new Exception('Ada kesalahan ketika simpan data mutasi nama!');
            }
            $msg = 'Biaya Akomodasi Jamaah ' . $data['nama'] . ' berhasil diproses!';
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
        }

        $this->session->set_flashdata('info', $msg);
        redirect('biaya_lain/cetak_invoice_akomodasi/'.$data['id_jamaah']);
    }
    
    function cetak_invoice_akomodasi($id_jamaah){
        $getdata = $this->biaya_model->get_data_aktivasi($id_jamaah,'133')->result();
        $data = array();
        foreach($getdata as $row){
            $data = array(
                        'id' => $row->id,
                        'id_booking' => $row->id_booking,
                        'id_jamaah' => $row->registrasi_jamaah_id_jamaah,
                        'nama' => $row->nama,
                        'alamat' => $row->alamat,
                        'telp' => $row->telp,
                        'biaya_akomodasi' => $row->nominal,
                        'keterangan' => $row->keterangan,
                        'pay_method' => $row->payment_method,
                        'payment_date' => $row->payment_date,
                        'bank_transfer' => $row->bank,
                        'kd_trans' => $row->kd_trans
                    );           
        }
        
        $this->load->view('inv_akomodasi', $data);
    }
    
    function lap_akomodasi(){
        $this->template->load('template/template', $this->folder . '/lap_akomodasi');
    }

    public function ajax_lap_akomodasi() {
        $awal = !empty($this->uri->segment(3)) ? $this->uri->segment(3) : '';
        $akhir = !empty($this->uri->segment(4)) ? $this->uri->segment(4) : '';
        
        $filter_query = '';
        if(!empty($awal) && !empty($akhir)){
            $filter_query = " AND create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
        }
        
        $list = $this->biaya_model->get_data_aktivasi_lain($filter_query);
        $data = array();
        $no = 0;
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            
            $registrasi = $this->db->get_where('registrasi_jamaah', array('id_jamaah'=> $r->registrasi_jamaah_id_jamaah))->row_array();
            
            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah' => $registrasi['tipe_jamaah']))->row_array();
            $row[] = '<td width="20%" >Invoice : ' . $registrasi['invoice'] . '</strong><br>
	             			 <strong>(' . $registrasi['id_jamaah'] . ' - ' . $registrasi['nama'] . ')<br>
	             			 Telp : ' . $registrasi['telp'] . '<br>
	             			 ' . $tipe['nama'] . '
	               			 </td>';
            $product = $this->db->get_where('product', array('id_product' => $registrasi['id_product']))->row_array();
            $category = $this->db->get_where('category', array('id_room_category' => $registrasi['category']))->row_array();
            $row[] = '<td width="18%" >' . $product['nama'] . ' - ' . $category['category'] . '<br>
	             			 </td>';
            $row[] = '<td width="18%" >Tgl Daftar : ' . date('d-m-Y', strtotime($registrasi['tgl_daftar'])) . '<br>
	             			 Tgl Aktivasi : ' . date('d-m-Y', strtotime($registrasi['update_date'])) . '<br>
                      </td>';
            
            $row[] = number_format($r->nominal);
            
            $row[] = '<td width="18%" >'.
                        $r->payment_method.'<br>'.
                        $r->bank.'<br>'.
                        date('d-m-Y', strtotime($r->payment_date)).'<br>'.
                     '</td>';
           
            if ($r->status == 1) {
                $row[] = '<td width="18%" ><font style="color:green;"><strong>Sudah di bayar</strong></font></td>';
            }
            
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    
    function export_lap_akomodasi() {
        $awal = !empty($this->input->post('awal')) ? $this->input->post('awal') : '';
        $akhir = !empty($this->input->post('akhir')) ? $this->input->post('akhir') : '';

        $filter_query = '';
        if(!empty($awal) && !empty($akhir)){
            $filter_query = " AND create_date BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'";
        }

        $hasil = $this->biaya_model->get_data_aktivasi_lain($filter_query);

        $data = array();
        $no = 0;
        foreach ($hasil as $item) {
            $no++;
            
            //get data from table registrasi_jamaah
            $reg = $this->db->get_where('registrasi_jamaah', array('id_jamaah'=> $item->registrasi_jamaah_id_jamaah))->row_array();
            //get data tipe from table tipe_jamaah
            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah'=> $reg['tipe_jamaah']))->row_array();
            $item['tipe'] =  $tipe['nama'];
            //get data paket from table product
            $product = $this->db->get_where('product', array('id_product' => $registrasi['id_product']))->row_array();
            $item['paket'] =  $tipe['nama'];
            $category = $this->db->get_where('category', array('id_room_category' => $registrasi['category']))->row_array();
            
            $row[] = $no;
            $row[] = $item->kd_trans;
            $row[] = $reg['invoice'];
            $row[] = $reg['id_jamaah'];
            $row[] = $reg['nama'];
            $row[] = $reg['telp'];
            $row[] = $item['tipe'];
            $row[] = $item['paket'];
            
            $data[] = $item;
        }
        $data['list'] = $data;
        //print_r($data);
        $this->load->view('biaya_lain/lap_akomodasi_excel', ($data));
    }
    
    
    public function muhrim() {
        $this->template->load('template/template', $this->folder . '/muhrim');
    }

    public function ajax_muhrim_list() {
        $keyword = $this->uri->segment(3) ? $this->uri->segment(3) : '';
        $keyword = urldecode($keyword);
        
        $list = $this->biaya_model->get_data_muhrim($keyword);
        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : '';
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $tipe = $this->db->get_where('tipe_jamaah', array('id_tipe_jamaah' => $r->tipe_jamaah))->row_array();
            $row[] = '<td width="20%" >Invoice : ' . $r->invoice . '</strong><br>
	             			 <strong>(' . $r->id_jamaah . ' - ' . $r->nama . ')<br>
	             			 Telp : ' . $r->telp . '<br>
	             			 ' . $tipe['nama'] . '
	               			 </td>';
            $product = $this->db->get_where('product', array('id_product' => $r->id_product))->row_array();
            $category = $this->db->get_where('category', array('id_room_category' => $r->category))->row_array();
            $row[] = '<td width="18%" >' . $product['nama'] . ' - ' . $category['category'] . '<br>
	             			 </td>';
            $row[] = '<td width="18%" >Tgl Daftar : ' . date('d-m-Y', strtotime($r->tgl_daftar)) . '<br>
	             			 Tgl Aktivasi : ' . date('d-m-Y', strtotime($r->update_date)) . '<br>
                      </td>';
            $biaya = $this->db->get_where('setting', array('id_setting' => '9'))->row_array();
            $row[] = number_format($biaya['key_setting']);

            $aktivasi_biaya_lain = $this->db->get_where('aktivasi_biaya_lain', array('registrasi_jamaah_id_jamaah' => $r->id_jamaah, 'tipe_biaya' => '144'))->row_array();

            if (count($aktivasi_biaya_lain['registrasi_jamaah_id_jamaah']) > 0) {
                $row[] = '<font style="color:green;"><strong>Sudah di bayar</strong></font>';
            } else {
                $row[] = '<a title="Proses" class="btn btn-sm btn-primary" href="' . base_url() . 'biaya_lain/proses_muhrim/' . $r->id_jamaah . '">
                            <i class="fa fa-pencil"></i>Proses</a> ';
            }

            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function proses_muhrim($id_jamaah) {

        $registrasi = $this->db->get_where('registrasi_jamaah', array('id_jamaah' => $id_jamaah))->row_array();
        $biaya = $this->db->get_where('setting', array('id_setting' => '9'))->row_array();
        $data = array(
            'id_jamaah' => $registrasi['id_jamaah'],
            'nama' => $registrasi['nama'],
            'id_booking' => $registrasi['id_booking'],
            'id_affiliate' => $registrasi['id_affiliate'],
            'biaya' => $biaya['key_setting'],
            'select_bank' => $this->_select_bank(),
            'select_pay_method' => $this->_select_pay_method(),
        );

        $this->template->load('template/template', $this->folder . '/proses_muhrim', $data);
    }

    function update_muhrim() {
        $data = $this->input->post(null, true);
        //$this->myDebug($data);
        try {
            //SAVE FILE IMAGE
            $flag = 0;
            $rename_file = array();
            for ($i = 0; $i < count($_FILES['pic']['name']); $i++) {
                if ($_FILES['pic']['name'][$i]) {
                    $rename_file[$i] = 'pic' . ($i + 1) . '_mutasi_nama_' . $_FILES['pic']['name'][$i];
                    $flag++;
                } else {
                    $rename_file[$i] = '';
                }
            }
            if ($flag > 0) {
                $this->load->library('upload');
                $this->upload->initialize(array(
                    "file_name" => $rename_file,
                    'upload_path' => './assets/images/bukti_pembayaran/',
                    'allowed_types' => 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx',
                    'max_size' => '2000' //Max 2MB
                ));

                if ($this->upload->do_multi_upload("pic")) {
                    $info = $this->upload->get_multi_upload_data();
                    foreach ($info as $in) {
                        $picx = substr($in['file_name'], 0, 4);
                        $data[$picx] = $in['file_name'];
                    }
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    echo "Errors Occured : "; //sini aja lah
                    print_r($error);
                }
            }
            //END SAVE FILE IMAGE
            if (!$this->biaya_model->proses_muhrim($data)) {
                throw new Exception('Ada kesalahan ketika simpan data mutasi nama!');
            }
            $msg = 'Biaya Akomodasi Jamaah ' . $data['nama'] . ' berhasil diproses!';
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
        }

        $this->session->set_flashdata('info', $msg);
        redirect('biaya_lain/cetak_invoice_muhrim/'.$data['id_jamaah']);
    }
    
    function cetak_invoice_muhrim($id_jamaah){
        $getdata = $this->biaya_model->get_data_aktivasi($id_jamaah,'144')->result();
        $data = array();
        foreach($getdata as $row){
            $data = array(
                        'id' => $row->id,
                        'id_booking' => $row->id_booking,
                        'id_jamaah' => $row->registrasi_jamaah_id_jamaah,
                        'nama' => $row->nama,
                        'alamat' => $row->alamat,
                        'telp' => $row->telp,
                        'biaya_akomodasi' => $row->nominal,
                        'keterangan' => $row->keterangan,
                        'pay_method' => $row->payment_method,
                        'payment_date' => $row->payment_date,
                        'bank_transfer' => $row->bank,
                        'kd_trans' => $row->kd_trans
                    );
           
        }
        
        $this->load->view('inv_muhrim', $data);
    }

    private function _select_pay_method() {
        return $this->db->get('pay_method')->result();
    }

    private function _select_bank() {
        return $this->db->get('bank')->result();
    }

}
