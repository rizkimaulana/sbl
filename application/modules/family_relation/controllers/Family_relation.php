<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Family_relation extends CI_Controller{
	var $folder = "family_relation";
	public function __construct(){
		
		parent::__construct();
		if(!$this->session->userdata('is_login'))redirect('frontend/login');
		if(!$this->general->privilege_check(FAMILY_RELATION,'view'))
		    $this->general->no_access();
		$this->session->set_userdata('menu','master_data');	
		$this->load->model('familyrelation_model');
	}
	
	public function index(){
	    
	    $this->template->load('template/template', $this->folder.'/family_relation');
		
	}
	

	private function _select_status(){
		$kdstatus = array('1', '0');
		$this->db->where_in('kdstatus', $kdstatus);
		return $this->db->get('status')->result();
	    // return $this->db->get('status')->result();
	    // $status = array('1'=>'Active','0'=>'Non Active');
	}

	
	public function get_data(){
	    	    
	    $limit = $this->config->item('limit');
	    $offset= $this->uri->segment(3,0);
	    $q     = isset($_POST['q']) ? $_POST['q'] : '';	    
	    $data  = $this->familyrelation_model->get_data($offset,$limit,$q);
	    $rows  = $paging = '';
	    $total = $data['total'];
	    
	    if($data['data']){
	        
	        $i= $offset+1;
	        $j= 1;
	        foreach($data['data'] as $r){
	            
	            $rows .='<tr>';
	                
	                $rows .='<td>'.$i.'</td>';
	                // $rows .='<td width="10%">'.$r->kode.'</td>';
	                // $rows .='<td width="40%">'.$r->nama.'</td>';
	                $rows .='<td width="70%">'.$r->keterangan.'</td>';
	                $rows .='<td width="30%" align="center">';
	                
	                $rows .='<a title="Edit" class="btn btn-sm btn-primary" href="'.base_url().'family_relation/edit/'.$r->id_relation.'">
	                            <i class="fa fa-pencil"></i> Edit
	                        </a> ';
	                $rows .='<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_family('."'".$r->id_relation."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
	               
	               $rows .='</td>';
	            
	            $rows .='</tr>';
	            
	            ++$i;
	            ++$j;
	        }
	        
	        $paging .= '<li><span class="page-info">Displaying '.($j-1).' Of '.$total.' items</span></i></li>';
            $paging .= $this->_paging($total,$limit);
	        	       	        
	    	    
	    }else{
	        
	        $rows .='<tr>';
	            $rows .='<td colspan="6">No Data</td>';
	        $rows .='</tr>';
	        
	    }
	    
	    echo json_encode(array('rows'=>$rows,'total'=>$total,'paging'=>$paging));
	}
	
	private function _paging($total,$limit){
	
	    $config = array(
                
            'base_url'  => base_url().'family_relation/get_data/',
            'total_rows'=> $total, 
            'per_page'  => $limit,
			'uri_segment'=> 3
        
        );
        $this->pagination->initialize($config); 

        return $this->pagination->create_links();
	}
	
	public function save(){
	    
	     $data = $this->input->post(null,true);
	     
	     $send = $this->familyrelation_model->save($data);
	     if($send)
	        redirect('family_relation');
	}

	public function add(){
	     
	     if(!$this->general->privilege_check(FAMILY_RELATION,'add'))
		    $this->general->no_access();

	    
	    // $data = array('select_status'=>$this->_select_status());
        $this->template->load('template/template', $this->folder.'/add');	
	}
	
	public function edit(){
	     
	     if(!$this->general->privilege_check(FAMILY_RELATION,'edit'))
		    $this->general->no_access();
	    
	    $id = $this->uri->segment(3);
	    $get = $this->db->get_where('family_relation',array('id_relation'=>$id))->row_array();
	    if(!$get)
	        show_404();
	        
	   
        $this->template->load('template/template', $this->folder.'/edit',array_merge($get));
	}

	public function update(){
	     $data = $this->input->post(null,true);
	     $send = $this->familyrelation_model->update($data);
	     if($send)
	        redirect('family_relation');
		
	}
	
	
	public function ajax_delete($id_relation)
	{
		if(!$this->general->privilege_check(FAMILY_RELATION,'remove'))
		    $this->general->no_access();
		$send = $this->familyrelation_model->delete_by_id($id_relation);
		echo json_encode(array("status" => TRUE));
		if($send)
	        redirect('family_relation');
	}


}
