<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice {
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

    function send_sms($nomer_tujuan, $pesan){
        $data = array(
            'nomer_tujuan' => $nomer_tujuan,
            'pesan' => $pesan
        );
        try {
            $return = '0';
            if(empty($nomer_tujuan)){
                throw new Exception('data nomer tujuan tidak valid');
            }
            if(empty($pesan)){
                throw new Exception('data pesan tidak valid');
            }
            $api_element = '/api/web/send/';
            $api_params = "http://103.16.199.187/masking/send.php?username=sahabatsbl2&password=123456789&hp=" . $nomer_tujuan . "&message=" . str_replace(' ', '+', $pesan);
            $smsgatewaydata = $api_params;
            $url = $smsgatewaydata;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if(!curl_exec($ch)){
                throw new Exception(file_get_contents($smsgatewaydata));
            }
            curl_close($ch);
            return TRUE;
        } catch (Exception $exc) {
            $msg = $exc->getMessage();
            return FALSE;
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc=" JPI Web Service API">
    function service_mobile_sbl($service, $data){
        $username = 'jpi';
        $password = 'jpi123123';
        $url = "http://103.52.146.2:4153/services"; // Production Server
        //$url = "http://103.52.146.3:4169/services"; // Development Server
        $list_data = array(
            'service' => $service,
            'data' => $data
        );
        $data_string = json_encode($list_data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            'useragent: dsJPI')
        );        
        $result = curl_exec($ch);
        $hasil = json_decode($result, TRUE);
        return $hasil;
    }
    
    /**
     * Service yang digunakan untuk registrasi member/user SBLMobile
     * @param type $data
     * @return type
     */
    function service_registrasi_mobile($data){
        $service = "registerSBLMobile";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk menampilkan informasi virtual account member SBLMobile
     * @param type $data
     * @return type
     */
    function service_get_va($data){
        $service = "getVa";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk mem-posting fee/bonus ke virtual account SBLMobile
     * @param type $data
     * @return type
     */
    function service_posting_fee($data){
        $service = "postingFee";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk menampilkan informasi saldo dari virtual account SBLMobile
     * @param type $data
     * @return type
     */
    function service_get_balance($data){
        $service = "getBalance";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk menampilkan informasi statement (rekening koran) dari virtual account SBLMobile berdasarkan tanggal transaksi tertentu
     * @param type $data
     * @return type
     */
    function service_get_statement($data){
        $service = "getStatement";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk menampilkan informasi statement (rekening koran) dari virtual account SBLMobile berdasarkan range tanggal transaksi
     * @param type $data
     * @return type
     */
    function service_get_statements($data){
        $service = "getStatements";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk mengubah IMEI account SBLMobile
     * @param type $data
     * @return type
     */
    function service_ubah_imei($data){
        $service = "ubahIMEI";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk mengubah data referensi Bank member yang akan digunakan untuk WD (Withdrawal)
     * @param type $data
     * @return type
     */
    function service_account_bank($data){
        $service = "ubahAccountBank";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk mem-block status user/member SBLMobile
     * @param type $data
     * @return type
     */
    function service_block_member($data){
        $service = "blockMember";
        return $this->service_mobile_sbl($service, $data);
    }
    
    /**
     * Service untuk mengubah IMEI account SBLMobile
     * @param type $data
     * @return type
     */
    function service_ubah_password($data){
        $service = "ubahPassword";
        return $this->service_mobile_sbl($service, $data);
    }
    // </editor-fold>
    
    
    
    // <editor-fold defaultstate="collapsed" desc=" FUNGSI UNTUK WEB SERVICE KONVEN">
    function ws_get_schedule($id_schedule){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_schedule');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_schedule');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'id_schedule' => $id_schedule
        ));

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_take_schedule($id_schedule, $jumlah_seat){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/take_schedule');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/take_schedule');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'id_schedule' => $id_schedule,
            'jumlah_seat' => $jumlah_seat
        ));

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_jamaah($invoice){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_jamaah_booking');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_jamaah_booking');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'invoice' => $invoice
        ));

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_kelamin(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_kelamin');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_kelamin');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_nikah(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_nikah');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_nikah');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        
        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_umur(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_umur');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_umur');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_berangkat(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_berangkat');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_berangkat');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_pemberangkatant(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_pemberangkatan');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_pemberangkatan');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_waris(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_waris');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_waris');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_merchandise(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_merchandise');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_merchandise');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_list_visa(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_list_visa');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_list_visa');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_simpan_registrasi_jamaah($data){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/save_registrasi_jamaah');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/save_registrasi_jamaah');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'data' => json_encode($data)
        ));
        
        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_registrasi_jamaah($id_jamaah){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_registrasi_jamaah');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_registrasi_jamaah');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'id_jamaah' => $id_jamaah
        ));

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    
    function ws_get_manifest_jamaah($id_jamaah){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_manifest_jamaah');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_manifest_jamaah');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'id_jamaah' => $id_jamaah
        ));

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
        
    function get_ws_embarkasi(){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';
        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/departure');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/departure');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer);
        return $result;
    }
    
    function get_ws_waktu_keberangkatan($tahun, $bulan, $embarkasi){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/search_waktu_keberangkatan');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/search_waktu_keberangkatan');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'tahun' => $tahun,
            'bulan' => $bulan,
            'embarkasi' => $embarkasi
        ));

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer);
        return $result;
    }
    
    function ws_get_data_embarkasi($id_embarkasi){
        $username = 'sbl';
        $password = 'ws_sblkonven2017';

        $curl_handle = curl_init();
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/sbl/ws_konvensbl/get_data_embarkasi');
        curl_setopt($curl_handle, CURLOPT_URL, 'http://office.sbl.co.id/ws_konvensbl/get_data_embarkasi');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'data' => $id_embarkasi
        ));
        
        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer, TRUE);
        return $result;
    }
    // </editor-fold>
}