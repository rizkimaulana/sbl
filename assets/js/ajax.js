$('#next_step').hide();
$(document).ready(function(){
  $("#check").click(function()
    {
      var Schedule_list = [];
      var Schedule = [];


     $.ajax({
         type: "POST",
         url: base_url + "Schedule/get_data",
         data: {date_schedule: $("#date_schedule").val()},

         dataType: 'json',
         cache:false,
         success:
            function(data){

              var obj = jQuery.parseJSON(data);
              var Schedule = '';

              if(obj.length === 0){
                $('#next_step').hide().attr( "disabled", "disabled" );
                Schedule = '<h2 class="text-muted" style="text-align:center; margin-top: 20%;">'+currently+'<br/> '+from+' <strong>'+$("#from option:selected").text()+'</strong> '+to+' <strong>'+$("#to option:selected").text()+'</strong></h2>';
              }
              else{
                $('#next_step').show().attr( "disabled", "disabled" );
                Schedule += ('<div class="col-sm-12 col-md-12">');
                Schedule += ('<div class="table-responsive ">');
                Schedule += ('<table class="table table-bordered">');
                Schedule += ('<thead>');
                Schedule += ('<th></th>');
                Schedule += ('<th>departure Date</th>');
                Schedule += ('<th>date_schedule</th>');
                Schedule += ('<th>seats</th> ');
                Schedule += ('</thead>');
                Schedule += ('<tbody id="from_results">');

                $.each( obj, function(index, value) {
                  if(value.ticket_type == 'one_way') {
                    Schedule += ("<tr>");
                    Schedule += ('<td style="text-align:center;"><input type="radio" name="selected_one_way" value="' + index + '"></td>');
                    Schedule += ('<td>' + $("#from option:selected").text() + ' <span class="icon-arrow-right"></span> ' + $("#to option:selected").text() + '</td>');
                    Schedule += ("<td>" + value.start_time + "</td>");
                    Schedule += ("<td>" + value.start_price + " " + value.currency + "</td>");
                    Schedule += ("</tr>");
                  }
                });

                Schedule += ('</tbody>');
                Schedule += ('</table>');
                Schedule += ('</div>');
                Schedule += ('</div><!-- col-sm -->');
                Schedule += ('</div><!-- .table-responsive -->');

                  //RETURNING TABLE
                  if($('input[name="returning"]:checked').length > 0) {
                    Schedule += ('<div class="col-sm-12 col-md-12">');
                    Schedule += ('<div class="table-responsive ">');
                    Schedule += ('<table class="table table-bordered">');
                    Schedule += ('<thead>');
                    Schedule += ('<th></th>');
                    Schedule += ('<th>'+destination+'</th>');
                    Schedule += ('<th>'+time+'</th>');
                    Schedule += ('<th>'+price+'</th> ');
                    Schedule += ('</thead>');
                    Schedule += ('<tbody id="from_results">');

                    $.each( obj, function(index, value) {
                      if(value.ticket_type == 'returning') {
                        Schedule += ("<tr>");
                        Schedule += ('<td style="text-align:center;"><input type="radio" name="selected_returning" value="' + index + '"></td>');
                        Schedule += ('<td>' + $("#to option:selected").text() + ' <span class="icon-arrow-right"></span> ' + $("#from option:selected").text() + '</td>');
                        Schedule += ("<td>" + value.start_time + "</td>");
                        Schedule += ("<td>" + value.start_price + " " + value.currency + "</td>");
                        Schedule += ("</tr>");
                      }
                    });

                    Schedule += ('</tbody>');
                    Schedule += ('</table>');
                    Schedule += ('</div>');
                    Schedule += ('</div><!-- col-sm -->');
                    Schedule += ('</div><!-- .table-responsive -->');
                  }
              }
              $('.booking_results').html(Schedule);

              //enable NEXT button if atleast the one way tour is selected from the results list
              $('input[type=radio][name=selected_one_way]').change(function(){
                 if ($(this).is(':checked')) {
                      $('#next_step').attr("disabled", false);
                    }
              });

            }//endof function(data)

    });
     return false;
	});



  $("#next_step").click(function()
  {
    var tour_back_id = '';
    if( typeof $('input:radio[name=selected_returning]:checked').val() != 'undefined'){
      tour_back_id = $('input:radio[name=selected_returning]:checked').val();
    }

    var tour_id = $('input:radio[name=selected_one_way]:checked').val();
    var from = $("#from option:selected").text()
    var to = $("#to option:selected").text();
    var tickets = $('#booked_seats').val();
    //alert(tour_id);
    window.location = 'process_ticket?from='+from+'&to='+to+'&tickets='+tickets+'&tour_id='+tour_id+'&tour_back_id=' + tour_back_id;
  });

});
